
package com.gcloud.mesh.dcs.entity;

import com.gcloud.framework.db.jdbc.annotation.ID;
import com.gcloud.framework.db.jdbc.annotation.Table;

import lombok.Data;

@Table(name = "dcs_scheduler_models")
@Data
public class SchedulerModelEntity {

    @ID
    private String id;
    private String schedulerModelTypeId;
    

}
