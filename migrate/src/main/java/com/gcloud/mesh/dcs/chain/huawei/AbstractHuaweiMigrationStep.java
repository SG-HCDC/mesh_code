package com.gcloud.mesh.dcs.chain.huawei;

import com.gcloud.mesh.dcs.chain.IMigrationStepChain;
import com.gcloud.mesh.supplier.enums.SystemType;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public abstract class AbstractHuaweiMigrationStep implements IMigrationStepChain {

    @Override
    public String chainType() {
        return SystemType.HUAWEI.getName();
    }
}
