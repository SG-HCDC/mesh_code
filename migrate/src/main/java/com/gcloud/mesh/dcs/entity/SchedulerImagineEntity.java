package com.gcloud.mesh.dcs.entity;

import java.util.Date;

import com.gcloud.framework.db.jdbc.annotation.ID;
import com.gcloud.framework.db.jdbc.annotation.Table;

import lombok.Data;

@Table(name = "dcs_scheduler_imagine")
@Data
public class SchedulerImagineEntity {

	 @ID
	 private String id;
	 private String appId;
	 private String sourceNodeId;
	 private String destNodeId;
	 private String modelType;
	 private Double difference;
	 private Double temperatureImagine;
	 private Double distributionBoxVoltageImagine;
	 private Double datacenterEnergyImagine;
	 private Double businessRespondImagine;
	 private Date updateTime;
	 
	 private Double temperature;
	 private Double distributionBoxVoltage;
	 private Double datacenterEnergy;
	 private Double businessRespond;
	 
	 private String values;
	 private String factorTypes;
	 private String imagines;
}
