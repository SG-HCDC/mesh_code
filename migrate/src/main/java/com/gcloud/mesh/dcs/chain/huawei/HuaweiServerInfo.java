package com.gcloud.mesh.dcs.chain.huawei;

import java.util.List;

import com.huaweicloud.sdk.sms.v3.model.TargetDisks;

import lombok.Data;
@Data
public class HuaweiServerInfo {
	private List<TargetDisks> disks;
	private String osType;
}
