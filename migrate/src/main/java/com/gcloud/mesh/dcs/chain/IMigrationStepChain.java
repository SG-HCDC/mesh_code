package com.gcloud.mesh.dcs.chain;

public interface IMigrationStepChain extends IChain{
    /**
     * 异步调用时需要实现，检查步骤状态
     * @param stepId 步骤ID
     * @return
     */
    default boolean checkStatus(String stepId){return false;};

    /**
     * 步骤类型 SystemType 中获取
     * @return
     */
    String chainType();

    /**
     * 步骤名称
     * @return
     */
    String chainName(String attach);

    /**
     * 是否是同步步骤，默认是同步
     * @return
     */
    default boolean isSyncChain(){return true;};

    /**
     * 执行结果
     * @return
     */
    default String getExecResult(){return null;};
}
