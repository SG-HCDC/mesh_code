package com.gcloud.mesh.dcs.dataclean.consumer;

public interface IConsumer {

	public Object process(Object t);
	
	default public String getName(Object obj) {
		
		String[] spaces = obj.getClass().getTypeName().split("\\.");
		if(spaces.length > 0) {
			return spaces[spaces.length-1];
		}
		return null;
	}
}
