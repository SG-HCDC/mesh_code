
package com.gcloud.mesh.dcs.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.gcloud.mesh.dcs.dao.SchedulerStepDao;
import com.gcloud.mesh.header.msg.dcs.ListSchedulerStepMsg;
import com.gcloud.mesh.header.vo.dcs.SchedulerStepVo;

import lombok.extern.slf4j.Slf4j;

@Service
//@CacheConfig(cacheNames = Module.COMPONENT)
@Slf4j
public class SchedulerStepService {

    @Autowired
    private SchedulerStepDao dao;

    public List<SchedulerStepVo> list(ListSchedulerStepMsg msg) {
        return this.dao.list(msg.getSchedulerJobId(), SchedulerStepVo.class);
    }

}
