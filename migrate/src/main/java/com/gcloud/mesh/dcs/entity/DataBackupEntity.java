
package com.gcloud.mesh.dcs.entity;

import java.util.Date;

import com.gcloud.framework.db.jdbc.annotation.ID;
import com.gcloud.framework.db.jdbc.annotation.Table;

import lombok.Data;

@Table(name = "dcs_data_backups")
@Data
public class DataBackupEntity {

    @ID
    private String id;
    private String name;
    private String policy;
    private Date createTime;

}
