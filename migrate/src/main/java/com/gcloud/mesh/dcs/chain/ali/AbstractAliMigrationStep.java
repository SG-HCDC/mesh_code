package com.gcloud.mesh.dcs.chain.ali;

import com.aliyun.smc20190601.Client;
import com.aliyun.teaopenapi.models.Config;
import com.gcloud.mesh.dcs.chain.IMigrationStepChain;
import com.gcloud.mesh.supplier.enums.SystemType;
import lombok.extern.slf4j.Slf4j;
import org.jeecg.common.exception.MyBusinessException;

@Slf4j
public abstract class AbstractAliMigrationStep implements IMigrationStepChain {

    Integer maxCheckTime = 10;

    public  com.aliyun.ecs20140526.Client createECSClient(String accessKeyId, String accessKeySecret,String regionId) {
        com.aliyun.teaopenapi.models.Config config = new com.aliyun.teaopenapi.models.Config()
                // 您的AccessKey ID
                .setAccessKeyId(accessKeyId)
                // 您的AccessKey Secret
                .setAccessKeySecret(accessKeySecret);
        // 访问的域名
        config.endpoint = "ecs."+regionId+".aliyuncs.com";
        try {
            return new com.aliyun.ecs20140526.Client(config);
        } catch (Exception e) {
            log.error("创建阿里云同步连接失败",e);
            throw new MyBusinessException("创建阿里云同步连接失败"+"::"+e.getMessage());
        }
    }

    @Override
    public String chainType() {
        return SystemType.ALI.getName();
    }


    public Client createSMCClient(String accessKeyId, String accessKeySecret) {
        Config config = new Config()
                // 您的AccessKey ID
                .setAccessKeyId(accessKeyId)
                // 您的AccessKey Secret
                .setAccessKeySecret(accessKeySecret);
        // 访问的域名
        config.endpoint = "smc.aliyuncs.com";
        try {
            return  new Client(config);
        } catch (Exception e) {
            log.error("创建阿里云同步连接失败",e);
            throw new MyBusinessException("创建阿里云同步连接失败"+"::"+e.getMessage());
        }
    }

}
