package com.gcloud.mesh.dcs.rpc;

import io.grpc.Server;
import io.grpc.ServerBuilder;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;

import javax.annotation.PostConstruct;
import java.io.IOException;

@Slf4j
@Configuration
public class RPCServer {
    @Autowired
    private RPCConfig rpcConfig;
    @Autowired
    private RPCReportDvfsService rpcReportDvfsService;

    @PostConstruct
    public void init() throws IOException {
        if (!rpcConfig.isEnable()) {
            return;
        }
        Server server = ServerBuilder.
                forPort(rpcConfig.getControllerPort())
                .addService(rpcReportDvfsService)
                .build()
                .start();
        log.info("grpc服务端启动成功, 端口=" + rpcConfig.getControllerPort());
    }
}
