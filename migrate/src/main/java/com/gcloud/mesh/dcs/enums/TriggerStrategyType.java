package com.gcloud.mesh.dcs.enums;

public enum TriggerStrategyType {

	MANUAL(0, "manual", "手动"), AUTO(1, "auto","自动");

	private Integer value;
	private String name;
	private String cname;

	TriggerStrategyType(Integer value, String name, String cname) {
		this.value = value;
		this.name = name;
		this.cname = cname;
	}

	public static TriggerStrategyType get(String value) {
		if (value != null) {
			for (TriggerStrategyType driver : TriggerStrategyType.values()) {
				if (driver.name().equals(value)) {
					return driver;
				}
			}
		}
		return null;
	}

	public Integer getValue() {
		return value;
	}

	public void setValue(Integer value) {
		this.value = value;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCname() {
		return cname;
	}

	public void setCname(String cname) {
		this.cname = cname;
	}

}
