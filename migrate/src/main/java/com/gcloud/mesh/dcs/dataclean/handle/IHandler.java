package com.gcloud.mesh.dcs.dataclean.handle;

import com.gcloud.mesh.header.msg.DataCleanBaseMsg;

public interface IHandler<T extends DataCleanBaseMsg> {

	public T process(T msg);
}
