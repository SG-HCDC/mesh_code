package com.gcloud.mesh.dcs.chain.ali;

import com.alibaba.fastjson.JSONObject;
import com.aliyun.ecs20140526.Client;
import com.aliyun.ecs20140526.models.DescribeDisksRequest;
import com.gcloud.mesh.dcs.chain.FilterChain;
import com.gcloud.mesh.dcs.chain.IMigrationStepChain;
import lombok.extern.slf4j.Slf4j;
import org.jeecg.common.exception.MyBusinessException;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class FindSystemDiskSize_2 extends AbstractAliMigrationStep {
    @Override
    public void doExec(String schedulerJobId, String attach, FilterChain filterChain, IMigrationStepChain stepChain) {
        JSONObject jsonObject = JSONObject.parseObject(attach);
        String srcRegionId = jsonObject.getString("srcRegionId");
        String srcInstanceId = jsonObject.getString("srcInstanceId");
        String accessKeyId = jsonObject.getString("accessKeyId");
        String accessSecret = jsonObject.getString("accessSecret");
        Integer systemDiskSize = getSystemDiskSize(accessKeyId,accessSecret,srcRegionId,srcInstanceId);
        jsonObject.put("systemDiskSize",systemDiskSize);
        filterChain.doExec(schedulerJobId,jsonObject.toString(),filterChain,stepChain);
    }


    @Override
    public String chainName(String attach) {
        return "获取源虚拟机系统盘信息";
    }

    @Override
    public int getOrder() {
        return 2;
    }

    public Integer getSystemDiskSize(String accessKeyId, String accessKeySecret,String regionId,String instanceId){
        try {
            Client client = createECSClient(accessKeyId, accessKeySecret, regionId);
            DescribeDisksRequest describeDisksRequest = new DescribeDisksRequest()
                    .setRegionId(regionId)
                    .setInstanceId(instanceId)
                    .setDiskType("system");
           return client.describeDisks(describeDisksRequest).getBody().getDisks().getDisk().get(0).getSize();
        }catch (Exception e){
            log.error("获取源虚拟机系统盘信息失败",e);
            throw new MyBusinessException("获取源虚拟机系统盘信息失败"+"::"+e.getMessage());
        }
    }


}
