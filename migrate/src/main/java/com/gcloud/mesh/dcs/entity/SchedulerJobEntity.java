
package com.gcloud.mesh.dcs.entity;

import com.gcloud.framework.db.jdbc.annotation.ID;
import com.gcloud.framework.db.jdbc.annotation.Table;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

@Table(name = "dcs_scheduler_jobs")
@Data
public class SchedulerJobEntity implements Serializable {

    @ID
    private String id;
    private String appId;
    private Integer status;
    private String srcDatacenterId;
    private String dstDatacenterId;
    private String dstNodeId;
    private Date beginTime;
    private Date endTime;
    private Integer schedulerModelTool;
    private Integer schedulerStrategyConfig;
    private Integer schedulerModel;
    private String appName;
    private String cloudResourceTypeName;
    private String cloudResourceId;
    private String cloudResourceName;
    private String sourceNodeId;
    private String instanceId;
    private String instanceName;
    private String dstCloudResourceId;
}
