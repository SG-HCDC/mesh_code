package com.gcloud.mesh.dcs.enums;

import org.springframework.util.StringUtils;

import java.util.Objects;



public enum AliPowerEventRepType {
    ME_NONE("ME_NONE"),

    ME_TOKEN_UPDATED("ME_TOKEN_UPDATED"),

    ME_DEVICE_READY("ME_DEVICE_READY"),

    ME_DEVICE_RELEASE("ME_DEVICE_RELEASE"),

    ME_DEVICE_ONLINE("ME_DEVICE_ONLINE"),

    ME_DEVICE_OFFLINE("ME_DEVICE_OFFLINE"),

    ME_DEVICE_UPDATED("ME_DEVICE_UPDATED"),

    ME_DEVICE_RULE("ME_DEVICE_RULE");

    private String name;

    AliPowerEventRepType(String name) {
        this.name = name;
    }

    public static AliPowerEventRepType getByName(String name) {
        if (!StringUtils.isEmpty(name)) {
            for (AliPowerEventRepType type : AliPowerEventRepType.values()) {
                if (Objects.equals(type.name, name)) {
                    return type;
                }
            }
        }
        return null;
    }
}
