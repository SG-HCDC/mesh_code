package com.gcloud.mesh.dcs.dao;

import com.gcloud.framework.db.PageResult;
import com.gcloud.framework.db.dao.impl.JdbcBaseDaoImpl;
import com.gcloud.mesh.dcs.entity.VoltageFmEntity;
import io.micrometer.core.instrument.util.StringUtils;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

@Repository
public class VoltageFmDao extends JdbcBaseDaoImpl<VoltageFmEntity, String>{

	public <E> PageResult<E> getPage(Integer pageNo,Integer pageSize,String factorName,String datacenterId,Class<E> clazz) {
		StringBuilder sql=new StringBuilder();
		sql.append("SELECT i.id id,i.name factor_name,n.ipmi_ip,i.datacenter_id,d.name datacenter_name,v.rated_power,v.power_consumption,v.voltage_fm_strategy,v.support_strategies,v.control_type" +
				" FROM asset_iaas i" + 
				" LEFT JOIN asset_nodes n ON n.id = i.device_id" + 
				" LEFT JOIN asset_datacenters d ON d.id = i.datacenter_id" + 
				" LEFT JOIN dcs_voltage_fm v ON v.resource_id = i.id" + 
				" WHERE 1 = 1 AND i.type = 6");
		List<Object> values=new ArrayList<>();
		if (StringUtils.isNotEmpty(factorName)) {
			sql.append(" AND i.name like concat('%',?,'%')");
			values.add(factorName);
		}
		if (StringUtils.isNotEmpty(datacenterId)) {
			sql.append(" AND i.datacenter_id =?");
			values.add(datacenterId);
		}
		return this.findBySql(sql.toString(),values,pageNo,pageSize,clazz);
	}

}
