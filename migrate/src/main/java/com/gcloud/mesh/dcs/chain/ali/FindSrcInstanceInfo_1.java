package com.gcloud.mesh.dcs.chain.ali;

import com.alibaba.fastjson.JSONObject;
import com.aliyun.ecs20140526.models.DescribeInstancesRequest;
import com.aliyun.ecs20140526.models.DescribeInstancesResponseBody;
import com.gcloud.mesh.dcs.chain.FilterChain;
import com.gcloud.mesh.dcs.chain.IMigrationStepChain;
import com.gcloud.mesh.header.param.AliMigrationParam;
import com.gcloud.mesh.header.vo.dcs.ALiVMInstantInfoVO;
import lombok.extern.slf4j.Slf4j;
import org.jeecg.common.exception.MyBusinessException;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class FindSrcInstanceInfo_1 extends AbstractAliMigrationStep {


    @Override
    public void doExec(String schedulerJobId, String attach, FilterChain filterChain, IMigrationStepChain stepChain) {
        JSONObject jsonObject = JSONObject.parseObject(attach);
        AliMigrationParam aliMigrationParam = JSONObject.toJavaObject(jsonObject,AliMigrationParam.class);
        log.info("====>>>>>>>阿里云迁移参数："+JSONObject.toJSONString(aliMigrationParam));
        String accessKeyId = aliMigrationParam.getAccessKeyId();
        String accessSecret = aliMigrationParam.getAccessSecret();
        ALiVMInstantInfoVO srcInstanceInfo = this.getInstanceInfo(accessKeyId,accessSecret,aliMigrationParam.getSrcRegionId(),aliMigrationParam.getSrcInstanceId(),super.maxCheckTime);
        jsonObject.put("srcInstanceInfo",srcInstanceInfo);
        filterChain.doExec(schedulerJobId,jsonObject.toString(),filterChain,stepChain);
    }

    @Override
    public String chainName(String attach) {
        return "获取源虚拟机实例信息";
    }

    @Override
    public int getOrder() {
        return 1;
    }


    public ALiVMInstantInfoVO getInstanceInfo(String accessKeyId, String accessKeySecret,String srcRegionId,String srcInstanceId,Integer maxCheckTime){
        ALiVMInstantInfoVO vo = new ALiVMInstantInfoVO();
        try {
            DescribeInstancesRequest describeInstancesRequest = new DescribeInstancesRequest()
                    .setRegionId(srcRegionId)
                    .setInstanceIds("[\""+srcInstanceId+"\"]");
            DescribeInstancesResponseBody.DescribeInstancesResponseBodyInstancesInstance ii = createECSClient(accessKeyId, accessKeySecret, srcRegionId)
                    .describeInstances(describeInstancesRequest).getBody().getInstances().getInstance().get(0);
            vo.setInstanceType(ii.getInstanceType());
            vo.setInstanceChargeType(ii.getInstanceChargeType());
            vo.setInternetChargeType(ii.getInternetChargeType());
            vo.setInternetMaxBandwidthIn(ii.getInternetMaxBandwidthIn());
            vo.setInternetMaxBandwidthOut(ii.getInternetMaxBandwidthOut());
            vo.setHostName(ii.getHostName());
            vo.setInstanceId(ii.getInstanceId());
        } catch (Exception e) {
            if(maxCheckTime!=null && maxCheckTime-->0){
                return getInstanceInfo(accessKeyId,accessKeySecret,srcRegionId,srcInstanceId,maxCheckTime);
            }
            log.error("获取源虚拟机实例信息失败",e);
            throw new MyBusinessException("获取源虚拟机实例信息失败"+"::"+e.getMessage());
        }
        return vo;
    }
}
