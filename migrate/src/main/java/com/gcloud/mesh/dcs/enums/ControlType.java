package com.gcloud.mesh.dcs.enums;

import com.gcloud.mesh.header.exception.BaseException;
import com.gcloud.mesh.header.exception.DvfsErrorCode;

public enum ControlType {
    AUTO(0,"auto","自动"),
    MANUAL(1,"manual","手动");

    /*编号*/
    private int no;
    /*名字*/
    private String name;
    /*中文名字*/
    private String cnName;

    ControlType(int no, String name, String cnName) {
        this.no = no;
        this.name = name;
        this.cnName = cnName;
    }

    public int getNo() {
        return no;
    }

    public String getName() {
        return name;
    }

    public String getCnName() {
        return cnName;
    }

    public static Integer checkAndGet(Integer controlType){
        for (ControlType type:ControlType.values() ){
            if (type.getNo()==controlType){
                return type.getNo();
            }
        }
        throw new BaseException(DvfsErrorCode.CONTROL_TYPE_NOT_SUPPORTED,"控制方式不支持");
    }
}
