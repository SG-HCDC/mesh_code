package com.gcloud.mesh.dcs.entity;

import com.gcloud.framework.db.jdbc.annotation.ID;
import com.gcloud.framework.db.jdbc.annotation.Table;

import lombok.Data;

@Table(name = "dcs_voltage_fm")
@Data
public class VoltageFmEntity {
	
	@ID
	private String id;
	private String resourceId;
	private Float ratedPower;
	private Float powerConsumption;
	private String voltageFmStrategy;
	private String supportStrategies;
	private Float frequency;
	private Integer controlType;
}
