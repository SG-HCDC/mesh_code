package com.gcloud.mesh.dcs.chain.huawei;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.alibaba.fastjson.JSONObject;
import com.gcloud.mesh.dcs.chain.FilterChain;
import com.gcloud.mesh.dcs.chain.IMigrationStepChain;
import com.gcloud.mesh.dcs.dao.SchedulerStepDao;
import com.gcloud.mesh.dcs.entity.SchedulerStepEntity;
import com.gcloud.mesh.dcs.strategy.huawei.ShowServerDetailSolution;
import com.gcloud.mesh.header.param.HuaweiMigrationParam;
import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.exception.ConnectionException;
import com.huaweicloud.sdk.core.exception.RequestTimeoutException;
import com.huaweicloud.sdk.core.exception.ServiceResponseException;
import com.huaweicloud.sdk.core.http.HttpConfig;
import com.huaweicloud.sdk.ecs.v2.EcsClient;
import com.huaweicloud.sdk.ecs.v2.model.DeleteServersRequest;
import com.huaweicloud.sdk.ecs.v2.model.DeleteServersRequestBody;
import com.huaweicloud.sdk.ecs.v2.model.DeleteServersResponse;
import com.huaweicloud.sdk.ecs.v2.model.ServerId;
import com.huaweicloud.sdk.ecs.v2.model.ShowServerResponse;
import com.huaweicloud.sdk.ecs.v2.region.EcsRegion;

import lombok.extern.slf4j.Slf4j;
import me.zhyd.oauth.log.Log;

@Component
@Slf4j
public class DeleteServers_6 extends AbstractHuaweiMigrationStep{


    @Autowired
    private SchedulerStepDao schedulerStepDao;

    @Override
    public boolean checkStatus(String stepId) {
        SchedulerStepEntity stepEntity = schedulerStepDao.getById(stepId);
        JSONObject json = JSONObject.parseObject(stepEntity.getAttach());
        String accessKeyId = json.getString("accessKey");
        String accessSecret = json.getString("secretKey");
        String vmid = json.getString("vmId");
        String regionId = json.getString("regionId");
        ShowServerDetailSolution show = new ShowServerDetailSolution();
        ShowServerResponse resp = show.showServer(accessKeyId,accessSecret,regionId, vmid);
        log.info("查询源虚拟机返回是"+resp);
        return (resp == null || resp.getServer() == null || resp.getServer().getStatus().equalsIgnoreCase("DELETED"));
    }

    @Override
    public boolean isSyncChain() {
        return false;
    }

    @Override
    public void doExec(String schedulerJobId, String attach, FilterChain filterChain, IMigrationStepChain stepChain) {
        JSONObject jsonObject = JSONObject.parseObject(attach);
        HuaweiMigrationParam huaweiMigrationParam = JSONObject.toJavaObject(jsonObject,HuaweiMigrationParam.class);
        // AppInstanceEntity appInstanceEntity = appInstanceDao.findOneByProperty("name",huaweiMigrationParam.getVmName());
        String accessKeyId = huaweiMigrationParam.getAccessKey();
        String accessSecret = huaweiMigrationParam.getSecretKey();
        String regionId = huaweiMigrationParam.getRegionId();
        String instanceId = huaweiMigrationParam.getVmId();
        deleteServers(accessKeyId, accessSecret, regionId, instanceId);
        filterChain.doExec(schedulerJobId,jsonObject.toString(),filterChain,stepChain);
    }

    @Override
    public String chainName(String attach) {
        return "删除源虚拟机实例";
    }

    @Override
    public int getOrder() {
        return 6;
    }

    public DeleteServersResponse deleteServers(String ak, String sk, String regionId, String instanceId) {
        HttpConfig config = HttpConfig.getDefaultHttpConfig();
        config.withIgnoreSSLVerification(true);
        ICredential auth = new BasicCredentials()
                .withAk(ak)
                .withSk(sk);

        EcsClient client = EcsClient.newBuilder()
                .withHttpConfig(config)
                .withCredential(auth)
                .withRegion(EcsRegion.valueOf(regionId))
                .build();
        DeleteServersRequest request = new DeleteServersRequest();
        DeleteServersRequestBody body = new DeleteServersRequestBody();
        List<ServerId> listbodyServers = new ArrayList<>();
        listbodyServers.add(
                new ServerId()
                        .withId(instanceId)
        );
        body.withServers(listbodyServers);
        body.withDeleteVolume(true);
        body.withDeletePublicip(true);
        request.setBody(body);
        DeleteServersResponse response = null;
        try {
            response = client.deleteServers(request);
        } catch (ConnectionException e) {
            Log.error("删除失败"+e);
            e.printStackTrace();
        } catch (RequestTimeoutException e) {
            Log.error("删除失败"+e);
            e.printStackTrace();
        } catch (ServiceResponseException e) {
            Log.error("删除失败"+e);
            e.printStackTrace();

        }
        return response;
    }

}
