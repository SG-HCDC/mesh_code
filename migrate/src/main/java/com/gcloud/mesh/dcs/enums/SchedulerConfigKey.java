package com.gcloud.mesh.dcs.enums;

import java.util.ArrayList;
import java.util.List;

public class SchedulerConfigKey {
	public static int sortKey(String key1, String key2) {
		// String[] keys = new String[]{"DATACENTER_COST","DATACENTER_DEVICE","DATACENTER_POWER","DATACENTER_ELECTRIC","APP"};
		List<String> keys = new ArrayList<>();
		keys.add("DATACENTER_COST");
		keys.add("DATACENTER_DEVICE");
		keys.add("DATACENTER_POWER");
		keys.add("DATACENTER_ELECTRIC");
		keys.add("APP");
		int sort = keys.indexOf(key1)-keys.indexOf(key2);
		return sort;
	} 
}
