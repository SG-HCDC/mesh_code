
package com.gcloud.mesh.dcs.entity;

import java.util.Date;

import com.gcloud.framework.db.jdbc.annotation.ID;
import com.gcloud.framework.db.jdbc.annotation.Table;

import lombok.Data;

@Table(name = "dcs_app_profiles")
@Data
public class AppProfileEntity {

    @ID
    private String id;
    private String appId;
    private Date createTime;
    private String datacenterName;
    private String featureSuggest;
    private String featureChoose;
    private String sceneSuggest;
    private String sceneChoose;

}
