package com.gcloud.mesh.dcs.chain.ali;

import com.alibaba.fastjson.JSONObject;
import com.aliyun.ecs20140526.models.DeleteInstancesRequest;
import com.gcloud.mesh.dcs.chain.FilterChain;
import com.gcloud.mesh.dcs.chain.IMigrationStepChain;
import com.gcloud.mesh.dcs.dao.AppInstanceDao;
import com.gcloud.mesh.dcs.dao.SchedulerAdviceDao;
import com.gcloud.mesh.dcs.dao.SchedulerJobDao;
import lombok.extern.slf4j.Slf4j;
import org.jeecg.common.exception.MyBusinessException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class DeleteSrcInstance_9 extends AbstractAliMigrationStep {

    @Autowired
    private AppInstanceDao appInstanceDao;

    @Autowired
    private SchedulerJobDao schedulerJobDao;

    @Autowired
    private SchedulerAdviceDao schedulerAdviceDao;

    @Override
    public void doExec(String schedulerJobId, String attach, FilterChain filterChain, IMigrationStepChain stepChain) {
        JSONObject jsonObject = JSONObject.parseObject(attach);
        String accessKeyId = jsonObject.getString("accessKeyId");
        String accessSecret = jsonObject.getString("accessSecret");
        String srcRegionId = jsonObject.getString("srcRegionId");
        String srcInstanceId = jsonObject.getString("srcInstanceId");
        String dstInstanceId = jsonObject.getString("dstInstanceId");
        String schedulerModel = jsonObject.getString("schedulerModel");
        String destDatacenterId = jsonObject.getString("dstDatacenterId");
        String destNodeId = jsonObject.getString("dstNodeId");
        this.deleteSrcInstanct(accessKeyId,accessSecret,srcRegionId,srcInstanceId);


        String jobId = schedulerJobId;
        log.info("迁移完成更新appinstance表数据："+srcInstanceId);
        appInstanceDao.updateAppIdBySrcId(srcInstanceId, dstInstanceId);
        log.info("迁移完成更新job表数据："+srcInstanceId);
        int result = schedulerJobDao.updateJobInstanceId(jobId, dstInstanceId);
        log.info("迁移完成删除建议表数据："+srcInstanceId);
        schedulerAdviceDao.deleteByInstanceId(srcInstanceId);


//        appService.updateInstanceDBInfo(schedulerJobId, srcInstanceId, dstInstanceId);
        filterChain.doExec(schedulerJobId,attach,filterChain,stepChain);
    }

    @Override
    public String chainName(String attach) {
        return "删除源虚拟机实例";
    }

    @Override
    public int getOrder() {
        return 9;
    }

    public void deleteSrcInstanct(String accessKeyId, String accessKeySecret , String regionId,String InstanceId){
        DeleteInstancesRequest deleteInstancesRequest = new DeleteInstancesRequest()
                .setRegionId(regionId)
                .setForce(true)
                .setInstanceId(java.util.Arrays.asList(
                        InstanceId
                ));
        try {
            createECSClient(accessKeyId, accessKeySecret,regionId).deleteInstances(deleteInstancesRequest);
        } catch (Exception e) {
            log.error("删除源虚拟机失败",e);
            throw new MyBusinessException("删除源虚拟机"+"::"+e.getMessage());
        }
    }

}
