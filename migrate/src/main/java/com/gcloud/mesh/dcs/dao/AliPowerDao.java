package com.gcloud.mesh.dcs.dao;


import com.gcloud.framework.db.dao.impl.JdbcBaseDaoImpl;
import com.gcloud.mesh.dcs.entity.AliPowerEntity;
import org.springframework.stereotype.Repository;


@Repository
public class AliPowerDao extends JdbcBaseDaoImpl<AliPowerEntity,String> {

    public void deleteByEsn(String esn){
        String sql="delete from dcs_ali_power_nodes where esn= ?";
        super.jdbcTemplate.update(sql,esn);
    }
}
