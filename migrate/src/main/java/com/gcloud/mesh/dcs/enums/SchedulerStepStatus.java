package com.gcloud.mesh.dcs.enums;

public enum SchedulerStepStatus {

	READY(0, "ready", "准备"), PROGRESS(1, "progress", "进行中"), FAIL(2, "fail", "失败"), SUCCESS(3, "success", "成功");

	private Integer value;
	private String name;
	private String cnName;

	SchedulerStepStatus(Integer value, String name, String cnName) {
		this.value = value;
		this.name = name;
		this.cnName = cnName;
	}

	public Integer getValue() {
		return value;
	}

	public String getName() {
		return name;
	}

	public String getCnName() {
		return cnName;
	}

}
