
package com.gcloud.mesh.dcs.dao;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Repository;

import com.gcloud.framework.db.PageResult;
import com.gcloud.framework.db.dao.impl.JdbcBaseDaoImpl;
import com.gcloud.mesh.dcs.entity.AppProfileEntity;

@Repository
public class AppProfileDao extends JdbcBaseDaoImpl<AppProfileEntity, String> {

    public AppProfileEntity getByAppId(String appId) {
        return this.findUniqueByProperty("appId", appId, AppProfileEntity.class);
    }

    public <E> PageResult<E> pageFeature(String name, String datacenterName, int pageNo, int pageSize, Class<E> clazz) {
        StringBuilder sb = new StringBuilder();
        List<Object> values = new ArrayList<>();
        sb.append("SELECT a.id AS app_id, a.name AS app_name, b.feature_suggest, b.feature_choose, a.create_time, b.datacenter_name");
        sb.append(" FROM dcs_apps a LEFT JOIN dcs_app_profiles b ON a.id=b.app_id");
        sb.append(" WHERE 1 = 1");
    	if (StringUtils.isNotBlank(name)) {
			sb.append(" AND a.name LIKE concat('%', ?, '%')");
			values.add(name);
		}
    	if (StringUtils.isNotBlank(datacenterName)) {
    		sb.append(" AND b.datacenter_name = ? ");
			values.add(datacenterName);
    	}
    	sb.append(" ORDER BY a.create_time DESC ");
        return this.findBySql(sb.toString(), values, pageNo, pageSize, clazz);
    }

    public <E> PageResult<E> pageScene(String name, String datacenterName, int pageNo, int pageSize, Class<E> clazz) {
        StringBuilder sb = new StringBuilder();
        List<Object> values = new ArrayList<>();
        sb.append("SELECT a.id AS app_id, a.name AS app_name, b.scene_suggest, b.scene_choose, a.create_time, b.datacenter_name");
        sb.append(" FROM dcs_apps a LEFT JOIN dcs_app_profiles b ON a.id=b.app_id");
        sb.append(" WHERE 1 = 1");
    	if (StringUtils.isNotBlank(name)) {
			sb.append(" AND a.name LIKE concat('%', ?, '%')");
			values.add(name);
		}
    	if (StringUtils.isNotBlank(datacenterName)) {
    		sb.append(" AND b.datacenter_name = ? ");
			values.add(datacenterName);
    	}
    	sb.append(" ORDER BY a.create_time DESC ");
        return this.findBySql(sb.toString(), values, pageNo, pageSize, clazz);
    }

    public <E> PageResult<E> pageAppProfile(String name, int pageNo, int pageSize, Class<E> clazz) {
    	StringBuilder sb = new StringBuilder();
    	List<Object> values = new ArrayList<>();
    	sb.append("SELECT a.id AS app_id, a.name AS app_name, b.scene_suggest, b.scene_choose,  b.feature_suggest, b.feature_choose, a.create_time, b.datacenter_name");
    	sb.append(" FROM dcs_apps a LEFT JOIN dcs_app_profiles b ON a.id=b.app_id");
    	sb.append(" WHERE 1 = 1");
    	if (StringUtils.isNotBlank(name)) {
			sb.append(" AND a.name LIKE concat('%', ?, '%')");
			values.add(name);
		}
    	sb.append(" ORDER BY a.create_time DESC ");
    	return this.findBySql(sb.toString(), values, pageNo, pageSize, clazz);
    }
}
