package com.gcloud.mesh.dcs.chain.ali;

import com.alibaba.fastjson.JSONObject;
import com.aliyun.smc20190601.Client;
import com.aliyun.smc20190601.models.DescribeSourceServersRequest;
import com.aliyun.smc20190601.models.DescribeSourceServersResponseBody;
import com.gcloud.mesh.dcs.chain.FilterChain;
import com.gcloud.mesh.dcs.chain.IMigrationStepChain;
import com.gcloud.mesh.header.param.AliMigrationParam;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.jeecg.common.exception.MyBusinessException;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@Slf4j
public class FindSrcJob_3 extends AbstractAliMigrationStep {
    @Override
    public void doExec(String schedulerJobId, String attach, FilterChain filterChain, IMigrationStepChain stepChain) {
        JSONObject jsonObject = JSONObject.parseObject(attach);
        AliMigrationParam aliMigrationParam = JSONObject.toJavaObject(jsonObject,AliMigrationParam.class);
        Client client = super.createSMCClient(aliMigrationParam.getAccessKeyId(),aliMigrationParam.getAccessSecret());
        String sourceId = this.queryJobId(client, aliMigrationParam.getSrcAppName());
        jsonObject.put("sourceId",sourceId);
        filterChain.doExec(schedulerJobId,jsonObject.toString(),filterChain,stepChain);
    }


    @Override
    public String chainName(String attach) {
        return "查找迁移源";
    }

    @Override
    public int getOrder() {
        return 3;
    }


    public String queryJobId(Client client, String hostname){
        String id = null;
        DescribeSourceServersRequest describeSourceServersRequest = new DescribeSourceServersRequest()
                .setPageSize(50);
        try {
            List<DescribeSourceServersResponseBody.DescribeSourceServersResponseBodySourceServersSourceServer> sourceServer = client.describeSourceServers(describeSourceServersRequest).getBody().getSourceServers().getSourceServer();
            for (DescribeSourceServersResponseBody.DescribeSourceServersResponseBodySourceServersSourceServer describeSourceServersResponseBodySourceServersSourceServer : sourceServer) {
                String systemInfo = describeSourceServersResponseBodySourceServersSourceServer.getSystemInfo();
                String state = describeSourceServersResponseBodySourceServersSourceServer.getState();
                if(!"Available".equalsIgnoreCase(state)) continue;
                JSONObject jsonObject = JSONObject.parseObject(systemInfo);
                String hn = jsonObject.getString("hostname");
                if(hn.equalsIgnoreCase(hostname)) {
                    id = describeSourceServersResponseBodySourceServersSourceServer.getSourceId();
                    break;
                }
            }
            if(StringUtils.isBlank(id)) throw new MyBusinessException("查找迁移源失败");
            log.debug("**********成功查找迁移源**********");
        } catch (Exception e) {
            log.error("查找迁移源失败",e);
            throw new MyBusinessException("查找迁移源失败"+"::"+e.getMessage());
        }
        return id;
    }
}
