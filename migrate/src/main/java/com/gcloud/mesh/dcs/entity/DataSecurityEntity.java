
package com.gcloud.mesh.dcs.entity;

import java.util.Date;

import com.gcloud.framework.db.jdbc.annotation.ID;
import com.gcloud.framework.db.jdbc.annotation.Table;

import lombok.Data;

@Table(name = "dcs_data_configs")
@Data
public class DataSecurityEntity {

    @ID
    private String id;
    private String policy;
    private String cron;
    private Boolean enabled;
    private Date updateTime;
    private String baseDir;

}
