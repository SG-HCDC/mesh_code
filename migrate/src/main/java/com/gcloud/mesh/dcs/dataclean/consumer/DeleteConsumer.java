package com.gcloud.mesh.dcs.dataclean.consumer;

import org.springframework.beans.factory.annotation.Autowired;

import com.gcloud.mesh.dcs.dataclean.handle.ConsumerHandleManager;
import com.gcloud.mesh.dcs.dataclean.handle.IHandler;
import com.gcloud.mesh.dcs.dataclean.util.DataCleanJsonUtil;
import com.gcloud.mesh.header.msg.DataCleanBaseMsg;

public class DeleteConsumer implements IConsumer {
	
	private static final String DELETE_TYPE = "Delete";
	
	@Autowired
	private ConsumerHandleManager handlerManager;

	@Override
	public Object process(Object msg) {
		// TODO Auto-generated method stub
		if(msg != null) {
			String prefix = getName(msg);
			String action = prefix + DELETE_TYPE;	
			IHandler handler = handlerManager.getHandler(action);
			if(handler != null) {
				Class clazz = handlerManager.getClass(action);
				String jsonMsg = DataCleanJsonUtil.toJson(msg);
				DataCleanBaseMsg handledMsg = (DataCleanBaseMsg)DataCleanJsonUtil.fromJson(jsonMsg, clazz);
				return handler.process(handledMsg);
			}
		}
		return null;
	}

}
