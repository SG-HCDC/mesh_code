package com.gcloud.mesh.dcs.entity;

import com.gcloud.framework.db.jdbc.annotation.ID;
import com.gcloud.framework.db.jdbc.annotation.Table;

import lombok.Data;

@Table(name = "dcs_data_clean_configs")
@Data
public class DataCleanConfigEntity {

	@ID
    private String id;
	private String source;
	private String sourceName;
	private String operateType;
    private Boolean enabled;

}
