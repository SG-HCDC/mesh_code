
package com.gcloud.mesh.dcs.entity;

import lombok.Data;

//@Table(name = "dcs_model_factors")
@Data
public class ModelFactorStepEntity {

//    @ID
//    private String id;
    private String factorId;
    private Integer left;
    private Integer right;
    private Integer multiple;

    public ModelFactorStepEntity() {

    }

    public ModelFactorStepEntity(String factorId, Integer left, Integer right, Integer multiple) {
        this.factorId = factorId;
        this.left = left;
        this.right = right;
        this.multiple = multiple;
    }

}
