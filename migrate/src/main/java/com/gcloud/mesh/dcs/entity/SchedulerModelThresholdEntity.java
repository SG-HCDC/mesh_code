
package com.gcloud.mesh.dcs.entity;

import com.gcloud.framework.db.jdbc.annotation.ID;
import com.gcloud.framework.db.jdbc.annotation.Table;

import lombok.Data;

@Table(name = "dcs_scheduler_model_thresholds")
@Data
public class SchedulerModelThresholdEntity {

    @ID
    private String id;
    private String schedulerModelTypeId;
    private String key;
    private String value;
    private Integer type;

}
