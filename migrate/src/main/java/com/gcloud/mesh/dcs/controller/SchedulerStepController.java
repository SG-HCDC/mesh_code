
package com.gcloud.mesh.dcs.controller;

import com.gcloud.mesh.dcs.annotation.ParamSecurity;
import com.gcloud.mesh.dcs.chain.FilterChain;
import com.gcloud.mesh.dcs.service.SchedulerStepService;
import com.gcloud.mesh.header.msg.BaseReplyMsg;
import com.gcloud.mesh.header.msg.dcs.ListSchedulerStepMsg;
import com.gcloud.mesh.header.vo.dcs.SchedulerStepVo;
import org.apache.commons.lang.StringUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.jeecg.common.aspect.annotation.AutoLog;
import org.jeecg.common.constant.CommonConstant;
import org.jeecg.common.constant.enums.SysLogOperateType;
import org.jeecg.common.exception.MyBusinessException;
import org.jeecg.common.system.api.ISysBaseAPI;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/step")
public class SchedulerStepController {

	@Autowired
	private SchedulerStepService service;
	
	//处理Mass Assignment: Insecure Binder Configuration
	@Autowired
	private ISysBaseAPI sysBaseAPI;

	@Autowired
	private FilterChain filterChain;

	@InitBinder
	public void populateCustomerRequest(WebDataBinder binder) {
	    binder.setDisallowedFields(new String[]{});
	}

	// @AutoLog(value = "list",operateType = CommonConstant.OPERATE_TYPE_LIST)
	@RequestMapping(value = "list1", method = RequestMethod.POST)
	@RequiresPermissions(value = {"stepList"})
	@ParamSecurity(enabled = true)
	public List<SchedulerStepVo> list(@Valid ListSchedulerStepMsg msg) throws Exception {
		sysBaseAPI.addLog("查看调度过程监控信息展示，调度ID："+msg.getSchedulerJobId(), null, SysLogOperateType.DETAIL, null ,null , "多数据中心调度-动态调度机制-调度过程监控");
		return this.service.list(msg);
	}

	@RequestMapping(value = "stepRestart", method = RequestMethod.POST)
	@RequiresPermissions(value = {"stepRestart"})
	@ParamSecurity(enabled = true)
	public BaseReplyMsg stepRestart(String schedulerJobStepId) throws Exception {
		sysBaseAPI.addLog("重新执行调度步骤，调度ID："+schedulerJobStepId, null, SysLogOperateType.DETAIL, null ,null , "多数据中心调度-动态调度机制-调度过程监控");
		if(StringUtils.isBlank(schedulerJobStepId)) throw new MyBusinessException("执行步骤ID不能为空");
		filterChain.restart(schedulerJobStepId);
		return new BaseReplyMsg();
	}

}
