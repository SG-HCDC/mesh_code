package com.gcloud.mesh.utils;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;

import lombok.extern.slf4j.Slf4j;
@Slf4j
public class DnsUtil {
	   //zhangdp替换文件
	   public static void addAndRunDnsFile(String server, String domain, String ip) {
		String path = "/mesh-controller/config/my.expect";
		File file = new File(path);
		try {
			BufferedWriter bw = new BufferedWriter(new FileWriter(file));
			StringBuffer buff = new StringBuffer();
			buff.append("#!/usr/bin/expect\n");
			buff.append("set dns_server ").append(server).append("\n");
			buff.append("spawn nsupdate \n");
			buff.append("send \"server ${dns_server}\\r\"\n");
			buff.append("send \"update delete ").append(domain).append(" A\\r\"\n");
			buff.append("send \"update add ").append(domain).append(" 6000 A ").append(ip).append("\\r\"\n");
			buff.append("send \"send\\r\"\n");
			buff.append("send \"quit\\r\"\n");
			buff.append("expect eof\n");
			bw.write(buff.toString());
			bw.close();
		} catch (Exception e) {
			log.error("更新dns失败{}",path,e);
		}
		String[] execCmds = { "expect", path };
		CmdExecuteUtil.runAndGetValue(execCmds);
	   
	}

}
