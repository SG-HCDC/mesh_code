package com.gcloud.mesh.utils;

import java.util.ArrayList;
import java.util.List;

import com.gcloud.framework.db.PageResult;

/**
 * 程序分页工具类
 * @author lcarr
 *
 */
public class PageUtil {
	
	public static <E> PageResult<E> page(int pageNum, int pageSize, List<E> list) {
		List<E> finalList = new ArrayList<E>();
	    // 所有dataList数据中的第几条
		if(list != null) {
			int currIdx = pageNum > 1 ? (pageNum -1) * pageSize : 0;
		    for (int i = 0; i < pageSize && i < list.size() - currIdx; i++) {
		    	finalList.add(list.get(currIdx + i));
		    }
		}
		PageResult<E> res = new PageResult<E>();
		res.setList(finalList);
		res.setPageNo(pageNum);
		res.setPageSize(pageSize);
		res.setTotalCount(list != null ? list.size(): 0);
		return res;
	}
	


}
