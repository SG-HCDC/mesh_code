package com.gcloud.mesh.utils;

import java.util.HashMap;
import java.util.Map;

import com.alibaba.fastjson.JSONObject;
import com.gcloud.mesh.header.vo.supplier.AliPowerConsumptionVo;
import com.gcloud.mesh.header.vo.supplier.K8sClusterVo;
import com.gcloud.mesh.header.vo.supplier.XbrotherSupervisionSystemVo;
import com.gcloud.mesh.header.vo.supplier.YingZeSupervisionSystemVo;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class SupplierSystemTypeUtil {

	public static final Map<String, Class> maps = new HashMap<String, Class>() {
		{
			put("k8s", K8sClusterVo.class);
			put("supervision_system_xbrother", XbrotherSupervisionSystemVo.class);
			put("supervision_system_yingze", YingZeSupervisionSystemVo.class);
			put("ali_powerconsumption", AliPowerConsumptionVo.class);
		}
	};
	
	public static Object getByName(String name, String content) {
		if (!maps.keySet().contains(name)) {
			return null;
		}
		Class clazz = maps.get(name);
		Object obj = null;
		try {
			obj = JSONObject.parseObject(content, clazz);
		} catch(Exception e) {
			log.error("{} object convert error  {}", content, e.getMessage());
		}
		return obj;
	}
	
}
