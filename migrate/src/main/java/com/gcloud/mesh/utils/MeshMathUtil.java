package com.gcloud.mesh.utils;

import java.math.BigDecimal;
import java.text.DecimalFormat;

public class MeshMathUtil {

	/**
	 * 保留小数位数
	 * @param n
	 * @param f
	 * @return
	 */
	public static double setScale(int n, double f) {
		 BigDecimal b = new BigDecimal(f);  
		 double f1 = b.setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue(); 
		 return f1;
	}
	
	public static float setScale(int n, float f) {
		 BigDecimal b = new BigDecimal(f);  
		 float f1 = b.setScale(2, BigDecimal.ROUND_HALF_UP).floatValue(); 
		 return f1;
	}
	
	public static String setScale(double f, String format) {
		DecimalFormat df = new DecimalFormat(format);
		return df.format(f);
	}
}
