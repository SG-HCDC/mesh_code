package com.gcloud.mesh;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.ComponentScan.Filter;
import org.springframework.context.annotation.FilterType;
import org.springframework.scheduling.annotation.EnableScheduling;

import java.net.UnknownHostException;

@Slf4j
@EnableCaching
@EnableScheduling
@EnableDiscoveryClient
@EnableFeignClients(basePackages = { "com.gcloud.mesh.sdk" })
@SpringBootApplication
@ComponentScan(basePackages = { "com.gcloud.mesh", "org.jeecg" }, excludeFilters = {@Filter(type = FilterType.REGEX, pattern = "com.gcloud.mesh.framework.core.mq.*")})
public class MigrateApplication {
	public static void main(String[] args) throws UnknownHostException {
		SpringApplication.run(MigrateApplication.class, args);
	}
}
