package com.gcloud.mesh.monitor.cloud;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.jeecg.common.constant.enums.SysLogOperateType;
import org.jeecg.common.constant.enums.SysLogType;
import org.jeecg.common.system.api.ISysBaseAPI;
import org.jeecg.common.util.DateUtils;
import org.jeecg.modules.system.entity.SysLog;
import org.jeecg.modules.system.mapper.SysLogMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.gcloud.mesh.redis.MockRedis;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
public class RunLogTimer {

	@Autowired
	private ISysBaseAPI sysBaseAPI;

	@Autowired
	private SysLogMapper sysLogMapper;
	
	@Autowired
	private MockRedis<Long> redis;
	
	@Scheduled(initialDelay=1000 * 60, fixedDelay = 1000 * 60 * 60)
	public void work() {
		String runLogId = sysBaseAPI.addLog("监控服务", SysLogType.RUN, SysLogOperateType.RUN);
		
		Date now = new Date(redis.getBeginTime());
		
		SysLog sysLog = sysLogMapper.selectById(runLogId);
		if (sysLog != null) {
			sysLog.setRemark(DateUtils.date2Str(now, new SimpleDateFormat("yyyy-MM-dd HH:mm:ss")));
			sysLogMapper.updateById(sysLog);
		}
	}
}
