package com.gcloud.mesh.redis;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.Date;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.system.ApplicationHome;
import org.springframework.stereotype.Component;
import org.springframework.util.ResourceUtils;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.gcloud.mesh.asset.entity.CloudResourceEntity;
import com.gcloud.mesh.asset.enums.MockType;
import com.gcloud.mesh.dcs.entity.AppEntity;
import com.gcloud.mesh.header.enums.ResourceSourceType;
import com.gcloud.mesh.header.vo.asset.MockCloudResourceVo;
import com.gcloud.mesh.header.vo.asset.MockIaasVo;
import com.gcloud.mesh.header.vo.asset.MockNodeVo;
import com.gcloud.mesh.header.vo.asset.MockSwitcherVo;
import com.gcloud.mesh.header.vo.dcs.MockAppVo;
import com.gcloud.mesh.utils.FileUtil;

import lombok.extern.slf4j.Slf4j;

@Component
@Slf4j
public class MockRedisData {
	
	private static final String APP = "app";
	
	private static final String CLOUD = "cloud";
	
	private static final String IT_DEVICE = "it_device";
	
	private static final String DEVICE = "iaas";
	
	private static final String SERVER = "server";
	
	private static final String SWITCHER = "switcher";
	
	@Autowired
	private MockRedis<MockIaasVo> mockIaasRedis;

	@Autowired
	private MockRedis<MockNodeVo> mockNodeRedis;

	@Autowired
	private MockRedis<MockSwitcherVo> mockSwitcherRedis;
	
	@Autowired
	private MockRedis<MockCloudResourceVo> mockCloudRedis;
	
	@Autowired
	private MockRedis<MockAppVo> mockAppRedis;
	
	@PostConstruct
	public void init() {	
		String jsonStr = null;
		log.info("[MockRedisData][init] 读取json文件");
		try {
			ApplicationHome applicationHome = new ApplicationHome(this.getClass());
			File jsonFile = new File(applicationHome.getDir(), "mock-data.json");
			jsonStr = FileUtil.readFile(jsonFile.getAbsolutePath());
		}catch(Exception e) {
			log.error("[MockRedisData][init] 读取文件失败");
		}
		if(jsonStr != null) {
			JSONObject jsonObj = null;
			try {
				jsonObj = JSON.parseObject(jsonStr);
			}catch(Exception e) {
				log.error("[MockRedisData][init] json 文件解析失败");
			}	
			if( jsonObj != null) {
				try {
					initIaasSync(jsonObj.getJSONArray(DEVICE));
				}catch(Exception e) {
					log.error("[MockRedisData][init]device 解析失败");
				}
				try {
					initItDeviceSync(jsonObj.getJSONObject(IT_DEVICE));
				}catch(Exception e) {
					log.error("[MockRedisData][init] it_device 解析失败");
				}
				try {
					initCloudSync(jsonObj.getJSONArray(CLOUD));
				}catch(Exception e) {
					log.error("[MockRedisData][init] cloud 解析失败");
				}
				try {
					initAppSync(jsonObj.getJSONArray(APP));
				}catch(Exception e) {
					log.error("[MockRedisData][init] app 解析失败");
				}
			}
		}

	}
	
	private void initIaasSync(JSONArray jsonArr) {
		for(int i=0 ; i<jsonArr.size(); i++) {
			String jsonStr = jsonArr.getJSONObject(i).toJSONString();
			MockIaasVo vo = JSON.parseObject(jsonStr, MockIaasVo.class);
			vo.setFrom(ResourceSourceType.SYNC.getName());
//			vo.setCreateTime(new Date());
			mockIaasRedis.set(vo.getId(), MockType.IAAS, vo);
		}
	}
	
	private void initItDeviceSync(JSONObject jsonObj) {
		
		initNodeSync(jsonObj.getJSONArray(SERVER));
		initSwitcherSync(jsonObj.getJSONArray(SWITCHER));
		
	}
	
	private void initNodeSync(JSONArray jsonArr) {
		for(int i=0 ; i<jsonArr.size(); i++) {
			String jsonStr = jsonArr.getJSONObject(i).toJSONString();
			MockNodeVo vo = JSON.parseObject(jsonStr, MockNodeVo.class);
			vo.setFrom(ResourceSourceType.SYNC.getName());
//			vo.setCreateTime(new Date());
			mockNodeRedis.set("node_" + vo.getId(), MockType.IT_DEVICE, vo);
		}
	}
	
	private void initSwitcherSync(JSONArray jsonArr) {
		for(int i=0 ; i<jsonArr.size(); i++) {
			String jsonStr = jsonArr.getJSONObject(i).toJSONString();
			MockSwitcherVo vo = JSON.parseObject(jsonStr, MockSwitcherVo.class);
			vo.setFrom(ResourceSourceType.SYNC.getName());
//			vo.setCreateTime(new Date());
			mockSwitcherRedis.set("switcher_" + vo.getId(), MockType.IT_DEVICE, vo);
		}
	}
	
	private void initCloudSync(JSONArray jsonArr) {
		for(int i=0 ; i<jsonArr.size(); i++) {
			String jsonStr = jsonArr.getJSONObject(i).toJSONString();
			MockCloudResourceVo vo = JSON.parseObject(jsonStr, MockCloudResourceVo.class);
			vo.setFrom(ResourceSourceType.SYNC.getName());
//			vo.setCreateTime(new Date());
			mockCloudRedis.set(vo.getId(), MockType.CLOUD_RESOURCE, vo);
		}
	}
	
	private void initAppSync(JSONArray jsonArr) {
		for(int i=0 ; i<jsonArr.size(); i++) {
			String jsonStr = jsonArr.getJSONObject(i).toJSONString();
			MockAppVo vo = JSON.parseObject(jsonStr, MockAppVo.class);
			vo.setFrom(ResourceSourceType.SYNC.getName());
//			vo.setCreateTime(new Date());
			mockAppRedis.set(vo.getId(), MockType.APP, vo);
		}
	}

}
