package com.gcloud.mesh.config;

import java.util.List;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import com.gcloud.mesh.header.vo.dcs.DataSourceItemVo;

import lombok.Data;

@ConfigurationProperties(prefix = "mesh.data-classification")
@Data
@Component
public class DataClassificationConfig {

	private String host;
	private int port;
	private String user;
	private String password;
	private String mima;
	private List<DataSourceItemVo> source;

}
