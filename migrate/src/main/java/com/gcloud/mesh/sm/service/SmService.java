
package com.gcloud.mesh.sm.service;

import javax.annotation.PostConstruct;

import org.jeecg.common.util.RedisUtil;
import org.jeecg.common.util.sm.SmManager;
import org.jeecg.common.util.sm.UserKeyInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.gcloud.mesh.header.exception.BaseException;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class SmService {
	
	@Autowired
    private RedisUtil redisUtil;

	@PostConstruct
	public void init() {
		initSm();
	}
	
    public String encryptSm2(String data, String userId) throws Exception {
    	String cipher = null;
    	UserKeyInfo userKey = (UserKeyInfo)redisUtil.get(SmManager.SM + userId);
    	if(userKey != null) {
        	cipher = SmManager.encryptSm2(userKey, data);
    	}
    	
        return cipher;
    }
    
    public String decryptSm2(String cipher, String userId) throws Exception {
    	String data = null;
    	UserKeyInfo userKey = (UserKeyInfo)redisUtil.get(SmManager.SM + userId);
    	if(userKey != null) {
        	data = SmManager.decryptSm2(userKey, cipher);
    	}
    	
    	return data;
    }
    
//    public String encryptClientSm4(String data, String userId) throws Exception {
//    	String cipher = null;
//    	UserKeyInfo userKey = (UserKeyInfo)redisUtil.get(SmManager.SM + userId);
//    	if(userKey != null) {
//        	cipher = SmManager.encryptClientSm4(userKey, data);
//    	}
//        return cipher;
//    }
    
//    public String decryptClientSm4(String cipher, String userId) throws Exception {
//    	String data = null;
//    	UserKeyInfo userKey = (UserKeyInfo)redisUtil.get(SmManager.SM + userId);
//    	if(userKey != null) {
//    		data = SmManager.decryptClientSm4(userKey, cipher);
//    	}
//        return data;
//    }
    
    public String encryptClientSm4(String key, String data, String userId) throws Exception {
    	String cipher = null;
    	if(key != null) {
        	cipher = SmManager.encryptClientSm4(key, data);
    	}
        return cipher;
    }
    
    public String decryptClientSm4(String key, String cipher, String userId) throws Exception {
    	String data = null;
    	if(key != null) {
    		data = SmManager.decryptClientSm4(key, cipher);
    	}
        return data;
    }
    
    public String encryptServerSm4(String data, String userId) throws Exception {
    	String cipher = SmManager.encryptServerSm4(data);
    	if(cipher == null) {
    		throw new BaseException();
    	}
        return cipher;
    }
    
    public String decryptServerSm4(String cipher, String userId) throws Exception {
    	String data = SmManager.decryptServerSm4(cipher);
    	if(data == null) {
    		throw new BaseException();
    	}
        return data;
    }
    
    public String encryptSm3(String data, String userId) throws Exception {
    	String digest = SmManager.encryptSm3(data);
    	if(digest == null) {
    		log.error("[SmService][encryptSm3] SmManager encryptSm3 error");
    	}
        return digest;
    }
    
    public boolean verifySm3(String data, String key) throws Exception {
    	boolean res = false;
    	try {
    		res = SmManager.verifySm3(data, key);
    	}catch(Exception e) {
    		log.error("[SmService][verifySm3] SmManager verifySm3 error: {}", e.getMessage());
    	}
    	return res;
    }
    
    public void saveSm4KeyPairByUser(String cipher, String userId) throws Exception {
    	UserKeyInfo userKey = (UserKeyInfo)redisUtil.get(SmManager.SM + userId);
    	if(userKey == null) {
    		throw new BaseException();
    	}
    	String data = SmManager.decryptSm2(userKey, cipher);
    	if(data == null) {
    		throw new BaseException();
    	}
    	userKey.setSm4Key(data);
    	redisUtil.set(SmManager.SM + userId, userKey);
    }
    
    public String getSm2PublicKeyByUser(String userId) throws Exception {
    	UserKeyInfo userKey = (UserKeyInfo)redisUtil.get(SmManager.SM + userId);
    	if(userKey == null) {
    		throw new BaseException();
    	}
    	return userKey.getSm2PublicKey();
    }
    
    private void clearKeyByUser() {
    	//系统启动时清空所有的国密缓存
    	log.info("sm redis init");
    	try {
    	    redisUtil.del(SmManager.SM + "*");
    	}catch (Exception e) {
    		log.error("[SmService][clearKeyByUser] sm redis init error");
    	}
    	
    } 
    
    private void initSm() {
    	clearKeyByUser();
    }
    
}
