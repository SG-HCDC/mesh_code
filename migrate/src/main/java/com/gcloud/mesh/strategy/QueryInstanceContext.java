package com.gcloud.mesh.dcs.strategy;

import java.util.List;

import org.springframework.stereotype.Component;

import com.gcloud.mesh.dcs.entity.AppEntity;
import com.gcloud.mesh.framework.core.SpringUtil;

@Component
public class QueryInstanceContext {

    public List<AppEntity> queryServerList(String resourceTypeName,String cloudResourceId){
    	QueryInstance instance = (QueryInstance) SpringUtil.getBean(resourceTypeName+"QueryInstance");
        return instance.queryServerList(cloudResourceId);
    };


}
