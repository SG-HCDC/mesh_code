package com.gcloud.mesh.dcs.strategy.huawei;



import com.huaweicloud.sdk.core.auth.ICredential;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;

import com.gcloud.mesh.dcs.annotation.SchedulerLog;
import com.huaweicloud.sdk.core.auth.GlobalCredentials;
import com.huaweicloud.sdk.core.exception.ConnectionException;
import com.huaweicloud.sdk.core.exception.RequestTimeoutException;
import com.huaweicloud.sdk.core.exception.ServiceResponseException;
import com.huaweicloud.sdk.core.http.HttpConfig;
import com.huaweicloud.sdk.sms.v3.*;
import com.huaweicloud.sdk.sms.v3.region.SmsRegion;

import lombok.extern.slf4j.Slf4j;

import com.huaweicloud.sdk.sms.v3.model.*;

@Slf4j
@Component
public class UpdateTaskStatusSolution {

    public static void main(String[] args) {
    	UpdateTaskStatusSolution updateSolution = new UpdateTaskStatusSolution();
    	updateSolution.updateTaskStatus(null, null, null, null, null);
    }
    @SchedulerLog(stepName="启动迁移任务")
    public UpdateTaskStatusResponse updateTaskStatus(String jobId, String ak, String sk, String taskId, String action) {
    	return updateTask(ak,sk, taskId, action);
    }
    public UpdateTaskStatusResponse updateTaskTest(String jobId, String ak, String sk, String taskId, String action) {
    	return updateTask(ak,sk, taskId, action);
    }
    private UpdateTaskStatusResponse updateTask(String ak, String sk, String taskId, String action) {
    	if(StringUtils.isBlank(ak)) {
   		 ak = "8QVISVT4XJHUNMIEPZBQ";
   	}
   	if(StringUtils.isBlank(sk)) {
   		sk = "GingsSlHInSkQ3x47jMSEjFT0uumSJgplZs926Cw";
   	}
   	if(StringUtils.isBlank(taskId)) {
   		   taskId = "ff3b0bd1-c37d-40d0-8bae-b4302f398222";
   	}
       UpdateTaskStatusResponse response = null;
 	    HttpConfig config = HttpConfig.getDefaultHttpConfig();
 	    config.withIgnoreSSLVerification(true);
       ICredential auth = new GlobalCredentials()
               .withAk(ak)
               .withSk(sk);

       SmsClient client = SmsClient.newBuilder()
               .withCredential(auth)
               .withHttpConfig(config)
               .withRegion(SmsRegion.valueOf("ap-southeast-1"))
               .build();
       UpdateTaskStatusRequest request = new UpdateTaskStatusRequest();
       request.withTaskId(taskId);
       UpdateTaskStatusReq body = new UpdateTaskStatusReq();
       body.withOperation(UpdateTaskStatusReq.OperationEnum.fromValue(action));
       request.withBody(body);
       try {
           response = client.updateTaskStatus(request);
//           System.out.println(response.toString());
       } catch (ConnectionException e) {
       	log.error("迁移开始任务连接失败："+ e);
//           e.printStackTrace();
       } catch (RequestTimeoutException e) {
       	log.error("迁移开始任务超时失败："+ e);
//           e.printStackTrace();
       } catch (ServiceResponseException e) {
       	log.error("迁移开始任务失败："+ e);
//           e.printStackTrace();
//           System.out.println(e.getHttpStatusCode());
//           System.out.println(e.getErrorCode());
//           System.out.println(e.getErrorMsg());
       }
       return response;
    }
}