package com.gcloud.mesh.dcs.strategy.huawei;



import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.exception.ConnectionException;
import com.huaweicloud.sdk.core.exception.RequestTimeoutException;
import com.huaweicloud.sdk.core.exception.ServiceResponseException;
import com.huaweicloud.sdk.core.http.HttpConfig;
import com.huaweicloud.sdk.ces.v1.*;
import com.huaweicloud.sdk.ces.v1.region.CesRegion;
import com.huaweicloud.sdk.ces.v1.model.*;

import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
@Component
public class BatchListMetricDataSolution {

    public static void main(String[] args) {
    	BatchListMetricDataSolution list = new BatchListMetricDataSolution();
    	long from = System.currentTimeMillis()-60000;//60秒钟检查一次
    	long to = System.currentTimeMillis();
    	list.listMetricData(null,null,null,null, from, to);
    }
    public BatchListMetricDataResponse listMetricData(String vmId, String regionId, String ak, String sk, long from, long to) {
      	if(StringUtils.isBlank(ak)) {
    		 ak = "8QVISVT4XJHUNMIEPZBQ";
 	   	}
 	   	if(StringUtils.isBlank(sk)) {
 	   		sk = "GingsSlHInSkQ3x47jMSEjFT0uumSJgplZs926Cw";
 	   	}
 	   	if(StringUtils.isBlank(vmId)) {
 	   	 vmId = "105901c5-1d08-479a-9270-685a50d38c01";
 	   	 regionId = "cn-south-1";
 	   	}
  	    HttpConfig config = HttpConfig.getDefaultHttpConfig();
  	    config.withIgnoreSSLVerification(true);
        ICredential auth = new BasicCredentials()
                .withAk(ak)
                .withSk(sk);

        CesClient client = CesClient.newBuilder()
                .withCredential(auth)
                .withHttpConfig(config)
                .withRegion(CesRegion.valueOf(regionId))
                .build();
        BatchListMetricDataRequest request = new BatchListMetricDataRequest();
        BatchListMetricDataRequestBody body = new BatchListMetricDataRequestBody();
        List<MetricsDimension> listMetricsDimensions = new ArrayList<>();
        listMetricsDimensions.add(
            new MetricsDimension()
                .withName("instance_id")
                .withValue(vmId)
        );
//        List<MetricsDimension> listMetricsDimension = new ArrayList<>();
//        listMetricsDimensions.add(
//            new MetricsDimension()
//                .withName("instance_id")
//                .withValue("5f94d2e4-2116-4087-abc0-cf883a4c7f6f")
//        );
        
        List<MetricInfo> listbodyMetrics = new ArrayList<>();
        listbodyMetrics.add(
            new MetricInfo()
                .withNamespace("AGT.ECS")
                .withMetricName("cpu_usage")
                .withDimensions(listMetricsDimensions)
        );
        listbodyMetrics.add(
            new MetricInfo()
                .withNamespace("AGT.ECS")
                .withMetricName("mem_usedPercent")
                .withDimensions(listMetricsDimensions)
        );
        body.withTo(to);
        body.withFrom(from);
        body.withFilter("average");
        body.withPeriod("1200");
        body.withMetrics(listbodyMetrics);
        request.withBody(body);
        BatchListMetricDataResponse response = null;
        try {
            response = client.batchListMetricData(request);
        } catch (ConnectionException e) {
            // e.printStackTrace();
        } catch (RequestTimeoutException e) {
            // e.printStackTrace();
        } catch (ServiceResponseException e) {
           // e.printStackTrace();

        }
        return response;
    }
}