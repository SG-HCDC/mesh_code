package com.gcloud.mesh.dcs.strategy;

import com.gcloud.mesh.dcs.entity.AppEntity;
import com.gcloud.mesh.header.vo.dcs.DescribeMetricVO;
import com.google.common.collect.Lists;

import java.io.Serializable;
import java.util.List;

/**
 * 服务迁移
 */
public interface ServerMigrationStrategy {
    Object doMigration(Serializable obj);
    String getStrategyName();
    default Boolean checkStatus(Integer stepNo,String attach){
        return true;
    };
    default List<AppEntity> queryServerList(String datacenterId){return Lists.newArrayList();};
    default List<DescribeMetricVO> queryDescribeMetricLastList(String datacenterId){return Lists.newArrayList();};
}
