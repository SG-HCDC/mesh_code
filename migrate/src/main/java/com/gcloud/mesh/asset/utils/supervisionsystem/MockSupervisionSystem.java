package com.gcloud.mesh.asset.utils.supervisionsystem;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Component;

import com.gcloud.mesh.asset.dao.IaasDao;
import com.gcloud.mesh.asset.dao.NodeDao;
import com.gcloud.mesh.asset.dao.SwitcherDao;
import com.gcloud.mesh.asset.entity.IaasEntity;
import com.gcloud.mesh.asset.entity.NodeEntity;
import com.gcloud.mesh.asset.entity.SwitcherEntity;
import com.gcloud.mesh.asset.enums.DeviceType;
import com.gcloud.mesh.asset.enums.MockType;
import com.gcloud.mesh.header.enums.ResourceSourceType;
import com.gcloud.mesh.header.vo.asset.MockIaasVo;
import com.gcloud.mesh.header.vo.asset.MockNodeVo;
import com.gcloud.mesh.header.vo.asset.MockSwitcherVo;
import com.gcloud.mesh.redis.MockRedis;
import com.gcloud.mesh.sm.service.SmService;
import com.gcloud.mesh.utils.TimestampUtil;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
@ConditionalOnProperty(name = "asset.supervision_system", havingValue = "mock")
public class MockSupervisionSystem implements ISupervisionSystem {
	
	@Autowired
	private IaasDao iaasDao;

	@Autowired
	private NodeDao nodeDao;

	@Autowired
	private SwitcherDao switcherDao;
	
	@Autowired
	private MockRedis<MockIaasVo> mockIaasRedis;

	@Autowired
	private MockRedis<MockNodeVo> mockNodeRedis;

	@Autowired
	private MockRedis<MockSwitcherVo> mockSwitcherRedis;

	@Autowired
	private SmService smService;

	@Override
	public <E> List<E> sync(Class clazz) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void syncDevice(int type) {
		// TODO Auto-generated method stub
		
		List<MockIaasVo> res = mockIaasRedis.getByType(MockType.IAAS);
		List<String> exists = iaasDao.findAll().stream().filter(s -> {
			DeviceType deviceType = DeviceType.getDeviceTypeByNo(s.getType());
			if (DeviceType.AIR_CONDITION.equals(deviceType)
					&& DeviceType.DISTRIBUTION_BOX.equals(deviceType)
					&& DeviceType.UPS.equals(deviceType)) {
				return true;
			}
			return false;
		}).map(s -> s.getId()).collect(Collectors.toList());
		if (res != null) {
			for (MockIaasVo r : res) {
				if (!exists.contains(r.getId()) && r.getType() == type) {
					IaasEntity iaas = new IaasEntity();
					BeanUtils.copyProperties(r, iaas);
					iaas.setFrom(ResourceSourceType.SYNC.getName());
					iaas.setCreateTime(TimestampUtil.strToDate(r.getCreateTime(), "yyyy/MM/dd HH:mm:ss"));
//					iaas.setVisible(authorityMap.get(r.getType()));
					try {
						iaasDao.save(iaas);
					} catch (Exception e) {
						log.error("数据库保存失败：{}", e.getMessage());
					}
				}
			}
		}
	}

	@Override
	public void syncNodeAndSwitcher(int type) {
		// TODO Auto-generated method stub
		String nodeKey = String.format("mesh_controller_mock_type_%d_id_node_*", MockType.IT_DEVICE.getType());
		String switcherKey = String.format("mesh_controller_mock_type_%d_id_switcher_*", MockType.IT_DEVICE.getType());
		List<MockNodeVo> redisNodes = mockNodeRedis.getByType(nodeKey);
		List<MockSwitcherVo> redisSwitchers = mockSwitcherRedis.getByType(switcherKey);
		List<String> nodeExists = iaasDao.findAll().stream()
				.filter(s -> DeviceType.SERVER.equals(DeviceType.getDeviceTypeByNo(s.getType()))).map(s -> s.getId())
				.collect(Collectors.toList());
		List<String> switcherExists = iaasDao.findAll().stream()
				.filter(s -> DeviceType.SWITCHER.equals(DeviceType.getDeviceTypeByNo(s.getType()))).map(s -> s.getId())
				.collect(Collectors.toList());
		if (redisNodes != null && DeviceType.SERVER.equals(DeviceType.getDeviceTypeByNo(type))) {
			for (MockNodeVo r : redisNodes) {
				if (!nodeExists.contains(r.getId())) {
					IaasEntity iaas = new IaasEntity();
					NodeEntity node = new NodeEntity();
					BeanUtils.copyProperties(r, iaas);
					BeanUtils.copyProperties(r, node);
					node.setId(iaas.getDeviceId());
					iaas.setFrom(ResourceSourceType.SYNC.getName());
					iaas.setCreateTime(TimestampUtil.strToDate(r.getCreateTime(), "yyyy/MM/dd HH:mm:ss"));
					try {
						iaasDao.save(iaas);
						nodeDao.save(node);
					} catch (Exception e) {
						log.error("数据库保存失败：{}", e.getMessage());
					}
				}
			}
		}
		if (redisSwitchers != null && DeviceType.SWITCHER.equals(DeviceType.getDeviceTypeByNo(type))) {
			for (MockSwitcherVo r : redisSwitchers) {
				if (!switcherExists.contains(r.getId())) {
					String typeId = DeviceType.getDeviceTypeByNo(r.getType()).getName();
					if (typeId == null) {
						continue;
					}
					IaasEntity iaas = new IaasEntity();
					SwitcherEntity switcher = new SwitcherEntity();
					BeanUtils.copyProperties(r, iaas);
					BeanUtils.copyProperties(r, switcher);
					switcher.setId(iaas.getDeviceId());
					iaas.setCreateTime(TimestampUtil.strToDate(r.getCreateTime(), "yyyy/MM/dd HH:mm:ss"));
					iaas.setFrom(ResourceSourceType.SYNC.getName());
					try {
						iaasDao.save(iaas);
						switcherDao.save(switcher);
					} catch (Exception e) {
						log.error("数据库保存失败：{}", e.getMessage());
					}

				}
			}
		}
	}

}
