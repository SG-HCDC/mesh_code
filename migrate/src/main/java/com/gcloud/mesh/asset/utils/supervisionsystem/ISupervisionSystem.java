package com.gcloud.mesh.asset.utils.supervisionsystem;

import java.util.List;

public interface ISupervisionSystem {

	public <E> List<E> sync(Class clazz);
	
	public void syncDevice(int type);
	
	public void syncNodeAndSwitcher(int type);
}
