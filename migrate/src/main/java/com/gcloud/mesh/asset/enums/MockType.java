package com.gcloud.mesh.asset.enums;

import java.util.stream.Stream;

public enum MockType {

	CLOUD_RESOURCE(0, "云平台资源"),
	APP(1, "应用"),
	IAAS(2, "基础设施"),
	IT_DEVICE(3, "IT设备");

	private Integer type;
	private String name;

	MockType(Integer type, String name) {
		this.type = type;
		this.name = name;
	}

	public static MockType getMockTypeByNo(Integer type) {
		return Stream.of(MockType.values()).filter(s -> s.getType() == type).findFirst().orElse(null);
	}

	public Integer getType() {
		return type;
	}

	public String getName() {
		return name;
	}

}
