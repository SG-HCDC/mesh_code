package com.gcloud.mesh.asset.sync;

public interface ISyncL1System {

	public void sync();
	
	public void syncDevice(int type);
}
