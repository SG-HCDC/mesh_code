package com.gcloud.mesh.asset.entity;

import java.util.Date;

import com.gcloud.framework.db.jdbc.annotation.ID;
import com.gcloud.framework.db.jdbc.annotation.Table;

import lombok.Data;

@Table( name = "asset_iaas")
@Data
public class IaasEntity {
	
	@ID
	private String id;
	private String name;
	private String esn;
	private Integer type;
	private String deviceId;
	private String deviceModel;
	private String deviceManufacturer;
	private String datacenterId;
	private String creator;
	private Date createTime;
	private String from;
	private Boolean visible;

}
