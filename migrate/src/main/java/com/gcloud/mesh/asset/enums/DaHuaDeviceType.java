package com.gcloud.mesh.asset.enums;

import java.util.stream.Stream;

public enum DaHuaDeviceType {
	
	//GENERAL(0, "general", "内存设备", DeviceType.GENERAL),
	AIR_CONDITION(1, "air_condition", "TH01数字式温湿度传感器", DeviceType.AIR_CONDITION);
	//DISTRIBUTION_BOX(2, "general", "开关量采集", DeviceType.GENERAL),
	//UPS(3, "ups", "UPS", DeviceType.UPS);

	private int no;
	private String name;
	private String cnName;
	private DeviceType deviceType;

	DaHuaDeviceType(int no, String name, String cnName, DeviceType deviceType) {
		this.no = no;
		this.name = name;
		this.cnName = cnName;
		this.deviceType = deviceType;
	}

	public static DaHuaDeviceType getDeviceTypeByNo(int no) {
		return Stream.of(DaHuaDeviceType.values()).filter(s -> s.getNo() == no).findFirst().orElse(null);
	}

	public static DaHuaDeviceType getDeviceTypeByName(String name) {
		return Stream.of(DaHuaDeviceType.values()).filter(s -> s.getName().equals(name)).findFirst().orElse(null);
	}

	public static DaHuaDeviceType getDeviceTypeByCnName(String cnName) {
		return Stream.of(DaHuaDeviceType.values()).filter(s -> s.getCnName().equals(cnName)).findFirst().orElse(null);
	}

	public int getNo() {
		return no;
	}

	public String getName() {
		return name;
	}

	public String getCnName() {
		return cnName;
	}

	public DeviceType getDeviceType() {
		return deviceType;
	}
}
