package com.gcloud.mesh.asset.enums;

import java.util.stream.Stream;

public enum YingZeDeviceType {
	
	GENERAL(0, "general", "内存设备", DeviceType.GENERAL),
	AIR_CONDITION(1, "air_condition", "温湿度", DeviceType.AIR_CONDITION),
	DISTRIBUTION_BOX(2, "general", "开关量采集", DeviceType.GENERAL),
	UPS(3, "ups", "UPS", DeviceType.UPS);
	
	private int no;
	private String name;
	private String cnName;
	private DeviceType deviceType;
	
	YingZeDeviceType(int no, String name, String cnName, DeviceType deviceType) {
		this.no = no;
		this.name = name;
		this.cnName = cnName;
		this.deviceType = deviceType;
	}
	
	public static YingZeDeviceType getDeviceTypeByNo(int no) {
		return Stream.of(YingZeDeviceType.values())
				.filter( s -> s.getNo() == no)
				.findFirst()
				.orElse(null);
	}
	
	public static YingZeDeviceType getDeviceTypeByName(String name) {
		return Stream.of(YingZeDeviceType.values())
				.filter( s -> s.getName().equals(name))
				.findFirst()
				.orElse(null);
	}
	
	public static YingZeDeviceType getDeviceTypeByCnName(String cnName) {
		return Stream.of(YingZeDeviceType.values())
				.filter( s -> s.getCnName().equals(cnName))
				.findFirst()
				.orElse(null);
	}
	
	public int getNo() {
		return no;
	}
	
	public String getName() {
		return name;
	}
	
	public String getCnName() {
		return cnName;
	}
	
	public DeviceType getDeviceType() {
		return deviceType;
	}
}
