package com.gcloud.mesh.asset.dao;

import com.gcloud.framework.db.PageResult;
import com.gcloud.framework.db.dao.impl.JdbcBaseDaoImpl;
import com.gcloud.mesh.asset.entity.NodeEntity;
import com.gcloud.mesh.dcs.service.AuthorityService;
import com.gcloud.mesh.header.enums.AuthorityResourceClassification;
import com.gcloud.mesh.header.enums.AuthorityResourceType;
import com.gcloud.mesh.header.msg.dcs.ListByClassificationMsg;
import com.gcloud.mesh.header.vo.dcs.AuthorityVo;
import io.micrometer.core.instrument.util.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;


@Repository
public class NodeDao extends JdbcBaseDaoImpl<NodeEntity, String>{
	
	@Autowired
	private AuthorityService authorityService;

	public <E> E getById(String id, String userId, Class<E> clazz) {
		StringBuffer sql = new StringBuffer();
		sql.append(" select i.*, n.mgm_ip mgm_ip, n.ipmi_ip ipmi_ip, n.ipmi_user ipmi_user, n.ipmi_password ipmi_password, "
				+ " n.network_status network_status, n.power_status power_status, "
				+ " d.name datacenter_name, d.ip datacenter_ip, "
				+ " n.hostname hostname, "
				+ " n.rated_power rated_power, "
				+ " n.power_consumption power_consumption, "
				+ " n.energy_efficiency_ratio energy_efficiency_ratio, "
				+ " n.storage_capacity storage_capacity, "
				+ " n.isolation isolation,"
				+ " n.rated_power_sm rated_power_sm,"
				+ " n.rated_voltage_sm rated_voltage_sm,"
				+ " n.sm_data sm_data,"
				+ " n.cabinet_id cabinet_id"
				+ " from asset_iaas i");
		sql.append(" left join asset_nodes n on i.device_id = n.id");
		sql.append(" left join asset_datacenters d on i.datacenter_id = d.id");
		sql.append(" where 1 = 1");
		List<Object> values = new ArrayList<Object>();
		if(StringUtils.isNotEmpty(id)) {	
			sql.append(" and i.id = ?");
			values.add(id);
		}
		if(StringUtils.isNotEmpty(userId)) {
			sql.append(" and i.creator = ?");
			values.add(userId);
		}
		return findBySql(sql.toString(), values, clazz).size() > 0? findBySql(sql.toString(), values, clazz).get(0): null;
	}
	
	public <E> PageResult<E> page(int pageNum, int pageSize, String datacenterId, String name, String order, String userId, Class<E> clazz) {
		StringBuffer sql = new StringBuffer();
		sql.append(" select i.*, d.name datacenter_name, n.network_status network_status, n.power_status power_status, n.ipmi_ip ipmi_ip, n.mgm_ip mgm_ip, n.power_consumption power_consumption, n.energy_efficiency_ratio energy_efficiency_ratio, n.storage_capacity storage_capacity, n.isolation isolation, n.cabinet_id cabinet_id");
		sql.append(" from asset_iaas i");
		sql.append(" left join asset_nodes n on i.device_id = n.id");
		sql.append(" left join asset_datacenters d on i.datacenter_id = d.id");
		sql.append(" where 1 = 1");
		sql.append(" and i.type = 6");
		
		checkResourceAuthority(sql);
		
		List<Object> values = new ArrayList<Object>();
		if(StringUtils.isNotEmpty(datacenterId)) {
			sql.append(" and i.datacenter_id = ?");
			values.add(datacenterId);
		}
//		if(StringUtils.isNotEmpty(userId)) {
//			sql.append(" and i.creator = ?");
//			values.add(userId);
//		}
		if(StringUtils.isNotEmpty(name)) {
			sql.append(" and i.name like concat('%', ?, '%')");
			values.add(name);
		}
		//已对order参数进行了验证，不会出现sql注入
		if(StringUtils.isNotEmpty(order)) {
			sql.append(" order by n." + order + " desc");
		}
		
		return this.findBySql(sql.toString(), values, pageNum, pageSize, clazz);	
		
	}
	
	public <E> PageResult<E> page(int pageNum, int pageSize, String name, String order, String userId, Class<E> clazz) {
		
		return page(pageNum, pageSize, null, name, order, userId, clazz);	
		
	}
	
	private StringBuffer checkResourceAuthority(StringBuffer sql) {

		ListByClassificationMsg msg = new ListByClassificationMsg();
		msg.setClassification(AuthorityResourceClassification.IT_DEVICE.getName());
		List<AuthorityVo> authorities = null;
		try {
			authorities = authorityService.listByClassification(msg);
		}catch(Exception e) {
			
		}
		if(authorities != null) {
			for(AuthorityVo vo: authorities) {
				if(AuthorityResourceType.SERVER.equals(AuthorityResourceType.getByName(vo.getId())) && !vo.getEnabled()) {
					sql.append(" and i.from != 'sync'");
					break;
				}
			}
		}
		
		return sql;
	}
	
}
