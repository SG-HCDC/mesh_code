package com.gcloud.mesh.asset.entity;

import java.util.Date;

import com.gcloud.framework.db.jdbc.annotation.ID;
import com.gcloud.framework.db.jdbc.annotation.Table;

import lombok.Data;

@Table( name = "asset_datacenters")
@Data
public class DatacenterEntity {

	@ID
	private String id;
	private String name;
	private String ip;
	private String creator;
	private Date createTime;
	private Integer power;
	private Boolean powerConservation;
	private Integer threshold;
	private Boolean isScheduler;
	//增加架构参数
	private String structure;
}
