package com.gcloud.mesh.asset.enums;

import java.util.stream.Stream;

public enum CloudResourceType {


	K8S(1, "容器", "k8s"),
	ALI(2, "阿里云", "ali"),
	HUAWEI(3, "华为云", "huawei"),
	UNKNOWN(4, "虚拟机", "unknown");

	private Integer type;
	private String name;
	private String enName;

	CloudResourceType(Integer type, String name, String enName) {
		this.type = type;
		this.name = name;
		this.enName = enName;
	}

	public static CloudResourceType getByType(Integer type) {
		return Stream.of(CloudResourceType.values()).filter(s -> s.getType() == type).findFirst().orElse(CloudResourceType.UNKNOWN);
	}
	
	public static CloudResourceType getByEnName(String enName) {
		return Stream.of(CloudResourceType.values()).filter(s -> s.getEnName().equals(enName)).findFirst().orElse(CloudResourceType.UNKNOWN);
	}


	public Integer getType() {
		return type;
	}

	public String getName() {
		return name;
	}

	public String getEnName() {
		return enName;
	}
}
