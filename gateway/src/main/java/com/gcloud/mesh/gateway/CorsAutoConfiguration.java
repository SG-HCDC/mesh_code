
package com.gcloud.mesh.gateway;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.reactive.CorsWebFilter;
import org.springframework.web.cors.reactive.UrlBasedCorsConfigurationSource;

/**
 * @author roger yang
 * @date 11/21/2019
 */
@Configuration
public class CorsAutoConfiguration {

//	@Bean
//	public CorsWebFilter corsFilter() {
//		UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource(new PathPatternParser());
//		globalCorsProperties.getCorsConfigurations()
//				.forEach((path, corsConfiguration) -> source.registerCorsConfiguration(path, corsConfiguration));
//		return new CorsWebFilter(source);
//	}

    @Bean
    CorsWebFilter corsFilter() {

        CorsConfiguration config = new CorsConfiguration();

        config.setAllowCredentials(true);
//        config.addAllowedOrigin("http://192.168.20.198");
        config.addAllowedOrigin("*");
        config.addAllowedHeader("*");
        config.addAllowedMethod("*");

        UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
        source.registerCorsConfiguration("/**", config);

        return new CorsWebFilter(source);
    }

}
