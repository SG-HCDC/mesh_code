package org.jeecg.common.isc;

import org.jeecg.common.vo.PermissionsCompareVo;

public interface IIscService {
	PermissionsCompareVo permissionsCompare() throws Exception ;
}
