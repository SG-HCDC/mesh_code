
package org.jeecg.common.vo;

import lombok.Data;

@Data
public class PermissionsInfoVo {

	private String code;
	private String name;
	private String url;

}
