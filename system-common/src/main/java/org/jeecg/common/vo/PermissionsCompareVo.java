
package org.jeecg.common.vo;

import java.util.List;

import lombok.Data;

@Data
public class PermissionsCompareVo {

	private List<PermissionsInfoVo> meshPermissions;

	private List<PermissionsInfoVo> iscPermissions;

	private List<PermissionsInfoVo> iscLackPermissions;

}
