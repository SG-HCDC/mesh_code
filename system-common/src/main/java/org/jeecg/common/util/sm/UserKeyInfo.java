package org.jeecg.common.util.sm;

import lombok.Data;

@Data
public class UserKeyInfo {
	
	private static final long serialVersionUID = 1L;
	
	private String userKey;
	private String sm2PublicKey;
	private String sm2PrivateKey;
	private String sm4Key;

}
