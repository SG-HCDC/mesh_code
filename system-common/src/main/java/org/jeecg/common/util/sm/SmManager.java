package org.jeecg.common.util.sm;

import java.util.Map;

import org.bouncycastle.pqc.math.linearalgebra.ByteUtils;

import lombok.extern.slf4j.Slf4j;


@Slf4j
public class SmManager {
	
	public static final String SM2_HEADER = "sm2";
	
	public static final String SM3_HEADER = "sm3";
	    
	public static final String SM4_HEADER = "sm4";
	
//	public static final String SM4_SERVER_KEY = "E526CD8DFC4E3BCAB0BA15F3C822B6AE";
	
	public static final String SM4_SERVER_KEY_1 = "E526CD8DFC4E3BCA";
	
	public static final String SM4_SERVER_KEY_2 = "B0BA15F3C822B6AE";
	
	public static final String SM = "sm_check_";
	
//	public static final String SM3 = "sm3_check_";
	
	public static final String SM4_SERVER = "sm4_server_";
	
	public static final String SM4_CLIENT = "sm4_client_";
	
	public static final String ENCODING = "utf-8";
    
    public static Map<String,String> generateKeypair() throws Exception {
    	Map<String,String> keypair = SM2Util.generateKeypair();
    	return keypair;
    }
    
    public static String encryptSm2(UserKeyInfo userKey, String data) throws Exception {
    	String cipher = null;
    	if(userKey != null) {
    		try {
    			String hexStr = GMSSLUtil.encryptSm2(userKey.getSm2PublicKey(), userKey.getSm2PrivateKey(), data);
    			cipher = hexStr;
    		}catch(Exception e) {
    			log.error(" SmManager encryptSm2 error: {}", e.getMessage());
    		}	
    	}
    	
        return cipher;
    }
    
    public static String decryptSm2(UserKeyInfo userKey, String cipher) throws Exception {
    	String data = null;
    	if(userKey != null) {
    		try {
    			String hexStr = GMSSLUtil.decryptSm2(userKey.getSm2PublicKey(), userKey.getSm2PrivateKey(), cipher);
    			data = StrUtil.hexToStr(hexStr);
    		}catch(Exception e) {
    			log.error(" SmManager decryptSm2 error: {}", e.getMessage());
    		}
    	}
    	
    	return data;
    }
    
//    public static String encryptClientSm4(UserKeyInfo userKey, String data) throws Exception {
//    	String cipher = null;
//    	if(userKey != null) {
//    		try {
//    			SymmetricCrypto sm4 = SmUtil.sm4(ByteUtils.fromHexString(userKey.getSm4Key()));
//        		cipher = sm4.encryptHex(data);
//    		}catch(Exception e) {
//    			log.error(" SmManager encryptClientSm4 error: {}", e.getMessage());
//    		}
//    		
//    	}  	
//        return cipher;
//    }
    
//    public static String decryptClientSm4(UserKeyInfo userKey, String cipher) throws Exception {
//    	String data = null;
//    	if(userKey != null) {
//    		try {
//    			SymmetricCrypto sm4 = SmUtil.sm4(ByteUtils.fromHexString(userKey.getSm4Key()));
//        		data = sm4.decryptStr(cipher, CharsetUtil.CHARSET_UTF_8);
//    		}catch(Exception e) {
//    			log.error(" SmManager decryptClientSm4 error: {}", e.getMessage());
//    		}
//    		
//    	}
//    	
//    	return data;
//    }
    
    /**
     * sm4  ecb
     * @param key
     * @param data
     * @return hexStr
     * @throws Exception
     */
    public static String encryptClientSm4(String key, String data) throws Exception {
    	String cipher = null;
    	if(key != null) {
    		try {
    			String dataHex = StrUtil.strToHex(data);
    			cipher = GMSSLUtil.encryptSm4(key, dataHex);
//    			cipher = SM4Util.encryptEcb(key, dataHex);
    		}catch(Exception e) {
    			log.error(" SmManager encryptClientSm4 error: {}", e.getMessage());
    		}
    		
    	}  	
        return cipher;
    }
    
    /**
     * sm4 ecb
     * @param key
     * @param cipher
     * @return str
     * @throws Exception
     */
    public static String decryptClientSm4(String key, String cipher) throws Exception {
    	String data = null;
    	if(key != null) {
    		try {
    			String hexStr = GMSSLUtil.decryptSm4(key, cipher);
//    			String hexStr = SM4Util.decryptEcb(key, cipher);
        		data = StrUtil.hexToStr(hexStr);
    		}catch(Exception e) {
    			log.error(" SmManager decryptClientSm4 error: {}", e.getMessage());
    		}	
    	}
    	
    	return data;
    }
    
    public static String encryptServerSm4(String data) throws Exception {
    	String cipher = null;
		try {
			String privateKey = SM4_SERVER_KEY_1 + SM4_SERVER_KEY_2;
			String hexStr = StrUtil.strToHex(data);
			cipher = GMSSLUtil.encryptSm4(privateKey, hexStr);
//			cipher = SM4Util.encryptEcb(privateKey, hexStr);
		}catch(Exception e) {
			log.error(" SmManager encryptServerSm4 error: {}", e.getMessage());
		}   
        return cipher;
    }
    
    public static String decryptServerSm4(String cipher) throws Exception {
    	String data = null;
    	try {
			String privateKey = SM4_SERVER_KEY_1 + SM4_SERVER_KEY_2;
			String hexStr = GMSSLUtil.decryptSm4(privateKey, cipher);
//			String hexStr = SM4Util.decryptEcb(privateKey, cipher);
			data = StrUtil.hexToStr(hexStr);
		}catch(Exception e) {
			log.error(" SmManager decryptServerSm4 error: {}", e.getMessage());
		} 
    	return data;
    }
    
    public static String encryptSm3(String data) throws Exception {
    	String digestHex = null;
    	try {
    		String hexStr = StrUtil.strToHex(data);
    		digestHex = GMSSLUtil.encryptSm3(hexStr);
//    		digestHex = SM3Util.encrypt(hexStr);
    	}catch(Exception e) {
    		log.error(" SmManager encryptSm3 error: {}", e.getMessage());
    	}
    	return digestHex;
    }
    
    public static boolean verifySm3(String data, String cipher) throws Exception {
    	try {
    		String hexStr = StrUtil.strToHex(data);
    		String digestHex = GMSSLUtil.encryptSm3(hexStr);
//    		String digestHex = SmUtil.sm3(data);
    		return digestHex.equals(cipher);
    	}catch(Exception e) {
    		log.error(" SmManager verifySm3 error: {}", e.getMessage());
    	}
    	return false;
    }

}
