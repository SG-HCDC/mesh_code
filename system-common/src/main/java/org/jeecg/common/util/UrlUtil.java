package org.jeecg.common.util;

public class UrlUtil {
	public static String formatUrl(String url) {
		String requestUrl = url.replace("//", "/");
		if (requestUrl.startsWith("/mesh-controller")) {
			requestUrl = requestUrl.substring("/mesh-controller".length(), requestUrl.length());
		}
		return requestUrl;
	}

}
