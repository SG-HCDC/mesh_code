package org.jeecg.common.util.sm;
import java.security.Key;
import java.security.SecureRandom;
import java.security.Security;
import java.util.Arrays;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

import org.apache.commons.codec.binary.Hex;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.bouncycastle.pqc.math.linearalgebra.ByteUtils;

public class SM4Util {
    static {
        Security.addProvider(new BouncyCastleProvider());
    }
    
    private static final String ENCODING = "UTF-8";
    public static final String ALGORITHM_NAME = "SM4";
    // 加密算法/分组加密模式/分组填充方式
    // PKCS5Padding-以8个字节为一组进行分组加密
    // 定义分组加密模式使用：PKCS5Padding
    public static final String ALGORITHM_NAME_ECB_PADDING = "SM4/ECB/PKCS5Padding";
    public static final String ALGORITHM_NAME_CBC_PADDING = "SM4/CBC/PKCS5Padding";
    // 128-32位16进制；256-64位16进制
    public static final int DEFAULT_KEY_SIZE = 128;
    private static final int ENCRYPT_MODE = 1;
    private static final int DECRYPT_MODE = 2;
    private static final String PROVIDER_NAME = "BC";
    
    /**
     * 自动生成密钥
     *
     * @return
     * @explain
     */
    public static String generateKey() throws Exception {
        return new String(Hex.encodeHex(generateKey(DEFAULT_KEY_SIZE),false));
    }
    
    /**
     * @param keySize
     * @return
     * @throws Exception
     * @explain
     */
    public static byte[] generateKey(int keySize) throws Exception {
        KeyGenerator kg = KeyGenerator.getInstance(ALGORITHM_NAME, BouncyCastleProvider.PROVIDER_NAME);
        kg.init(keySize, new SecureRandom());
        return kg.generateKey().getEncoded();
    }
    
    /**
     * 生成ECB暗号
     *
     * @param algorithmName 算法名称
     * @param mode          模式
     * @param key
     * @return
     * @throws Exception
     * @explain ECB模式（电子密码本模式：Electronic codebook）
     */
    private static Cipher generateEcbCipher(String algorithmName, int mode, byte[] key) throws Exception {
        Cipher cipher = Cipher.getInstance(algorithmName, BouncyCastleProvider.PROVIDER_NAME);
        Key sm4Key = new SecretKeySpec(key, ALGORITHM_NAME);
        cipher.init(mode, sm4Key);
        return cipher;
    }
    
    /**
     * sm4加密
     *
     * @param hexKey   16进制密钥（忽略大小写）
     * @param paramStr 待加密字符串
     * @return 返回16进制的加密字符串
     * @explain 加密模式：ECB
     * 密文长度不固定，会随着被加密字符串长度的变化而变化
     */
    public static String encryptEcb(String hexKey, String paramStr) {
        try {
            String cipherText = "";
            // 16进制字符串--&gt;byte[]
            byte[] keyData = ByteUtils.fromHexString(hexKey);
            // String--&gt;byte[]
            byte[] srcData = ByteUtils.fromHexString(paramStr);
            		//paramStr.getBytes(ENCODING);
            // 加密后的数组
            byte[] cipherArray = encrypt_Ecb_Padding(keyData, srcData);
            // byte[]--&gt;hexString
            cipherText = ByteUtils.toHexString(cipherArray);
            return cipherText;
        } catch (Exception e) {
            return paramStr;
        }
    }
    
    /**
     * sm4加密
     *
     * @param hexKey   16进制密钥（忽略大小写）
     * @param paramStr 待加密字符串
     * @return 返回16进制的加密字符串
     * @explain 加密模式：cbc
     * 密文长度不固定，会随着被加密字符串长度的变化而变化
     */
    public static String encryptCbc(String hexKey, String vi, String paramStr) {
        try {
            String cipherText = "";
            // 16进制字符串--&gt;byte[]
            byte[] keyData = ByteUtils.fromHexString(hexKey);
            // String--&gt;byte[]
            byte[] srcData = ByteUtils.fromHexString(paramStr);
            		//paramStr.getBytes(ENCODING);
            
            byte[] ivData = ByteUtils.fromHexString(vi);
            // 加密后的数组
            byte[] cipherArray = encryptCbcPadding(keyData, ivData, srcData);
            // byte[]--&gt;hexString
            cipherText = ByteUtils.toHexString(cipherArray);
            return cipherText;
        } catch (Exception e) {
            return paramStr;
        }
    }
    
    /**
     * 加密模式之Ecb
     *
     * @param key
     * @param data
     * @return
     * @throws Exception
     * @explain
     */
    public static byte[] encrypt_Ecb_Padding(byte[] key, byte[] data) throws Exception {
        Cipher cipher = generateEcbCipher(ALGORITHM_NAME_ECB_PADDING, Cipher.ENCRYPT_MODE, key);
        return cipher.doFinal(data);
    }
    
    /**
     * sm4解密
     *
     * @param hexKey     16进制密钥
     * @param cipherText 16进制的加密字符串（忽略大小写）
     * @return 解密后的字符串
     * @throws Exception
     * @explain 解密模式：采用ECB
     */
    public static String decryptEcb(String hexKey, String cipherText) {
        // 用于接收解密后的字符串
        String decryptStr = "";
        // hexString--&gt;byte[]
        byte[] keyData = ByteUtils.fromHexString(hexKey);
        // hexString--&gt;byte[]
        byte[] cipherData = ByteUtils.fromHexString(cipherText);
        // 解密
        byte[] srcData = new byte[0];
        try {
            srcData = decrypt_Ecb_Padding(keyData, cipherData);
            // byte[]--&gt;String
            //ByteUtils.toHexString(arg0, arg1, arg2)
            decryptStr = ByteUtils.toHexString(srcData);
            //ByteUtils.fromHexString(decryptStr);
            
        } catch (Exception e) {
            e.printStackTrace();
        }
        return decryptStr;
    }
    
    
    /**
     * sm4解密
     *
     * @param hexKey     16进制密钥
     * @param cipherText 16进制的加密字符串（忽略大小写）
     * @return 解密后的字符串
     * @throws Exception
     * @explain 解密模式：采用cbc
     */
    public static String decryptcbc(String hexKey, String iv, String cipherText) {
        // 用于接收解密后的字符串
        String decryptStr = "";
        // hexString--&gt;byte[]
        byte[] keyData = ByteUtils.fromHexString(hexKey);
        // hexString--&gt;byte[]
        byte[] cipherData = ByteUtils.fromHexString(cipherText);
        
        byte[] ivData = ByteUtils.fromHexString(iv);
        // 解密
        byte[] srcData = new byte[0];
        try {
            srcData = decryptCbcPadding(keyData, ivData, cipherData);
            decryptStr = ByteUtils.toHexString(srcData);
            
        } catch (Exception e) {
            e.printStackTrace();
        }
        return decryptStr;
    }
    
    
    /**
     * 解密
     *
     * @param key
     * @param cipherText
     * @return
     * @throws Exception
     * @explain
     */
    public static byte[] decrypt_Ecb_Padding(byte[] key, byte[] cipherText) throws Exception {
        Cipher cipher = generateEcbCipher(ALGORITHM_NAME_ECB_PADDING, Cipher.DECRYPT_MODE, key);
        return cipher.doFinal(cipherText);
    }
    
    /**
     * 校验加密前后的字符串是否为同一数据
     *
     * @param hexKey     16进制密钥（忽略大小写）
     * @param cipherText 16进制加密后的字符串
     * @param paramStr   加密前的字符串
     * @return 是否为同一数据
     * @throws Exception
     * @explain
     */
    public static boolean verifyEcb(String hexKey, String cipherText, String paramStr) throws Exception {
        // 用于接收校验结果
        boolean flag = false;
        // hexString--&gt;byte[]
        byte[] keyData = ByteUtils.fromHexString(hexKey);
        // 将16进制字符串转换成数组
        byte[] cipherData = ByteUtils.fromHexString(cipherText);
        // 解密
        byte[] decryptData = decrypt_Ecb_Padding(keyData, cipherData);
        // 将原字符串转换成byte[]
        byte[] srcData = ByteUtils.fromHexString(paramStr);
        		
        //		paramStr.getBytes(ENCODING);
        // 判断2个数组是否一致
        flag = Arrays.equals(decryptData, srcData);
        return flag;
    }
    
    /**
     * 校验加密前后的字符串是否为同一数据
     *
     * @param hexKey     16进制密钥（忽略大小写）
     * @param cipherText 16进制加密后的字符串
     * @param paramStr   加密前的字符串
     * @return 是否为同一数据
     * @throws Exception
     * @explain
     */
    public static boolean verifyCbc(String hexKey, String cipherText, String paramStr, String vi) throws Exception {
        // 用于接收校验结果
        boolean flag = false;
        // hexString--&gt;byte[]
        byte[] keyData = ByteUtils.fromHexString(hexKey);
        // 将16进制字符串转换成数组
        byte[] cipherData = ByteUtils.fromHexString(cipherText);
        byte[] viData = ByteUtils.fromHexString(vi);
        // 解密
        byte[] decryptData = encryptCbcPadding(keyData, viData, cipherData);
        // 将原字符串转换成byte[]
        byte[] srcData = ByteUtils.fromHexString(paramStr);
        		
        //		paramStr.getBytes(ENCODING);
        // 判断2个数组是否一致
        flag = Arrays.equals(decryptData, srcData);
        return flag;
    }
    
    public static byte[] encryptCbcPadding(byte[] key, byte[] iv, byte[] data) throws Exception {
        Cipher cipher = generateCbcCipher(ENCRYPT_MODE, key, iv);
        return cipher.doFinal(data);
     }
    
    public static byte[] decryptCbcPadding(byte[] key, byte[] iv, byte[] data) throws Exception {
        Cipher cipher = generateCbcCipher(DECRYPT_MODE, key, iv);
        return cipher.doFinal(data);
     }
    private static Cipher generateCbcCipher(int mode, byte[] key, byte[] iv) throws Exception {
        Cipher cipher = Cipher.getInstance(ALGORITHM_NAME_CBC_PADDING, PROVIDER_NAME);
        Key sm4Key = new SecretKeySpec(key, ALGORITHM_NAME);
        IvParameterSpec ivParameterSpec = new IvParameterSpec(iv);
        cipher.init(mode, sm4Key, ivParameterSpec);
        return cipher;
     }
    
//    public static void main(String[] args) {
//        try {
//            String json = "4bb34f9dbf3f24d1f20a97519e6606ee";
//            System.out.println("sm4Ecb加密前源数据————" + json);
//            // 生成32位16进制密钥
//            String key = "5ec863eb9a4c37e11c87c7ed85ecd3d3";
//            System.out.println(key + "-----生成key");
//            String cipher = SM4Util.encryptEcb(key, json);
//            System.out.println("加密串---" + cipher);
//            System.out.println(SM4Util.verifyEcb(key, cipher, json));
//            json = SM4Util.decryptEcb(key, cipher);
//            System.out.println("解密后数据---" + json);
//            
//            String json2 = "BB5DCE7C7E453BFAC84D677581EEEC16EDA7E1AD4F94828280DF1FC7A89BB2BE";
//            String key2 = "E526CD8DFC4E3BCAB0BA15F3C822B6AE";
//            String vi = "00000000000000000000000000000000";
//            System.out.println("sm4CBC加密明文："+key2);
//            System.out.println("sm4CBCKey："+json2);
//            System.out.println("sm4CBC加密VI："+vi);
//            String cipher2 = SM4Util.encryptCbc(key2, vi, json2);
//            System.out.println("sm4CBC加密串---" + cipher2);
//            System.out.println(SM4Util.verifyCbc(key2, cipher2, json2, vi));
//            json2 = SM4Util.decryptcbc(key2, vi, cipher2);
//            System.out.println("解密后数据---" + json2);
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//    }
}