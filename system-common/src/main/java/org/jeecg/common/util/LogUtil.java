package org.jeecg.common.util;

import java.net.URLDecoder;
import java.net.URLEncoder;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.jeecg.common.cache.PermissionCodeCache;
import org.jeecg.common.constant.CommonConstant;
import org.jeecg.common.constant.enums.SysLogOperateType;
import org.jeecg.common.constant.enums.SysLogType;
import org.jeecg.common.constant.enums.SysLogUrlOperate;
import org.jeecg.common.system.api.ISysBaseAPI;
import org.jeecg.common.vo.PermissionsInfoVo;

import com.gcloud.mesh.framework.core.SpringUtil;

import cn.hutool.core.lang.UUID;

public class LogUtil {
	public static void log(HttpServletRequest request, String remark) {
		String url = UrlUtil.formatUrl(request.getRequestURI());
		ISysBaseAPI sysBaseAPI = SpringUtil.getBean(ISysBaseAPI.class);
		String etag = request.getHeader("ETag");
		
		String title = null;
		String content = null;
		if(etag != null) {
			etag = URLDecoder.decode(etag);
			String[] str = etag.split(">");
			if(str.length == 1) {
				title = str[0];
			}else if(str.length == 2) {
				title = str[0];
				content = str[1];
			}
		}
		if(url.equals("/history/page")) {
			sysBaseAPI.addLog("查询诊断告警信息列表" , null, SysLogOperateType.QUERY, null, remark, title);
		}else if(url.equals("/linkman/page")) {
			if(!StringUtils.isBlank(title)) {
				sysBaseAPI.addLog(content != null?content:"查看联系人", null, SysLogOperateType.DETAIL, null, remark, title);
			}
		}else if(url.equals("/linkman/create")) {
			sysBaseAPI.addLog(content != null?content:"新增联系人", null, SysLogOperateType.ADD, null, remark, title);
		}else if(url.equals("/linkman/delete")) {
			sysBaseAPI.addLog(content != null?content:"删除联系人", null, SysLogOperateType.DELETE, null, remark, title);
		}else if(url.equals("/linkman/update1")) {
			sysBaseAPI.addLog((content != null?content:"更新联系人"), null, SysLogOperateType.UPDATE, null, remark, title);
		}else if(url.equals("/linkman/list1")) {
			if(!StringUtils.isBlank(title)) {
				sysBaseAPI.addLog(content != null?content:"查看告警联系人" , null, SysLogOperateType.DETAIL, null, remark, title);
			}
		}
		else if(url.equals("/policy/page")) {
			if(!StringUtils.isBlank(title)) {
				if(title.contains("告警通知方式设置")) {
					sysBaseAPI.addLog(content != null?content:"查询告警通知方式设置列表", null, SysLogOperateType.QUERY, null, remark, title);
				}else if(title.contains("告警策略设置")){
					sysBaseAPI.addLog(content != null?content:"查询告警策略设置列表", null, SysLogOperateType.QUERY, null, remark, title);
				}else {
					sysBaseAPI.addLog(content != null?content:"查询告警策略列表", null, SysLogOperateType.QUERY, null, remark, title);
				}
				
			}
		}else if(url.equals("/policy/create")) {
			if(!StringUtils.isBlank(title)) {
				StringBuffer buff = new StringBuffer();
				buff.append(content != null?content:"新增策略");
				// append(request.toString());
				sysBaseAPI.addLog(buff.toString(), null, SysLogOperateType.ADD, null, remark, title);
			}
		}else if(url.equals("/policy/delete")) {
			if(!StringUtils.isBlank(title)) {
				sysBaseAPI.addLog(content != null?content:"删除策略", null, SysLogOperateType.DELETE, null, remark, title);
			}
		}else if(url.equals("/policy/update1")) {
			if(!StringUtils.isBlank(title)) {
				StringBuffer buff = new StringBuffer();
				buff.append(content != null?content:"更新策略");
				// buff.append(request.toString());
				sysBaseAPI.addLog(buff.toString(), null, SysLogOperateType.UPDATE, null, remark, title);
			}
		}else if(url.equals("/policy/detail")) {
			if(!StringUtils.isBlank(title)) {
				sysBaseAPI.addLog(content != null?content:"查看策略详情" , null, SysLogOperateType.DETAIL, null, remark, title);
				// sysBaseAPI.addLog("删除联系人；ID:" + request.getLinkmanId(), null, SysLogOperateType.DELETE, null, remark, title);
			}
		}else if(url.equals("/policy/addPolicyLinkman")) {
			if(!StringUtils.isBlank(title)) {
				sysBaseAPI.addLog(content != null?content:"新增策略联系人" , null, SysLogOperateType.ADD, null, remark, title);
			}
		}else if(url.equals("/policy/deletePolicyLinkman")) {
			if(!StringUtils.isBlank(title)) {
				sysBaseAPI.addLog(content != null?content:"删除策略联系人" , null, SysLogOperateType.DELETE, null, remark, title);
			}
		}
		else if(url.equals("/analysis/pageNodeFitter")) {
			sysBaseAPI.addLog(content != null?content:"拟合", null, SysLogOperateType.DETAIL, null, remark ,title == null?"健康智能监控-数据中心关键设备节点传导特性拟合":title);
		}else if(url.equals("/analysis/doFitter")) {
			sysBaseAPI.addLog(content != null?content:"拟合", null, SysLogOperateType.DETAIL, null, remark ,title == null ? "健康智能监控-数据中心关键设备节点传导特性拟合":title);
		}else if(url.equals("/analysis/doSimulate")) {
			sysBaseAPI.addLog(content != null?content:"查看基于能耗传导的数据中心运行仿真", null, SysLogOperateType.DETAIL, null, remark ,title == null ? "健康智能监控-基于能耗传导的数据中心运行仿真":title);
		}else if(url.equals("/analysis/optimum")) {
			sysBaseAPI.addLog(content != null?content:"查看最优策略建议", null, SysLogOperateType.DETAIL, null, remark ,title == null ? "健康智能监控-设备运行情况分析模型的最优策略建议-最优策略建议":title);
		}else if(url.equals("/analysis/pageNodeHistory")) {
			sysBaseAPI.addLog(content != null?content:"查询历史最优策略建议列表", null, SysLogOperateType.QUERY, null, remark ,title == null ? "健康智能监控-设备运行情况分析模型的最优策略建议-历史最优策略建议":title);
		}else if(url.equals("/analysis/pageNodeSimulate")) {
			sysBaseAPI.addLog(content != null?content:"查看历史最优策略仿真", null, SysLogOperateType.DETAIL, null, remark ,"健康智能监控-设备运行情况分析模型的最优策略建议-历史最优策略仿真");
		}else if(url.equals("/subhealth/pageAir")) {
			sysBaseAPI.addLog(content != null?content:"查询空调亚健康分析列表", null, SysLogOperateType.QUERY, null, remark ,title == null ? "健康智能监控-数据中心设备亚健康分析与预警-空调亚健康分析":title);
		}else if(url.equals("/subhealth/pageUps")) {
			sysBaseAPI.addLog(content != null?content:"查询UPS亚健康分析列表", null, SysLogOperateType.QUERY, null, remark ,title == null ? "健康智能监控-数据中心设备亚健康分析与预警-UPS亚健康分析":title);
		}else if(url.equals("/subhealth/listDatacenter")) {
			if(title != null) {
				sysBaseAPI.addLog(content != null?content:"查看亚健康阈值设置页面", null, SysLogOperateType.DETAIL, null, remark ,title == null?"健康智能监控-数据中心设备亚健康分析与预警-亚健康阈值设置":title);
			}else {
				sysBaseAPI.addLog(content != null?content:"查询数据中心亚健康分析列表", null, SysLogOperateType.QUERY, null, remark ,title == null?"健康智能监控-数据中心设备亚健康分析与预警-数据中心亚健康分析":title);
			}
		}else if(url.equals("/subhealth/pageDatacenter")) {
			sysBaseAPI.addLog(content != null?content:"查询数据中心亚健康分析列表", null, SysLogOperateType.QUERY, null, remark ,title == null ? "健康智能监控-数据中心设备亚健康分析与预警-数据中心亚健康分析":title);
		}else if(url.equals("/subhealth/setThreshold")) {
			sysBaseAPI.addLog("设置亚健康阈值", null, SysLogOperateType.UPDATE, null, remark, "健康智能监控-数据中心设备亚健康分析与预警-亚健康阈值设置");
		}else if(url.equals("/subhealth/listThreshold")) {
			sysBaseAPI.addLog(content != null?content:"查询阈值列表", null, SysLogOperateType.QUERY, null, remark ,title == null ? "健康智能监控-数据中心设备亚健康分析与预警-亚健康阈值设置":title);
		}else if(url.equals("/subhealth/listThresholdMeter")) {
			sysBaseAPI.addLog(content != null?content:"查询亚健康-阈值告警项列表", null, SysLogOperateType.QUERY, null, remark ,title == null ? "健康智能监控-数据中心设备亚健康分析与预警-亚健康阈值设置":title);
		}else if(url.equals("/subhealth/listAlertLevel")) {
			sysBaseAPI.addLog(content != null?content:"查询亚健康-阈值告警级别列表", null, SysLogOperateType.QUERY, null, remark ,title == null ? "健康智能监控-数据中心设备亚健康分析与预警-亚健康阈值设置":title);
		}else if(url.equals("/subhealth/countAlerts")) {
			sysBaseAPI.addLog(content != null?content:"查看告警数量", null, SysLogOperateType.DETAIL, null, remark ,title == null ? "健康智能监控-数据中心设备亚健康分析与预警-亚健康预警":title);
		}else if(url.equals("/subhealth/alertStatistic")) {
			sysBaseAPI.addLog(content != null?content:"查看告警统计", null, SysLogOperateType.DETAIL, null, remark ,title == null ? "健康智能监控-数据中心设备亚健康分析与预警-亚健康预警":title);
		}else if(url.equals("/subhealth/pageAlert")) {
			sysBaseAPI.addLog(content != null?content:"查询告警列表", null, SysLogOperateType.QUERY, null, remark ,title == null ? "健康智能监控-数据中心设备亚健康分析与预警-亚健康预警":title);
		}else if(url.equals("/asset/updateDatacenter")) {
			if(StringUtils.isNotBlank(title)) {
				sysBaseAPI.addLog(content != null?content+"":"设置动环节能" , SysLogType.OPERATE, SysLogOperateType.UPDATE, null, remark, "精确管控与供电制冷联动-统一管理联动平台-动环节能");
			}else {
				sysBaseAPI.addLog(content != null?content:"更新数据中心" , SysLogType.OPERATE, SysLogOperateType.UPDATE, null, remark, "精确管控与供电制冷联动-基础资源细粒度感知-资源管理-数据中心");
			}
		}else if(url.equals("/asse/detailDatacentert")) {
			sysBaseAPI.addLog(content != null?content+"":"查看数据中心详情", SysLogType.OPERATE, SysLogOperateType.DETAIL, null, remark, "精确管控与供电制冷联动-基础资源细粒度感知-资源管理-数据中心");
		}else if(url.equals("/asset/pageDatacenter")) {
			if(title != null) {
				sysBaseAPI.addLog(content != null?content:"查看数据中心", SysLogType.OPERATE, SysLogOperateType.DETAIL, null, remark, title != null ? title:"精确管控与供电制冷联动-基础资源细粒度感知-资源管理-数据中心");
			}
		}else if(url.equals("/asset/listDatacenter")) {
			if(title != null) {
				sysBaseAPI.addLog("查询数据中心列表", SysLogType.OPERATE, SysLogOperateType.QUERY, null, remark, title != null ? title:"精确管控与供电制冷联动-基础资源细粒度感知-资源管理-数据中心");
			}
		}else if(url.equals("/asset/createDevice")) {
			sysBaseAPI.addLog("新增基础设施", SysLogType.OPERATE, SysLogOperateType.ADD, null, remark, "精确管控与供电制冷联动-基础资源细粒度感知-资源管理-L1层：基础设施");
		}else if(url.equals("/asset/updateDevice")) {
	    	StringBuffer buff = new StringBuffer();
	    	buff.append("编辑基础设施");
			sysBaseAPI.addLog(buff.toString(), SysLogType.OPERATE, SysLogOperateType.UPDATE, null, remark, "精确管控与供电制冷联动-基础资源细粒度感知-资源管理-L1层：基础设施");
		}else if(url.equals("/asset/deleteDevice")) {
			sysBaseAPI.addLog("删除基础设施",SysLogType.OPERATE, SysLogOperateType.DELETE, null, remark, "精确管控与供电制冷联动-基础资源细粒度感知-资源管理-L1层：基础设施");
		}else if(url.equals("/asset/detailDevice")) {
			sysBaseAPI.addLog("查看基础设施详情" ,SysLogType.OPERATE, SysLogOperateType.DETAIL, null, remark, "精确管控与供电制冷联动-基础资源细粒度感知-资源管理-L1层：基础设施");
		}else if(url.equals("/asset/pageDevice")) {
			sysBaseAPI.addLog(content != null?content:"查询基础设施列表", SysLogType.OPERATE, SysLogOperateType.QUERY, null, remark, title != null ? title:"精确管控与供电制冷联动-基础资源细粒度感知-资源管理-L1层：基础设施");
		}else if(url.equals("/asset/createSwitcher")) {
			sysBaseAPI.addLog("新增IT设备", SysLogType.OPERATE, SysLogOperateType.ADD, null, remark, title != null ? title:"精确管控与供电制冷联动-基础资源细粒度感知-资源管理-L2层：IT设备");
		}else if(url.equals("/asset/updateSwitcher")) {
			sysBaseAPI.addLog("编辑IT设备",SysLogType.OPERATE, SysLogOperateType.UPDATE, null, remark, title != null ? title: "精确管控与供电制冷联动-基础资源细粒度感知-资源管理-L2层：IT设备");
		}else if(url.equals("/asset/deleteSwitcher")) {
			sysBaseAPI.addLog("删除IT设备",SysLogType.OPERATE, SysLogOperateType.DELETE, null, remark, title != null ? title:"精确管控与供电制冷联动-基础资源细粒度感知-资源管理-L2层：IT设备");
		}else if(url.equals("/asset/detailSwitcher")) {
			sysBaseAPI.addLog("IT设备详情详情" ,SysLogType.OPERATE, SysLogOperateType.DETAIL, null, remark, title != null ? title:"精确管控与供电制冷联动-基础资源细粒度感知-资源管理-L2层：IT设备");
		}else if(url.equals("/asset/pageSwitcher")) {
			sysBaseAPI.addLog("查询IT设备列表", SysLogType.OPERATE, SysLogOperateType.QUERY, null, remark, title != null ? title:"精确管控与供电制冷联动-基础资源细粒度感知-资源管理-L2层：IT设备");
		}else if(url.equals("/asset/createNode")) {
			sysBaseAPI.addLog("新增IT设备", SysLogType.OPERATE, SysLogOperateType.ADD, null, remark, title != null ? title:"精确管控与供电制冷联动-基础资源细粒度感知-资源管理-L2层：IT设备");
		}else if(url.equals("/asset/deleteNode")) {
			sysBaseAPI.addLog("删除IT设备", SysLogType.OPERATE, SysLogOperateType.DELETE, null, remark, title != null ? title:"精确管控与供电制冷联动-基础资源细粒度感知-资源管理-L2层：IT设备");
		}else if(url.equals("/asset/updateNode")) {
	    	if(StringUtils.isNotBlank(title)) {
	    		if(request.getParameter("isolation") != null) {
	    			if(Boolean.valueOf(request.getParameter("isolation"))) {
	    				sysBaseAPI.addLog("设备隔离", SysLogType.OPERATE, SysLogOperateType.UPDATE, null, remark, title);
	    			}else {
	    				sysBaseAPI.addLog("异常复位", SysLogType.OPERATE, SysLogOperateType.UPDATE, null, remark, title != null ? title:"精确管控与供电制冷联动-统一管理联动-设备恢复");
	    			}    			
	    		}else {
	    			sysBaseAPI.addLog("编辑IT设备", SysLogType.OPERATE, SysLogOperateType.UPDATE, null, remark, title != null ? title:"精确管控与供电制冷联动-基础资源细粒度感知-资源管理-L2层：IT设备");
	    		}
	    	}else {
	    		sysBaseAPI.addLog("编辑IT设备", SysLogType.OPERATE, SysLogOperateType.UPDATE, null, remark, title != null ? title:"精确管控与供电制冷联动-基础资源细粒度感知-资源管理-L2层：IT设备");
	    	}
		}else if(url.equals("/asset/detailNode")) {
			sysBaseAPI.addLog("查看IT设备详情", SysLogType.OPERATE, SysLogOperateType.DETAIL, null, remark, title != null ? title:"精确管控与供电制冷联动-基础资源细粒度感知-资源管理-L2层：IT设备");
		}else if(url.equals("/asset/pageNode")) {
			sysBaseAPI.addLog(content != null?content:"查询服务器列表", SysLogType.OPERATE, SysLogOperateType.QUERY, null, remark, title != null ? title:"精确管控与供电制冷联动-基础资源细粒度感知-资源管理-L2层：IT设备");
		}else if(url.equals("/asset/syncDevice")) {
			sysBaseAPI.addLog("基础设施同步", SysLogType.OPERATE, SysLogOperateType.SYNC, null, remark, "精确管控与供电制冷联动-基础资源细粒度感知-资源管理-L1层：基础设施");
		}else if(url.equals("/asset/syncNodeSwitcher")) {
			sysBaseAPI.addLog("IT设备同步", SysLogType.OPERATE, SysLogOperateType.SYNC, null, remark, "精确管控与供电制冷联动-基础资源细粒度感知-资源管理-L2层：IT设备");
		}else if(url.equals("/asset/pageItDevice")) {
			sysBaseAPI.addLog(content != null?content:"查询IT设备列表", SysLogType.OPERATE, SysLogOperateType.QUERY, null, remark, title != null ? title:"精确管控与供电制冷联动-基础资源细粒度感知-资源管理-L2层：IT设备");
		}else if(url.equals("/asset/countItDevice")) {
			sysBaseAPI.addLog(request.getParameter("content") != null ? request.getParameter("content"): "查询IT设备列表", SysLogType.OPERATE, SysLogOperateType.DETAIL, null, remark, request.getParameter("title") != null ? request.getParameter("title"):"精确管控与供电制冷联动-基础资源细粒度感知-资源管理-L2层：IT设备");
		}else if(url.equals("/asset/listDevice")) {
			sysBaseAPI.addLog("查询IT设备列表", SysLogType.OPERATE, SysLogOperateType.QUERY, null, remark, title != null ? title:"精确管控与供电制冷联动-基础资源细粒度感知-资源管理-L2层：IT设备");
		}else if(url.equals("/cloud_resource/page")) {
	        if(title != null) {
				sysBaseAPI.addLog(content != null?content:"查询云平台资源列表" , SysLogType.OPERATE, SysLogOperateType.QUERY, null, remark, title != null ? title: "精确管控与供电制冷联动-基础资源细粒度感知-资源管理-L3层：云平台资源");
			}
		}else if(url.equals("/cloud_resource/create")) {
			StringBuffer buff = new StringBuffer();
	    	buff.append("新增云平台资源");
	    	// buff.append("，"+msg.toString());
			sysBaseAPI.addLog(buff.toString(), SysLogType.OPERATE, SysLogOperateType.ADD, null, remark, "精确管控与供电制冷联动-基础资源细粒度感知-资源管理-L3层：云平台资源");
		}else if(url.equals("/cloud_resource/delete")) {
			sysBaseAPI.addLog("删除云平台资源", null, CommonConstant.OPERATE_TYPE_DELETE);
		}else if(url.equals("/cloud_resource/detail")) {
			sysBaseAPI.addLog("查看云平台资源详情", SysLogType.OPERATE, SysLogOperateType.DETAIL, null, remark, "精确管控与供电制冷联动-基础资源细粒度感知-资源管理L3层-云平台资源");
		}else if(url.equals("/cloud_resource/vm_detail")) {
			sysBaseAPI.addLog("查看虚拟机详情", null, SysLogOperateType.DETAIL,null, remark, title);
		}else if(url.equals("/cloud_resource/k8s_detail")) {
			sysBaseAPI.addLog("查看容器详情", null, SysLogOperateType.DETAIL,null,   remark, title);
		}else if(url.equals("/cloud_resource/update1")) {
			StringBuffer buff = new StringBuffer();
	    	buff.append("更新云平台资源");
			sysBaseAPI.addLog(buff.toString(), SysLogType.OPERATE, SysLogOperateType.UPDATE, null, remark, "精确管控与供电制冷联动-基础资源细粒度感知-资源管理-L3层：云平台资源");
		}else if(url.equals("/cloud_resource/sync")) {
			sysBaseAPI.addLog("云平台资源同步", SysLogType.OPERATE, SysLogOperateType.SYNC, null, remark, "精确管控与供电制冷联动-基础资源细粒度感知-资源管理-L3层：云平台资源");
		}else if(url.equals("/cloud_resource/analysis")) {
			sysBaseAPI.addLog("查看云平台资源分析" , null, CommonConstant.OPERATE_TYPE_DETAIL);
		}else if(url.equals("/app/create")) {
			sysBaseAPI.addLog("新增应用资源", SysLogType.OPERATE, SysLogOperateType.ADD, null, remark,"精确管控与供电制冷联动-基础资源细粒度感知-资源管理-L4层：应用资源");
		}else if(url.equals("/app/delete")) {
			sysBaseAPI.addLog("删除应用" , SysLogType.OPERATE, SysLogOperateType.DELETE, null, remark ,"精确管控与供电制冷联动-基础资源细粒度感知-资源管理-L4层：应用资源");
		}else if(url.equals("/app/update1")) {
			sysBaseAPI.addLog("更新应用资源", SysLogType.OPERATE, SysLogOperateType.UPDATE, null, remark, "精确管控与供电制冷联动-基础资源细粒度感知-资源管理-L4层：应用资源");
		}else if(url.equals("/app/update_scheduler_config")) {
			sysBaseAPI.addLog("调度触发策略设置" , SysLogType.OPERATE, SysLogOperateType.UPDATE, null, remark, "多数据中心调度-动态调度机制-调度触发策略设置");
		}else if(url.equals("/app/page")) {
			if("应用资源详情".equals(content)){
				sysBaseAPI.addLog("查看应用资源详情；ID:" + UUID.randomUUID().toString(),SysLogType.OPERATE, SysLogOperateType.DETAIL, null, remark, "精确管控与供电制冷联动-基础资源细粒度感知-资源管理-L4层：应用资源");
			}else {
				sysBaseAPI.addLog(content != null?content:"查询应用资源列表", SysLogType.OPERATE, SysLogOperateType.QUERY, null, remark ,title != null ?title: "精确管控与供电制冷联动-基础资源细粒度感知-资源管理-L4层：应用资源");
			}
		}else if(url.equals("/app/sync")) {
			sysBaseAPI.addLog("应用同步", SysLogType.OPERATE, SysLogOperateType.SYNC, null, remark, "精精确管控与供电制冷联动-基础资源细粒度感知-资源管理-L4层：应用资源");
		}else if(url.equals("/profile/updateFeatureChoose")) {
			sysBaseAPI.addLog("设置应用业务特点", null, SysLogOperateType.UPDATE, null, remark, title);
		}else if(url.equals("/profile/updateSceneChoose")) {
			sysBaseAPI.addLog("设置应用场景特点" , null, SysLogOperateType.UPDATE, null, remark, title);
		}else if(url.equals("/profile/pageFeature")) {
			sysBaseAPI.addLog("查询业务应用画像业务特点管理列表" , SysLogType.OPERATE, SysLogOperateType.QUERY, null, remark);
		}else if(url.equals("/profile/pageScene")) {
			sysBaseAPI.addLog("查询业务应用画像业务特点管理列表" , SysLogType.OPERATE, SysLogOperateType.QUERY, null, remark);
		}else if(url.equals("/profile/pageAppProfile")) {
			if(StringUtils.isNotBlank(title)) {
				sysBaseAPI.addLog(content != null ? content : "查询应用画像列表", null, SysLogOperateType.QUERY, null, remark, title);
			}else {
				sysBaseAPI.addLog(content != null ? content : "查询应用画像列表", null, SysLogOperateType.QUERY, null, remark, title);
			}
		}else if(url.equals("/profile/countAppProfile")) {
			sysBaseAPI.addLog(content != null ? content : "查询应用画像列表", null, SysLogOperateType.DETAIL, null, remark, request.getParameter("title"));
		}else if(url.equals("/authority/listByClassification")) {
			if(title != null) {
				if("iaas".equals(request.getParameter("classification"))) {
					sysBaseAPI.addLog(content != null ? content :"查看L1层：基础设施权限", SysLogType.OPERATE, SysLogOperateType.DETAIL, null, remark, title);
				}else if("device".equals(request.getParameter("classification"))) {
					sysBaseAPI.addLog(content != null ? content :"查看L2层：IT设备权限", SysLogType.OPERATE, SysLogOperateType.DETAIL, null, remark,title);
				}else if("cloud".equals(request.getParameter("classification"))) {
					sysBaseAPI.addLog(content != null ? content :"查看L3层：云平台资源权限", SysLogType.OPERATE, SysLogOperateType.DETAIL, null, remark,title);
				}else if("app".equals(request.getParameter("classification"))) {
					sysBaseAPI.addLog(content != null ? content :"查看L4层：应用资源权限", SysLogType.OPERATE, SysLogOperateType.DETAIL, null, remark,title);
				}else {
					sysBaseAPI.addLog(content != null ? content :"查看资源权限", SysLogType.OPERATE, SysLogOperateType.DETAIL, null, remark,title);
				}
			}else {
				sysBaseAPI.addLog(content != null ? content :"查看权限", null, SysLogOperateType.DETAIL, null, remark, title);
			}
		}else if(url.equals("/authority/set1")) {
			String mytitle = "";
			if("air_condition".equals(request.getParameter("items[0].type")) || "distribution_box".equals(request.getParameter("items[0].type")) || "ups".equals(request.getParameter("items[0].type"))) {
				mytitle = "精确管控与供电制冷联动-基础资源细粒度感知-权限管理-L1层：基础设施";
			}else if("server".equals(request.getParameter("items[0].type")) || "switcher".equals(request.getParameter("items[0].type"))) {
				mytitle = "精确管控与供电制冷联动-基础资源细粒度感知-权限管理-L2层：IT设备";
			}else if("k8s".equals(request.getParameter("items[0].type")) || "vm".equals(request.getParameter("items[0].type"))) {
				mytitle = "精确管控与供电制冷联动-基础资源细粒度感知-权限管理-L3层：云平台资源";
			}else if("app".equals(request.getParameter("items[0].type"))) {
				mytitle = "精确管控与供电制冷联动-基础资源细粒度感知-权限管理-L4层：应用资源";
			}
			sysBaseAPI.addLog(content != null ? content :"设置权限", null, SysLogOperateType.UPDATE, null, remark ,title == null ? mytitle : title);
			
		}else if(url.equals("/dcEnergy/statisticsByYear")) {
			sysBaseAPI.addLog("查看数据中心统计-年度能耗统计", null, SysLogOperateType.DETAIL, null, remark, "系统管理-统计报表-数据中心统计-年度能耗统计");
		}else if(url.equals("/dcEnergy/statisticsByQuarter")) {
			sysBaseAPI.addLog("查看数据中心统计-季度能耗统计", null, SysLogOperateType.DETAIL, null, remark, "系统管理-统计报表-数据中心统计-季度能耗统计");
		}else if(url.equals("/dcEnergy/statisticsByMonth")) {
			sysBaseAPI.addLog("查看数据中心统计-月度能耗统计", null, SysLogOperateType.DETAIL, null, remark, "系统管理-统计报表-数据中心统计-月度能耗统");
		}else if(url.equals("/dcEnergy/reportByYear")) {
			sysBaseAPI.addLog("查看数据中心报表-年度能耗统计报表", null, SysLogOperateType.DETAIL, null, remark, "系统管理-统计报表-数据中心报表-年度能耗统计报表");
		}else if(url.equals("/dcEnergy/reportByQuarter")) {
			sysBaseAPI.addLog("查看数据中心报表-季度能耗统计报表", null, SysLogOperateType.DETAIL, null, remark, "系统管理-统计报表-数据中心报表-季度能耗统计报表");
		}else if(url.equals("/dcEnergy/reportByMonth")) {
			sysBaseAPI.addLog("查看数据中心统计-月度能耗统计", null, SysLogOperateType.DETAIL, null, remark);
		}else if(url.equals("/dataClassification/statisticSource")) {
			sysBaseAPI.addLog("查看数据归类 - 数据源占比", null, SysLogOperateType.DETAIL, null, remark);
		}else if(url.equals("/dataClassification/listCountTrend")) {
			sysBaseAPI.addLog("查看数据归类 - 趋势", null, SysLogOperateType.DETAIL, null, remark);
		}else if(url.equals("/dataClean/get1")) {
			sysBaseAPI.addLog("查看数据清洗策略", null, SysLogOperateType.DETAIL, null, remark, "精确管控与供电制冷联动-精确能耗管控-冗余数据清洗");
		}else if(url.equals("/dataClean/update1")) {
			sysBaseAPI.addLog("修改冗余数据清洗策略", null, SysLogOperateType.UPDATE, null, remark, "精确管控与供电制冷联动-精确能耗管控-冗余数据清洗");
		}else if(url.equals("/dataSecurity/set2")) {
			sysBaseAPI.addLog("设置数据安全策略", null, SysLogOperateType.UPDATE, null, remark, "精确管控与供电制冷联动-精确能耗管控-数据安全");
		}else if(url.equals("/dataSecurity/detail")) {
			sysBaseAPI.addLog("查看数据安全策略设置", null, SysLogOperateType.DETAIL, null, remark, "精确管控与供电制冷联动-精确能耗管控-数据安全");
		}else if(url.equals("/dataSecurity/pageBackup")) {
			sysBaseAPI.addLog("查询数据安全-备份记录列表列表", null, SysLogOperateType.QUERY, null, remark);
		}else if(url.equals("/model/setModelFactors")) {
			StringBuffer buff = new StringBuffer();
	    	buff.append("模型因子设置");
	    	buff.append("");
	    	// buff.append(msg.toString());
	    	// buff.append("，"+msg.toString());
	    	if(StringUtils.isNotBlank(title)) {
	    		sysBaseAPI.addLog(buff.toString(), null, SysLogOperateType.UPDATE, null, remark, title);
	    	}else {
	    		if("AIR".equals(request.getParameter("modelType"))) {
	    			sysBaseAPI.addLog(buff.toString(), null, SysLogOperateType.UPDATE, null, remark, "精确管控与供电制冷联动-供电制冷最优模型-供电制冷设备指标管理-制冷设备最有成本模型指标设置");
	    		}else {
	    			sysBaseAPI.addLog(buff.toString(), null, SysLogOperateType.UPDATE, null, remark, "精确管控与供电制冷联动-供电制冷最优模型-供电制冷设备指标管理-供电设备最有成本模型指标设置");
	    		}
	    		
	    	}
		}else if(url.equals("/model/getModelFactors")) {
			if(StringUtils.isNotBlank(title)) {
				sysBaseAPI.addLog(content != null?content:"查看模型因子", null, SysLogOperateType.DETAIL, null, remark,  title);
			}else {
				if("AIR".equals(request.getParameter("modelType"))) {
					sysBaseAPI.addLog(content != null?content:"查看模型因子", null, SysLogOperateType.DETAIL,null, remark, "精确管控与供电制冷联动-供电制冷最优模型-供电制冷设备指标管理-制冷设备最有成本模型指标设置");
				}else {
					sysBaseAPI.addLog(content != null?content:"查看模型因子", null, SysLogOperateType.DETAIL, null, remark,"精确管控与供电制冷联动-供电制冷最优模型-供电制冷设备指标管理-供电设备最有成本模型指标设置");
				}
			}
		}else if(url.equals("/model/pageModelScore")) {
			if(StringUtils.isNotBlank(title)) {
				sysBaseAPI.addLog(content != null?content:"设备模型评分列表", null, SysLogOperateType.QUERY, null, remark, title);
			}else {
				if(request.getParameter("modelType")!= null && request.getParameter("modelType").equals("UPS")) {
					sysBaseAPI.addLog(content != null?content:"设备模型评分列表", null, SysLogOperateType.QUERY, null, remark, "精确管控与供电制冷联动-供电制冷最优模型-设备最优成本模型总览-供电设备最优成本模型");
				}else if(request.getParameter("modelType")!= null && request.getParameter("modelType").equals("AIR")) {
					sysBaseAPI.addLog(content != null?content:"设备模型评分列表", null, SysLogOperateType.QUERY, null, remark, "精确管控与供电制冷联动-供电制冷最优模型-设备最优成本模型总览-制冷设备最优成本模型");
				}
			}
		}else if(url.equals("/model/detailModelScore")) {
			if(StringUtils.isNotBlank(title)) {
				sysBaseAPI.addLog("查看最优模型评分详情", null, SysLogOperateType.DETAIL, null, remark, title);
			}else {
				sysBaseAPI.addLog("查看迁移代价信息", null, SysLogOperateType.DETAIL, null, remark, "离线业务多数据中心迁移-离线业务异步迁移代价-代价展示-异步迁移代价计算结果展示");
			}
		}else if(url.equals("/model/pageDeviceScore")) {
			if(StringUtils.isNotBlank(title)) {
				sysBaseAPI.addLog("查询最优模型评分服务器列表", null, SysLogOperateType.QUERY, null, remark, title);
			}else {
				sysBaseAPI.addLog("查询最优模型评分服务器列表", null, SysLogOperateType.QUERY, null, remark, title);
			}
		}else if(url.equals("/model/bestPredict")) {
			sysBaseAPI.addLog(content != null ?content:"查看最优策略评估算法", null, SysLogOperateType.DETAIL, null, remark, title);
		}else if(url.equals("/scheduler/job/page")) {
			if(StringUtils.isNotBlank(title)) {
				sysBaseAPI.addLog(content != null?content:"查询调度任务列表", null, SysLogOperateType.QUERY, null, remark, title);
			}
		}else if(url.equals("/scheduler/step/list1")) {
			sysBaseAPI.addLog("查询调度步骤列表", null, SysLogOperateType.QUERY, null, remark,  "多数据中心调度-动态调度机制-调度过程监控");
		}else if(url.equals("/scheduler/datacenter_config/page")) {
			sysBaseAPI.addLog("查询调度触发策略设置列表", null, SysLogOperateType.QUERY,null, remark, "多数据中心调度-动态调度机制-调度触发策略设置");
		}else if(url.equals("/scheduler/update_scheduler1")) {
			StringBuffer buff = new StringBuffer();
	    	buff.append("设置多数据中心调度对象粒度");
	    	// buff.append(","+msg.toString());
			sysBaseAPI.addLog(buff.toString(), null, SysLogOperateType.UPDATE, null, remark);
		}else if(url.equals("/scheduler/scheduler_model_list")) {
			sysBaseAPI.addLog(content != null?content:"查看调度策略设置", null, SysLogOperateType.DETAIL, null, remark, title);
		}else if(url.equals("/scheduler/update_scheduler_model")) {
			StringBuffer buff = new StringBuffer();
	    	buff.append("修改调度策略");
			sysBaseAPI.addLog(buff.toString(), null, SysLogOperateType.UPDATE, null, remark, "多数据中心调度-调度模型管理-调度策略设置");
		}else if(url.equals("/scheduler/scheduler_model_tool_list")) {
			sysBaseAPI.addLog("查看异步迁移模型工具", null, SysLogOperateType.DETAIL, null, remark, "离线业务多数据中心迁移-异步模型迁移工具");
		}else if(url.equals("/scheduler/update_scheduler_model_tool")) {
			StringBuffer buff = new StringBuffer();
	    	buff.append("配置异步迁移模型工具");
			sysBaseAPI.addLog(buff.toString(), null, SysLogOperateType.UPDATE, null, remark, "离线业务多数据中心迁移-异步模型迁移工具");
		}else if(url.equals("/scheduler/update_scheduler_strategy_config")) {
			StringBuffer buff = new StringBuffer();
	    	buff.append("配置迁移策略");
			sysBaseAPI.addLog(buff.toString(), null, SysLogOperateType.UPDATE, null, remark);
		}else if(url.equals("/scheduler/scheduler_strategy_config_list")) {
			sysBaseAPI.addLog("查看迁移策略配置", null, SysLogOperateType.QUERY, null, remark);
		}else if(url.equals("/scheduler/update_scheduler_strategy_rule")) {
			StringBuffer buff = new StringBuffer();
	    	buff.append("修改迁移策略执行规则");
			sysBaseAPI.addLog(buff.toString(), null, SysLogOperateType.UPDATE, null, remark);
		}else if(url.equals("/scheduler/get_scheduler_strategy_rule")) {
			sysBaseAPI.addLog("查看迁移策略执行规则设置", null, SysLogOperateType.DETAIL, null, remark, "离线业务多数据中心迁移-多数据中心弹性迁移策略-迁移策略执行规则");
		}else if(url.equals("/scheduler/scheduler_effect_prediction")) {
			sysBaseAPI.addLog("查看调度效果预测", null, SysLogOperateType.DETAIL, null, remark);
		}else if(url.equals("/scheduler/scheduler_result_analysis")) {
			sysBaseAPI.addLog("查看调度结果偏差分析", null, SysLogOperateType.DETAIL, null, remark);
		}else if(url.equals("/scheduler/migrate_resource")) {
			sysBaseAPI.addLog("迁移资源", null, SysLogOperateType.UPDATE, null, remark, "精确管控与供电制冷联动-统一管理联动-业务隔离");
		}else if(url.equals("/fm/page")) {
	    	if(title != null) {
	    		sysBaseAPI.addLog("查询计算节点控制列表", null, SysLogOperateType.QUERY, null, remark, title);
	    	}else {
	    		sysBaseAPI.addLog("查询动态电压调频列表", null, SysLogOperateType.QUERY, null, remark, title);
	    	}
		}else if(url.equals("/fm/setStrategy")) {
			StringBuffer buff = new StringBuffer();
	    	buff.append("设置动态电压调频");
	    	// buff.append(","+msg.toString());
	    	if( title != null) {
	    		sysBaseAPI.addLog(buff.toString(), null, SysLogOperateType.UPDATE, null, remark, title);
	    	}else {
	    		sysBaseAPI.addLog(buff.toString(), null, SysLogOperateType.UPDATE, null, remark, title);
	    	}
		}else if(url.equals("/fm/detailDvfs")) {
			sysBaseAPI.addLog("查看动态电压调频详情", null, SysLogOperateType.DETAIL, null, remark);
		}else if(url.equals("/statistics/latestSample")) {
			if(!StringUtils.isBlank(title)) {
				if(title.contains("精确管控与供电制冷联动-基础资源细粒度感知-指标监控")) {
					if(request.getParameter("meter").equals("switcher.out_octets") || "server.power".equals(request.getParameter("meter"))) {
						sysBaseAPI.addLog("查看IT设备监控详情:", null, SysLogOperateType.DETAIL, null, remark,title+"-L2层：IT设备");
					}else if(request.getParameter("meter").equals("k8s.cpu_util") || "vm.cpu_util".equals(request.getParameter("meter"))) {
						sysBaseAPI.addLog("查看云平台资源监控详情", null, SysLogOperateType.DETAIL, null, remark,title+"-L3层：云平台资源");
					}else if(request.getParameter("meter").equals("container.memory_usage")) {
						sysBaseAPI.addLog("查看应用资源监控详情", null, SysLogOperateType.DETAIL, null, remark,title+"-L4层：应用资源");
					}
				}else if(title.contains("多数据中心调度-数据跨层感知与智能分析-L1层：基础设施")) {
					if(request.getParameter("meter").equals("environment.air_out_temperature") || "environment.ups".equals(request.getParameter("meter")) || "environment.distribution".equals(request.getParameter("meter"))) {
						sysBaseAPI.addLog(content != null?content :"监控详情", null, SysLogOperateType.DETAIL, null, remark,title);
					}
				}
			}else {
				sysBaseAPI.addLog(content != null?content :"监控详情", null, SysLogOperateType.DETAIL, null, remark,title);
			}
		}else if(url.equals("/statistics/latestSamples")) {
			if(StringUtils.isNotBlank(title)) {
				if(title.contains("精确管控与供电制冷联动-基础资源细粒度感知-指标监控")) {
					if(request.getParameter("meter").equals("environment.air")) {
						sysBaseAPI.addLog("监控详情", null, SysLogOperateType.DETAIL, null, remark,title+"-L1层：基础设施");
					}
				}
			}
		}else if(url.equals("/statistics/listMeter")) {
			sysBaseAPI.addLog("查看监控项列表", null, SysLogOperateType.QUERY, null, remark,"精确管控与供电制冷联动-基础资源细粒度感知-诊断告警-告警策略设置");
		}else if(url.equals("/sys/log/list")) {
			if (StringUtils.isBlank(title)) {
				// sysBaseAPI.addLog("查询易分析的操作日志列表",
				// CommonConstant.LOG_TYPE_OPERATE,
				// CommonConstant.OPERATE_TYPE_AUDIT_QUERY);
				sysBaseAPI.addLog("查询易分析的操作日志列表", SysLogType.OPERATE, SysLogOperateType.AUDIT_QUERY, null, remark, "系统管理-易分析日志");
			} else {
				if (title.equals("系统日志") || title.equals("业务日志")) {
					sysBaseAPI.addLog("查询日志管理的" + title + "列表" , SysLogType.OPERATE, SysLogOperateType.AUDIT_QUERY, null, remark, "系统管理-日志管理-" + title);

				} else {
					sysBaseAPI.addLog("查询易分析的" + title + "列表" , SysLogType.OPERATE, SysLogOperateType.AUDIT_QUERY, null, remark, "系统管理-易分析日志-" + title);
				}
			}
		}else if(url.equals("/sys/log/logStatistics")) {
			sysBaseAPI.addLog("查询日志统计列表", SysLogType.OPERATE, SysLogOperateType.LOG_STATISTICS, null, remark, "系统管理-日志统计");
		}else if(url.equals("/sys/log/columnarStatistics")) {
			
			sysBaseAPI.addLog("查看日志柱状统计", SysLogType.OPERATE, SysLogOperateType.LOG_STATISTICS, null, remark, "系统管理-日志统计");
		}else if(url.equals("/sys/log/config")) {
			sysBaseAPI.addLog("修改日志配置", SysLogType.OPERATE, SysLogOperateType.LOG_CONFIG, null, remark, "系统管理-日志配置");
			
		}else if(url.equals("/sys/log/deleteLogs")) {
			sysBaseAPI.addLog("删除业务日志", SysLogType.OPERATE, SysLogOperateType.LOG_DELETE, null, remark, "系统管理-日志管理-业务日志");
		}else if(url.equals("/sys/log/resume")) {
			sysBaseAPI.addLog("恢复日志", SysLogType.OPERATE, SysLogOperateType.resume, null, remark, "系统管理-日志管理-"+title);
		}else if(url.equals("/sys/log/backup")) {
			sysBaseAPI.addLog("备份日志", SysLogType.OPERATE, SysLogOperateType.BACKUP, null, remark, "系统管理-日志管理-"+title);
		}else if(url.equals("/sys/log/getConfig")) {
			sysBaseAPI.addLog("查看日志配置", SysLogType.OPERATE, SysLogOperateType.AUDIT_QUERY, null, remark, "系统管理-日志配置");
		}else if(url.equals("/sys/log/operate_type_list")) {
			sysBaseAPI.addLog("查看日志操作类型列表", SysLogType.OPERATE, SysLogOperateType.AUDIT_QUERY, null, remark,  title == null?"系统管理-日志配置":title);
		}else {
			//找不到url就记录默认的
//			PermissionsInfoVo vo = PermissionCodeCache.getByUrl(url);
//			Integer logType = SysLogUrlOperate.getOperateTypeByUrl(url);
//			if(vo != null) {
//				//获取title
//				if(StringUtils.isNotBlank(title)) {
//					sysBaseAPI.addLog(vo.getName(), SysLogType.OPERATE, SysLogOperateType.getByType(logType), null, SysLogUrlOperate.errorRemark ,title);
//				}else {
//					sysBaseAPI.addLog(vo.getName(), SysLogType.OPERATE,  SysLogOperateType.getByType(logType), null, SysLogUrlOperate.errorRemark, null);
//				}			
//			}
		}
		
	}
}
