package org.jeecg.common.util.sm;

import java.io.UnsupportedEncodingException;

import org.bouncycastle.pqc.math.linearalgebra.ByteUtils;

public class StrUtil {
	
	public static final String ENCODING = "utf-8";
	
	/**
	 * 16进制字符串转文本
	 * @param hexStr
	 * @return
	 */
	public static String hexToStr(String hexStr) {
		byte[] data = ByteUtils.fromHexString(hexStr);
		return new String(data);
	}

	/**
	 * 文本转16进制字符串
	 * @param data
	 * @return
	 * @throws UnsupportedEncodingException 
	 */
	public static String strToHex(String data) throws UnsupportedEncodingException {
		byte[] input = data.getBytes(ENCODING);
		String hexStr = ByteUtils.toHexString(input);
		return hexStr;
	}

}
