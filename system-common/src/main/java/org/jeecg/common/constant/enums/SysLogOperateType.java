package org.jeecg.common.constant.enums;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

import org.jeecg.common.constant.CommonConstant;

/**
 * 
 */
public enum SysLogOperateType {

	/**
	 * 查看
	 */
	DETAIL(CommonConstant.OPERATE_TYPE_DETAIL, "查看", SysLogType.OPERATE, false),
	/**
	 * 查询
	 */
	QUERY(CommonConstant.OPERATE_TYPE_LIST, "查询", SysLogType.OPERATE, false),
	/**
	 * 添加
	 */
	ADD(CommonConstant.OPERATE_TYPE_ADD, "新增", SysLogType.OPERATE, false),
	/**
	 * 修改
	 */
	UPDATE(CommonConstant.OPERATE_TYPE_UPDATE, "修改", SysLogType.OPERATE, false),
	/**
	 * 删除
	 */
	DELETE(CommonConstant.OPERATE_TYPE_DELETE, "删除", SysLogType.OPERATE, false),
	/**
	 * 导入
	 */
	IMPORT(5, "导入", null, false),
	/**
	 * 导出
	 */
	EXPORT(6, "导出", null, false),
	/**
	 * 运行
	 */
	RUN(7, "运行", SysLogType.RUN, null),
	/**
	 * 配置调试
	 */
	CONFIG_DEBUG(CommonConstant.OPERATE_TYPE_CONFIG_DEBUG, "配置调试", SysLogType.DEBUG, false),
	/**
	 * 越权
	 */
	UNAUTHORIZED(9, "越权", SysLogType.ALARM, true),
	/**
	 * 审计查询
	 */
	AUDIT_QUERY(CommonConstant.OPERATE_TYPE_AUDIT_QUERY, "审计查询", SysLogType.OPERATE, true),
	/**
	 * 配置
	 */
	CONFIG(CommonConstant.OPERATE_TYPE_CONFIG, "配置", SysLogType.OPERATE, false),
	/**
	 * 12 备份
	 */
	BACKUP(CommonConstant.OPERATE_TYPE_BACKUP, "备份", SysLogType.OPERATE, true),
	/**
	 * 恢复
	 */
	resume(CommonConstant.OPERATE_TYPE_RESUME, "恢复", SysLogType.OPERATE, true),
	/**
	 * 启动
	 */
	START(14, "启动", null, false),
	/**
	 * 停止
	 */
	STOP(15, "停止", null, false),
	/**
	 * 程序错误
	 */
	PROCEDURE_ERROR(16, "程序错误", SysLogType.ERROR, false),
	/**
	 * 配置错误
	 */
	CONFIG_ERROR(17, "配置错误", SysLogType.ERROR, false),
	/**
	 * 数据库错误
	 */
	DATABASE_ERROR(18, "数据库错误", SysLogType.ERROR, false),
	/**
	 * 容量
	 */
	CAPACITY(19, "容量", SysLogType.ALARM, false),

	/**
	 * 日志统计
	 */
	LOG_STATISTICS(CommonConstant.OPERATE_TYPE_LOG_STATISTICS, "日志统计", SysLogType.OPERATE, true),

	/**
	 * 日志配置
	 */
	LOG_CONFIG(CommonConstant.OPERATE_TYPE_LOG_CONFIG, "日志配置", SysLogType.OPERATE, true),

	/**
	 * 登录
	 */
	LOGIN(CommonConstant.OPERATE_TYPE_LOGIN, "登录", SysLogType.LOGIN, true),

	/**
	 * 注销
	 */
	LOGOUT(CommonConstant.OPERATE_TYPE_LOGOUT, "注销", SysLogType.LOGIN, true),

	/**
	 * 数据库调试
	 */
	DB_DEBUG(CommonConstant.OPERATE_TYPE_DB_DEBUG, "数据库调试", SysLogType.DEBUG, false),

	/**
	 * 程序调试
	 */
	PROCEDURE_DEBUG(CommonConstant.OPERATE_TYPE_PROCEDURE_DEBUG, "程序调试", SysLogType.DEBUG, false),

	/**
	 * 同步
	 */
	SYNC(CommonConstant.OPERATE_TYPE_SYNC, "同步", SysLogType.OPERATE, false),

	/**
	 * 同步
	 */
	LOG_DELETE(CommonConstant.OPERATE_TYPE_LOG_DELETE, "删除日志", SysLogType.OPERATE, true);
	
	/**
	 * 类型
	 */
	Integer type;

	/**
	 * 名称
	 */
	String name;

	// 日志类型
	SysLogType logType;

	Boolean isSystem;

	/**
	 * 构造器
	 *
	 * @param type
	 * @param code
	 * @param templatePath
	 * @param note
	 */
	SysLogOperateType(Integer type, String name, SysLogType logType, Boolean isSystem) {
		this.type = type;
		this.name = name;
		this.logType = logType;
		this.isSystem = isSystem;
	}

	public Integer getType() {
		return type;
	}

	public String getName() {
		return name;
	}

	public Boolean getIsSystem() {
		return isSystem;
	}

	public SysLogType getLogType() {
		return logType;
	}

	public static SysLogOperateType getByType(Integer type) {
		return Stream.of(SysLogOperateType.values()).filter(s -> s.getType() == type).findFirst().orElse(null);
	}

	public static List<SysLogOperateType> getByLogType(SysLogType logType) {
		List<SysLogOperateType> res = new ArrayList<>();

		Stream.of(SysLogOperateType.values()).filter(s -> s.getLogType() == logType).forEach(s -> res.add(s));
		return res;
	}

	public static List<SysLogOperateType> getByIsSystem(Boolean isSystem) {
		List<SysLogOperateType> res = new ArrayList<>();
		Stream.of(SysLogOperateType.values()).filter(s -> isSystem == s.getIsSystem()).forEach(s -> res.add(s));
		return res;
	}

	public static List<SysLogOperateType> getAll() {
		List<SysLogOperateType> list = new ArrayList<>();
		for (SysLogOperateType type : SysLogOperateType.values()) {
			list.add(type);
		}
		return list;
	}

	public static List<SysLogOperateType> getByLogTypes(List<Integer> includeOperateType) {
		if (includeOperateType == null) {
			return getAll();
		}
		List<SysLogOperateType> list = new ArrayList<>();
		for (SysLogOperateType type : SysLogOperateType.values()) {
			for (Integer typeReq : includeOperateType) {
				if (typeReq.equals(type.getType())) {
					list.add(type);
				}
			}
		}
		return list;
	}

	public static List<SysLogOperateType> getByLogTypeAndIsSystem(SysLogType lt, Boolean isSystem) {
		List<SysLogOperateType> list = new ArrayList<>();
		for (SysLogOperateType type : SysLogOperateType.values()) {
			if (type.getLogType() != null && type.getLogType().equals(lt) && type.getIsSystem() != null && type.getIsSystem().equals(isSystem)) {
				list.add(type);
			}
		}
		return list;
	}

}
