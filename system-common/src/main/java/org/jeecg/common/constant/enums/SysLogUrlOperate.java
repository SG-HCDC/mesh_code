package org.jeecg.common.constant.enums;

import java.util.HashMap;
import java.util.Map;

import org.jeecg.common.constant.CommonConstant;

import com.baomidou.mybatisplus.core.toolkit.StringUtils;

public class SysLogUrlOperate {
	public static final String  errorSecurity= "数据完整性被破坏";
	public static final String errorRemark= "参数验证失败";
	public static final String syncRemark = "同步失败";
	private static Map<String, Integer> map = new HashMap<>();
	private static Map<String, String> moduleMap = new HashMap<>();
	static {
		map.put("/sys/category/importExcel" ,CommonConstant.OPERATE_TYPE_LIST);
		map.put("/asset/createDevice" ,CommonConstant.OPERATE_TYPE_ADD);
		map.put("/sys/dictItem/add" ,CommonConstant.OPERATE_TYPE_ADD);
		map.put("/model/detailModelScore" ,CommonConstant.OPERATE_TYPE_DETAIL);
		map.put("/asset/deleteNode" ,CommonConstant.OPERATE_TYPE_DELETE);
		map.put("/sys/user/userDepartList" ,CommonConstant.OPERATE_TYPE_LIST);
		map.put("/scheduler/update_scheduler1" ,CommonConstant.OPERATE_TYPE_UPDATE);
		map.put("/sys/sysDepartPermission/importExcel" ,CommonConstant.OPERATE_TYPE_LIST);
		
		map.put("/dataClean/update1" ,CommonConstant.OPERATE_TYPE_UPDATE);
		map.put("/cloud_resource/create" ,CommonConstant.OPERATE_TYPE_ADD);
		map.put("/sys/position/exportXls" ,CommonConstant.OPERATE_TYPE_LIST);
		map.put("/sys/role/exportXls" ,CommonConstant.OPERATE_TYPE_LIST);
		map.put("/sys/selectDepart" ,CommonConstant.OPERATE_TYPE_LIST);
		map.put("/test/jeecgDemo/html" ,CommonConstant.OPERATE_TYPE_DETAIL);
		map.put("/sys/log/logStatistics" ,CommonConstant.OPERATE_TYPE_LOG_STATISTICS);
		map.put("/sys/permission/queryDepartPermission" ,CommonConstant.OPERATE_TYPE_LIST);
		map.put("/asset/pageNode" ,CommonConstant.OPERATE_TYPE_LIST);
		map.put("/sys/user/putRecycleBin" ,CommonConstant.OPERATE_TYPE_UPDATE);
		map.put("/sys/log/columnarStatistics" ,CommonConstant.OPERATE_TYPE_LOG_STATISTICS);
		map.put("/sys/checkRule/importExcel" ,CommonConstant.OPERATE_TYPE_LIST);
		map.put("/sys/dict/queryAllDictItems" ,CommonConstant.OPERATE_TYPE_LIST);
		map.put("/dataClassification/statisticSource" ,CommonConstant.OPERATE_TYPE_DETAIL);
		map.put("/sys/logout" ,CommonConstant.OPERATE_TYPE_LOGOUT);
		map.put("/dcEnergy/reportByYear" ,CommonConstant.OPERATE_TYPE_DETAIL);
		map.put("/sys/fillRule/exportXls" ,CommonConstant.OPERATE_TYPE_LIST);
		map.put("/scheduler/scheduler_model_tool_list" ,CommonConstant.OPERATE_TYPE_LIST);
		map.put("/dataSecurity/detail" ,CommonConstant.OPERATE_TYPE_DETAIL);
		map.put("/app/delete" ,CommonConstant.OPERATE_TYPE_DELETE);
		map.put("/sys/role/queryById" ,CommonConstant.OPERATE_TYPE_DETAIL);
		map.put("/sys/dict/loadDict/{dictCode}" ,CommonConstant.OPERATE_TYPE_DETAIL);
		map.put("/sys/user/deleteUserInDepartBatch" ,CommonConstant.OPERATE_TYPE_DELETE);
		map.put("/history/create" ,CommonConstant.OPERATE_TYPE_ADD);
		map.put("/test/joaDemo/importExcel" ,CommonConstant.OPERATE_TYPE_LIST);
		map.put("/sys/permission/getSystemSubmenu" ,CommonConstant.OPERATE_TYPE_DETAIL);
		
		map.put("/linkman/delete" ,CommonConstant.OPERATE_TYPE_DELETE);
		map.put("/analysis/pageNodeHistory" ,CommonConstant.OPERATE_TYPE_LIST);
		map.put("/sys/tenant/queryList" ,CommonConstant.OPERATE_TYPE_LIST);
		map.put("/sys/dictItem/deleteBatch" ,CommonConstant.OPERATE_TYPE_DELETE);
		map.put("/asset/pageDevice" ,CommonConstant.OPERATE_TYPE_LIST);
		map.put("/sys/annountCement/exportXls" ,CommonConstant.OPERATE_TYPE_LIST);
		map.put("/sys/dataLog/queryCompareList" ,CommonConstant.OPERATE_TYPE_LIST);
		map.put("/sys/dict/loadTreeData" ,CommonConstant.OPERATE_TYPE_LIST);
		
		map.put("/scheduler/update_scheduler_model" ,CommonConstant.OPERATE_TYPE_UPDATE);
		map.put("/sys/annountCement/listByUser" ,CommonConstant.OPERATE_TYPE_LIST);
		map.put("/model/setStrategy" ,CommonConstant.OPERATE_TYPE_UPDATE);
		map.put("/sys/quartzJob/deleteBatch" ,CommonConstant.OPERATE_TYPE_DELETE);
		map.put("/scheduler/get_scheduler_strategy_rule" ,CommonConstant.OPERATE_TYPE_DETAIL);
		map.put("/sys/user/queryUserRole" ,CommonConstant.OPERATE_TYPE_LIST);
		map.put("/sys/dict/deleteList" ,CommonConstant.OPERATE_TYPE_DELETE);
		map.put("/linkman/update1" ,CommonConstant.OPERATE_TYPE_UPDATE);
		map.put("/sys/sysDepart/searchBy" ,CommonConstant.OPERATE_TYPE_LIST);
		map.put("/linkman/page" ,CommonConstant.OPERATE_TYPE_LIST);
		map.put("/policy/addPolicyLinkman" ,CommonConstant.OPERATE_TYPE_ADD);
		map.put("/sys/category/loadAllData" ,CommonConstant.OPERATE_TYPE_DETAIL);
		
		map.put("/sys/tenant/edit" ,CommonConstant.OPERATE_TYPE_UPDATE);
		map.put("/sys/dict/deleteBatch" ,CommonConstant.OPERATE_TYPE_DELETE);
		map.put("/sys/dictItem/edit" ,CommonConstant.OPERATE_TYPE_UPDATE);
		map.put("/sys/tenant/delete" ,CommonConstant.OPERATE_TYPE_DELETE);
		map.put("/policy/update1" ,CommonConstant.OPERATE_TYPE_UPDATE);
		map.put("/sys/role/deleteBatch" ,CommonConstant.OPERATE_TYPE_DETAIL);
		
		map.put("/supplier/list" ,CommonConstant.OPERATE_TYPE_LIST);
		map.put("/asset/listDevice" ,CommonConstant.OPERATE_TYPE_LIST);
		map.put("/sys/log/getConfig" ,CommonConstant.OPERATE_TYPE_DETAIL);
		map.put("/sys/user/edit" ,CommonConstant.OPERATE_TYPE_UPDATE);
		map.put("/asset/createSwitcher" ,CommonConstant.OPERATE_TYPE_ADD);
		map.put("/sys/role/queryall" ,CommonConstant.OPERATE_TYPE_LIST);
		map.put("/sys/permission/getPermRuleListByPermId" ,CommonConstant.OPERATE_TYPE_DETAIL);
		map.put("/sys/role/edit" ,CommonConstant.OPERATE_TYPE_UPDATE);
		map.put("/online/cgreport/api/exportXls/{reportId}" ,CommonConstant.OPERATE_TYPE_DETAIL);
		map.put("/cloud_resource/update1" ,CommonConstant.OPERATE_TYPE_UPDATE);
		map.put("/asset/listDatacenter" ,CommonConstant.OPERATE_TYPE_LIST);
		map.put("/dataClassification/listCountTrend" ,CommonConstant.OPERATE_TYPE_LIST);
		map.put("/sys/sysDepartRole/deptRoleUserAdd" ,CommonConstant.OPERATE_TYPE_UPDATE);
		map.put("/sys/permission/add" ,CommonConstant.OPERATE_TYPE_ADD);
		map.put("/supplier/delete" ,CommonConstant.OPERATE_TYPE_DELETE);
		map.put("/sys/sysUserAgent/importExcel" ,CommonConstant.OPERATE_TYPE_LIST);
		map.put("/analysis/doSimulate" ,CommonConstant.OPERATE_TYPE_DETAIL);
		map.put("/scheduler/scheduler_model_list" ,CommonConstant.OPERATE_TYPE_LIST);
		map.put("/sys/category/loadTreeData" ,CommonConstant.OPERATE_TYPE_LIST);
		map.put("/scheduler/datacenter_config/page" ,CommonConstant.OPERATE_TYPE_LIST);
		map.put("/sys/user/deleteBatch" ,CommonConstant.OPERATE_TYPE_DELETE);
		map.put("/sys/permission/queryPermissionRule" ,CommonConstant.OPERATE_TYPE_LIST);
		
		map.put("/scheduler/job/page" ,CommonConstant.OPERATE_TYPE_LIST);
		map.put("/rule/create" ,CommonConstant.OPERATE_TYPE_ADD);
		map.put("/sys/user/list" ,CommonConstant.OPERATE_TYPE_LIST);
		map.put("/dataClean/get1" ,CommonConstant.OPERATE_TYPE_DETAIL);
		map.put("/analysis/doFitter" ,CommonConstant.OPERATE_TYPE_DETAIL);
		
		map.put("/sys/sysDepart/add" ,CommonConstant.OPERATE_TYPE_ADD);
		map.put("/profile/pageFeature" ,CommonConstant.OPERATE_TYPE_LIST);
		map.put("/sys/sysDepart/exportXls" ,CommonConstant.OPERATE_TYPE_LIST);
		map.put("/sys/annountCement/doReleaseData" ,CommonConstant.OPERATE_TYPE_DETAIL);
		map.put("/isc/get_isc_url" ,CommonConstant.OPERATE_TYPE_DETAIL);
		map.put("/profile/updateSceneChoose" ,CommonConstant.OPERATE_TYPE_CONFIG);
		map.put("/subhealth/listDatacenter" ,CommonConstant.OPERATE_TYPE_LIST);
		map.put("/sys/fillRule/importExcel" ,CommonConstant.OPERATE_TYPE_LIST);
		
		map.put("/asset/detailDevice" ,CommonConstant.OPERATE_TYPE_DETAIL);
		map.put("/sys/sysDepart/importExcel" ,CommonConstant.OPERATE_TYPE_LIST);
		map.put("/linkman/create" ,CommonConstant.OPERATE_TYPE_ADD);
		map.put("/sys/annountCement/syncNotic" ,CommonConstant.OPERATE_TYPE_DETAIL);
		map.put("/history/list" ,CommonConstant.OPERATE_TYPE_LIST);
		map.put("/sys/dataSource/importExcel" ,CommonConstant.OPERATE_TYPE_LIST);
		map.put("/dcEnergy/reportByMonth" ,CommonConstant.OPERATE_TYPE_DETAIL);
		map.put("/dcEnergy/statisticsByYear" ,CommonConstant.OPERATE_TYPE_DETAIL);
		map.put("/sys/user/userRoleList" ,CommonConstant.OPERATE_TYPE_LIST);
		map.put("/asset/createNode" ,CommonConstant.OPERATE_TYPE_ADD);
		map.put("/subhealth/pageDatacenter" ,CommonConstant.OPERATE_TYPE_LIST);
		map.put("/test/jeecgDemo/exportXls" ,CommonConstant.OPERATE_TYPE_LIST);
		map.put("/sys/user/importExcel" ,CommonConstant.OPERATE_TYPE_LIST);
		map.put("/subhealth/alertStatistic" ,CommonConstant.OPERATE_TYPE_LIST);
		map.put("/sys/role/checkRoleCode" ,CommonConstant.OPERATE_TYPE_DETAIL);
		map.put("/scheduler/update_scheduler_strategy_config" ,CommonConstant.OPERATE_TYPE_UPDATE);
		
		map.put("/sys/sysDepart/edit" ,CommonConstant.OPERATE_TYPE_UPDATE);
		map.put("/scheduler/step/list1" ,CommonConstant.OPERATE_TYPE_LIST);
		map.put("/asset/detailNode" ,CommonConstant.OPERATE_TYPE_DETAIL);
		map.put("/fm/setStrategy" ,CommonConstant.OPERATE_TYPE_UPDATE);
		map.put("/history/page" ,CommonConstant.OPERATE_TYPE_LIST);
		map.put("/sys/permission/deletePermissionRule" ,CommonConstant.OPERATE_TYPE_DELETE);
		map.put("/model/migrate_k8s" ,CommonConstant.OPERATE_TYPE_UPDATE);
		map.put("/sys/dictItem/list" ,CommonConstant.OPERATE_TYPE_LIST);
		map.put("/asset/pageDatacenter" ,CommonConstant.OPERATE_TYPE_LIST);
		map.put("/sys/user/updatePassword" ,CommonConstant.OPERATE_TYPE_UPDATE);
		map.put("/sys/role/add" ,CommonConstant.OPERATE_TYPE_ADD);
		map.put("/sys/log/backup" ,CommonConstant.OPERATE_TYPE_BACKUP);
		
		map.put("/sys/user/queryById" ,CommonConstant.OPERATE_TYPE_DETAIL);
		map.put("/sys/log/config" ,CommonConstant.OPERATE_TYPE_UPDATE);
		map.put("/test/jeecgDemo/importExcel" ,CommonConstant.OPERATE_TYPE_LIST);
		map.put("/supplier/update" ,CommonConstant.OPERATE_TYPE_UPDATE);
		map.put("/model/vm_detail" ,CommonConstant.OPERATE_TYPE_DETAIL);
		map.put("/sys/annountCement/edit" ,CommonConstant.OPERATE_TYPE_UPDATE);
		map.put("/sys/log/resume" ,CommonConstant.OPERATE_TYPE_RESUME);
		map.put("/scheduler/migrate_resource" ,CommonConstant.OPERATE_TYPE_UPDATE);
		map.put("/sys/category/loadTreeRoot" ,CommonConstant.OPERATE_TYPE_LIST);
		
		map.put("/subhealth/listThreshold" ,CommonConstant.OPERATE_TYPE_LIST);
		map.put("/model/detailDevice" ,CommonConstant.OPERATE_TYPE_DETAIL);
		map.put("/sys/dict/exportXls" ,CommonConstant.OPERATE_TYPE_LIST);
		map.put("/sys/log/delete" ,CommonConstant.OPERATE_TYPE_DELETE);
		map.put("/scheduler/migrate_k8s" ,CommonConstant.OPERATE_TYPE_UPDATE);
		map.put("/cloud_resource/detail" ,CommonConstant.OPERATE_TYPE_DETAIL);
		map.put("/asset/countItDevice" ,CommonConstant.OPERATE_TYPE_DETAIL);
		map.put("/sys/sysDepart/delete" ,CommonConstant.OPERATE_TYPE_DELETE);
		map.put("/statistics/latestSample" ,CommonConstant.OPERATE_TYPE_DETAIL);
		map.put("/app/update_scheduler_config" ,CommonConstant.OPERATE_TYPE_UPDATE);
		map.put("/scheduler/scheduler_strategy_config_list" ,CommonConstant.OPERATE_TYPE_LIST);
		map.put("/sys/log/list" ,CommonConstant.OPERATE_TYPE_LIST);
		
		map.put("/supplier/create" ,CommonConstant.OPERATE_TYPE_ADD);
		map.put("/sys/permission/deleteBatch" ,CommonConstant.OPERATE_TYPE_DELETE);
		map.put("/sys/ng-alain/getDictItemsByTable/{table}/{key}/{value}" ,CommonConstant.OPERATE_TYPE_DETAIL);
		map.put("/sys/sysDepartRole/importExcel" ,CommonConstant.OPERATE_TYPE_UPDATE);
		map.put("/asset/updateSwitcher" ,CommonConstant.OPERATE_TYPE_UPDATE);
		map.put("/sys/user/exportXls" ,CommonConstant.OPERATE_TYPE_LIST);
		map.put("/sys/sysDepartRole/getDeptRoleByUserId" ,CommonConstant.OPERATE_TYPE_DETAIL);
		map.put("/rule/delete" ,CommonConstant.OPERATE_TYPE_DELETE);
		map.put("/sys/user/addSysUserRole" ,CommonConstant.OPERATE_TYPE_ADD);
		map.put("/sys/checkCaptcha" ,CommonConstant.OPERATE_TYPE_DETAIL);
		map.put("/sys/log/deleteBatch" ,CommonConstant.OPERATE_TYPE_DELETE);
		map.put("/sys/permission/list" ,CommonConstant.OPERATE_TYPE_LIST);
		map.put("/sys/tenant/deleteBatch" ,CommonConstant.OPERATE_TYPE_DELETE);
		map.put("/sys/sysDepartPermission/saveDeptRolePermission" ,CommonConstant.OPERATE_TYPE_ADD);
		map.put("/isc/permissions_compare" ,CommonConstant.OPERATE_TYPE_DETAIL);
		map.put("/sys/quartzJob/list" ,CommonConstant.OPERATE_TYPE_LIST);
		map.put("/asset/pageItDevice" ,CommonConstant.OPERATE_TYPE_LIST);
		map.put("/profile/pageScene" ,CommonConstant.OPERATE_TYPE_LIST);
		map.put("/dcEnergy/statisticsByQuarter" ,CommonConstant.OPERATE_TYPE_LIST);
		
		map.put("/sys/log/operate_type_list" ,CommonConstant.OPERATE_TYPE_LIST);
		map.put("/sm/saveSm4" ,CommonConstant.OPERATE_TYPE_ADD);
		map.put("/sys/user/frozenBatch" ,CommonConstant.OPERATE_TYPE_UPDATE);
		map.put("/sys/quartzJob/queryById" ,CommonConstant.OPERATE_TYPE_DETAIL);
		map.put("/sys/user/delete" ,CommonConstant.OPERATE_TYPE_DELETE);
		map.put("/sys/position/importExcel" ,CommonConstant.OPERATE_TYPE_LIST);
		map.put("/cloud_resource/k8s_detail" ,CommonConstant.OPERATE_TYPE_DETAIL);
		map.put("/profile/countAppProfile" ,CommonConstant.OPERATE_TYPE_DETAIL);
		map.put("/sys/quartzJob/exportXls" ,CommonConstant.OPERATE_TYPE_DETAIL);
		map.put("/sys/dict/deletePhysic/{id}" ,CommonConstant.OPERATE_TYPE_DELETE);
		map.put("/sys/dict/importExcel" ,CommonConstant.OPERATE_TYPE_LIST);
		map.put("/sys/checkRule/exportXls" ,CommonConstant.OPERATE_TYPE_LIST);
		map.put("/sys/permission/getSystemMenuList" ,CommonConstant.OPERATE_TYPE_LIST);
		map.put("/sys/user/deleteUserRoleBatch" ,CommonConstant.OPERATE_TYPE_DELETE);
		map.put("/sys/dataLog/list" ,CommonConstant.OPERATE_TYPE_LIST);
		map.put("/sys/annountCement/deleteBatch" ,CommonConstant.OPERATE_TYPE_DELETE);
		map.put("/fm/page" ,CommonConstant.OPERATE_TYPE_LIST);
		map.put("/sys/user/getCurrentUserDeparts" ,CommonConstant.OPERATE_TYPE_DETAIL);
		map.put("/supplier/getByClusterId" ,CommonConstant.OPERATE_TYPE_DETAIL);
		map.put("/sys/annountCement/list" ,CommonConstant.OPERATE_TYPE_LIST);
		map.put("/asset/deleteDatacenter" ,CommonConstant.OPERATE_TYPE_DELETE);
		map.put("/scheduler/update_scheduler_model_tool" ,CommonConstant.OPERATE_TYPE_UPDATE);
		map.put("/sys/permission/editPermissionRule" ,CommonConstant.OPERATE_TYPE_UPDATE);
		map.put("/sys/user/deleteRecycleBin" ,CommonConstant.OPERATE_TYPE_DELETE);
		map.put("/asset/detailSwitcher" ,CommonConstant.OPERATE_TYPE_DETAIL);
		map.put("/sys/checkLogin" ,CommonConstant.LOG_TYPE_LOGIN);
		map.put("/sys/tenant/queryById" ,CommonConstant.OPERATE_TYPE_DETAIL);
		map.put("/model/detailNode" ,CommonConstant.OPERATE_TYPE_DETAIL);
		map.put("/sys/role/list" ,CommonConstant.OPERATE_TYPE_LIST);
		map.put("/scheduler/scheduler_result_analysis" ,CommonConstant.OPERATE_TYPE_DETAIL);
		map.put("/test/jeecgOrderMain/exportXls" ,CommonConstant.OPERATE_TYPE_LIST);
		map.put("/sys/permission/saveDepartPermission" ,CommonConstant.OPERATE_TYPE_ADD);
		map.put("/linkman/detail" ,CommonConstant.OPERATE_TYPE_DETAIL);
		map.put("/model/setModelFactors" ,CommonConstant.OPERATE_TYPE_UPDATE);
		map.put("/sys/sysDepartRole/exportXls" ,CommonConstant.OPERATE_TYPE_LIST);
		map.put("/sys/annountCement/add" ,CommonConstant.OPERATE_TYPE_ADD);
		map.put("/profile/pageAppProfile" ,CommonConstant.OPERATE_TYPE_LIST);
		map.put("/sys/annountCement/delete" ,CommonConstant.OPERATE_TYPE_DELETE);
		map.put("/subhealth/listAlertLevel" ,CommonConstant.OPERATE_TYPE_LIST);
		map.put("/sys/tenant/list" ,CommonConstant.OPERATE_TYPE_LIST);
		map.put("/sys/dict/refleshCache" ,CommonConstant.OPERATE_TYPE_UPDATE);
		map.put("/sys/sysUserAgent/exportXls" ,CommonConstant.OPERATE_TYPE_LIST);
		map.put("/subhealth/pageAlert" ,CommonConstant.OPERATE_TYPE_LIST);
		map.put("/asset/deleteSwitcher" ,CommonConstant.OPERATE_TYPE_DELETE);
		map.put("/sys/login" ,CommonConstant.LOG_TYPE_LOGIN);
		map.put("/sys/dict/add" ,CommonConstant.OPERATE_TYPE_ADD);
		
		map.put("/asset/updateNode" ,CommonConstant.OPERATE_TYPE_UPDATE);
		map.put("/sys/tenant/add" ,CommonConstant.OPERATE_TYPE_ADD);
		map.put("/sm/getSm2PublicKey" ,CommonConstant.OPERATE_TYPE_DETAIL);
		map.put("/dcEnergy/reportByQuarter" ,CommonConstant.OPERATE_TYPE_LIST);
		map.put("/sys/annountCement/doReovkeData" ,CommonConstant.OPERATE_TYPE_UPDATE);
		map.put("/scheduler/scheduler_effect_prediction" ,CommonConstant.OPERATE_TYPE_UPDATE);
		map.put("/subhealth/setThreshold" ,CommonConstant.OPERATE_TYPE_UPDATE);
		
		map.put("/sys/duplicate/check" ,CommonConstant.OPERATE_TYPE_DETAIL);
		map.put("/test/jeecgOrderMain/importExcel" ,CommonConstant.OPERATE_TYPE_UPDATE);
		map.put("/cloud_resource/vm_detail" ,CommonConstant.OPERATE_TYPE_DETAIL);
		
		map.put("/sys/dict/list" ,CommonConstant.OPERATE_TYPE_LIST);
		map.put("/linkman/list1" ,CommonConstant.OPERATE_TYPE_LIST);
		map.put("/step/list1" ,CommonConstant.OPERATE_TYPE_LIST);
		map.put("/sys/quartzJob/add" ,CommonConstant.OPERATE_TYPE_ADD);
		map.put("/subhealth/countAlerts" ,CommonConstant.OPERATE_TYPE_DETAIL);
		map.put("/sys/quartzJob/edit" ,CommonConstant.OPERATE_TYPE_UPDATE);
		map.put("/analysis/pageNodeSimulate" ,CommonConstant.OPERATE_TYPE_LIST);
		map.put("/policy/delete" ,CommonConstant.OPERATE_TYPE_DELETE);
		map.put("/sys/user/queryUserByDepId" ,CommonConstant.OPERATE_TYPE_LIST);
		map.put("/app/page" ,CommonConstant.OPERATE_TYPE_LIST);
		map.put("/sys/dict/back/{id}" ,CommonConstant.OPERATE_TYPE_DETAIL);
		map.put("/statistics/statistics" ,CommonConstant.OPERATE_TYPE_LIST);
		map.put("/sys/permission/queryListAsync" ,CommonConstant.OPERATE_TYPE_LIST);
		map.put("/scheduler/migrate_vm" ,CommonConstant.OPERATE_TYPE_UPDATE);
		map.put("/sys/sysDepartPermission/queryDeptRolePermission" ,CommonConstant.OPERATE_TYPE_LIST);
		
		map.put("/sys/dataLog/queryDataVerList" ,CommonConstant.OPERATE_TYPE_LIST);
		map.put("/sys/role/queryTreeList" ,CommonConstant.OPERATE_TYPE_LIST);
		map.put("/dataSecurity/set2" ,CommonConstant.OPERATE_TYPE_UPDATE);
		map.put("/analysis/pageNodeFitter" ,CommonConstant.OPERATE_TYPE_LIST);
		map.put("/statistics/latestSamples" ,CommonConstant.OPERATE_TYPE_DETAIL);
		map.put("/sys/sysDepart/queryIdTree" ,CommonConstant.OPERATE_TYPE_LIST);
		map.put("/app/sync" ,CommonConstant.OPERATE_TYPE_SYNC);
		map.put("/sys/permission/getUserPermissionByToken" ,CommonConstant.OPERATE_TYPE_LIST);
		map.put("/statistics/listMeter" ,CommonConstant.OPERATE_TYPE_LIST);
		map.put("/model/pageModelScore" ,CommonConstant.OPERATE_TYPE_LIST);
		
		map.put("/sys/permission/edit" ,CommonConstant.OPERATE_TYPE_UPDATE);
		map.put("/sys/user/add" ,CommonConstant.OPERATE_TYPE_ADD);
		map.put("/sys/ng-alain/getDictItems/{dictCode}" ,CommonConstant.OPERATE_TYPE_LIST);
		map.put("/sys/category/exportXls" ,CommonConstant.OPERATE_TYPE_LIST);
		map.put("/sys/user/deleteUserRole" ,CommonConstant.OPERATE_TYPE_DELETE);
		
		map.put("/dataSecurity/pageBackup" ,CommonConstant.OPERATE_TYPE_LIST);
		map.put("/model/latestSamples" ,CommonConstant.OPERATE_TYPE_DETAIL);
		map.put("/fm/detailDvfs" ,CommonConstant.OPERATE_TYPE_DETAIL);
		map.put("/sys/permission/queryRolePermission" ,CommonConstant.OPERATE_TYPE_LIST);
		map.put("/sys/user/editSysDepartWithUser" ,CommonConstant.OPERATE_TYPE_UPDATE);
		map.put("/sys/role/importExcel" ,CommonConstant.OPERATE_TYPE_ADD);
		
		map.put("/asset/pageSwitcher" ,CommonConstant.OPERATE_TYPE_LIST);
		map.put("/test/joaDemo/exportXls" ,CommonConstant.OPERATE_TYPE_LIST);
		map.put("/sys/user/queryByIds" ,CommonConstant.OPERATE_TYPE_LIST);
		map.put("/sys/dict/getDictText/{dictCode}/{key}" ,CommonConstant.OPERATE_TYPE_DETAIL);
			
		map.put("/sys/sysDepart/queryMyDeptTreeList" ,CommonConstant.OPERATE_TYPE_LIST);
		map.put("/sys/permission/addPermissionRule" ,CommonConstant.OPERATE_TYPE_ADD);
		map.put("/analysis/optimum" ,CommonConstant.OPERATE_TYPE_DETAIL);
		
		map.put("/sys/category/loadTreeChildren" ,CommonConstant.OPERATE_TYPE_DETAIL);
		map.put("/sys/dataSource/exportXls" ,CommonConstant.OPERATE_TYPE_UPDATE);
		map.put("/asset/updateDatacenter" ,CommonConstant.OPERATE_TYPE_UPDATE);
		map.put("/sys/ng-alain/getAppData" ,CommonConstant.OPERATE_TYPE_DETAIL);
		map.put("/authority/listByClassification" ,CommonConstant.OPERATE_TYPE_LIST);
		map.put("/policy/deletePolicyLinkman" ,CommonConstant.OPERATE_TYPE_DELETE);
		
		map.put("/policy/create" ,CommonConstant.OPERATE_TYPE_ADD);
		map.put("/cloud_resource/analysis" ,CommonConstant.OPERATE_TYPE_LIST);
		map.put("/sys/dict/delete" ,CommonConstant.OPERATE_TYPE_DELETE);
		map.put("/sys/dict/getDictItems/{dictCode}" ,CommonConstant.OPERATE_TYPE_LIST);
		map.put("/sys/category/loadOne" ,CommonConstant.OPERATE_TYPE_DETAIL);
		map.put("/model/migrate_vm" ,CommonConstant.OPERATE_TYPE_UPDATE);
		map.put("/asset/updateDevice" ,CommonConstant.OPERATE_TYPE_UPDATE);
		map.put("/sys/sysDepart/queryTreeByKeyWord" ,CommonConstant.OPERATE_TYPE_LIST);
		map.put("/model/getModelFactors" ,CommonConstant.OPERATE_TYPE_DETAIL);
		map.put("/subhealth/pageAir" ,CommonConstant.OPERATE_TYPE_LIST);
		map.put("/sys/role/delete" ,CommonConstant.OPERATE_TYPE_DELETE);
		
		map.put("/sys/common/pdf/pdfPreviewIframe" ,CommonConstant.OPERATE_TYPE_DETAIL);
		map.put("/subhealth/listThresholdMeter" ,CommonConstant.OPERATE_TYPE_LIST);
		map.put("/sys/permission/saveRolePermission" ,CommonConstant.OPERATE_TYPE_UPDATE);
		map.put("/sys/dict/edit" ,CommonConstant.OPERATE_TYPE_UPDATE);
		map.put("/authority/set1" ,CommonConstant.OPERATE_TYPE_UPDATE);
		map.put("/sys/user/departUserList" ,CommonConstant.OPERATE_TYPE_LIST);
		map.put("/isc/get_db_status" ,CommonConstant.OPERATE_TYPE_DETAIL);
		map.put("/sys/sysDepart/deleteBatch" ,CommonConstant.OPERATE_TYPE_DELETE);
		map.put("/sys/quartzJob/importExcel" ,CommonConstant.OPERATE_TYPE_ADD);
		
		map.put("/policy/page" ,CommonConstant.OPERATE_TYPE_LIST);
		map.put("/sys/dict/loadDictItem/{dictCode}" ,CommonConstant.OPERATE_TYPE_DETAIL);
		map.put("/subhealth/pageUps" ,CommonConstant.OPERATE_TYPE_LIST);
		map.put("/dcEnergy/statisticsByMonth" ,CommonConstant.OPERATE_TYPE_DETAIL);
		map.put("/asset/syncDevice" ,CommonConstant.OPERATE_TYPE_SYNC);
		map.put("/cloud_resource/delete" ,CommonConstant.OPERATE_TYPE_DELETE);
		map.put("/sys/category/loadDictItem" ,CommonConstant.OPERATE_TYPE_LIST);
		map.put("/statistics/requestStatistics" ,CommonConstant.OPERATE_TYPE_LIST);
		map.put("/app/update1" ,CommonConstant.OPERATE_TYPE_UPDATE);
		map.put("/sys/quartzJob/delete" ,CommonConstant.OPERATE_TYPE_DELETE);
		map.put("/sys/user/changePassword" ,CommonConstant.OPERATE_TYPE_UPDATE);
		map.put("/policy/detail" ,CommonConstant.OPERATE_TYPE_DETAIL);
		
		map.put("/sys/user/appEdit" ,CommonConstant.OPERATE_TYPE_UPDATE);
		map.put("/model/k8s_detail" ,CommonConstant.OPERATE_TYPE_DETAIL);
		map.put("/sys/permission/queryTreeList" ,CommonConstant.OPERATE_TYPE_LIST);
		map.put("/sys/dict/treeList" ,CommonConstant.OPERATE_TYPE_LIST);
		map.put("/asset/listNode" ,CommonConstant.OPERATE_TYPE_LIST);
		map.put("/profile/updateFeatureChoose" ,CommonConstant.OPERATE_TYPE_CONFIG);
		map.put("/supplier/get" ,CommonConstant.OPERATE_TYPE_DETAIL);
		map.put("/model/bestPredict" ,CommonConstant.OPERATE_TYPE_DETAIL);
		
		map.put("/cloud_resource/sync" ,CommonConstant.OPERATE_TYPE_SYNC);
		map.put("/cloud_resource/page" ,CommonConstant.OPERATE_TYPE_LIST);
		map.put("/analysis/pageAirFitter" ,CommonConstant.OPERATE_TYPE_LIST);
		map.put("/sys/sysDepartRole/getDeptRoleList" ,CommonConstant.OPERATE_TYPE_LIST);
		map.put("/sys/user/generateUserId" ,CommonConstant.OPERATE_TYPE_DETAIL);
		map.put("/model/powerRate" ,CommonConstant.OPERATE_TYPE_DETAIL);
		map.put("/sys/dictItem/delete" ,CommonConstant.OPERATE_TYPE_DELETE);
		map.put("/sys/user/checkOnlyUser" ,CommonConstant.OPERATE_TYPE_DETAIL);
		map.put("/sys/sysDepart/queryTreeList" ,CommonConstant.OPERATE_TYPE_LIST);
		map.put("/sys/user/deleteUserInDepart" ,CommonConstant.OPERATE_TYPE_DELETE);
		map.put("/app/create" ,CommonConstant.OPERATE_TYPE_ADD);
		map.put("/api/json/{filename}" ,CommonConstant.OPERATE_TYPE_DETAIL);
		map.put("/sys/annountCement/importExcel" ,CommonConstant.OPERATE_TYPE_ADD);
		map.put("/sys/sysDepartPermission/queryTreeListForDeptRole" ,CommonConstant.OPERATE_TYPE_LIST);
		map.put("/sys/common/transitRESTful" ,CommonConstant.OPERATE_TYPE_DETAIL);
		map.put("/asset/detailDatacenter" ,CommonConstant.OPERATE_TYPE_DETAIL);		
		
		
		moduleMap.put("/sys/log/list","系统管理-易分析日志");
		moduleMap.put("/sys/log/logStatistics", "系统管理-日志统计");
		moduleMap.put("/sys/log/config", "系统管理-日志配置");
		moduleMap.put("/sys/log/columnarStatistics", "系统管理-日志统计");
		moduleMap.put("/sys/log/getConfig", "系统管理-日志配置");
		
		moduleMap.put("/analysis/pageNodeFitter","健康智能监控-数据中心关键设备节点传导特性拟合");
		moduleMap.put("/analysis/pageAirFitter","健康智能监控-数据中心关键设备节点传导特性拟合");
		moduleMap.put("/analysis/doFitter","健康智能监控-数据中心关键设备节点传导特性拟合");
		moduleMap.put("/analysis/doSimulate","健康智能监控-基于能耗传导的数据中心运行仿真");
		moduleMap.put("/analysis/optimum","健康智能监控-设备运行情况分析模型的最优策略建议-最优策略建议");
		moduleMap.put("/analysis/pageNodeHistory","健康智能监控-设备运行情况分析模型的最优策略建议-历史最优策略建议");
		moduleMap.put("/analysis/pageNodeSimulate","健康智能监控-设备运行情况分析模型的最优策略建议-历史最优策略仿真");
		moduleMap.put("/subhealth/pageAir","健康智能监控-数据中心设备亚健康分析与预警-空调亚健康分析");
		moduleMap.put("/subhealth/pageUps","健康智能监控-数据中心设备亚健康分析与预警-UPS亚健康分析");
		moduleMap.put("/subhealth/listDatacenter","健康智能监控-数据中心设备亚健康分析与预警-数据中心亚健康分析");
		moduleMap.put("/subhealth/pageDatacenter","健康智能监控-数据中心设备亚健康分析与预警-数据中心亚健康分析");
		moduleMap.put("/subhealth/setThreshold","健康智能监控-数据中心设备亚健康分析与预警-亚健康阈值设置");
		moduleMap.put("/subhealth/listThreshold","健康智能监控-数据中心设备亚健康分析与预警-亚健康阈值设置");
		moduleMap.put("/subhealth/listThresholdMeter","健康智能监控-数据中心设备亚健康分析与预警-亚健康阈值设置");
		moduleMap.put("/subhealth/listAlertLevel","健康智能监控-数据中心设备亚健康分析与预警-亚健康阈值设置");
		moduleMap.put("/subhealth/alertStatistic", "健康智能监控-数据中心设备亚健康分析与预警-亚健康预警");
		moduleMap.put("/subhealth/countAlerts", "健康智能监控-数据中心设备亚健康分析与预警-亚健康预警");
		moduleMap.put("/subhealth/pageAlert", "健康智能监控-数据中心设备亚健康分析与预警-亚健康预警");
		
		
		moduleMap.put("/asset/createDatacenter", "精确管控与供电制冷联动-基础资源细粒度感知-资源管理-数据中心");
		moduleMap.put("/asset/updateDatacenter", "精确管控与供电制冷联动-统一管理联动平台-动环节能");
		moduleMap.put("/asset/detailDatacenter", "精确管控与供电制冷联动-基础资源细粒度感知-资源管理-数据中心");
		moduleMap.put("/asset/pageDatacenter", "精确管控与供电制冷联动-基础资源细粒度感知-资源管理-数据中心");
		moduleMap.put("/asset/listDatacenter", "精确管控与供电制冷联动-基础资源细粒度感知-资源管理-数据中心");
		moduleMap.put("/asset/createDevice", "精确管控与供电制冷联动-基础资源细粒度感知-资源管理-L1层：基础设施");
		moduleMap.put("/asset/updateDevice", "精确管控与供电制冷联动-基础资源细粒度感知-资源管理-L1层：基础设施");
		moduleMap.put("/asset/deleteDevice", "精确管控与供电制冷联动-基础资源细粒度感知-资源管理-L1层：基础设施");
		moduleMap.put("/asset/detailDevice", "精确管控与供电制冷联动-基础资源细粒度感知-资源管理-L1层：基础设施");
		moduleMap.put("/asset/pageDevice", "精确管控与供电制冷联动-基础资源细粒度感知-资源管理-L1层：基础设施");
		moduleMap.put("/asset/createSwitcher", "精确管控与供电制冷联动-基础资源细粒度感知-资源管理-L2层：IT设备");
		moduleMap.put("/asset/updateSwitcher", "精确管控与供电制冷联动-基础资源细粒度感知-资源管理-L2层：IT设备");
		
		moduleMap.put("/asset/deleteSwitcher", "精确管控与供电制冷联动-基础资源细粒度感知-资源管理-L2层：IT设备");
		moduleMap.put("/asset/detailSwitcher", "精确管控与供电制冷联动-基础资源细粒度感知-资源管理-L2层：IT设备");
		
		
		moduleMap.put("/asset/pageSwitcher", "精确管控与供电制冷联动-基础资源细粒度感知-资源管理-L2层：IT设备");
		moduleMap.put("/asset/createNode", "精确管控与供电制冷联动-基础资源细粒度感知-资源管理-L2层：IT设备");
		moduleMap.put("/asset/updateNode", "精确管控与供电制冷联动-基础资源细粒度感知-资源管理-L2层：IT设备");
		moduleMap.put("/asset/deleteNode", "精确管控与供电制冷联动-基础资源细粒度感知-资源管理-L2层：IT设备");
		moduleMap.put("/asset/detailNode", "精确管控与供电制冷联动-基础资源细粒度感知-资源管理-L2层：IT设备");
		moduleMap.put("/asset/pageNode", "精确管控与供电制冷联动-基础资源细粒度感知-资源管理-L2层：IT设备");
		
		
		// moduleMap.put("/asset/syncDevice", "精确管控与供电制冷联动-基础资源细粒度感知-资源管理-L2层：IT设备");
		moduleMap.put("/asset/detailNode", "精确管控与供电制冷联动-基础资源细粒度感知-资源管理-L2层：IT设备");
		moduleMap.put("/asset/pageNode", "精确管控与供电制冷联动-基础资源细粒度感知-资源管理-L2层：IT设备");
		moduleMap.put("/asset/syncDevice", "精确管控与供电制冷联动-基础资源细粒度感知-资源管理-L1层：基础设施");
		moduleMap.put("/asset/syncNodeSwitcher", "精确管控与供电制冷联动-基础资源细粒度感知-资源管理-L2层：IT设备");
		
		moduleMap.put("/asset/pageItDevice", "精确管控与供电制冷联动-基础资源细粒度感知-资源管理-L2层：IT设备");
		
		moduleMap.put("/asset/countItDevice", "精确管控与供电制冷联动-基础资源细粒度感知-资源管理-L2层：IT设备");
		moduleMap.put("/asset/listDevice", "精确管控与供电制冷联动-基础资源细粒度感知-资源管理-L2层：IT设备");
		moduleMap.put("/asset/listNode", "精确管控与供电制冷联动-基础资源细粒度感知-资源管理-L1层：基础设施");
		// moduleMap.put("/asset/syncNodeSwitcher", "精确管控与供电制冷联动-基础资源细粒度感知-资源管理-L2层：IT设备");
		
		moduleMap.put("/cloud_resource/page", "精确管控与供电制冷联动-基础资源细粒度感知-资源管理-L3层：云平台资源");
		moduleMap.put("/cloud_resource/create", "精确管控与供电制冷联动-基础资源细粒度感知-资源管理-L3层：云平台资源");
		moduleMap.put("/cloud_resource/delete", "精确管控与供电制冷联动-基础资源细粒度感知-资源管理-L3层：云平台资源");
		moduleMap.put("/cloud_resource/detail", "精确管控与供电制冷联动-基础资源细粒度感知-资源管理-L3层：云平台资源");
		moduleMap.put("/cloud_resource/vm_detail", "精确管控与供电制冷联动-基础资源细粒度感知-资源管理-L3层：云平台资源");
		moduleMap.put("/cloud_resource/k8s_detail", "精确管控与供电制冷联动-基础资源细粒度感知-资源管理-L3层：云平台资源");
		moduleMap.put("/cloud_resource/update1", "精确管控与供电制冷联动-基础资源细粒度感知-资源管理-L3层：云平台资源");
		moduleMap.put("/cloud_resource/sync", "精确管控与供电制冷联动-基础资源细粒度感知-资源管理-L3层：云平台资源");
		moduleMap.put("/cloud_resource/analysis", "精确管控与供电制冷联动-基础资源细粒度感知-资源管理-L3层：云平台资源");
		
		
		moduleMap.put("/app/create", "精确管控与供电制冷联动-基础资源细粒度感知-资源管理-L4层：应用资源");
		moduleMap.put("/app/delete", "精确管控与供电制冷联动-基础资源细粒度感知-资源管理-L4层：应用资源");
		moduleMap.put("/app/update1", "精确管控与供电制冷联动-基础资源细粒度感知-资源管理-L4层：应用资源");
		moduleMap.put("/app/update_scheduler_config", "多数据中心调度-动态调度机制-调度触发策略设置");
		moduleMap.put("/app/page", "精确管控与供电制冷联动-基础资源细粒度感知-资源管理-L4层：应用资源");
		moduleMap.put("/app/sync", "精确管控与供电制冷联动-基础资源细粒度感知-资源管理-L4层：应用资源");
		
		moduleMap.put("/authority/listByClassification","精确管控与供电制冷联动-基础资源细粒度感知-权限管理");
		moduleMap.put("/authority/set1", "精确管控与供电制冷联动-基础资源细粒度感知-权限管理");
		
		moduleMap.put("/dcEnergy/statisticsByYear", "系统管理-统计报表-数据中心统计-年度能耗统计");
		moduleMap.put("/dcEnergy/statisticsByQuarter", "系统管理-统计报表-数据中心统计-季度能耗统计");
		moduleMap.put("/dcEnergy/statisticsByMonth", "系统管理-统计报表-数据中心统计-月度能耗统");
		moduleMap.put("/dcEnergy/reportByYear", "系统管理-统计报表-数据中心报表-年度能耗统计报表");
		moduleMap.put("/dcEnergy/reportByQuarter", "系统管理-统计报表-数据中心报表-季度能耗统计报表");
		moduleMap.put("/dcEnergy/reportByMonth", "系统管理-统计报表-数据中心报表-月度能耗统计报表");
		
		moduleMap.put("/dataClean/get1", "精确管控与供电制冷联动-精确能耗管控-冗余数据清洗");
		moduleMap.put("/dataClean/update1", "精确管控与供电制冷联动-精确能耗管控-冗余数据清洗");
		moduleMap.put("/dataSecurity/set2", "精确管控与供电制冷联动-精确能耗管控-数据安全");
		moduleMap.put("/dataSecurity/detail", "精确管控与供电制冷联动-精确能耗管控-数据安全");
		
		moduleMap.put("/scheduler/step/list1", "多数据中心调度-动态调度机制-调度过程监控");
		
		moduleMap.put("/scheduler/datacenter_config/page", "多数据中心调度-动态调度机制-调度触发策略设置");
		moduleMap.put("/scheduler/update_scheduler_model", "多数据中心调度-调度模型管理-调度策略设置");
		moduleMap.put("/scheduler/scheduler_model_tool_list", "离线业务多数据中心迁移-异步模型迁移工具");
		moduleMap.put("/scheduler/migrate_resource", "精确管控与供电制冷联动-统一管理联动-业务隔离");
		
		moduleMap.put("/scheduler/scheduler_model_tool_list", "离线业务多数据中心迁移-异步模型迁移工具");
		moduleMap.put("/scheduler/migrate_resource", "精确管控与供电制冷联动-统一管理联动-业务隔离");

	}
	public static Integer getOperateTypeByUrl(String url) {
		Integer type = map.get(url);
		if(type != null) {
			return type;
		}
		return CommonConstant.OPERATE_TYPE_LIST;
	}
	public static String getModule(String url, String title) {
		if("/sys/log/list".equals(url)) {
			if(StringUtils.isBlank(title)) {
				return "系统管理-易分析日志";
			}else {
				if (title.equals("系统日志") || title.equals("业务日志")) {
					return  "系统管理-日志管理-" + title;

				} else {
					return "系统管理-易分析日志-" + title;
				}
			}
		}
		if(StringUtils.isNotBlank(title)){
			return title;
		}
		String s =  moduleMap.get(url);
		return s;
	}
	
}
