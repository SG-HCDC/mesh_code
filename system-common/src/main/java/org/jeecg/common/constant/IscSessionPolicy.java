package org.jeecg.common.constant;

import org.jeecg.common.system.util.JwtUtil;

public class IscSessionPolicy {
	public static long sessionTimeOut = JwtUtil.EXPIRE_TIME*2 / 1000;
	private Long maxSessionCount;
	private Boolean singleClient;
	private Long sessionTimeout;
	public Long getMaxSessionCount() {
		return maxSessionCount;
	}
	public void setMaxSessionCount(Long maxSessionCount) {
		this.maxSessionCount = maxSessionCount;
	}
	public Boolean getSingleClient() {
		return singleClient;
	}
	public void setSingleClient(Boolean singleClient) {
		this.singleClient = singleClient;
	}
	public Long getSessionTimeout() {
		return sessionTimeout;
	}
	public void setSessionTimeout(Long sessionTimeout) {
		this.sessionTimeout = sessionTimeout;
	}
	
}
