package org.jeecg.common.constant;

import java.util.stream.Stream;

public enum DictCode {

	SCHEDULER_MODEL("scheduler_model", "调度模型"),
	SCHEDULER_MODEL_TOOL("scheduler_model_tool", "调度模型工具"),
	SCHEDULER_STRATEGY_CONFIG("scheduler_strategy_config", "迁移策略配置"),
	SCHEDULER_STRATEGY_RULE("scheduler_strategy_rule", "迁移策略执行规则"),
	SCHEDULER_ERROR_SWITCH("scheduler_error_switch", "调度迁移异常开关"),
	LOG_CONFIG("log_config", "日志配置"),
	MONITOR_STATISTICS_CONFIG("monitor_statistics_config", "监控数据波动配置"),
	MONITOR_TYPE_CONFIG("monitor_type_config", "监控类型变更配置");

	/* 编号 */
	private String code;
	/* 名字 */
	private String name;

	DictCode(String code, String name) {
		this.code = code;
		this.name = name;
	}

	public String getCode() {
		return code;
	}

	public String getName() {
		return name;
	}

	public static DictCode getByType(String code) {
		return Stream.of(DictCode.values()).filter(s -> s.getCode().equals(code)).findFirst().orElse(null);
	}

}
