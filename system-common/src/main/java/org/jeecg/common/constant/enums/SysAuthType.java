package org.jeecg.common.constant.enums;

import java.util.stream.Stream;

/**
 * 
 */
public enum SysAuthType {

	/**
	 * 
	 */
	ISC("isc"),
	/**
	 * 
	 */
	LOCAL("local");


	/**
	 * 名称
	 */
	String name;

	/**
	 * 构造器
	 *
	 * @param type
	 * @param code
	 * @param templatePath
	 * @param note
	 */
	SysAuthType(String name) {
		this.name = name;
	}


	public String getName() {
		return name;
	}

	public static SysAuthType getByName(String name) {
		return Stream.of(SysAuthType.values()).filter(s -> s.getName().equals(name)).findFirst().orElse(null);
	}

}
