package org.jeecg.common.constant.enums;

import java.util.stream.Stream;

import org.jeecg.common.constant.CommonConstant;

/**
 * 
 */
public enum SysLogType {

	/**
	 * 操作日志
	 */
	OPERATE(CommonConstant.LOG_TYPE_OPERATE, "操作日志"),
	/**
	 * 登录日志
	 */
	LOGIN(CommonConstant.LOG_TYPE_LOGIN, "登录日志"),
	/**
	 * 定时任务
	 */
	TASK(2, "定时任务"),
	/**
	 * 调试日志
	 */
	DEBUG(CommonConstant.LOG_TYPE_DEBUG, "调试日志"),
	/**
	 * 错误日志
	 */
	ERROR(4, "错误日志"),
	/**
	 * 告警日志
	 */
	ALARM(5, "告警日志"),
	/**
	 * 运行日志
	 */
	RUN(6, "运行日志");

	/**
	 * 类型
	 */
	Integer type;

	/**
	 * 名称
	 */
	String name;

	/**
	 * 构造器
	 *
	 * @param type
	 * @param code
	 * @param templatePath
	 * @param note
	 */
	SysLogType(Integer type, String name) {
		this.type = type;
		this.name = name;
	}

	public Integer getType() {
		return type;
	}

	public String getName() {
		return name;
	}

	public static SysLogType getByType(Integer type) {
		return Stream.of(SysLogType.values()).filter(s -> s.getType() == type).findFirst().orElse(null);
	}

}
