package org.jeecg.common.constant.enums;

import java.util.stream.Stream;

/**
 * 
 */
public enum SysLogConfig {
	
	LIST_SWITCH("listSwitch"),
	DETAIL_SWITCH("detailSwitch"),
	ADD_SWITCH("addSwitch"),
	UPDATE_SWITCH("updateSwitch"),
	DELETE_SWITCH("deleteSwitch"),
	SQL_SWITCH("sqlSwitch"),
	CAPACITY("capacity"),
	APPQUERY_SWITCH("appQuerySwitch");


	/**
	 * 名称
	 */
	String name;

	/**
	 * 构造器
	 *
	 * @param type
	 * @param code
	 * @param templatePath
	 * @param note
	 */
	SysLogConfig(String name) {
		this.name = name;
	}


	public String getName() {
		return name;
	}

	public static SysLogConfig getByName(String name) {
		return Stream.of(SysLogConfig.values()).filter(s -> s.getName().equals(name)).findFirst().orElse(null);
	}

}
