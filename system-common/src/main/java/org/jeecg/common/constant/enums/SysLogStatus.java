package org.jeecg.common.constant.enums;

import java.util.stream.Stream;

/**
 * 
 */
public enum SysLogStatus {

	/**
	 * 操作日志
	 */
	FAIL(0, "失败"),
	/**
	 * 登录日志
	 */
	SUCCESS(1, "成功");

	/**
	 * 类型
	 */
	Integer type;

	/**
	 * 名称
	 */
	String name;

	/**
	 * 构造器
	 *
	 * @param type
	 * @param code
	 * @param templatePath
	 * @param note
	 */
	SysLogStatus(Integer type, String name) {
		this.type = type;
		this.name = name;
	}

	public Integer getType() {
		return type;
	}

	public String getName() {
		return name;
	}

	public static SysLogStatus getByType(Integer type) {
		return Stream.of(SysLogStatus.values()).filter(s -> s.getType() == type).findFirst().orElse(null);
	}

}
