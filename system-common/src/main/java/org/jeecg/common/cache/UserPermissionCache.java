package org.jeecg.common.cache;

import java.util.HashMap;
import java.util.Map;

import org.jeecg.common.util.RedisUtil;

import com.alibaba.fastjson.JSONObject;
import com.gcloud.mesh.framework.core.SpringUtil;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class UserPermissionCache {

	private static Map<String, JSONObject> cache = new HashMap<>();

	public static JSONObject getByUsername(String username) {

		RedisUtil redisUtil = SpringUtil.getBean(RedisUtil.class);
		Object result = redisUtil.get("UserPermissionCache");
		if (result != null && (Boolean) result == true) {
			log.debug("用户权限缓存开启");
			return null;
		} else {
			log.debug("用户权限缓存关闭");
			return cache.get(username);
		}
	}

	public static void put(String username, JSONObject json) {
		cache.put(username, json);
	}
}
