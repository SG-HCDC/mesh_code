package org.jeecg.common.cache;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import org.jeecg.common.isc.IIscService;
import org.jeecg.common.vo.PermissionsCompareVo;
import org.jeecg.common.vo.PermissionsInfoVo;

import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import com.gcloud.mesh.framework.core.SpringUtil;

import lombok.extern.slf4j.Slf4j;
@Slf4j
public class  PermissionCodeCache{

	private static Map<String, PermissionsInfoVo> cache = null;

	public static PermissionsInfoVo getByUrl(String url) {
		if(cache != null) {
			synchronized (PermissionCodeCache.class) {
				return cache.get(url);
			}
		}else {
			//手动加载权限
			try {
				IIscService service = SpringUtil.getBean(IIscService.class);
				PermissionsCompareVo res = service.permissionsCompare();
				if (res != null && res.getMeshPermissions() != null) {
					for (PermissionsInfoVo p : res.getMeshPermissions()) {
						if(StringUtils.isNotBlank(p.getUrl())) {
							PermissionCodeCache.put(p.getUrl(), p);
						}		
					}
				}
				log.info("权限缓存手动加载成功："+PermissionCodeCache.getUrls().size());
			} catch (Exception e) {
				log.error("【权限手动加载失败-PermissionCodeCacheRunner】", e);
			}
		}
		return cache.get(url);
	}

	public static void put(String url, PermissionsInfoVo p) {
		synchronized (PermissionCodeCache.class) {
			if(cache == null) {
				cache = new HashMap<>();
			}
			cache.put(url, p);
		}
	}
	public static  Set<String> getUrls(){
		return cache.keySet();
	}
}
