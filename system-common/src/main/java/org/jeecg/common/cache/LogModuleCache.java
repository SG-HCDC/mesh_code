package org.jeecg.common.cache;

import java.util.HashMap;
import java.util.Map;

public class LogModuleCache {

	private static Map<String, Map<String, String>> cache = new HashMap<>();

	public static Map<String, String> getByUserId(String userId) {
		return cache.get(userId);
	}

	public static void put(String userId, Map<String, String> map) {
		cache.put(userId, map);
	}
}
