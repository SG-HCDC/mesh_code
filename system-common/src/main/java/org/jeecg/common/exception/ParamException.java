package org.jeecg.common.exception;

public class ParamException extends RuntimeException{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
    private String errorCode;///错误代码
    private String errorMsg;///错误信息

    public ParamException() {
        super();
    }

    public ParamException(String errorCode) {
        super(errorCode);
        this.errorCode = errorCode;
    }

    public ParamException(String errorCode, String errorMsg) {
        super(errorCode);
        this.errorCode = errorCode;
        this.errorMsg = errorMsg;
    }

    public ParamException(Throwable throwable) {
        super(throwable);
    }

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public String getErrorMsg() {
        return errorMsg;
    }

    public void setErrorMsg(String errorMsg) {
        this.errorMsg = errorMsg;
    }

}
