package org.jeecg.common.exception;

import lombok.Data;

@Data
public class MyBusinessException extends RuntimeException{
    private String errorCode = "190100";///错误代码
    private String errorMsg;///错误信息

    public MyBusinessException(String code,String msg){
        this.errorCode = code;
        this.errorMsg = msg;
    }

    public MyBusinessException(String msg){
        this.errorMsg = msg;
    }
}
