-- 2021/1/6
alter table dcs_apps add deleted tinyint(2) NULL DEFAULT 0;

-- 2021/1/7
alter table dcs_subhealth_alerts add level varchar(36) NULL DEFAULT 0;

-- 2021/1/8
INSERT INTO `sys_dict_item` VALUES ('1342732234372739123', '1342732234116886529', 'sqlSwitch', '1', NULL, NULL, NULL, 'zhangzj', '2020-12-26 15:21:26', NULL, NULL);

-- 2021/1/10
ALTER TABLE `mesh_controller`.`sys_log` ADD COLUMN `module` varchar(100) NULL COMMENT '模块名称';
ALTER TABLE `mesh_controller`.`sys_log_backup` ADD COLUMN `module` varchar(100) NULL COMMENT '模块名称';


-- 2021/1/11 zhangdp
ALTER TABLE `mesh_controller`.`dcs_scheduler_jobs` ADD COLUMN `app_name` varchar(32) NULL COMMENT '应用名称';
ALTER TABLE `mesh_controller`.`dcs_scheduler_jobs` ADD COLUMN `cloud_resource_name` varchar(32) NULL COMMENT '云资源名称';
ALTER TABLE `mesh_controller`.`dcs_scheduler_jobs` ADD COLUMN `cloud_resource_type_name` varchar(32) NULL COMMENT '云资源类型名称';
ALTER TABLE `mesh_controller`.`dcs_scheduler_jobs` ADD COLUMN `cloud_resource_id` varchar(32) NULL COMMENT '云资源id';

-- 2021/1/11 huanglz
INSERT INTO `sys_dict` VALUES ('1335151234955660218', '监控类型修改配置', 'monitor_type_config', NULL, 0, 'sgcc_super_admin', '2020-12-05 17:14:00', NULL, NULL, 0);
INSERT INTO `sys_dict` VALUES ('1335151234955660219', '监控数据波动配置', 'monitor_statistics_config', NULL, 0, 'sgcc_super_admin', '2020-12-05 17:14:00', NULL, NULL, 0);
INSERT INTO `sys_dict_item` VALUES ('1341712115307670233', '1335151234955660218', '监控类型修改配置', '3', NULL, NULL, NULL, 'sgcc_super_admin', '2020-12-05 19:05:58', NULL, NULL);
INSERT INTO `sys_dict_item` VALUES ('1341712115307670234', '1335151234955660219', '监控数据波动配置', '0', NULL, NULL, NULL, 'sgcc_super_admin', '2020-12-05 19:05:58', NULL, NULL);

ALTER TABLE `mesh_controller`.`dcs_model_scores` ADD COLUMN `samples` varchar(200) NULL;

-- 2021/1/12 huanglz
ALTER TABLE `mesh_controller`.`dcs_model_scores` ADD COLUMN `update_time` datetime(0) NULL;

-- 2021/1/13 huanglz
ALTER TABLE `mesh_controller`.`dcs_scheduler_imagine` ADD COLUMN `temperature` double(11,2) NULL;
ALTER TABLE `mesh_controller`.`dcs_scheduler_imagine` ADD COLUMN `distribution_box_voltage` double(11,2) NULL;
ALTER TABLE `mesh_controller`.`dcs_scheduler_imagine` ADD COLUMN `datacenter_energy` double(11,2) NULL;
ALTER TABLE `mesh_controller`.`dcs_scheduler_imagine` ADD COLUMN `business_respond` double(11,2) NULL;

ALTER TABLE `mesh_controller`.`dcs_scheduler_jobs` ADD COLUMN `source_node_id` varchar(36) NULL COMMENT '源节点id';

ALTER TABLE `mesh_controller`.`asset_cloud_resources` ADD COLUMN `cpu_cores` int(11) NULL;
ALTER TABLE `mesh_controller`.`asset_cloud_resources` ADD COLUMN `memory` int(11) NULL;

-- 2021/1/19
alter table asset_nodes add sm_data varchar(500) NULL;

-- 2021/1/25 zhangzj
-- INSERT INTO `sys_log` VALUES ('1353345268686295041', 3, 'org.springframework.web.bind.MissingRequestHeaderException: Missing request header \'pass\' on config file', 8, 'E0CA3B6207CF483A8C4E9429CC340C2F', 'sgccyp3', '192.168.203.81', 'Isc调试', 'com.gcloud.mesh.common.controller.IscController.getIscUrl()', '/isc/get_isc_url', '', NULL, 6, 'sgccyp3', '2021-01-21 12:13:51', NULL, NULL, NULL, 1);
-- INSERT INTO `sys_log` VALUES ('1353345268686295042', 3, 'at org.jeecg.config.shiro.ShiroRealm.checkUserTokenIsEffect(ShiroRealm.java:135)', 8, 'E0CA3B6207CF483A8C4E9429CC340C2F', 'sgccyp3', '192.168.203.81', 'Isc调试', 'com.gcloud.mesh.common.controller.IscController.getIscUrl()', '/isc/get_isc_url', '', NULL, 6, 'sgccyp3', '2021-01-22 09:11:21', NULL, NULL, NULL, 1);
-- INSERT INTO `sys_log` VALUES ('1353345268686295043', 3, 'at org.apache.catalina.core.ApplicationFilterChain.internalDoFilter(ApplicationFilterChain.java:193)', 8, 'E0CA3B6207CF483A8C4E9429CC340C2F', 'sgccyp3', '192.168.203.81', 'Isc调试', 'com.gcloud.mesh.common.controller.IscController.getIscUrl()', '/isc/get_isc_url', '', NULL, 6, 'sgccyp3', '2021-01-24 18:03:11', NULL, NULL, NULL, 1);
-- INSERT INTO `sys_log` VALUES ('1353339927856709633', 3, 'update `dcs_apps` set `cloud_resource_id`=?,`create_time`=?,`deleted`=?,`from`=?,`name`=?,`type`=? where `id`=?', 24, 'E0CA3B6207CF483A8C4E9429CC340C2F', 'sgccyp3', '192.168.203.81', '精确管控与供电制冷联动-基础资源细粒度感知-资源管理-应用资源', NULL, '/app/update1', NULL, NULL, NULL, 'sgccyp3', '2021-01-21 11:52:31', NULL, NULL, NULL, 1);
-- INSERT INTO `sys_log` VALUES ('1353339927856709634', 3, 'SELECT id,version,resource_id,file_id,create_user_id,create_time,update_user_id,update_time FROM sc_edition', 24, 'E0CA3B6207CF483A8C4E9429CC340C2F', 'sgccyp3', '192.168.203.81', '精确管控与供电制冷联动-基础资源细粒度感知-资源管理-应用资源', NULL, '/app/update1', NULL, NULL, NULL, 'sgccyp3', '2021-01-21 18:12:27', NULL, NULL, NULL, 1);
-- INSERT INTO `sys_log` VALUES ('1353339927856709635', 3, 'SELECT id,name,image_url,status,type,config,content FROM sys_third_platform', 24, 'E0CA3B6207CF483A8C4E9429CC340C2F', 'sgccyp3', '192.168.203.81', '精确管控与供电制冷联动-基础资源细粒度感知-资源管理-应用资源', NULL, '/app/update1', NULL, NULL, NULL, 'sgccyp3', '2021-01-23 22:18:29', NULL, NULL, NULL, 1);
-- INSERT INTO `sys_log` VALUES ('1353339831790370817', 3, 'java.lang.IllegalArgumentException: Source must not be null        at org.springframework.util.Assert.notNull(Assert.java:198) ~[spring-core-5.2.4.RELEASE.jar!/:5.2.4.RELEASE]', 25, 'E0CA3B6207CF483A8C4E9429CC340C2F', 'sgccyp3', '192.168.203.81', '程序权限列表调试', 'com.gcloud.mesh.common.controller.IscController.permissionsCompare()', '/isc/permissions_compare', '', NULL, 961, 'sgccyp3', '2021-01-21 14:13:28', NULL, NULL, NULL, 1);
-- INSERT INTO `sys_log` VALUES ('1353339831790370818', 3, 's.d.s.w.r.operation.CachingOperationNameGenerator:40 - Generating unique operation named: deleteBatchUsingDELETE_6', 25, 'E0CA3B6207CF483A8C4E9429CC340C2F', 'sgccyp3', '192.168.203.81', '程序权限列表调试', 'com.gcloud.mesh.common.controller.IscController.permissionsCompare()', '/isc/permissions_compare', '', NULL, 961, 'sgccyp3', '2021-01-21 18:12:28', NULL, NULL, NULL, 1);
-- INSERT INTO `sys_log` VALUES ('1353339831790370819', 3, 'o.s.scheduling.quartz.LocalDataSourceJobStore:3488 - ClusterManager: Scanning for instance acfdad748fce1611500650272  in-progress jobs.', 25, 'E0CA3B6207CF483A8C4E9429CC340C2F', 'sgccyp3', '192.168.203.81', '程序权限列表调试', 'com.gcloud.mesh.common.controller.IscController.permissionsCompare()', '/isc/permissions_compare', '', NULL, 961, 'sgccyp3', '2021-01-23 18:28:14', NULL, NULL, NULL, 1);


-- 2021/1/25 zhangzj
ALTER TABLE `mesh_controller`.`sys_log` ADD COLUMN `operate_type_name` varchar(200) NULL COMMENT '操作类型名称';
ALTER TABLE `mesh_controller`.`sys_log_backup` ADD COLUMN `operate_type_name` varchar(200) NULL COMMENT '操作类型名称';