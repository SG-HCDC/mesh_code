DROP DATABASE IF EXISTS gcloud_ce_identity;
create database `gcloud_ce_identity` default character set utf8mb4 collate utf8mb4_general_ci;
/*
 Navicat Premium Data Transfer

 Source Server         : 192.168.215.84
 Source Server Type    : MySQL
 Source Server Version : 50734
 Source Host           : 192.168.215.84:30306
 Source Schema         : gcloud_ce_identity

 Target Server Type    : MySQL
 Target Server Version : 50734
 File Encoding         : 65001

 Date: 10/08/2021 16:33:38
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for gce_api
-- ----------------------------
DROP TABLE IF EXISTS `gce_api`;
CREATE TABLE `gce_api`  (
  `id` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `parent_id` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `path` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `api__index_01`(`path`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of gce_api
-- ----------------------------
INSERT INTO `gce_api` VALUES ('01', NULL, NULL, '集群管理');
INSERT INTO `gce_api` VALUES ('0101', '01', '/cluster/page', '集群列表');
INSERT INTO `gce_api` VALUES ('0102', '01', '/cluster/detail', '集群详情');
INSERT INTO `gce_api` VALUES ('0103', '01', '/cluster/edit', '编辑集群');
INSERT INTO `gce_api` VALUES ('0104', '01', '/cluster/delete', '删除集群');
INSERT INTO `gce_api` VALUES ('0105', '01', '/cluster/list_with_namespace', '集群命名空间树');
INSERT INTO `gce_api` VALUES ('0106', '01', '/cluster/create', '创建集群');
INSERT INTO `gce_api` VALUES ('0107', '01', '/cluster/attach_node', '添加单个节点');
INSERT INTO `gce_api` VALUES ('0108', '01', '/cluster/attach_nodes', '添加节点');
INSERT INTO `gce_api` VALUES ('02', NULL, NULL, 'deployment管理');
INSERT INTO `gce_api` VALUES ('0201', '02', '/deployment/page', 'deployment列表');
INSERT INTO `gce_api` VALUES ('0202', '02', '/deployment/create', '创建deployment');
INSERT INTO `gce_api` VALUES ('0203', '02', '/deployment/edit', '编辑deployment');
INSERT INTO `gce_api` VALUES ('0204', '02', '/deployment/port_add', '新增访问方式');
INSERT INTO `gce_api` VALUES ('0205', '02', '/deployment/port_del', '删除访问方式');
INSERT INTO `gce_api` VALUES ('0206', '02', '/deployment/delete', '删除deployment');
INSERT INTO `gce_api` VALUES ('0207', '02', '/deployment/detail', 'deployment详情');
INSERT INTO `gce_api` VALUES ('0208', '02', '/deployment/pause', '停止编排');
INSERT INTO `gce_api` VALUES ('0209', '02', '/deployment/yaml', '查看yaml');
INSERT INTO `gce_api` VALUES ('0210', '02', '/deployment/resume', '重新编排');
INSERT INTO `gce_api` VALUES ('03', NULL, NULL, 'Kubernetes版本管理');
INSERT INTO `gce_api` VALUES ('0301', '03', '/kubernetes_version/list', 'Kubernetes版本所有记录');
INSERT INTO `gce_api` VALUES ('04', NULL, NULL, '网络驱动管理');
INSERT INTO `gce_api` VALUES ('0401', '04', '/network_plugin/list', '网络驱动所有记录');
INSERT INTO `gce_api` VALUES ('05', NULL, NULL, '命名空间管理');
INSERT INTO `gce_api` VALUES ('0501', '05', '/namespace/create', '创建命名空间');
INSERT INTO `gce_api` VALUES ('0502', '05', '/namespace/delete', '删除命名空间');
INSERT INTO `gce_api` VALUES ('0503', '05', '/namespace/update', '更新命名空间');
INSERT INTO `gce_api` VALUES ('0504', '05', '/namespace/page', '命名空间列表');
INSERT INTO `gce_api` VALUES ('0505', '05', '/namespace/yaml', '获取yaml');
INSERT INTO `gce_api` VALUES ('06', NULL, NULL, '集群节点管理');
INSERT INTO `gce_api` VALUES ('0601', '06', '/cluster/node/page', '集群节点列表');
INSERT INTO `gce_api` VALUES ('0602', '06', '/cluster/node/detail', '集群节点详情');
INSERT INTO `gce_api` VALUES ('0603', '06', '/cluster/node/update', '更新集群节点');
INSERT INTO `gce_api` VALUES ('0604', '06', '/cluster/node/delete', '删除集群节点');
INSERT INTO `gce_api` VALUES ('0605', '06', '/cluster/node/cordon', '暂停集群节点');
INSERT INTO `gce_api` VALUES ('0606', '06', '/cluster/node/uncordon', '激活集群节点');
INSERT INTO `gce_api` VALUES ('0607', '06', '/cluster/node/drain', '驱散集群节点');
INSERT INTO `gce_api` VALUES ('07', NULL, NULL, '容器节点管理');
INSERT INTO `gce_api` VALUES ('0701', '07', '/container/node/page', '容器节点列表');
INSERT INTO `gce_api` VALUES ('0702', '07', '/container/node/cluster_available', '可以加入集群的节点列表');
INSERT INTO `gce_api` VALUES ('08', NULL, NULL, 'pod管理');
INSERT INTO `gce_api` VALUES ('0801', '08', '/pod/page', 'pod列表');
INSERT INTO `gce_api` VALUES ('0802', '08', '/pod/create', '创建pod');
INSERT INTO `gce_api` VALUES ('0803', '08', '/pod/edit', '编辑pod');
INSERT INTO `gce_api` VALUES ('0804', '08', '/pod/port_add', '新增访问方式');
INSERT INTO `gce_api` VALUES ('0805', '08', '/pod/port_del', '删除访问方式');
INSERT INTO `gce_api` VALUES ('0806', '08', '/pod/environment_edit', '编辑环境变量');
INSERT INTO `gce_api` VALUES ('0807', '08', '/pod/delete', '删除pod');
INSERT INTO `gce_api` VALUES ('0808', '08', '/pod/detail', 'pod详情');
INSERT INTO `gce_api` VALUES ('0809', '08', '/pod/yaml', 'pod查看yaml');
INSERT INTO `gce_api` VALUES ('09', NULL, NULL, 'service管理');
INSERT INTO `gce_api` VALUES ('0901', '09', '/service/page', 'service列表');
INSERT INTO `gce_api` VALUES ('10', NULL, NULL, 'api管理');
INSERT INTO `gce_api` VALUES ('1001', '10', '/api/ignore_urls', 'ignore_urls');
INSERT INTO `gce_api` VALUES ('1002', '10', '/api/auth', 'auth');
INSERT INTO `gce_api` VALUES ('11', NULL, NULL, 'function管理');
INSERT INTO `gce_api` VALUES ('1101', '11', '/function/list', '功能权限列表');
INSERT INTO `gce_api` VALUES ('12', NULL, NULL, '角色管理');
INSERT INTO `gce_api` VALUES ('1201', '12', '/role/create', '创建角色');
INSERT INTO `gce_api` VALUES ('1202', '12', '/role/update', '修改角色');
INSERT INTO `gce_api` VALUES ('1203', '12', '/role/delete', '删除角色');
INSERT INTO `gce_api` VALUES ('1204', '12', '/role/user_role', '配置用户角色');
INSERT INTO `gce_api` VALUES ('1205', '12', '/role/user_role_info', '用户角色信息');
INSERT INTO `gce_api` VALUES ('1206', '12', '/role/role_function', '配置角色权限');
INSERT INTO `gce_api` VALUES ('1207', '12', '/role/page', '角色列表');
INSERT INTO `gce_api` VALUES ('1208', '12', '/role/detail', '角色详情');
INSERT INTO `gce_api` VALUES ('13', NULL, NULL, '用户管理');
INSERT INTO `gce_api` VALUES ('1301', '13', '/user/resource_type', 'resource_type');

-- ----------------------------
-- Table structure for gce_api_ignore
-- ----------------------------
DROP TABLE IF EXISTS `gce_api_ignore`;
CREATE TABLE `gce_api_ignore`  (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `api_id` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `api_ignore_idx_01`(`api_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of gce_api_ignore
-- ----------------------------

-- ----------------------------
-- Table structure for gce_function
-- ----------------------------
DROP TABLE IF EXISTS `gce_function`;
CREATE TABLE `gce_function`  (
  `id` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `parent_id` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `state` tinyint(1) NULL DEFAULT NULL,
  `function` tinyint(1) NULL DEFAULT NULL,
  `region` tinyint(1) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of gce_function
-- ----------------------------
INSERT INTO `gce_function` VALUES ('01', NULL, '系统管理', 1, 0, 0);
INSERT INTO `gce_function` VALUES ('0101', '01', '集群管理', 1, 0, 0);
INSERT INTO `gce_function` VALUES ('010101', '0101', '列表', 1, 1, 0);
INSERT INTO `gce_function` VALUES ('010102', '0101', '详情', 1, 1, 0);
INSERT INTO `gce_function` VALUES ('010103', '0101', '新增', 1, 1, 0);
INSERT INTO `gce_function` VALUES ('010104', '0101', '编辑', 1, 1, 0);
INSERT INTO `gce_function` VALUES ('010105', '0101', '删除', 1, 1, 0);
INSERT INTO `gce_function` VALUES ('010106', '0101', '添加节点', 1, 1, 0);
INSERT INTO `gce_function` VALUES ('0102', '01', '集群节点管理', 1, 0, 0);
INSERT INTO `gce_function` VALUES ('010201', '0102', '列表', 1, 1, 0);
INSERT INTO `gce_function` VALUES ('010202', '0102', '详情', 1, 1, 0);
INSERT INTO `gce_function` VALUES ('010203', '0102', '编辑主机', 1, 1, 0);
INSERT INTO `gce_function` VALUES ('010204', '0102', '删除', 1, 1, 0);
INSERT INTO `gce_function` VALUES ('010205', '0102', '暂停', 1, 1, 0);
INSERT INTO `gce_function` VALUES ('010206', '0102', '驱散', 1, 1, 0);
INSERT INTO `gce_function` VALUES ('010207', '0102', '激活', 1, 1, 0);
INSERT INTO `gce_function` VALUES ('0103', '01', '系统节点管理', 1, 0, 0);
INSERT INTO `gce_function` VALUES ('010301', '0103', '列表', 1, 1, 0);
INSERT INTO `gce_function` VALUES ('0104', '01', '角色管理', 1, 0, 0);
INSERT INTO `gce_function` VALUES ('010401', '0104', '创建角色', 1, 1, 0);
INSERT INTO `gce_function` VALUES ('010402', '0104', '编辑角色', 1, 1, 0);
INSERT INTO `gce_function` VALUES ('010403', '0104', '删除角色', 1, 1, 0);
INSERT INTO `gce_function` VALUES ('010404', '0104', '角色详情', 1, 1, 0);
INSERT INTO `gce_function` VALUES ('010405', '0104', '权限分配', 1, 1, 0);
INSERT INTO `gce_function` VALUES ('010406', '0104', '角色列表', 1, 1, 0);
INSERT INTO `gce_function` VALUES ('02', NULL, '容器管理', 1, 0, 0);
INSERT INTO `gce_function` VALUES ('0201', '02', '命名空间', 1, 0, 0);
INSERT INTO `gce_function` VALUES ('020101', '0201', '列表', 1, 1, 0);
INSERT INTO `gce_function` VALUES ('020102', '0201', '新增', 1, 1, 0);
INSERT INTO `gce_function` VALUES ('020103', '0201', '删除', 1, 1, 0);
INSERT INTO `gce_function` VALUES ('0202', '02', '无状态', 1, 0, 0);
INSERT INTO `gce_function` VALUES ('020201', '0202', '列表', 1, 1, 0);
INSERT INTO `gce_function` VALUES ('020202', '0202', '详情', 1, 1, 0);
INSERT INTO `gce_function` VALUES ('020203', '0202', '部署服务', 1, 1, 0);
INSERT INTO `gce_function` VALUES ('020204', '0202', '编辑', 1, 1, 0);
INSERT INTO `gce_function` VALUES ('020205', '0202', '查看yaml', 1, 1, 0);
INSERT INTO `gce_function` VALUES ('020206', '0202', '停止编排', 1, 1, 0);
INSERT INTO `gce_function` VALUES ('020207', '0202', '恢复编排', 1, 1, 0);
INSERT INTO `gce_function` VALUES ('020208', '0202', '添加服务', 1, 1, 0);
INSERT INTO `gce_function` VALUES ('020209', '0202', '删除服务', 1, 1, 0);
INSERT INTO `gce_function` VALUES ('0203', '02', '容器组', 1, 0, 0);
INSERT INTO `gce_function` VALUES ('020301', '0203', '列表', 1, 1, 0);
INSERT INTO `gce_function` VALUES ('020302', '0203', '详情', 1, 1, 0);
INSERT INTO `gce_function` VALUES ('020303', '0203', '查看yaml', 1, 1, 0);
INSERT INTO `gce_function` VALUES ('020304', '0203', '删除', 1, 1, 0);

-- ----------------------------
-- Table structure for gce_function_api
-- ----------------------------
DROP TABLE IF EXISTS `gce_function_api`;
CREATE TABLE `gce_function_api`  (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `function_id` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `api_id` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `function_api__idx_01`(`function_id`) USING BTREE,
  INDEX `function_api__idx_02`(`api_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 45 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of gce_function_api
-- ----------------------------
INSERT INTO `gce_function_api` VALUES (1, '010101', '0101');
INSERT INTO `gce_function_api` VALUES (2, '010102', '0102');
INSERT INTO `gce_function_api` VALUES (3, '010103', '0106');
INSERT INTO `gce_function_api` VALUES (4, '010103', '0301');
INSERT INTO `gce_function_api` VALUES (5, '010103', '0401');
INSERT INTO `gce_function_api` VALUES (6, '010104', '0103');
INSERT INTO `gce_function_api` VALUES (7, '010105', '0104');
INSERT INTO `gce_function_api` VALUES (8, '010106', '0107');
INSERT INTO `gce_function_api` VALUES (9, '010106', '0108');
INSERT INTO `gce_function_api` VALUES (10, '010201', '0601');
INSERT INTO `gce_function_api` VALUES (11, '010202', '0602');
INSERT INTO `gce_function_api` VALUES (12, '010203', '0603');
INSERT INTO `gce_function_api` VALUES (13, '010204', '0604');
INSERT INTO `gce_function_api` VALUES (14, '010205', '0605');
INSERT INTO `gce_function_api` VALUES (15, '010206', '0607');
INSERT INTO `gce_function_api` VALUES (16, '010207', '0606');
INSERT INTO `gce_function_api` VALUES (17, '010301', '0701');
INSERT INTO `gce_function_api` VALUES (18, '010401', '1201');
INSERT INTO `gce_function_api` VALUES (19, '010402', '1202');
INSERT INTO `gce_function_api` VALUES (20, '010403', '1203');
INSERT INTO `gce_function_api` VALUES (21, '010404', '1208');
INSERT INTO `gce_function_api` VALUES (22, '010404', '1101');
INSERT INTO `gce_function_api` VALUES (23, '010405', '1206');
INSERT INTO `gce_function_api` VALUES (24, '010405', '1101');
INSERT INTO `gce_function_api` VALUES (25, '010406', '1207');
INSERT INTO `gce_function_api` VALUES (26, '020101', '0504');
INSERT INTO `gce_function_api` VALUES (27, '020101', '0105');
INSERT INTO `gce_function_api` VALUES (28, '020102', '0501');
INSERT INTO `gce_function_api` VALUES (29, '020103', '0502');
INSERT INTO `gce_function_api` VALUES (30, '020201', '0201');
INSERT INTO `gce_function_api` VALUES (31, '020202', '0207');
INSERT INTO `gce_function_api` VALUES (32, '020203', '0202');
INSERT INTO `gce_function_api` VALUES (33, '020204', '0203');
INSERT INTO `gce_function_api` VALUES (34, '020205', '0209');
INSERT INTO `gce_function_api` VALUES (35, '020206', '0208');
INSERT INTO `gce_function_api` VALUES (36, '020207', '0210');
INSERT INTO `gce_function_api` VALUES (37, '020208', '0204');
INSERT INTO `gce_function_api` VALUES (38, '020209', '0205');
INSERT INTO `gce_function_api` VALUES (39, '020301', '0801');
INSERT INTO `gce_function_api` VALUES (40, '020302', '0808');
INSERT INTO `gce_function_api` VALUES (41, '020303', '0808');
INSERT INTO `gce_function_api` VALUES (42, '020303', '0809');
INSERT INTO `gce_function_api` VALUES (43, '020304', '0808');
INSERT INTO `gce_function_api` VALUES (44, '020304', '0807');

-- ----------------------------
-- Table structure for gce_log
-- ----------------------------
DROP TABLE IF EXISTS `gce_log`;
CREATE TABLE `gce_log`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '日志ID，自增',
  `user_id` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '用户ID，关联用户表',
  `user_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `fun_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '功能名称，关联功能表',
  `fun_path` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '功能路径',
  `ip` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '操作人IP',
  `object_id` char(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '操作对象ID，比如虚拟机ID/用户ID等',
  `object_name` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `result` tinyint(4) NOT NULL DEFAULT 0 COMMENT '操作结果，命令执行成功与否,0 未知；1 成功；2 失败；3 进行中',
  `start_time` datetime NULL DEFAULT NULL COMMENT '开始时间',
  `end_time` datetime NULL DEFAULT NULL COMMENT '结束时间',
  `task_id` char(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '任务ID',
  `task_expect` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '期望结果',
  `timeout` int(11) NULL DEFAULT NULL,
  `error_code` varchar(256) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '错误代码',
  `remark` varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注，如记录操作原因',
  `description` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '描述，其他相关描述',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of gce_log
-- ----------------------------

-- ----------------------------
-- Table structure for gce_role
-- ----------------------------
DROP TABLE IF EXISTS `gce_role`;
CREATE TABLE `gce_role`  (
  `id` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `state` int(2) NULL DEFAULT NULL,
  `creator` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `create_date` datetime NULL DEFAULT NULL,
  `description` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `create_user` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `modify_date` datetime NULL DEFAULT NULL,
  `modify_user` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of gce_role
-- ----------------------------

-- ----------------------------
-- Table structure for gce_role_function
-- ----------------------------
DROP TABLE IF EXISTS `gce_role_function`;
CREATE TABLE `gce_role_function`  (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `role_id` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `function_id` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `region_id` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `role_function_idx_01`(`role_id`) USING BTREE,
  INDEX `role_function_idx_02`(`function_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of gce_role_function
-- ----------------------------

-- ----------------------------
-- Table structure for gce_user_resource_auth
-- ----------------------------
DROP TABLE IF EXISTS `gce_user_resource_auth`;
CREATE TABLE `gce_user_resource_auth`  (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `user_id` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `region_id` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `resource_auth_type` tinyint(1) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of gce_user_resource_auth
-- ----------------------------

-- ----------------------------
-- Table structure for gce_user_role
-- ----------------------------
DROP TABLE IF EXISTS `gce_user_role`;
CREATE TABLE `gce_user_role`  (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `user_id` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `role_id` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of gce_user_role
-- ----------------------------

SET FOREIGN_KEY_CHECKS = 1;
