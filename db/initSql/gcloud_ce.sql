DROP DATABASE IF EXISTS gcloud_ce;
create database `gcloud_ce` default character set utf8mb4 collate utf8mb4_general_ci;
/*
 Navicat Premium Data Transfer

 Source Server         : 192.168.215.84
 Source Server Type    : MySQL
 Source Server Version : 50734
 Source Host           : 192.168.215.84:30306
 Source Schema         : gcloud_ce

 Target Server Type    : MySQL
 Target Server Version : 50734
 File Encoding         : 65001

 Date: 10/08/2021 16:33:41
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for gc_work_flow_batch
-- ----------------------------
DROP TABLE IF EXISTS `gc_work_flow_batch`;
CREATE TABLE `gc_work_flow_batch`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'ID，自增',
  `ptask_id` char(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '父任务id',
  `flow_id` int(10) NOT NULL,
  `state` char(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'waiting、executing、success、fail',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of gc_work_flow_batch
-- ----------------------------

-- ----------------------------
-- Table structure for gc_work_flow_command_params_template
-- ----------------------------
DROP TABLE IF EXISTS `gc_work_flow_command_params_template`;
CREATE TABLE `gc_work_flow_command_params_template`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'ID，自增',
  `exec_command` char(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'command class类名',
  `param_type` char(10) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '参数类型，req 或 res',
  `field_name` char(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '字段名称',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of gc_work_flow_command_params_template
-- ----------------------------

-- ----------------------------
-- Table structure for gc_work_flow_command_value_template
-- ----------------------------
DROP TABLE IF EXISTS `gc_work_flow_command_value_template`;
CREATE TABLE `gc_work_flow_command_value_template`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'ID，自增',
  `flow_type_code` char(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '任务流类型code',
  `step_id` int(10) NOT NULL COMMENT '任务流模板stepID',
  `field_name` char(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '字段名称',
  `from_step_id` int(10) NOT NULL COMMENT '任务流模板stepID,0代表work_flow_instance paramsJson中获取',
  `from_param_type` char(10) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'req、res',
  `from_field_name` char(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '根据这个字段获取',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 40 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of gc_work_flow_command_value_template
-- ----------------------------
INSERT INTO `gc_work_flow_command_value_template` VALUES (1, 'CreateClusterWorkflow', 1, '', 0, '', '');
INSERT INTO `gc_work_flow_command_value_template` VALUES (2, 'CreateClusterWorkflow', 2, 'platformType', 1, 'res', 'platformType');
INSERT INTO `gc_work_flow_command_value_template` VALUES (3, 'CreateClusterWorkflow', 2, 'clusterNodeInfos', 1, 'res', 'clusterNodeInfos');
INSERT INTO `gc_work_flow_command_value_template` VALUES (4, 'CreateClusterWorkflow', 2, 'platformId', 1, 'res', 'platformId');
INSERT INTO `gc_work_flow_command_value_template` VALUES (5, 'CreateClusterWorkflow', 2, 'createInfo', 1, 'res', 'createInfo');
INSERT INTO `gc_work_flow_command_value_template` VALUES (6, 'CreateClusterWorkflow', 2, 'clusterId', 1, 'res', 'clusterId');
INSERT INTO `gc_work_flow_command_value_template` VALUES (7, 'CreateClusterWorkflow', 3, 'platformType', 1, 'res', 'platformType');
INSERT INTO `gc_work_flow_command_value_template` VALUES (8, 'CreateClusterWorkflow', 3, 'clusterNodeInfos', 1, 'res', 'clusterNodeInfos');
INSERT INTO `gc_work_flow_command_value_template` VALUES (9, 'CreateClusterWorkflow', 3, 'clusterId', 1, 'res', 'clusterId');
INSERT INTO `gc_work_flow_command_value_template` VALUES (10, 'CreateClusterWorkflow', 3, 'createInfo', 1, 'res', 'createInfo');
INSERT INTO `gc_work_flow_command_value_template` VALUES (11, 'CreateClusterWorkflow', 4, 'clusterId', 1, 'res', 'clusterId');
INSERT INTO `gc_work_flow_command_value_template` VALUES (12, 'CreateClusterPhysicalWorkflow', 1, 'clusterNodeInfos', 0, '', 'clusterNodeInfos');
INSERT INTO `gc_work_flow_command_value_template` VALUES (13, 'CreateClusterPhysicalWorkflow', 2, 'createInfo', 0, '', 'createInfo');
INSERT INTO `gc_work_flow_command_value_template` VALUES (14, 'CreateClusterPhysicalWorkflow', 2, 'clusterId', 0, '', 'clusterId');
INSERT INTO `gc_work_flow_command_value_template` VALUES (15, 'CreateClusterPhysicalWorkflow', 2, 'platformId', 0, '', 'platformId');
INSERT INTO `gc_work_flow_command_value_template` VALUES (16, 'CreateClusterPhysicalWorkflow', 3, 'agentCmd', 2, 'res', 'agentCmd');
INSERT INTO `gc_work_flow_command_value_template` VALUES (17, 'CreateClusterPhysicalWorkflow', 3, 'clusterNodeInfos', 0, '', 'clusterNodeInfos');
INSERT INTO `gc_work_flow_command_value_template` VALUES (18, 'CreateClusterPhysicalWorkflow', 3, 'clusterId', 0, '', 'clusterId');
INSERT INTO `gc_work_flow_command_value_template` VALUES (19, 'AttachPhysicalNodesWorkflow', 1, 'clusterNodeInfos', 0, '', 'clusterNodeInfos');
INSERT INTO `gc_work_flow_command_value_template` VALUES (20, 'AttachPhysicalNodesWorkflow', 1, 'agentCmd', 0, '', 'agentCmd');
INSERT INTO `gc_work_flow_command_value_template` VALUES (21, 'AttachPhysicalNodesWorkflow', 1, 'clusterId', 0, '', 'clusterId');
INSERT INTO `gc_work_flow_command_value_template` VALUES (22, 'AttachPhysicalNodeWorkflow', 1, 'clusterNodeInfo', 0, '', 'batchParams');
INSERT INTO `gc_work_flow_command_value_template` VALUES (23, 'AttachPhysicalNodeWorkflow', 1, 'agentCmd', 0, '', 'agentCmd');
INSERT INTO `gc_work_flow_command_value_template` VALUES (24, 'AttachPhysicalNodeWorkflow', 1, 'clusterId', 0, '', 'clusterId');
INSERT INTO `gc_work_flow_command_value_template` VALUES (25, 'AttachPhysicalNodeWorkflow', 1, 'clusterId', 0, '', 'clusterId');
INSERT INTO `gc_work_flow_command_value_template` VALUES (26, 'AddNodesWorkflow', 1, '', 0, '', '');
INSERT INTO `gc_work_flow_command_value_template` VALUES (27, 'AddNodesWorkflow', 2, 'platformType', 1, 'res', 'platformType');
INSERT INTO `gc_work_flow_command_value_template` VALUES (28, 'AddNodesWorkflow', 2, 'clusterNodeInfos', 1, 'res', 'clusterNodeInfos');
INSERT INTO `gc_work_flow_command_value_template` VALUES (29, 'AddNodesWorkflow', 2, 'platformId', 1, 'res', 'platformId');
INSERT INTO `gc_work_flow_command_value_template` VALUES (30, 'AddNodesWorkflow', 2, 'clusterId', 1, 'res', 'clusterId');
INSERT INTO `gc_work_flow_command_value_template` VALUES (31, 'AddNodesWorkflow', 3, 'platformType', 1, 'res', 'platformType');
INSERT INTO `gc_work_flow_command_value_template` VALUES (32, 'AddNodesWorkflow', 3, 'clusterNodeInfos', 1, 'res', 'clusterNodeInfos');
INSERT INTO `gc_work_flow_command_value_template` VALUES (33, 'AddNodesWorkflow', 3, 'clusterId', 1, 'res', 'clusterId');
INSERT INTO `gc_work_flow_command_value_template` VALUES (34, 'AddNodesWorkflow', 3, 'clusterId', 1, 'res', 'clusterId');
INSERT INTO `gc_work_flow_command_value_template` VALUES (35, 'AddNodesWorkflow', 4, 'clusterId', 1, 'res', 'clusterId');
INSERT INTO `gc_work_flow_command_value_template` VALUES (36, 'AddNodesPhysicalWorkflow', 1, 'clusterId', 0, '', 'clusterId');
INSERT INTO `gc_work_flow_command_value_template` VALUES (37, 'AddNodesPhysicalWorkflow', 2, 'agentCmd', 1, 'res', 'agentCmd');
INSERT INTO `gc_work_flow_command_value_template` VALUES (38, 'AddNodesPhysicalWorkflow', 2, 'clusterNodeInfos', 0, '', 'clusterNodeInfos');
INSERT INTO `gc_work_flow_command_value_template` VALUES (39, 'AddNodesPhysicalWorkflow', 2, 'clusterId', 0, '', 'clusterId');

-- ----------------------------
-- Table structure for gc_work_flow_instance
-- ----------------------------
DROP TABLE IF EXISTS `gc_work_flow_instance`;
CREATE TABLE `gc_work_flow_instance`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'ID，自增',
  `flow_type_code` char(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `state` char(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'executing、success、failure',
  `start_time` datetime NULL DEFAULT NULL COMMENT '开始时间',
  `end_time` datetime NULL DEFAULT NULL COMMENT '结束时间',
  `task_id` char(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `params_json` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '参数',
  `error_code` varchar(256) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `parent_flow_id` bigint(20) NULL DEFAULT NULL COMMENT '父工作流id',
  `parent_flow_step_id` int(10) NULL DEFAULT NULL COMMENT '父工作流中的stepid',
  `batch_params` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `topest_flow_task_id` char(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '最顶层任务id，根据这个字段判断是否在同一个任务流程',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of gc_work_flow_instance
-- ----------------------------

-- ----------------------------
-- Table structure for gc_work_flow_instance_step
-- ----------------------------
DROP TABLE IF EXISTS `gc_work_flow_instance_step`;
CREATE TABLE `gc_work_flow_instance_step`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'ID，自增',
  `flow_id` bigint(20) NOT NULL COMMENT '任务流实例ID',
  `template_step_id` int(10) NOT NULL COMMENT '模板表中的stepId',
  `task_id` char(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `exec_command` char(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'flow command class类名 或 flowTypeCode',
  `step_type` char(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'command、flow；flow时代表是工作流嵌套',
  `req_json` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT 'command请求参数json',
  `res_json` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '返回的结果json',
  `from_ids` char(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '上一级ID,逗号隔开，跟step_id对应',
  `y_to_ids` char(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '下一级ID,逗号隔开',
  `n_to_ids` char(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '下一级ID,逗号隔开',
  `state` char(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'waiting、executing、success、failure、rollbacking、rollbacked',
  `necessary` tinyint(1) NOT NULL DEFAULT 0 COMMENT '是否是必要条件，0 | 1；步骤执行失败时，1则执行rollback，0则执行nToIds',
  `start_time` datetime NULL DEFAULT NULL COMMENT '开始时间',
  `update_time` datetime NULL DEFAULT NULL COMMENT '修改时间',
  `rollback_start_time` datetime NULL DEFAULT NULL COMMENT '回滚开始时间',
  `rollback_update_time` datetime NULL DEFAULT NULL COMMENT '回滚修改时间',
  `rollback_task_id` char(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '回滚command的任务ID',
  `async` tinyint(1) NOT NULL DEFAULT 0 COMMENT '0同步/1异步',
  `rollback_async` tinyint(1) NOT NULL DEFAULT 0 COMMENT '0同步/1异步',
  `rollback_skip` tinyint(1) NOT NULL DEFAULT 0 COMMENT '0 不跳过，1跳过；是否跳过rollback代码逻辑',
  `rollback_fail_continue` tinyint(1) NOT NULL DEFAULT 0 COMMENT '0 流程回滚失败后不回滚，1 流程回滚失败后回滚；',
  `from_relation` char(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '父记录的关系（from_ids为多条记录时有效），FROM_ALL_DONE、FROM_ONE_DONE,默认FROM_ONE_DONE',
  `time_out` int(10) NULL DEFAULT 180 COMMENT '超时时间，单位秒，command时有效',
  `repeat_type` char(16) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'serial、parral',
  `repeat_index` int(10) NULL DEFAULT NULL,
  `visible` tinyint(1) NOT NULL DEFAULT 0 COMMENT '是否展示，0 | 1',
  `topest_flow_task_id` char(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '最顶层任务id，根据这个字段判断是否在同一个任务流程',
  `step_desc` varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '步骤描述',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of gc_work_flow_instance_step
-- ----------------------------

-- ----------------------------
-- Table structure for gc_work_flow_task
-- ----------------------------
DROP TABLE IF EXISTS `gc_work_flow_task`;
CREATE TABLE `gc_work_flow_task`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'ID，自增',
  `task_id` char(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '总流程任务的任务id',
  `flow_type_code` char(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '任务流类型code',
  `state` char(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'waiting、executing、success、fail',
  `batch_size` int(10) NULL DEFAULT 1,
  `user_id` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '用户ID',
  `region_id` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '所属区域id',
  `start_time` datetime NULL DEFAULT NULL COMMENT '开始时间',
  `end_time` datetime NULL DEFAULT NULL COMMENT '结束时间',
  `parent_flow_id` bigint(20) NULL DEFAULT NULL COMMENT '父工作流id',
  `parent_flow_step_id` int(10) NULL DEFAULT NULL COMMENT '父工作流中的stepid',
  `need_feedback_log` tinyint(1) NOT NULL DEFAULT 1 COMMENT '是否需要feedback日志，0 | 1',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of gc_work_flow_task
-- ----------------------------

-- ----------------------------
-- Table structure for gc_work_flow_template
-- ----------------------------
DROP TABLE IF EXISTS `gc_work_flow_template`;
CREATE TABLE `gc_work_flow_template`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'ID，自增',
  `flow_type_code` char(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '任务流类型代码',
  `step_id` int(10) NULL DEFAULT NULL COMMENT '步骤ID',
  `exec_command` char(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'flow command class类名 或 flowTypeCode',
  `step_type` char(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'command、flow；flow时代表是工作流嵌套',
  `from_ids` char(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '上一级ID,逗号隔开',
  `y_to_ids` char(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '下一级ID,逗号隔开',
  `n_to_ids` char(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '步骤为非必要条件且步骤执行失败时，下一步id',
  `necessary` tinyint(1) UNSIGNED ZEROFILL NOT NULL DEFAULT 0 COMMENT '是否是必要条件，0 | 1；步骤执行失败时，1则执行rollback，0则执行nToIds',
  `async` tinyint(1) NOT NULL DEFAULT 0 COMMENT '0 同步，1异步；一般stepType为flow时都为异步',
  `rollback_async` tinyint(1) NOT NULL DEFAULT 0 COMMENT '0 同步，1异步；',
  `rollback_skip` tinyint(1) NOT NULL DEFAULT 0 COMMENT '0 不跳过，1跳过；是否跳过rollback代码逻辑',
  `rollback_fail_continue` tinyint(1) NOT NULL DEFAULT 0 COMMENT '0 流程回滚失败后不回滚，1 流程回滚失败后回滚；',
  `from_relation` char(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '父记录的关系（from_ids为多条记录时有效），FROM_ALL_DONE、FROM_ONE_DONE,默认FROM_ONE_DONE',
  `repeat_type` char(16) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'serial、parral',
  `visible` tinyint(1) NOT NULL DEFAULT 0 COMMENT '是否展示，0 | 1',
  `step_desc` varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '步骤描述',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 17 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of gc_work_flow_template
-- ----------------------------
INSERT INTO `gc_work_flow_template` VALUES (1, 'CreateClusterWorkflow', 1, 'CreateClusterInitFlowCommand', 'command', NULL, '2', NULL, 1, 0, 0, 0, 0, 'FROM_ONE_DONE', NULL, 0, NULL);
INSERT INTO `gc_work_flow_template` VALUES (2, 'CreateClusterWorkflow', 2, 'CreateClusterPhysicalWorkflow', 'task_flow', '1', '3', NULL, 1, 1, 1, 0, 0, 'FROM_ONE_DONE', NULL, 0, NULL);
INSERT INTO `gc_work_flow_template` VALUES (3, 'CreateClusterWorkflow', 3, 'CreateClusterGCloud8Workflow', 'task_flow', '2', '4', NULL, 1, 1, 1, 0, 0, 'FROM_ONE_DONE', NULL, 0, NULL);
INSERT INTO `gc_work_flow_template` VALUES (4, 'CreateClusterWorkflow', 4, 'CreateClusterDoneFlowCommand', 'command', '3', NULL, NULL, 1, 0, 0, 0, 0, 'FROM_ONE_DONE', NULL, 0, NULL);
INSERT INTO `gc_work_flow_template` VALUES (5, 'CreateClusterPhysicalWorkflow', 1, 'CreateClusterPhysicalInitFlowCommand', 'command', NULL, '2', NULL, 1, 0, 0, 0, 0, 'FROM_ONE_DONE', NULL, 0, NULL);
INSERT INTO `gc_work_flow_template` VALUES (6, 'CreateClusterPhysicalWorkflow', 2, 'CreateProviderClusterFlowCommand', 'command', '1', '3', NULL, 1, 0, 1, 0, 0, 'FROM_ONE_DONE', NULL, 0, NULL);
INSERT INTO `gc_work_flow_template` VALUES (7, 'CreateClusterPhysicalWorkflow', 3, 'AttachPhysicalNodesWorkflow', 'task_flow', '2', '4', NULL, 1, 1, 0, 0, 0, 'FROM_ONE_DONE', NULL, 0, NULL);
INSERT INTO `gc_work_flow_template` VALUES (8, 'CreateClusterPhysicalWorkflow', 4, 'CreateClusterPhysicalDoneFlowCommand', 'command', '3', NULL, NULL, 1, 0, 0, 0, 0, 'FROM_ONE_DONE', NULL, 0, NULL);
INSERT INTO `gc_work_flow_template` VALUES (9, 'AttachPhysicalNodesWorkflow', 1, 'AttachPhysicalNodeWorkflow', 'task_flow', NULL, NULL, NULL, 1, 0, 0, 0, 0, 'FROM_ONE_DONE', NULL, 0, NULL);
INSERT INTO `gc_work_flow_template` VALUES (10, 'AttachPhysicalNodeWorkflow', 1, 'AttachPhysicalNodeFlowCommand', 'command', NULL, NULL, NULL, 1, 1, 0, 0, 0, 'FROM_ONE_DONE', NULL, 0, NULL);
INSERT INTO `gc_work_flow_template` VALUES (11, 'AddNodesWorkflow', 1, 'AddNodesInitFlowCommand', 'command', NULL, '2', NULL, 1, 0, 0, 0, 0, 'FROM_ONE_DONE', NULL, 0, NULL);
INSERT INTO `gc_work_flow_template` VALUES (12, 'AddNodesWorkflow', 2, 'AddNodesPhysicalWorkflow', 'task_flow', '1', '3', NULL, 1, 1, 0, 0, 0, 'FROM_ONE_DONE', NULL, 0, NULL);
INSERT INTO `gc_work_flow_template` VALUES (13, 'AddNodesWorkflow', 3, 'AddNodesGCloud8Workflow', 'task_flow', '2', '4', NULL, 1, 1, 0, 0, 0, 'FROM_ONE_DONE', NULL, 0, NULL);
INSERT INTO `gc_work_flow_template` VALUES (14, 'AddNodesWorkflow', 4, 'AddNodesDoneFlowCommand', 'command', '3', NULL, NULL, 1, 0, 0, 0, 0, 'FROM_ONE_DONE', NULL, 0, NULL);
INSERT INTO `gc_work_flow_template` VALUES (15, 'AddNodesPhysicalWorkflow', 1, 'AddNodesPhysicalInitFlowCommand', 'command', NULL, '2', NULL, 1, 0, 0, 0, 0, 'FROM_ONE_DONE', NULL, 0, NULL);
INSERT INTO `gc_work_flow_template` VALUES (16, 'AddNodesPhysicalWorkflow', 2, 'AttachPhysicalNodesWorkflow', 'task_flow', '1', NULL, NULL, 1, 1, 0, 0, 0, 'FROM_ONE_DONE', NULL, 0, NULL);

-- ----------------------------
-- Table structure for gc_work_flow_type
-- ----------------------------
DROP TABLE IF EXISTS `gc_work_flow_type`;
CREATE TABLE `gc_work_flow_type`  (
  `flow_type_code` char(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `flow_type_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `description` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `fault_rollback_continue` tinyint(1) NOT NULL COMMENT '0 不继续，1继续；rollback出错后是否继续rollback剩余步骤',
  PRIMARY KEY (`flow_type_code`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of gc_work_flow_type
-- ----------------------------

-- ----------------------------
-- Table structure for gce_cluster
-- ----------------------------
DROP TABLE IF EXISTS `gce_cluster`;
CREATE TABLE `gce_cluster`  (
  `id` varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `name` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `provider` int(11) NULL DEFAULT NULL,
  `provider_ref_id` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `tenant_id` varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `user_id` varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `provider_config_id` varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `state` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `insecure_command` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `cluster_idx01`(`provider`, `provider_ref_id`) USING BTREE,
  UNIQUE INDEX `gce_cluster_name_uindex`(`name`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of gce_cluster
-- ----------------------------
INSERT INTO `gce_cluster` VALUES ('7ae9dc0e-0388-468e-bea7-72b8decdaa2f', '95k8s', 1, 'c-8hgl4', 'f6d4b7a3-db29-476b-aaa2-392fad843442', 'cc81efe719bf4b05a3c21524ccf6c1ea', '1', 'creating', 'curl --insecure -sfL https://192.168.215.95:1443/v3/import/db2q9m9dw9qqj8ds6knzstzh8rnsltzm6dlvr6ttwgwbtr7nx66mqg.yaml | kubectl apply -f -');
INSERT INTO `gce_cluster` VALUES ('e0c98c82-09f0-4e37-825e-b5e57dffcaef', '35k8s', 1, 'c-6tzgb', 'f6d4b7a3-db29-476b-aaa2-392fad843442', 'cc81efe719bf4b05a3c21524ccf6c1ea', '1', 'creating', 'curl --insecure -sfL https://192.168.215.95:1443/v3/import/2wbxjjqcxljvxt8j4vtrp9svt5r78q27sf67688cj5q469r95z454k.yaml | kubectl apply -f -');
INSERT INTO `gce_cluster` VALUES ('f98e8871-ed77-4ab5-b619-142288202029', '112', 1, 'c-rnrr9', 'f6d4b7a3-db29-476b-aaa2-392fad843442', 'cc81efe719bf4b05a3c21524ccf6c1ea', '1', 'creating', 'curl --insecure -sfL https://192.168.215.95:1443/v3/import/dc84rqkw8qnxw7l6rg49dhpbspqd4bvhxv97qsqp9zbc2bs654ghgl.yaml | kubectl apply -f -');

-- ----------------------------
-- Table structure for gce_cluster_init
-- ----------------------------
DROP TABLE IF EXISTS `gce_cluster_init`;
CREATE TABLE `gce_cluster_init`  (
  `cluster_id` varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `handler` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `handler_hash` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `create_time` datetime NULL DEFAULT NULL,
  `last_handle_time` datetime NULL DEFAULT NULL,
  `info` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of gce_cluster_init
-- ----------------------------

-- ----------------------------
-- Table structure for gce_cluster_node
-- ----------------------------
DROP TABLE IF EXISTS `gce_cluster_node`;
CREATE TABLE `gce_cluster_node`  (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `cluster_id` varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `platform_type` int(2) NULL DEFAULT NULL,
  `node_id` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `container_name` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `platform_id` varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `cluster_node_unq_01`(`platform_type`, `node_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of gce_cluster_node
-- ----------------------------

-- ----------------------------
-- Table structure for gce_cluster_node_log
-- ----------------------------
DROP TABLE IF EXISTS `gce_cluster_node_log`;
CREATE TABLE `gce_cluster_node_log`  (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `cluster_id` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `state` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `info` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `create_time` datetime NULL DEFAULT NULL,
  `node_id` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of gce_cluster_node_log
-- ----------------------------

-- ----------------------------
-- Table structure for gce_container_node
-- ----------------------------
DROP TABLE IF EXISTS `gce_container_node`;
CREATE TABLE `gce_container_node`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `hostname` varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `ip` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `state` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 6 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of gce_container_node
-- ----------------------------
INSERT INTO `gce_container_node` VALUES (1, 'vm', '192.168.204.81', 1);
INSERT INTO `gce_container_node` VALUES (2, 'k8s-master', '192.168.215.87', 1);
INSERT INTO `gce_container_node` VALUES (3, 'sgcc-gce-6fb9b7c556-rcx2s', '10.244.37.127', 1);
INSERT INTO `gce_container_node` VALUES (4, 'sgcc-gce-5f4c945476-7p55m', '10.244.37.74', 1);
INSERT INTO `gce_container_node` VALUES (5, 'sgcc-gce-778c65fb5b-ppcsf', '10.244.37.98', 0);

-- ----------------------------
-- Table structure for gce_kubernetes_version
-- ----------------------------
DROP TABLE IF EXISTS `gce_kubernetes_version`;
CREATE TABLE `gce_kubernetes_version`  (
  `id` varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `name` varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `provider` int(10) NULL DEFAULT NULL,
  `provider_ref_id` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of gce_kubernetes_version
-- ----------------------------
INSERT INTO `gce_kubernetes_version` VALUES ('ae9d89a7-7978-11ea-b62a-fa163e111a6c', 'v1.15.11', 1, 'v1.15.11-rancher1-0');

-- ----------------------------
-- Table structure for gce_log
-- ----------------------------
DROP TABLE IF EXISTS `gce_log`;
CREATE TABLE `gce_log`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '日志ID，自增',
  `user_id` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '用户ID，关联用户表',
  `user_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `fun_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '功能名称，关联功能表',
  `fun_path` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '功能路径',
  `ip` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '操作人IP',
  `object_id` char(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '操作对象ID，比如虚拟机ID/用户ID等',
  `object_name` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `result` tinyint(4) NOT NULL DEFAULT 0 COMMENT '操作结果，命令执行成功与否,0 未知；1 成功；2 失败；3 进行中',
  `start_time` datetime NULL DEFAULT NULL COMMENT '开始时间',
  `end_time` datetime NULL DEFAULT NULL COMMENT '结束时间',
  `task_id` char(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '任务ID',
  `task_expect` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '期望结果',
  `timeout` int(11) NULL DEFAULT NULL,
  `error_code` varchar(256) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '错误代码',
  `remark` varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注，如记录操作原因',
  `description` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '描述，其他相关描述',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 15 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of gce_log
-- ----------------------------
INSERT INTO `gce_log` VALUES (1, NULL, NULL, '创建命名空间', '/namespace/create', '172.18.0.5', 'b9f9c896-c0c8-44af-8ff4-e14da8cc62a9', 'bb', 1, '2021-03-26 01:06:41', '2021-03-26 01:06:42', '2ebbdd91-b6e6-4b3b-a6e1-de3b26423ec5', '', NULL, NULL, NULL, NULL);
INSERT INTO `gce_log` VALUES (2, NULL, NULL, '创建命名空间', '/namespace/create', '172.18.0.5', '82de2dd6-a97d-4f17-8b35-3c238cb34bc1', 'aaa', 1, '2021-03-26 01:08:23', '2021-03-26 01:08:23', '58691ffe-4c80-44b5-a55d-6b64926b8aa5', '', NULL, NULL, NULL, NULL);
INSERT INTO `gce_log` VALUES (3, NULL, NULL, '创建命名空间', '/namespace/create', '127.0.0.1', '54d75801-34a4-4ad0-ab20-3d33f39a0d21', 'dddd', 1, '2021-03-26 01:07:42', '2021-03-26 01:07:43', '4ad2e16e-95ff-472b-9ffc-d6f80eaf6878', '', NULL, NULL, NULL, NULL);
INSERT INTO `gce_log` VALUES (4, NULL, NULL, '创建命名空间', '/namespace/create', '127.0.0.1', '00ea9b61-3c64-4f65-b88b-01eef779d5e7', 'ddd', 1, '2021-03-26 01:08:08', '2021-03-26 01:08:08', 'd1f5eb88-a62b-48e0-8430-7d3d205161a4', '', NULL, NULL, NULL, NULL);
INSERT INTO `gce_log` VALUES (5, NULL, NULL, '创建命名空间', '/namespace/create', '127.0.0.1', NULL, NULL, 2, '2021-03-26 01:10:41', '2021-03-26 01:10:41', '17f061a7-2ae8-412b-86fa-7cb6153e2d8d', '', NULL, '0020104', NULL, NULL);
INSERT INTO `gce_log` VALUES (6, NULL, NULL, '创建命名空间', '/namespace/create', '127.0.0.1', NULL, NULL, 2, '2021-03-26 01:11:01', '2021-03-26 01:11:01', '5074a33f-c207-4123-a49e-30fd9668a1e8', '', NULL, '0020104', NULL, NULL);
INSERT INTO `gce_log` VALUES (7, NULL, NULL, '创建命名空间', '/namespace/create', '127.0.0.1', '3f738c77-fb16-4267-837d-aa31a09fe464', 'xxxxx', 1, '2021-03-26 01:15:07', '2021-03-26 01:15:07', '35c0c05b-7d01-4172-87d5-61050fd074ef', '', NULL, NULL, NULL, NULL);
INSERT INTO `gce_log` VALUES (8, NULL, NULL, '创建命名空间', '/namespace/create', '127.0.0.1', '9ea28213-c484-43b6-8297-975b092fccba', '123', 1, '2021-03-26 01:17:45', '2021-03-26 01:17:46', 'dbedde25-4921-4ebb-8081-4fe7b699a395', '', NULL, NULL, NULL, NULL);
INSERT INTO `gce_log` VALUES (9, NULL, NULL, '创建命名空间', '/namespace/create', '172.18.0.5', 'bd03e36b-dce5-4645-9d39-2f64b8e7c5f5', 'ddd', 1, '2021-03-26 01:33:18', '2021-03-26 01:33:18', 'ed67b21c-2111-46e2-b7b7-37253ae638c9', '', NULL, NULL, NULL, NULL);
INSERT INTO `gce_log` VALUES (10, NULL, NULL, '创建命名空间', '/namespace/create', '172.18.0.5', '522dc2e6-4d22-4e57-9798-370ff29eb1f2', 'zzz', 1, '2021-03-31 13:55:49', '2021-03-31 13:55:52', 'a8ee9698-6c06-451d-97e8-c78391836ecf', '', NULL, NULL, NULL, NULL);
INSERT INTO `gce_log` VALUES (11, NULL, NULL, '创建命名空间', '/namespace/create', '172.18.0.5', NULL, NULL, 2, '2021-04-01 12:12:57', '2021-04-01 12:12:57', '829fdac5-b7d7-42c5-8064-e41661264c16', '', NULL, '0020104', NULL, NULL);
INSERT INTO `gce_log` VALUES (12, NULL, NULL, '创建命名空间', '/namespace/create', '172.18.0.5', 'ba3a9abd-0183-43d6-9916-242ff5c64c90', 'ttttt', 1, '2021-04-02 03:03:18', '2021-04-02 03:03:19', 'd47ec2a7-0b25-43a3-b6da-295f0bc6850f', '', NULL, NULL, NULL, NULL);
INSERT INTO `gce_log` VALUES (13, NULL, NULL, '创建命名空间', '/namespace/create', '172.18.0.5', '97c7a2ed-b45f-43fa-b435-3f085e5f50d2', '35kk', 1, '2021-04-02 03:05:59', '2021-04-02 03:05:59', 'b431f4b9-0fe1-4f27-a55d-315377ad0202', '', NULL, NULL, NULL, NULL);
INSERT INTO `gce_log` VALUES (14, NULL, NULL, '创建命名空间', '/namespace/create', '172.18.0.2', NULL, NULL, 2, '2021-04-07 09:34:37', '2021-04-07 09:34:37', 'd459ee05-cf45-4b97-85c0-8d90bea25a18', '', NULL, '0020104', NULL, NULL);

-- ----------------------------
-- Table structure for gce_message_tasks
-- ----------------------------
DROP TABLE IF EXISTS `gce_message_tasks`;
CREATE TABLE `gce_message_tasks`  (
  `task_id` varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `message` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `start_time` datetime NULL DEFAULT NULL,
  `end_time` datetime NULL DEFAULT NULL,
  `timeout_time` datetime NULL DEFAULT NULL,
  `status` int(4) NULL DEFAULT NULL,
  `mdata` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  PRIMARY KEY (`task_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of gce_message_tasks
-- ----------------------------

-- ----------------------------
-- Table structure for gce_namespace
-- ----------------------------
DROP TABLE IF EXISTS `gce_namespace`;
CREATE TABLE `gce_namespace`  (
  `id` varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `name` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `cluster_id` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `project_id` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `provider` int(11) NULL DEFAULT NULL,
  `provider_ref_id` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `provider_config_id` varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `state` tinyint(3) NULL DEFAULT NULL,
  `create_at` datetime NULL DEFAULT NULL,
  `user_id` varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `tenant_id` varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `namespace_idx01`(`provider_ref_id`, `provider`, `cluster_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of gce_namespace
-- ----------------------------
INSERT INTO `gce_namespace` VALUES ('03d90e95-b688-4e14-9a5d-7c114be594fa', 'cattle-prometheus', 'e0c98c82-09f0-4e37-825e-b5e57dffcaef', 'c-6tzgb:p-scxs6', 1, 'cattle-prometheus', '1', 1, '2021-04-16 14:44:01', 'cc81efe719bf4b05a3c21524ccf6c1ea', 'f6d4b7a3-db29-476b-aaa2-392fad843442');
INSERT INTO `gce_namespace` VALUES ('04cea591-377d-4b03-b12a-82c1addd1370', 'cattle-system', '7ae9dc0e-0388-468e-bea7-72b8decdaa2f', 'c-428c4:p-htt9c', 1, 'cattle-system', '1', 1, '2021-07-15 10:39:13', 'cc81efe719bf4b05a3c21524ccf6c1ea', 'f6d4b7a3-db29-476b-aaa2-392fad843442');
INSERT INTO `gce_namespace` VALUES ('178ec1a3-12c4-4b7b-9fda-4a2ffc421657', 'qwe', '7ae9dc0e-0388-468e-bea7-72b8decdaa2f', 'c-428c4:p-pgnz6', 1, 'qwe', '1', 1, '2021-08-08 11:55:07', 'cc81efe719bf4b05a3c21524ccf6c1ea', 'f6d4b7a3-db29-476b-aaa2-392fad843442');
INSERT INTO `gce_namespace` VALUES ('200c23ce-152d-4e79-a014-2578708e4760', 'cattle-prometheus', '7ae9dc0e-0388-468e-bea7-72b8decdaa2f', 'c-8hgl4:p-zfzt8', 1, 'cattle-prometheus', '1', 1, '2021-07-15 10:54:23', 'cc81efe719bf4b05a3c21524ccf6c1ea', 'f6d4b7a3-db29-476b-aaa2-392fad843442');
INSERT INTO `gce_namespace` VALUES ('22e815ec-b72f-4de6-b599-bfafe0820143', 'default', '7ae9dc0e-0388-468e-bea7-72b8decdaa2f', 'c-428c4:p-pgnz6', 1, 'default', '1', 1, '2021-07-15 10:23:56', 'cc81efe719bf4b05a3c21524ccf6c1ea', 'f6d4b7a3-db29-476b-aaa2-392fad843442');
INSERT INTO `gce_namespace` VALUES ('258ffa81-65ce-4dd6-8eb1-84e3834c4bef', 'kube-system', 'e0c98c82-09f0-4e37-825e-b5e57dffcaef', 'c-mtmmc:p-shqzp', 1, 'kube-system', '1', 1, '2021-04-16 08:46:56', 'cc81efe719bf4b05a3c21524ccf6c1ea', 'f6d4b7a3-db29-476b-aaa2-392fad843442');
INSERT INTO `gce_namespace` VALUES ('2810c2f3-501d-4d6e-bedf-4c053237ce49', 'kube-system', '7ae9dc0e-0388-468e-bea7-72b8decdaa2f', 'c-428c4:p-htt9c', 1, 'kube-system', '1', 1, '2021-07-15 10:23:54', 'cc81efe719bf4b05a3c21524ccf6c1ea', 'f6d4b7a3-db29-476b-aaa2-392fad843442');
INSERT INTO `gce_namespace` VALUES ('300a0a10-a9dd-4798-a333-24551f46fdcf', 'qqqq', 'e0c98c82-09f0-4e37-825e-b5e57dffcaef', 'c-6tzgb:p-z5dw6', 1, 'qqqq', '1', 1, '2021-08-08 11:20:03', 'cc81efe719bf4b05a3c21524ccf6c1ea', 'f6d4b7a3-db29-476b-aaa2-392fad843442');
INSERT INTO `gce_namespace` VALUES ('4ee971c9-2a75-4be7-867b-afd3cb25474b', 'sgcc', 'e0c98c82-09f0-4e37-825e-b5e57dffcaef', 'c-6tzgb:p-z5dw6', 1, 'sgcc', '1', 1, '2021-08-07 20:35:32', 'cc81efe719bf4b05a3c21524ccf6c1ea', 'f6d4b7a3-db29-476b-aaa2-392fad843442');
INSERT INTO `gce_namespace` VALUES ('520fdd45-973b-44d3-9afb-0aacecbc549f', 'kube-node-lease', 'e0c98c82-09f0-4e37-825e-b5e57dffcaef', 'c-mtmmc:p-shqzp', 1, 'kube-node-lease', '1', 1, '2021-04-16 08:46:56', 'cc81efe719bf4b05a3c21524ccf6c1ea', 'f6d4b7a3-db29-476b-aaa2-392fad843442');
INSERT INTO `gce_namespace` VALUES ('5e80ad02-be6e-4f06-9fdd-67029189ddb8', 'kube-public', '7ae9dc0e-0388-468e-bea7-72b8decdaa2f', 'c-428c4:p-htt9c', 1, 'kube-public', '1', 1, '2021-07-15 10:23:54', 'cc81efe719bf4b05a3c21524ccf6c1ea', 'f6d4b7a3-db29-476b-aaa2-392fad843442');
INSERT INTO `gce_namespace` VALUES ('60c0fa1b-6225-4976-8398-d4c5eca62816', 'sgcc', '7ae9dc0e-0388-468e-bea7-72b8decdaa2f', 'c-8hgl4:p-wqsrt', 1, 'sgcc', '1', 1, '2021-08-08 15:40:20', 'cc81efe719bf4b05a3c21524ccf6c1ea', 'f6d4b7a3-db29-476b-aaa2-392fad843442');
INSERT INTO `gce_namespace` VALUES ('6cc4f62d-19c2-482c-adbe-9b5d6bc30e3b', 'velero', 'e0c98c82-09f0-4e37-825e-b5e57dffcaef', 'c-6tzgb:p-z5dw6', 1, 'velero', '1', 1, '2021-08-08 18:18:00', 'cc81efe719bf4b05a3c21524ccf6c1ea', 'f6d4b7a3-db29-476b-aaa2-392fad843442');
INSERT INTO `gce_namespace` VALUES ('6d5b766c-e034-4a6b-a276-8495662b9a21', 'mysql', 'e0c98c82-09f0-4e37-825e-b5e57dffcaef', 'c-6tzgb:p-x95b9', 1, 'mysql', '1', 1, '2021-08-10 11:29:48', 'cc81efe719bf4b05a3c21524ccf6c1ea', 'f6d4b7a3-db29-476b-aaa2-392fad843442');
INSERT INTO `gce_namespace` VALUES ('83b73a3c-b8f0-4b8e-a5b0-a022bc03a500', 'kube-public', 'e0c98c82-09f0-4e37-825e-b5e57dffcaef', 'c-mtmmc:p-shqzp', 1, 'kube-public', '1', 1, '2021-04-16 08:46:56', 'cc81efe719bf4b05a3c21524ccf6c1ea', 'f6d4b7a3-db29-476b-aaa2-392fad843442');
INSERT INTO `gce_namespace` VALUES ('851964be-84fa-4f02-9bfb-5a3468264fd4', 'cattle-system', 'e0c98c82-09f0-4e37-825e-b5e57dffcaef', 'c-mtmmc:p-shqzp', 1, 'cattle-system', '1', 1, '2021-04-16 08:50:49', 'cc81efe719bf4b05a3c21524ccf6c1ea', 'f6d4b7a3-db29-476b-aaa2-392fad843442');
INSERT INTO `gce_namespace` VALUES ('97419e5b-906c-4e97-b6c2-0401ac6cce79', 'kube-node-lease', '7ae9dc0e-0388-468e-bea7-72b8decdaa2f', 'c-428c4:p-htt9c', 1, 'kube-node-lease', '1', 1, '2021-07-15 10:23:54', 'cc81efe719bf4b05a3c21524ccf6c1ea', 'f6d4b7a3-db29-476b-aaa2-392fad843442');
INSERT INTO `gce_namespace` VALUES ('a0e5934f-97d9-47a2-abc0-e45484b42ae8', 'velero', '7ae9dc0e-0388-468e-bea7-72b8decdaa2f', 'c-8hgl4:p-wqsrt', 1, 'velero', '1', 1, '2021-08-08 11:53:03', 'cc81efe719bf4b05a3c21524ccf6c1ea', 'f6d4b7a3-db29-476b-aaa2-392fad843442');
INSERT INTO `gce_namespace` VALUES ('b2792ae0-476a-47d7-965b-d585608158fe', 'default', 'e0c98c82-09f0-4e37-825e-b5e57dffcaef', 'c-mtmmc:p-54ggp', 1, 'default', '1', 1, '2021-04-16 08:46:59', 'cc81efe719bf4b05a3c21524ccf6c1ea', 'f6d4b7a3-db29-476b-aaa2-392fad843442');

-- ----------------------------
-- Table structure for gce_network_plugin
-- ----------------------------
DROP TABLE IF EXISTS `gce_network_plugin`;
CREATE TABLE `gce_network_plugin`  (
  `id` varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `name` varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `provider` int(10) NULL DEFAULT NULL,
  `providerRefId` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of gce_network_plugin
-- ----------------------------
INSERT INTO `gce_network_plugin` VALUES ('c5d98ab5-7978-11ea-b62a-fa163e111a6c', 'Flannel', 1, 'flannel');
INSERT INTO `gce_network_plugin` VALUES ('d41dfd51-7978-11ea-b62a-fa163e111a6c', 'Calico', 1, 'calico');
INSERT INTO `gce_network_plugin` VALUES ('d91548ff-619f-11ea-b62a-fa163e111a6d', 'Weave', 1, 'weave');
INSERT INTO `gce_network_plugin` VALUES ('e7fe2a74-7978-11ea-b62a-fa163e111a6c', 'Canal (支持网络隔离)', 1, 'canal');

-- ----------------------------
-- Table structure for gce_platform
-- ----------------------------
DROP TABLE IF EXISTS `gce_platform`;
CREATE TABLE `gce_platform`  (
  `id` varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `type` int(11) NULL DEFAULT NULL COMMENT '0 host, 1 gcloud8',
  `endpoint` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `access_key` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `secret_key` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `config` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of gce_platform
-- ----------------------------
INSERT INTO `gce_platform` VALUES ('1', 0, NULL, NULL, NULL, NULL);

-- ----------------------------
-- Table structure for gce_provider_config
-- ----------------------------
DROP TABLE IF EXISTS `gce_provider_config`;
CREATE TABLE `gce_provider_config`  (
  `id` varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `provider` int(11) NULL DEFAULT NULL,
  `endpoint` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `insecure` tinyint(1) NULL DEFAULT NULL,
  `access_key` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `secret_key` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `config` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `platform_id` varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `platform_ref_type` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `platform_ref_id` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `config_ref_type` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `config_ref_id` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of gce_provider_config
-- ----------------------------
INSERT INTO `gce_provider_config` VALUES ('1', NULL, 'https://192.168.215.95:1443/v3', 1, 'token-lfznr', 'zbr85k6c8swg66qd2p5s56p7f4bltfrtvvq9xr4kkbdwqx4w6dlqhr', NULL, '1', NULL, NULL, NULL, NULL);

-- ----------------------------
-- Table structure for gce_pv
-- ----------------------------
DROP TABLE IF EXISTS `gce_pv`;
CREATE TABLE `gce_pv`  (
  `id` varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `name` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `cluster_id` varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `volume_source` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `provider` int(11) NULL DEFAULT NULL,
  `provider_ref_id` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `state` tinyint(3) NULL DEFAULT NULL,
  `create_at` datetime NULL DEFAULT NULL,
  `user_id` varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `tenant_id` varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of gce_pv
-- ----------------------------
INSERT INTO `gce_pv` VALUES ('06e1f90b-4828-449d-902b-f305efd9e830', 'pv0326', 'ce57c962-c9b4-46cf-ae32-5ebf0fc29e98', 'hostPath', 1, 'pv0326', 1, '2021-03-26 08:15:21', 'cc81efe719bf4b05a3c21524ccf6c1ea', 'f6d4b7a3-db29-476b-aaa2-392fad843442');
INSERT INTO `gce_pv` VALUES ('08a76c99-a487-4e0b-98de-5802e7af6f71', 'mysql', '0983201a-a326-45f6-a518-8de1981253d9', 'local', 1, 'mysql', 1, '2021-03-29 02:06:40', 'cc81efe719bf4b05a3c21524ccf6c1ea', 'f6d4b7a3-db29-476b-aaa2-392fad843442');
INSERT INTO `gce_pv` VALUES ('0dc386dc-89c1-4ddc-b81e-cf80d58d4284', 'ggg-40e92db1-5153-43ad-be33-a92999ea5f9a', 'a9e59a0d-7513-4f9e-8bbf-770d2eca4b1f', 'hostPath', 1, 'ggg-40e92db1-5153-43ad-be33-a92999ea5f9a', 1, '2021-03-26 03:35:17', 'cc81efe719bf4b05a3c21524ccf6c1ea', 'f6d4b7a3-db29-476b-aaa2-392fad843442');
INSERT INTO `gce_pv` VALUES ('114b11b6-9153-452d-95ce-4dc7a466b917', 'redis-data-pv', 'b9ce6b59-b8a5-4bba-b241-144a6c391f2c', 'local', 1, 'redis-data-pv', 1, '2021-06-24 03:39:26', 'cc81efe719bf4b05a3c21524ccf6c1ea', 'f6d4b7a3-db29-476b-aaa2-392fad843442');
INSERT INTO `gce_pv` VALUES ('13a8f724-75b7-4d73-9052-f61947edb2a1', 'liang-wordpressdb-pv', 'ce57c962-c9b4-46cf-ae32-5ebf0fc29e95', 'local', 1, 'liang-wordpressdb-pv', 1, '2021-04-02 07:01:51', 'cc81efe719bf4b05a3c21524ccf6c1ea', 'f6d4b7a3-db29-476b-aaa2-392fad843442');
INSERT INTO `gce_pv` VALUES ('182ae692-ae92-4b9d-8069-fcfd865ddb8f', 'wordpress-pv', 'b9ce6b59-b8a5-4bba-b241-144a6c391f2c', 'local', 1, 'wordpress-pv', 1, '2021-07-12 01:36:54', 'cc81efe719bf4b05a3c21524ccf6c1ea', 'f6d4b7a3-db29-476b-aaa2-392fad843442');
INSERT INTO `gce_pv` VALUES ('1b4a7a94-6711-4617-b0eb-18b336f1e4d0', 'liang-nginx-pv', 'b9ce6b59-b8a5-4bba-b241-144a6c391f2c', 'local', 1, 'liang-nginx-pv', 1, '2021-04-19 02:39:33', 'cc81efe719bf4b05a3c21524ccf6c1ea', 'f6d4b7a3-db29-476b-aaa2-392fad843442');
INSERT INTO `gce_pv` VALUES ('2776d77f-ffeb-4d12-b462-99848d98a363', 'pv-mysql-liang', '7ae9dc0e-0388-468e-bea7-72b8decdaa2f', 'local', 1, 'pv-mysql-liang', 1, '2021-08-10 11:22:52', 'cc81efe719bf4b05a3c21524ccf6c1ea', 'f6d4b7a3-db29-476b-aaa2-392fad843442');
INSERT INTO `gce_pv` VALUES ('28fd6a24-2718-43c2-8d97-0a57a44c5f6c', 'liang-nginx-pv', 'a25c2d8b-a588-4cba-be34-ef90fa465fad', 'local', 1, 'liang-nginx-pv', 1, '2021-07-06 16:15:24', 'cc81efe719bf4b05a3c21524ccf6c1ea', 'f6d4b7a3-db29-476b-aaa2-392fad843442');
INSERT INTO `gce_pv` VALUES ('2acf7cf2-10c5-4921-af37-7d33bacd7b02', 'mysql', 'ce57c962-c9b4-46cf-ae32-5ebf0fc29e98', 'local', 1, 'mysql', 1, '2021-03-29 03:03:15', 'cc81efe719bf4b05a3c21524ccf6c1ea', 'f6d4b7a3-db29-476b-aaa2-392fad843442');
INSERT INTO `gce_pv` VALUES ('362196a4-df06-4b29-9b66-30585fcb78f8', 'mysql-pv', '9d58faf3-4040-49f8-8890-944577ca6285', 'local', 1, 'mysql-pv', 1, '2021-07-20 09:14:03', 'cc81efe719bf4b05a3c21524ccf6c1ea', 'f6d4b7a3-db29-476b-aaa2-392fad843442');
INSERT INTO `gce_pv` VALUES ('3ab94855-3e5d-473d-a6c0-c94580c67ece', 'liang-nginx-pv', '9d58faf3-4040-49f8-8890-944577ca6285', 'local', 1, 'liang-nginx-pv', 1, '2021-07-05 14:55:50', 'cc81efe719bf4b05a3c21524ccf6c1ea', 'f6d4b7a3-db29-476b-aaa2-392fad843442');
INSERT INTO `gce_pv` VALUES ('3c3be1df-4be0-4e54-9b3b-675aee1ee382', 'liang-wordpress-pv', 'ce57c962-c9b4-46cf-ae32-5ebf0fc29e95', 'local', 1, 'liang-wordpress-pv', 1, '2021-04-02 07:00:10', 'cc81efe719bf4b05a3c21524ccf6c1ea', 'f6d4b7a3-db29-476b-aaa2-392fad843442');
INSERT INTO `gce_pv` VALUES ('40f38d2d-9064-463f-a917-bc275e316bcb', 'liang-util-pv', 'b9ce6b59-b8a5-4bba-b241-144a6c391f5c', 'local', 1, 'liang-util-pv', 1, '2021-05-28 01:25:29', 'cc81efe719bf4b05a3c21524ccf6c1ea', 'f6d4b7a3-db29-476b-aaa2-392fad843442');
INSERT INTO `gce_pv` VALUES ('4de9ba58-8127-49ab-b8a3-e15a53ded9da', 'pv4', 'ce57c962-c9b4-46cf-ae32-5ebf0fc29e98', 'local', 1, 'pv4', 1, '2021-03-30 03:36:10', 'cc81efe719bf4b05a3c21524ccf6c1ea', 'f6d4b7a3-db29-476b-aaa2-392fad843442');
INSERT INTO `gce_pv` VALUES ('51197f87-8bc9-4565-9202-91d375407660', 'redis-config-pv', 'b9ce6b59-b8a5-4bba-b241-144a6c391f5c', 'local', 1, 'redis-config-pv', 1, '2021-06-24 03:51:46', 'cc81efe719bf4b05a3c21524ccf6c1ea', 'f6d4b7a3-db29-476b-aaa2-392fad843442');
INSERT INTO `gce_pv` VALUES ('5773e7a1-8898-4927-a439-b0dc058c06f4', 'movemysql', 'ce57c962-c9b4-46cf-ae32-5ebf0fc29e98', 'hostPath', 1, 'movemysql', 1, '2021-03-30 06:36:13', 'cc81efe719bf4b05a3c21524ccf6c1ea', 'f6d4b7a3-db29-476b-aaa2-392fad843442');
INSERT INTO `gce_pv` VALUES ('629e0914-7a50-44c6-879b-ea107bcf813e', 'pvc-d8dd76d8-3fbc-4725-99f7-af8e139ce657', 'b9ce6b59-b8a5-4bba-b241-144a6c391f2c', 'nfs', 1, 'pvc-d8dd76d8-3fbc-4725-99f7-af8e139ce657', 1, '2021-07-16 06:54:44', 'cc81efe719bf4b05a3c21524ccf6c1ea', 'f6d4b7a3-db29-476b-aaa2-392fad843442');
INSERT INTO `gce_pv` VALUES ('70daad90-6031-40ee-828c-101b4e186416', 'pv3', 'ce57c962-c9b4-46cf-ae32-5ebf0fc29e98', 'local', 1, 'pv3', 1, '2021-03-26 02:55:15', 'cc81efe719bf4b05a3c21524ccf6c1ea', 'f6d4b7a3-db29-476b-aaa2-392fad843442');
INSERT INTO `gce_pv` VALUES ('790b7a1e-42ae-4229-927c-eb7f8ae03331', 'redis-data-pv', 'b9ce6b59-b8a5-4bba-b241-144a6c391f5c', 'local', 1, 'redis-data-pv', 1, '2021-06-24 03:40:03', 'cc81efe719bf4b05a3c21524ccf6c1ea', 'f6d4b7a3-db29-476b-aaa2-392fad843442');
INSERT INTO `gce_pv` VALUES ('81157a1f-e2d7-4c5e-a7a0-faccd1be7a98', 'mysql', 'ce57c962-c9b4-46cf-ae32-5ebf0fc29e99', 'local', 1, 'mysql', 1, '2021-03-29 02:06:40', 'cc81efe719bf4b05a3c21524ccf6c1ea', 'f6d4b7a3-db29-476b-aaa2-392fad843442');
INSERT INTO `gce_pv` VALUES ('92318f6e-f1d3-4f0c-a531-8d17bf65b8a0', 'wordpress-pv', 'b9ce6b59-b8a5-4bba-b241-144a6c391f5c', 'local', 1, 'wordpress-pv', 1, '2021-07-12 03:17:30', 'cc81efe719bf4b05a3c21524ccf6c1ea', 'f6d4b7a3-db29-476b-aaa2-392fad843442');
INSERT INTO `gce_pv` VALUES ('929a77ba-f53e-46ef-afb4-dcc2d0908ac0', 'liang-util-pv', 'a25c2d8b-a588-4cba-be34-ef90fa465fad', 'local', 1, 'liang-util-pv', 1, '2021-07-06 16:18:37', 'cc81efe719bf4b05a3c21524ccf6c1ea', 'f6d4b7a3-db29-476b-aaa2-392fad843442');
INSERT INTO `gce_pv` VALUES ('9388c468-a65d-42b4-93d7-9f4951557893', 'liang-nginx-pv', 'ce57c962-c9b4-46cf-ae32-5ebf0fc29e95', 'local', 1, 'liang-nginx-pv', 1, '2021-04-01 08:49:47', 'cc81efe719bf4b05a3c21524ccf6c1ea', 'f6d4b7a3-db29-476b-aaa2-392fad843442');
INSERT INTO `gce_pv` VALUES ('94bf2666-da8a-42c6-862c-0ccf595b5e21', 'liang-nginx-pv', '', 'local', 1, 'liang-nginx-pv', 1, '2021-04-15 06:57:29', 'cc81efe719bf4b05a3c21524ccf6c1ea', 'f6d4b7a3-db29-476b-aaa2-392fad843442');
INSERT INTO `gce_pv` VALUES ('9e7cba75-498d-4b13-b19e-cad031dbc547', 'liang-util-pv', '9d58faf3-4040-49f8-8890-944577ca6285', 'local', 1, 'liang-util-pv', 1, '2021-07-05 14:52:25', 'cc81efe719bf4b05a3c21524ccf6c1ea', 'f6d4b7a3-db29-476b-aaa2-392fad843442');
INSERT INTO `gce_pv` VALUES ('a8534eee-bd46-413e-8336-dde5048c920d', 'pvtest0326', 'a9e59a0d-7513-4f9e-8bbf-770d2eca4b1f', 'hostPath', 1, 'pvtest0326', 1, '2021-03-26 03:27:41', 'cc81efe719bf4b05a3c21524ccf6c1ea', 'f6d4b7a3-db29-476b-aaa2-392fad843442');
INSERT INTO `gce_pv` VALUES ('afd3bb06-2092-4756-b746-4db381849504', 'wordpress-pv', '9d58faf3-4040-49f8-8890-944577ca6285', 'local', 1, 'wordpress-pv', 1, '2021-07-12 02:50:24', 'cc81efe719bf4b05a3c21524ccf6c1ea', 'f6d4b7a3-db29-476b-aaa2-392fad843442');
INSERT INTO `gce_pv` VALUES ('b571cc61-4e16-4ede-b576-2c1156a7d7fe', 'pvc-732eadf6-b03e-43dc-baf7-4456db161439', 'b9ce6b59-b8a5-4bba-b241-144a6c391f5c', 'nfs', 1, 'pvc-732eadf6-b03e-43dc-baf7-4456db161439', 1, '2021-07-16 03:03:57', 'cc81efe719bf4b05a3c21524ccf6c1ea', 'f6d4b7a3-db29-476b-aaa2-392fad843442');
INSERT INTO `gce_pv` VALUES ('b67a8b92-cd23-43d6-ae7b-e16456abd324', 'mo-mysql-pv', 'ce57c962-c9b4-46cf-ae32-5ebf0fc29e95', 'local', 1, 'mo-mysql-pv', 1, '2021-03-31 14:57:27', 'cc81efe719bf4b05a3c21524ccf6c1ea', 'f6d4b7a3-db29-476b-aaa2-392fad843442');
INSERT INTO `gce_pv` VALUES ('b9c776b0-fe59-432f-93d2-ecbdb1ef0532', 'pvtestmove', 'ce57c962-c9b4-46cf-ae32-5ebf0fc29e98', 'hostPath', 1, 'pvtestmove', 1, '2021-03-30 06:04:34', 'cc81efe719bf4b05a3c21524ccf6c1ea', 'f6d4b7a3-db29-476b-aaa2-392fad843442');
INSERT INTO `gce_pv` VALUES ('bacabffc-6c9c-4f83-a3fb-4c616261ad7c', 'mo-nginx-pv', 'b9ce6b59-b8a5-4bba-b241-144a6c391f5c', 'local', 1, 'mo-nginx-pv', 1, '2021-04-16 07:49:28', 'cc81efe719bf4b05a3c21524ccf6c1ea', 'f6d4b7a3-db29-476b-aaa2-392fad843442');
INSERT INTO `gce_pv` VALUES ('bd4887da-569d-4e5a-9fa0-672ff2d7226b', 'mysql-pv', 'b9ce6b59-b8a5-4bba-b241-144a6c391f2a', 'local', 1, 'mysql-pv', 1, '2021-08-06 02:38:06', 'cc81efe719bf4b05a3c21524ccf6c1ea', 'f6d4b7a3-db29-476b-aaa2-392fad843442');
INSERT INTO `gce_pv` VALUES ('be3780ea-fcb7-4eed-b446-9dfb33357406', 'pvc-52346daa-a649-4b5f-b544-9a9cf7eea41e', 'b9ce6b59-b8a5-4bba-b241-144a6c391f5c', 'nfs', 1, 'pvc-52346daa-a649-4b5f-b544-9a9cf7eea41e', 1, '2021-07-16 08:09:30', 'cc81efe719bf4b05a3c21524ccf6c1ea', 'f6d4b7a3-db29-476b-aaa2-392fad843442');
INSERT INTO `gce_pv` VALUES ('beba17db-eefb-45c0-84d9-acf19803c1b1', 'mysql-pv', 'b9ce6b59-b8a5-4bba-b241-144a6c391f2c', 'local', 1, 'mysql-pv', 1, '2021-06-23 00:43:13', 'cc81efe719bf4b05a3c21524ccf6c1ea', 'f6d4b7a3-db29-476b-aaa2-392fad843442');
INSERT INTO `gce_pv` VALUES ('c620351f-cc31-446c-b348-9ec5ce8affb9', 'redis-config-pv', 'b9ce6b59-b8a5-4bba-b241-144a6c391f2c', 'local', 1, 'redis-config-pv', 1, '2021-06-24 08:18:15', 'cc81efe719bf4b05a3c21524ccf6c1ea', 'f6d4b7a3-db29-476b-aaa2-392fad843442');
INSERT INTO `gce_pv` VALUES ('c68aa75b-fcbd-402a-b79b-5b21ca8374e7', 'liang-nginx-pv', 'b9ce6b59-b8a5-4bba-b241-144a6c391f5c', 'local', 1, 'liang-nginx-pv', 1, '2021-04-16 01:42:05', 'cc81efe719bf4b05a3c21524ccf6c1ea', 'f6d4b7a3-db29-476b-aaa2-392fad843442');
INSERT INTO `gce_pv` VALUES ('cf6829ee-fc07-4db4-b008-7d1de58d93ce', 'mysql-pv', 'b9ce6b59-b8a5-4bba-b241-144a6c391f5c', 'hostPath', 1, 'mysql-pv', 1, '2021-06-22 07:34:15', 'cc81efe719bf4b05a3c21524ccf6c1ea', 'f6d4b7a3-db29-476b-aaa2-392fad843442');
INSERT INTO `gce_pv` VALUES ('cfcbe3d6-bbce-4fb6-a6dd-9b56f23b4ded', 'pvc-bed7ce95-9c29-4d6e-9fd6-92bbe65a13f9', 'b9ce6b59-b8a5-4bba-b241-144a6c391f5c', 'nfs', 1, 'pvc-bed7ce95-9c29-4d6e-9fd6-92bbe65a13f9', 1, '2021-07-16 02:35:50', 'cc81efe719bf4b05a3c21524ccf6c1ea', 'f6d4b7a3-db29-476b-aaa2-392fad843442');
INSERT INTO `gce_pv` VALUES ('d18dfc98-9e62-44cc-837e-0b79f3528cfe', 'liang-util-pv', 'b9ce6b59-b8a5-4bba-b241-144a6c391f2c', 'local', 1, 'liang-util-pv', 1, '2021-05-28 01:36:02', 'cc81efe719bf4b05a3c21524ccf6c1ea', 'f6d4b7a3-db29-476b-aaa2-392fad843442');
INSERT INTO `gce_pv` VALUES ('d707e80c-42e9-4aa3-b690-23273113fdb9', 'mysql', '6649023a-17cd-4a8e-bee2-559557396354', 'local', 1, 'mysql', 1, '2021-03-29 05:55:09', 'cc81efe719bf4b05a3c21524ccf6c1ea', 'f6d4b7a3-db29-476b-aaa2-392fad843442');
INSERT INTO `gce_pv` VALUES ('dfc38ec3-3855-4d34-9d54-161f4836c46e', 'example-pv', 'ce57c962-c9b4-46cf-ae32-5ebf0fc29e98', 'local', 1, 'example-pv', 1, '2021-03-26 08:27:50', 'cc81efe719bf4b05a3c21524ccf6c1ea', 'f6d4b7a3-db29-476b-aaa2-392fad843442');
INSERT INTO `gce_pv` VALUES ('eecb8ea6-ca89-4b9f-be29-b94524479c99', 'pv-mysql-liang', 'e0c98c82-09f0-4e37-825e-b5e57dffcaef', 'local', 1, 'pv-mysql-liang', 1, '2021-08-10 11:27:45', 'cc81efe719bf4b05a3c21524ccf6c1ea', 'f6d4b7a3-db29-476b-aaa2-392fad843442');
INSERT INTO `gce_pv` VALUES ('fb3e3f0a-ff08-452f-84f0-9bde350dac51', 'mo-nginx-pv', 'ce57c962-c9b4-46cf-ae32-5ebf0fc29e95', 'local', 1, 'mo-nginx-pv', 1, '2021-03-31 15:08:10', 'cc81efe719bf4b05a3c21524ccf6c1ea', 'f6d4b7a3-db29-476b-aaa2-392fad843442');

-- ----------------------------
-- Function structure for getPriority
-- ----------------------------
DROP FUNCTION IF EXISTS `getPriority`;
delimiter ;;
CREATE FUNCTION `getPriority`(inID INT, flowId INT)
 RETURNS varchar(255) CHARSET utf8
  DETERMINISTIC
begin
  DECLARE gParentID VARCHAR(255) DEFAULT '';
  DECLARE gPriority VARCHAR(255) DEFAULT '';
  DECLARE commaLocate tinyint;
  DECLARE firstId VARCHAR(255) DEFAULT '';
  DECLARE secondId VARCHAR(255) DEFAULT '';
  DECLARE firstPriority VARCHAR(255) DEFAULT '';
  DECLARE secondPriority VARCHAR(255) DEFAULT '';
  DECLARE firstPriorityLevel tinyint;
  DECLARE secondPriorityLevel tinyint;
  DECLARE gNewParentID VARCHAR(255) DEFAULT '';
  SET commaLocate = 0;
  SET gPriority = inID;
  SELECT from_ids INTO gParentID FROM gc_work_flow_instance_step WHERE template_step_id = inID and flow_id=flowId;
  WHILE gParentID is not null DO
    SET commaLocate = locate(',', gParentID);
    IF commaLocate >0 THEN
      SET firstId = SUBSTRING(gParentID,1,locate(',', gParentID)-1);
      SET secondId = SUBSTRING(gParentID,locate(',', gParentID)+1);
	    SET firstPriority = getPrioritySub(firstId, flowId);
      SET secondPriority = getPrioritySub(secondId, flowId);
      SET firstPriorityLevel = LENGTH(firstPriority)- LENGTH(REPLACE(firstPriority,'.',''));
      SET secondPriorityLevel = LENGTH(secondPriority)- LENGTH(REPLACE(secondPriority,'.',''));
      IF firstPriorityLevel = secondPriorityLevel THEN
        SET gPriority = CONCAT(gParentID, '.', gPriority);
      ELSE
        IF firstPriorityLevel > secondPriorityLevel THEN
          SET gNewParentID = CONCAT(secondId, ".",firstId);
				ELSE
				  SET gNewParentID = CONCAT(firstId, ".", secondId);
				end IF;
        SET gPriority = CONCAT(gNewParentID, '.', gPriority);
      END IF;
      SELECT from_ids INTO gParentID FROM gc_work_flow_instance_step WHERE template_step_id = firstId and flow_id=flowId;
    ELSE
      SET gPriority = CONCAT(gParentID, '.', gPriority);
      SELECT from_ids INTO gParentID FROM gc_work_flow_instance_step WHERE template_step_id = gParentID and flow_id=flowId;
    END IF;
  END WHILE;
  RETURN gPriority;
end
;;
delimiter ;

-- ----------------------------
-- Function structure for getPrioritySub
-- ----------------------------
DROP FUNCTION IF EXISTS `getPrioritySub`;
delimiter ;;
CREATE FUNCTION `getPrioritySub`(inID INT, flowId INT)
 RETURNS varchar(255) CHARSET utf8
  DETERMINISTIC
begin
  DECLARE gParentID VARCHAR(255) DEFAULT '';
  DECLARE gPriority VARCHAR(255) DEFAULT '';
  SET gPriority = inID;
  SELECT from_ids INTO gParentID FROM gc_work_flow_instance_step WHERE template_step_id = inID and flow_id=flowId;
  WHILE gParentID is not null DO
    SET gPriority = CONCAT(gParentID, '.', gPriority);
    SELECT from_ids INTO gParentID FROM gc_work_flow_instance_step WHERE template_step_id = gParentID and flow_id=flowId;
  END WHILE;
  RETURN gPriority;
end
;;
delimiter ;

-- ----------------------------
-- Function structure for SPLIT_STRING
-- ----------------------------
DROP FUNCTION IF EXISTS `SPLIT_STRING`;
delimiter ;;
CREATE FUNCTION `SPLIT_STRING`(s VARCHAR(1024) , del CHAR(1) , i INT)
 RETURNS varchar(1024) CHARSET utf8
  DETERMINISTIC
BEGIN

        DECLARE n INT ;

        -- get max number of items
        SET n = LENGTH(s) - LENGTH(REPLACE(s, del, '')) + 1;

        IF i > n THEN
            RETURN NULL ;
        ELSE
            RETURN SUBSTRING_INDEX(SUBSTRING_INDEX(s, del, i) , del , -1 ) ;
        END IF;

    END
;;
delimiter ;

SET FOREIGN_KEY_CHECKS = 1;
