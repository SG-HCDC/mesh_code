-- 创建mysql库
DROP DATABASE IF EXISTS `mesh_controller`;
create database `mesh_controller` default character set utf8mb4 collate utf8mb4_general_ci;
/*
 Navicat Premium Data Transfer

 Source Server         : mysql-203-96（原20198）
 Source Server Type    : MySQL
 Source Server Version : 50732
 Source Host           : 192.168.203.96:13306
 Source Schema         : mesh_controller

 Target Server Type    : MySQL
 Target Server Version : 50732
 File Encoding         : 65001

 Date: 05/12/2020 20:46:09
*/

USE `mesh_controller`;

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for alert_history
-- ----------------------------
DROP TABLE IF EXISTS `alert_history`;
CREATE TABLE `alert_history`  (
  `id` varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '主键',
  `title` varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '告警标题',
  `meter` varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '监控项',
  `level` int(11) NULL DEFAULT NULL COMMENT '告警等级',
  `resource_name` varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '告警资源名称',
  `resource_type` varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '告警资源类型',
  `resource_id` varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '告警资源ID',
  `resource_instance` varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '告警资源实例',
  `create_time` datetime(0) NOT NULL COMMENT '创建时间',
  `alert_time` datetime(0) NOT NULL COMMENT '告警时间',
  `notify_count` int(11) NOT NULL COMMENT '告警次数',
  `status` varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '告警状态',
  `platform_type` varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '平台',
  `region_id` varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '区域',
  `content` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '告警内容',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;


-- ----------------------------
-- Table structure for alert_linkman
-- ----------------------------
DROP TABLE IF EXISTS `alert_linkman`;
CREATE TABLE `alert_linkman`  (
  `id` varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '主键',
  `user_name` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '联系人名称',
  `email` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '邮箱',
  `phone_number` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '手机号码',
  `create_time` datetime(0) NOT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

INSERT INTO `alert_linkman` (`id`, `user_name`, `email`, `create_time`) VALUES ('68f22b22-aafa-4625-a0fc-81176ff4be19', 'man', 'man123@qq.om', '2021-02-05 14:28:59');

-- ----------------------------
-- Table structure for alert_policy_linkman
-- ----------------------------
DROP TABLE IF EXISTS `alert_policy_linkman`;
CREATE TABLE `alert_policy_linkman`  (
  `id` varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '主键',
  `policy_id` varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '策略ID',
  `linkman_id` varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '联系人ID',
  `create_time` datetime(0) NOT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for alert_policys
-- ----------------------------
DROP TABLE IF EXISTS `alert_policys`;
CREATE TABLE `alert_policys`  (
  `id` varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '主键',
  `policy_name` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '策略名称',
  `enable` tinyint(1) NOT NULL COMMENT '是否生效',
  `enable_start_time` int(11) NULL DEFAULT NULL COMMENT '生效的开始时间',
  `enable_end_time` int(11) NULL DEFAULT NULL COMMENT ' 生效的结束时间',
  `channel_silence` int(11) NULL DEFAULT NULL COMMENT '通道沉默时间，单位为秒',
  `notify_enable` tinyint(1) NULL DEFAULT NULL COMMENT '是否发送通知联系人',
  `create_time` datetime(0) NOT NULL COMMENT '创建时间',
  `platform_type` varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '平台',
  `region_id` varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '区域',
  `update_time` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;


-- ----------------------------
-- Table structure for alert_rules
-- ----------------------------
DROP TABLE IF EXISTS `alert_rules`;
CREATE TABLE `alert_rules`  (
  `id` varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '主键',
  `policy_id` varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '策略主键',
  `meter` varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '监控项',
  `meter_chn_name` varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '监控项中文名',
  `unit` varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '监控项单位',
  `resource_type` varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '资源类型',
  `resource_id` varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '资源ID',
  `create_time` datetime(0) NOT NULL COMMENT '创建时间',
  `comparison_operator` int(2) NOT NULL COMMENT '对比方式',
  `threshold` decimal(36, 2) NOT NULL COMMENT '阈值',
  `level` int(2) NOT NULL COMMENT '告警等级',
  `enabled` tinyint(1) NOT NULL COMMENT '是否生效',
  `statistics` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '统计方法，默认是平均值avg、可以选择最大值max、最小值min',
  `duration` int(11) NOT NULL COMMENT '连续多少秒超过阈值',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for alert_templates
-- ----------------------------
DROP TABLE IF EXISTS `alert_templates`;
CREATE TABLE `alert_templates`  (
  `id` varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '主键',
  `name` varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '模板名称',
  `title` varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '告警标题',
  `content` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '告警内容',
  `recover_title` varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '恢复标题',
  `recover_content` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '恢复内容',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of alert_templates
-- ----------------------------
INSERT INTO `alert_templates` VALUES ('120', 'environment.distribution_box_power', '告警通知', '配电箱（${resourceName}）的有功功率为${value}KW ${comparisonOperator} 阈值${threshold}KW', '', '');
INSERT INTO `alert_templates` VALUES ('121', 'environment.distribution_box_energy', '告警通知', '配电箱（${resourceName}）的有功电能为${value}KW/h ${comparisonOperator} 阈值${threshold}KW/h', '', '');
INSERT INTO `alert_templates` VALUES ('122', 'environment.ups_in_voltage', '告警通知', 'UPS（${resourceName}）的输入电压为${value}V ${comparisonOperator} 阈值${threshold}V', '', '');
INSERT INTO `alert_templates` VALUES ('125', 'environment.air_setting_temperature', '告警通知', '空调（${resourceName}）的设定温度为${value}°C ${comparisonOperator} 阈值${threshold}°C', '', '');
INSERT INTO `alert_templates` VALUES ('126', 'environment.air_in_humidity', '告警通知', '空调（${resourceName}）的回风湿度为${value}%RH ${comparisonOperator} 阈值${threshold}%RH', '', '');
INSERT INTO `alert_templates` VALUES ('127', 'environment.air_out_humidity', '告警通知', '空调（${resourceName}）的出风湿度为${value}%RH ${comparisonOperator} 阈值${threshold}%RH', '', '');
INSERT INTO `alert_templates` VALUES ('128', 'environment.distribution_box_voltage', '告警通知', '配电箱（${resourceName}）的额定电压为${value}V ${comparisonOperator} 阈值${threshold}V', '', '');
INSERT INTO `alert_templates` VALUES ('129', 'environment.distribution_box_electron', '告警通知', '配电箱（${resourceName}）的额定电流为${value}A ${comparisonOperator} 阈值${threshold}A', '', '');
INSERT INTO `alert_templates` VALUES ('130', 'environment.ups_out_voltage', '告警通知', 'UPS（${resourceName}）的输出电压为${value}V ${comparisonOperator} 阈值${threshold}V', '', '');
INSERT INTO `alert_templates` VALUES ('131', 'environment.ups_temperature', '告警通知', 'UPS（${resourceName}）的温度为${value}°C ${comparisonOperator} 阈值${threshold}°C', '', '');
INSERT INTO `alert_templates` VALUES ('132', 'server.power', '告警通知', '服务器（${resourceName}）的实时功耗为${value}W ${comparisonOperator} 阈值${threshold}W', '', '');
INSERT INTO `alert_templates` VALUES ('133', 'server.cpu_cores', '告警通知', '服务器（${resourceName}）的CPU核数为${value} ${comparisonOperator} 阈值${threshold}', '', '');
INSERT INTO `alert_templates` VALUES ('134', 'server.cpu_L2cache', '告警通知', '服务器（${resourceName}）的CPU缓存为${value} ${comparisonOperator} 阈值${threshold}', '', '');
INSERT INTO `alert_templates` VALUES ('135', 'server.cpu_frequency', '告警通知', '服务器（${resourceName}）的CPU频率为${value}GHz ${comparisonOperator} 阈值${threshold}GHz', '', '');
INSERT INTO `alert_templates` VALUES ('136', 'server.mem_speed', '告警通知', '服务器（${resourceName}）的内存速率为${value}MHz ${comparisonOperator} 阈值${threshold}MHz', '', '');
INSERT INTO `alert_templates` VALUES ('137', 'server.storage_total', '告警通知', '服务器（${resourceName}）的存储总量为${value}GB ${comparisonOperator} 阈值${threshold}GB', '', '');
INSERT INTO `alert_templates` VALUES ('138', 'server.mem_total', '告警通知', '服务器（${resourceName}）的内存总量为${value}GB ${comparisonOperator} 阈值${threshold}GB', '', '');
INSERT INTO `alert_templates` VALUES ('139', 'server.storage_available', '告警通知', '服务器（${resourceName}）的存储可用量为${value}GB ${comparisonOperator} 阈值${threshold}GB', '', '');
INSERT INTO `alert_templates` VALUES ('140', 'server.bandWidth', '告警通知', '服务器（${resourceName}）的带宽为${value}MB/s ${comparisonOperator} 阈值${threshold}MB/s', '', '');
INSERT INTO `alert_templates` VALUES ('141', 'host.cpu_util', '告警通知', '服务器（${resourceName}）的CPU利用率为${value}% ${comparisonOperator} 阈值${threshold}%', '', '');
INSERT INTO `alert_templates` VALUES ('142', 'host.memory_util', '告警通知', '服务器（${resourceName}）的内存利用率为${value}% ${comparisonOperator} 阈值${threshold}%', '', '');
INSERT INTO `alert_templates` VALUES ('143', 'container.cpu_util', '告警通知', '应用（${resourceName}）的CPU利用率为${value}% ${comparisonOperator} 阈值${threshold}%', '', '');
INSERT INTO `alert_templates` VALUES ('144', 'container.net_rx_rate', '告警通知', '应用（${resourceName}）的网络下行速率为${value}KB/s ${comparisonOperator} 阈值${threshold}KB/s', '', '');
INSERT INTO `alert_templates` VALUES ('145', 'container.memory_usage', '告警通知', '应用（${resourceName}）的内存使用量为${value}MB ${comparisonOperator} 阈值${threshold}MB', '', '');
INSERT INTO `alert_templates` VALUES ('146', 'container.net_tx_rate', '告警通知', '应用${resourceName}）的网络上行速率为${value}KB/s ${comparisonOperator} 阈值${threshold}KB/s', '', '');
INSERT INTO `alert_templates` VALUES ('147', 'container.fs_writes_rate', '告警通知', '应用（${resourceName}）的磁盘写速率为${value}KB/s ${comparisonOperator} 阈值${threshold}KB/s', '', '');
INSERT INTO `alert_templates` VALUES ('148', 'container.fs_reads_rate', '告警通知', '应用（${resourceName}）的磁盘读速率为${value}KB/s ${comparisonOperator} 阈值${threshold}KB/s', '', '');
INSERT INTO `alert_templates` VALUES ('149', 'container.cores', '告警通知', '应用（${resourceName}）的CPU核数为${value} ${comparisonOperator} 阈值${threshold}', '', '');
INSERT INTO `alert_templates` VALUES ('150', 'container.storage_usage', '告警通知', '应用（${resourceName}）的存储使用量为${value}GB ${comparisonOperator} 阈值${threshold}GB', '', '');
INSERT INTO `alert_templates` VALUES ('151', 'container.bandWidth', '告警通知', '应用（${resourceName}）的带宽为${value}MB/s ${comparisonOperator} 阈值${threshold}MB/s', '', '');
INSERT INTO `alert_templates` VALUES ('152', 'switcher.in_octets', '告警通知', '交换机（${resourceName}）的收包速率为${value}KB/s ${comparisonOperator} 阈值${threshold}KB/s', '', '');
INSERT INTO `alert_templates` VALUES ('153', 'switcher.out_octets', '告警通知', '交换机（${resourceName}）的发包速率为${value}KB/s ${comparisonOperator} 阈值${threshold}KB/s', '', '');
INSERT INTO `alert_templates` VALUES ('154', 'vm.cpu_util', '告警通知', '虚拟机（${resourceName}）的CPU使用率为${value}% ${comparisonOperator} 阈值${threshold}%', '', '');
INSERT INTO `alert_templates` VALUES ('155', 'vm.memory_util', '告警通知', '虚拟机（${resourceName}）的内存使用率为${value}% ${comparisonOperator} 阈值${threshold}%', '', '');
INSERT INTO `alert_templates` VALUES ('156', 'environment.pue', '告警通知', '（${resourceName}）的PUE为${value} ${comparisonOperator} 阈值${threshold}', '', '');
INSERT INTO `alert_templates` VALUES ('157', 'environment.air_in_temperature', '告警通知', '空调（${resourceName}）的回风温度为${value}°C ${comparisonOperator} 阈值${threshold}°C', '', '');
INSERT INTO `alert_templates` VALUES ('158', 'environment.air_out_temperature', '告警通知', '空调（${resourceName}）的出风温度为${value}°C ${comparisonOperator} 阈值${threshold}°C', '', '');
INSERT INTO `alert_templates` VALUES ('160', 'host.cpu_load_1min', '告警通知', '服务器（${resourceName}）的CPU负载为${value} ${comparisonOperator} 阈值${threshold}', '', '');
INSERT INTO `alert_templates` VALUES ('161', 'host.disk_read_rate', '告警通知', '服务器（${resourceName}）的磁盘读速率为${value}KB/s ${comparisonOperator} 阈值${threshold}KB/s', '', '');
INSERT INTO `alert_templates` VALUES ('162', 'host.disk_write_rate', '告警通知', '服务器（${resourceName}）的磁盘写速率为${value}KB/s ${comparisonOperator} 阈值${threshold}KB/s', '', '');
INSERT INTO `alert_templates` VALUES ('163', 'host.temperature', '告警通知', '服务器（${resourceName}）的温度${value}°C ${comparisonOperator} 阈值${threshold}°C', '', '');
INSERT INTO `alert_templates` VALUES ('164', 'cost.temperature', '告警通知', '（${resourceName}）的迁移温度差为${value}°C ${comparisonOperator} 阈值${threshold}°C', '', '');
INSERT INTO `alert_templates` VALUES ('165', 'cost.power', '告警通知', '（${resourceName}）的迁移能耗差为${value}W ${comparisonOperator} 阈值${threshold}W', '', '');
INSERT INTO `alert_templates` VALUES ('166', 'cost.response_time', '告警通知', '（${resourceName}）的迁移响应时间差为${value}s ${comparisonOperator} 阈值${threshold}s', '', '');
INSERT INTO `alert_templates` VALUES ('167', 'cost.load', '告警通知', '（${resourceName}）的迁移负荷差为${value}W ${comparisonOperator} 阈值${threshold}W', '', '');
INSERT INTO `alert_templates` VALUES ('168', 'k8s.cpu_util', '告警通知', '容器（${resourceName}）的CPU利用率为${value}% ${comparisonOperator} 阈值${threshold}%', '', '');
INSERT INTO `alert_templates` VALUES ('169', 'k8s.memory_usage', '告警通知', '容器（${resourceName}）的内存使用量为${value}MB ${comparisonOperator} 阈值${threshold}MB', '', '');

-- ----------------------------
-- Table structure for analysis_demos
-- ----------------------------
DROP TABLE IF EXISTS `analysis_demos`;
CREATE TABLE `analysis_demos`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '主键',
  `timestamp` datetime(0) NOT NULL COMMENT '时间戳',
  `data_x` varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '自变量指标',
  `data_y` varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '因变量指标',
  `type` varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '类型，拟合：fitter; 仿真：simulate',
  `result` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '拟合结果，从0阶到n阶，以|分隔',
  `resource_id` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '资源id',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 30 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for analysis_fitter_devices
-- ----------------------------
DROP TABLE IF EXISTS `analysis_fitter_devices`;
CREATE TABLE `analysis_fitter_devices`  (
  `id` varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '主键',
  `node_id` varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '服务器id',
  `air_id` varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '空调id',
  `algorithm` varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '拟合算法：1、多项式拟合: polynomial',
  `metric_x` varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '自变量指标',
  `metric_y` varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '因变量指标',
  `status` varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '当前状态0: 无；1：计算中；2：已完成',
  `degree` varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '拟合阶数',
  `result` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '拟合结果，从0阶到n阶，以|分隔',
  `start_time` datetime(0) NULL DEFAULT NULL COMMENT '数据查询开始时间',
  `end_time` datetime(0) NULL DEFAULT NULL COMMENT '数据查询结束时间',
  `sample` int(11) NULL DEFAULT NULL COMMENT '取样点数',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for analysis_simulate_datacenters
-- ----------------------------
DROP TABLE IF EXISTS `analysis_simulate_datacenters`;
CREATE TABLE `analysis_simulate_datacenters`  (
  `id` varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '主键',
  `datacenter_id` varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '数据中心id',
  `device_id` varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '设备id',
  `metric_x` varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '自变量指标',
  `status` varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '当前状态0: 无；1：计算中；2：已完成',
  `degree` varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '拟合阶数',
  `result` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '拟合结果，从0阶到n阶，以|分隔',
  `start_time` datetime(0) NULL DEFAULT NULL COMMENT '数据查询开始时间',
  `end_time` datetime(0) NULL DEFAULT NULL COMMENT '数据查询结束时间',
  `history_metric` double NULL DEFAULT NULL COMMENT '历史最优因素值',
  `history_value` double NULL DEFAULT NULL COMMENT '历史最优PUE值',
  `simulate_metric` double NULL DEFAULT NULL COMMENT '仿真最优因素值',
  `simulate_value` double NULL DEFAULT NULL COMMENT '仿真最优PUE值',
  `enabled` tinyint(4) NULL DEFAULT NULL COMMENT '1：启用，0：禁用',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `name` varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '服务器名称',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;


-- ----------------------------
-- Table structure for asset_cloud_resources
-- ----------------------------
DROP TABLE IF EXISTS `asset_cloud_resources`;
CREATE TABLE `asset_cloud_resources`  (
  `id` varchar(60) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '服务器id',
  `name` varchar(60) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '资源名称',
  `type` tinyint(4) NULL DEFAULT NULL COMMENT '资源类型',
  `datacenter_id` varchar(60) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '所属区域ID',
  `expand` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '所属区域类型',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `from` varchar(60) COMMENT '来源',
  `cpu_cores` int(11) NULL COMMENT '虚拟机CPU核数',
  `memory` int(11) NULL COMMENT '虚拟机内存大小', 
  `object_id` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '目标ID',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '服务器信息表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for dcs_statistics_data
-- ----------------------------
DROP TABLE IF EXISTS `dcs_statistics_data`;
CREATE TABLE `dcs_statistics_data` (
  `id` varchar(36) NOT NULL,
  `meter` varchar(255) NOT NULL,
  `resource_id` varchar(36) NOT NULL,
  `timestamp` datetime NOT NULL,
  `value` double(11,2) NOT NULL,
   PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Table structure for asset_datacenters
-- ----------------------------
DROP TABLE IF EXISTS `asset_datacenters`;
CREATE TABLE `asset_datacenters`  (
  `id` varchar(60) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '数据中心id',
  `name` varchar(60) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '名称',
  `ip` varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '数据中心管理端ip',
  `creator` varchar(60) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '创建人',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `power` int(11) NULL DEFAULT NULL,
  `power_conservation` tinyint(1) NULL DEFAULT NULL,
  `threshold` int(11) NULL DEFAULT NULL,
  `is_scheduler` tinyint(4) NULL DEFAULT NULL COMMENT '是否调度',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '数据中心表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of asset_datacenters
-- ----------------------------
INSERT INTO `asset_datacenters` VALUES ('36fa22ae-8858-4817-b1b1-c667a5012ddc', '数据中心200', '192.168.203.38', NULL, '2020-09-04 14:25:29', 534, 0, 45, 1);
INSERT INTO `asset_datacenters` VALUES ('36fa22ae-8858-4817-b1b1-ceqweqweqwe', '数据中心210', '192.168.203.39', NULL, '2020-09-04 14:25:29', 761, 1, 86, 0);
INSERT INTO `asset_datacenters` VALUES ('c2a90948-1a62-4da2-bcdd-e935f80e4094', 'node212', '192.168.203.200', NULL, '2020-10-29 10:37:40', 1124, 0, NULL, 1);

-- ----------------------------
-- Table structure for asset_iaas
-- ----------------------------
DROP TABLE IF EXISTS `asset_iaas`;
CREATE TABLE `asset_iaas`  (
  `id` varchar(60) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '资源id',
  `name` varchar(60) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '资源名称',
  `esn` varchar(60) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '设备识别号',
  `type` int(8) NOT NULL COMMENT '资源类型，1：空调；2：ups；3：pdu；4、温感；5、机柜；6、服务器；7、交换机；8、配电箱',
  `device_id` varchar(60) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '设备id',
  `device_model` varchar(60) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '设备型号',
  `device_manufacturer` varchar(60) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '设备厂商',
  `datacenter_id` varchar(60) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '数据中心id',
  `creator` varchar(60) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '创建人',
  `create_time` datetime(0) NOT NULL COMMENT '创建时间',
  `from` varchar(60) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '来源类型，manual：手动；supervision_system：动环；k8s：容器云',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '基础设施层资源表' ROW_FORMAT = Dynamic;

-- order by create_time 排序索引
create index index_sort_create_time on asset_iaas(`create_time`, `type`, `name`, `esn`, `device_id`, `device_model`, `device_manufacturer`, `datacenter_id`, `from`);

-- ----------------------------
-- Table structure for asset_nodes
-- ----------------------------
DROP TABLE IF EXISTS `asset_nodes`;
CREATE TABLE `asset_nodes`  (
  `id` varchar(60) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '服务器id',
  `mgm_ip` varchar(60) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '管理网ip',
  `ipmi_ip` varchar(60) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '带外管理ip',
  `ipmi_user` varchar(60) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '带外管理用户',
  `ipmi_password` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '带外管理密码',
  `network_status` int(2) NULL DEFAULT NULL COMMENT '网络状态0：离线；1：在线；',
  `power_status` int(2) NULL DEFAULT NULL COMMENT '电源状态0：已关闭；1：运行中；',
  `hostname` varchar(60) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '主机名',
  `rated_power` float(255, 0) NULL DEFAULT NULL COMMENT '额定功率',
  `power_consumption` float(255, 0) NULL DEFAULT NULL COMMENT '功耗',
  `energy_efficiency_ratio` float(255, 2) NULL DEFAULT NULL,
  `storage_capacity` float(255, 2) NULL DEFAULT NULL,
  `isolation` tinyint(1) NULL DEFAULT NULL,
  `rated_power_sm` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '额定功率（加密）',
  `rated_voltage_sm` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '额定电压（加密）',
  `sm_data` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '重要数据',
  `cabinet_id` varchar(60) CHARACTER SET utf8 COLLATE utf8_general_ci COMMENT '机柜id',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '服务器信息表' ROW_FORMAT = Dynamic;


-- ----------------------------
-- Table structure for asset_switchers
-- ----------------------------
DROP TABLE IF EXISTS `asset_switchers`;
CREATE TABLE `asset_switchers`  (
  `id` varchar(60) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '设备id',
  `name` varchar(60) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '名称',
  `ip` varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '设备ip',
  `community` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '交换机community',
  `status` int(2) NOT NULL COMMENT '0: 离线; 1:在线',
  `cabinet_id` varchar(60) CHARACTER SET utf8 COLLATE utf8_general_ci COMMENT '机柜id',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '交换机信息表' ROW_FORMAT = Dynamic;


-- ----------------------------
-- Table structure for ces_field_kongj
-- ----------------------------
DROP TABLE IF EXISTS `ces_field_kongj`;
CREATE TABLE `ces_field_kongj`  (
  `id` varchar(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '主键',
  `create_by` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '创建人',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建日期',
  `update_by` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '更新人',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新日期',
  `sys_org_code` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '所属部门',
  `name` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '输入框',
  `sex` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '下拉框',
  `radio` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT 'radio',
  `checkbox` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT 'checkbox',
  `sel_mut` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '下拉多选',
  `sel_search` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '下拉搜索',
  `birthday` datetime(0) NULL DEFAULT NULL COMMENT '时间',
  `pic` varchar(1000) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '图片',
  `files` varchar(1000) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '文件',
  `remakr` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT 'markdown',
  `fuwenb` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT '富文本',
  `user_sel` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '用户',
  `dep_sel` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '部门',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of ces_field_kongj
-- ----------------------------
INSERT INTO `ces_field_kongj` VALUES ('1260184757548961793', 'admin', '2020-05-12 20:27:13', 'admin', '2020-07-11 11:26:07', 'A01', '11', '1', '1', '1,2', '1,2', 'zhagnxiao', '2020-05-14 00:00:00', '20180607175028Fn1Lq7zw_1589286404350.png', '12323_1589286409334.pdf', '111', '<p>111</p>', 'zhagnxiao', '5159cde220114246b045e574adceafe9');
INSERT INTO `ces_field_kongj` VALUES ('1260185893546840066', 'admin', '2020-05-12 20:31:44', 'admin', '2020-07-10 16:50:52', 'A01', 'A011', NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', NULL, 'zhagnxiao', 'A01');
INSERT INTO `ces_field_kongj` VALUES ('1260185911146139650', 'admin', '2020-05-12 20:31:49', 'admin', '2020-07-11 11:29:04', 'A01', 'A01', NULL, '1', '1,2', NULL, 'admin', NULL, 'd52a2834349b033b6d20d5cc16ce36d3d539bd7f_1594438138364.jpg', '', '', NULL, 'admin', 'A01');

-- ----------------------------
-- Table structure for ces_order_customer
-- ----------------------------
DROP TABLE IF EXISTS `ces_order_customer`;
CREATE TABLE `ces_order_customer`  (
  `id` varchar(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `create_by` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '创建人',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建日期',
  `update_by` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '更新人',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新日期',
  `sys_org_code` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '所属部门',
  `name` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '客户名字',
  `sex` varchar(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '客户性别',
  `birthday` datetime(0) NULL DEFAULT NULL COMMENT '客户生日',
  `age` int(11) NULL DEFAULT NULL COMMENT '年龄',
  `address` varchar(300) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '常用地址',
  `order_main_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '订单ID',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of ces_order_customer
-- ----------------------------
INSERT INTO `ces_order_customer` VALUES ('1281792646935842818', 'admin', '2020-07-11 11:29:16', NULL, NULL, 'A01', NULL, NULL, NULL, NULL, NULL, '1260135645520654338');
INSERT INTO `ces_order_customer` VALUES ('1281794386984779777', 'admin', '2020-07-11 11:36:11', NULL, NULL, 'A01', '管理员', '1', '2018-12-05 00:00:00', NULL, NULL, '1281794386716344321');
INSERT INTO `ces_order_customer` VALUES ('15889531549290863128', 'admin', '2020-05-08 23:54:00', NULL, NULL, 'A01', '小王', '1', '2020-05-08 00:00:00', 1, '', '1258787241599250433');
INSERT INTO `ces_order_customer` VALUES ('15889531551231672264', 'admin', '2020-05-08 23:54:00', NULL, NULL, 'A01', '校长', '2', '2020-05-23 00:00:00', 2, '', '1258787241599250433');
INSERT INTO `ces_order_customer` VALUES ('15889531554322486243', 'admin', '2020-05-08 23:54:00', NULL, NULL, 'A01', '小明', '2', '2020-05-03 00:00:00', 3, '', '1258787241599250433');
INSERT INTO `ces_order_customer` VALUES ('15892741044590530487', 'admin', '2020-05-12 17:02:32', NULL, NULL, 'A01A05', '111', '1', '2020-05-12 00:00:00', NULL, '', '1260133243631562754');
INSERT INTO `ces_order_customer` VALUES ('15892741436981375151', 'admin', '2020-05-12 17:02:32', NULL, NULL, 'A01A05', '11', '1', '2020-05-14 00:00:00', NULL, '', '1260133243631562754');

-- ----------------------------
-- Table structure for ces_order_goods
-- ----------------------------
DROP TABLE IF EXISTS `ces_order_goods`;
CREATE TABLE `ces_order_goods`  (
  `id` varchar(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `create_by` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '创建人',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建日期',
  `update_by` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '更新人',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新日期',
  `sys_org_code` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '所属部门',
  `good_name` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '商品名字',
  `price` double NULL DEFAULT NULL COMMENT '价格',
  `num` int(11) NULL DEFAULT NULL COMMENT '数量',
  `zong_price` double NULL DEFAULT NULL COMMENT '单品总价',
  `order_main_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '订单ID',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of ces_order_goods
-- ----------------------------
INSERT INTO `ces_order_goods` VALUES ('15889531536650244013', 'admin', '2020-05-08 23:54:00', NULL, NULL, 'A01', '显示器', 11, 1, 11, '1258787241599250433');
INSERT INTO `ces_order_goods` VALUES ('15889531538491119218', 'admin', '2020-05-08 23:54:00', NULL, NULL, 'A01', '鼠标', 22, 2, 44, '1258787241599250433');
INSERT INTO `ces_order_goods` VALUES ('15889532051192650399', 'admin', '2020-05-08 23:54:00', NULL, NULL, 'A01', '键盘', 33, 3, 99, '1258787241599250433');
INSERT INTO `ces_order_goods` VALUES ('15892741033410312304', 'admin', '2020-05-12 17:02:32', NULL, NULL, 'A01A05', '雷蛇键盘', 200, 2, NULL, '1260133243631562754');
INSERT INTO `ces_order_goods` VALUES ('15892741162151118866', 'admin', '2020-05-12 17:02:32', NULL, NULL, 'A01A05', '樱桃键盘', 200, 4, NULL, '1260133243631562754');
INSERT INTO `ces_order_goods` VALUES ('15892747180850346324', 'admin', '2020-07-11 11:29:16', NULL, NULL, 'A01', '11', 11, NULL, NULL, '1260135645520654338');
INSERT INTO `ces_order_goods` VALUES ('15892934593221167669', 'admin', '2020-07-11 11:29:16', NULL, NULL, 'A01', '', NULL, NULL, NULL, '1260135645520654338');
INSERT INTO `ces_order_goods` VALUES ('15892934594762635052', 'admin', '2020-07-11 11:29:16', NULL, NULL, 'A01', '', NULL, NULL, NULL, '1260135645520654338');
INSERT INTO `ces_order_goods` VALUES ('15944385645060861881', 'admin', '2020-07-11 11:36:11', NULL, NULL, 'A01', '11', NULL, NULL, NULL, '1281794386716344321');
INSERT INTO `ces_order_goods` VALUES ('15944385663981814101', 'admin', '2020-07-11 11:36:11', NULL, NULL, 'A01', '22', NULL, NULL, NULL, '1281794386716344321');

-- ----------------------------
-- Table structure for ces_order_main
-- ----------------------------
DROP TABLE IF EXISTS `ces_order_main`;
CREATE TABLE `ces_order_main`  (
  `id` varchar(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `create_by` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '创建人',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建日期',
  `update_by` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '更新人',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新日期',
  `sys_org_code` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '所属部门',
  `order_code` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '订单编码',
  `xd_date` datetime(0) NULL DEFAULT NULL COMMENT '下单时间',
  `money` double NULL DEFAULT NULL COMMENT '订单总额',
  `remark` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of ces_order_main
-- ----------------------------
INSERT INTO `ces_order_main` VALUES ('1258787241599250433', 'admin', '2020-05-08 23:54:00', NULL, NULL, 'A01', '20200508-0001', '2020-05-08 23:52:32', 50000, NULL);
INSERT INTO `ces_order_main` VALUES ('1260133243631562754', 'admin', '2020-05-12 17:02:32', NULL, NULL, 'A01A05', '202005120001', '2020-05-12 17:01:39', NULL, NULL);
INSERT INTO `ces_order_main` VALUES ('1260135645520654338', 'admin', '2020-05-12 17:12:04', 'admin', '2020-07-11 11:29:16', 'A01', 'JDFX2020051217115656', '2020-07-11 11:29:12', NULL, NULL);
INSERT INTO `ces_order_main` VALUES ('1281794386716344321', 'admin', '2020-07-11 11:36:11', NULL, NULL, 'A01', 'CN2020071111360067', '2020-07-11 11:36:00', NULL, NULL);

-- ----------------------------
-- Table structure for ces_shop_goods
-- ----------------------------
DROP TABLE IF EXISTS `ces_shop_goods`;
CREATE TABLE `ces_shop_goods`  (
  `id` varchar(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '主键',
  `create_by` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '创建人',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建日期',
  `update_by` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '更新人',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新日期',
  `sys_org_code` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '所属部门',
  `name` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '商品名字',
  `price` decimal(10, 5) NULL DEFAULT NULL COMMENT '价格',
  `chuc_date` datetime(0) NULL DEFAULT NULL COMMENT '出厂时间',
  `contents` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT '商品简介',
  `good_type_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '商品分类',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of ces_shop_goods
-- ----------------------------
INSERT INTO `ces_shop_goods` VALUES ('1258408897326149634', 'admin', '2020-05-07 22:50:35', 'admin', '2020-05-07 22:52:17', 'A01', '华为手机note', 5000.00000, '2020-05-07 00:00:00', '<div class=\"page1-content\" style=\"margin: 0px 0px 20px; padding: 0px; display: flex; justify-content: space-around; color: #2c3e50; font-family: Avenir, Helvetica, Arial, sans-serif; text-align: center; background-color: #eeeeee;\">\n<div class=\"page1-item\" style=\"margin: auto; padding: 0px; width: 180px;\">\n<div class=\"page1-item-img\" style=\"margin: 0px; padding: 0px; width: 180px; height: 180px; display: flex; -webkit-box-align: center; align-items: center; -webkit-box-pack: center; justify-content: center; border-radius: 100%; background: #ffffff; position: relative; z-index: 1; transition: box-shadow 0.3s cubic-bezier(0.215, 0.61, 0.355, 1) 0s, transform 0.3s cubic-bezier(0.215, 0.61, 0.355, 1) 0s, -webkit-box-shadow 0.3s cubic-bezier(0.215, 0.61, 0.355, 1) 0s, -webkit-transform 0.3s cubic-bezier(0.215, 0.61, 0.355, 1) 0s; box-shadow: rgba(166, 55, 112, 0.1) 0px 16px 32px;\"><img style=\"margin: 0px; padding: 0px; width: 100px;\" src=\"http://www.jeecg.com/images/soo.png\" alt=\"img\" /></div>\n<div class=\"page1-item-title\" style=\"margin: 56px 0px 0px; padding: 0px; font-size: 20px; color: #0d1a26;\"><span style=\"margin: 0px; padding: 0px;\">快速开发</span></div>\n</div>\n<div class=\"page1-item\" style=\"margin: auto; padding: 0px; width: 180px;\">\n<div class=\"page1-item-img\" style=\"margin: 0px; padding: 0px; width: 180px; height: 180px; display: flex; -webkit-box-align: center; align-items: center; -webkit-box-pack: center; justify-content: center; border-radius: 100%; background: #ffffff; position: relative; z-index: 1; transition: box-shadow 0.3s cubic-bezier(0.215, 0.61, 0.355, 1) 0s, transform 0.3s cubic-bezier(0.215, 0.61, 0.355, 1) 0s, -webkit-box-shadow 0.3s cubic-bezier(0.215, 0.61, 0.355, 1) 0s, -webkit-transform 0.3s cubic-bezier(0.215, 0.61, 0.355, 1) 0s; box-shadow: rgba(191, 188, 21, 0.1) 0px 16px 32px;\"><img style=\"margin: 0px; padding: 0px; width: 100px;\" src=\"http://www.jeecg.com/images/rule.png\" alt=\"img\" /></div>\n<div class=\"page1-item-title\" style=\"margin: 56px 0px 0px; padding: 0px; font-size: 20px; color: #0d1a26;\"><span style=\"margin: 0px; padding: 0px;\">数据权限</span></div>\n</div>\n<div class=\"page1-item\" style=\"margin: auto; padding: 0px; width: 180px;\">\n<div class=\"page1-item-img\" style=\"margin: 0px; padding: 0px; width: 180px; height: 180px; display: flex; -webkit-box-align: center; align-items: center; -webkit-box-pack: center; justify-content: center; border-radius: 100%; background: #ffffff; position: relative; z-index: 1; transition: box-shadow 0.3s cubic-bezier(0.215, 0.61, 0.355, 1) 0s, transform 0.3s cubic-bezier(0.215, 0.61, 0.355, 1) 0s, -webkit-box-shadow 0.3s cubic-bezier(0.215, 0.61, 0.355, 1) 0s, -webkit-transform 0.3s cubic-bezier(0.215, 0.61, 0.355, 1) 0s; box-shadow: rgba(73, 101, 166, 0.1) 0px 16px 32px;\"><img style=\"margin: 0px; padding: 0px; width: 100px;\" src=\"http://www.jeecg.com/images/bpm2.png\" alt=\"img\" /></div>\n<div class=\"page1-item-title\" style=\"margin: 56px 0px 0px; padding: 0px; font-size: 20px; color: #0d1a26;\"><span style=\"margin: 0px; padding: 0px;\">工作流</span></div>\n</div>\n<div class=\"page1-item\" style=\"margin: auto; padding: 0px; width: 180px;\">\n<div class=\"page1-item-img\" style=\"margin: 0px; padding: 0px; width: 180px; height: 180px; display: flex; -webkit-box-align: center; align-items: center; -webkit-box-pack: center; justify-content: center; border-radius: 100%; background: #ffffff; position: relative; z-index: 1; transition: box-shadow 0.3s cubic-bezier(0.215, 0.61, 0.355, 1) 0s, transform 0.3s cubic-bezier(0.215, 0.61, 0.355, 1) 0s, -webkit-box-shadow 0.3s cubic-bezier(0.215, 0.61, 0.355, 1) 0s, -webkit-transform 0.3s cubic-bezier(0.215, 0.61, 0.355, 1) 0s; box-shadow: rgba(135, 85, 255, 0.1) 0px 16px 32px;\"><img style=\"margin: 0px; padding: 0px; width: 100px;\" src=\"http://www.jeecg.com/images/report1.png\" alt=\"img\" /></div>\n<div class=\"page1-item-title\" style=\"margin: 56px 0px 0px; padding: 0px; font-size: 20px; color: #0d1a26;\"><span style=\"margin: 0px; padding: 0px;\">丰富组件库</span></div>\n</div>\n</div>\n<div class=\"page1-content\" style=\"margin: 0px; padding: 0px; display: flex; justify-content: space-around; color: #2c3e50; font-family: Avenir, Helvetica, Arial, sans-serif; text-align: center; background-color: #eeeeee;\">\n<div class=\"page1-item\" style=\"margin: auto; padding: 0px; width: 180px;\">\n<div class=\"page1-item-img\" style=\"margin: 0px; padding: 0px; width: 180px; height: 180px; display: flex; -webkit-box-align: center; align-items: center; -webkit-box-pack: center; justify-content: center; border-radius: 100%; background: #ffffff; position: relative; z-index: 1; transition: box-shadow 0.3s cubic-bezier(0.215, 0.61, 0.355, 1) 0s, transform 0.3s cubic-bezier(0.215, 0.61, 0.355, 1) 0s, -webkit-box-shadow 0.3s cubic-bezier(0.215, 0.61, 0.355, 1) 0s, -webkit-transform 0.3s cubic-bezier(0.215, 0.61, 0.355, 1) 0s; box-shadow: rgba(166, 55, 112, 0.1) 0px 16px 32px;\"><img style=\"margin: 0px; padding: 0px; width: 100px;\" src=\"http://www.jeecg.com/images/icon1.png\" alt=\"img\" /></div>\n<div class=\"page1-item-title\" style=\"margin: 56px 0px 0px; padding: 0px; font-size: 20px; color: #0d1a26;\"><span style=\"margin: 0px; padding: 0px;\">代码生成器</span></div>\n</div>\n<div class=\"page1-item\" style=\"margin: auto; padding: 0px; width: 180px;\">\n<div class=\"page1-item-img\" style=\"margin: 0px; padding: 0px; width: 180px; height: 180px; display: flex; -webkit-box-align: center; align-items: center; -webkit-box-pack: center; justify-content: center; border-radius: 100%; background: #ffffff; position: relative; z-index: 1; transition: box-shadow 0.3s cubic-bezier(0.215, 0.61, 0.355, 1) 0s, transform 0.3s cubic-bezier(0.215, 0.61, 0.355, 1) 0s, -webkit-box-shadow 0.3s cubic-bezier(0.215, 0.61, 0.355, 1) 0s, -webkit-transform 0.3s cubic-bezier(0.215, 0.61, 0.355, 1) 0s; box-shadow: rgba(191, 188, 21, 0.1) 0px 16px 32px;\"><img style=\"margin: 0px; padding: 0px; width: 100px;\" src=\"http://www.jeecg.com/images/Automation.png\" alt=\"img\" /></div>\n<div class=\"page1-item-title\" style=\"margin: 56px 0px 0px; padding: 0px; font-size: 20px; color: #0d1a26;\"><span style=\"margin: 0px; padding: 0px;\">Online在线开发</span></div>\n</div>\n<div class=\"page1-item\" style=\"margin: auto; padding: 0px; width: 180px;\">\n<div class=\"page1-item-img\" style=\"margin: 0px; padding: 0px; width: 180px; height: 180px; display: flex; -webkit-box-align: center; align-items: center; -webkit-box-pack: center; justify-content: center; border-radius: 100%; background: #ffffff; position: relative; z-index: 1; transition: box-shadow 0.3s cubic-bezier(0.215, 0.61, 0.355, 1) 0s, transform 0.3s cubic-bezier(0.215, 0.61, 0.355, 1) 0s, -webkit-box-shadow 0.3s cubic-bezier(0.215, 0.61, 0.355, 1) 0s, -webkit-transform 0.3s cubic-bezier(0.215, 0.61, 0.355, 1) 0s; box-shadow: rgba(73, 101, 166, 0.1) 0px 16px 32px;\"><img style=\"margin: 0px; padding: 0px; width: 100px;\" src=\"http://www.jeecg.com/images/form2.png\" alt=\"img\" /></div>\n<div class=\"page1-item-title\" style=\"margin: 56px 0px 0px; padding: 0px; font-size: 20px; color: #0d1a26;\"><span style=\"margin: 0px; padding: 0px;\">表单设计器</span></div>\n</div>\n<div class=\"page1-item\" style=\"margin: auto; padding: 0px; width: 180px;\">\n<div class=\"page1-item-img\" style=\"margin: 0px; padding: 0px; width: 180px; height: 180px; display: flex; -webkit-box-align: center; align-items: center; -webkit-box-pack: center; justify-content: center; border-radius: 100%; background: #ffffff; position: relative; z-index: 1; transition: box-shadow 0.3s cubic-bezier(0.215, 0.61, 0.355, 1) 0s, transform 0.3s cubic-bezier(0.215, 0.61, 0.355, 1) 0s, -webkit-box-shadow 0.3s cubic-bezier(0.215, 0.61, 0.355, 1) 0s, -webkit-transform 0.3s cubic-bezier(0.215, 0.61, 0.355, 1) 0s; box-shadow: rgba(135, 85, 255, 0.1) 0px 16px 32px;\"><img style=\"margin: 0px; padding: 0px; width: 100px;\" src=\"http://www.jeecg.com/images/bpm3.png\" alt=\"img\" /></div>\n<div class=\"page1-item-title\" style=\"margin: 56px 0px 0px; padding: 0px; font-size: 20px; color: #0d1a26;\"><span style=\"margin: 0px; padding: 0px;\">流程设计器</span></div>\n</div>\n</div>', '1258408044439597058');
INSERT INTO `ces_shop_goods` VALUES ('1258783909887422466', 'admin', '2020-05-08 23:40:45', 'admin', '2020-05-08 23:43:03', 'A01', '雷蛇鼠标', 500.00000, '2020-05-08 00:00:00', '', '1258408003595464706');

-- ----------------------------
-- Table structure for ces_shop_type
-- ----------------------------
DROP TABLE IF EXISTS `ces_shop_type`;
CREATE TABLE `ces_shop_type`  (
  `id` varchar(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `create_by` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '创建人',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建日期',
  `update_by` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '更新人',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新日期',
  `sys_org_code` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '所属部门',
  `name` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '分类名字',
  `content` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '描述',
  `pics` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '图片',
  `pid` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '父级节点',
  `has_child` varchar(3) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '是否有子节点',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of ces_shop_type
-- ----------------------------
INSERT INTO `ces_shop_type` VALUES ('1258408003595464706', 'admin', '2020-05-07 22:47:02', NULL, NULL, 'A01', '衣服', NULL, 'e1fe9925bc315c60addea1b98eb1cb1349547719_1588862821565.jpg', '0', '0');
INSERT INTO `ces_shop_type` VALUES ('1258408044439597058', 'admin', '2020-05-07 22:47:12', NULL, NULL, 'A01', '鞋子', NULL, 'jeewxshop测试号_1588862831749.jpg', '0', '1');
INSERT INTO `ces_shop_type` VALUES ('1258408076693794818', 'admin', '2020-05-07 22:47:20', 'admin', '2020-07-10 16:51:10', 'A01', '耐克', NULL, 'd52a2834349b033b6d20d5cc16ce36d3d539bd7f_1594371069797.jpg', '1258408044439597058', '0');
INSERT INTO `ces_shop_type` VALUES ('1258408105487691777', 'admin', '2020-05-07 22:47:27', 'admin', '2020-05-12 22:24:35', 'A01', '阿迪', NULL, 'temp/20180607175028Fn1Lq7zw_1589293474710.png', '1258408044439597058', '0');

-- ----------------------------
-- Table structure for dcs_app_profiles
-- ----------------------------
DROP TABLE IF EXISTS `dcs_app_profiles`;
CREATE TABLE `dcs_app_profiles`  (
  `id` varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `app_id` varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `create_time` datetime(0) NULL,
  `datacenter_name` varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `feature_suggest` varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `feature_choose` varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `scene_suggest` varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `scene_choose` varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;


-- ----------------------------
-- Table structure for dcs_apps
-- ----------------------------
DROP TABLE IF EXISTS `dcs_apps`;
CREATE TABLE `dcs_apps`  (
  `id` varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `cloud_resource_id` varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `create_time` datetime(0) NULL,
  `type` tinyint(4) NOT NULL,
  `from` varchar(50) COMMENT '来源',
  `deleted` tinyint(2) NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;


-- ----------------------------
-- Table structure for dcs_authorities
-- ----------------------------
DROP TABLE IF EXISTS `dcs_authorities`;
CREATE TABLE `dcs_authorities`  (
  `id` varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '主键',
  `name` varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '名称',
  `classification` varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '类型：基础设施：iaas:IT设备:device; 云平台资源:cloud; 应用资源:app',
  `enabled` tinyint(1) NOT NULL COMMENT '启用状态',
  `update_time` varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of dcs_authorities
-- ----------------------------
INSERT INTO `dcs_authorities` VALUES ('air_condition', '空调设备权限', 'iaas', 1, '2020-12-02 17:29:57.108');
INSERT INTO `dcs_authorities` VALUES ('app', '应用资源权限', 'app', 1, '2020-12-02 16:22:24');
INSERT INTO `dcs_authorities` VALUES ('distribution_box', '配电箱设备权限', 'iaas', 1, '2020-12-02 17:29:57.108');
INSERT INTO `dcs_authorities` VALUES ('k8s', '容器资源权限', 'cloud', 1, '2020-12-02 16:22:24');
-- INSERT INTO `dcs_authorities` VALUES ('pdu', 'PDU设备权限', 'iaas', 1, '2020-12-03 15:07:13.951');
INSERT INTO `dcs_authorities` VALUES ('server', '服务器设备权限', 'device', 1, '2020-12-02 17:31:10.206');
INSERT INTO `dcs_authorities` VALUES ('switcher', '交换机设备权限', 'device', 1, '2020-12-02 16:22:24');
-- INSERT INTO `dcs_authorities` VALUES ('temperture_sensor', '温度传感器设备权限', 'iaas', 0, '2020-12-02 17:29:57.107');
INSERT INTO `dcs_authorities` VALUES ('ups', 'UPS设备权限', 'iaas', 1, '2020-12-02 17:29:57.107');
INSERT INTO `dcs_authorities` VALUES ('vm', '虚拟机资源权限', 'cloud', 1, '2020-12-02 16:22:24');

-- ----------------------------
-- Table structure for dcs_classification_data
-- ----------------------------
DROP TABLE IF EXISTS `dcs_classification_data`;
CREATE TABLE `dcs_classification_data`  (
  `id` varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '主键',
  `timestamp` datetime(0) NOT NULL COMMENT '采样时间戳',
  `general_count` bigint(20) NOT NULL COMMENT '一般业务记录数',
  `vital_count` bigint(20) NOT NULL COMMENT '重要业务记录数',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;


-- ----------------------------
-- Table structure for dcs_energy_data
-- ----------------------------
DROP TABLE IF EXISTS `dcs_energy_data`;
CREATE TABLE `dcs_energy_data`  (
  `id` varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '主键',
  `timestamp` datetime(0) NOT NULL COMMENT '采样时间戳',
  `value` double NOT NULL COMMENT '采集能耗值',
  `count` bigint(20) NULL DEFAULT NULL COMMENT '采样次数',
  `datacenter_id` varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '数据中心id',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for dcs_data_backups
-- ----------------------------
DROP TABLE IF EXISTS `dcs_data_backups`;
CREATE TABLE `dcs_data_backups`  (
  `id` varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '主键',
  `name` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '名称',
  `policy` varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '策略，全量备份：full, 增量备份：increment',
  `create_time` datetime(0) NOT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for dcs_data_clean_configs
-- ----------------------------
DROP TABLE IF EXISTS `dcs_data_clean_configs`;
CREATE TABLE `dcs_data_clean_configs`  (
  `id` varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '主键',
  `clean_type` varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '策略类型',
  `clean_time` int(11) NULL DEFAULT NULL COMMENT '清洗多少天之前的数据',
  `enabled` tinyint(1) NOT NULL COMMENT '是否开启',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of dcs_data_clean_configs
-- ----------------------------
INSERT INTO `dcs_data_clean_configs` VALUES ('1', 'ALERT_HISTORY', 125, 1);

-- ----------------------------
-- Table structure for dcs_data_configs
-- ----------------------------
DROP TABLE IF EXISTS `dcs_data_configs`;
CREATE TABLE `dcs_data_configs`  (
  `id` varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '主键',
  `policy` varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '策略',
  `cron` varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '定时规则',
  `enabled` tinyint(1) NOT NULL COMMENT '启用',
  `update_time` varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '更新时间',
  `base_dir` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '增量基目录',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of dcs_data_configs
-- ----------------------------
INSERT INTO `dcs_data_configs` VALUES ('security', 'full', '13 12 11 * * ?', 0, '2020-11-30 19:20:39.26', '');

-- ----------------------------
-- Table structure for dcs_model_factors
-- ----------------------------
DROP TABLE IF EXISTS `dcs_model_factors`;
CREATE TABLE `dcs_model_factors`  (
  `id` varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `model_type` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `factor_type` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `factor_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `enabled` tinyint(1) NOT NULL,
  `score_sample` int(11) NOT NULL,
  `score_type` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `weight` int(11) NULL DEFAULT NULL,
  `unit_conversion` int(11) NOT NULL,
  `start_x` int(11) NOT NULL,
  `start_y` int(11) NOT NULL,
  `end_x` int(11) NOT NULL,
  `end_y` int(11) NOT NULL,
  `score_best_predict` int(11) NOT NULL,
  `score_predict_standard` int(11) NOT NULL,
  `score_history` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of dcs_model_factors
-- ----------------------------
INSERT INTO `dcs_model_factors` VALUES ('00000000-0000-0000-0001-000000000001', 'OPTIMAL_COST', 'server.power', '实时功耗', 1, 200, 'SAMPLE_PERCENTAGE', 15, 1, 0, 0, 500, 0, 250, 0);
INSERT INTO `dcs_model_factors` VALUES ('00000000-0000-0000-0001-000000000002', 'OPTIMAL_COST', 'host.cpu_util', 'CPU利用率', 1, 20, 'SAMPLE_PERCENTAGE', 20, 1, 0, 0, 100, 0, 50, 0);
INSERT INTO `dcs_model_factors` VALUES ('00000000-0000-0000-0001-000000000003', 'OPTIMAL_COST', 'host.cpu_load_1min', 'CPU负载', 1, 0, 'SAMPLE_PERCENTAGE', 20, 1, 0, 0, 10, 0, 5, 0);
INSERT INTO `dcs_model_factors` VALUES ('00000000-0000-0000-0001-000000000004', 'OPTIMAL_COST', 'host.memory_util', '内存利用率', 1, 0, 'SAMPLE_PERCENTAGE', 20, 1, 0, 0, 100, 0, 50, 0);
INSERT INTO `dcs_model_factors` VALUES ('00000000-0000-0000-0001-000000000005', 'OPTIMAL_COST', 'server.bandWidth', '带宽', 1, 0, 'SAMPLE_PERCENTAGE', 20, 1, 0, 0, 2000, 0, 1000, 0);
INSERT INTO `dcs_model_factors` VALUES ('00000000-0000-0000-0001-000000000006', 'OPTIMAL_COST', 'server.storage_available', '存储可用量', 1, 0, 'SAMPLE_PERCENTAGE', 5, 1, 0, 0, 128, 0, 64, 0);
INSERT INTO `dcs_model_factors` VALUES ('00000000-0000-0000-0001-000000001001', 'OPTIMAL_DEVICE', 'server.power', '实时功耗', 1, 0, 'SAMPLE_PERCENTAGE', 20, 1, 0, 0, 500, 0, 250, 0);
INSERT INTO `dcs_model_factors` VALUES ('00000000-0000-0000-0001-000000001002', 'OPTIMAL_DEVICE', 'host.temperature', '温度', 1, 0, 'SAMPLE_PERCENTAGE', 15, 1, 0, 0, 80, 0, 40, 0);
INSERT INTO `dcs_model_factors` VALUES ('00000000-0000-0000-0001-000000001003', 'OPTIMAL_DEVICE', 'server.mem_speed', '内存速率', 1, 0, 'SAMPLE_PERCENTAGE', 15, 1, 0, 0, 4400, 0, 2200, 0);
INSERT INTO `dcs_model_factors` VALUES ('00000000-0000-0000-0001-000000001004', 'OPTIMAL_DEVICE', 'server.cpu_frequency', 'CPU频率', 1, 0, 'SAMPLE_PERCENTAGE', 20, 1, 0, 0, 5, 0, 2.5, 0);
INSERT INTO `dcs_model_factors` VALUES ('00000000-0000-0000-0001-000000001005', 'OPTIMAL_DEVICE', 'host.disk_read_rate', '磁盘读速率', 1, 0, 'SAMPLE_PERCENTAGE', 15, 1, 0, 0, 20480, 0, 10240, 0);
INSERT INTO `dcs_model_factors` VALUES ('00000000-0000-0000-0001-000000001006', 'OPTIMAL_DEVICE', 'host.disk_write_rate', '磁盘写速率', 1, 0, 'SAMPLE_PERCENTAGE', 15, 1, 0, 0, 20480, 0, 10240, 0);
INSERT INTO `dcs_model_factors` VALUES ('00000000-0000-0000-0001-000000002001', 'AIR', 'environment.air_in_temperature', '回风温度', 1, 23, 'SAMPLE_PERCENTAGE', 20, 1, 0, 0, 30, 0, 25, 0);
INSERT INTO `dcs_model_factors` VALUES ('00000000-0000-0000-0001-000000002002', 'AIR', 'environment.air_out_temperature', '出风温度', 1, 28, 'SAMPLE_PERCENTAGE', 20, 1, 0, 0, 30, 0, 25, 0);
INSERT INTO `dcs_model_factors` VALUES ('00000000-0000-0000-0001-000000002003', 'AIR', 'environment.air_setting_temperature', '设定温度', 1, 0, 'SAMPLE_PERCENTAGE', 30, 1, 0, 0, 30, 0, 25, 0);
INSERT INTO `dcs_model_factors` VALUES ('00000000-0000-0000-0001-000000002004', 'AIR', 'environment.air_in_humidity', '回风湿度', 1, 0, 'SAMPLE_PERCENTAGE', 10, 1, 0, 0, 60, 0, 50, 0);
INSERT INTO `dcs_model_factors` VALUES ('00000000-0000-0000-0001-000000002005', 'AIR', 'environment.air_out_humidity', '出风湿度', 1, 0, 'SAMPLE_PERCENTAGE', 20, 1, 0, 0, 10, 0, 2.5, 0);
INSERT INTO `dcs_model_factors` VALUES ('00000000-0000-0000-0001-000000003001', 'UPS', 'environment.ups_in_voltage', '输入电压', 1, 400, 'SAMPLE_PERCENTAGE', 50, 1, 0, 0, 242, 0, 800, 0);
INSERT INTO `dcs_model_factors` VALUES ('00000000-0000-0000-0001-000000003002', 'UPS', 'environment.ups_out_voltage', '输出电压', 1, 400, 'SAMPLE_PERCENTAGE', 20, 1, 0, 0, 242, 0, 800, 0);
INSERT INTO `dcs_model_factors` VALUES ('00000000-0000-0000-0001-000000003003', 'UPS', 'environment.ups_temperature', '温度', 1, 0, 'SAMPLE_PERCENTAGE', 30, 1, 0, 0, 43, 0, 25, 0);
INSERT INTO `dcs_model_factors` VALUES ('00000000-0000-0000-0001-000000004001', 'COST', 'cost.temperature', '迁移温度差', 1, 12, 'SAMPLE_PERCENTAGE', 25, 1, 0, 0, 20, 0, 2, 0);
INSERT INTO `dcs_model_factors` VALUES ('00000000-0000-0000-0001-000000004002', 'COST', 'cost.load', '迁移负荷差', 1, 0, 'SAMPLE_PERCENTAGE', 25, 1, 0, 0, 500, 0, 50, 0);
INSERT INTO `dcs_model_factors` VALUES ('00000000-0000-0000-0001-000000004003', 'COST', 'cost.power', '迁移能耗差', 1, 0, 'SAMPLE_PERCENTAGE', 25, 1, 0, 0, 200, 0, 25, 0);
INSERT INTO `dcs_model_factors` VALUES ('00000000-0000-0000-0001-000000004004', 'COST', 'cost.response_time', '迁移响应时间差', 1, 0, 'SAMPLE_PERCENTAGE', 25, 1, 0, 0, 10, 0, 0.5, 0);

INSERT INTO `dcs_model_factors` VALUES ('00000000-0000-0000-0001-000000007001', 'DATACENTER_COST', 'datacenter.load', '配电子网负荷', 1, 0, 'SAMPLE_PERCENTAGE', 15, 1, 0, 0, 500, 0, 217, 0);
INSERT INTO `dcs_model_factors` VALUES ('00000000-0000-0000-0001-000000007002', 'DATACENTER_COST', 'environment.distribution_box_power', '配电有功功率', 1, 0, 'SAMPLE_PERCENTAGE', 7, 1, 0, 0, 10, 0, 217, 0);
INSERT INTO `dcs_model_factors` VALUES ('00000000-0000-0000-0001-000000007003', 'DATACENTER_COST', 'environment.distribution_box_energy', '配电有功电能', 1, 0, 'SAMPLE_PERCENTAGE', 7, 1, 0, 0, 1000, 0, 217, 0);
INSERT INTO `dcs_model_factors` VALUES ('00000000-0000-0000-0001-000000007004', 'DATACENTER_COST', 'datacenter.server_power', '服务器实时功耗', 1, 0, 'SAMPLE_PERCENTAGE', 7, 1, 0, 0, 500, 0, 217, 0);
INSERT INTO `dcs_model_factors` VALUES ('00000000-0000-0000-0001-000000007005', 'DATACENTER_COST', 'datacenter.bandWidth', '服务器带宽', 1, 0, 'SAMPLE_PERCENTAGE', 7, 1, 0, 0, 2000, 0, 217, 0);
INSERT INTO `dcs_model_factors` VALUES ('00000000-0000-0000-0001-000000007006', 'DATACENTER_COST', 'datacenter.server_cpu_frequency', '服务器cpu频率', 1, 0, 'SAMPLE_PERCENTAGE', 7, 1, 0, 0, 5, 0, 217, 0);
INSERT INTO `dcs_model_factors` VALUES ('00000000-0000-0000-0001-000000007007', 'DATACENTER_COST', 'datacenter.server_temperature', '服务器温度', 1, 0, 'SAMPLE_PERCENTAGE', 7, 1, 0, 0, 85, 0, 217, 0);
INSERT INTO `dcs_model_factors` VALUES ('00000000-0000-0000-0001-000000007008', 'DATACENTER_COST', 'datacenter.cpu_util', 'CPU使用率', 1, 0, 'SAMPLE_PERCENTAGE', 7, 1, 0, 0, 100, 0, 217, 0);
INSERT INTO `dcs_model_factors` VALUES ('00000000-0000-0000-0001-000000007009', 'DATACENTER_COST', 'datacenter.memory_util', '内存使用率', 1, 0, 'SAMPLE_PERCENTAGE', 7, 1, 0, 0, 100, 0, 217, 0);
INSERT INTO `dcs_model_factors` VALUES ('00000000-0000-0000-0001-000000007010', 'DATACENTER_COST', 'datacenter.qos', '应用QOS', 1, 0, 'SAMPLE_PERCENTAGE', 7, 1, 0, 0, 10240, 0, 217, 0);
INSERT INTO `dcs_model_factors` VALUES ('00000000-0000-0000-0001-000000007011', 'DATACENTER_COST', 'datacenter.app_cpu_util', '应用CPU使用率', 1, 0, 'SAMPLE_PERCENTAGE', 7, 1, 0, 0, 100, 0, 217, 0);
INSERT INTO `dcs_model_factors` VALUES ('00000000-0000-0000-0001-000000007012', 'DATACENTER_COST', 'datacenter.app_memory_util', '应用内存使用率', 1, 0, 'SAMPLE_PERCENTAGE', 5, 1, 0, 0, 100, 0, 217, 0);

INSERT INTO `dcs_model_factors` VALUES ('00000000-0000-0000-0001-000000007013', 'DATACENTER_DEVICE', 'datacenter.load', '配电子网负荷', 1, 0, 'SAMPLE_PERCENTAGE', 10, 1, 0, 0, 500, 0, 10, 0);
INSERT INTO `dcs_model_factors` VALUES ('00000000-0000-0000-0001-000000007014', 'DATACENTER_DEVICE', 'environment.air_in_temperature', '空调回风温度', 1, 0, 'SAMPLE_PERCENTAGE', 5, 1, 0, 0, 30, 0, 30, 0);
INSERT INTO `dcs_model_factors` VALUES ('00000000-0000-0000-0001-000000007015', 'DATACENTER_DEVICE', 'environment.air_out_temperature', '空调出风温度', 1, 0, 'SAMPLE_PERCENTAGE', 5, 1, 0, 0, 30, 0, 30, 0);
INSERT INTO `dcs_model_factors` VALUES ('00000000-0000-0000-0001-000000007016', 'DATACENTER_DEVICE', 'environment.air_setting_temperature', '空调设定温度', 1, 0, 'SAMPLE_PERCENTAGE', 5, 1, 0, 0, 30, 0, 30, 0);
INSERT INTO `dcs_model_factors` VALUES ('00000000-0000-0000-0001-000000007017', 'DATACENTER_DEVICE', 'environment.air_in_wind', '空调回风量', 1, 0, 'SAMPLE_PERCENTAGE', 5, 1, 0, 0, 30, 0, 60, 0);
INSERT INTO `dcs_model_factors` VALUES ('00000000-0000-0000-0001-000000007018', 'DATACENTER_DEVICE', 'environment.air_out_wind', '空调出风量', 1, 0, 'SAMPLE_PERCENTAGE', 5, 1, 0, 0, 30, 0, 10, 0);
INSERT INTO `dcs_model_factors` VALUES ('00000000-0000-0000-0001-000000007019', 'DATACENTER_DEVICE', 'datacenter.server_power', '服务器实时功耗', 1, 0, 'SAMPLE_PERCENTAGE', 5, 1, 0, 0, 500, 0, 10, 0);
INSERT INTO `dcs_model_factors` VALUES ('00000000-0000-0000-0001-000000007020', 'DATACENTER_DEVICE', 'datacenter.server_cpu_load_1min', '服务器cpu负载', 1, 0, 'SAMPLE_PERCENTAGE', 5, 1, 0, 0, 30, 0, 10, 0);
INSERT INTO `dcs_model_factors` VALUES ('00000000-0000-0000-0001-000000007021', 'DATACENTER_DEVICE', 'datacenter.mem_speed', '服务器内存速率', 1, 0, 'SAMPLE_PERCENTAGE', 5, 1, 0, 0, 5000, 0, 10, 0);
INSERT INTO `dcs_model_factors` VALUES ('00000000-0000-0000-0001-000000007022', 'DATACENTER_DEVICE', 'datacenter.server_disk_write_rate', '服务器磁盘写速率', 1, 0, 'SAMPLE_PERCENTAGE', 5, 1, 0, 0, 1024000, 0, 10, 0);
INSERT INTO `dcs_model_factors` VALUES ('00000000-0000-0000-0001-000000007023', 'DATACENTER_DEVICE', 'datacenter.server_disk_read_rate', '服务器磁盘读速率', 1, 0, 'SAMPLE_PERCENTAGE', 5, 1, 0, 0, 1024000, 0, 10, 0);
INSERT INTO `dcs_model_factors` VALUES ('00000000-0000-0000-0001-000000007024', 'DATACENTER_DEVICE', 'datacenter.cpu_util', 'CPU使用率', 1, 0, 'SAMPLE_PERCENTAGE', 5, 1, 0, 0, 100, 0, 217, 0);
INSERT INTO `dcs_model_factors` VALUES ('00000000-0000-0000-0001-000000007025', 'DATACENTER_DEVICE', 'datacenter.memory_util', '内存使用率', 1, 0, 'SAMPLE_PERCENTAGE', 5, 1, 0, 0, 100, 0, 217, 0);
INSERT INTO `dcs_model_factors` VALUES ('00000000-0000-0000-0001-000000007026', 'DATACENTER_DEVICE', 'datacenter.qos', '应用QOS', 1, 0, 'SAMPLE_PERCENTAGE', 10, 1, 0, 0, 10240, 0, 217, 0);
INSERT INTO `dcs_model_factors` VALUES ('00000000-0000-0000-0001-000000007027', 'DATACENTER_DEVICE', 'datacenter.app_cpu_util', '应用CPU使用率', 1, 0, 'SAMPLE_PERCENTAGE', 5, 1, 0, 0, 100, 0, 217, 0);
INSERT INTO `dcs_model_factors` VALUES ('00000000-0000-0000-0001-000000007028', 'DATACENTER_DEVICE', 'datacenter.app_memory_util', '应用内存使用率', 1, 0, 'SAMPLE_PERCENTAGE', 5, 1, 0, 0, 100, 0, 217, 0);

INSERT INTO `dcs_model_factors` VALUES ('00000000-0000-0000-0001-000000007029', 'DATACENTER_POWER', 'datacenter.load', '配电子网负荷', 1, 0, 'SAMPLE_PERCENTAGE', 10, 1, 0, 0, 500, 0, 10, 0);
INSERT INTO `dcs_model_factors` VALUES ('00000000-0000-0000-0001-000000007030', 'DATACENTER_POWER', 'environment.distribution_box_power', '配电有功功率', 1, 0, 'SAMPLE_PERCENTAGE', 7, 1, 0, 0, 10, 0, 217, 0);
INSERT INTO `dcs_model_factors` VALUES ('00000000-0000-0000-0001-000000007031', 'DATACENTER_POWER', 'environment.distribution_box_energy', '配电有功电能', 1, 0, 'SAMPLE_PERCENTAGE', 7, 1, 0, 0, 1000, 0, 217, 0);
INSERT INTO `dcs_model_factors` VALUES ('00000000-0000-0000-0001-000000007032', 'DATACENTER_POWER', 'environment.air_in_wind', '空调回风量', 1, 0, 'SAMPLE_PERCENTAGE', 5, 1, 0, 0, 30, 0, 60, 0);
INSERT INTO `dcs_model_factors` VALUES ('00000000-0000-0000-0001-000000007033', 'DATACENTER_POWER', 'environment.air_out_wind', '空调出风量', 1, 0, 'SAMPLE_PERCENTAGE', 5, 1, 0, 0, 30, 0, 10, 0);
INSERT INTO `dcs_model_factors` VALUES ('00000000-0000-0000-0001-000000007034', 'DATACENTER_POWER', 'environment.air_setting_temperature', '空调设定温度', 1, 0, 'SAMPLE_PERCENTAGE', 5, 1, 0, 0, 30, 0, 30, 0);
INSERT INTO `dcs_model_factors` VALUES ('00000000-0000-0000-0001-000000007035', 'DATACENTER_POWER', 'datacenter.server_power', '服务器实时功耗', 1, 0, 'SAMPLE_PERCENTAGE', 5, 1, 0, 0, 500, 0, 10, 0);
INSERT INTO `dcs_model_factors` VALUES ('00000000-0000-0000-0001-000000007036', 'DATACENTER_POWER', 'datacenter.server_temperature', '服务器温度', 1, 0, 'SAMPLE_PERCENTAGE', 7, 1, 0, 0, 85, 0, 217, 0);
INSERT INTO `dcs_model_factors` VALUES ('00000000-0000-0000-0001-000000007037', 'DATACENTER_POWER', 'datacenter.cpu_util', 'CPU使用率', 1, 0, 'SAMPLE_PERCENTAGE', 5, 1, 0, 0, 100, 0, 217, 0);
INSERT INTO `dcs_model_factors` VALUES ('00000000-0000-0000-0001-000000007038', 'DATACENTER_POWER', 'datacenter.memory_util', '内存使用率', 1, 0, 'SAMPLE_PERCENTAGE', 5, 1, 0, 0, 100, 0, 217, 0);
INSERT INTO `dcs_model_factors` VALUES ('00000000-0000-0000-0001-000000007039', 'DATACENTER_POWER', 'datacenter.app_cpu_util', '应用CPU使用率', 1, 0, 'SAMPLE_PERCENTAGE', 5, 1, 0, 0, 100, 0, 217, 0);
INSERT INTO `dcs_model_factors` VALUES ('00000000-0000-0000-0001-000000007040', 'DATACENTER_POWER', 'datacenter.app_memory_util', '应用内存使用率', 1, 0, 'SAMPLE_PERCENTAGE', 5, 1, 0, 0, 100, 0, 217, 0);

INSERT INTO `dcs_model_factors` VALUES ('00000000-0000-0000-0001-000000007041', 'DATACENTER_ELECTRIC', 'datacenter.load', '配电子网负荷', 1, 0, 'SAMPLE_PERCENTAGE', 10, 1, 0, 0, 500, 0, 10, 0);
INSERT INTO `dcs_model_factors` VALUES ('00000000-0000-0000-0001-000000007042', 'DATACENTER_ELECTRIC', 'environment.distribution_box_power', '配电有功功率', 1, 0, 'SAMPLE_PERCENTAGE', 7, 1, 0, 0, 10, 0, 217, 0);
INSERT INTO `dcs_model_factors` VALUES ('00000000-0000-0000-0001-000000007043', 'DATACENTER_ELECTRIC', 'environment.distribution_box_energy', '配电有功电能', 1, 0, 'SAMPLE_PERCENTAGE', 7, 1, 0, 0, 1000, 0, 217, 0);
INSERT INTO `dcs_model_factors` VALUES ('00000000-0000-0000-0001-000000007044', 'DATACENTER_ELECTRIC', 'environment.air_setting_temperature', '空调设定温度', 1, 0, 'SAMPLE_PERCENTAGE', 5, 1, 0, 0, 30, 0, 30, 0);
INSERT INTO `dcs_model_factors` VALUES ('00000000-0000-0000-0001-000000007045', 'DATACENTER_ELECTRIC', 'datacenter.server_power', '服务器实时功耗', 1, 0, 'SAMPLE_PERCENTAGE', 5, 1, 0, 0, 500, 0, 10, 0);
INSERT INTO `dcs_model_factors` VALUES ('00000000-0000-0000-0001-000000007046', 'DATACENTER_ELECTRIC', 'datacenter.server_temperature', '服务器温度', 1, 0, 'SAMPLE_PERCENTAGE', 7, 1, 0, 0, 85, 0, 217, 0);
INSERT INTO `dcs_model_factors` VALUES ('00000000-0000-0000-0001-000000007047', 'DATACENTER_ELECTRIC', 'datacenter.cpu_util', 'CPU使用率', 1, 0, 'SAMPLE_PERCENTAGE', 5, 1, 0, 0, 100, 0, 217, 0);
INSERT INTO `dcs_model_factors` VALUES ('00000000-0000-0000-0001-000000007048', 'DATACENTER_ELECTRIC', 'datacenter.memory_util', '内存使用率', 1, 0, 'SAMPLE_PERCENTAGE', 5, 1, 0, 0, 100, 0, 217, 0);
INSERT INTO `dcs_model_factors` VALUES ('00000000-0000-0000-0001-000000007049', 'DATACENTER_ELECTRIC', 'datacenter.app_cpu_util', '应用CPU使用率', 1, 0, 'SAMPLE_PERCENTAGE', 5, 1, 0, 0, 100, 0, 217, 0);
INSERT INTO `dcs_model_factors` VALUES ('00000000-0000-0000-0001-000000007050', 'DATACENTER_ELECTRIC', 'datacenter.app_memory_util', '应用内存使用率', 1, 0, 'SAMPLE_PERCENTAGE', 5, 1, 0, 0, 100, 0, 217, 0);

INSERT INTO `dcs_model_factors` VALUES ('00000000-0000-0000-0001-000000001101', 'APP', 'container.cpu_util', '应用CPU使用率', 1, 0, 'SAMPLE_PERCENTAGE', 25, 1, 0, 0, 100, 0, 60, 0);
INSERT INTO `dcs_model_factors` VALUES ('00000000-0000-0000-0001-000000001102', 'APP', 'container.memory_util', '应用内存使用率', 1, 0, 'SAMPLE_PERCENTAGE', 25, 1, 0, 0, 100, 0, 60, 0);
INSERT INTO `dcs_model_factors` VALUES ('00000000-0000-0000-0001-000000001103', 'APP', 'container.bandWidth', '应用QOS', 1, 0, 'SAMPLE_PERCENTAGE', 25, 1, 0, 0, 10240, 0, 1024, 0);
INSERT INTO `dcs_model_factors` VALUES ('00000000-0000-0000-0001-000000001104', 'APP', 'container.respond_time', '应用迁移时长', 1, 0, 'SAMPLE_PERCENTAGE', 25, 1, 0, 0, 10, 0, 0, 0);

-- ----------------------------
-- Table structure for dcs_model_scores
-- ----------------------------
DROP TABLE IF EXISTS `dcs_model_scores`;
CREATE TABLE `dcs_model_scores`  (
  `id` varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `resource_id` varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `model_type` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `scores` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `total_score` int(11) NOT NULL,
  `factor_names` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `samples` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `values` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for dcs_scheduler_configs
-- ----------------------------
DROP TABLE IF EXISTS `dcs_scheduler_configs`;
CREATE TABLE `dcs_scheduler_configs`  (
  `id` varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `datacenter_id` varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `threshold_key` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '调度阈值KEY',
  `threshold_operation` varchar(5) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '运算符',
  `threshold_value` double(36, 3) NULL DEFAULT NULL COMMENT '调度阈值',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for `dcs_scheduler_jobs`
-- ----------------------------
DROP TABLE IF EXISTS `dcs_scheduler_jobs`;
CREATE TABLE `dcs_scheduler_jobs` (
  `id` varchar(36) NOT NULL,
  `status` int(11) NOT NULL,
  `app_id` varchar(36) NOT NULL,
  `src_datacenter_id` varchar(36) DEFAULT NULL,
  `dst_datacenter_id` varchar(36) NOT NULL,
  `dst_node_id` varchar(36) DEFAULT NULL,
  `begin_time` datetime DEFAULT NULL,
  `end_time` datetime DEFAULT NULL,
  `scheduler_model_tool` int(11) DEFAULT NULL,
  `scheduler_strategy_config` int(11) DEFAULT NULL,
  `scheduler_model` int(11) DEFAULT NULL,
  `app_name` varchar(36) DEFAULT NULL,
  `cloud_resource_name` varchar(36) DEFAULT NULL,
  `cloud_resource_type_name` varchar(36) DEFAULT NULL,
  `cloud_resource_id` varchar(36) DEFAULT NULL,
  `source_node_id` varchar(36) DEFAULT NULL,
  `instance_id` varchar(36) DEFAULT NULL COMMENT '迁移应用涉及的云平台实例：如虚拟机和namespace',
  `instance_name` varchar(64) DEFAULT NULL,
  `dst_cloud_resource_id` varchar(36) DEFAULT NULL COMMENT '迁移的目标云资源ID',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;


-- ----------------------------
-- Table structure for dcs_scheduler_steps
-- ----------------------------
DROP TABLE IF EXISTS `dcs_scheduler_steps`;
CREATE TABLE `dcs_scheduler_steps`  (
  `id` varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `scheduler_job_id` varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `status` int(11) NOT NULL,
  `name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `begin_time` datetime(0) NULL DEFAULT NULL,
  `end_time` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for dcs_scheduler_imagine
-- ----------------------------
DROP TABLE IF EXISTS `dcs_scheduler_imagine`;
CREATE TABLE `dcs_scheduler_imagine` (
`id`  varchar(36) NOT NULL ,
`app_id`  varchar(36) NOT NULL ,
`source_node_id`  varchar(36) NULL ,
`dest_node_id`  varchar(36) NOT NULL ,
`model_type`  varchar(255) NOT NULL ,
`update_time`  datetime NULL ,
`difference`  double(11,2) NULL ,
`temperature_imagine`  double(11,2) NULL ,
`distribution_box_voltage_imagine`  double(11,2) NULL ,
`datacenter_energy_imagine`  double(11,2) NULL ,
`business_respond_imagine`  double(11,2) NULL ,
`temperature`  double(11,2) NULL ,
`distribution_box_voltage`  double(11,2) NULL ,
`datacenter_energy`  double(11,2) NULL ,
`business_respond`  double(11,2) NULL ,
`imagines`  varchar(255) NULL ,
`values` varchar(255) NULL,
`factor_types` varchar(4000) NULL,
PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;




-- ----------------------------
-- Table structure for dcs_subhealth_alerts
-- ----------------------------
DROP TABLE IF EXISTS `dcs_subhealth_alerts`;
CREATE TABLE `dcs_subhealth_alerts`  (
  `id` varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `device_type` varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `meter` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `meter_name` varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `resource_id` varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `resource_name` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `content` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `create_time` datetime(0) NULL,
  `level` varchar(36) NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- order by create_time 排序索引
create index index_sort_create_time on `dcs_subhealth_alerts`(`create_time`, `device_type`, `meter`, `meter_name`, `resource_id`, `resource_name`, `level`, `content`);

-- ----------------------------
-- Table structure for dcs_subhealth_infos
-- ----------------------------
DROP TABLE IF EXISTS `dcs_subhealth_infos`;
CREATE TABLE `dcs_subhealth_infos`  (
  `id` varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `device_type` varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `subhealth` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of dcs_subhealth_infos
-- ----------------------------
-- INSERT INTO `dcs_subhealth_infos` VALUES ('1093a981-4b4c-4c12-8c44-58aea4e68ed0', 'UPS', 0);
-- INSERT INTO `dcs_subhealth_infos` VALUES ('36fa22ae-8858-4817-b1b1-c667a5012ddc', 'DATACENTER', 0);
-- INSERT INTO `dcs_subhealth_infos` VALUES ('a7b9ddf4-50d1-4e4c-89e3-22e67c18b308', 'AIR', 0);

-- ----------------------------
-- Table structure for dcs_subhealth_thresholds
-- ----------------------------
DROP TABLE IF EXISTS `dcs_subhealth_thresholds`;
CREATE TABLE `dcs_subhealth_thresholds`  (
  `id` varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '主键',
  `resource_id` varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '资源id',
  `device_type` varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '设备类型：UPS|AIR|DATACENTER',
  `meter` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '监控指标',
  `meter_name` varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '监控指标名',
  `level` varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '告警级别：一般：info；警告：warning；严重：critical',
  `upper_threshold` float NULL DEFAULT NULL COMMENT '阈值上限值',
  `lower_threshold` float NULL DEFAULT NULL COMMENT '阈值下限值',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;


-- ----------------------------
-- Table structure for dcs_voltage_fm
-- ----------------------------
DROP TABLE IF EXISTS `dcs_voltage_fm`;
CREATE TABLE `dcs_voltage_fm`  (
  `id` varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `resource_id` varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `rated_power` float(11, 0) NOT NULL COMMENT '额定功率',
  `power_consumption` float(11, 0) NOT NULL COMMENT '功耗',
  `voltage_fm_strategy` varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '调频策略',
  `support_strategies` varchar(160) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '支持策略',
  `frequency` float(11, 0) NOT NULL COMMENT '频率',
  `control_type` int(11) NULL DEFAULT 0 COMMENT '控制方式，0为自动，1为手动',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;


-- ----------------------------
-- Table structure for demo
-- ----------------------------
DROP TABLE IF EXISTS `demo`;
CREATE TABLE `demo`  (
  `id` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '主键ID',
  `name` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '姓名',
  `key_word` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '关键词',
  `punch_time` datetime(0) NULL DEFAULT NULL COMMENT '打卡时间',
  `salary_money` decimal(10, 3) NULL DEFAULT NULL COMMENT '工资',
  `bonus_money` double(10, 2) NULL DEFAULT NULL COMMENT '奖金',
  `sex` varchar(2) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '性别 {男:1,女:2}',
  `age` int(11) NULL DEFAULT NULL COMMENT '年龄',
  `birthday` date NULL DEFAULT NULL COMMENT '生日',
  `email` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '邮箱',
  `content` varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '个人简介',
  `create_by` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '创建人',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '修改人',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '修改时间',
  `sys_org_code` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '所属部门编码',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of demo
-- ----------------------------
INSERT INTO `demo` VALUES ('1260935385049055234', '111', '111', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'jeecg', '2020-05-14 22:09:57', NULL, NULL, 'A02A01');
INSERT INTO `demo` VALUES ('1260935402346364930', '22', '222', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'jeecg', '2020-05-14 22:10:02', NULL, NULL, 'A02A01');
INSERT INTO `demo` VALUES ('1c2ba51b29a42d9de02bbd708ea8121a', '777777', '777', '2018-12-07 19:43:17', NULL, NULL, NULL, 7, '2018-12-07', NULL, NULL, NULL, NULL, 'admin', '2019-02-21 18:26:04', NULL);
INSERT INTO `demo` VALUES ('1dc29e80be14d1400f165b5c6b30c707', 'zhang daihao', NULL, NULL, NULL, NULL, '2', NULL, NULL, 'zhangdaiscott@163.com', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `demo` VALUES ('304e651dc769d5c9b6e08fb30457a602', '小白兔', NULL, NULL, NULL, NULL, '2', 28, NULL, NULL, NULL, 'scott', '2019-01-19 13:12:53', 'qinfeng', '2019-01-19 13:13:12', NULL);
INSERT INTO `demo` VALUES ('4', 'Sandy', '开源，很好', '2018-12-15 00:00:00', NULL, NULL, '2', 21, '2018-12-15', 'test4@baomidou.com', '聪明00', NULL, NULL, 'admin', '2019-02-25 16:29:27', NULL);
INSERT INTO `demo` VALUES ('4436302a0de50bb83025286bc414d6a9', 'zhang daihao', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'zhangdaiscott@163.com', NULL, 'admin', '2019-01-19 15:39:04', NULL, NULL, NULL);
INSERT INTO `demo` VALUES ('4981637bf71b0c1ed1365241dfcfa0ea', '小虎', NULL, NULL, NULL, NULL, '2', 28, NULL, NULL, NULL, 'scott5', '2019-01-19 13:12:53', 'qinfeng', '2019-01-19 13:13:12', 'A02');
INSERT INTO `demo` VALUES ('7', 'zhangdaiscott', NULL, NULL, NULL, NULL, '1', NULL, '2019-01-03', NULL, NULL, NULL, NULL, NULL, NULL, 'A02A01A01');
INSERT INTO `demo` VALUES ('73bc58611012617ca446d8999379e4ac', '郭靖', '777', '2018-12-07 00:00:00', NULL, NULL, '1', NULL, NULL, NULL, NULL, 'jeecg-boot', '2019-03-28 18:16:39', 'admin', '2020-05-02 18:14:14', 'A02A01A02');
INSERT INTO `demo` VALUES ('917e240eaa0b1b2d198ae869b64a81c3', 'zhang daihao', NULL, NULL, NULL, NULL, '2', 0, '2018-11-29', 'zhangdaiscott@163.com', NULL, NULL, NULL, NULL, NULL, 'A02');
INSERT INTO `demo` VALUES ('94420c5d8fc4420dde1e7196154b3a24', '秦风', NULL, NULL, NULL, NULL, '2', NULL, NULL, NULL, NULL, 'scott', '2019-01-19 12:54:58', 'admin', '2020-05-02 18:14:33', NULL);
INSERT INTO `demo` VALUES ('b86897900c770503771c7bb88e5d1e9b', 'scott1', '开源、很好、hello', NULL, NULL, NULL, '1', NULL, NULL, 'zhangdaiscott@163.com', NULL, 'scott', '2019-01-19 12:22:34', NULL, NULL, NULL);
INSERT INTO `demo` VALUES ('c28fa8391ef81d6fabd8bd894a7615aa', '小麦', NULL, NULL, NULL, NULL, '2', NULL, NULL, 'zhangdaiscott@163.com', NULL, 'jeecg-boot', '2019-04-04 17:18:09', NULL, NULL, NULL);
INSERT INTO `demo` VALUES ('c2c0d49e3c01913067cf8d1fb3c971d2', 'zhang daihao', NULL, NULL, NULL, NULL, '2', NULL, NULL, 'zhangdaiscott@163.com', NULL, 'admin', '2019-01-19 23:37:18', 'admin', '2019-01-21 16:49:06', NULL);
INSERT INTO `demo` VALUES ('c96279c666b4b82e3ef1e4e2978701ce', '报名时间', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'jeecg-boot', '2019-03-28 18:00:52', NULL, NULL, NULL);
INSERT INTO `demo` VALUES ('d24668721446e8478eeeafe4db66dcff', 'zhang daihao999', NULL, NULL, NULL, NULL, '1', NULL, NULL, 'zhangdaiscott@163.com', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `demo` VALUES ('eaa6c1116b41dc10a94eae34cf990133', 'zhang daihao', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'zhangdaiscott@163.com', NULL, NULL, NULL, NULL, NULL, NULL);

-- ----------------------------
-- Table structure for demo_field_def_val_main
-- ----------------------------
DROP TABLE IF EXISTS `demo_field_def_val_main`;
CREATE TABLE `demo_field_def_val_main`  (
  `id` varchar(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `code` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '编码',
  `name` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '姓名',
  `sex` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '性别',
  `address` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '地址',
  `address_param` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '地址（传参）',
  `create_by` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '创建人',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建日期',
  `update_by` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '更新人',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新日期',
  `sys_org_code` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '所属部门',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of demo_field_def_val_main
-- ----------------------------
INSERT INTO `demo_field_def_val_main` VALUES ('1256548579457114114', 'q', 'q', '1', 'q', 'q', 'admin', '2020-05-02 19:38:21', 'admin', '2020-05-03 01:02:51', 'A01');

-- ----------------------------
-- Table structure for demo_field_def_val_sub
-- ----------------------------
DROP TABLE IF EXISTS `demo_field_def_val_sub`;
CREATE TABLE `demo_field_def_val_sub`  (
  `id` varchar(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `code` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '编码',
  `name` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '名称',
  `date` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '日期',
  `main_id` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '主表ID',
  `create_by` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '创建人',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建日期',
  `update_by` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '更新人',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新日期',
  `sys_org_code` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '所属部门',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of demo_field_def_val_sub
-- ----------------------------
INSERT INTO `demo_field_def_val_sub` VALUES ('15884194974080425602', '133', '管理员', '2020-05-02', '1256548579457114114', 'admin', '2020-05-03 01:02:51', NULL, NULL, 'A01');
INSERT INTO `demo_field_def_val_sub` VALUES ('15884194979201535108', '1144', '管理员', '2020-05-02', '1256548579457114114', 'admin', '2020-05-03 01:02:51', NULL, NULL, 'A01');

-- ----------------------------
-- Table structure for jeecg_monthly_growth_analysis
-- ----------------------------
DROP TABLE IF EXISTS `jeecg_monthly_growth_analysis`;
CREATE TABLE `jeecg_monthly_growth_analysis`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `year` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `month` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '月份',
  `main_income` decimal(18, 2) NULL DEFAULT 0.00 COMMENT '佣金/主营收入',
  `other_income` decimal(18, 2) NULL DEFAULT 0.00 COMMENT '其他收入',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 25 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of jeecg_monthly_growth_analysis
-- ----------------------------
INSERT INTO `jeecg_monthly_growth_analysis` VALUES (1, '2018', '1月', 114758.90, 4426054.19);
INSERT INTO `jeecg_monthly_growth_analysis` VALUES (2, '2018', '2月', 8970734.12, 1230188.67);
INSERT INTO `jeecg_monthly_growth_analysis` VALUES (3, '2018', '3月', 26755421.23, 2048836.84);
INSERT INTO `jeecg_monthly_growth_analysis` VALUES (4, '2018', '4月', 2404990.63, 374171.44);
INSERT INTO `jeecg_monthly_growth_analysis` VALUES (5, '2018', '5月', 5450793.02, 502306.10);
INSERT INTO `jeecg_monthly_growth_analysis` VALUES (6, '2018', '6月', 17186212.11, 1375154.97);
INSERT INTO `jeecg_monthly_growth_analysis` VALUES (7, '2018', '7月', 579975.67, 461483.99);
INSERT INTO `jeecg_monthly_growth_analysis` VALUES (8, '2018', '8月', 1393590.06, 330403.76);
INSERT INTO `jeecg_monthly_growth_analysis` VALUES (9, '2018', '9月', 735761.21, 1647474.92);
INSERT INTO `jeecg_monthly_growth_analysis` VALUES (10, '2018', '10月', 1670442.44, 3423368.33);
INSERT INTO `jeecg_monthly_growth_analysis` VALUES (11, '2018', '11月', 2993130.34, 3552024.00);
INSERT INTO `jeecg_monthly_growth_analysis` VALUES (12, '2018', '12月', 4206227.26, 3645614.92);
INSERT INTO `jeecg_monthly_growth_analysis` VALUES (13, '2019', '1月', 483834.66, 418046.77);
INSERT INTO `jeecg_monthly_growth_analysis` VALUES (14, '2019', '2月', 11666578.65, 731352.20);
INSERT INTO `jeecg_monthly_growth_analysis` VALUES (15, '2019', '3月', 27080982.08, 1878538.81);
INSERT INTO `jeecg_monthly_growth_analysis` VALUES (16, '2019', '4月', 0.00, 0.00);
INSERT INTO `jeecg_monthly_growth_analysis` VALUES (17, '2019', '5月', 0.00, 0.00);
INSERT INTO `jeecg_monthly_growth_analysis` VALUES (18, '2019', '6月', 0.00, 0.00);
INSERT INTO `jeecg_monthly_growth_analysis` VALUES (19, '2019', '7月', 0.00, 0.00);
INSERT INTO `jeecg_monthly_growth_analysis` VALUES (20, '2019', '8月', 0.00, 0.00);
INSERT INTO `jeecg_monthly_growth_analysis` VALUES (21, '2019', '9月', 0.00, 0.00);
INSERT INTO `jeecg_monthly_growth_analysis` VALUES (22, '2019', '10月', 0.00, 0.00);
INSERT INTO `jeecg_monthly_growth_analysis` VALUES (23, '2019', '11月', 0.00, 0.00);
INSERT INTO `jeecg_monthly_growth_analysis` VALUES (24, '2019', '12月', 0.00, 0.00);

-- ----------------------------
-- Table structure for jeecg_order_customer
-- ----------------------------
DROP TABLE IF EXISTS `jeecg_order_customer`;
CREATE TABLE `jeecg_order_customer`  (
  `id` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '主键',
  `name` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '客户名',
  `sex` varchar(4) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '性别',
  `idcard` varchar(18) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '身份证号码',
  `idcard_pic` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '身份证扫描件',
  `telphone` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '电话1',
  `order_id` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '外键',
  `create_by` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '创建人',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '修改人',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of jeecg_order_customer
-- ----------------------------
INSERT INTO `jeecg_order_customer` VALUES ('1256527640480821249', 'scott', '2', NULL, NULL, NULL, 'b190737bd04cca8360e6f87c9ef9ec4e', 'admin', '2020-05-02 18:15:09', NULL, NULL);
INSERT INTO `jeecg_order_customer` VALUES ('1256527640531152898', 'x秦风', '1', NULL, NULL, NULL, 'b190737bd04cca8360e6f87c9ef9ec4e', 'admin', '2020-05-02 18:15:09', NULL, NULL);
INSERT INTO `jeecg_order_customer` VALUES ('1256527874216800257', '小王1', '1', '', NULL, '', '9a57c850e4f68cf94ef7d8585dbaf7e6', 'admin', '2020-05-02 18:17:37', NULL, NULL);
INSERT INTO `jeecg_order_customer` VALUES ('15538561502720', '3333', '1', '', NULL, '', '0d4a2e67b538ee1bc881e5ed34f670f0', 'jeecg-boot', '2019-03-29 18:42:55', NULL, NULL);
INSERT INTO `jeecg_order_customer` VALUES ('15538561512681', '3332333', '2', '', NULL, '', '0d4a2e67b538ee1bc881e5ed34f670f0', 'jeecg-boot', '2019-03-29 18:42:55', 'admin', '2019-03-29 18:43:12');
INSERT INTO `jeecg_order_customer` VALUES ('15538561550142', '4442', '2', '', NULL, '', '0d4a2e67b538ee1bc881e5ed34f670f0', 'jeecg-boot', '2019-03-29 18:42:55', NULL, NULL);
INSERT INTO `jeecg_order_customer` VALUES ('15541168497342', '444', '', '', '', '', 'f71f7f8930b5b6b1703d9948d189982b', 'admin', '2019-04-01 19:08:45', NULL, NULL);
INSERT INTO `jeecg_order_customer` VALUES ('15541168499553', '5555', '', '', '', '', 'f71f7f8930b5b6b1703d9948d189982b', 'admin', '2019-04-01 19:08:45', NULL, NULL);
INSERT INTO `jeecg_order_customer` VALUES ('18dc5eb1068ccdfe90e358951ca1a3d6', 'dr2', '', '', '', '', '8ab1186410a65118c4d746eb085d3bed', 'admin', '2019-04-04 17:25:33', NULL, NULL);
INSERT INTO `jeecg_order_customer` VALUES ('195d280490fe88ca1475512ddcaf2af9', '12', NULL, NULL, NULL, NULL, '54e739bef5b67569c963c38da52581ec', 'admin', '2019-03-15 16:50:15', NULL, NULL);
INSERT INTO `jeecg_order_customer` VALUES ('217a2bf83709775d2cd85bf598392327', '2', NULL, NULL, NULL, NULL, '54e739bef5b67569c963c38da52581ec', 'admin', '2019-03-15 16:50:15', NULL, NULL);
INSERT INTO `jeecg_order_customer` VALUES ('22bc052ae53ed09913b946abba93fa89', '1', NULL, NULL, NULL, NULL, '54e739bef5b67569c963c38da52581ec', 'admin', '2019-03-15 16:50:15', NULL, NULL);
INSERT INTO `jeecg_order_customer` VALUES ('23bafeae88126c3bf3322a29a04f0d5e', 'x秦风', NULL, NULL, NULL, NULL, '163e2efcbc6d7d54eb3f8a137da8a75a', 'jeecg-boot', '2019-03-29 18:43:59', NULL, NULL);
INSERT INTO `jeecg_order_customer` VALUES ('25c4a552c6843f36fad6303bfa99a382', '1', NULL, NULL, NULL, NULL, '54e739bef5b67569c963c38da52581ec', 'admin', '2019-03-15 16:50:15', NULL, NULL);
INSERT INTO `jeecg_order_customer` VALUES ('2d32144e2bee63264f3f16215c258381', '33333', '2', NULL, NULL, NULL, 'd908bfee3377e946e59220c4a4eb414a', 'admin', '2019-04-01 16:27:03', NULL, NULL);
INSERT INTO `jeecg_order_customer` VALUES ('2d43170d6327f941bd1a017999495e25', '1', NULL, NULL, NULL, NULL, '54e739bef5b67569c963c38da52581ec', 'admin', '2019-03-15 16:50:15', NULL, NULL);
INSERT INTO `jeecg_order_customer` VALUES ('2e5f62a8b6e0a0ce19b52a6feae23d48', '3', NULL, NULL, NULL, NULL, '54e739bef5b67569c963c38da52581ec', 'admin', '2019-03-15 16:50:15', NULL, NULL);
INSERT INTO `jeecg_order_customer` VALUES ('34a1c5cf6cee360ed610ed0bed70e0f9', '导入秦风', NULL, NULL, NULL, NULL, 'a2cce75872cc8fcc47f78de9ffd378c2', 'jeecg-boot', '2019-03-29 18:43:59', NULL, NULL);
INSERT INTO `jeecg_order_customer` VALUES ('3c87400f8109b4cf43c5598f0d40e34d', '2', NULL, NULL, NULL, NULL, '54e739bef5b67569c963c38da52581ec', 'admin', '2019-03-15 16:50:15', NULL, NULL);
INSERT INTO `jeecg_order_customer` VALUES ('40964bcbbecb38e5ac15e6d08cf3cd43', '233', NULL, NULL, NULL, NULL, '54e739bef5b67569c963c38da52581ec', 'admin', '2019-03-15 16:50:15', NULL, NULL);
INSERT INTO `jeecg_order_customer` VALUES ('41e3dee0b0b6e6530eccb7fbb22fd7a3', '4555', '1', '370285198602058823', NULL, '18611788674', '54e739bef5b67569c963c38da52581ec', 'admin', '2019-03-15 16:50:15', NULL, NULL);
INSERT INTO `jeecg_order_customer` VALUES ('4808ae8344c7679a4a2f461db5dc3a70', '44', '1', '370285198602058823', NULL, '18611788674', '54e739bef5b67569c963c38da52581ec', 'admin', '2019-03-15 16:50:15', NULL, NULL);
INSERT INTO `jeecg_order_customer` VALUES ('4b6cef12f195fad94d57279b2241770d', 'dr12', '', '', '', '', '8ab1186410a65118c4d746eb085d3bed', 'admin', '2019-04-04 17:25:33', NULL, NULL);
INSERT INTO `jeecg_order_customer` VALUES ('524e695283f8e8c256cc24f39d6d8542', '小王', '2', '370285198604033222', NULL, '18611788674', 'eb13ab35d2946a2b0cfe3452bca1e73f', 'admin', '2019-02-25 16:29:41', NULL, NULL);
INSERT INTO `jeecg_order_customer` VALUES ('57c2a8367db34016114cbc9fa368dba0', '2', NULL, NULL, NULL, NULL, '54e739bef5b67569c963c38da52581ec', 'admin', '2019-03-15 16:50:15', NULL, NULL);
INSERT INTO `jeecg_order_customer` VALUES ('5df36a1608b8c7ac99ad9bc408fe54bf', '4', NULL, NULL, NULL, NULL, '54e739bef5b67569c963c38da52581ec', 'admin', '2019-03-15 16:50:15', NULL, NULL);
INSERT INTO `jeecg_order_customer` VALUES ('6c6fd2716c2dcd044ed03c2c95d261f8', '李四', '2', '370285198602058833', '', '18611788676', 'f71f7f8930b5b6b1703d9948d189982b', 'admin', '2019-04-01 19:08:45', NULL, NULL);
INSERT INTO `jeecg_order_customer` VALUES ('742d008214dee0afff2145555692973e', '秦风', '1', '370285198602058822', NULL, '18611788676', '54e739bef5b67569c963c38da52581ec', 'admin', '2019-03-15 16:50:15', NULL, NULL);
INSERT INTO `jeecg_order_customer` VALUES ('7469c3e5d371767ff90a739d297689b5', '导入秦风', '2', NULL, NULL, NULL, '3a867ebf2cebce9bae3f79676d8d86f3', 'jeecg-boot', '2019-03-29 18:43:59', 'admin', '2019-04-08 17:35:02');
INSERT INTO `jeecg_order_customer` VALUES ('7a96e2c7b24847d4a29940dbc0eda6e5', 'drscott', NULL, NULL, NULL, NULL, 'e73434dad84ebdce2d4e0c2a2f06d8ea', 'jeecg-boot', '2019-03-29 18:43:59', NULL, NULL);
INSERT INTO `jeecg_order_customer` VALUES ('7f5a40818e225ee18bda6da7932ac5f9', '2', NULL, NULL, NULL, NULL, '54e739bef5b67569c963c38da52581ec', 'admin', '2019-03-15 16:50:15', NULL, NULL);
INSERT INTO `jeecg_order_customer` VALUES ('8011575abfd7c8085e71ff66df1124b9', '1', NULL, NULL, NULL, NULL, '54e739bef5b67569c963c38da52581ec', 'admin', '2019-03-15 16:50:15', NULL, NULL);
INSERT INTO `jeecg_order_customer` VALUES ('8404f31d7196221a573c9bd6c8f15003', '小张', '1', '370285198602058211', NULL, '18611788676', 'eb13ab35d2946a2b0cfe3452bca1e73f', 'admin', '2019-02-25 16:29:41', NULL, NULL);
INSERT INTO `jeecg_order_customer` VALUES ('859020e10a2f721f201cdbff78cf7b9f', 'scott', NULL, NULL, NULL, NULL, '163e2efcbc6d7d54eb3f8a137da8a75a', 'jeecg-boot', '2019-03-29 18:43:59', NULL, NULL);
INSERT INTO `jeecg_order_customer` VALUES ('8cc3c4d26e3060975df3a2adb781eeb4', 'dr33', NULL, NULL, NULL, NULL, 'b2feb454e43c46b2038768899061e464', 'jeecg-boot', '2019-04-04 17:23:09', NULL, NULL);
INSERT INTO `jeecg_order_customer` VALUES ('8d1725c23a6a50685ff0dedfd437030d', '4', NULL, NULL, NULL, NULL, '54e739bef5b67569c963c38da52581ec', 'admin', '2019-03-15 16:50:15', NULL, NULL);
INSERT INTO `jeecg_order_customer` VALUES ('933cae3a79f60a93922d59aace5346ce', '小王', NULL, '370285198604033222', NULL, '18611788674', '6a719071a29927a14f19482f8693d69a', 'jeecg-boot', '2019-03-29 18:43:59', NULL, NULL);
INSERT INTO `jeecg_order_customer` VALUES ('9bdb5400b709ba4eaf3444de475880d7', 'dr22', NULL, NULL, NULL, NULL, '22c17790dcd04b296c4a2a089f71895f', 'jeecg-boot', '2019-04-04 17:23:09', NULL, NULL);
INSERT INTO `jeecg_order_customer` VALUES ('9f87677f70e5f864679314389443a3eb', '33', '2', '370285198602058823', NULL, '18611788674', '54e739bef5b67569c963c38da52581ec', 'admin', '2019-03-15 16:50:15', NULL, NULL);
INSERT INTO `jeecg_order_customer` VALUES ('a2c2b7101f75c02deb328ba777137897', '44', '2', '370285198602058823', NULL, '18611788674', '54e739bef5b67569c963c38da52581ec', 'admin', '2019-03-15 16:50:15', NULL, NULL);
INSERT INTO `jeecg_order_customer` VALUES ('ab4d002dc552c326147e318c87d3bed4', '小红1', '1', '370285198604033222', NULL, '18611755848', '9a57c850e4f68cf94ef7d8585dbaf7e6', 'admin', '2020-05-02 18:17:37', NULL, NULL);
INSERT INTO `jeecg_order_customer` VALUES ('ad116f722a438e5f23095a0b5fcc8e89', 'dr秦风', NULL, NULL, NULL, NULL, 'e73434dad84ebdce2d4e0c2a2f06d8ea', 'jeecg-boot', '2019-03-29 18:43:59', NULL, NULL);
INSERT INTO `jeecg_order_customer` VALUES ('b1ba147b75f5eaa48212586097fc3fd1', '2', NULL, NULL, NULL, NULL, '54e739bef5b67569c963c38da52581ec', 'admin', '2019-03-15 16:50:15', NULL, NULL);
INSERT INTO `jeecg_order_customer` VALUES ('b43bf432c251f0e6b206e403b8ec29bc', 'lisi', NULL, NULL, NULL, NULL, 'f8889aaef6d1bccffd98d2889c0aafb5', 'jeecg-boot', '2019-03-29 18:43:59', NULL, NULL);
INSERT INTO `jeecg_order_customer` VALUES ('bcdd300a7d44c45a66bdaac14903c801', '33', NULL, NULL, NULL, NULL, '54e739bef5b67569c963c38da52581ec', 'admin', '2019-03-15 16:50:15', NULL, NULL);
INSERT INTO `jeecg_order_customer` VALUES ('beb983293e47e2dc1a9b3d649aa3eb34', 'ddd3', NULL, NULL, NULL, NULL, 'd908bfee3377e946e59220c4a4eb414a', 'admin', '2019-04-01 16:27:03', NULL, NULL);
INSERT INTO `jeecg_order_customer` VALUES ('c219808196406f1b8c7f1062589de4b5', '44', '1', '370285198602058823', NULL, '18611788674', '54e739bef5b67569c963c38da52581ec', 'admin', '2019-03-15 16:50:15', NULL, NULL);
INSERT INTO `jeecg_order_customer` VALUES ('c8ed061d4b27c0c7a64e100f2b1c8ab5', '张经理', '2', '370285198602058823', NULL, '18611788674', '54e739bef5b67569c963c38da52581ec', 'admin', '2019-03-15 16:50:15', NULL, NULL);
INSERT INTO `jeecg_order_customer` VALUES ('cc5de4af7f06cd6d250965ebe92a0395', '1', NULL, NULL, NULL, NULL, '54e739bef5b67569c963c38da52581ec', 'admin', '2019-03-15 16:50:15', NULL, NULL);
INSERT INTO `jeecg_order_customer` VALUES ('cf8817bd703bf7c7c77a2118edc26cc7', '1', NULL, NULL, NULL, NULL, '54e739bef5b67569c963c38da52581ec', 'admin', '2019-03-15 16:50:15', NULL, NULL);
INSERT INTO `jeecg_order_customer` VALUES ('d72b26fae42e71270fce2097a88da58a', '导入scott', NULL, 'www', NULL, NULL, '3a867ebf2cebce9bae3f79676d8d86f3', 'jeecg-boot', '2019-03-29 18:43:59', 'admin', '2019-04-08 17:35:05');
INSERT INTO `jeecg_order_customer` VALUES ('dbdc60a6ac1a8c43f24afee384039b68', 'xiaowang', NULL, NULL, NULL, NULL, 'f8889aaef6d1bccffd98d2889c0aafb5', 'jeecg-boot', '2019-03-29 18:43:59', NULL, NULL);
INSERT INTO `jeecg_order_customer` VALUES ('dc5883b50466de94d900919ed96d97af', '33', '1', '370285198602058823', NULL, '18611788674', '54e739bef5b67569c963c38da52581ec', 'admin', '2019-03-15 16:50:15', NULL, NULL);
INSERT INTO `jeecg_order_customer` VALUES ('deeb73e553ad8dc0a0b3cfd5a338de8e', '3333', NULL, NULL, NULL, NULL, '54e739bef5b67569c963c38da52581ec', 'admin', '2019-03-15 16:50:15', NULL, NULL);
INSERT INTO `jeecg_order_customer` VALUES ('e2570278bf189ac05df3673231326f47', '1', NULL, NULL, NULL, NULL, '54e739bef5b67569c963c38da52581ec', 'admin', '2019-03-15 16:50:15', NULL, NULL);
INSERT INTO `jeecg_order_customer` VALUES ('e39cb23bb950b2bdedfc284686c6128a', '1', NULL, NULL, NULL, NULL, '54e739bef5b67569c963c38da52581ec', 'admin', '2019-03-15 16:50:15', NULL, NULL);
INSERT INTO `jeecg_order_customer` VALUES ('e46fe9111a9100844af582a18a2aa402', '1', NULL, NULL, NULL, NULL, '54e739bef5b67569c963c38da52581ec', 'admin', '2019-03-15 16:50:15', NULL, NULL);
INSERT INTO `jeecg_order_customer` VALUES ('ee7af0acb9beb9bf8d8b3819a8a7fdc3', '2', NULL, NULL, NULL, NULL, '54e739bef5b67569c963c38da52581ec', 'admin', '2019-03-15 16:50:15', NULL, NULL);
INSERT INTO `jeecg_order_customer` VALUES ('f5d2605e844192d9e548f9bd240ac908', '小张', NULL, '370285198602058211', NULL, '18611788676', '6a719071a29927a14f19482f8693d69a', 'jeecg-boot', '2019-03-29 18:43:59', NULL, NULL);
INSERT INTO `jeecg_order_customer` VALUES ('f6db6547382126613a3e46e7cd58a5f2', '导入scott', NULL, NULL, NULL, NULL, 'a2cce75872cc8fcc47f78de9ffd378c2', 'jeecg-boot', '2019-03-29 18:43:59', NULL, NULL);

-- ----------------------------
-- Table structure for jeecg_order_main
-- ----------------------------
DROP TABLE IF EXISTS `jeecg_order_main`;
CREATE TABLE `jeecg_order_main`  (
  `id` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '主键',
  `order_code` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '订单号',
  `ctype` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '订单类型',
  `order_date` datetime(0) NULL DEFAULT NULL COMMENT '订单日期',
  `order_money` double(10, 3) NULL DEFAULT NULL COMMENT '订单金额',
  `content` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '订单备注',
  `create_by` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '创建人',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '修改人',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of jeecg_order_main
-- ----------------------------
INSERT INTO `jeecg_order_main` VALUES ('163e2efcbc6d7d54eb3f8a137da8a75a', 'B100', NULL, NULL, 3000.000, NULL, 'jeecg-boot', '2019-03-29 18:43:59', NULL, NULL);
INSERT INTO `jeecg_order_main` VALUES ('3a867ebf2cebce9bae3f79676d8d86f3', '导入B100', '2222', NULL, 3000.000, NULL, 'jeecg-boot', '2019-03-29 18:43:59', 'admin', '2019-04-08 17:35:13');
INSERT INTO `jeecg_order_main` VALUES ('4cba137333127e8e31df7ad168cc3732', '青岛订单A0001', '2', '2019-04-03 10:56:07', NULL, NULL, 'admin', '2019-04-03 10:56:11', NULL, NULL);
INSERT INTO `jeecg_order_main` VALUES ('54e739bef5b67569c963c38da52581ec', 'NC911', '1', '2019-02-18 09:58:51', 40.000, NULL, 'admin', '2019-02-18 09:58:47', 'admin', '2019-02-18 09:58:59');
INSERT INTO `jeecg_order_main` VALUES ('6a719071a29927a14f19482f8693d69a', 'c100', NULL, NULL, 5000.000, NULL, 'jeecg-boot', '2019-03-29 18:43:59', NULL, NULL);
INSERT INTO `jeecg_order_main` VALUES ('8ab1186410a65118c4d746eb085d3bed', '导入400', '1', '2019-02-18 09:58:51', 40.000, NULL, 'admin', '2019-02-18 09:58:47', 'admin', '2019-02-18 09:58:59');
INSERT INTO `jeecg_order_main` VALUES ('9a57c850e4f68cf94ef7d8585dbaf7e6', 'halou001', '1', '2019-04-04 17:30:32', 500.000, NULL, 'admin', '2019-04-04 17:30:41', 'admin', '2020-05-02 18:17:36');
INSERT INTO `jeecg_order_main` VALUES ('a2cce75872cc8fcc47f78de9ffd378c2', '导入B100', NULL, NULL, 3000.000, NULL, 'jeecg-boot', '2019-03-29 18:43:59', NULL, NULL);
INSERT INTO `jeecg_order_main` VALUES ('b190737bd04cca8360e6f87c9ef9ec4e', 'B0018888', '1', NULL, NULL, NULL, 'admin', '2019-02-15 18:39:29', 'admin', '2020-05-02 18:15:09');
INSERT INTO `jeecg_order_main` VALUES ('d908bfee3377e946e59220c4a4eb414a', 'SSSS001', NULL, NULL, 599.000, NULL, 'admin', '2019-04-01 15:43:03', 'admin', '2019-04-01 16:26:52');
INSERT INTO `jeecg_order_main` VALUES ('e73434dad84ebdce2d4e0c2a2f06d8ea', '导入200', NULL, NULL, 3000.000, NULL, 'jeecg-boot', '2019-03-29 18:43:59', NULL, NULL);
INSERT INTO `jeecg_order_main` VALUES ('eb13ab35d2946a2b0cfe3452bca1e73f', 'BJ9980', '1', NULL, 90.000, NULL, 'admin', '2019-02-16 17:36:42', 'admin', '2019-02-16 17:46:16');
INSERT INTO `jeecg_order_main` VALUES ('f71f7f8930b5b6b1703d9948d189982b', 'BY911', NULL, '2019-04-06 19:08:39', NULL, NULL, 'admin', '2019-04-01 16:36:02', 'admin', '2019-04-01 16:36:08');
INSERT INTO `jeecg_order_main` VALUES ('f8889aaef6d1bccffd98d2889c0aafb5', 'A100', NULL, '2018-10-10 00:00:00', 6000.000, NULL, 'jeecg-boot', '2019-03-29 18:43:59', NULL, NULL);

-- ----------------------------
-- Table structure for jeecg_order_ticket
-- ----------------------------
DROP TABLE IF EXISTS `jeecg_order_ticket`;
CREATE TABLE `jeecg_order_ticket`  (
  `id` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '主键',
  `ticket_code` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '航班号',
  `tickect_date` datetime(0) NULL DEFAULT NULL COMMENT '航班时间',
  `order_id` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '外键',
  `create_by` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '创建人',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '修改人',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of jeecg_order_ticket
-- ----------------------------
INSERT INTO `jeecg_order_ticket` VALUES ('0f0e3a40a215958f807eea08a6e1ac0a', '88', NULL, '54e739bef5b67569c963c38da52581ec', 'admin', '2019-03-15 16:50:15', NULL, NULL);
INSERT INTO `jeecg_order_ticket` VALUES ('0fa3bd0bbcf53650c0bb3c0cac6d8cb7', 'ffff', '2019-02-21 00:00:00', 'eb13ab35d2946a2b0cfe3452bca1e73f', 'admin', '2019-02-25 16:29:41', NULL, NULL);
INSERT INTO `jeecg_order_ticket` VALUES ('1256527640543735810', '222', '2019-02-23 00:00:00', 'b190737bd04cca8360e6f87c9ef9ec4e', 'admin', '2020-05-02 18:15:09', NULL, NULL);
INSERT INTO `jeecg_order_ticket` VALUES ('1256527640560513025', '111', '2019-02-01 00:00:00', 'b190737bd04cca8360e6f87c9ef9ec4e', 'admin', '2020-05-02 18:15:09', NULL, NULL);
INSERT INTO `jeecg_order_ticket` VALUES ('14221afb4f5f749c1deef26ac56fdac3', '33', '2019-03-09 00:00:00', '54e739bef5b67569c963c38da52581ec', 'admin', '2019-03-15 16:50:15', NULL, NULL);
INSERT INTO `jeecg_order_ticket` VALUES ('15538561502730', '222', NULL, '0d4a2e67b538ee1bc881e5ed34f670f0', 'jeecg-boot', '2019-03-29 18:42:55', NULL, NULL);
INSERT INTO `jeecg_order_ticket` VALUES ('15538561526461', '2244', '2019-03-29 00:00:00', '0d4a2e67b538ee1bc881e5ed34f670f0', 'jeecg-boot', '2019-03-29 18:42:55', 'admin', '2019-03-29 18:43:26');
INSERT INTO `jeecg_order_ticket` VALUES ('15541168478913', 'hhhhh', NULL, 'f71f7f8930b5b6b1703d9948d189982b', 'admin', '2019-04-01 19:08:45', NULL, NULL);
INSERT INTO `jeecg_order_ticket` VALUES ('18905bc89ee3851805aab38ed3b505ec', '44', NULL, '54e739bef5b67569c963c38da52581ec', 'admin', '2019-03-15 16:50:15', NULL, NULL);
INSERT INTO `jeecg_order_ticket` VALUES ('1f809cbd26f4e574697e1c10de575d72', 'A100', NULL, 'e73434dad84ebdce2d4e0c2a2f06d8ea', 'jeecg-boot', '2019-03-29 18:43:59', NULL, NULL);
INSERT INTO `jeecg_order_ticket` VALUES ('21051adb51529bdaa8798b5a3dd7f7f7', 'C10029', '2019-02-20 00:00:00', '54e739bef5b67569c963c38da52581ec', 'admin', '2019-03-15 16:50:15', NULL, NULL);
INSERT INTO `jeecg_order_ticket` VALUES ('269576e766b917f8b6509a2bb0c4d4bd', 'A100', NULL, '163e2efcbc6d7d54eb3f8a137da8a75a', 'jeecg-boot', '2019-03-29 18:43:59', NULL, NULL);
INSERT INTO `jeecg_order_ticket` VALUES ('2d473ffc79e5b38a17919e15f8b7078e', '66', '2019-03-29 00:00:00', '54e739bef5b67569c963c38da52581ec', 'admin', '2019-03-15 16:50:15', NULL, NULL);
INSERT INTO `jeecg_order_ticket` VALUES ('3655b66fca5fef9c6aac6d70182ffda2', 'AA123', '2019-04-01 00:00:00', 'd908bfee3377e946e59220c4a4eb414a', 'admin', '2019-04-01 16:27:03', NULL, NULL);
INSERT INTO `jeecg_order_ticket` VALUES ('365d5919155473ade45840fd626c51a9', 'dddd', '2019-04-04 17:25:29', '8ab1186410a65118c4d746eb085d3bed', 'admin', '2019-04-04 17:25:33', NULL, NULL);
INSERT INTO `jeecg_order_ticket` VALUES ('4889a782e78706ab4306a925cfb163a5', 'C34', '2019-04-01 00:00:00', 'd908bfee3377e946e59220c4a4eb414a', 'admin', '2019-04-01 16:35:00', 'admin', '2019-04-01 16:35:07');
INSERT INTO `jeecg_order_ticket` VALUES ('48d385796382cf87fa4bdf13b42d9a28', '导入A100', NULL, '3a867ebf2cebce9bae3f79676d8d86f3', 'jeecg-boot', '2019-03-29 18:43:59', NULL, NULL);
INSERT INTO `jeecg_order_ticket` VALUES ('541faed56efbeb4be9df581bd8264d3a', '88', NULL, '54e739bef5b67569c963c38da52581ec', 'admin', '2019-03-15 16:50:15', NULL, NULL);
INSERT INTO `jeecg_order_ticket` VALUES ('57a27a7dfd6a48e7d981f300c181b355', '6', '2019-03-30 00:00:00', '54e739bef5b67569c963c38da52581ec', 'admin', '2019-03-15 16:50:15', NULL, NULL);
INSERT INTO `jeecg_order_ticket` VALUES ('5ce4dc439c874266e42e6c0ff8dc8b5c', '导入A100', NULL, 'a2cce75872cc8fcc47f78de9ffd378c2', 'jeecg-boot', '2019-03-29 18:43:59', NULL, NULL);
INSERT INTO `jeecg_order_ticket` VALUES ('645a06152998a576c051474157625c41', '88', '2019-04-04 17:25:31', '8ab1186410a65118c4d746eb085d3bed', 'admin', '2019-04-04 17:25:33', NULL, NULL);
INSERT INTO `jeecg_order_ticket` VALUES ('6e3562f2571ea9e96b2d24497b5f5eec', '55', '2019-03-23 00:00:00', '54e739bef5b67569c963c38da52581ec', 'admin', '2019-03-15 16:50:15', NULL, NULL);
INSERT INTO `jeecg_order_ticket` VALUES ('8fd2b389151568738b1cc4d8e27a6110', '导入A100', NULL, 'a2cce75872cc8fcc47f78de9ffd378c2', 'jeecg-boot', '2019-03-29 18:43:59', NULL, NULL);
INSERT INTO `jeecg_order_ticket` VALUES ('93f1a84053e546f59137432ff5564cac', '55', NULL, '54e739bef5b67569c963c38da52581ec', 'admin', '2019-03-15 16:50:15', NULL, NULL);
INSERT INTO `jeecg_order_ticket` VALUES ('969ddc5d2e198d50903686917f996470', 'A10029', '2019-04-01 00:00:00', 'f71f7f8930b5b6b1703d9948d189982b', 'admin', '2019-04-01 19:08:45', NULL, NULL);
INSERT INTO `jeecg_order_ticket` VALUES ('96e7303a8d22a5c384e08d7bcf7ac2bf', 'A100', NULL, 'e73434dad84ebdce2d4e0c2a2f06d8ea', 'jeecg-boot', '2019-03-29 18:43:59', NULL, NULL);
INSERT INTO `jeecg_order_ticket` VALUES ('9e8a3336f6c63f558f2b68ce2e1e666e', '深圳1001', '2020-05-02 00:00:00', '9a57c850e4f68cf94ef7d8585dbaf7e6', 'admin', '2020-05-02 18:17:37', NULL, NULL);
INSERT INTO `jeecg_order_ticket` VALUES ('a28db02c810c65660015095cb81ed434', 'A100', NULL, 'f8889aaef6d1bccffd98d2889c0aafb5', 'jeecg-boot', '2019-03-29 18:43:59', NULL, NULL);
INSERT INTO `jeecg_order_ticket` VALUES ('b217bb0e4ec6a45b6cbf6db880060c0f', 'A100', NULL, '6a719071a29927a14f19482f8693d69a', 'jeecg-boot', '2019-03-29 18:43:59', NULL, NULL);
INSERT INTO `jeecg_order_ticket` VALUES ('ba708df70bb2652ed1051a394cfa0bb3', '333', NULL, '54e739bef5b67569c963c38da52581ec', 'admin', '2019-03-15 16:50:15', NULL, NULL);
INSERT INTO `jeecg_order_ticket` VALUES ('beabbfcb195d39bedeeafe8318794562', 'A1345', '2019-04-01 00:00:00', 'd908bfee3377e946e59220c4a4eb414a', 'admin', '2019-04-01 16:27:04', NULL, NULL);
INSERT INTO `jeecg_order_ticket` VALUES ('bf450223cb505f89078a311ef7b6ed16', '777', '2019-03-30 00:00:00', '54e739bef5b67569c963c38da52581ec', 'admin', '2019-03-15 16:50:15', NULL, NULL);
INSERT INTO `jeecg_order_ticket` VALUES ('c06165b6603e3e1335db187b3c841eef', '北京2001', '2020-05-23 00:00:00', '9a57c850e4f68cf94ef7d8585dbaf7e6', 'admin', '2020-05-02 18:17:37', NULL, NULL);
INSERT INTO `jeecg_order_ticket` VALUES ('c113136abc26ace3a6da4e41d7dc1c7e', '44', '2019-03-15 00:00:00', '54e739bef5b67569c963c38da52581ec', 'admin', '2019-03-15 16:50:15', NULL, NULL);
INSERT INTO `jeecg_order_ticket` VALUES ('c1abdc2e30aeb25de13ad6ee3488ac24', '77', '2019-03-22 00:00:00', '54e739bef5b67569c963c38da52581ec', 'admin', '2019-03-15 16:50:15', NULL, NULL);
INSERT INTO `jeecg_order_ticket` VALUES ('c23751a7deb44f553ce50a94948c042a', '33', '2019-03-09 00:00:00', '8ab1186410a65118c4d746eb085d3bed', 'admin', '2019-04-04 17:25:33', NULL, NULL);
INSERT INTO `jeecg_order_ticket` VALUES ('c64547666b634b3d6a0feedcf05f25ce', 'C10019', '2019-04-01 00:00:00', 'f71f7f8930b5b6b1703d9948d189982b', 'admin', '2019-04-01 19:08:45', NULL, NULL);
INSERT INTO `jeecg_order_ticket` VALUES ('c8b8d3217f37da78dddf711a1f7da485', 'A100', NULL, '163e2efcbc6d7d54eb3f8a137da8a75a', 'jeecg-boot', '2019-03-29 18:43:59', NULL, NULL);
INSERT INTO `jeecg_order_ticket` VALUES ('cab691c1c1ff7a6dfd7248421917fd3c', 'A100', NULL, 'f8889aaef6d1bccffd98d2889c0aafb5', 'jeecg-boot', '2019-03-29 18:43:59', NULL, NULL);
INSERT INTO `jeecg_order_ticket` VALUES ('cca10a9a850b456d9b72be87da7b0883', '77', NULL, '54e739bef5b67569c963c38da52581ec', 'admin', '2019-03-15 16:50:15', NULL, NULL);
INSERT INTO `jeecg_order_ticket` VALUES ('d2fbba11f4814d9b1d3cb1a3f342234a', 'C10019', '2019-02-18 00:00:00', '54e739bef5b67569c963c38da52581ec', 'admin', '2019-03-15 16:50:15', NULL, NULL);
INSERT INTO `jeecg_order_ticket` VALUES ('dbdb07a16826808e4276e84b2aa4731a', '导入A100', NULL, '3a867ebf2cebce9bae3f79676d8d86f3', 'jeecg-boot', '2019-03-29 18:43:59', NULL, NULL);
INSERT INTO `jeecg_order_ticket` VALUES ('e7075639c37513afc0bbc4bf7b5d98b9', '88', NULL, '54e739bef5b67569c963c38da52581ec', 'admin', '2019-03-15 16:50:15', NULL, NULL);
INSERT INTO `jeecg_order_ticket` VALUES ('fa759dc104d0371f8aa28665b323dab6', '888', NULL, '54e739bef5b67569c963c38da52581ec', 'admin', '2019-03-15 16:50:15', NULL, NULL);
INSERT INTO `jeecg_order_ticket` VALUES ('ff197da84a9a3af53878eddc91afbb2e', '33', NULL, '54e739bef5b67569c963c38da52581ec', 'admin', '2019-03-15 16:50:15', NULL, NULL);

-- ----------------------------
-- Table structure for jeecg_project_nature_income
-- ----------------------------
DROP TABLE IF EXISTS `jeecg_project_nature_income`;
CREATE TABLE `jeecg_project_nature_income`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nature` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '项目性质',
  `insurance_fee` decimal(18, 2) NULL DEFAULT 0.00 COMMENT '保险经纪佣金费',
  `risk_consulting_fee` decimal(18, 2) NULL DEFAULT 0.00 COMMENT '风险咨询费',
  `evaluation_fee` decimal(18, 2) NULL DEFAULT 0.00 COMMENT '承保公估评估费',
  `insurance_evaluation_fee` decimal(18, 2) NULL DEFAULT 0.00 COMMENT '保险公估费',
  `bidding_consulting_fee` decimal(18, 2) NULL DEFAULT 0.00 COMMENT '投标咨询费',
  `interol_consulting_fee` decimal(18, 2) NULL DEFAULT 0.00 COMMENT '内控咨询费',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 9 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of jeecg_project_nature_income
-- ----------------------------
INSERT INTO `jeecg_project_nature_income` VALUES (1, '市场化-电商业务', 4865.41, 0.00, 0.00, 0.00, 0.00, 0.00);
INSERT INTO `jeecg_project_nature_income` VALUES (2, '统筹型', 35767081.88, 0.00, 0.00, 0.00, 0.00, 0.00);
INSERT INTO `jeecg_project_nature_income` VALUES (3, '市场化-非股东', 1487045.35, 0.00, 0.00, 0.00, 0.00, 0.00);
INSERT INTO `jeecg_project_nature_income` VALUES (4, '市场化-参控股', 382690.56, 0.00, 0.00, 0.00, 0.00, 0.00);
INSERT INTO `jeecg_project_nature_income` VALUES (5, '市场化-员工福利', 256684.91, 0.00, 0.00, 0.00, 0.00, 0.00);
INSERT INTO `jeecg_project_nature_income` VALUES (6, '市场化-再保险', 563451.03, 0.00, 0.00, 0.00, 0.00, 0.00);
INSERT INTO `jeecg_project_nature_income` VALUES (7, '市场化-海外业务', 760576.25, 770458.75, 0.00, 0.00, 0.00, 0.00);
INSERT INTO `jeecg_project_nature_income` VALUES (8, '市场化-风险咨询', 910183.93, 0.00, 0.00, 0.00, 0.00, 226415.09);

-- ----------------------------
-- Table structure for joa_demo
-- ----------------------------
DROP TABLE IF EXISTS `joa_demo`;
CREATE TABLE `joa_demo`  (
  `id` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'ID',
  `name` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '请假人',
  `days` int(11) NULL DEFAULT NULL COMMENT '请假天数',
  `begin_date` datetime(0) NULL DEFAULT NULL COMMENT '开始时间',
  `end_date` datetime(0) NULL DEFAULT NULL COMMENT '请假结束时间',
  `reason` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '请假原因',
  `bpm_status` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '1' COMMENT '流程状态',
  `create_by` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '创建人id',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '修改时间',
  `update_by` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '修改人id'
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '流程测试' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for onl_cgform_button
-- ----------------------------
DROP TABLE IF EXISTS `onl_cgform_button`;
CREATE TABLE `onl_cgform_button`  (
  `ID` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '主键ID',
  `BUTTON_CODE` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '按钮编码',
  `BUTTON_ICON` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '按钮图标',
  `BUTTON_NAME` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '按钮名称',
  `BUTTON_STATUS` varchar(2) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '按钮状态',
  `BUTTON_STYLE` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '按钮样式',
  `EXP` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '表达式',
  `CGFORM_HEAD_ID` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '表单ID',
  `OPT_TYPE` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '按钮类型',
  `ORDER_NUM` int(11) NULL DEFAULT NULL COMMENT '排序',
  `OPT_POSITION` varchar(3) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '按钮位置1侧面 2底部',
  PRIMARY KEY (`ID`) USING BTREE,
  INDEX `index_formid`(`CGFORM_HEAD_ID`) USING BTREE,
  INDEX `index_button_code`(`BUTTON_CODE`) USING BTREE,
  INDEX `index_button_status`(`BUTTON_STATUS`) USING BTREE,
  INDEX `index_button_order`(`ORDER_NUM`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = 'Online表单自定义按钮' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of onl_cgform_button
-- ----------------------------
INSERT INTO `onl_cgform_button` VALUES ('108a564643763de3f4c81bc2deb463df', 'bt1', NULL, '激活', '1', 'button', NULL, '05a3a30dada7411c9109306aa4117068', 'js', NULL, '2');
INSERT INTO `onl_cgform_button` VALUES ('7c140322fb6b1da7a5daed8b6edc0fb7', 'tjbpm', NULL, '提交流程', '1', 'link', NULL, '05a3a30dada7411c9109306aa4117068', 'js', NULL, '2');
INSERT INTO `onl_cgform_button` VALUES ('a45bc1c6fba96be6b0c91ffcdd6b54aa', 'genereate_person_config', 'icon-edit', '生成配置', '1', 'link', NULL, 'e2faf977fdaf4b25a524f58c2441a51c', 'js', NULL, '2');
INSERT INTO `onl_cgform_button` VALUES ('cc1d12de57a1a41d3986ed6d13e3ac11', '链接按钮测试', 'icon-edit', '自定义link', '1', 'link', NULL, 'd35109c3632c4952a19ecc094943dd71', 'js', NULL, '2');
INSERT INTO `onl_cgform_button` VALUES ('e2a339b9fdb4091bee98408c233ab36d', 'zuofei', NULL, '作废', '1', 'form', NULL, '05a3a30dada7411c9109306aa4117068', 'js', NULL, '2');
INSERT INTO `onl_cgform_button` VALUES ('ebcc48ef0bde4433a6faf940a5e170c1', 'button按钮测试', 'icon-edit', '自定义button', '1', 'button', NULL, 'd35109c3632c4952a19ecc094943dd71', 'js', NULL, '2');

-- ----------------------------
-- Table structure for onl_cgform_enhance_java
-- ----------------------------
DROP TABLE IF EXISTS `onl_cgform_enhance_java`;
CREATE TABLE `onl_cgform_enhance_java`  (
  `ID` varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `BUTTON_CODE` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '按钮编码',
  `CG_JAVA_TYPE` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '类型',
  `CG_JAVA_VALUE` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '数值',
  `CGFORM_HEAD_ID` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '表单ID',
  `ACTIVE_STATUS` varchar(2) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '1' COMMENT '生效状态',
  `EVENT` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT 'end' COMMENT '事件状态(end:结束，start:开始)',
  PRIMARY KEY (`ID`) USING BTREE,
  INDEX `index_fmid`(`CGFORM_HEAD_ID`) USING BTREE,
  INDEX `index_buttoncode`(`BUTTON_CODE`) USING BTREE,
  INDEX `index_status`(`ACTIVE_STATUS`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for onl_cgform_enhance_js
-- ----------------------------
DROP TABLE IF EXISTS `onl_cgform_enhance_js`;
CREATE TABLE `onl_cgform_enhance_js`  (
  `ID` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '主键ID',
  `CG_JS` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT 'JS增强内容',
  `CG_JS_TYPE` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '类型',
  `CONTENT` varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注',
  `CGFORM_HEAD_ID` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '表单ID',
  PRIMARY KEY (`ID`) USING BTREE,
  INDEX `index_fmid`(`CGFORM_HEAD_ID`) USING BTREE,
  INDEX `index_jstype`(`CG_JS_TYPE`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of onl_cgform_enhance_js
-- ----------------------------
INSERT INTO `onl_cgform_enhance_js` VALUES ('274b5d741a0262d3411958f0c465c5f0', 'genereate_person_config(row){\nconsole.log(\'选择\',row)\nalert(row.name + \'，个人积分配置生成成功！\');\n}', 'list', NULL, 'e2faf977fdaf4b25a524f58c2441a51c');
INSERT INTO `onl_cgform_enhance_js` VALUES ('2cbaf25f1edb620bea2d8de07f8233a1', 'air_china_post_materiel_item_onlChange(){\n    return {\n        wl_name(){\n           \n            let id = event.row.id\n            let cnum = event.row.num\n            let value = event.value\n            let targrt = event.target\n            let columnKey = event.column.key\n           let nval = 200*cnum\n           console.log(\'row\',event.row);\n           console.log(\'cnum\',cnum);\n           let otherValues = {\'jifen\': nval}\n              \n                that.triggleChangeValues(targrt,id,otherValues)\n\n        }\n    }\n}', 'form', NULL, 'e67d26b610dd414c884c4dbb24e71ce3');
INSERT INTO `onl_cgform_enhance_js` VALUES ('32e7b7373abe0fb9c4dd608b4517f814', '', 'form', NULL, '53a3e82b54b946c2b904f605875a275c');
INSERT INTO `onl_cgform_enhance_js` VALUES ('35d4ef464e5e8c87c9aa82ea89215fc1', '', 'list', NULL, 'e67d26b610dd414c884c4dbb24e71ce3');
INSERT INTO `onl_cgform_enhance_js` VALUES ('44cad4eec436328ed3cc134855f8d1d5', ' onlChange(){\n   return {\n    name(that, event) {\n      that.executeMainFillRule()\n    }\n  }\n }', 'form', NULL, '4adec929a6594108bef5b35ee9966e9f');
INSERT INTO `onl_cgform_enhance_js` VALUES ('4569bc0e6126d2b8a0e0c69b9a47e8db', '', 'list', NULL, '56efb74326e74064b60933f6f8af30ea');
INSERT INTO `onl_cgform_enhance_js` VALUES ('6dd82d8eac166627302230a809233481', 'ces_order_goods_onlChange(){\n    return {\n        num(){\n           \n            let id = event.row.id\n            let num = event.row.num\n            let price = event.row.price\n\n            let targrt = event.target\n            \n            let nval = price*num\n            console.log(\'row\',event.row);\n            console.log(\'num\',num);\n            console.log(\'that\',that);\n            let otherValues = {\'zong_price\': nval}\n              \n            that.triggleChangeValues(otherValues,id,targrt)\n   \n\n        }\n    }\n}', 'form', NULL, '56efb74326e74064b60933f6f8af30ea');
INSERT INTO `onl_cgform_enhance_js` VALUES ('85e7acd772c8ec322b97a1fd548007e0', '', 'form', NULL, '09fd28e4b7184c1a9668496a5c496450');
INSERT INTO `onl_cgform_enhance_js` VALUES ('8b76f282ddc81ce99a129e90fdd977ce', '', 'form', NULL, '86bf17839a904636b7ed96201b2fa6ea');
INSERT INTO `onl_cgform_enhance_js` VALUES ('90394fbc3d48978cc0937bc56f2d5370', '', 'list', NULL, 'deea5a8ec619460c9245ba85dbc59e80');
INSERT INTO `onl_cgform_enhance_js` VALUES ('ae9cf52fbe13cc718de2de6e1b3d6792', '', 'list', NULL, '18f064d1ef424c93ba7a16148851664f');
INSERT INTO `onl_cgform_enhance_js` VALUES ('beec235f0b2d633ff3a6c395affdf59d', '', 'list', NULL, '4adec929a6594108bef5b35ee9966e9f');
INSERT INTO `onl_cgform_enhance_js` VALUES ('c5ac9a2b2fd92ef68274f630b8aec78a', 'tjbpm(row){\n  alert(\'提交流程\')\n  console.log(\'row\',row)\n}\n\nbt1(){\n   console.log(\'that.table.selectionRows\',that.table.selectionRows)\n   console.log(\'that.table.selectedRowKeys\',that.table.selectedRowKeys)\n   console.log(\'that.table.dataSource\',that.table.dataSource)\n   alert(\'激活全部数据\')\n}', 'list', NULL, '05a3a30dada7411c9109306aa4117068');
INSERT INTO `onl_cgform_enhance_js` VALUES ('d7ddb7aa407f6deed75aac11f0a25f0e', '222', 'list', NULL, '09fd28e4b7184c1a9668496a5c496450');
INSERT INTO `onl_cgform_enhance_js` VALUES ('f6f8f230566d09d4b66338955ffb5691', '', 'form', NULL, '18f064d1ef424c93ba7a16148851664f');
INSERT INTO `onl_cgform_enhance_js` VALUES ('fd711738f58d5481ca0ce9bc3a415223', '', 'list', NULL, '86bf17839a904636b7ed96201b2fa6ea');

-- ----------------------------
-- Table structure for onl_cgform_enhance_sql
-- ----------------------------
DROP TABLE IF EXISTS `onl_cgform_enhance_sql`;
CREATE TABLE `onl_cgform_enhance_sql`  (
  `ID` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '主键ID',
  `BUTTON_CODE` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '按钮编码',
  `CGB_SQL` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT 'SQL内容',
  `CGB_SQL_NAME` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'Sql名称',
  `CONTENT` varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注',
  `CGFORM_HEAD_ID` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '表单ID',
  PRIMARY KEY (`ID`) USING BTREE,
  INDEX `index_formid`(`CGFORM_HEAD_ID`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of onl_cgform_enhance_sql
-- ----------------------------
INSERT INTO `onl_cgform_enhance_sql` VALUES ('0ebf418bd02f486342123eaf84cd39ad', 'add', '', NULL, '', '18f064d1ef424c93ba7a16148851664f');
INSERT INTO `onl_cgform_enhance_sql` VALUES ('5ab418a13fd0bbf30ee9dd04203f3c28', 'add', '', NULL, '', '4adec929a6594108bef5b35ee9966e9f');
INSERT INTO `onl_cgform_enhance_sql` VALUES ('8750b93ba5332460c76c492359d7a06b', 'edit', '', NULL, '', '18f064d1ef424c93ba7a16148851664f');
INSERT INTO `onl_cgform_enhance_sql` VALUES ('edfab059050b19328ac81e6833b5ebc2', 'delete', '', NULL, '', '18f064d1ef424c93ba7a16148851664f');

-- ----------------------------
-- Table structure for onl_cgform_field
-- ----------------------------
DROP TABLE IF EXISTS `onl_cgform_field`;
CREATE TABLE `onl_cgform_field`  (
  `id` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '主键ID',
  `cgform_head_id` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '表ID',
  `db_field_name` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '字段名字',
  `db_field_txt` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '字段备注',
  `db_field_name_old` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '原字段名',
  `db_is_key` tinyint(1) NULL DEFAULT NULL COMMENT '是否主键 0否 1是',
  `db_is_null` tinyint(1) NULL DEFAULT NULL COMMENT '是否允许为空0否 1是',
  `db_type` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '数据库字段类型',
  `db_length` int(11) NOT NULL COMMENT '数据库字段长度',
  `db_point_length` int(11) NULL DEFAULT NULL COMMENT '小数点',
  `db_default_val` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '表字段默认值',
  `dict_field` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '字典code',
  `dict_table` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '字典表',
  `dict_text` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '字典Text',
  `field_show_type` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '表单控件类型',
  `field_href` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '跳转URL',
  `field_length` int(11) NULL DEFAULT NULL COMMENT '表单控件长度',
  `field_valid_type` varchar(300) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '表单字段校验规则',
  `field_must_input` varchar(2) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '字段是否必填',
  `field_extend_json` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '扩展参数JSON',
  `field_default_value` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '控件默认值，不同的表达式展示不同的结果。\r\n1. 纯字符串直接赋给默认值；\r\n2. #{普通变量}；\r\n3. {{ 动态JS表达式 }}；\r\n4. ${填值规则编码}；\r\n填值规则表达式只允许存在一个，且不能和其他规则混用。',
  `is_query` tinyint(1) NULL DEFAULT NULL COMMENT '是否查询条件0否 1是',
  `is_show_form` tinyint(1) NULL DEFAULT NULL COMMENT '表单是否显示0否 1是',
  `is_show_list` tinyint(1) NULL DEFAULT NULL COMMENT '列表是否显示0否 1是',
  `is_read_only` tinyint(1) NULL DEFAULT 0 COMMENT '是否是只读（1是 0否）',
  `query_mode` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '查询模式',
  `main_table` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '外键主表名',
  `main_field` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '外键主键字段',
  `order_num` int(11) NULL DEFAULT NULL COMMENT '排序',
  `update_by` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '修改人',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '修改时间',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `create_by` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '创建人',
  `converter` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '自定义值转换器',
  `query_def_val` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '查询默认值',
  `query_dict_text` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '查询配置字典text',
  `query_dict_field` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '查询配置字典code',
  `query_dict_table` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '查询配置字典table',
  `query_show_type` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '查询显示控件',
  `query_config_flag` varchar(3) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '是否启用查询配置1是0否',
  `query_valid_type` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '查询字段校验类型',
  `query_must_input` varchar(3) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '查询字段是否必填1是0否',
  `sort_flag` varchar(3) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '是否支持排序1是0否',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `inex_table_id`(`cgform_head_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of onl_cgform_field
-- ----------------------------
INSERT INTO `onl_cgform_field` VALUES ('0021c969dc23a9150d6f70a13b52e73e', '402860816aa5921f016aa5921f480000', 'begin_date', '开始时间', 'begin_date', 0, 1, 'Date', 0, 0, '', '', '', '', 'date', '', 120, NULL, '0', '', '', 0, 1, 1, 0, 'group', '', '', 4, 'admin', '2019-05-11 15:31:54', '2019-05-11 14:26:19', 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `onl_cgform_field` VALUES ('020c1622c3df0aef30185f57874f6959', '79091e8277c744158530321513119c68', 'bpm_status', '流程状态', NULL, 0, 1, 'String', 32, 0, '1', 'bpm_status', '', '', 'text', '', 120, NULL, '0', '', '', 0, 0, 1, 0, 'single', '', '', 8, 'admin', '2019-05-11 15:29:47', '2019-05-11 15:29:26', 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `onl_cgform_field` VALUES ('02b20e692456403e2fed1a89a06833b4', '402860816bff91c0016bff91d2810005', 'phone', '联系方式', 'phone', 0, 1, 'string', 20, 0, '', '', '', '', 'text', '', 120, NULL, '0', '', '', 0, 1, 1, 0, 'group', '', '', 8, 'admin', '2019-07-19 18:05:55', '2019-07-17 18:54:35', 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `onl_cgform_field` VALUES ('03668009f0ad92b20abb1a377197ee47', 'deea5a8ec619460c9245ba85dbc59e80', 'order_fk_id', '订单外键ID', NULL, 0, 0, 'String', 32, 0, '', '', '', '', 'text', '', 120, NULL, '0', '', '', 0, 0, 0, 0, 'single', 'test_order_main', 'id', 10, 'admin', '2020-05-03 01:01:18', '2019-04-20 11:42:53', 'admin', '', '', '', '', '', NULL, '0', NULL, NULL, '0');
INSERT INTO `onl_cgform_field` VALUES ('03709092184fdf4a66b0cdb4dd10a159', '402860816bff91c0016bffa220a9000b', 'bpm_status', '流程状态', NULL, 0, 1, 'String', 32, 0, '1', '', '', '', 'text', '', 120, NULL, '0', '', '', 0, 1, 1, 0, 'single', '', '', 46, 'admin', '2019-07-22 16:15:32', '2019-07-19 15:34:44', 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `onl_cgform_field` VALUES ('03988419d751a330c2b0f0519a531880', '997ee931515a4620bc30a9c1246429a9', 'create_time', '创建日期', NULL, 0, 1, 'Date', 20, 0, '', '', '', '', 'datetime', '', 120, NULL, '0', '', '', 0, 0, 0, 0, 'single', '', '', 3, 'admin', '2020-05-03 00:57:44', '2020-05-03 00:56:56', 'admin', '', '', '', '', '', NULL, '0', NULL, NULL, '0');
INSERT INTO `onl_cgform_field` VALUES ('03c105d2706c8286416833684de67406', '79091e8277c744158530321513119c68', 'create_by', '创建人', NULL, 0, 1, 'string', 50, 0, '', '', '', '', 'text', '', 120, NULL, '0', '', '', 0, 0, 0, 0, 'single', '', '', 2, 'admin', '2019-05-11 15:29:47', '2019-05-11 15:27:17', 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `onl_cgform_field` VALUES ('03fd5ab69f331ff760c3f7d86b4a93f8', '4028318169e81b970169e81b97650000', 'log_content', '日志内容', 'log_content', 0, 1, 'string', 1000, 0, NULL, NULL, NULL, NULL, 'text', NULL, 120, NULL, '0', NULL, NULL, 0, 1, 1, 0, 'group', NULL, NULL, 3, NULL, NULL, '2019-04-04 19:28:36', 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `onl_cgform_field` VALUES ('045eb432c418b2b103b1e1b8e8a8a75d', 'fb7125a344a649b990c12949945cb6c1', 'age', '年龄', NULL, 0, 1, 'int', 32, 0, '', '', '', '', 'text', '', 120, '', '0', '', '', 0, 1, 1, 0, 'single', '', '', NULL, NULL, NULL, '2019-03-26 19:24:11', 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `onl_cgform_field` VALUES ('04e4185a503e6aaaa31c243829ff4ac7', 'd35109c3632c4952a19ecc094943dd71', 'birthday', '生日', NULL, 0, 1, 'Date', 32, 0, '', '', '', '', 'date', '', 120, '', '0', '', '', 0, 1, 1, 0, 'single', '', '', 10, 'admin', '2020-05-06 11:33:01', '2019-03-15 14:24:35', 'admin', '', '', '', '', '', NULL, '0', NULL, NULL, '0');
INSERT INTO `onl_cgform_field` VALUES ('04ff134cb4aae845059e10b3b85f1451', '7ea60a25fa27470e9080d6a921aabbd1', 'update_time', '更新日期', NULL, 0, 1, 'Date', 20, 0, '', '', '', '', 'datetime', '', 120, NULL, '0', '', '', 0, 0, 0, 0, 'single', '', '', 5, NULL, NULL, '2019-04-17 00:22:21', 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `onl_cgform_field` VALUES ('051dd70c504c97a028daab2af261ea35', '1acb6f81a1d9439da6cc4e868617b565', 'update_time', '更新日期', NULL, 0, 1, 'Date', 20, 0, '', '', '', '', 'datetime', '', 120, NULL, '0', '', '', 0, 0, 0, 0, 'single', '', '', 5, 'admin', '2019-06-10 14:47:14', '2019-04-23 22:58:19', 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `onl_cgform_field` VALUES ('052dcc6f34976b66754fd99415bd22ce', '79091e8277c744158530321513119c68', 'update_time', '更新日期', NULL, 0, 1, 'Date', 20, 0, '', '', '', '', 'datetime', '', 120, NULL, '0', '', '', 0, 0, 0, 0, 'single', '', '', 5, 'admin', '2019-05-11 15:29:47', '2019-05-11 15:27:17', 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `onl_cgform_field` VALUES ('054db05394e83b318f097a60bc044134', '402860816bff91c0016bffa220a9000b', 'residence_address', '户籍地址', 'residence_address', 0, 1, 'string', 200, 0, '', '', '', '', 'text', '', 120, NULL, '0', '', '', 0, 1, 1, 0, 'group', '', '', 28, 'admin', '2019-07-22 16:15:32', '2019-07-17 19:12:24', 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `onl_cgform_field` VALUES ('05dbdd8d9f0f84c8504faa6f24c7d4ac', '402880eb71d61d3d0171d61d3de30000', 'update_time', '更新日期', NULL, 0, 1, 'Date', 20, 0, '', '', '', '', 'datetime', '', 120, NULL, '0', '', '', 0, 0, 0, 0, 'single', '', '', 9, 'admin', '2020-05-03 00:54:16', '2020-05-02 23:59:33', 'admin', '', '', '', '', '', 'text', '0', NULL, NULL, '0');
INSERT INTO `onl_cgform_field` VALUES ('05ed6a78a2e9d0c82bfa2c55898570b8', '997ee931515a4620bc30a9c1246429a9', 'id', '主键', NULL, 1, 0, 'string', 36, 0, '', '', '', '', 'text', '', 120, NULL, '0', '', '', 0, 0, 0, 1, 'single', '', '', 1, 'admin', '2020-05-03 00:57:44', '2020-05-03 00:56:56', 'admin', '', '', '', '', '', NULL, '0', NULL, NULL, '0');
INSERT INTO `onl_cgform_field` VALUES ('0604945c206e867644e9a44b4c9b20c6', 'fb19fb067cd841f9ae93d4eb3b883dc0', '2', '4', NULL, 0, 1, 'string', 32, 0, '', '', '', '', 'text', '', 120, '', '0', '', '', 0, 1, 1, 0, 'single', '', '', 5, NULL, NULL, '2019-03-23 11:39:48', 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `onl_cgform_field` VALUES ('0617de6d735b37a3f80f2f35ad5d1511', '4028839a6de2ebd3016de2ebd3870000', 'size_type', '尺码类型', 'size_type', 0, 1, 'string', 2, 0, NULL, NULL, NULL, NULL, 'text', NULL, 120, NULL, '0', NULL, NULL, 0, 1, 1, 0, 'group', NULL, NULL, 13, NULL, NULL, '2019-10-19 15:29:30', 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `onl_cgform_field` VALUES ('0619dfc3fbf9e193534bb3460a1a9d92', 'cb2d8534a2f544bc9c618dd49da66336', 'id', '主键', NULL, 1, 0, 'string', 36, 0, '', '', '', '', 'text', '', 120, NULL, '0', '', '', 0, 0, 0, 1, 'single', '', '', 1, 'admin', '2020-02-24 17:22:42', '2020-02-24 15:15:13', 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `onl_cgform_field` VALUES ('06a1badae6119abf4ec48858a3e94e1c', '402860816bff91c0016bffa220a9000b', 'sys_org_code', '组织机构编码', 'sys_org_code', 0, 1, 'string', 50, 0, '', '', '', '', 'text', '', 120, NULL, '0', '', '', 0, 1, 1, 0, 'group', '', '', 43, 'admin', '2019-07-22 16:15:32', '2019-07-17 19:12:24', 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `onl_cgform_field` VALUES ('06f1cfff58395ff62526b894f6182641', 'e67d26b610dd414c884c4dbb24e71ce3', 'create_by', '创建人', NULL, 0, 1, 'string', 50, 0, '', '', '', '', 'text', '', 120, NULL, '0', '', '', 0, 0, 0, 0, 'single', '', '', 2, 'admin', '2019-04-24 11:03:32', '2019-04-24 11:02:57', 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `onl_cgform_field` VALUES ('07a307972076a392ffc61b11437f89dd', '402860816bff91c0016bff91c0cb0000', 'create_time', '创建时间', 'create_time', 0, 1, 'Date', 0, 0, '', '', '', '', 'date', '', 120, NULL, '0', '', '', 0, 1, 1, 0, 'group', '', '', 13, 'admin', '2019-07-19 18:09:01', '2019-07-17 18:54:32', 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `onl_cgform_field` VALUES ('07f4776fd641389a8c98a85713990dce', '402860816bff91c0016bff91c0cb0000', 'update_by', '更新人', 'update_by', 0, 1, 'string', 100, 0, '', '', '', '', 'text', '', 120, NULL, '0', '', '', 0, 1, 1, 0, 'group', '', '', 14, 'admin', '2019-07-19 18:09:01', '2019-07-17 18:54:32', 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `onl_cgform_field` VALUES ('08f002095da7c1886c86648fcec38ca9', '56efb74326e74064b60933f6f8af30ea', 'sys_org_code', '所属部门', NULL, 0, 1, 'string', 64, 0, '', '', '', '', 'text', '', 120, NULL, '0', '', '', 0, 0, 0, 0, 'single', '', '', 6, 'admin', '2020-07-10 16:53:27', '2020-05-08 23:45:32', 'admin', '', '', '', '', '', NULL, '0', NULL, NULL, '0');
INSERT INTO `onl_cgform_field` VALUES ('09450359eb90b40d224ec43588a62f9e', '402860816bff91c0016bff91c0cb0000', 'user_id', '用户ID', 'user_id', 0, 1, 'string', 32, 0, '', '', '', '', 'text', '', 120, NULL, '0', '', '', 0, 1, 1, 0, 'group', '', '', 3, 'admin', '2019-07-19 18:09:01', '2019-07-17 18:54:32', 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `onl_cgform_field` VALUES ('0a453a63e33654aa1b9ee2affa854a6d', '4fb8e12a697f4d5bbe9b9fb1e9009486', 'create_by', '创建人', NULL, 0, 1, 'string', 50, 0, '', '', '', '', 'text', '', 120, NULL, '0', '', '', 0, 0, 0, 0, 'single', '', '', 6, 'admin', '2020-04-10 19:51:27', '2020-04-10 19:47:01', 'admin', '', '', '', '', '', NULL, '0', NULL, NULL, '0');
INSERT INTO `onl_cgform_field` VALUES ('0a4cdcb7e54f614ab952024f6c72bb6d', 'beee191324fd40c1afec4fda18bd9d47', 'update_time', '更新日期', NULL, 0, 1, 'Date', 20, 0, '', '', '', '', 'datetime', '', 120, NULL, '0', '', '', 0, 0, 0, 0, 'single', '', '', 5, 'admin', '2019-04-13 13:41:13', '2019-04-13 13:40:56', 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `onl_cgform_field` VALUES ('0acfc27e493c204f72d42b6dc00a2ded', '53a3e82b54b946c2b904f605875a275c', 'update_by', '更新人', NULL, 0, 1, 'string', 50, 0, '', '', '', '', 'text', '', 120, NULL, '0', '', '', 0, 0, 0, 0, 'single', '', '', 4, 'admin', '2020-05-08 23:42:51', '2020-05-07 22:49:47', 'admin', '', '', '', '', '', NULL, '0', NULL, NULL, '0');
INSERT INTO `onl_cgform_field` VALUES ('0adc06d9b497684bcbb5a781e044517c', '1acb6f81a1d9439da6cc4e868617b565', 'supplier', '供应商', NULL, 0, 1, 'String', 32, 0, '', 'air_china_ supplier', '', '', 'list', '', 120, NULL, '0', '', '', 0, 1, 1, 0, 'single', '', '', 15, 'admin', '2019-06-10 14:47:14', '2019-04-24 16:52:00', 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `onl_cgform_field` VALUES ('0ae4bc5d4921aa240d814e018ddb7779', '56efb74326e74064b60933f6f8af30ea', 'money', '订单总额', NULL, 0, 1, 'double', 10, 3, '', '', '', '', 'text', '', 120, NULL, '0', '', '', 0, 1, 1, 0, 'single', '', '', 9, 'admin', '2020-07-10 16:53:27', '2020-05-08 23:45:32', 'admin', '', '', '', '', '', 'text', '0', NULL, NULL, '0');
INSERT INTO `onl_cgform_field` VALUES ('0b3e833ac4aae3a13ec2f8ae460708f8', '4028839a6de2ebd3016de2ebd3870000', 'no', '预算表序号', 'no', 0, 1, 'string', 50, 0, NULL, NULL, NULL, NULL, 'text', NULL, 120, NULL, '0', NULL, NULL, 0, 1, 1, 0, 'group', NULL, NULL, 8, NULL, NULL, '2019-10-19 15:29:30', 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `onl_cgform_field` VALUES ('0b63bd30b8646901310d8d0374df5587', '18f064d1ef424c93ba7a16148851664f', 'fuwenb', '富文本', NULL, 0, 1, 'Text', 0, 0, '', '', '', '', 'umeditor', '', 120, NULL, '0', '', '', 0, 1, 1, 0, 'single', '', '', 17, 'admin', '2020-05-12 20:30:57', '2020-05-12 20:26:01', 'admin', '', '', '', '', '', 'text', '0', NULL, NULL, '0');
INSERT INTO `onl_cgform_field` VALUES ('0ba1bf74e2a6a94a7a63010ec7230706', '402860816bff91c0016bffa220a9000b', 'update_time', '更新时间', 'update_time', 0, 1, 'Date', 0, 0, '', '', '', '', 'date', '', 120, NULL, '0', '', '', 0, 1, 1, 0, 'group', '', '', 42, 'admin', '2019-07-22 16:15:32', '2019-07-17 19:12:24', 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `onl_cgform_field` VALUES ('0bd10d416252bdc6b169056d2a1a4a68', '402880e5721355dd01721355dd390000', 'create_time', '创建日期', NULL, 0, 1, 'Date', 20, 0, '', '', '', '', 'datetime', '', 120, NULL, '0', '', '', 0, 0, 0, 0, 'single', NULL, NULL, 3, NULL, NULL, '2020-05-14 21:18:14', 'admin', '', '', '', '', '', NULL, '0', NULL, NULL, '0');
INSERT INTO `onl_cgform_field` VALUES ('0bf9c178173bd86eec4144b819cfac0b', '18f064d1ef424c93ba7a16148851664f', 'create_by', '创建人', NULL, 0, 1, 'string', 50, 0, '', '', '', '', 'text', '', 120, NULL, '0', '', '', 0, 0, 0, 0, 'single', '', '', 2, 'admin', '2020-05-12 20:30:57', '2020-05-12 20:26:01', 'admin', '', '', '', '', '', NULL, '0', NULL, NULL, '0');
INSERT INTO `onl_cgform_field` VALUES ('0c34ebfe2e2a619d42db13f93d2a2d40', 'b81de38db24047b497d476516f8a0865', 'create_time', '创建日期', NULL, 0, 1, 'Date', 20, 0, '', '', '', '', 'datetime', '', 120, NULL, '0', '', '', 0, 0, 0, 0, 'single', '', '', 3, NULL, NULL, '2020-02-24 14:56:08', 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `onl_cgform_field` VALUES ('0cba94f0497d4d3d829fc573f58eff9f', '402860816bff91c0016bffa220a9000b', 'graduation_time', '毕业时间', 'graduation_time', 0, 1, 'Date', 0, 0, '', '', '', '', 'date', '', 120, NULL, '0', '', '', 0, 1, 1, 0, 'group', '', '', 16, 'admin', '2019-07-22 16:15:32', '2019-07-17 19:12:24', 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `onl_cgform_field` VALUES ('0d00c51a4ddad2598a587fadc968a8b2', '402860816bff91c0016bff91cfea0004', 'sys_org_code', '组织机构编码', 'sys_org_code', 0, 1, 'string', 50, 0, '', '', '', '', 'text', '', 120, NULL, '0', '', '', 0, 1, 1, 0, 'group', '', '', 13, 'admin', '2019-07-19 18:05:13', '2019-07-17 18:54:35', 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `onl_cgform_field` VALUES ('0ddd0c0afc967a9ab6050401ca62a4be', 'e67d26b610dd414c884c4dbb24e71ce3', 'create_time', '创建日期', NULL, 0, 1, 'Date', 20, 0, '', '', '', '', 'datetime', '', 120, NULL, '0', '', '', 0, 0, 0, 0, 'single', '', '', 3, 'admin', '2019-04-24 11:03:32', '2019-04-24 11:02:57', 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `onl_cgform_field` VALUES ('0e5fb96c3f5a37c758eb7f5d1322694f', '402880e5721355dd01721355dd390000', 'good_name', '商品名字', NULL, 0, 1, 'string', 32, 0, '', '', '', '', 'text', '', 120, NULL, '0', '', '', 0, 1, 1, 0, 'single', NULL, NULL, 7, NULL, NULL, '2020-05-14 21:18:14', 'admin', '', '', '', '', '', 'text', '0', NULL, NULL, '0');
INSERT INTO `onl_cgform_field` VALUES ('0e6952db23d5a578ab74344a299c2322', '18f064d1ef424c93ba7a16148851664f', 'birthday', '时间', NULL, 0, 1, 'Date', 0, 0, '', '', '', '', 'date', '', 120, NULL, '0', '', '', 0, 1, 1, 0, 'single', '', '', 13, 'admin', '2020-05-12 20:30:57', '2020-05-12 20:26:01', 'admin', '', '', '', '', '', 'text', '0', NULL, NULL, '0');
INSERT INTO `onl_cgform_field` VALUES ('0f4c07621dcd465f7954b4297962db9b', '18f064d1ef424c93ba7a16148851664f', 'create_time', '创建日期', NULL, 0, 1, 'Date', 20, 0, '', '', '', '', 'datetime', '', 120, NULL, '0', '', '', 0, 0, 0, 0, 'single', '', '', 3, 'admin', '2020-05-12 20:30:57', '2020-05-12 20:26:01', 'admin', '', '', '', '', '', NULL, '0', NULL, NULL, '0');
INSERT INTO `onl_cgform_field` VALUES ('0fb6fa76c5c78a1e957dbb411e110738', '402860816bff91c0016bff91d8830007', 'politically_status', '政治面貌', 'politically_status', 0, 1, 'string', 100, 0, '', '', '', '', 'text', '', 120, NULL, '0', '', '', 0, 1, 1, 0, 'group', '', '', 7, 'admin', '2019-07-19 18:04:41', '2019-07-17 18:54:37', 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `onl_cgform_field` VALUES ('101a73df0aa5199ac05c4ce92a4f0e3e', '4adec929a6594108bef5b35ee9966e9f', 'name', '姓名', NULL, 0, 1, 'String', 200, 0, '', '', '', '', 'text', '', 120, NULL, '0', '', '#{sysUserCode}', 0, 1, 1, 0, 'single', '', '', 3, 'admin', '2020-04-10 19:43:38', '2020-04-10 19:35:58', 'admin', '', '', '', '', '', NULL, '0', NULL, NULL, '0');
INSERT INTO `onl_cgform_field` VALUES ('105be0f7d827a0324216cf8af268fb9e', '09fd28e4b7184c1a9668496a5c496450', 'birthday', '客户生日', NULL, 0, 1, 'Date', 0, 0, '', '', '', '', 'date', '', 120, NULL, '0', '', '', 0, 1, 1, 0, 'single', '', '', 9, 'admin', '2020-05-14 21:19:21', '2020-05-08 23:51:49', 'admin', '', '', '', '', '', 'text', '0', NULL, NULL, '0');
INSERT INTO `onl_cgform_field` VALUES ('105c8e44ad13026b641f0363601f30f3', 'e5464aa8fa7b47c580e91593cf9b46dc', 'num', '循环数量', NULL, 0, 1, 'int', 32, 0, '', '', '', '', 'text', '', 120, NULL, '0', '', '', 0, 1, 1, 0, 'single', '', '', 9, 'admin', '2019-04-24 17:09:49', '2019-04-24 11:05:10', 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `onl_cgform_field` VALUES ('105e112bddec9cc23e853b9c68af1b05', '18f064d1ef424c93ba7a16148851664f', 'radio', 'radio', NULL, 0, 1, 'string', 32, 0, '', 'sex', '', '', 'radio', '', 120, NULL, '0', '', '', 0, 1, 1, 0, 'single', '', '', 9, 'admin', '2020-05-12 20:30:57', '2020-05-12 20:26:01', 'admin', '', '', '', '', '', 'text', '0', NULL, NULL, '0');
INSERT INTO `onl_cgform_field` VALUES ('10b78ee7954f230117689a226c44c0db', '402880e570a5d7000170a5d700f50000', 'descc', '描述', NULL, 0, 1, 'String', 32, 0, '', '', '', '', 'text', '', 120, NULL, '0', '', '', 0, 1, 1, 0, 'single', NULL, NULL, 11, NULL, NULL, '2020-03-04 21:58:16', 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `onl_cgform_field` VALUES ('1130f1e252533529bb1167b896dffe32', 'deea5a8ec619460c9245ba85dbc59e80', 'update_time', '更新日期', NULL, 0, 1, 'Date', 20, 0, '', '', '', '', 'datetime', '', 120, NULL, '0', '', '', 0, 0, 0, 0, 'single', '', '', 5, 'admin', '2020-05-03 01:01:18', '2019-04-20 11:41:19', 'admin', '', '', '', '', '', NULL, '0', NULL, NULL, '0');
INSERT INTO `onl_cgform_field` VALUES ('117fc4ba649d6690a3ac482ad5e4ad38', '56870166aba54ebfacb20ba6c770bd73', 'create_time', '创建日期', NULL, 0, 1, 'Date', 20, 0, '', '', '', '', 'datetime', '', 120, NULL, '0', '', '', 0, 0, 0, 0, 'single', '', '', 3, 'admin', '2020-07-10 17:09:01', '2019-04-20 11:38:39', 'admin', '', '', '', '', '', NULL, '0', NULL, NULL, '0');
INSERT INTO `onl_cgform_field` VALUES ('12aa08f8e948e2b60b40a7b6429c866b', '56efb74326e74064b60933f6f8af30ea', 'order_code', '订单编码', NULL, 0, 1, 'string', 32, 0, '', '', '', '', 'text', '', 120, NULL, '0', '', '${shop_order_num}', 0, 1, 1, 1, 'single', '', '', 7, 'admin', '2020-07-10 16:53:27', '2020-05-08 23:45:32', 'admin', '', '', '', '', '', 'text', '0', NULL, NULL, '0');
INSERT INTO `onl_cgform_field` VALUES ('13246645b7650491b70205d99703ca06', '402860816aa5921f016aa5dedcb90009', 'bpm_status', '流程状态', 'bpm_status', 0, 1, 'string', 32, 0, '1', 'bpm_status', '', '', 'text', '', 120, NULL, '0', '', '', 0, 0, 1, 0, 'group', '', '', 8, 'admin', '2019-05-11 15:56:47', '2019-05-11 15:50:08', 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `onl_cgform_field` VALUES ('135dd0ee50712722db65b8762bd487ea', '8994f2817b5a45d9890aa04497a317c5', 'update_time', '更新日期', NULL, 0, 1, 'date', 20, 0, '', '', '', '', 'text', '', 120, '', '0', '', '', 0, 1, 1, 0, 'single', '', '', 4, NULL, NULL, '2019-03-23 11:39:16', 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `onl_cgform_field` VALUES ('13d9c22ba0a4f09111d115022a148d23', '09fd28e4b7184c1a9668496a5c496450', 'create_by', '创建人', NULL, 0, 1, 'string', 50, 0, '', '', '', '', 'text', '', 120, NULL, '0', '', '', 0, 0, 0, 0, 'single', '', '', 2, 'admin', '2020-05-14 21:19:21', '2020-05-08 23:51:49', 'admin', '', '', '', '', '', NULL, '0', NULL, NULL, '0');
INSERT INTO `onl_cgform_field` VALUES ('14b7e6161d1f908e13026439af302a66', '3b5e18da618247539481816995b6bf9f', 'update_time', '更新日期', NULL, 0, 1, 'Date', 20, 0, '', '', '', '', 'datetime', '', 120, NULL, '0', '', '', 0, 0, 0, 0, 'single', '', '', 5, 'admin', '2020-07-11 11:27:29', '2020-07-11 11:27:17', 'admin', '', '', '', '', '', NULL, '0', NULL, NULL, '0');
INSERT INTO `onl_cgform_field` VALUES ('14ec4c83c29966ab42b6b718c5a3e774', '7ea60a25fa27470e9080d6a921aabbd1', 'create_by', '创建人', NULL, 0, 1, 'string', 50, 0, '', '', '', '', 'text', '', 120, NULL, '0', '', '', 0, 0, 0, 0, 'single', '', '', 2, NULL, NULL, '2019-04-17 00:22:21', 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `onl_cgform_field` VALUES ('154ba4ca5328866010e042086ffc2b81', '56efb74326e74064b60933f6f8af30ea', 'id', '主键', NULL, 1, 0, 'string', 36, 0, '', '', '', '', 'text', '', 120, NULL, '0', '', '', 0, 0, 0, 1, 'single', '', '', 1, 'admin', '2020-07-10 16:53:27', '2020-05-08 23:45:32', 'admin', '', '', '', '', '', NULL, '0', NULL, NULL, '0');
INSERT INTO `onl_cgform_field` VALUES ('16363d0bc125125e395772278d0cf22e', '4b556f0168f64976a3d20bfb932bc798', 'update_time', '更新日期', NULL, 0, 1, 'Date', 20, 0, '', '', '', '', 'datetime', '', 120, NULL, '0', '', '', 0, 0, 0, 0, 'single', '', '', 5, NULL, NULL, '2019-04-12 23:38:28', 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `onl_cgform_field` VALUES ('168426cf1016cf0b99705ae1c4c8591e', '402880e5721355dd01721355dd390000', 'id', '主键', NULL, 1, 0, 'string', 36, 0, '', '', '', '', 'text', '', 120, NULL, '0', '', '', 0, 0, 0, 1, 'single', NULL, NULL, 1, NULL, NULL, '2020-05-14 21:18:14', 'admin', '', '', '', '', '', NULL, '0', NULL, NULL, '0');
INSERT INTO `onl_cgform_field` VALUES ('16918ac159cb6313fec1dea7ac4bd0a0', '402880eb71d52dc30171d52dc3a10000', 'sys_org_code', '所属部门', NULL, 0, 1, 'string', 64, 0, '', '', '', '', 'text', '', 120, NULL, '0', '', '', 0, 0, 0, 0, 'single', NULL, NULL, 10, NULL, NULL, '2020-05-02 19:37:58', 'admin', '', '', '', '', '', 'text', '0', NULL, NULL, '0');
INSERT INTO `onl_cgform_field` VALUES ('169318fa19cf5acf77c58a98c2d5a6bf', '18f064d1ef424c93ba7a16148851664f', 'remakr', 'markdown', NULL, 0, 1, 'Text', 0, 0, '', '', '', '', 'markdown', '', 120, NULL, '0', '', '', 0, 1, 1, 0, 'single', '', '', 16, 'admin', '2020-05-12 20:30:57', '2020-05-12 20:26:01', 'admin', '', '', '', '', '', 'text', '0', NULL, NULL, '0');
INSERT INTO `onl_cgform_field` VALUES ('179c290e383009810fb738f07bd5af8d', '402860816bff91c0016bff91d2810005', 'id', 'id', 'id', 1, 1, 'string', 32, 0, '', '', '', '', 'text', '', 120, NULL, '0', '', '', 0, 0, 0, 0, 'group', '', '', 1, 'admin', '2019-07-19 18:05:55', '2019-07-17 18:54:35', 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `onl_cgform_field` VALUES ('17cbda69da9dd3632625a0647c259070', '73162c3b8161413e8ecdca7eb288d0c9', 'wl_name', '物料名字', NULL, 0, 1, 'String', 200, 0, '', '', '', '', 'text', '', 120, NULL, '1', '', '', 0, 1, 1, 0, 'single', '', '', 7, 'admin', '2019-07-01 14:23:32', '2019-06-10 15:18:34', 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `onl_cgform_field` VALUES ('182d038554a6ee7ddfd07763aaa10686', '53a3e82b54b946c2b904f605875a275c', 'update_time', '更新日期', NULL, 0, 1, 'Date', 20, 0, '', '', '', '', 'datetime', '', 120, NULL, '0', '', '', 0, 0, 0, 0, 'single', '', '', 5, 'admin', '2020-05-08 23:42:51', '2020-05-07 22:49:47', 'admin', '', '', '', '', '', NULL, '0', NULL, NULL, '0');
INSERT INTO `onl_cgform_field` VALUES ('18fefb2257901f05508f8ec13ada78a3', 'e5464aa8fa7b47c580e91593cf9b46dc', 'id', '主键', NULL, 1, 0, 'string', 36, 0, '', '', '', '', 'text', '', 120, NULL, '0', '', '', 0, 0, 0, 0, 'single', '', '', 1, 'admin', '2019-04-24 17:09:48', '2019-04-24 11:05:10', 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `onl_cgform_field` VALUES ('1973ef1d3728fbff2db6a352e001f5f7', 'fb7125a344a649b990c12949945cb6c1', 'name', '用户名', NULL, 0, 1, 'string', 32, 0, '', '', '', '', 'text', '', 120, '', '0', '', '', 0, 1, 1, 0, 'single', '', '', 5, 'admin', '2019-03-26 19:24:11', '2019-03-26 19:01:52', 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `onl_cgform_field` VALUES ('1ab5be1f937f393b3e5cc214ef1b855c', '7ea60a25fa27470e9080d6a921aabbd1', 'update_by', '更新人', NULL, 0, 1, 'string', 50, 0, '', '', '', '', 'text', '', 120, NULL, '0', '', '', 0, 0, 0, 0, 'single', '', '', 4, NULL, NULL, '2019-04-17 00:22:21', 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `onl_cgform_field` VALUES ('1b071c02de2830f9fe82a542b31cce7f', '3b5e18da618247539481816995b6bf9f', 'age', '年龄', NULL, 0, 1, 'int', 10, 0, '', '', '', '', 'text', '', 120, NULL, '0', '', '', 0, 1, 1, 0, 'single', '', '', 8, 'admin', '2020-07-11 11:27:29', '2020-07-11 11:27:17', 'admin', '', '', '', '', '', 'text', '0', NULL, NULL, '0');
INSERT INTO `onl_cgform_field` VALUES ('1b6c7b95028bed9ff656d65557dd2bdf', '402860816bff91c0016bffa220a9000b', 'user_id', '用户id', 'user_id', 0, 1, 'string', 32, 0, '', '', '', '', 'text', '', 120, NULL, '0', '', '', 0, 1, 1, 0, 'group', '', '', 3, 'admin', '2019-07-22 16:15:32', '2019-07-17 19:12:24', 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `onl_cgform_field` VALUES ('1c2f307e315bac77a6d3f02e88387a43', 'deea5a8ec619460c9245ba85dbc59e80', 'id', '主键', NULL, 1, 0, 'string', 36, 0, '', '', '', '', 'text', '', 120, NULL, '0', '', '', 0, 0, 0, 0, 'single', '', '', 1, 'admin', '2020-05-03 01:01:18', '2019-04-20 11:41:19', 'admin', '', '', '', '', '', NULL, '0', NULL, NULL, '0');
INSERT INTO `onl_cgform_field` VALUES ('1c3b2ad0a52ecb47fa7fd53f25875beb', 'deea5a8ec619460c9245ba85dbc59e80', 'price', '价格', NULL, 0, 1, 'double', 32, 0, '', '', '', '', 'text', '', 120, 'n', '0', '', '', 0, 1, 1, 0, 'single', '', '', 7, 'admin', '2020-05-03 01:01:18', '2019-04-20 11:41:19', 'admin', '', '', '', '', '', NULL, '0', NULL, NULL, '0');
INSERT INTO `onl_cgform_field` VALUES ('1c4d25a12388c80d397bb4f4664fe4e6', '4b556f0168f64976a3d20bfb932bc798', 'update_by', '更新人', NULL, 0, 1, 'string', 50, 0, '', '', '', '', 'text', '', 120, NULL, '0', '', '', 0, 0, 0, 0, 'single', '', '', 4, NULL, NULL, '2019-04-12 23:38:28', 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `onl_cgform_field` VALUES ('1cfe967bb457cbaa6e041e45d019b583', '402860816bff91c0016bff91c7010001', 'update_time', '更新时间', 'update_time', 0, 1, 'Date', 0, 0, '', '', '', '', 'date', '', 120, NULL, '0', '', '', 0, 1, 1, 0, 'group', '', '', 10, 'admin', '2019-07-19 18:07:47', '2019-07-17 18:54:32', 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `onl_cgform_field` VALUES ('1d0037eba10efd76be45150479399a7e', '8d66ea41c7cc4ef9ab3aab9055657fc9', 'update_time', '更新日期', NULL, 0, 1, 'Date', 20, 0, '', '', '', '', 'datetime', '', 120, NULL, '0', '', '', 0, 0, 0, 0, 'single', '', '', 5, NULL, NULL, '2020-05-07 22:46:32', 'admin', '', '', '', '', '', NULL, '0', NULL, NULL, '0');
INSERT INTO `onl_cgform_field` VALUES ('1d712db19506ee40b2c1ef5a611baf88', '53a3e82b54b946c2b904f605875a275c', 'name', '商品名字', NULL, 0, 1, 'string', 32, 0, '', '', '', '', 'text', '', 120, NULL, '0', '', '', 0, 1, 1, 0, 'single', '', '', 7, 'admin', '2020-05-08 23:42:51', '2020-05-07 22:49:47', 'admin', '', '', '', '', '', 'text', '0', NULL, NULL, '0');
INSERT INTO `onl_cgform_field` VALUES ('1d95bdf1120c5a1776df022d0a571f21', '4fb8e12a697f4d5bbe9b9fb1e9009486', 'create_time', '创建日期', NULL, 0, 1, 'Date', 20, 0, '', '', '', '', 'datetime', '', 120, NULL, '0', '', '', 0, 0, 0, 0, 'single', '', '', 7, 'admin', '2020-04-10 19:51:27', '2020-04-10 19:47:01', 'admin', '', '', '', '', '', 'text', '0', NULL, NULL, '0');
INSERT INTO `onl_cgform_field` VALUES ('1e37ae77c0d406d4ff3c5442ec63cd1f', '8d66ea41c7cc4ef9ab3aab9055657fc9', 'create_time', '创建日期', NULL, 0, 1, 'Date', 20, 0, '', '', '', '', 'datetime', '', 120, NULL, '0', '', '', 0, 0, 0, 0, 'single', '', '', 3, NULL, NULL, '2020-05-07 22:46:32', 'admin', '', '', '', '', '', NULL, '0', NULL, NULL, '0');
INSERT INTO `onl_cgform_field` VALUES ('1e3d8cfbf12155559666a23ee2c6c5ca', 'e5464aa8fa7b47c580e91593cf9b46dc', 'update_time', '更新日期', NULL, 0, 1, 'Date', 20, 0, '', '', '', '', 'datetime', '', 120, NULL, '0', '', '', 0, 0, 0, 0, 'single', '', '', 5, 'admin', '2019-04-24 17:09:49', '2019-04-24 11:05:10', 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `onl_cgform_field` VALUES ('1ed46fdeb289bd7805c9b83332ccd3b4', '402860816bff91c0016bff91d2810005', 'relation', '关系', 'relation', 0, 1, 'string', 20, 0, '', '', '', '', 'text', '', 120, NULL, '0', '', '', 0, 1, 1, 0, 'group', '', '', 4, 'admin', '2019-07-19 18:05:55', '2019-07-17 18:54:35', 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `onl_cgform_field` VALUES ('1eda61dece35abd76b8d8d49e1b139b8', '8d66ea41c7cc4ef9ab3aab9055657fc9', 'content', '描述', NULL, 0, 1, 'string', 200, 0, '', '', '', '', 'text', '', 120, NULL, '0', '', '', 0, 1, 1, 0, 'single', '', '', 8, NULL, NULL, '2020-05-07 22:46:32', 'admin', '', '', '', '', '', 'text', '0', NULL, NULL, '0');
INSERT INTO `onl_cgform_field` VALUES ('1f0c6d33b79713fe79fb30373c81f6f7', '758334cb1e7445e2822b60e807aec4a3', 'update_time', '更新日期', NULL, 0, 1, 'Date', 20, 0, '', '', '', '', 'datetime', '', 120, NULL, '0', '', '', 0, 0, 0, 0, 'single', '', '', 5, NULL, NULL, '2019-10-18 18:02:09', 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `onl_cgform_field` VALUES ('1fa5f07b3e70d4925b69b2bf51309421', '56870166aba54ebfacb20ba6c770bd73', 'update_time', '更新日期', NULL, 0, 1, 'Date', 20, 0, '', '', '', '', 'datetime', '', 120, NULL, '0', '', '', 0, 0, 0, 0, 'single', '', '', 5, 'admin', '2020-07-10 17:09:01', '2019-04-20 11:38:39', 'admin', '', '', '', '', '', NULL, '0', NULL, NULL, '0');
INSERT INTO `onl_cgform_field` VALUES ('209ddb923d8dab9f454d56d82c0cc725', '3d447fa919b64f6883a834036c14aa67', 'create_by', '创建人', NULL, 0, 1, 'string', 50, 0, '', '', '', '', 'text', '', 120, NULL, '0', '', '', 0, 0, 0, 0, 'single', '', '', 6, 'admin', '2020-02-21 17:58:46', '2020-02-20 16:19:00', 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `onl_cgform_field` VALUES ('20ff34fb0466089cb633d73d5a6f08d6', 'd35109c3632c4952a19ecc094943dd71', 'update_time', '更新日期', NULL, 0, 1, 'date', 20, 0, '', '', '', '', 'text', '', 120, '', '0', '', '', 0, 0, 0, 0, 'single', '', '', 5, 'admin', '2020-05-06 11:33:01', '2019-03-15 14:24:35', 'admin', '', '', '', '', '', NULL, '0', NULL, NULL, '0');
INSERT INTO `onl_cgform_field` VALUES ('2113a4ec7b88b4820dcbbdf96e46bbb7', 'fbc35f067da94a70adb622ddba259352', 'create_time', '创建日期', NULL, 0, 1, 'Date', 20, 0, '', '', '', '', 'datetime', '', 120, NULL, '0', '', '', 0, 0, 0, 0, 'single', '', '', 3, NULL, NULL, '2019-07-03 19:44:23', 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `onl_cgform_field` VALUES ('2150e48b2cb6072d2d8ecd79a7daf7cc', '402860816bff91c0016bff91ca7e0002', 'create_time', '创建时间', 'create_time', 0, 1, 'Date', 0, 0, '', '', '', '', 'date', '', 120, NULL, '0', '', '', 0, 1, 1, 0, 'group', '', '', 10, 'admin', '2019-07-19 18:07:13', '2019-07-17 18:54:33', 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `onl_cgform_field` VALUES ('2323239efb5a40b73034411868dfc41d', 'fb19fb067cd841f9ae93d4eb3b883dc0', 'update_by', '更新人登录名称', NULL, 0, 1, 'string', 50, 0, '', '', '', '', 'text', '', 120, '', '0', '', '', 0, 1, 1, 0, 'single', '', '', 3, NULL, NULL, '2019-03-23 11:39:48', 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `onl_cgform_field` VALUES ('23f42061ed218bdbc1262913c071e1cd', 'e5464aa8fa7b47c580e91593cf9b46dc', 'iz_valid', '启动状态', NULL, 0, 1, 'int', 2, 0, '', 'air_china_valid', '', '', 'list', '', 120, NULL, '0', '', '', 0, 1, 1, 0, 'single', '', '', 12, 'admin', '2019-04-24 17:09:49', '2019-04-24 14:09:06', 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `onl_cgform_field` VALUES ('242cc59b23965a92161eca69ffdbf018', 'd35109c3632c4952a19ecc094943dd71', 'age', '年龄', NULL, 0, 1, 'int', 32, 0, '', '', '', '', 'text', 'http://www.baidu.com', 120, '', '0', '', '', 0, 1, 1, 0, 'single', '', '', 8, 'admin', '2020-05-06 11:33:01', '2019-03-15 14:24:35', 'admin', '', '', '', '', '', NULL, '0', NULL, NULL, '0');
INSERT INTO `onl_cgform_field` VALUES ('24588340313096179b9ec0b49f40bed3', '18f064d1ef424c93ba7a16148851664f', 'sel_mut', '下拉多选', NULL, 0, 1, 'string', 32, 0, '', 'sex', '', '', 'list_multi', '', 120, NULL, '0', '', '', 0, 1, 1, 0, 'single', '', '', 11, 'admin', '2020-05-12 20:30:57', '2020-05-12 20:26:01', 'admin', '', '', '', '', '', 'text', '0', NULL, NULL, '0');
INSERT INTO `onl_cgform_field` VALUES ('2640235b9638547f1776b930bd8c12b4', '997ee931515a4620bc30a9c1246429a9', 'update_time', '更新日期', NULL, 0, 1, 'Date', 20, 0, '', '', '', '', 'datetime', '', 120, NULL, '0', '', '', 0, 0, 0, 0, 'single', '', '', 5, 'admin', '2020-05-03 00:57:44', '2020-05-03 00:56:56', 'admin', '', '', '', '', '', NULL, '0', NULL, NULL, '0');
INSERT INTO `onl_cgform_field` VALUES ('265702edb8872e322fe72d3640e34ac5', '402860816bff91c0016bff91cfea0004', 'from_time', '开始日期', 'from_time', 0, 1, 'Date', 0, 0, '', '', '', '', 'date', '', 120, NULL, '0', '', '', 0, 1, 1, 0, 'group', '', '', 3, 'admin', '2019-07-19 18:05:13', '2019-07-17 18:54:35', 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `onl_cgform_field` VALUES ('26645f6f6eb2646033bad11e0402d7e4', '18f064d1ef424c93ba7a16148851664f', 'sys_org_code', '所属部门', NULL, 0, 1, 'string', 64, 0, '', '', '', '', 'text', '', 120, NULL, '0', '', '', 0, 0, 0, 0, 'single', '', '', 6, 'admin', '2020-05-12 20:30:57', '2020-05-12 20:26:01', 'admin', '', '', '', '', '', NULL, '0', NULL, NULL, '0');
INSERT INTO `onl_cgform_field` VALUES ('2739ab1ece4d6053ba931bb6572f4ed8', '4028839a6de2ebd3016de2ebd3870000', 'iz_valid', '启用状态', 'iz_valid', 0, 1, 'string', 2, 0, NULL, NULL, NULL, NULL, 'text', NULL, 120, NULL, '0', NULL, NULL, 0, 1, 1, 0, 'group', NULL, NULL, 9, NULL, NULL, '2019-10-19 15:29:30', 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `onl_cgform_field` VALUES ('273b0fd37640a9ef1614e987e2bc44a0', '8d66ea41c7cc4ef9ab3aab9055657fc9', 'pics', '图片', NULL, 0, 1, 'string', 500, 0, '', '', '', '', 'image', '', 120, NULL, '0', '', '', 0, 1, 1, 0, 'single', '', '', 9, NULL, NULL, '2020-05-07 22:46:32', 'admin', '', '', '', '', '', 'text', '0', NULL, NULL, '0');
INSERT INTO `onl_cgform_field` VALUES ('281ce0b5343cd42b28825d7df16422f1', 'b81de38db24047b497d476516f8a0865', 'vv', 'vv', NULL, 0, 1, 'String', 32, 0, '', '', '', '', 'text', '', 120, NULL, '0', '', '', 0, 1, 1, 0, 'single', '', '', 8, NULL, NULL, '2020-02-24 14:56:08', 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `onl_cgform_field` VALUES ('283f42283b9d0bf3b95ba3384ab2d255', '758334cb1e7445e2822b60e807aec4a3', 'update_by', '更新人', NULL, 0, 1, 'string', 50, 0, '', '', '', '', 'text', '', 120, NULL, '0', '', '', 0, 0, 0, 0, 'single', '', '', 4, NULL, NULL, '2019-10-18 18:02:09', 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `onl_cgform_field` VALUES ('284864d99fddfdcb00e188e3a512cb28', '1acb6f81a1d9439da6cc4e868617b565', 'no', '预算表序号', NULL, 0, 1, 'string', 50, 0, '', '', '', '', 'text', '', 120, NULL, '0', '', '', 0, 1, 1, 0, 'single', '', '', 10, 'admin', '2019-06-10 14:47:14', '2019-04-23 22:58:19', 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `onl_cgform_field` VALUES ('2889d3cef706f91e092d76a56b8055be', '402860816bff91c0016bff91cda80003', 'order_no', '序号', 'order_no', 0, 1, 'int', 10, 0, '', '', '', '', 'text', '', 120, NULL, '0', '', '', 0, 1, 1, 0, 'group', '', '', 8, 'admin', '2019-07-19 18:06:36', '2019-07-17 18:54:34', 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `onl_cgform_field` VALUES ('29e4abea55d9fa7dbbd0c8dbbb2b3756', '402860816bff91c0016bff91cda80003', 'update_time', '更新时间', 'update_time', 0, 1, 'Date', 0, 0, '', '', '', '', 'date', '', 120, NULL, '0', '', '', 0, 1, 1, 0, 'group', '', '', 12, 'admin', '2019-07-19 18:06:36', '2019-07-17 18:54:34', 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `onl_cgform_field` VALUES ('2aef31179964d0a5a945df7bddff00ae', '53a3e82b54b946c2b904f605875a275c', 'sys_org_code', '所属部门', NULL, 0, 1, 'string', 64, 0, '', '', '', '', 'text', '', 120, NULL, '0', '', '', 0, 0, 0, 0, 'single', '', '', 6, 'admin', '2020-05-08 23:42:51', '2020-05-07 22:49:47', 'admin', '', '', '', '', '', NULL, '0', NULL, NULL, '0');
INSERT INTO `onl_cgform_field` VALUES ('2c70edc7d5a9861239c6537ae0eb39c7', '18f064d1ef424c93ba7a16148851664f', 'dep_sel', '部门', NULL, 0, 1, 'string', 200, 0, '', '', '', '', 'sel_depart', '', 120, NULL, '0', '', '#{sysOrgCode}', 0, 1, 1, 0, 'single', '', '', 19, 'admin', '2020-05-12 20:30:57', '2020-05-12 20:26:01', 'admin', '', '', '', '', '', 'text', '0', NULL, NULL, '0');
INSERT INTO `onl_cgform_field` VALUES ('2c9be627e7cab7f5a2ae9c7ca7ce94a2', 'cb2d8534a2f544bc9c618dd49da66336', 'create_by', '创建人', NULL, 0, 1, 'string', 50, 0, '', '', '', '', 'text', '', 120, NULL, '0', '', '', 0, 0, 0, 0, 'single', '', '', 2, 'admin', '2020-02-24 17:22:42', '2020-02-24 15:15:14', 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `onl_cgform_field` VALUES ('2d53a66f0b72d820b86ff445e2181d76', 'beee191324fd40c1afec4fda18bd9d47', 'id', '主键', NULL, 1, 0, 'string', 36, 0, '', '', '', '', 'text', '', 120, NULL, '0', '', '', 0, 0, 0, 0, 'single', '', '', 1, 'admin', '2019-04-13 13:41:13', '2019-04-13 13:40:56', 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `onl_cgform_field` VALUES ('2dfc4c81926f678c5f8d5ffd27858201', 'e2faf977fdaf4b25a524f58c2441a51c', 'account', '用户编码', NULL, 0, 1, 'String', 32, 0, '', '', '', '', 'text', '', 120, NULL, '0', '', '', 0, 1, 1, 0, 'single', '', '', 7, 'admin', '2019-06-10 17:27:00', '2019-04-24 17:12:11', 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `onl_cgform_field` VALUES ('2e0374878fb3d5be3b0b3f868a97fb59', '09fd28e4b7184c1a9668496a5c496450', 'sex', '客户性别', NULL, 0, 1, 'string', 1, 0, '', 'sex', '', '', 'list', '', 120, NULL, '0', '', '', 0, 1, 1, 0, 'single', '', '', 8, 'admin', '2020-05-14 21:19:21', '2020-05-08 23:51:49', 'admin', '', '', '', '', '', 'text', '0', NULL, NULL, '0');
INSERT INTO `onl_cgform_field` VALUES ('2e5275b6407e1b4265af8519077fa4a5', 'd3ae1c692b9640e0a091f8c46e17bb01', 'sys_org_code', '所属部门', NULL, 0, 1, 'string', 64, 0, '', '', '', '', 'text', '', 120, NULL, '0', '', '', 0, 0, 0, 0, 'single', '', '', 6, NULL, NULL, '2019-07-24 14:47:30', 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `onl_cgform_field` VALUES ('2e66b9db37648389e0846e2204111732', '73162c3b8161413e8ecdca7eb288d0c9', 'has_child', '是否有子节点', NULL, 0, 1, 'string', 3, 0, '', 'valid_status', '', '', 'list', '', 120, NULL, '0', '', '', 0, 0, 0, 0, 'single', '', '', 10, 'admin', '2019-07-01 14:23:32', '2019-06-10 15:18:34', 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `onl_cgform_field` VALUES ('2e6740e79d51ac935d673b7d207611d2', '18f064d1ef424c93ba7a16148851664f', 'id', '主键', NULL, 1, 0, 'string', 36, 0, '', '', '', '', 'text', '', 120, NULL, '0', '', '', 0, 0, 0, 1, 'single', '', '', 1, 'admin', '2020-05-12 20:30:57', '2020-05-12 20:26:01', 'admin', '', '', '', '', '', NULL, '0', NULL, NULL, '0');
INSERT INTO `onl_cgform_field` VALUES ('2e6f741fa96a49a0adccc7b4682c1cef', '4fb8e12a697f4d5bbe9b9fb1e9009486', 'name', '名称', NULL, 0, 1, 'String', 200, 0, '', '', '', '', 'text', '', 120, NULL, '0', '', '#{sysUserName}', 0, 1, 1, 0, 'single', '', '', 3, 'admin', '2020-04-10 19:51:27', '2020-04-10 19:47:01', 'admin', '', '', '', '', '', NULL, '0', NULL, NULL, '0');
INSERT INTO `onl_cgform_field` VALUES ('2ee58d8e4844dfe1fa6b1b841ae0b312', '402860816bff91c0016bff91d2810005', 'politically_status', '政治面貌', 'politically_status', 0, 1, 'string', 100, 0, '', '', '', '', 'text', '', 120, NULL, '0', '', '', 0, 1, 1, 0, 'group', '', '', 7, 'admin', '2019-07-19 18:05:55', '2019-07-17 18:54:35', 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `onl_cgform_field` VALUES ('2f111722eb3a994450e67e3211fd69a8', '402860816bff91c0016bff91ca7e0002', 'id', 'id', 'id', 1, 1, 'string', 32, 0, '', '', '', '', 'text', '', 120, NULL, '0', '', '', 0, 0, 0, 0, 'group', '', '', 1, 'admin', '2019-07-19 18:07:13', '2019-07-17 18:54:33', 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `onl_cgform_field` VALUES ('30143cc3de69c413828f9fba20662026', '402860816bff91c0016bffa220a9000b', 'healthy', '健康状况', 'healthy', 0, 1, 'string', 100, 0, '', '', '', '', 'text', '', 120, NULL, '0', '', '', 0, 1, 1, 0, 'group', '', '', 12, 'admin', '2019-07-22 16:15:32', '2019-07-17 19:12:24', 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `onl_cgform_field` VALUES ('3087aa8f38c787e066a886d950a9edfa', '05a3a30dada7411c9109306aa4117068', 'create_by', '创建人', NULL, 0, 1, 'string', 50, 0, '', '', '', '', 'text', '', 120, NULL, '0', '', '', 0, 0, 0, 0, 'single', '', '', 2, 'admin', '2020-05-12 22:39:41', '2020-05-06 11:34:31', 'admin', '', '', '', '', '', NULL, '0', NULL, NULL, '0');
INSERT INTO `onl_cgform_field` VALUES ('31193dc8ceacf979e4042e784ea8278a', '402880e570a5d7000170a5d700f50000', 'order_fk_id', '订单外键ID', NULL, 0, 0, 'String', 32, 0, '', '', '', '', 'text', '', 120, NULL, '0', '', '', 0, 0, 0, 0, 'single', NULL, NULL, 10, NULL, NULL, '2020-03-04 21:58:16', 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `onl_cgform_field` VALUES ('315eab14c7d9de37434b3cb7fa7b054d', '18f064d1ef424c93ba7a16148851664f', 'files', '文件', NULL, 0, 1, 'string', 1000, 0, '', '', '', '', 'file', '', 120, NULL, '0', '', '', 0, 1, 1, 0, 'single', '', '', 15, 'admin', '2020-05-12 20:30:57', '2020-05-12 20:26:01', 'admin', '', '', '', '', '', 'text', '0', NULL, NULL, '0');
INSERT INTO `onl_cgform_field` VALUES ('31fd90306c3942f09cb79deabbf2f541', '402860816bff91c0016bff91d2810005', 'employee_id', '员工ID', 'employee_id', 0, 1, 'string', 32, 0, '', '', '', '', 'text', '', 120, NULL, '0', '', '', 0, 1, 1, 0, 'group', 'oa_employee_info', 'id', 2, 'admin', '2019-07-19 18:05:55', '2019-07-17 18:54:35', 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `onl_cgform_field` VALUES ('336495117e3a31351fed6963b950dddb', '402860816bff91c0016bffa220a9000b', 'inside_transfer', '内部工作调动情况', 'inside_transfer', 0, 1, 'string', 255, 0, '', '', '', '', 'text', '', 120, NULL, '0', '', '', 0, 1, 1, 0, 'group', '', '', 37, 'admin', '2019-07-22 16:15:32', '2019-07-17 19:12:24', 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `onl_cgform_field` VALUES ('34138092d743d4232341a920efd2699e', '402880eb71d52dc30171d52dc3a10000', 'name', '名称', NULL, 0, 1, 'String', 200, 0, '', '', '', '', 'text', '', 120, NULL, '0', '', '#{sysUserName}', 0, 1, 1, 0, 'single', NULL, NULL, 3, NULL, NULL, '2020-05-02 19:37:58', 'admin', '', '', '', '', '', NULL, '0', NULL, NULL, '0');
INSERT INTO `onl_cgform_field` VALUES ('345c8b48e1e128e77c4c6e2b36512804', '402860816aa5921f016aa5dedcb90009', 'create_by', '创建人', 'create_by', 0, 1, 'string', 50, 0, '', '', '', '', 'text', '', 120, NULL, '0', '', '', 0, 0, 0, 0, 'group', '', '', 2, 'admin', '2019-05-11 15:56:47', '2019-05-11 15:50:08', 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `onl_cgform_field` VALUES ('34625a911b39e0596690c1a15f784448', '402880e570a5d7000170a5d700f50000', 'update_time', '更新日期', NULL, 0, 1, 'Date', 20, 0, '', '', '', '', 'datetime', '', 120, NULL, '0', '', '', 0, 0, 0, 0, 'single', NULL, NULL, 5, NULL, NULL, '2020-03-04 21:58:16', 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `onl_cgform_field` VALUES ('3489a6451bbbcabc0f39ca04b0dd62f2', '8d66ea41c7cc4ef9ab3aab9055657fc9', 'sys_org_code', '所属部门', NULL, 0, 1, 'string', 64, 0, '', '', '', '', 'text', '', 120, NULL, '0', '', '', 0, 0, 0, 0, 'single', '', '', 6, NULL, NULL, '2020-05-07 22:46:32', 'admin', '', '', '', '', '', NULL, '0', NULL, NULL, '0');
INSERT INTO `onl_cgform_field` VALUES ('351faaeb2dd8105e9c66f678211c9d4f', 'dbf4675875e14676a3f9a8b2b8941140', 'update_time', '更新日期', NULL, 0, 1, 'Date', 20, 0, '', '', '', '', 'datetime', '', 120, NULL, '0', '', '', 0, 0, 0, 0, 'single', '', '', 5, NULL, NULL, '2019-05-27 18:02:07', 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `onl_cgform_field` VALUES ('35417ba3993afe3405e1e0b9abbd7e1b', '402880e5721355dd01721355dd390000', 'num', '数量', NULL, 0, 1, 'int', 10, 0, '', '', '', '', 'text', '', 120, NULL, '0', '', '', 0, 1, 1, 0, 'single', NULL, NULL, 9, NULL, NULL, '2020-05-14 21:18:14', 'admin', '', '', '', '', '', 'text', '0', NULL, NULL, '0');
INSERT INTO `onl_cgform_field` VALUES ('354b2ce39c5e8ec3f0bbb01bf8ff0fb7', '32f75e4043ef4070919dbd4337186a3d', 'content', '描述', NULL, 0, 1, 'String', 300, 0, '', '', '', '', 'textarea', '', 120, NULL, '0', '', '', 0, 1, 1, 0, 'single', '', '', 9, 'admin', '2019-04-11 10:15:31', '2019-03-28 15:24:24', 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `onl_cgform_field` VALUES ('35ca1c8aa1501bc8a79c880928841f18', '402860816aa5921f016aa5921f480000', 'update_by', '修改人id', 'update_by', 0, 1, 'string', 32, 0, '', '', '', '', 'text', '', 120, NULL, '0', '', '', 0, 0, 0, 0, 'group', '', '', 11, 'admin', '2019-05-11 15:31:55', '2019-05-11 14:26:19', 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `onl_cgform_field` VALUES ('3635793767371c6db9f76b4b79f9d321', '402860816bff91c0016bff91d8830007', 'create_time', '创建时间', 'create_time', 0, 1, 'Date', 0, 0, '', '', '', '', 'date', '', 120, NULL, '0', '', '', 0, 1, 1, 0, 'group', '', '', 11, 'admin', '2019-07-19 18:04:41', '2019-07-17 18:54:37', 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `onl_cgform_field` VALUES ('370a6eebc2d732eaf121fe0830d853a6', 'e5464aa8fa7b47c580e91593cf9b46dc', 'wl_code', '物料编码', NULL, 0, 1, 'String', 100, 0, '', '', '', '', 'text', '', 120, NULL, '0', '', '', 0, 0, 0, 0, 'single', '', '', 7, 'admin', '2019-04-24 17:09:49', '2019-04-24 11:05:10', 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `onl_cgform_field` VALUES ('37462a4d78968d0e432d746423603b81', '3d447fa919b64f6883a834036c14aa67', 'province', '省份', NULL, 0, 1, 'String', 100, 0, '', '', '{table:\'sys_category\',txt:\'name\',key:\'id\',linkField:\'city,area\',idField:\'id\',pidField:\'pid\', condition:\" pid = \'1230769196661510146\' \"         }', '', 'link_down', '', 120, NULL, '0', '', '', 0, 1, 1, 0, 'single', '', '', 2, 'admin', '2020-02-21 17:58:46', '2020-02-20 16:19:00', 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `onl_cgform_field` VALUES ('37e2548874f09ef7d08642a30bc918fa', 'fbc35f067da94a70adb622ddba259352', 'group_name', '小组名', NULL, 0, 1, 'String', 32, 0, '', '', '', '', 'text', '', 120, NULL, '0', '', '', 0, 1, 1, 0, 'single', '', '', 7, NULL, NULL, '2019-07-03 19:44:23', 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `onl_cgform_field` VALUES ('391e7cbd9f29743b11bb555c50547b1f', '32f75e4043ef4070919dbd4337186a3d', 'update_time', '更新日期', NULL, 0, 1, 'Date', 20, 0, '', '', '', '', 'datetime', '', 120, NULL, '0', '', '', 0, 0, 0, 0, 'single', '', '', 4, 'admin', '2019-04-11 10:15:32', '2019-03-27 15:54:49', 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `onl_cgform_field` VALUES ('396c36fa5320975851d06772d10ea7b1', 'cb2d8534a2f544bc9c618dd49da66336', 'update_by', '更新人', NULL, 0, 1, 'string', 50, 0, '', '', '', '', 'text', '', 120, NULL, '0', '', '', 0, 0, 0, 0, 'single', '', '', 4, 'admin', '2020-02-24 17:22:42', '2020-02-24 15:15:14', 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `onl_cgform_field` VALUES ('3b439859f98e30e34d25e983eb22e408', '402860816bff91c0016bff91c7010001', 'award_time', '获奖时间', 'award_time', 0, 1, 'Date', 0, 0, '', '', '', '', 'date', '', 120, NULL, '0', '', '', 0, 1, 1, 0, 'group', '', '', 3, 'admin', '2019-07-19 18:07:47', '2019-07-17 18:54:32', 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `onl_cgform_field` VALUES ('3bf44e68de518f3ddf72b87671d0ff90', '8994f2817b5a45d9890aa04497a317c5', 'update_by', '更新人登录名称', NULL, 0, 1, 'string', 50, 0, '', '', '', '', 'text', '', 120, '', '0', '', '', 0, 1, 1, 0, 'single', '', '', 3, NULL, NULL, '2019-03-23 11:39:16', 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `onl_cgform_field` VALUES ('3cd2061ea15ce9eeb4b7cf2e544ccb6b', 'd35109c3632c4952a19ecc094943dd71', 'file_kk', '附件', NULL, 0, 1, 'String', 500, 0, '', '', '', '', 'file', '', 120, NULL, '0', '', '', 0, 1, 1, 0, 'single', '', '', 13, 'admin', '2020-05-06 11:33:01', '2019-06-10 20:06:57', 'admin', '', '', '', '', '', NULL, '0', NULL, NULL, '0');
INSERT INTO `onl_cgform_field` VALUES ('3cfd4d60c7d8409ae716a579bcb0910d', '402860816bff91c0016bff91c0cb0000', 'sys_org_code', '组织机构编码', 'sys_org_code', 0, 1, 'string', 50, 0, '', '', '', '', 'text', '', 120, NULL, '0', '', '', 0, 1, 1, 0, 'group', '', '', 16, 'admin', '2019-07-19 18:09:01', '2019-07-17 18:54:32', 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `onl_cgform_field` VALUES ('3e32f6c30c9028872388f70743c5d6a5', '402860816bff91c0016bff91c0cb0000', 'reason', '申请理由', 'reason', 0, 1, 'string', 200, 0, '', '', '', '', 'text', '', 120, NULL, '0', '', '', 0, 1, 1, 0, 'group', '', '', 9, 'admin', '2019-07-19 18:09:01', '2019-07-17 18:54:32', 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `onl_cgform_field` VALUES ('3e70d1c516c3533c6698300665c669e1', '402860816bff91c0016bff91c0cb0000', 'id', 'id', 'id', 1, 1, 'string', 32, 0, '', '', '', '', 'text', '', 120, NULL, '0', '', '', 0, 0, 0, 0, 'group', '', '', 1, 'admin', '2019-07-19 18:09:01', '2019-07-17 18:54:31', 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `onl_cgform_field` VALUES ('3ef0a9ed04605267f7fa304a8c353576', '05a3a30dada7411c9109306aa4117068', 'name', '用户名', NULL, 0, 1, 'string', 32, 0, '', 'username', 'tj_user_report', 'name', 'popup', '', 120, NULL, '0', '', '', 1, 1, 1, 0, 'single', '', '', 7, 'admin', '2020-05-12 22:39:41', '2020-05-06 11:34:31', 'admin', '', '', '', '', '', 'text', '0', NULL, NULL, '1');
INSERT INTO `onl_cgform_field` VALUES ('3eff1d21b946e23d7f83de977962d9cf', '402880eb71d61d3d0171d61d3de30000', 'main_id', '主表ID', NULL, 0, 1, 'String', 200, 0, '', '', '', '', 'text', '', 120, NULL, '0', '', '', 0, 0, 0, 0, 'single', '', '', 5, 'admin', '2020-05-03 00:54:16', '2020-05-02 23:59:33', 'admin', '', '', '', '', '', NULL, '0', NULL, NULL, '0');
INSERT INTO `onl_cgform_field` VALUES ('3f2ace8f968a0e5b91d1340ee2957cda', '402860816bff91c0016bff91d8830007', 'real_name', '姓名', 'real_name', 0, 1, 'string', 100, 0, '', '', '', '', 'text', '', 120, NULL, '0', '', '', 0, 1, 1, 0, 'group', '', '', 3, 'admin', '2019-07-19 18:04:41', '2019-07-17 18:54:37', 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `onl_cgform_field` VALUES ('3f7f7720ee65648cb2850fccedf7be9d', '53a3e82b54b946c2b904f605875a275c', 'contents', '商品简介', NULL, 0, 1, 'Text', 0, 0, '', '', '', '', 'umeditor', '', 120, NULL, '0', '', '', 0, 1, 1, 0, 'single', '', '', 10, 'admin', '2020-05-08 23:42:51', '2020-05-07 22:49:47', 'admin', '', '', '', '', '', 'text', '0', NULL, NULL, '0');
INSERT INTO `onl_cgform_field` VALUES ('3f9deca6c193f0b2609113713956ad19', '86bf17839a904636b7ed96201b2fa6ea', 'order_main_id', '订单ID', NULL, 0, 1, 'string', 32, 0, '', '', '', '', 'text', '', 120, NULL, '0', '', '', 0, 0, 0, 0, 'single', 'ces_order_main', 'id', 11, 'admin', '2020-05-14 21:18:49', '2020-05-08 23:48:31', 'admin', '', '', '', '', '', 'text', '0', NULL, NULL, '0');
INSERT INTO `onl_cgform_field` VALUES ('40471eb4560bf0bbd2ffef17d48a269d', 'dbf4675875e14676a3f9a8b2b8941140', 'update_by', '更新人', NULL, 0, 1, 'string', 50, 0, '', '', '', '', 'text', '', 120, NULL, '0', '', '', 0, 0, 0, 0, 'single', '', '', 4, NULL, NULL, '2019-05-27 18:02:07', 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `onl_cgform_field` VALUES ('404b516d4f2229f292783db595b02ba1', '402860816bff91c0016bff91d8830007', 'update_time', '更新时间', 'update_time', 0, 1, 'Date', 0, 0, '', '', '', '', 'date', '', 120, NULL, '0', '', '', 0, 1, 1, 0, 'group', '', '', 13, 'admin', '2019-07-19 18:04:41', '2019-07-17 18:54:37', 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `onl_cgform_field` VALUES ('405de5ea82e54138a0613dd41b006dfb', '56870166aba54ebfacb20ba6c770bd73', 'update_by', '更新人', NULL, 0, 1, 'string', 50, 0, '', '', '', '', 'text', '', 120, NULL, '0', '', '', 0, 0, 0, 0, 'single', '', '', 4, 'admin', '2020-07-10 17:09:01', '2019-04-20 11:38:39', 'admin', '', '', '', '', '', NULL, '0', NULL, NULL, '0');
INSERT INTO `onl_cgform_field` VALUES ('40675bb9f053aabf8823ddf4b5389141', 'b81de38db24047b497d476516f8a0865', 'aa', 'aa', NULL, 0, 1, 'String', 32, 0, '', '', '', '', 'text', '', 120, NULL, '0', '', '', 0, 1, 1, 0, 'single', '', '', 7, NULL, NULL, '2020-02-24 14:56:08', 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `onl_cgform_field` VALUES ('4164314d6a51d100169a29872b7504d8', '402860816bff91c0016bff91ca7e0002', 'cert_time', '发证时间', 'cert_time', 0, 1, 'Date', 0, 0, '', '', '', '', 'date', '', 120, NULL, '0', '', '', 0, 1, 1, 0, 'group', '', '', 3, 'admin', '2019-07-19 18:07:13', '2019-07-17 18:54:33', 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `onl_cgform_field` VALUES ('41d4215c01b0d26871f2cb83d3e532ae', '402860816bff91c0016bff91c0cb0000', 'bpm_status', '流程状态', NULL, 0, 1, 'String', 32, 0, '1', '', '', '', 'text', '', 120, NULL, '0', '', '', 0, 1, 1, 0, 'single', '', '', 17, 'admin', '2019-07-19 18:09:01', '2019-07-19 15:35:23', 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `onl_cgform_field` VALUES ('422a44a15fa39fd57c3c23eb601f7c03', '56870166aba54ebfacb20ba6c770bd73', 'descc', '描述', NULL, 0, 1, 'String', 100, 0, '', '', '', '', 'text', '', 120, NULL, '0', '', '', 0, 1, 1, 0, 'single', '', '', 8, 'admin', '2020-07-10 17:09:01', '2019-04-20 11:38:39', 'admin', '', '', '', '', '', NULL, '0', NULL, NULL, '0');
INSERT INTO `onl_cgform_field` VALUES ('42cccfa014c9e131a0a1b23f563d3688', '402860816bff91c0016bffa220a9000b', 'sex', '性别', 'sex', 0, 1, 'string', 20, 0, '', '', '', '', 'text', '', 120, NULL, '0', '', '', 0, 1, 1, 0, 'group', '', '', 6, 'admin', '2019-07-22 16:15:32', '2019-07-17 19:12:24', 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `onl_cgform_field` VALUES ('4312f618c83e07db82e468b81a1eaa45', '402860816bff91c0016bffa220a9000b', 'photo', '照片', 'photo', 0, 1, 'string', 255, 0, '', '', '', '', 'text', '', 120, NULL, '0', '', '', 0, 1, 1, 0, 'group', '', '', 20, 'admin', '2019-07-22 16:15:32', '2019-07-17 19:12:24', 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `onl_cgform_field` VALUES ('435b57180fc995e3c4ec42516963bca3', '4028839a6de2ebd3016de2ebd3870000', 'wl_code', '物料编码', 'wl_code', 0, 1, 'string', 60, 0, NULL, NULL, NULL, NULL, 'text', NULL, 120, NULL, '0', NULL, NULL, 0, 1, 1, 0, 'group', NULL, NULL, 6, NULL, NULL, '2019-10-19 15:29:30', 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `onl_cgform_field` VALUES ('44bdc595f1e565fc053e01134b92bb47', 'd3ae1c692b9640e0a091f8c46e17bb01', 'update_by', '更新人', NULL, 0, 1, 'string', 50, 0, '', '', '', '', 'text', '', 120, NULL, '0', '', '', 0, 0, 0, 0, 'single', '', '', 4, NULL, NULL, '2019-07-24 14:47:30', 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `onl_cgform_field` VALUES ('44e81e24d2384b0f187e8f69eda55390', '402860816bff91c0016bff91cda80003', 'create_time', '创建时间', 'create_time', 0, 1, 'Date', 0, 0, '', '', '', '', 'date', '', 120, NULL, '0', '', '', 0, 1, 1, 0, 'group', '', '', 10, 'admin', '2019-07-19 18:06:36', '2019-07-17 18:54:34', 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `onl_cgform_field` VALUES ('45c0a1a89a1e2a72533b9af894be1011', '27fc5f91274344afa7673a732b279939', 'create_time', '创建日期', NULL, 0, 1, 'Date', 20, 0, '', '', '', '', 'datetime', '', 120, NULL, '0', '', '', 0, 0, 0, 0, 'single', '', '', 3, 'admin', '2019-07-01 16:28:20', '2019-07-01 16:26:42', 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `onl_cgform_field` VALUES ('45d59eb647257fcbcb9d143ff1ba2080', 'deea5a8ec619460c9245ba85dbc59e80', 'pro_type', '产品类型', NULL, 0, 1, 'String', 32, 0, '', 'sex', '', '', 'radio', '', 120, NULL, '0', '', '', 0, 1, 1, 0, 'single', '', '', 9, 'admin', '2020-05-03 01:01:18', '2019-04-23 20:54:08', 'admin', '', '', '', '', '', NULL, '0', NULL, NULL, '0');
INSERT INTO `onl_cgform_field` VALUES ('469b250595f15dfebe69991d72e4bfb2', 'e9faf717024b4aae95cff224ae9b6d97', 'name', '员工姓名', NULL, 0, 1, 'String', 32, 0, '', '', '', '', 'text', '', 120, NULL, '0', '', '', 0, 1, 1, 0, 'single', '', '', 6, 'admin', '2019-07-03 18:23:49', '2019-07-03 18:22:35', 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `onl_cgform_field` VALUES ('46be01bef342519e268902d0d36a7473', 'deea5a8ec619460c9245ba85dbc59e80', 'descc', '描述', NULL, 0, 1, 'String', 32, 0, '', '', '', '', 'text', '', 120, NULL, '0', '', '', 0, 1, 1, 0, 'single', '', '', 11, 'admin', '2020-05-03 01:01:18', '2019-04-20 11:41:19', 'admin', '', '', '', '', '', NULL, '0', NULL, NULL, '0');
INSERT INTO `onl_cgform_field` VALUES ('46f1a875f86a4f48d0540ad0d5e667d7', '56870166aba54ebfacb20ba6c770bd73', 'order_date', '下单时间', NULL, 0, 1, 'Date', 32, 0, '', '', '', '', 'date', '', 120, NULL, '0', '', '', 0, 1, 1, 0, 'single', '', '', 7, 'admin', '2020-07-10 17:09:01', '2019-04-20 11:38:39', 'admin', '', '', '', '', '', NULL, '0', NULL, NULL, '0');
INSERT INTO `onl_cgform_field` VALUES ('47c21a6b45e59a6b70bb9c0cc4510a68', '1acb6f81a1d9439da6cc4e868617b565', 'integral_val', '积分值', NULL, 0, 1, 'int', 32, 0, '', '', '', '', 'text', '', 120, NULL, '0', '', '', 0, 1, 1, 0, 'single', '', '', 13, 'admin', '2019-06-10 14:47:14', '2019-04-23 22:58:19', 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `onl_cgform_field` VALUES ('47fa05530f3537a1be8f9e7a9e98be82', 'd35109c3632c4952a19ecc094943dd71', 'sex', '性别', NULL, 0, 1, 'string', 32, 0, '', 'sex', '', '', 'list', '', 120, '', '0', '', '', 0, 1, 1, 0, 'single', '', '', 7, 'admin', '2020-05-06 11:33:01', '2019-03-15 14:24:35', 'admin', '', '', '', '', '', NULL, '0', NULL, NULL, '0');
INSERT INTO `onl_cgform_field` VALUES ('4851697fdf63709d2bc7451b7415f4af', '32f75e4043ef4070919dbd4337186a3d', 'sex', '性别', NULL, 0, 1, 'String', 32, 0, '1', 'sex', '', '', 'list', '', 120, NULL, '0', '', '', 1, 1, 1, 0, 'single', '', '', 6, 'admin', '2019-04-11 10:15:32', '2019-03-27 15:54:49', 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `onl_cgform_field` VALUES ('485a8ddce2c033f88af674ec98b68e32', '402860816bff91c0016bffa220a9000b', 'create_time', '创建时间', 'create_time', 0, 1, 'Date', 0, 0, '', '', '', '', 'date', '', 120, NULL, '0', '', '', 0, 1, 1, 0, 'group', '', '', 40, 'admin', '2019-07-22 16:15:32', '2019-07-17 19:12:24', 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `onl_cgform_field` VALUES ('492a462b212fc34b0ee70e872684ed7e', '7ea60a25fa27470e9080d6a921aabbd1', 'name', '用户名', NULL, 0, 1, 'String', 32, 0, '', '', '', '', 'text', '', 120, NULL, '0', '', '', 0, 1, 1, 0, 'single', '', '', 6, NULL, NULL, '2019-04-17 00:22:21', 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `onl_cgform_field` VALUES ('499a5dac033a01ce58009e4c5b786697', 'e9faf717024b4aae95cff224ae9b6d97', 'age', '员工年龄', NULL, 0, 1, 'int', 32, 0, '', '', '', '', 'text', '', 120, NULL, '0', '', '', 0, 1, 1, 0, 'single', '', '', 7, 'admin', '2019-07-03 18:23:49', '2019-07-03 18:22:35', 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `onl_cgform_field` VALUES ('49cd3dbd4f7f7cf0d19b1ee1045cfa69', 'e67d26b610dd414c884c4dbb24e71ce3', 'post_code', '岗位编码', NULL, 0, 1, 'String', 32, 0, '', '', '', '', 'text', '', 120, NULL, '0', '', '', 0, 1, 1, 0, 'single', '', '', 6, 'admin', '2019-04-24 11:03:32', '2019-04-24 11:02:57', 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `onl_cgform_field` VALUES ('4ad94222c025b56fb0833a88a1514aeb', '86bf17839a904636b7ed96201b2fa6ea', 'id', '主键', NULL, 1, 0, 'string', 36, 0, '', '', '', '', 'text', '', 120, NULL, '0', '', '', 0, 0, 0, 1, 'single', '', '', 1, 'admin', '2020-05-14 21:18:49', '2020-05-08 23:48:31', 'admin', '', '', '', '', '', NULL, '0', NULL, NULL, '0');
INSERT INTO `onl_cgform_field` VALUES ('4b136f953675fffcc1b6d7d6d414d57e', '402880eb71d61d3d0171d61d3de30000', 'date', '日期', NULL, 0, 1, 'string', 200, 0, '', '', '', '', 'text', '', 120, NULL, '0', '', '#{date}', 0, 1, 1, 0, 'single', '', '', 4, 'admin', '2020-05-03 00:54:16', '2020-05-02 23:59:33', 'admin', '', '', '', '', '', NULL, '0', NULL, NULL, '0');
INSERT INTO `onl_cgform_field` VALUES ('4ba7c553ca4babcec75273c531cd65e1', '402860816bff91c0016bff91cfea0004', 'workplace', '工作单位', 'workplace', 0, 1, 'string', 100, 0, '', '', '', '', 'text', '', 120, NULL, '0', '', '', 0, 1, 1, 0, 'group', '', '', 5, 'admin', '2019-07-19 18:05:13', '2019-07-17 18:54:35', 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `onl_cgform_field` VALUES ('4c2cba9fc950333421c4193576b8384d', '32f75e4043ef4070919dbd4337186a3d', 'salary', '工资', NULL, 0, 1, 'double', 32, 0, '', '', '', '', 'text', '', 120, NULL, '0', '', '', 0, 1, 1, 0, 'single', '', '', 10, 'admin', '2019-04-11 10:15:32', '2019-03-28 15:24:24', 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `onl_cgform_field` VALUES ('4c4f4067fa31737f3ff9e088130ef793', '4adec929a6594108bef5b35ee9966e9f', 'sex', '性别', NULL, 0, 1, 'String', 200, 0, '', 'sex', '', '', 'list', '', 120, NULL, '0', '', '{{ 2 }}', 0, 1, 1, 0, 'single', '', '', 4, 'admin', '2020-04-10 19:43:38', '2020-04-10 19:35:58', 'admin', '', '', '', '', '', NULL, '0', NULL, NULL, '0');
INSERT INTO `onl_cgform_field` VALUES ('4c570c5cf05590348e12621ca62773cf', '402860816aa5921f016aa5921f480000', 'name', '请假人', 'name', 0, 1, 'string', 100, 0, '', '', '', '', 'text', '', 120, NULL, '0', '', '', 0, 1, 1, 0, 'group', '', '', 2, 'admin', '2019-05-11 15:31:54', '2019-05-11 14:26:19', 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `onl_cgform_field` VALUES ('4cacfa054e96791ab938b5c8f8e02cd1', '27fc5f91274344afa7673a732b279939', 'bpm_status', '流程状态', NULL, 0, 1, 'String', 2, 0, '', 'bpm_status', '', '', 'list', '', 120, NULL, '0', '', '', 0, 1, 1, 0, 'single', '', '', 9, NULL, NULL, '2019-07-01 16:28:20', 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `onl_cgform_field` VALUES ('4dc3f7e772564de45773a8379adc4335', '3b5e18da618247539481816995b6bf9f', 'create_time', '创建日期', NULL, 0, 1, 'Date', 20, 0, '', '', '', '', 'datetime', '', 120, NULL, '0', '', '', 0, 0, 0, 0, 'single', '', '', 3, 'admin', '2020-07-11 11:27:29', '2020-07-11 11:27:17', 'admin', '', '', '', '', '', NULL, '0', NULL, NULL, '0');
INSERT INTO `onl_cgform_field` VALUES ('4e3760f9068aee4318123d85fbf2ebf9', '53a3e82b54b946c2b904f605875a275c', 'good_type_id', '商品分类', NULL, 0, 1, 'string', 32, 0, '', '0', 'ces_shop_type', 'id,pid,name,has_child', 'sel_tree', '', 120, NULL, '0', '', '', 0, 1, 1, 0, 'single', '', '', 11, 'admin', '2020-05-08 23:42:51', '2020-05-07 22:51:42', 'admin', '', '', '', '', '', 'text', '0', NULL, NULL, '0');
INSERT INTO `onl_cgform_field` VALUES ('4f718d95ad9de33eac18fd0663e4c1f1', '32f75e4043ef4070919dbd4337186a3d', 'birthday', '生日', NULL, 0, 1, 'Date', 32, 0, '', '', '', '', 'date', '', 120, NULL, '0', '', '', 1, 1, 1, 0, 'single', '', '', 8, 'admin', '2019-04-11 10:15:32', '2019-03-27 15:54:49', 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `onl_cgform_field` VALUES ('4f7cba71de7afe6efbd024f5f9935521', '402860816bff91c0016bff91cda80003', 'to_time', '截止时间', 'to_time', 0, 1, 'Date', 0, 0, '', '', '', '', 'date', '', 120, NULL, '0', '', '', 0, 1, 1, 0, 'group', '', '', 4, 'admin', '2019-07-19 18:06:36', '2019-07-17 18:54:34', 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `onl_cgform_field` VALUES ('4fa29e880104e0ed0cb9143d801b209f', '18f064d1ef424c93ba7a16148851664f', 'checkbox', 'checkbox', NULL, 0, 1, 'string', 32, 0, '', 'sex', '', '', 'checkbox', '', 120, NULL, '0', '', '', 0, 1, 1, 0, 'single', '', '', 10, 'admin', '2020-05-12 20:30:57', '2020-05-12 20:26:01', 'admin', '', '', '', '', '', 'text', '0', NULL, NULL, '0');
INSERT INTO `onl_cgform_field` VALUES ('4faa7848b92f05bcb3cf03de34af9ff2', 'cb2d8534a2f544bc9c618dd49da66336', 'ddd', 'dd', NULL, 0, 1, 'String', 32, 0, '', '', '', '', 'text', '', 120, NULL, '0', '', '', 0, 1, 1, 0, 'single', '', '', 9, 'admin', '2020-02-24 17:22:42', '2020-02-24 15:15:14', 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `onl_cgform_field` VALUES ('506c9b0b2331a24e5c284274d28fe569', '27fc5f91274344afa7673a732b279939', 'kkk', '描述', NULL, 0, 1, 'String', 32, 0, '', '', '', '', 'text', '', 120, NULL, '0', '', '', 0, 1, 1, 0, 'single', '', '', 8, 'admin', '2019-07-01 16:28:20', '2019-07-01 16:26:42', 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `onl_cgform_field` VALUES ('508eb28e1409a2a9501cdf6fd7eb24c7', 'dbf4675875e14676a3f9a8b2b8941140', 'create_by', '创建人', NULL, 0, 1, 'string', 50, 0, '', '', '', '', 'text', '', 120, NULL, '0', '', '', 0, 0, 0, 0, 'single', '', '', 2, NULL, NULL, '2019-05-27 18:02:07', 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `onl_cgform_field` VALUES ('509a4f63f02e784bc04499a6a9be8528', 'd35109c3632c4952a19ecc094943dd71', 'update_by', '更新人登录名称', NULL, 0, 1, 'string', 50, 0, '', '', '', '', 'text', '', 120, '', '0', '', '', 0, 0, 0, 0, 'single', '', '', 4, 'admin', '2020-05-06 11:33:01', '2019-03-15 14:24:35', 'admin', '', '', '', '', '', NULL, '0', NULL, NULL, '0');
INSERT INTO `onl_cgform_field` VALUES ('519f68557b953fc2d38400182b187366', '402860816bff91c0016bffa220a9000b', 'residence_type', '户籍类别', 'residence_type', 0, 1, 'string', 100, 0, '', '', '', '', 'text', '', 120, NULL, '0', '', '', 0, 1, 1, 0, 'group', '', '', 13, 'admin', '2019-07-22 16:15:32', '2019-07-17 19:12:24', 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `onl_cgform_field` VALUES ('52ee861bc1b62cd8e4f10632b3d9d1b2', '79091e8277c744158530321513119c68', 'name', '顺序会签标题', NULL, 0, 1, 'String', 32, 0, '', '', '', '', 'text', '', 120, NULL, '0', '', '', 0, 1, 1, 0, 'single', '', '', 6, 'admin', '2019-05-11 15:29:47', '2019-05-11 15:27:17', 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `onl_cgform_field` VALUES ('5531fb261c77e9d12f7cca1af528f70a', '05a3a30dada7411c9109306aa4117068', 'update_time', '更新日期', NULL, 0, 1, 'Date', 20, 0, '', '', '', '', 'datetime', '', 120, NULL, '0', '', '', 0, 0, 0, 0, 'single', '', '', 5, 'admin', '2020-05-12 22:39:41', '2020-05-06 11:34:31', 'admin', '', '', '', '', '', NULL, '0', NULL, NULL, '0');
INSERT INTO `onl_cgform_field` VALUES ('553a250fb89de39e4ba9f8450fd72ade', '05a3a30dada7411c9109306aa4117068', 'update_by', '更新人', NULL, 0, 1, 'string', 50, 0, '', '', '', '', 'text', '', 120, NULL, '0', '', '', 0, 0, 0, 0, 'single', '', '', 4, 'admin', '2020-05-12 22:39:41', '2020-05-06 11:34:31', 'admin', '', '', '', '', '', NULL, '0', NULL, NULL, '0');
INSERT INTO `onl_cgform_field` VALUES ('5562a7242e4978f02e6d3a08d5828bbf', '53a3e82b54b946c2b904f605875a275c', 'create_by', '创建人', NULL, 0, 1, 'string', 50, 0, '', '', '', '', 'text', '', 120, NULL, '0', '', '', 0, 0, 0, 0, 'single', '', '', 2, 'admin', '2020-05-08 23:42:51', '2020-05-07 22:49:47', 'admin', '', '', '', '', '', NULL, '0', NULL, NULL, '0');
INSERT INTO `onl_cgform_field` VALUES ('5562ad3165b1399f97a48f5f06d7fa06', '3b5e18da618247539481816995b6bf9f', 'ccc', 'cc', NULL, 0, 1, 'string', 1000, 0, '', '', '', '', 'umeditor', '', 120, NULL, '0', '', '', 0, 1, 1, 0, 'single', '', '', 10, 'admin', '2020-07-11 11:27:29', '2020-07-11 11:27:17', 'admin', '', '', '', '', '', 'text', '0', NULL, NULL, '0');
INSERT INTO `onl_cgform_field` VALUES ('56a7800e4e476812c74217c2aad781aa', '32feeb502544416c9bf41329c10a88f4', 'update_time', '更新日期', NULL, 0, 1, 'Date', 20, 0, '', '', '', '', 'datetime', '', 120, NULL, '0', '', '', 0, 0, 0, 0, 'single', '', '', 5, 'admin', '2019-08-23 20:03:40', '2019-07-02 18:23:23', 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `onl_cgform_field` VALUES ('56cd0a76f922bf76d982b24a509e4782', '4028839a6de2ebd3016de2ebd3870000', 'create_time', '创建日期', 'create_time', 0, 1, 'Date', 0, 0, NULL, NULL, NULL, NULL, 'date', NULL, 120, NULL, '0', NULL, NULL, 0, 1, 1, 0, 'group', NULL, NULL, 3, NULL, NULL, '2019-10-19 15:29:30', 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `onl_cgform_field` VALUES ('56e247f12d62b49cd9bd537e3efecf16', '402860816bff91c0016bff91c0cb0000', 'create_by', '创建人', 'create_by', 0, 1, 'string', 100, 0, '', '', '', '', 'text', '', 120, NULL, '0', '', '', 0, 1, 1, 0, 'group', '', '', 12, 'admin', '2019-07-19 18:09:01', '2019-07-17 18:54:32', 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `onl_cgform_field` VALUES ('57552a4f0b7b5c096ab8985ced57cc7d', 'cb2d8534a2f544bc9c618dd49da66336', 'update_time', '更新日期', NULL, 0, 1, 'Date', 20, 0, '', '', '', '', 'datetime', '', 120, NULL, '0', '', '', 0, 0, 0, 0, 'single', '', '', 5, 'admin', '2020-02-24 17:22:42', '2020-02-24 15:15:14', 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `onl_cgform_field` VALUES ('581d8e8ce270b762458121b1dea0be9a', '8d66ea41c7cc4ef9ab3aab9055657fc9', 'create_by', '创建人', NULL, 0, 1, 'string', 50, 0, '', '', '', '', 'text', '', 120, NULL, '0', '', '', 0, 0, 0, 0, 'single', '', '', 2, NULL, NULL, '2020-05-07 22:46:32', 'admin', '', '', '', '', '', NULL, '0', NULL, NULL, '0');
INSERT INTO `onl_cgform_field` VALUES ('588400f6ebcdd0bc9bb560dd36636af9', 'e2faf977fdaf4b25a524f58c2441a51c', 'update_by', '更新人', NULL, 0, 1, 'string', 50, 0, '', '', '', '', 'text', '', 120, NULL, '0', '', '', 0, 0, 0, 0, 'single', '', '', 4, 'admin', '2019-06-10 17:27:00', '2019-04-24 17:12:11', 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `onl_cgform_field` VALUES ('58a96f945912d33b64ebf5dee98156dc', '402860816bff91c0016bffa220a9000b', 'mobile', '手机号', 'mobile', 0, 1, 'string', 20, 0, '', '', '', '', 'text', '', 120, NULL, '0', '', '', 0, 1, 1, 0, 'group', '', '', 19, 'admin', '2019-07-22 16:15:32', '2019-07-17 19:12:24', 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `onl_cgform_field` VALUES ('58eea85add4788b83c893092434bc413', 'fb19fb067cd841f9ae93d4eb3b883dc0', 'update_time', '更新日期', NULL, 0, 1, 'date', 20, 0, '', '', '', '', 'text', '', 120, '', '0', '', '', 0, 1, 1, 0, 'single', '', '', 4, NULL, NULL, '2019-03-23 11:39:48', 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `onl_cgform_field` VALUES ('59ae1e853668c676e85329aa029c68a6', '402860816bff91c0016bff91c0cb0000', 'status', '状态（1：申请中 2：通过）', 'status', 0, 1, 'string', 2, 0, '', '', '', '', 'text', '', 120, NULL, '0', '', '', 0, 1, 1, 0, 'group', '', '', 11, 'admin', '2019-07-19 18:09:01', '2019-07-17 18:54:32', 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `onl_cgform_field` VALUES ('5a043c7ae042e48f50d1fb0bb3d760a8', '402880eb71d61d3d0171d61d3de30000', 'code', '编码', NULL, 0, 1, 'String', 200, 0, '', '', '', '', 'text', '', 120, NULL, '0', '', '${order_num_rule_param}', 0, 1, 1, 0, 'single', '', '', 2, 'admin', '2020-05-03 00:54:16', '2020-05-02 23:59:33', 'admin', '', '', '', '', '', NULL, '0', NULL, NULL, '0');
INSERT INTO `onl_cgform_field` VALUES ('5a1ab458d88bb766f92c3d791495cdcd', '402860816bff91c0016bff91d2810005', 'age', '年龄', 'age', 0, 1, 'int', 10, 0, '', '', '', '', 'text', '', 120, NULL, '0', '', '', 0, 1, 1, 0, 'group', '', '', 5, 'admin', '2019-07-19 18:05:55', '2019-07-17 18:54:35', 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `onl_cgform_field` VALUES ('5a4ef083dd572114aeb581b6828de545', '402860816bff91c0016bff91c7010001', 'award_name', '获奖名称', 'award_name', 0, 1, 'string', 255, 0, '', '', '', '', 'text', '', 120, NULL, '0', '', '', 0, 1, 1, 0, 'group', '', '', 5, 'admin', '2019-07-19 18:07:47', '2019-07-17 18:54:32', 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `onl_cgform_field` VALUES ('5a621f27aa443fe9eccc73717e4fa172', '4028318169e81b970169e81b97650000', 'method', '请求java方法', 'method', 0, 1, 'string', 500, 0, NULL, NULL, NULL, NULL, 'text', NULL, 120, NULL, '0', NULL, NULL, 0, 1, 1, 0, 'group', NULL, NULL, 8, NULL, NULL, '2019-04-04 19:28:36', 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `onl_cgform_field` VALUES ('5a655b208d6318ed02f236f15a319b5f', 'fbc35f067da94a70adb622ddba259352', 'update_time', '更新日期', NULL, 0, 1, 'Date', 20, 0, '', '', '', '', 'datetime', '', 120, NULL, '0', '', '', 0, 0, 0, 0, 'single', '', '', 5, NULL, NULL, '2019-07-03 19:44:23', 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `onl_cgform_field` VALUES ('5a6f88710c49bbe8e2e0ca58e149abad', '402860816bff91c0016bff91cda80003', 'create_by', '创建人', 'create_by', 0, 1, 'string', 100, 0, '', '', '', '', 'text', '', 120, NULL, '0', '', '', 0, 1, 1, 0, 'group', '', '', 9, 'admin', '2019-07-19 18:06:36', '2019-07-17 18:54:34', 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `onl_cgform_field` VALUES ('5ab702dbc37d6fd8d3a1093fda7223bf', '53a3e82b54b946c2b904f605875a275c', 'create_time', '创建日期', NULL, 0, 1, 'Date', 20, 0, '', '', '', '', 'datetime', '', 120, NULL, '0', '', '', 0, 0, 0, 0, 'single', '', '', 3, 'admin', '2020-05-08 23:42:51', '2020-05-07 22:49:47', 'admin', '', '', '', '', '', NULL, '0', NULL, NULL, '0');
INSERT INTO `onl_cgform_field` VALUES ('5b17ba693745c258f6b66380ac851e5f', 'd35109c3632c4952a19ecc094943dd71', 'id', '主键', NULL, 1, 0, 'string', 36, 0, '', '', '', '', 'text', '', 120, '', '0', '', '', 0, 1, 1, 0, 'single', '', '', 1, 'admin', '2020-05-06 11:33:01', '2019-03-15 14:24:35', 'admin', '', '', '', '', '', NULL, '0', NULL, NULL, '0');
INSERT INTO `onl_cgform_field` VALUES ('5c76f5ecc774d7339eb0c2199c0052bc', '402860816bff91c0016bff91c0cb0000', 'biz_no', '编号', 'biz_no', 0, 1, 'string', 64, 0, '', '', '', '', 'text', '', 120, NULL, '0', '', '', 0, 1, 1, 0, 'group', '', '', 2, 'admin', '2019-07-19 18:09:01', '2019-07-17 18:54:32', 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `onl_cgform_field` VALUES ('5c8c8d573e01e4f40b5a7c451515e1d2', '32feeb502544416c9bf41329c10a88f4', 'create_time', '创建日期', NULL, 0, 1, 'Date', 20, 0, '', '', '', '', 'datetime', '', 120, NULL, '0', '', '', 0, 0, 0, 0, 'single', '', '', 3, 'admin', '2019-08-23 20:03:40', '2019-07-02 18:23:23', 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `onl_cgform_field` VALUES ('5dfbea516ee2390d712eace5405c5219', '402860816bff91c0016bff91ca7e0002', 'create_by', '创建人', 'create_by', 0, 1, 'string', 100, 0, '', '', '', '', 'text', '', 120, NULL, '0', '', '', 0, 1, 1, 0, 'group', '', '', 9, 'admin', '2019-07-19 18:07:13', '2019-07-17 18:54:33', 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `onl_cgform_field` VALUES ('5e4484b7348dc3e59a0c58bdc3828cc0', '27fc5f91274344afa7673a732b279939', 'update_by', '更新人', NULL, 0, 1, 'string', 50, 0, '', '', '', '', 'text', '', 120, NULL, '0', '', '', 0, 0, 0, 0, 'single', '', '', 4, 'admin', '2019-07-01 16:28:20', '2019-07-01 16:26:42', 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `onl_cgform_field` VALUES ('5e4ac29ac2007ceabf93368330290a42', '402860816bff91c0016bff91d8830007', 'order_no', '序号', 'order_no', 0, 1, 'int', 10, 0, '', '', '', '', 'text', '', 120, NULL, '0', '', '', 0, 1, 1, 0, 'group', '', '', 9, 'admin', '2019-07-19 18:04:41', '2019-07-17 18:54:37', 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `onl_cgform_field` VALUES ('5e729f9823b5cc96c50b0b7c0f07eb05', '402880e5721355dd01721355dd390000', 'create_by', '创建人', NULL, 0, 1, 'string', 50, 0, '', '', '', '', 'text', '', 120, NULL, '0', '', '', 0, 0, 0, 0, 'single', NULL, NULL, 2, NULL, NULL, '2020-05-14 21:18:14', 'admin', '', '', '', '', '', NULL, '0', NULL, NULL, '0');
INSERT INTO `onl_cgform_field` VALUES ('5f13f7087caa0bbf47e5eba01d0d0443', '05a3a30dada7411c9109306aa4117068', 'contents', '请假原因', NULL, 0, 1, 'string', 500, 0, '', '', '', '', 'markdown', '', 120, NULL, '0', '', '', 0, 1, 1, 0, 'single', '', '', 11, 'admin', '2020-05-12 22:39:41', '2020-05-06 11:34:31', 'admin', '', '', '', '', '', 'text', '0', NULL, NULL, '0');
INSERT INTO `onl_cgform_field` VALUES ('5f5ac4d37fd1a3a09e2b19f0d4d99c0f', '4adec929a6594108bef5b35ee9966e9f', 'code', '编码', NULL, 0, 1, 'String', 200, 0, '', '', '', '', 'text', '', 120, NULL, '0', '', '${order_num_rule_param}', 0, 1, 1, 0, 'single', '', '', 2, 'admin', '2020-04-10 19:43:38', '2020-04-10 19:35:58', 'admin', '', '', '', '', '', NULL, '0', NULL, NULL, '0');
INSERT INTO `onl_cgform_field` VALUES ('5f718d8968d908cd2e4de6ee4c74d644', '402880eb71d61d3d0171d61d3de30000', 'create_time', '创建日期', NULL, 0, 1, 'Date', 20, 0, '', '', '', '', 'datetime', '', 120, NULL, '0', '', '', 0, 0, 0, 0, 'single', '', '', 7, 'admin', '2020-05-03 00:54:16', '2020-05-02 23:59:33', 'admin', '', '', '', '', '', 'text', '0', NULL, NULL, '0');
INSERT INTO `onl_cgform_field` VALUES ('6130dbb6d36bab8ee9154e7ab58af34c', '402880e570a5d7000170a5d700f50000', 'id', '主键', NULL, 1, 0, 'string', 36, 0, '', '', '', '', 'text', '', 120, NULL, '0', '', '', 0, 0, 0, 0, 'single', NULL, NULL, 1, NULL, NULL, '2020-03-04 21:58:16', 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `onl_cgform_field` VALUES ('617349b18dab429009ccd304fd7d459c', '4028839a6de2ebd3016de2ebd3870000', 'update_by', '更新人', 'update_by', 0, 1, 'string', 50, 0, NULL, NULL, NULL, NULL, 'text', NULL, 120, NULL, '0', NULL, NULL, 0, 1, 1, 0, 'group', NULL, NULL, 4, NULL, NULL, '2019-10-19 15:29:30', 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `onl_cgform_field` VALUES ('61c7a0058c264dd746eb35e6f50fc15b', '402860816aa5921f016aa5dedcb90009', 'update_time', '更新日期', 'update_time', 0, 1, 'Date', 0, 0, '', '', '', '', 'date', '', 120, NULL, '0', '', '', 0, 0, 0, 0, 'group', '', '', 5, 'admin', '2019-05-11 15:56:47', '2019-05-11 15:50:08', 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `onl_cgform_field` VALUES ('6232ade7e2a0c1e97e2c0945b32e61b6', '402860816bff91c0016bffa220a9000b', 'paying_social_insurance', '是否上社保', 'paying_social_insurance', 0, 1, 'string', 20, 0, '', '', '', '', 'text', '', 120, NULL, '0', '', '', 0, 1, 1, 0, 'group', '', '', 32, 'admin', '2019-07-22 16:15:32', '2019-07-17 19:12:24', 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `onl_cgform_field` VALUES ('6352d477b6b2751780c5852e92d0daa6', '402880eb71d61d3d0171d61d3de30000', 'create_by', '创建人', NULL, 0, 1, 'string', 50, 0, '', '', '', '', 'text', '', 120, NULL, '0', '', '', 0, 0, 0, 0, 'single', '', '', 6, 'admin', '2020-05-03 00:54:16', '2020-05-02 23:59:33', 'admin', '', '', '', '', '', NULL, '0', NULL, NULL, '0');
INSERT INTO `onl_cgform_field` VALUES ('635e09afc01aad757faacf1321465c23', 'b81de38db24047b497d476516f8a0865', 'update_by', '更新人', NULL, 0, 1, 'string', 50, 0, '', '', '', '', 'text', '', 120, NULL, '0', '', '', 0, 0, 0, 0, 'single', '', '', 4, NULL, NULL, '2020-02-24 14:56:08', 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `onl_cgform_field` VALUES ('6490a98dccb6df218feaeb4ce11bc03b', '402860816aa5921f016aa5921f480000', 'update_time', '修改时间', 'update_time', 0, 1, 'Date', 0, 0, '', '', '', '', 'date', '', 120, NULL, '0', '', '', 0, 0, 0, 0, 'group', '', '', 10, 'admin', '2019-05-11 15:31:54', '2019-05-11 14:26:19', 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `onl_cgform_field` VALUES ('654362725195829005036b3db47ec826', '402860816bff91c0016bffa220a9000b', 'post', '职务', 'post', 0, 1, 'string', 100, 0, '', '', '', '', 'text', '', 120, NULL, '0', '', '', 0, 1, 1, 0, 'group', '', '', 4, 'admin', '2019-07-22 16:15:32', '2019-07-17 19:12:24', 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `onl_cgform_field` VALUES ('6603058186832c4ff9e8c6e43baa5c3d', '86bf17839a904636b7ed96201b2fa6ea', 'sys_org_code', '所属部门', NULL, 0, 1, 'string', 64, 0, '', '', '', '', 'text', '', 120, NULL, '0', '', '', 0, 0, 0, 0, 'single', '', '', 6, 'admin', '2020-05-14 21:18:49', '2020-05-08 23:48:31', 'admin', '', '', '', '', '', NULL, '0', NULL, NULL, '0');
INSERT INTO `onl_cgform_field` VALUES ('66a7ef842bc34e105a90186e48167ef2', 'dbf4675875e14676a3f9a8b2b8941140', 'create_time', '创建日期', NULL, 0, 1, 'Date', 20, 0, '', '', '', '', 'datetime', '', 120, NULL, '0', '', '', 0, 0, 0, 0, 'single', '', '', 3, NULL, NULL, '2019-05-27 18:02:07', 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `onl_cgform_field` VALUES ('671e62c685bc86bde3cef0e023418fb4', '05a3a30dada7411c9109306aa4117068', 'sys_org_code', '所属部门', NULL, 0, 1, 'string', 64, 0, '', '', '', '', 'text', '', 120, NULL, '0', '', '', 0, 0, 0, 0, 'single', '', '', 6, 'admin', '2020-05-12 22:39:41', '2020-05-06 11:34:31', 'admin', '', '', '', '', '', NULL, '0', NULL, NULL, '0');
INSERT INTO `onl_cgform_field` VALUES ('686dea2621feadcd9b4c611df046adb4', '86bf17839a904636b7ed96201b2fa6ea', 'price', '价格', NULL, 0, 1, 'double', 10, 3, '', '', '', '', 'text', '', 120, NULL, '0', '', '', 0, 1, 1, 0, 'single', '', '', 8, 'admin', '2020-05-14 21:18:49', '2020-05-08 23:48:31', 'admin', '', '', '', '', '', 'text', '0', NULL, NULL, '0');
INSERT INTO `onl_cgform_field` VALUES ('68769fa7e4696e3a28f4cecf63076b7b', '402860816bff91c0016bff91ca7e0002', 'order_no', '序号', 'order_no', 0, 1, 'int', 10, 0, '', '', '', '', 'text', '', 120, NULL, '0', '', '', 0, 1, 1, 0, 'group', '', '', 8, 'admin', '2019-07-19 18:07:13', '2019-07-17 18:54:33', 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `onl_cgform_field` VALUES ('69666f21896136af16a6303aff440156', '402860816bff91c0016bffa220a9000b', 'nation', '民族', 'nation', 0, 1, 'string', 100, 0, '', '', '', '', 'text', '', 120, NULL, '0', '', '', 0, 1, 1, 0, 'group', '', '', 11, 'admin', '2019-07-22 16:15:32', '2019-07-17 19:12:24', 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `onl_cgform_field` VALUES ('69d11490788fecfc9fb7d74bf449ba86', '32f75e4043ef4070919dbd4337186a3d', 'create_time', '创建日期', NULL, 0, 1, 'Date', 20, 0, '', '', '', '', 'datetime', '', 120, NULL, '0', '', '', 0, 0, 0, 0, 'single', '', '', 2, 'admin', '2019-04-11 10:15:32', '2019-03-27 15:54:49', 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `onl_cgform_field` VALUES ('69e568501d10281d061311d3db15e6af', '18f064d1ef424c93ba7a16148851664f', 'user_sel', '用户', NULL, 0, 1, 'string', 200, 0, '', '', '', '', 'sel_user', '', 120, NULL, '0', '', '#{sysUserCode}', 0, 1, 1, 0, 'single', '', '', 18, 'admin', '2020-05-12 20:30:57', '2020-05-12 20:26:01', 'admin', '', '', '', '', '', 'text', '0', NULL, NULL, '0');
INSERT INTO `onl_cgform_field` VALUES ('6a0082c8ffbae092e99689520f1c83f7', '4fb8e12a697f4d5bbe9b9fb1e9009486', 'update_time', '更新日期', NULL, 0, 1, 'Date', 20, 0, '', '', '', '', 'datetime', '', 120, NULL, '0', '', '', 0, 0, 0, 0, 'single', '', '', 9, 'admin', '2020-04-10 19:51:27', '2020-04-10 19:47:01', 'admin', '', '', '', '', '', 'text', '0', NULL, NULL, '0');
INSERT INTO `onl_cgform_field` VALUES ('6a30c2e6f01ddd24349da55a37025cc0', 'd35109c3632c4952a19ecc094943dd71', 'top_pic', '头像', NULL, 0, 1, 'String', 500, 0, '', '', '', '', 'image', '', 120, NULL, '0', '', '', 0, 1, 1, 0, 'single', '', '', 12, 'admin', '2020-05-06 11:33:01', '2019-06-10 20:06:56', 'admin', '', '', '', '', '', NULL, '0', NULL, NULL, '0');
INSERT INTO `onl_cgform_field` VALUES ('6ab2e3d9b944701b405fb1a5ed167012', '86bf17839a904636b7ed96201b2fa6ea', 'zong_price', '单品总价', NULL, 0, 1, 'double', 10, 3, '', '', '', '', 'text', '', 120, NULL, '0', '', '', 0, 1, 1, 0, 'single', '', '', 10, 'admin', '2020-05-14 21:18:49', '2020-05-08 23:48:31', 'admin', '', '', '', '', '', 'text', '0', NULL, NULL, '0');
INSERT INTO `onl_cgform_field` VALUES ('6b21f5239671023ca769b6717a51130e', '402880eb71d61d3d0171d61d3de30000', 'update_by', '更新人', NULL, 0, 1, 'string', 50, 0, '', '', '', '', 'text', '', 120, NULL, '0', '', '', 0, 0, 0, 0, 'single', '', '', 8, 'admin', '2020-05-03 00:54:16', '2020-05-02 23:59:33', 'admin', '', '', '', '', '', 'text', '0', NULL, NULL, '0');
INSERT INTO `onl_cgform_field` VALUES ('6b6f3aa00b8e73fb785154e795189739', '402860816aa5921f016aa5dedcb90009', 'start_time', '会签发起时间', 'start_time', 0, 1, 'Date', 0, 0, '', '', '', '', 'date', '', 120, NULL, '0', '', '', 0, 1, 1, 0, 'group', '', '', 7, 'admin', '2019-05-11 15:56:47', '2019-05-11 15:50:08', 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `onl_cgform_field` VALUES ('6beb40bdd28af22e06b26640bc0c3f4d', '3b5e18da618247539481816995b6bf9f', 'id', '主键', NULL, 1, 0, 'string', 36, 0, '', '', '', '', 'text', '', 120, NULL, '0', '', '', 0, 0, 0, 1, 'single', '', '', 1, 'admin', '2020-07-11 11:27:29', '2020-07-11 11:27:17', 'admin', '', '', '', '', '', NULL, '0', NULL, NULL, '0');
INSERT INTO `onl_cgform_field` VALUES ('6c35eb97737e9f86279939d264454a94', '86bf17839a904636b7ed96201b2fa6ea', 'update_time', '更新日期', NULL, 0, 1, 'Date', 20, 0, '', '', '', '', 'datetime', '', 120, NULL, '0', '', '', 0, 0, 0, 0, 'single', '', '', 5, 'admin', '2020-05-14 21:18:49', '2020-05-08 23:48:31', 'admin', '', '', '', '', '', NULL, '0', NULL, NULL, '0');
INSERT INTO `onl_cgform_field` VALUES ('6cfb5acbbb69782bf0c7043b53f595b2', '402860816bff91c0016bff91cda80003', 'update_by', '更新人', 'update_by', 0, 1, 'string', 100, 0, '', '', '', '', 'text', '', 120, NULL, '0', '', '', 0, 1, 1, 0, 'group', '', '', 11, 'admin', '2019-07-19 18:06:36', '2019-07-17 18:54:34', 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `onl_cgform_field` VALUES ('6d03a7774a50c6e6f76dbc7a8269beba', '3b5e18da618247539481816995b6bf9f', 'create_by', '创建人', NULL, 0, 1, 'string', 50, 0, '', '', '', '', 'text', '', 120, NULL, '0', '', '', 0, 0, 0, 0, 'single', '', '', 2, 'admin', '2020-07-11 11:27:29', '2020-07-11 11:27:17', 'admin', '', '', '', '', '', NULL, '0', NULL, NULL, '0');
INSERT INTO `onl_cgform_field` VALUES ('6d4a4414b55ad5b6f31c8fb327dad834', '09fd28e4b7184c1a9668496a5c496450', 'address', '常用地址', NULL, 0, 1, 'string', 300, 0, '', '', '', '', 'text', '', 120, NULL, '0', '', '', 0, 1, 1, 0, 'single', '', '', 11, 'admin', '2020-05-14 21:19:21', '2020-05-08 23:51:49', 'admin', '', '', '', '', '', 'text', '0', NULL, NULL, '0');
INSERT INTO `onl_cgform_field` VALUES ('6e5c9d8e8b7eb6980ec246cb813b180b', '4fb8e12a697f4d5bbe9b9fb1e9009486', 'sys_org_code', '所属部门', NULL, 0, 1, 'string', 64, 0, '', '', '', '', 'text', '', 120, NULL, '0', '', '', 0, 0, 0, 0, 'single', '', '', 10, 'admin', '2020-04-10 19:51:27', '2020-04-10 19:47:01', 'admin', '', '', '', '', '', 'text', '0', NULL, NULL, '0');
INSERT INTO `onl_cgform_field` VALUES ('6ed8d9acef3cccd1b8fd659b3b538023', '53a3e82b54b946c2b904f605875a275c', 'price', '价格', NULL, 0, 1, 'BigDecimal', 10, 5, '', '', '', '', 'text', '', 120, NULL, '0', '', '', 0, 1, 1, 0, 'single', '', '', 8, 'admin', '2020-05-08 23:42:51', '2020-05-07 22:49:47', 'admin', '', '', '', '', '', 'text', '0', NULL, NULL, '0');
INSERT INTO `onl_cgform_field` VALUES ('6f38a07ea2b1065315a6b89a02af5bb4', '3b5e18da618247539481816995b6bf9f', 'name', '用户名', NULL, 0, 1, 'string', 32, 0, '', '', '', '', 'text', '', 120, NULL, '0', '', '', 0, 1, 1, 0, 'single', '', '', 7, 'admin', '2020-07-11 11:27:29', '2020-07-11 11:27:17', 'admin', '', '', '', '', '', 'text', '0', NULL, NULL, '0');
INSERT INTO `onl_cgform_field` VALUES ('6f73e96a659c200c083006b6fce1f043', '402860816bff91c0016bff91ca7e0002', 'cert_name', '证书名称', 'cert_name', 0, 1, 'string', 100, 0, '', '', '', '', 'text', '', 120, NULL, '0', '', '', 0, 1, 1, 0, 'group', '', '', 4, 'admin', '2019-07-19 18:07:13', '2019-07-17 18:54:33', 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `onl_cgform_field` VALUES ('7154c75d754a5f88bef2b68829baf576', '4028318169e81b970169e81b97650000', 'operate_type', '操作类型', 'operate_type', 0, 1, 'string', 10, 0, NULL, NULL, NULL, NULL, 'text', NULL, 120, NULL, '0', NULL, NULL, 0, 1, 1, 0, 'group', NULL, NULL, 4, NULL, NULL, '2019-04-04 19:28:36', 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `onl_cgform_field` VALUES ('71afb00a1971125ecfa13b4dfa49665e', '402860816bff91c0016bff91cfea0004', 'order_no', '序号', 'order_no', 0, 1, 'int', 10, 0, '', '', '', '', 'text', '', 120, NULL, '0', '', '', 0, 1, 1, 0, 'group', '', '', 8, 'admin', '2019-07-19 18:05:13', '2019-07-17 18:54:35', 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `onl_cgform_field` VALUES ('71d5b0675df5aba71688c9d7d75cccee', '4028318169e81b970169e81b97650000', 'log_type', '日志类型（1登录日志，2操作日志）', 'log_type', 0, 1, 'string', 10, 0, NULL, NULL, NULL, NULL, 'text', NULL, 120, NULL, '0', NULL, NULL, 0, 1, 1, 0, 'group', NULL, NULL, 2, NULL, NULL, '2019-04-04 19:28:36', 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `onl_cgform_field` VALUES ('71e9ab74dae687837365e50eed090591', '1acb6f81a1d9439da6cc4e868617b565', 'id', '主键', NULL, 1, 0, 'string', 36, 0, '', '', '', '', 'text', '', 120, NULL, '0', '', '', 0, 0, 0, 0, 'single', '', '', 1, 'admin', '2019-06-10 14:47:14', '2019-04-23 22:58:19', 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `onl_cgform_field` VALUES ('7280c56a210e6a47794fda855d0c6abb', 'fbc35f067da94a70adb622ddba259352', 'update_by', '更新人', NULL, 0, 1, 'string', 50, 0, '', '', '', '', 'text', '', 120, NULL, '0', '', '', 0, 0, 0, 0, 'single', '', '', 4, NULL, NULL, '2019-07-03 19:44:23', 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `onl_cgform_field` VALUES ('72e784af5c47bbbc0534b29bf656bd61', '4028839a6de2ebd3016de2ebd3870000', 'id', '主键', 'id', 1, 0, 'string', 36, 0, NULL, NULL, NULL, NULL, 'text', NULL, 120, NULL, '0', NULL, NULL, 0, 0, 0, 0, 'group', NULL, NULL, 1, NULL, NULL, '2019-10-19 15:29:30', 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `onl_cgform_field` VALUES ('7365f05f551092716223d5d449efd8c7', 'beee191324fd40c1afec4fda18bd9d47', 'name', 'ss', NULL, 0, 1, 'String', 32, 0, '', '', '', '', 'text', '', 120, NULL, '0', '', '', 0, 1, 1, 0, 'single', '', '', 6, 'admin', '2019-04-13 13:41:13', '2019-04-13 13:40:56', 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `onl_cgform_field` VALUES ('73d3b57df0c6cf15c21970f4dd979319', '402880e5721355dd01721355dd390000', 'update_time', '更新日期', NULL, 0, 1, 'Date', 20, 0, '', '', '', '', 'datetime', '', 120, NULL, '0', '', '', 0, 0, 0, 0, 'single', NULL, NULL, 5, NULL, NULL, '2020-05-14 21:18:14', 'admin', '', '', '', '', '', NULL, '0', NULL, NULL, '0');
INSERT INTO `onl_cgform_field` VALUES ('73dc6089556f3446e39d42df3dedb2db', '402880e570a5d7000170a5d700f50000', 'num', '数量', NULL, 0, 1, 'int', 32, 0, '', '', '', '', 'text', '', 120, 'n', '0', '', '', 0, 1, 1, 0, 'single', NULL, NULL, 8, NULL, NULL, '2020-03-04 21:58:16', 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `onl_cgform_field` VALUES ('742329ccdb185cf5d3e0b5b0c05dcffa', '402860816bff91c0016bffa220a9000b', 'interest', '兴趣爱好', 'interest', 0, 1, 'string', 255, 0, '', '', '', '', 'text', '', 120, NULL, '0', '', '', 0, 1, 1, 0, 'group', '', '', 34, 'admin', '2019-07-22 16:15:32', '2019-07-17 19:12:24', 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `onl_cgform_field` VALUES ('744444a7ada3bbb05c6b114b5ba0d477', '402860816aa5921f016aa5dedcb90009', 'id', 'id', 'id', 1, 0, 'string', 36, 0, '', '', '', '', 'text', '', 120, NULL, '0', '', '', 0, 0, 0, 0, 'group', '', '', 1, 'admin', '2019-05-11 15:56:47', '2019-05-11 15:50:08', 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `onl_cgform_field` VALUES ('74af99545de724a4abd2022581a36026', 'fb7125a344a649b990c12949945cb6c1', 'create_time', '创建日期', NULL, 0, 1, 'Date', 20, 0, '', '', '', '', 'text', '', 120, '', '0', '', '', 0, 0, 0, 0, 'single', '', '', 2, 'admin', '2019-03-26 19:24:11', '2019-03-26 19:01:52', 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `onl_cgform_field` VALUES ('756b07656386dbd91245f7ffda32ae61', '402860816bff91c0016bff91d8830007', 'id', 'id', 'id', 1, 1, 'string', 32, 0, '', '', '', '', 'text', '', 120, NULL, '0', '', '', 0, 0, 0, 0, 'group', '', '', 1, 'admin', '2019-07-19 18:04:41', '2019-07-17 18:54:37', 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `onl_cgform_field` VALUES ('75841fa7c75ebdc94655bd5e44fbc9f6', '402860816bff91c0016bffa220a9000b', 'native_place', '籍贯', 'native_place', 0, 1, 'string', 100, 0, '', '', '', '', 'text', '', 120, NULL, '0', '', '', 0, 1, 1, 0, 'group', '', '', 10, 'admin', '2019-07-22 16:15:32', '2019-07-17 19:12:24', 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `onl_cgform_field` VALUES ('75ba781c67711bed71bba1c3e3c68e11', '8994f2817b5a45d9890aa04497a317c5', 'id', '主键', NULL, 1, 0, 'string', 36, 0, '', '', '', '', 'text', '', 120, '', '0', '', '', 0, 1, 1, 0, 'single', '', '', 0, NULL, NULL, '2019-03-23 11:39:16', 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `onl_cgform_field` VALUES ('75e82f151e8cc12455f7f0d25bf4dac0', '4028839a6de2ebd3016de2ebd3870000', 'wl_name', '物料名称', 'wl_name', 0, 1, 'string', 100, 0, NULL, NULL, NULL, NULL, 'text', NULL, 120, NULL, '0', NULL, NULL, 0, 1, 1, 0, 'group', NULL, NULL, 7, NULL, NULL, '2019-10-19 15:29:30', 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `onl_cgform_field` VALUES ('7639c1bc4327f1f674ffeab2ca261134', '32f75e4043ef4070919dbd4337186a3d', 'update_by', '更新人登录名称', NULL, 0, 1, 'string', 50, 0, '', '', '', '', 'text', '', 120, NULL, '0', '', '', 0, 0, 0, 0, 'single', '', '', 3, 'admin', '2019-04-11 10:15:32', '2019-03-27 15:54:49', 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `onl_cgform_field` VALUES ('766ca866b72d118f5d8883de46a8c043', '4028839a6de2ebd3016de2ebd3870000', 'supplier', '供应商', 'supplier', 0, 1, 'string', 32, 0, NULL, NULL, NULL, NULL, 'text', NULL, 120, NULL, '0', NULL, NULL, 0, 1, 1, 0, 'group', NULL, NULL, 15, NULL, NULL, '2019-10-19 15:29:30', 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `onl_cgform_field` VALUES ('78a40344207c791b8d7ac7de721ce1c4', '79091e8277c744158530321513119c68', 'create_time', '创建日期', NULL, 0, 1, 'Date', 20, 0, '', '', '', '', 'datetime', '', 120, NULL, '0', '', '', 0, 0, 0, 0, 'single', '', '', 3, 'admin', '2019-05-11 15:29:47', '2019-05-11 15:27:17', 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `onl_cgform_field` VALUES ('78eb7e3b77cd49f9acb9b024cfe834e1', '402860816aa5921f016aa5dedcb90009', 'create_time', '创建日期', 'create_time', 0, 1, 'Date', 0, 0, '', '', '', '', 'date', '', 120, NULL, '0', '', '', 0, 0, 0, 0, 'group', '', '', 3, 'admin', '2019-05-11 15:56:47', '2019-05-11 15:50:08', 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `onl_cgform_field` VALUES ('78fd804d93dc716fd8c2ccc45f788565', 'fb7125a344a649b990c12949945cb6c1', 'create_by', '创建人', NULL, 0, 1, 'string', 50, 0, '', '', '', '', 'text', '', 120, '', '0', '', '', 0, 0, 0, 0, 'single', '', '', 1, 'admin', '2019-03-26 19:24:11', '2019-03-26 19:01:52', 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `onl_cgform_field` VALUES ('790c9f3dba773ed9a6ea3ad627393f57', '402860816bff91c0016bffa220a9000b', 'archives_location', '档案所在地', 'archives_location', 0, 1, 'string', 255, 0, '', '', '', '', 'text', '', 120, NULL, '0', '', '', 0, 1, 1, 0, 'group', '', '', 36, 'admin', '2019-07-22 16:15:32', '2019-07-17 19:12:24', 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `onl_cgform_field` VALUES ('7a665ed90ef64b4d65632c941e5795b2', '4b556f0168f64976a3d20bfb932bc798', 'sex', '性别', NULL, 0, 1, 'String', 32, 0, '', '', '', '', 'text', '', 120, NULL, '0', '', '', 0, 1, 1, 0, 'single', '', '', 7, NULL, NULL, '2019-04-12 23:38:29', 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `onl_cgform_field` VALUES ('7a697e4053445f07ad1a56a246f593e7', '86bf17839a904636b7ed96201b2fa6ea', 'good_name', '商品名字', NULL, 0, 1, 'string', 32, 0, '', '', '', '', 'text', '', 120, NULL, '0', '', '', 0, 1, 1, 0, 'single', '', '', 7, 'admin', '2020-05-14 21:18:49', '2020-05-08 23:48:31', 'admin', '', '', '', '', '', 'text', '0', NULL, NULL, '0');
INSERT INTO `onl_cgform_field` VALUES ('7b4c181e4ebd76022f75535ed6fd9de3', '4adec929a6594108bef5b35ee9966e9f', 'update_time', '更新日期', NULL, 0, 1, 'Date', 20, 0, '', '', '', '', 'datetime', '', 120, NULL, '0', '', '', 0, 0, 0, 0, 'single', '', '', 10, 'admin', '2020-04-10 19:43:38', '2020-04-10 19:35:58', 'admin', '', '', '', '', '', 'text', '0', NULL, NULL, '0');
INSERT INTO `onl_cgform_field` VALUES ('7b642d983ac06bfef91edde2c932dbe7', '1acb6f81a1d9439da6cc4e868617b565', 'xg_shangxian', '选购上限', NULL, 0, 1, 'int', 32, 0, '', '', '', '', 'text', '', 120, NULL, '0', '', '', 0, 1, 1, 0, 'single', '', '', 14, 'admin', '2019-06-10 14:47:14', '2019-04-23 22:58:19', 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `onl_cgform_field` VALUES ('7b849e57debfb889caea5e0fef09062b', 'beee191324fd40c1afec4fda18bd9d47', 'sex2', 'dd', NULL, 0, 1, 'String', 32, 0, '', '', '', '', 'text', '', 120, NULL, '0', '', '', 0, 1, 1, 0, 'single', '', '', 7, 'admin', '2019-04-13 13:41:13', '2019-04-13 13:40:56', 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `onl_cgform_field` VALUES ('7beb639aa9ffda07edb5ce1e49c2287f', '402860816bff91c0016bff91d2810005', 'update_time', '更新时间', 'update_time', 0, 1, 'Date', 0, 0, '', '', '', '', 'date', '', 120, NULL, '0', '', '', 0, 1, 1, 0, 'group', '', '', 13, 'admin', '2019-07-19 18:05:55', '2019-07-17 18:54:35', 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `onl_cgform_field` VALUES ('7bf091da401b74d55b107fe9f930ea54', '4028839a6de2ebd3016de2ebd3870000', 'create_by', '创建人', 'create_by', 0, 1, 'string', 50, 0, NULL, NULL, NULL, NULL, 'text', NULL, 120, NULL, '0', NULL, NULL, 0, 1, 1, 0, 'group', NULL, NULL, 2, NULL, NULL, '2019-10-19 15:29:30', 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `onl_cgform_field` VALUES ('7c6aecc377816c69147f1193b17dfcc5', 'e9faf717024b4aae95cff224ae9b6d97', 'sex', '员工性别', NULL, 0, 1, 'string', 32, 0, '', '', '', '', 'text', '', 120, NULL, '0', '', '', 0, 1, 1, 0, 'single', '', '', 8, 'admin', '2019-07-03 18:23:49', '2019-07-03 18:22:35', 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `onl_cgform_field` VALUES ('7cadf4e0be0b173bb5b8d39613e25190', '402860816bff91c0016bffa220a9000b', 'residence_postcode', '户籍邮编', 'residence_postcode', 0, 1, 'string', 10, 0, '', '', '', '', 'text', '', 120, NULL, '0', '', '', 0, 1, 1, 0, 'group', '', '', 29, 'admin', '2019-07-22 16:15:32', '2019-07-17 19:12:24', 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `onl_cgform_field` VALUES ('7cdbe0d563e15f3fb1fb32d996fe4ba7', '3d447fa919b64f6883a834036c14aa67', 'area', '区', NULL, 0, 1, 'String', 100, 0, '', '', '', '', 'text', '', 120, NULL, '0', '', '', 0, 1, 1, 0, 'single', '', '', 4, 'admin', '2020-02-21 17:58:46', '2020-02-20 16:19:00', 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `onl_cgform_field` VALUES ('7d107728408c21ece332406a21f2d692', '402860816bff91c0016bff91cfea0004', 'update_by', '更新人', 'update_by', 0, 1, 'string', 100, 0, '', '', '', '', 'text', '', 120, NULL, '0', '', '', 0, 1, 1, 0, 'group', '', '', 11, 'admin', '2019-07-19 18:05:13', '2019-07-17 18:54:35', 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `onl_cgform_field` VALUES ('7e066f60680158d47b328ef519d80e49', 'beee191324fd40c1afec4fda18bd9d47', 'create_time', '创建日期', NULL, 0, 1, 'Date', 20, 0, '', '', '', '', 'datetime', '', 120, NULL, '0', '', '', 0, 0, 0, 0, 'single', '', '', 3, 'admin', '2019-04-13 13:41:13', '2019-04-13 13:40:56', 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `onl_cgform_field` VALUES ('7f10901c6ade3aa9d9ff46ed7039c70f', '1acb6f81a1d9439da6cc4e868617b565', 'create_time', '创建日期', NULL, 0, 1, 'Date', 20, 0, '', '', '', '', 'datetime', '', 120, NULL, '0', '', '', 0, 0, 0, 0, 'single', '', '', 3, 'admin', '2019-06-10 14:47:14', '2019-04-23 22:58:19', 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `onl_cgform_field` VALUES ('809a9fafe0b79c9997de32cb6e831c6f', '05a3a30dada7411c9109306aa4117068', 'birthday', '生日', NULL, 0, 1, 'Date', 0, 0, '', '', '', '', 'date', '', 120, NULL, '0', '', '', 0, 1, 1, 0, 'single', '', '', 10, 'admin', '2020-05-12 22:39:41', '2020-05-06 11:34:31', 'admin', '', '', '', '', '', 'text', '0', NULL, NULL, '0');
INSERT INTO `onl_cgform_field` VALUES ('8119d3de514092232935b038531828c5', '05a3a30dada7411c9109306aa4117068', 'sex', '性别', NULL, 0, 1, 'string', 32, 0, '', 'sex', '', '', 'list', '', 120, NULL, '0', '', '', 1, 1, 1, 0, 'single', '', '', 9, 'admin', '2020-05-12 22:39:41', '2020-05-06 11:34:31', 'admin', '', '', '', '', '', 'text', '0', NULL, NULL, '0');
INSERT INTO `onl_cgform_field` VALUES ('813a5ebf7335309c7edb3803f7e4b204', '402880e570a5d7000170a5d700f50000', 'create_by', '创建人', NULL, 0, 1, 'string', 50, 0, '', '', '', '', 'text', '', 120, NULL, '0', '', '', 0, 0, 0, 0, 'single', NULL, NULL, 2, NULL, NULL, '2020-03-04 21:58:16', 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `onl_cgform_field` VALUES ('817cc5e277e88164dffd4caee1169276', '56efb74326e74064b60933f6f8af30ea', 'remark', '备注', NULL, 0, 1, 'string', 500, 0, '', '', '', '', 'text', '', 120, NULL, '0', '', '', 0, 1, 1, 0, 'single', '', '', 10, 'admin', '2020-07-10 16:53:27', '2020-05-08 23:45:32', 'admin', '', '', '', '', '', 'text', '0', NULL, NULL, '0');
INSERT INTO `onl_cgform_field` VALUES ('81ed9556c9fda1bbb46d94a53a6c90c7', '402860816bff91c0016bff91c0cb0000', 'depart_name', '部门名称', 'depart', 0, 1, 'string', 100, 0, '', '', '', '', 'text', '', 120, NULL, '0', '', '', 0, 1, 1, 0, 'group', '', '', 7, 'admin', '2019-07-19 18:09:01', '2019-07-17 18:54:32', 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `onl_cgform_field` VALUES ('840343a59a8e705821d393506c2bc1fe', '402880e570a5d7000170a5d700f50000', 'create_time', '创建日期', NULL, 0, 1, 'Date', 20, 0, '', '', '', '', 'datetime', '', 120, NULL, '0', '', '', 0, 0, 0, 0, 'single', NULL, NULL, 3, NULL, NULL, '2020-03-04 21:58:16', 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `onl_cgform_field` VALUES ('8422485e1cbf4455f9ded7d0af59379c', '402860816bff91c0016bff91cfea0004', 'to_time', '截止时间', 'to_time', 0, 1, 'Date', 0, 0, '', '', '', '', 'date', '', 120, NULL, '0', '', '', 0, 1, 1, 0, 'group', '', '', 4, 'admin', '2019-07-19 18:05:13', '2019-07-17 18:54:35', 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `onl_cgform_field` VALUES ('845c70568d44a074f067d6d277950525', '402860816bff91c0016bffa220a9000b', 'entrytime', '入职时间', 'entrytime', 0, 1, 'Date', 0, 0, '', '', '', '', 'date', '', 120, NULL, '0', '', '', 0, 1, 1, 0, 'group', '', '', 23, 'admin', '2019-07-22 16:15:32', '2019-07-17 19:12:24', 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `onl_cgform_field` VALUES ('8529ddaed8d5f3d9084e873203d55cac', '402860816bff91c0016bffa220a9000b', 'marital_status', '婚姻状况', 'marital_status', 0, 1, 'string', 20, 0, '', '', '', '', 'text', '', 120, NULL, '0', '', '', 0, 1, 1, 0, 'group', '', '', 24, 'admin', '2019-07-22 16:15:32', '2019-07-17 19:12:24', 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `onl_cgform_field` VALUES ('857a0daa9cd8a058f2f15fc7c5fb3571', '402860816bff91c0016bffa220a9000b', 'email', '邮箱', 'email', 0, 1, 'string', 255, 0, '', '', '', '', 'text', '', 120, NULL, '0', '', '', 0, 1, 1, 0, 'group', '', '', 17, 'admin', '2019-07-22 16:15:32', '2019-07-17 19:12:24', 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `onl_cgform_field` VALUES ('85e43fa87074845f86cf52606a23a57c', 'b81de38db24047b497d476516f8a0865', 'update_time', '更新日期', NULL, 0, 1, 'Date', 20, 0, '', '', '', '', 'datetime', '', 120, NULL, '0', '', '', 0, 0, 0, 0, 'single', '', '', 5, NULL, NULL, '2020-02-24 14:56:08', 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `onl_cgform_field` VALUES ('8652ca69a947fd4c961a3ac7c0fa252a', 'fb19fb067cd841f9ae93d4eb3b883dc0', 'create_by', '创建人登录名称', NULL, 0, 1, 'string', 50, 0, '', '', '', '', 'text', '', 120, '', '0', '', '', 0, 1, 1, 0, 'single', '', '', 1, NULL, NULL, '2019-03-23 11:39:48', 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `onl_cgform_field` VALUES ('86bbafef5683674a736cf7241c458d44', '27fc5f91274344afa7673a732b279939', 'create_by', '创建人', NULL, 0, 1, 'string', 50, 0, '', '', '', '', 'text', '', 120, NULL, '0', '', '', 0, 0, 0, 0, 'single', '', '', 2, 'admin', '2019-07-01 16:28:20', '2019-07-01 16:26:42', 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `onl_cgform_field` VALUES ('86e0f3a8f31c60698157f139ed993954', '402860816bff91c0016bffa220a9000b', 'having_reserve_funds', '是否有公积金', 'having_reserve_funds', 0, 1, 'string', 20, 0, '', '', '', '', 'text', '', 120, NULL, '0', '', '', 0, 1, 1, 0, 'group', '', '', 33, 'admin', '2019-07-22 16:15:32', '2019-07-17 19:12:24', 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `onl_cgform_field` VALUES ('86f29e9919766e0d1128263608c016a0', '997ee931515a4620bc30a9c1246429a9', 'type_name', '商品分类', NULL, 0, 1, 'string', 32, 0, '', '', '', '', 'text', '', 120, NULL, '0', '', '', 0, 1, 1, 0, 'single', '', '', 7, 'admin', '2020-05-03 00:57:44', '2020-05-03 00:56:56', 'admin', '', '', '', '', '', 'text', '0', NULL, NULL, '0');
INSERT INTO `onl_cgform_field` VALUES ('873e2bb041b17bff77d3aca72900ea1b', '56870166aba54ebfacb20ba6c770bd73', 'order_code', '订单编码', NULL, 0, 1, 'String', 32, 0, '', '', '', '', 'text', '', 120, NULL, '0', '', '', 0, 1, 1, 0, 'single', '', '', 6, 'admin', '2020-07-10 17:09:01', '2019-04-20 11:38:39', 'admin', '', '', '', '', '', NULL, '0', NULL, NULL, '0');
INSERT INTO `onl_cgform_field` VALUES ('8756fbb5c23a0258e029e0cb3c0a045c', '402880e5721355dd01721355dd390000', 'price', '价格', NULL, 0, 1, 'double', 10, 3, '', '', '', '', 'text', '', 120, NULL, '0', '', '', 0, 1, 1, 0, 'single', NULL, NULL, 8, NULL, NULL, '2020-05-14 21:18:14', 'admin', '', '', '', '', '', 'text', '0', NULL, NULL, '0');
INSERT INTO `onl_cgform_field` VALUES ('877391ae770a4ce2c95181ef410982ce', '4028318169e81b970169e81b97650000', 'request_param', '请求参数', 'request_param', 0, 1, 'string', 255, 0, NULL, NULL, NULL, NULL, 'text', NULL, 120, NULL, '0', NULL, NULL, 0, 1, 1, 0, 'group', NULL, NULL, 10, NULL, NULL, '2019-04-04 19:28:36', 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `onl_cgform_field` VALUES ('87e82e3c723a6abb020122babdac6bd1', '8994f2817b5a45d9890aa04497a317c5', 'create_by', '创建人登录名称', NULL, 0, 1, 'string', 50, 0, '', '', '', '', 'text', '', 120, '', '0', '', '', 0, 1, 1, 0, 'single', '', '', 1, NULL, NULL, '2019-03-23 11:39:16', 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `onl_cgform_field` VALUES ('87f7a2703c5850f0b063dd866d0e2917', '402860816bff91c0016bffa220a9000b', 'birthday', '出生日期', 'birthday', 0, 1, 'Date', 0, 0, '', '', '', '', 'date', '', 120, NULL, '0', '', '', 0, 1, 1, 0, 'group', '', '', 7, 'admin', '2019-07-22 16:15:32', '2019-07-17 19:12:24', 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `onl_cgform_field` VALUES ('87fafe1a4a8a626e3875697574c19f15', '402860816bff91c0016bff91d2810005', 'sys_org_code', '组织机构编码', 'sys_org_code', 0, 1, 'string', 50, 0, '', '', '', '', 'text', '', 120, NULL, '0', '', '', 0, 1, 1, 0, 'group', '', '', 14, 'admin', '2019-07-19 18:05:55', '2019-07-17 18:54:35', 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `onl_cgform_field` VALUES ('883265736ad6db0c98a7517d1f4a2488', '402880eb71d52dc30171d52dc3a10000', 'main_id', '主表ID', NULL, 0, 1, 'String', 200, 0, '', '', '', '', 'text', '', 120, NULL, '0', '', '', 0, 0, 0, 0, 'single', NULL, NULL, 5, NULL, NULL, '2020-05-02 19:37:59', 'admin', '', '', '', '', '', NULL, '0', NULL, NULL, '0');
INSERT INTO `onl_cgform_field` VALUES ('88a12570e14c9f6f442e731ae5ad0eb1', 'beee191324fd40c1afec4fda18bd9d47', 'update_by', '更新人', NULL, 0, 1, 'string', 50, 0, '', '', '', '', 'text', '', 120, NULL, '0', '', '', 0, 0, 0, 0, 'single', '', '', 4, 'admin', '2019-04-13 13:41:13', '2019-04-13 13:40:56', 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `onl_cgform_field` VALUES ('89370ae67e241fa5d1e47d22adeaca7b', '402880eb71d52dc30171d52dc3a10000', 'date', '日期', NULL, 0, 1, 'string', 200, 0, '', '', '', '', 'text', '', 120, NULL, '0', '', '#{date}', 0, 1, 1, 0, 'single', NULL, NULL, 4, NULL, NULL, '2020-05-02 19:37:58', 'admin', '', '', '', '', '', NULL, '0', NULL, NULL, '0');
INSERT INTO `onl_cgform_field` VALUES ('89ab9eedbac6141e7a0df6d37a3655d0', 'e67d26b610dd414c884c4dbb24e71ce3', 'update_by', '更新人', NULL, 0, 1, 'string', 50, 0, '', '', '', '', 'text', '', 120, NULL, '0', '', '', 0, 0, 0, 0, 'single', '', '', 4, 'admin', '2019-04-24 11:03:32', '2019-04-24 11:02:57', 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `onl_cgform_field` VALUES ('8a24fb45e2af120c253c8b61c0085f7a', '402860816bff91c0016bff91cda80003', 'sys_org_code', '组织机构编码', 'sys_org_code', 0, 1, 'string', 50, 0, '', '', '', '', 'text', '', 120, NULL, '0', '', '', 0, 1, 1, 0, 'group', '', '', 13, 'admin', '2019-07-19 18:06:36', '2019-07-17 18:54:34', 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `onl_cgform_field` VALUES ('8a2f97fde739720e86fbcd3ce311c037', '09fd28e4b7184c1a9668496a5c496450', 'sys_org_code', '所属部门', NULL, 0, 1, 'string', 64, 0, '', '', '', '', 'text', '', 120, NULL, '0', '', '', 0, 0, 0, 0, 'single', '', '', 6, 'admin', '2020-05-14 21:19:21', '2020-05-08 23:51:49', 'admin', '', '', '', '', '', NULL, '0', NULL, NULL, '0');
INSERT INTO `onl_cgform_field` VALUES ('8a6440c447ca97b1ceac40fa8576044e', '3b5e18da618247539481816995b6bf9f', 'sys_org_code', '所属部门', NULL, 0, 1, 'string', 64, 0, '', '', '', '', 'text', '', 120, NULL, '0', '', '', 0, 0, 0, 0, 'single', '', '', 6, 'admin', '2020-07-11 11:27:29', '2020-07-11 11:27:17', 'admin', '', '', '', '', '', NULL, '0', NULL, NULL, '0');
INSERT INTO `onl_cgform_field` VALUES ('8ac8a0c0087469a4e7579229ff17f273', 'e5464aa8fa7b47c580e91593cf9b46dc', 'jifen', '合计积分', NULL, 0, 1, 'int', 32, 0, '', '', '', '', 'text', '', 120, NULL, '0', '', '', 0, 1, 1, 0, 'single', '', '', 10, 'admin', '2019-04-24 17:09:49', '2019-04-24 11:05:10', 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `onl_cgform_field` VALUES ('8bd4deadc9e96c1a6d7abd77033105f6', 'e67d26b610dd414c884c4dbb24e71ce3', 'update_time', '更新日期', NULL, 0, 1, 'Date', 20, 0, '', '', '', '', 'datetime', '', 120, NULL, '0', '', '', 0, 0, 0, 0, 'single', '', '', 5, 'admin', '2019-04-24 11:03:32', '2019-04-24 11:02:57', 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `onl_cgform_field` VALUES ('8c6518fec11fc4769ba4eb770c9e00f7', '4028839a6de2ebd3016de2ebd3870000', 'integral_val', '积分值', 'integral_val', 0, 1, 'int', 10, 0, NULL, NULL, NULL, NULL, 'text', NULL, 120, NULL, '0', NULL, NULL, 0, 1, 1, 0, 'group', NULL, NULL, 11, NULL, NULL, '2019-10-19 15:29:30', 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `onl_cgform_field` VALUES ('8c960ed82ba23e19580307c0e9434de0', '18f064d1ef424c93ba7a16148851664f', 'update_by', '更新人', NULL, 0, 1, 'string', 50, 0, '', '', '', '', 'text', '', 120, NULL, '0', '', '', 0, 0, 0, 0, 'single', '', '', 4, 'admin', '2020-05-12 20:30:57', '2020-05-12 20:26:01', 'admin', '', '', '', '', '', NULL, '0', NULL, NULL, '0');
INSERT INTO `onl_cgform_field` VALUES ('8ca56210938fbe649f840e505eb9fd41', '56870166aba54ebfacb20ba6c770bd73', 'create_by', '创建人', NULL, 0, 1, 'string', 50, 0, '', '', '', '', 'text', '', 120, NULL, '0', '', '', 0, 0, 0, 0, 'single', '', '', 2, 'admin', '2020-07-10 17:09:01', '2019-04-20 11:38:39', 'admin', '', '', '', '', '', NULL, '0', NULL, NULL, '0');
INSERT INTO `onl_cgform_field` VALUES ('8e080f4ded1e3b2a1daa5b11eca4a0ff', '4adec929a6594108bef5b35ee9966e9f', 'create_by', '创建人', NULL, 0, 1, 'string', 50, 0, '', '', '', '', 'text', '', 120, NULL, '0', '', '', 0, 0, 0, 0, 'single', '', '', 7, 'admin', '2020-04-10 19:43:38', '2020-04-10 19:35:58', 'admin', '', '', '', '', '', 'text', '0', NULL, NULL, '0');
INSERT INTO `onl_cgform_field` VALUES ('8e1212df6a94ed8f5102a327564e5af6', '8d66ea41c7cc4ef9ab3aab9055657fc9', 'name', '分类名字', NULL, 0, 1, 'string', 32, 0, '', '', '', '', 'text', '', 120, NULL, '0', '', '', 0, 1, 1, 0, 'single', '', '', 7, NULL, NULL, '2020-05-07 22:46:32', 'admin', '', '', '', '', '', 'text', '0', NULL, NULL, '0');
INSERT INTO `onl_cgform_field` VALUES ('8e131d4a438c907b3272474780be7603', '402880eb71d52dc30171d52dc3a10000', 'update_by', '更新人', NULL, 0, 1, 'string', 50, 0, '', '', '', '', 'text', '', 120, NULL, '0', '', '', 0, 0, 0, 0, 'single', NULL, NULL, 8, NULL, NULL, '2020-05-02 19:37:58', 'admin', '', '', '', '', '', 'text', '0', NULL, NULL, '0');
INSERT INTO `onl_cgform_field` VALUES ('8e1e4cb5c41ba685c48ebabf0aacc469', '402880eb71d61d3d0171d61d3de30000', 'name', '名称', NULL, 0, 1, 'String', 200, 0, '', '', '', '', 'text', '', 120, NULL, '0', '', '#{sysUserName}', 0, 1, 0, 0, 'single', '', '', 3, 'admin', '2020-05-03 00:54:16', '2020-05-02 23:59:33', 'admin', '', '', '', '', '', NULL, '0', NULL, NULL, '0');
INSERT INTO `onl_cgform_field` VALUES ('8ea43fd1e4ce82becee61b2f1e2e843f', '32feeb502544416c9bf41329c10a88f4', 'sex', '性别', NULL, 0, 1, 'String', 32, 0, '', 'sex', '', '', 'list', '', 120, NULL, '0', '', '', 0, 1, 1, 0, 'single', '', '', 8, 'admin', '2019-08-23 20:03:40', '2019-07-02 18:23:23', 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `onl_cgform_field` VALUES ('8f1d302868640b72cef52171a023a203', 'e9faf717024b4aae95cff224ae9b6d97', 'update_by', '更新人', NULL, 0, 1, 'string', 50, 0, '', '', '', '', 'text', '', 120, NULL, '0', '', '', 0, 0, 0, 0, 'single', '', '', 4, 'admin', '2019-07-03 18:23:49', '2019-07-03 18:22:35', 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `onl_cgform_field` VALUES ('8f3e6fb68179c690f748f3c541fb50f1', '7ea60a25fa27470e9080d6a921aabbd1', 'create_time', '创建日期', NULL, 0, 1, 'Date', 20, 0, '', '', '', '', 'datetime', '', 120, NULL, '0', '', '', 0, 0, 0, 0, 'single', '', '', 3, NULL, NULL, '2019-04-17 00:22:21', 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `onl_cgform_field` VALUES ('8fc0be84bed1216635c69af918e097ff', '402860816aa5921f016aa5dedcb90009', 'name', '并行会签标题', 'name', 0, 1, 'string', 32, 0, '', '', '', '', 'text', '', 120, NULL, '0', '', '', 0, 1, 1, 0, 'group', '', '', 6, 'admin', '2019-05-11 15:56:47', '2019-05-11 15:50:08', 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `onl_cgform_field` VALUES ('909b3de0c8a48a21ef8cf98eb4831689', '56efb74326e74064b60933f6f8af30ea', 'create_by', '创建人', NULL, 0, 1, 'string', 50, 0, '', '', '', '', 'text', '', 120, NULL, '0', '', '', 0, 0, 0, 0, 'single', '', '', 2, 'admin', '2020-07-10 16:53:27', '2020-05-08 23:45:32', 'admin', '', '', '', '', '', NULL, '0', NULL, NULL, '0');
INSERT INTO `onl_cgform_field` VALUES ('90a822b8a63bbbc1e9575c9f4e21e021', 'd35109c3632c4952a19ecc094943dd71', 'descc', '描述', NULL, 0, 1, 'string', 500, 0, '', '', '', '', 'textarea', '', 120, '', '0', '', '', 0, 1, 1, 0, 'single', '', '', 9, 'admin', '2020-05-06 11:33:01', '2019-03-15 14:24:35', 'admin', '', '', '', '', '', NULL, '0', NULL, NULL, '0');
INSERT INTO `onl_cgform_field` VALUES ('90f39a6e29dae2e1fbb59d7d605f7c09', '1acb6f81a1d9439da6cc4e868617b565', 'iz_valid', '启用状态', NULL, 0, 1, 'String', 2, 0, '', 'air_china_valid', '', '', 'list', '', 120, NULL, '0', '', '', 0, 1, 1, 0, 'single', '', '', 11, 'admin', '2019-06-10 14:47:14', '2019-04-23 22:58:19', 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `onl_cgform_field` VALUES ('91be98b96dea1528abc943f9f131fd16', '402880e570a5d7000170a5d700f50000', 'price', '价格', NULL, 0, 1, 'double', 32, 0, '', '', '', '', 'text', '', 120, 'n', '0', '', '', 0, 1, 1, 0, 'single', NULL, NULL, 7, NULL, NULL, '2020-03-04 21:58:16', 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `onl_cgform_field` VALUES ('91f7cd9b59c0da033363f8a09b02ec96', '3d447fa919b64f6883a834036c14aa67', 'create_time', '创建日期', NULL, 0, 1, 'Date', 20, 0, '', '', '', '', 'datetime', '', 120, NULL, '0', '', '', 0, 0, 0, 0, 'single', '', '', 5, 'admin', '2020-02-21 17:58:46', '2020-02-20 16:19:00', 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `onl_cgform_field` VALUES ('921cc5a92a79e1e21e9e631a1b7f3fbd', '8d66ea41c7cc4ef9ab3aab9055657fc9', 'id', '主键', NULL, 1, 0, 'string', 36, 0, '', '', '', '', 'text', '', 120, NULL, '0', '', '', 0, 0, 0, 1, 'single', '', '', 1, NULL, NULL, '2020-05-07 22:46:31', 'admin', '', '', '', '', '', NULL, '0', NULL, NULL, '0');
INSERT INTO `onl_cgform_field` VALUES ('9341a3b2a734d8c73455c136e1cac8ad', '4fb8e12a697f4d5bbe9b9fb1e9009486', 'update_by', '更新人', NULL, 0, 1, 'string', 50, 0, '', '', '', '', 'text', '', 120, NULL, '0', '', '', 0, 0, 0, 0, 'single', '', '', 8, 'admin', '2020-04-10 19:51:27', '2020-04-10 19:47:01', 'admin', '', '', '', '', '', 'text', '0', NULL, NULL, '0');
INSERT INTO `onl_cgform_field` VALUES ('9370c9304af30b8d29defe0a5ada6e5b', '62e29cdb81ac44d1a2d8ff89851b853d', 'DC_DDSA', 'DD', NULL, 0, 1, 'String', 32, 0, '', '', '', '', 'text', '', 120, NULL, '0', '', '', 0, 1, 1, 0, 'single', '', '', 6, NULL, NULL, '2019-05-11 14:01:14', 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `onl_cgform_field` VALUES ('9371f61d39c5d57ddb0a2db96b2e2412', '402860816bff91c0016bffa220a9000b', 'speciality', '专业', 'speciality', 0, 1, 'string', 100, 0, '', '', '', '', 'text', '', 120, NULL, '0', '', '', 0, 1, 1, 0, 'group', '', '', 15, 'admin', '2019-07-22 16:15:32', '2019-07-17 19:12:24', 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `onl_cgform_field` VALUES ('947174892512ea97fafde899d427ea7e', '402860816bff91c0016bff91c0cb0000', 'real_name', '姓名', 'real_name', 0, 1, 'string', 100, 0, '', '', '', '', 'text', '', 120, NULL, '0', '', '', 0, 1, 1, 0, 'group', '', '', 4, 'admin', '2019-07-19 18:09:01', '2019-07-17 18:54:32', 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `onl_cgform_field` VALUES ('94b8bf435175cc545366e11992280757', '32f75e4043ef4070919dbd4337186a3d', 'age', '年龄', NULL, 0, 1, 'int', 32, 0, '', '', '', '', 'text', '', 120, NULL, '0', '', '', 1, 1, 1, 0, 'group', '', '', 7, 'admin', '2019-04-11 10:15:32', '2019-03-27 15:54:49', 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `onl_cgform_field` VALUES ('94e682cb802777fe4205536888f69353', '402860816bff91c0016bff91d2810005', 'create_by', '创建人', 'create_by', 0, 1, 'string', 100, 0, '', '', '', '', 'text', '', 120, NULL, '0', '', '', 0, 1, 1, 0, 'group', '', '', 10, 'admin', '2019-07-19 18:05:55', '2019-07-17 18:54:35', 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `onl_cgform_field` VALUES ('951c51699d728072d88196d30f7aad10', '4adec929a6594108bef5b35ee9966e9f', 'address', '地址', NULL, 0, 1, 'String', 200, 0, '', '', '', '', 'text', '', 120, NULL, '0', '', '{{ demoFieldDefVal_getAddress() }}', 0, 1, 1, 0, 'single', '', '', 5, 'admin', '2020-04-10 19:43:38', '2020-04-10 19:35:58', 'admin', '', '', '', '', '', NULL, '0', NULL, NULL, '0');
INSERT INTO `onl_cgform_field` VALUES ('957386b500be42a200d6a56d54345392', 'deea5a8ec619460c9245ba85dbc59e80', 'num', '数量', NULL, 0, 1, 'int', 32, 0, '', '', '', '', 'text', '', 120, 'n', '0', '', '', 0, 1, 1, 0, 'single', '', '', 8, 'admin', '2020-05-03 01:01:18', '2019-04-20 11:41:19', 'admin', '', '', '', '', '', NULL, '0', NULL, NULL, '0');
INSERT INTO `onl_cgform_field` VALUES ('960d2847922b61dadeb3518ef55fb0c1', '1acb6f81a1d9439da6cc4e868617b565', 'wl_name', '物料名称', NULL, 0, 1, 'String', 100, 0, '', '', '', '', 'text', '', 120, NULL, '0', '', '', 0, 1, 1, 0, 'single', '', '', 7, 'admin', '2019-06-10 14:47:14', '2019-04-23 22:58:19', 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `onl_cgform_field` VALUES ('9665f02764774fdd77c19923d3ff3c3e', '4028318169e81b970169e81b97650000', 'cost_time', '耗时', 'cost_time', 0, 1, 'string', 19, 0, NULL, NULL, NULL, NULL, 'text', NULL, 120, NULL, '0', NULL, NULL, 0, 1, 1, 0, 'group', NULL, NULL, 12, NULL, NULL, '2019-04-04 19:28:36', 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `onl_cgform_field` VALUES ('966a4988298d5cb0be47848735ce8cb7', '4028839a6de2ebd3016de2ebd3870000', 'xg_shangxian', '选购上限', 'xg_shangxian', 0, 1, 'int', 10, 0, NULL, NULL, NULL, NULL, 'text', NULL, 120, NULL, '0', NULL, NULL, 0, 1, 1, 0, 'group', NULL, NULL, 12, NULL, NULL, '2019-10-19 15:29:30', 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `onl_cgform_field` VALUES ('9698964a9e06edf12fc88df976080984', '09fd28e4b7184c1a9668496a5c496450', 'update_time', '更新日期', NULL, 0, 1, 'Date', 20, 0, '', '', '', '', 'datetime', '', 120, NULL, '0', '', '', 0, 0, 0, 0, 'single', '', '', 5, 'admin', '2020-05-14 21:19:21', '2020-05-08 23:51:49', 'admin', '', '', '', '', '', NULL, '0', NULL, NULL, '0');
INSERT INTO `onl_cgform_field` VALUES ('96c585a4f71e5c38ed25b9741366365b', '402860816bff91c0016bff91c7010001', 'sys_org_code', '组织机构编码', 'sys_org_code', 0, 1, 'string', 50, 0, '', '', '', '', 'text', '', 120, NULL, '0', '', '', 0, 1, 1, 0, 'group', '', '', 11, 'admin', '2019-07-19 18:07:47', '2019-07-17 18:54:32', 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `onl_cgform_field` VALUES ('9765efa2cafde6d0ede2215848c9e80b', '32f75e4043ef4070919dbd4337186a3d', 'id', '主键', NULL, 1, 0, 'string', 36, 0, '', '', '', '', 'text', '', 120, NULL, '0', '', '', 0, 1, 1, 0, 'single', '', '', 0, 'admin', '2019-04-11 10:15:32', '2019-03-27 15:54:49', 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `onl_cgform_field` VALUES ('98e82cb1595609a3b42fa75c60ac1229', '402860816bff91c0016bff91d2810005', 'update_by', '更新人', 'update_by', 0, 1, 'string', 100, 0, '', '', '', '', 'text', '', 120, NULL, '0', '', '', 0, 1, 1, 0, 'group', '', '', 12, 'admin', '2019-07-19 18:05:55', '2019-07-17 18:54:35', 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `onl_cgform_field` VALUES ('9914a0c84805e72c4b6075e36edb13f9', '402860816aa5921f016aa5921f480000', 'create_time', '创建时间', 'create_time', 0, 1, 'Date', 0, 0, '', '', '', '', 'date', '', 120, NULL, '0', '', '', 0, 0, 0, 0, 'group', '', '', 9, 'admin', '2019-05-11 15:31:54', '2019-05-11 14:26:19', 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `onl_cgform_field` VALUES ('9920ecec9c9109fc6b93e86f8fdfa03b', '402860816bff91c0016bffa220a9000b', 'depart_name', '所在部门', 'depart_name', 0, 1, 'string', 100, 0, '', '', '', '', 'text', '', 120, NULL, '0', '', '', 1, 1, 1, 0, 'group', '', '', 2, 'admin', '2019-07-22 16:15:32', '2019-07-17 19:12:24', 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `onl_cgform_field` VALUES ('99b43bbb23237815ebb74b12b4d7ea2f', '62e29cdb81ac44d1a2d8ff89851b853d', 'id', '主键', NULL, 1, 0, 'string', 36, 0, '', '', '', '', 'text', '', 120, NULL, '0', '', '', 0, 0, 0, 0, 'single', '', '', 1, NULL, NULL, '2019-05-11 14:01:14', 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `onl_cgform_field` VALUES ('9a462d83210cad30f0e12b98e8a172a7', '3b5e18da618247539481816995b6bf9f', 'update_by', '更新人', NULL, 0, 1, 'string', 50, 0, '', '', '', '', 'text', '', 120, NULL, '0', '', '', 0, 0, 0, 0, 'single', '', '', 4, 'admin', '2020-07-11 11:27:29', '2020-07-11 11:27:17', 'admin', '', '', '', '', '', NULL, '0', NULL, NULL, '0');
INSERT INTO `onl_cgform_field` VALUES ('9a4dc8718000c30953a3923eb7db5096', '402880eb71d52dc30171d52dc3a10000', 'id', '主键', NULL, 1, 0, 'string', 36, 0, '', '', '', '', 'text', '', 120, NULL, '0', '', '', 0, 0, 0, 1, 'single', NULL, NULL, 1, NULL, NULL, '2020-05-02 19:37:59', 'admin', '', '', '', '', '', NULL, '0', NULL, NULL, '0');
INSERT INTO `onl_cgform_field` VALUES ('9a579c506f75f75baf88352a5eb2c249', '1acb6f81a1d9439da6cc4e868617b565', 'bpm_status', '流程状态', NULL, 0, 1, 'String', 2, 0, '1', 'bpm_status', '', '', 'list', '', 120, NULL, '0', '', '', 0, 0, 1, 0, 'single', '', '', 16, 'admin', '2019-06-10 14:47:14', '2019-05-07 16:54:43', 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `onl_cgform_field` VALUES ('9a9516645cbc8147b23333fea76aa2bb', 'b81de38db24047b497d476516f8a0865', 'create_by', '创建人', NULL, 0, 1, 'string', 50, 0, '', '', '', '', 'text', '', 120, NULL, '0', '', '', 0, 0, 0, 0, 'single', '', '', 2, NULL, NULL, '2020-02-24 14:56:08', 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `onl_cgform_field` VALUES ('9aa6738443d3d8a67cf50506199d15ad', 'cb2d8534a2f544bc9c618dd49da66336', 'aaae', 'aae', NULL, 0, 1, 'String', 32, 0, '', '', '', '', 'text', '', 120, NULL, '0', '', '', 0, 1, 1, 0, 'single', '', '', 8, NULL, NULL, '2020-02-24 17:22:43', 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `onl_cgform_field` VALUES ('9bd056786694d67666f6924cc225b1a0', '3d447fa919b64f6883a834036c14aa67', 'id', '主键', NULL, 1, 0, 'string', 36, 0, '', '', '', '', 'text', '', 120, NULL, '0', '', '', 0, 0, 0, 0, 'single', '', '', 1, 'admin', '2020-02-21 17:58:46', '2020-02-20 16:19:00', 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `onl_cgform_field` VALUES ('9c40fb4db8afed3c682c6b8a732fd69d', 'e2faf977fdaf4b25a524f58c2441a51c', 'post', '用户岗位', NULL, 0, 1, 'String', 32, 0, '', 'post_code', 'air_china_post_materiel_main', 'post_name', 'sel_search', '', 120, NULL, '0', '', '', 0, 1, 1, 0, 'single', '', '', 8, 'admin', '2019-06-10 17:27:00', '2019-04-24 17:12:11', 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `onl_cgform_field` VALUES ('9cc60be30026301b9eafb8c310387f54', '402880e570a5d7000170a5d700f50000', 'update_by', '更新人', NULL, 0, 1, 'string', 50, 0, '', '', '', '', 'text', '', 120, NULL, '0', '', '', 0, 0, 0, 0, 'single', NULL, NULL, 4, NULL, NULL, '2020-03-04 21:58:16', 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `onl_cgform_field` VALUES ('9ceff249ef81ca6fa145456667c89051', '4adec929a6594108bef5b35ee9966e9f', 'create_time', '创建日期', NULL, 0, 1, 'Date', 20, 0, '', '', '', '', 'datetime', '', 120, NULL, '0', '', '', 0, 0, 0, 0, 'single', '', '', 8, 'admin', '2020-04-10 19:43:38', '2020-04-10 19:35:58', 'admin', '', '', '', '', '', 'text', '0', NULL, NULL, '0');
INSERT INTO `onl_cgform_field` VALUES ('9d85bafa399f28a40e1de1eeef747223', '4028318169e81b970169e81b97650000', 'ip', 'IP', 'ip', 0, 1, 'string', 100, 0, NULL, NULL, NULL, NULL, 'text', NULL, 120, NULL, '0', NULL, NULL, 0, 1, 1, 0, 'group', NULL, NULL, 7, NULL, NULL, '2019-04-04 19:28:36', 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `onl_cgform_field` VALUES ('9d89ff1a019f41d80307652041490944', '32feeb502544416c9bf41329c10a88f4', 'name', '请假人', NULL, 0, 1, 'String', 32, 0, '', '', '', '', 'text', '', 120, NULL, '0', '', '', 0, 1, 1, 0, 'single', '', '', 7, 'admin', '2019-08-23 20:03:40', '2019-07-02 18:23:23', 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `onl_cgform_field` VALUES ('9e50680eb4e79b3af352a5933d239dff', 'dbf4675875e14676a3f9a8b2b8941140', 'id', '主键', NULL, 1, 0, 'string', 36, 0, '', '', '', '', 'text', '', 120, NULL, '0', '', '', 0, 0, 0, 0, 'single', '', '', 1, NULL, NULL, '2019-05-27 18:02:07', 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `onl_cgform_field` VALUES ('9ec6c73432e57354d7d55dbea0916776', '18f064d1ef424c93ba7a16148851664f', 'name', '输入框', NULL, 0, 1, 'string', 32, 0, '', '', '', '', 'text', '', 120, NULL, '0', '', '#{sysOrgCode}', 0, 1, 1, 0, 'single', '', '', 7, 'admin', '2020-05-12 20:30:57', '2020-05-12 20:26:01', 'admin', '', '', '', '', '', 'text', '0', NULL, NULL, '0');
INSERT INTO `onl_cgform_field` VALUES ('a01a7fe5660206e6f407ed98b6c732d6', '402860816bff91c0016bff91cfea0004', 'phone', '联系方式', 'phone', 0, 1, 'string', 20, 0, '', '', '', '', 'text', '', 120, NULL, '0', '', '', 0, 1, 1, 0, 'group', '', '', 7, 'admin', '2019-07-19 18:05:13', '2019-07-17 18:54:35', 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `onl_cgform_field` VALUES ('a1a2d49b35c185df9f9e149b290aa277', '56efb74326e74064b60933f6f8af30ea', 'update_by', '更新人', NULL, 0, 1, 'string', 50, 0, '', '', '', '', 'text', '', 120, NULL, '0', '', '', 0, 0, 0, 0, 'single', '', '', 4, 'admin', '2020-07-10 16:53:27', '2020-05-08 23:45:32', 'admin', '', '', '', '', '', NULL, '0', NULL, NULL, '0');
INSERT INTO `onl_cgform_field` VALUES ('a1f5daba36f536e7acf6a939826183b0', 'fb19fb067cd841f9ae93d4eb3b883dc0', 'id', '主键', NULL, 1, 0, 'string', 36, 0, '', '', '', '', 'text', '', 120, '', '0', '', '', 0, 1, 1, 0, 'single', '', '', 0, NULL, NULL, '2019-03-23 11:39:48', 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `onl_cgform_field` VALUES ('a232d608434d15fcecd8a3f31a9044b2', '86bf17839a904636b7ed96201b2fa6ea', 'create_time', '创建日期', NULL, 0, 1, 'Date', 20, 0, '', '', '', '', 'datetime', '', 120, NULL, '0', '', '', 0, 0, 0, 0, 'single', '', '', 3, 'admin', '2020-05-14 21:18:49', '2020-05-08 23:48:31', 'admin', '', '', '', '', '', NULL, '0', NULL, NULL, '0');
INSERT INTO `onl_cgform_field` VALUES ('a3dea64c8009780e213d16c69704c030', '53a3e82b54b946c2b904f605875a275c', 'id', '主键', NULL, 1, 0, 'string', 36, 0, '', '', '', '', 'text', '', 120, NULL, '0', '', '', 0, 0, 0, 1, 'single', '', '', 1, 'admin', '2020-05-08 23:42:51', '2020-05-07 22:49:47', 'admin', '', '', '', '', '', NULL, '0', NULL, NULL, '0');
INSERT INTO `onl_cgform_field` VALUES ('a45eba33810c485b9d8e6f70818a1dfa', '402860816aa5921f016aa5921f480000', 'bpm_status', '流程状态', 'bpm_status', 0, 1, 'string', 50, 0, '1', 'bpm_status', '', '', 'text', '', 120, NULL, '0', '', '', 0, 0, 1, 0, 'group', '', '', 7, 'admin', '2019-05-11 15:31:54', '2019-05-11 14:26:19', 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `onl_cgform_field` VALUES ('a6471d4fb3dbffef01dab1f7d452bb30', '27fc5f91274344afa7673a732b279939', 'update_time', '更新日期', NULL, 0, 1, 'Date', 20, 0, '', '', '', '', 'datetime', '', 120, NULL, '0', '', '', 0, 0, 0, 0, 'single', '', '', 5, 'admin', '2019-07-01 16:28:20', '2019-07-01 16:26:42', 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `onl_cgform_field` VALUES ('a6722b498602d7d7b5177b16789d8cc1', 'e5464aa8fa7b47c580e91593cf9b46dc', 'create_by', '创建人', NULL, 0, 1, 'string', 50, 0, '', '', '', '', 'text', '', 120, NULL, '0', '', '', 0, 0, 0, 0, 'single', '', '', 2, 'admin', '2019-04-24 17:09:48', '2019-04-24 11:05:10', 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `onl_cgform_field` VALUES ('a76f561057ac9e43a8ca09e478a1eab8', '402860816bff91c0016bff91ca7e0002', 'update_time', '更新时间', 'update_time', 0, 1, 'Date', 0, 0, '', '', '', '', 'date', '', 120, NULL, '0', '', '', 0, 1, 1, 0, 'group', '', '', 12, 'admin', '2019-07-19 18:07:13', '2019-07-17 18:54:33', 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `onl_cgform_field` VALUES ('a7822f6e4cffb37fc0729cbd4cfd8655', '32f75e4043ef4070919dbd4337186a3d', 'name', '用户名', NULL, 0, 1, 'String', 32, 0, '', '', '', '', 'text', '', 120, NULL, '0', '', '', 1, 1, 1, 0, 'single', '', '', 5, 'admin', '2019-04-11 10:15:32', '2019-03-27 15:54:49', 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `onl_cgform_field` VALUES ('a7b6ae09441ce36a14c7ce95f17a218e', '86bf17839a904636b7ed96201b2fa6ea', 'create_by', '创建人', NULL, 0, 1, 'string', 50, 0, '', '', '', '', 'text', '', 120, NULL, '0', '', '', 0, 0, 0, 0, 'single', '', '', 2, 'admin', '2020-05-14 21:18:49', '2020-05-08 23:48:31', 'admin', '', '', '', '', '', NULL, '0', NULL, NULL, '0');
INSERT INTO `onl_cgform_field` VALUES ('a82ca42a76e9d2b8dae6d57dbb5edb54', 'deea5a8ec619460c9245ba85dbc59e80', 'update_by', '更新人', NULL, 0, 1, 'string', 50, 0, '', '', '', '', 'text', '', 120, NULL, '0', '', '', 0, 0, 0, 0, 'single', '', '', 4, 'admin', '2020-05-03 01:01:18', '2019-04-20 11:41:19', 'admin', '', '', '', '', '', NULL, '0', NULL, NULL, '0');
INSERT INTO `onl_cgform_field` VALUES ('a83c029e021023771f646a20b63c4d4b', '18f064d1ef424c93ba7a16148851664f', 'sel_search', '下拉搜索', NULL, 0, 1, 'string', 32, 0, '', 'username', 'sys_user', 'username', 'sel_search', '', 120, NULL, '0', '', '', 0, 1, 1, 0, 'single', '', '', 12, 'admin', '2020-05-12 20:30:57', '2020-05-12 20:26:01', 'admin', '', '', '', '', '', 'text', '0', NULL, NULL, '0');
INSERT INTO `onl_cgform_field` VALUES ('a940adc4585fa3b5bd2114ea9abe8491', '402860816bff91c0016bff91ca7e0002', 'cert_level', '证书级别', 'cert_level', 0, 1, 'string', 100, 0, '', '', '', '', 'text', '', 120, NULL, '0', '', '', 0, 1, 1, 0, 'group', '', '', 5, 'admin', '2019-07-19 18:07:13', '2019-07-17 18:54:33', 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `onl_cgform_field` VALUES ('a94f1d7da64f3aa35c32155ea00ccb2f', '402860816bff91c0016bffa220a9000b', 'id', 'id', 'id', 1, 1, 'string', 32, 0, '', '', '', '', 'text', '', 120, NULL, '0', '', '', 0, 0, 0, 0, 'group', '', '', 1, 'admin', '2019-07-22 16:15:32', '2019-07-17 19:12:24', 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `onl_cgform_field` VALUES ('a9780eace237a15f26931dd6a9ec02e9', '758334cb1e7445e2822b60e807aec4a3', 'create_time', '创建日期', NULL, 0, 1, 'Date', 20, 0, '', '', '', '', 'datetime', '', 120, NULL, '0', '', '', 0, 0, 0, 0, 'single', '', '', 3, NULL, NULL, '2019-10-18 18:02:09', 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `onl_cgform_field` VALUES ('aa07931514727913413880b7a2b76dcb', 'd3ae1c692b9640e0a091f8c46e17bb01', 'create_time', '创建日期', NULL, 0, 1, 'Date', 20, 0, '', '', '', '', 'datetime', '', 120, NULL, '0', '', '', 0, 0, 0, 0, 'single', '', '', 3, NULL, NULL, '2019-07-24 14:47:30', 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `onl_cgform_field` VALUES ('aa4780601419c21dabb6c42fc511e71c', '402860816bff91c0016bffa220a9000b', 'have_children', '有无子女', 'have_children', 0, 1, 'string', 20, 0, '', '', '', '', 'text', '', 120, NULL, '0', '', '', 0, 1, 1, 0, 'group', '', '', 25, 'admin', '2019-07-22 16:15:32', '2019-07-17 19:12:24', 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `onl_cgform_field` VALUES ('ab10e0aa029ded2f4420a33420de225d', '1acb6f81a1d9439da6cc4e868617b565', 'wl_code', '物料编码', NULL, 0, 1, 'String', 60, 0, '', '', '', '', 'text', '', 120, NULL, '0', '', '', 0, 1, 1, 0, 'single', '', '', 6, 'admin', '2019-06-10 14:47:14', '2019-04-23 22:58:19', 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `onl_cgform_field` VALUES ('ab1f880ba593f3757dac70e003945aa2', '402860816bff91c0016bff91c0cb0000', 'depart_id', '部门ID', NULL, 0, 1, 'String', 32, 0, '', '', '', '', 'text', '', 120, NULL, '0', '', '', 0, 1, 1, 0, 'single', '', '', 6, 'admin', '2019-07-19 18:09:01', '2019-07-17 19:38:45', 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `onl_cgform_field` VALUES ('ab58f43f853fd1f65f83c22966883afb', 'beee191324fd40c1afec4fda18bd9d47', 'create_by', '创建人', NULL, 0, 1, 'string', 50, 0, '', '', '', '', 'text', '', 120, NULL, '0', '', '', 0, 0, 0, 0, 'single', '', '', 2, 'admin', '2019-04-13 13:41:13', '2019-04-13 13:40:56', 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `onl_cgform_field` VALUES ('ab8e6f1cca421c5ce395a2c1fdfd2100', '32feeb502544416c9bf41329c10a88f4', 'sys_org_code', '所属部门', NULL, 0, 1, 'string', 64, 0, '', '', '', '', 'text', '', 120, NULL, '0', '', '', 0, 0, 0, 0, 'single', '', '', 6, 'admin', '2019-08-23 20:03:40', '2019-07-02 18:23:23', 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `onl_cgform_field` VALUES ('abe61a8ddf966a979457b763329a537b', 'e5464aa8fa7b47c580e91593cf9b46dc', 'create_time', '创建日期', NULL, 0, 1, 'Date', 20, 0, '', '', '', '', 'datetime', '', 120, NULL, '0', '', '', 0, 0, 0, 0, 'single', '', '', 3, 'admin', '2019-04-24 17:09:49', '2019-04-24 11:05:10', 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `onl_cgform_field` VALUES ('ac91565da5fb8fe43a4da3dec660b25f', '402860816bff91c0016bff91c7010001', 'award_place', '获奖地点', 'award_place', 0, 1, 'string', 255, 0, '', '', '', '', 'text', '', 120, NULL, '0', '', '', 0, 1, 1, 0, 'group', '', '', 4, 'admin', '2019-07-19 18:07:47', '2019-07-17 18:54:32', 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `onl_cgform_field` VALUES ('acff5c8aef3b6288b87fd91215012206', 'e5464aa8fa7b47c580e91593cf9b46dc', 'update_by', '更新人', NULL, 0, 1, 'string', 50, 0, '', '', '', '', 'text', '', 120, NULL, '0', '', '', 0, 0, 0, 0, 'single', '', '', 4, 'admin', '2019-04-24 17:09:49', '2019-04-24 11:05:10', 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `onl_cgform_field` VALUES ('ad061417d5b53c67975eb83657505218', '73162c3b8161413e8ecdca7eb288d0c9', 'update_by', '更新人', NULL, 0, 1, 'string', 50, 0, '', '', '', '', 'text', '', 120, NULL, '0', '', '', 0, 0, 0, 0, 'single', '', '', 4, 'admin', '2019-07-01 14:23:32', '2019-06-10 15:18:34', 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `onl_cgform_field` VALUES ('ad93762c6c4a1dd8331e5fa11215b568', 'e2faf977fdaf4b25a524f58c2441a51c', 'id', '主键', NULL, 1, 0, 'string', 36, 0, '', '', '', '', 'text', '', 120, NULL, '0', '', '', 0, 0, 0, 0, 'single', '', '', 1, 'admin', '2019-06-10 17:27:00', '2019-04-24 17:12:11', 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `onl_cgform_field` VALUES ('ae31da96f38fc2941cb93d1bb1ab9431', 'deea5a8ec619460c9245ba85dbc59e80', 'product_name', '产品名字', NULL, 0, 1, 'String', 32, 0, '', '', '', '', 'text', '', 120, NULL, '0', '', '', 0, 1, 1, 0, 'single', '', '', 6, 'admin', '2020-05-03 01:01:18', '2019-04-20 11:41:19', 'admin', '', '', '', '', '', NULL, '0', NULL, NULL, '0');
INSERT INTO `onl_cgform_field` VALUES ('ae77bb317366622698c8ab9bf2325833', 'deea5a8ec619460c9245ba85dbc59e80', 'create_by', '创建人', NULL, 0, 1, 'string', 50, 0, '', '', '', '', 'text', '', 120, NULL, '0', '', '', 0, 0, 0, 0, 'single', '', '', 2, 'admin', '2020-05-03 01:01:18', '2019-04-20 11:41:19', 'admin', '', '', '', '', '', NULL, '0', NULL, NULL, '0');
INSERT INTO `onl_cgform_field` VALUES ('af0fe0df8b626129de62e22212732517', '402860816bff91c0016bff91cda80003', 'speciality', '专业', 'speciality', 0, 1, 'string', 100, 0, '', '', '', '', 'text', '', 120, NULL, '0', '', '', 0, 1, 1, 0, 'group', '', '', 6, 'admin', '2019-07-19 18:06:36', '2019-07-17 18:54:34', 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `onl_cgform_field` VALUES ('af6c582b902e2f2bf9930eba61ae7938', '73162c3b8161413e8ecdca7eb288d0c9', 'update_time', '更新日期', NULL, 0, 1, 'Date', 20, 0, '', '', '', '', 'datetime', '', 120, NULL, '0', '', '', 0, 0, 0, 0, 'single', '', '', 5, 'admin', '2019-07-01 14:23:32', '2019-06-10 15:18:34', 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `onl_cgform_field` VALUES ('afd3ef1d494a9b69d2c7a3cdde937f6f', '402860816bff91c0016bffa220a9000b', 'create_by', '创建人', 'create_by', 0, 1, 'string', 100, 0, '', '', '', '', 'text', '', 120, NULL, '0', '', '', 0, 1, 1, 0, 'group', '', '', 39, 'admin', '2019-07-22 16:15:32', '2019-07-17 19:12:24', 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `onl_cgform_field` VALUES ('b01304904babd7479de2acfe8a77157f', '402860816aa5921f016aa5921f480000', 'id', 'ID', 'id', 1, 1, 'string', 32, 0, '', '', '', '', 'text', '', 120, NULL, '0', '', '', 0, 0, 0, 0, 'group', '', '', 1, 'admin', '2019-05-11 15:31:54', '2019-05-11 14:26:19', 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `onl_cgform_field` VALUES ('b05b4cbb74f389a7376f51ed9fd97030', '402860816bff91c0016bff91d8830007', 'create_by', '创建人', 'create_by', 0, 1, 'string', 100, 0, '', '', '', '', 'text', '', 120, NULL, '0', '', '', 0, 1, 1, 0, 'group', '', '', 10, 'admin', '2019-07-19 18:04:41', '2019-07-17 18:54:37', 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `onl_cgform_field` VALUES ('b0a06bdbefd304d81a1838d8d94deda9', '4b556f0168f64976a3d20bfb932bc798', 'name', '用户名', NULL, 0, 1, 'String', 32, 0, '', '', '', '', 'text', '', 120, NULL, '0', '', '', 0, 1, 1, 0, 'single', '', '', 6, NULL, NULL, '2019-04-12 23:38:28', 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `onl_cgform_field` VALUES ('b0b1cf271dd6b221a902da2d2f8f889a', 'e9faf717024b4aae95cff224ae9b6d97', 'update_time', '更新日期', NULL, 0, 1, 'Date', 20, 0, '', '', '', '', 'datetime', '', 120, NULL, '0', '', '', 0, 0, 0, 0, 'single', '', '', 5, 'admin', '2019-07-03 18:23:49', '2019-07-03 18:22:35', 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `onl_cgform_field` VALUES ('b18f96f96d979daa7336e81086ea2bc1', 'cb2d8534a2f544bc9c618dd49da66336', 'sys_org_code', '所属部门', NULL, 0, 1, 'string', 64, 0, '', '', '', '', 'text', '', 120, NULL, '0', '', '', 0, 0, 0, 0, 'single', '', '', 6, 'admin', '2020-02-24 17:22:42', '2020-02-24 15:15:14', 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `onl_cgform_field` VALUES ('b1fc6e2ca671b19e57b08a4f57fc2454', 'fb7125a344a649b990c12949945cb6c1', 'update_time', '更新日期', NULL, 0, 1, 'date', 20, 0, '', '', '', '', 'text', '', 120, '', '0', '', '', 0, 0, 0, 0, 'single', '', '', 4, 'admin', '2019-03-26 19:24:11', '2019-03-26 19:01:52', 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `onl_cgform_field` VALUES ('b22694cf34ffb967b8717647816ad5df', 'e5464aa8fa7b47c580e91593cf9b46dc', 'fk_id', '外键', NULL, 0, 1, 'String', 32, 0, '', '', '', '', 'text', '', 120, NULL, '0', '', '', 0, 0, 0, 0, 'single', 'air_china_post_materiel_main', 'id', 15, 'admin', '2019-04-24 17:09:49', '2019-04-24 11:05:10', 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `onl_cgform_field` VALUES ('b276facab025f9750b0aff391693cc4b', '402860816bff91c0016bff91c7010001', 'id', 'id', 'id', 1, 1, 'string', 32, 0, '', '', '', '', 'text', '', 120, NULL, '0', '', '', 0, 0, 0, 0, 'group', '', '', 1, 'admin', '2019-07-19 18:07:47', '2019-07-17 18:54:32', 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `onl_cgform_field` VALUES ('b2b0cb30159639bb1190e150322b7541', '4028839a6de2ebd3016de2ebd3870000', 'wl_unit', '计量单位', 'wl_unit', 0, 1, 'string', 100, 0, NULL, NULL, NULL, NULL, 'text', NULL, 120, NULL, '0', NULL, NULL, 0, 1, 1, 0, 'group', NULL, NULL, 14, NULL, NULL, '2019-10-19 15:29:30', 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `onl_cgform_field` VALUES ('b3542d3e7908ed885ecc4ba9e7300705', '4b556f0168f64976a3d20bfb932bc798', 'create_by', '创建人', NULL, 0, 1, 'string', 50, 0, '', '', '', '', 'text', '', 120, NULL, '0', '', '', 0, 0, 0, 0, 'single', '', '', 2, NULL, NULL, '2019-04-12 23:38:28', 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `onl_cgform_field` VALUES ('b3b61f9386eb2262919e0835e3c82eb9', '86bf17839a904636b7ed96201b2fa6ea', 'num', '数量', NULL, 0, 1, 'int', 10, 0, '', '', '', '', 'text', '', 120, NULL, '0', '', '', 0, 1, 1, 0, 'single', '', '', 9, 'admin', '2020-05-14 21:18:49', '2020-05-08 23:48:31', 'admin', '', '', '', '', '', 'text', '0', NULL, NULL, '0');
INSERT INTO `onl_cgform_field` VALUES ('b47af4d937e55c6208939bac5378bfad', '62e29cdb81ac44d1a2d8ff89851b853d', 'update_time', '更新日期', NULL, 0, 1, 'Date', 20, 0, '', '', '', '', 'datetime', '', 120, NULL, '0', '', '', 0, 0, 0, 0, 'single', '', '', 5, NULL, NULL, '2019-05-11 14:01:14', 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `onl_cgform_field` VALUES ('b53203fc52d067bb4730dbcb7e496bd3', '56870166aba54ebfacb20ba6c770bd73', 'id', '主键', NULL, 1, 0, 'string', 36, 0, '', '', '', '', 'text', '', 120, NULL, '0', '', '', 0, 0, 0, 0, 'single', '', '', 1, 'admin', '2020-07-10 17:09:01', '2019-04-20 11:38:39', 'admin', '', '', '', '', '', NULL, '0', NULL, NULL, '0');
INSERT INTO `onl_cgform_field` VALUES ('b5cfd3c9691a884430f3d9cd5ecb211f', 'e2faf977fdaf4b25a524f58c2441a51c', 'create_by', '创建人', NULL, 0, 1, 'string', 50, 0, '', '', '', '', 'text', '', 120, NULL, '0', '', '', 0, 0, 0, 0, 'single', '', '', 2, 'admin', '2019-06-10 17:27:00', '2019-04-24 17:12:11', 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `onl_cgform_field` VALUES ('b6874a05734cad8bd96ffd2f31f1ebca', '402860816bff91c0016bff91c7010001', 'create_by', '创建人', 'create_by', 0, 1, 'string', 100, 0, '', '', '', '', 'text', '', 120, NULL, '0', '', '', 0, 1, 1, 0, 'group', '', '', 7, 'admin', '2019-07-19 18:07:47', '2019-07-17 18:54:32', 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `onl_cgform_field` VALUES ('b733fa73519603b22d401fabbf9e9781', '402860816bff91c0016bff91c0cb0000', 'hiredate', '入职时间', 'hiredate', 0, 1, 'Date', 0, 0, '', '', '', '', 'date', '', 120, NULL, '0', '', '', 0, 1, 1, 0, 'group', '', '', 5, 'admin', '2019-07-19 18:09:01', '2019-07-17 18:54:32', 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `onl_cgform_field` VALUES ('b7938e4518f9062ce62702cf45986e06', 'e2faf977fdaf4b25a524f58c2441a51c', 'create_time', '创建日期', NULL, 0, 1, 'Date', 20, 0, '', '', '', '', 'datetime', '', 120, NULL, '0', '', '', 0, 0, 0, 0, 'single', '', '', 3, 'admin', '2019-06-10 17:27:00', '2019-04-24 17:12:11', 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `onl_cgform_field` VALUES ('b7a1243aaa712e2c152c0c7a46f88683', '402860816bff91c0016bff91d8830007', 'age', '年龄', 'age', 0, 1, 'int', 10, 0, '', '', '', '', 'text', '', 120, NULL, '0', '', '', 0, 1, 1, 0, 'group', '', '', 5, 'admin', '2019-07-19 18:04:41', '2019-07-17 18:54:37', 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `onl_cgform_field` VALUES ('b7b311cf4483bd10a93a40891cc39a77', '8d66ea41c7cc4ef9ab3aab9055657fc9', 'has_child', '是否有子节点', NULL, 0, 1, 'string', 3, 0, '', 'yn', '', '', 'list', '', 120, NULL, '0', '', '', 0, 0, 0, 0, 'single', '', '', 11, NULL, NULL, '2020-05-07 22:46:32', 'admin', '', '', '', '', '', 'text', '0', NULL, NULL, '0');
INSERT INTO `onl_cgform_field` VALUES ('b91258e3dc15b28c2e3f0d934e6e27e8', 'fb7125a344a649b990c12949945cb6c1', 'id', '主键', NULL, 1, 0, 'string', 36, 0, '', '', '', '', 'text', '', 120, '', '0', '', '', 0, 1, 1, 0, 'single', '', '', 0, NULL, NULL, '2019-03-26 19:01:52', 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `onl_cgform_field` VALUES ('b92572ae142f8dd5f2ac02fb45e6b2c1', 'e2faf977fdaf4b25a524f58c2441a51c', 'name', '用户名', NULL, 0, 1, 'String', 32, 0, '', 'realname,username', 'report_user', 'name,account', 'sel_search', '', 120, NULL, '0', '', '', 0, 1, 1, 0, 'single', '', '', 6, 'admin', '2019-06-10 17:27:00', '2019-04-24 17:12:11', 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `onl_cgform_field` VALUES ('b9fbace24688c9c9a8c9be72c1d014e7', '402860816bff91c0016bffa220a9000b', 'phone', '电话', 'phone', 0, 1, 'string', 20, 0, '', '', '', '', 'text', '', 120, NULL, '0', '', '', 0, 1, 1, 0, 'group', '', '', 18, 'admin', '2019-07-22 16:15:32', '2019-07-17 19:12:24', 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `onl_cgform_field` VALUES ('ba5f4b2affa94f36eda7f6f133db7ae3', '402860816bff91c0016bff91d2810005', 'workplace', '工作单位', 'workplace', 0, 1, 'string', 255, 0, '', '', '', '', 'text', '', 120, NULL, '0', '', '', 0, 1, 1, 0, 'group', '', '', 6, 'admin', '2019-07-19 18:05:55', '2019-07-17 18:54:35', 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `onl_cgform_field` VALUES ('ba6f3e762d6e3ea1068a085ec2f7e501', '56efb74326e74064b60933f6f8af30ea', 'update_time', '更新日期', NULL, 0, 1, 'Date', 20, 0, '', '', '', '', 'datetime', '', 120, NULL, '0', '', '', 0, 0, 0, 0, 'single', '', '', 5, 'admin', '2020-07-10 16:53:27', '2020-05-08 23:45:32', 'admin', '', '', '', '', '', NULL, '0', NULL, NULL, '0');
INSERT INTO `onl_cgform_field` VALUES ('bad02e68ea37bf387337516af84a1ddb', '73162c3b8161413e8ecdca7eb288d0c9', 'id', '主键', NULL, 1, 0, 'string', 36, 0, '', '', '', '', 'text', '', 120, NULL, '0', '', '', 0, 0, 0, 0, 'single', '', '', 1, 'admin', '2019-07-01 14:23:32', '2019-06-10 15:18:34', 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `onl_cgform_field` VALUES ('bb44475e1d1738a19745bf9f3ebf9e40', '402860816bff91c0016bff91cfea0004', 'update_time', '更新时间', 'update_time', 0, 1, 'Date', 0, 0, '', '', '', '', 'date', '', 120, NULL, '0', '', '', 0, 1, 1, 0, 'group', '', '', 12, 'admin', '2019-07-19 18:05:13', '2019-07-17 18:54:35', 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `onl_cgform_field` VALUES ('bbbb38347b1a5340a1d293e455c632ce', 'fb19fb067cd841f9ae93d4eb3b883dc0', '3', '4', NULL, 0, 1, 'string', 32, 0, '', '', '', '', 'text', '', 120, '', '0', '', '', 0, 1, 1, 0, 'single', '', '', 6, NULL, NULL, '2019-03-23 11:39:48', 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `onl_cgform_field` VALUES ('bc648624ad14c826bbc6e9b23a2b9858', '402860816bff91c0016bff91ca7e0002', 'employee_id', '员工ID', 'employee_id', 0, 1, 'string', 32, 0, '', '', '', '', 'text', '', 120, NULL, '0', '', '', 0, 1, 1, 0, 'group', 'oa_employee_info', 'id', 2, 'admin', '2019-07-19 18:07:13', '2019-07-17 18:54:33', 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `onl_cgform_field` VALUES ('bc793fdbef9f6474425456c4eb9d197a', '402860816bff91c0016bff91cfea0004', 'witness', '证明人', 'references', 0, 1, 'string', 100, 0, '', '', '', '', 'text', '', 120, NULL, '0', '', '', 0, 1, 1, 0, 'group', '', '', 6, 'admin', '2019-07-19 18:05:13', '2019-07-17 18:54:35', 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `onl_cgform_field` VALUES ('bc7df6f3cf49f670c36a3de25e25e715', '402860816bff91c0016bff91d2810005', 'order_no', '序号', 'order_no', 0, 1, 'int', 10, 0, '', '', '', '', 'text', '', 120, NULL, '0', '', '', 0, 1, 1, 0, 'group', '', '', 9, 'admin', '2019-07-19 18:05:55', '2019-07-17 18:54:35', 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `onl_cgform_field` VALUES ('bcf2f5d1390227cf0d9ddfbd6121161f', '402880eb71d61d3d0171d61d3de30000', 'id', '主键', NULL, 1, 0, 'string', 36, 0, '', '', '', '', 'text', '', 120, NULL, '0', '', '', 0, 0, 0, 1, 'single', '', '', 1, 'admin', '2020-05-03 00:54:16', '2020-05-02 23:59:33', 'admin', '', '', '', '', '', NULL, '0', NULL, NULL, '0');
INSERT INTO `onl_cgform_field` VALUES ('bd39cb237049ac60218b3f4dd844f30c', '402860816bff91c0016bffa220a9000b', 'current_address', '现居住地', 'current_address', 0, 1, 'string', 200, 0, '', '', '', '', 'text', '', 120, NULL, '0', '', '', 0, 1, 1, 0, 'group', '', '', 30, 'admin', '2019-07-22 16:15:32', '2019-07-17 19:12:24', 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `onl_cgform_field` VALUES ('bde81809057b1a4c974fa0f090501fdd', '402860816aa5921f016aa5dedcb90009', 'update_by', '更新人', 'update_by', 0, 1, 'string', 50, 0, '', '', '', '', 'text', '', 120, NULL, '0', '', '', 0, 0, 0, 0, 'group', '', '', 4, 'admin', '2019-05-11 15:56:47', '2019-05-11 15:50:08', 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `onl_cgform_field` VALUES ('bdea5b776a15897265c43e6ee44af2e1', '997ee931515a4620bc30a9c1246429a9', 'create_by', '创建人', NULL, 0, 1, 'string', 50, 0, '', '', '', '', 'text', '', 120, NULL, '0', '', '', 0, 0, 0, 0, 'single', '', '', 2, 'admin', '2020-05-03 00:57:44', '2020-05-03 00:56:56', 'admin', '', '', '', '', '', NULL, '0', NULL, NULL, '0');
INSERT INTO `onl_cgform_field` VALUES ('be3f8c157d8a1b40e6f7b836552a8095', '8994f2817b5a45d9890aa04497a317c5', 'create_time', '创建日期', NULL, 0, 1, 'Date', 20, 0, '', '', '', '', 'text', '', 120, '', '0', '', '', 0, 1, 1, 0, 'single', '', '', 2, NULL, NULL, '2019-03-23 11:39:16', 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `onl_cgform_field` VALUES ('be5eeda7d51dace73d3818bd8467b53b', '402860816bff91c0016bff91c0cb0000', 'update_time', '更新时间', 'update_time', 0, 1, 'Date', 0, 0, '', '', '', '', 'date', '', 120, NULL, '0', '', '', 0, 1, 1, 0, 'group', '', '', 15, 'admin', '2019-07-19 18:09:01', '2019-07-17 18:54:32', 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `onl_cgform_field` VALUES ('be868eed386da3cfcf49ea9afcdadf11', 'd35109c3632c4952a19ecc094943dd71', 'create_time', '创建日期', NULL, 0, 1, 'Date', 20, 0, '', '', '', '', 'text', '', 120, '', '0', '', '', 0, 0, 0, 0, 'single', '', '', 3, 'admin', '2020-05-06 11:33:01', '2019-03-15 14:24:35', 'admin', '', '', '', '', '', NULL, '0', NULL, NULL, '0');
INSERT INTO `onl_cgform_field` VALUES ('bec3082fc5f0f194be5cd72cc2866ff4', 'e5464aa8fa7b47c580e91593cf9b46dc', 'wl_name', '物料名字', NULL, 0, 1, 'String', 200, 0, '', 'wl_code', 'air_china_materiel', 'wl_name', 'list', '', 120, NULL, '0', '', '', 0, 1, 1, 0, 'single', '', '', 6, 'admin', '2019-04-24 17:09:49', '2019-04-24 11:05:10', 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `onl_cgform_field` VALUES ('bed0bc67f570613eaa6a1bd8bcaaddcc', '4b556f0168f64976a3d20bfb932bc798', 'create_time', '创建日期', NULL, 0, 1, 'Date', 20, 0, '', '', '', '', 'datetime', '', 120, NULL, '0', '', '', 0, 0, 0, 0, 'single', '', '', 3, NULL, NULL, '2019-04-12 23:38:28', 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `onl_cgform_field` VALUES ('bed95ca6dd6bb4b4f7cfd787313a0a4b', '402880e570a5d7000170a5d700f50000', 'product_name', '产品名字', NULL, 0, 1, 'String', 32, 0, '', '', '', '', 'text', '', 120, NULL, '0', '', '', 0, 1, 1, 0, 'single', NULL, NULL, 6, NULL, NULL, '2020-03-04 21:58:16', 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `onl_cgform_field` VALUES ('bef85fd2846dd7ffc42d747095557d14', '4fb8e12a697f4d5bbe9b9fb1e9009486', 'date', '日期', NULL, 0, 1, 'string', 200, 0, '', '', '', '', 'text', '', 120, NULL, '0', '', '#{date}', 0, 1, 1, 0, 'single', '', '', 4, 'admin', '2020-04-10 19:51:27', '2020-04-10 19:47:01', 'admin', '', '', '', '', '', NULL, '0', NULL, NULL, '0');
INSERT INTO `onl_cgform_field` VALUES ('bf61aa04c5ca77ad54c764f8f8b2bdec', '402860816bff91c0016bff91d8830007', 'update_by', '更新人', 'update_by', 0, 1, 'string', 100, 0, '', '', '', '', 'text', '', 120, NULL, '0', '', '', 0, 1, 1, 0, 'group', '', '', 12, 'admin', '2019-07-19 18:04:41', '2019-07-17 18:54:37', 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `onl_cgform_field` VALUES ('bfc2f19fae367f885adb8bd82a344391', '4028318169e81b970169e81b97650000', 'userid', '操作用户账号', 'userid', 0, 1, 'string', 32, 0, NULL, NULL, NULL, NULL, 'text', NULL, 120, NULL, '0', NULL, NULL, 0, 1, 1, 0, 'group', NULL, NULL, 5, NULL, NULL, '2019-04-04 19:28:36', 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `onl_cgform_field` VALUES ('c047ed2cead5bab0307967435f370936', '53a3e82b54b946c2b904f605875a275c', 'chuc_date', '出厂时间', NULL, 0, 1, 'Date', 0, 0, '', '', '', '', 'date', '', 120, NULL, '0', '', '', 0, 1, 1, 0, 'single', '', '', 9, 'admin', '2020-05-08 23:42:51', '2020-05-07 22:49:47', 'admin', '', '', '', '', '', 'text', '0', NULL, NULL, '0');
INSERT INTO `onl_cgform_field` VALUES ('c0c80f370e1d400fe670d8702c3b84da', '4adec929a6594108bef5b35ee9966e9f', 'address_param', '地址（传参）', NULL, 0, 1, 'String', 32, 0, '', '', '', '', 'text', '', 120, NULL, '0', '', '{{ demoFieldDefVal_getAddress(\"昌平区\") }}', 0, 1, 1, 0, 'single', '', '', 6, 'admin', '2020-04-10 19:43:38', '2020-04-10 19:40:53', 'admin', '', '', '', '', '', NULL, '0', NULL, NULL, '0');
INSERT INTO `onl_cgform_field` VALUES ('c0d66c95773774e7ac1f2a88df307e7a', '402860816aa5921f016aa5921f480000', 'reason', '请假原因', 'reason', 0, 1, 'string', 500, 0, '', '', '', '', 'text', '', 120, NULL, '0', '', '', 0, 1, 1, 0, 'group', '', '', 6, 'admin', '2019-05-11 15:31:54', '2019-05-11 14:26:19', 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `onl_cgform_field` VALUES ('c29216d975fee50af175bca8c664a475', 'e67d26b610dd414c884c4dbb24e71ce3', 'id', '主键', NULL, 1, 0, 'string', 36, 0, '', '', '', '', 'text', '', 120, NULL, '0', '', '', 0, 0, 0, 0, 'single', '', '', 1, 'admin', '2019-04-24 11:03:32', '2019-04-24 11:02:56', 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `onl_cgform_field` VALUES ('c2b9eae184afe56d59ea7940d77cfced', '4adec929a6594108bef5b35ee9966e9f', 'id', '主键', NULL, 1, 0, 'string', 36, 0, '', '', '', '', 'text', '', 120, NULL, '0', '', '', 0, 0, 0, 1, 'single', '', '', 1, 'admin', '2020-04-10 19:43:38', '2020-04-10 19:35:57', 'admin', '', '', '', '', '', NULL, '0', NULL, NULL, '0');
INSERT INTO `onl_cgform_field` VALUES ('c2dbb6846ab0945a430d095a29e0f53d', '18f064d1ef424c93ba7a16148851664f', 'update_time', '更新日期', NULL, 0, 1, 'Date', 20, 0, '', '', '', '', 'datetime', '', 120, NULL, '0', '', '', 0, 0, 0, 0, 'single', '', '', 5, 'admin', '2020-05-12 20:30:57', '2020-05-12 20:26:01', 'admin', '', '', '', '', '', NULL, '0', NULL, NULL, '0');
INSERT INTO `onl_cgform_field` VALUES ('c382877700a9987f4bcc2deea9ee9daf', '4fb8e12a697f4d5bbe9b9fb1e9009486', 'id', '主键', NULL, 1, 0, 'string', 36, 0, '', '', '', '', 'text', '', 120, NULL, '0', '', '', 0, 0, 0, 1, 'single', '', '', 1, 'admin', '2020-04-10 19:51:27', '2020-04-10 19:47:01', 'admin', '', '', '', '', '', NULL, '0', NULL, NULL, '0');
INSERT INTO `onl_cgform_field` VALUES ('c43d87b6340c29c0c354aa9c579f387f', '32feeb502544416c9bf41329c10a88f4', 'create_by', '创建人', NULL, 0, 1, 'string', 50, 0, '', '', '', '', 'text', '', 120, NULL, '0', '', '', 0, 0, 0, 0, 'single', '', '', 2, 'admin', '2019-08-23 20:03:40', '2019-07-02 18:23:23', 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `onl_cgform_field` VALUES ('c456753a9c83e55fa241c4ec72c00f86', '402880eb71d52dc30171d52dc3a10000', 'create_by', '创建人', NULL, 0, 1, 'string', 50, 0, '', '', '', '', 'text', '', 120, NULL, '0', '', '', 0, 0, 0, 0, 'single', NULL, NULL, 6, NULL, NULL, '2020-05-02 19:37:58', 'admin', '', '', '', '', '', NULL, '0', NULL, NULL, '0');
INSERT INTO `onl_cgform_field` VALUES ('c4d71528f3f45661b1a87841b7256204', '402880e5721355dd01721355dd390000', 'order_main_id', '订单ID', NULL, 0, 1, 'string', 32, 0, '', '', '', '', 'text', '', 120, NULL, '0', '', '', 0, 0, 0, 0, 'single', NULL, NULL, 11, NULL, NULL, '2020-05-14 21:18:14', 'admin', '', '', '', '', '', 'text', '0', NULL, NULL, '0');
INSERT INTO `onl_cgform_field` VALUES ('c52711856d2e81ad1074265833f44328', 'b81de38db24047b497d476516f8a0865', 'id', '主键', NULL, 1, 0, 'string', 36, 0, '', '', '', '', 'text', '', 120, NULL, '0', '', '', 0, 0, 0, 1, 'single', '', '', 1, NULL, NULL, '2020-02-24 14:56:07', 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `onl_cgform_field` VALUES ('c59f39e7b3bb877398f16796b2cd7962', '997ee931515a4620bc30a9c1246429a9', 'pid', '父级节点', NULL, 0, 1, 'string', 32, 0, '', '', '', '', 'text', '', 120, NULL, '0', '', '', 0, 1, 0, 0, 'single', '', '', 9, 'admin', '2020-05-03 00:57:44', '2020-05-03 00:56:56', 'admin', '', '', '', '', '', 'text', '0', NULL, NULL, '0');
INSERT INTO `onl_cgform_field` VALUES ('c5a61724b3b1fdfd0eb39c361f22bafd', '8d66ea41c7cc4ef9ab3aab9055657fc9', 'update_by', '更新人', NULL, 0, 1, 'string', 50, 0, '', '', '', '', 'text', '', 120, NULL, '0', '', '', 0, 0, 0, 0, 'single', '', '', 4, NULL, NULL, '2020-05-07 22:46:32', 'admin', '', '', '', '', '', NULL, '0', NULL, NULL, '0');
INSERT INTO `onl_cgform_field` VALUES ('c5dd2fc34ae825ebfced2ec74948654c', '402860816aa5921f016aa5921f480000', 'end_date', '请假结束时间', 'end_date', 0, 1, 'Date', 0, 0, '', '', '', '', 'date', '', 120, NULL, '0', '', '', 0, 1, 1, 0, 'group', '', '', 5, 'admin', '2019-05-11 15:31:54', '2019-05-11 14:26:19', 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `onl_cgform_field` VALUES ('c5f6ea01a6523a60df153cc61dc92f4d', 'fbc35f067da94a70adb622ddba259352', 'create_by', '创建人', NULL, 0, 1, 'string', 50, 0, '', '', '', '', 'text', '', 120, NULL, '0', '', '', 0, 0, 0, 0, 'single', '', '', 2, NULL, NULL, '2019-07-03 19:44:23', 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `onl_cgform_field` VALUES ('c6024742fbc62f2fc39992e6b59ac13d', '05a3a30dada7411c9109306aa4117068', 'age', '年龄', NULL, 0, 1, 'int', 10, 0, '', '', '', '', 'text', '', 120, NULL, '0', '', '', 1, 1, 1, 0, 'group', '', '', 8, 'admin', '2020-05-12 22:39:41', '2020-05-06 11:34:31', 'admin', '', '', '', '', '', 'text', '0', NULL, NULL, '1');
INSERT INTO `onl_cgform_field` VALUES ('c6730e00df5efd77fedf181df29102de', '402860816bff91c0016bff91c7010001', 'update_by', '更新人', 'update_by', 0, 1, 'string', 100, 0, '', '', '', '', 'text', '', 120, NULL, '0', '', '', 0, 1, 1, 0, 'group', '', '', 9, 'admin', '2019-07-19 18:07:47', '2019-07-17 18:54:32', 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `onl_cgform_field` VALUES ('c75a7cb0a21958aa7ca5442f66019669', 'e9faf717024b4aae95cff224ae9b6d97', 'depart', '所属部门', NULL, 0, 1, 'String', 32, 0, '', '', '', '', 'text', '', 120, NULL, '0', '', '', 0, 1, 1, 0, 'single', '', '', 9, 'admin', '2019-07-03 18:23:49', '2019-07-03 18:22:35', 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `onl_cgform_field` VALUES ('c772ed9cbe2d1dc69e9ffa73d3487021', '4b556f0168f64976a3d20bfb932bc798', 'id', '主键', NULL, 1, 0, 'string', 36, 0, '', '', '', '', 'text', '', 120, NULL, '0', '', '', 0, 0, 0, 0, 'single', '', '', 1, NULL, NULL, '2019-04-12 23:38:28', 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `onl_cgform_field` VALUES ('c8027cf4f2483042445c89ba86c4e51f', '402880e5721355dd01721355dd390000', 'zong_price', '单品总价', NULL, 0, 1, 'double', 10, 3, '', '', '', '', 'text', '', 120, NULL, '0', '', '', 0, 1, 1, 0, 'single', NULL, NULL, 10, NULL, NULL, '2020-05-14 21:18:14', 'admin', '', '', '', '', '', 'text', '0', NULL, NULL, '0');
INSERT INTO `onl_cgform_field` VALUES ('c91b697b1bcd2be943fc746e2660bc9e', '402860816bff91c0016bff91d2810005', 'real_name', '姓名', 'real_name', 0, 1, 'string', 100, 0, '', '', '', '', 'text', '', 120, NULL, '0', '', '', 0, 1, 1, 0, 'group', '', '', 3, 'admin', '2019-07-19 18:05:55', '2019-07-17 18:54:35', 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `onl_cgform_field` VALUES ('c988bd2b6125c01ceb3579a207dd8784', '3d447fa919b64f6883a834036c14aa67', 'city', '市', NULL, 0, 1, 'String', 100, 0, '', '', '', '', 'text', '', 120, NULL, '0', '', '', 0, 1, 1, 0, 'single', '', '', 3, 'admin', '2020-02-21 17:58:46', '2020-02-20 16:19:00', 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `onl_cgform_field` VALUES ('c9b698d3f75aa780ee1eb67ef090b15b', '73162c3b8161413e8ecdca7eb288d0c9', 'wl_code', '物料编码', NULL, 0, 1, 'String', 200, 0, '', '', '', '', 'text', '', 120, NULL, '0', '', '', 0, 1, 1, 0, 'single', '', '', 8, 'admin', '2019-07-01 14:23:32', '2019-06-10 15:18:34', 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `onl_cgform_field` VALUES ('caf5a071f2299c0f9ff2f3038d6d0fc6', '402860816bff91c0016bff91ca7e0002', 'update_by', '更新人', 'update_by', 0, 1, 'string', 100, 0, '', '', '', '', 'text', '', 120, NULL, '0', '', '', 0, 1, 1, 0, 'group', '', '', 11, 'admin', '2019-07-19 18:07:13', '2019-07-17 18:54:33', 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `onl_cgform_field` VALUES ('cb33a708b95e19085f8c9001d2d5c64c', 'e9faf717024b4aae95cff224ae9b6d97', 'create_by', '创建人', NULL, 0, 1, 'string', 50, 0, '', '', '', '', 'text', '', 120, NULL, '0', '', '', 0, 0, 0, 0, 'single', '', '', 2, 'admin', '2019-07-03 18:23:49', '2019-07-03 18:22:35', 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `onl_cgform_field` VALUES ('cb50a5991ca29e617aa32e49e92c01e7', '09fd28e4b7184c1a9668496a5c496450', 'age', '年龄', NULL, 0, 1, 'int', 10, 0, '', '', '', '', 'text', '', 120, NULL, '0', '', '', 0, 1, 1, 0, 'single', '', '', 10, 'admin', '2020-05-14 21:19:21', '2020-05-08 23:51:49', 'admin', '', '', '', '', '', 'text', '0', NULL, NULL, '0');
INSERT INTO `onl_cgform_field` VALUES ('cb7da49a981a1b0acc5f7e8a0130bdcd', 'd35109c3632c4952a19ecc094943dd71', 'user_code', '用户编码', NULL, 0, 1, 'String', 32, 0, '', '', '', '', 'text', '', 120, NULL, '0', '', '', 0, 1, 0, 0, 'single', '', '', 11, 'admin', '2020-05-06 11:33:01', '2019-05-11 16:26:37', 'admin', '', '', '', '', '', NULL, '0', NULL, NULL, '0');
INSERT INTO `onl_cgform_field` VALUES ('cb871284e845e26e88242a96fac9c576', '402860816bff91c0016bff91c7010001', 'order_no', '序号', 'order_no', 0, 1, 'int', 10, 0, '', '', '', '', 'text', '', 120, NULL, '0', '', '', 0, 1, 1, 0, 'group', '', '', 6, 'admin', '2019-07-19 18:07:47', '2019-07-17 18:54:32', 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `onl_cgform_field` VALUES ('cc1ddc1304d3eb5d9a189da0a509ccd0', '32f75e4043ef4070919dbd4337186a3d', 'create_by', '创建人登录名称', NULL, 0, 1, 'string', 50, 0, '', '', '', '', 'text', '', 120, NULL, '0', '', '', 0, 0, 0, 0, 'single', '', '', 1, 'admin', '2019-04-11 10:15:32', '2019-03-27 15:54:49', 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `onl_cgform_field` VALUES ('ccda49a8cea079c34f0034361d058c08', '09fd28e4b7184c1a9668496a5c496450', 'update_by', '更新人', NULL, 0, 1, 'string', 50, 0, '', '', '', '', 'text', '', 120, NULL, '0', '', '', 0, 0, 0, 0, 'single', '', '', 4, 'admin', '2020-05-14 21:19:21', '2020-05-08 23:51:49', 'admin', '', '', '', '', '', NULL, '0', NULL, NULL, '0');
INSERT INTO `onl_cgform_field` VALUES ('ccf2f331af46ddc411b8039dd187621b', '4028839a6de2ebd3016de2ebd3870000', 'price', '单价', 'price', 0, 1, 'double', 10, 2, NULL, NULL, NULL, NULL, 'text', NULL, 120, NULL, '0', NULL, NULL, 0, 1, 1, 0, 'group', NULL, NULL, 10, NULL, NULL, '2019-10-19 15:29:30', 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `onl_cgform_field` VALUES ('cee3c1dbf67b4a7d9626b8032897a4c7', '402860816bff91c0016bff91d8830007', 'employee_id', '员工ID', 'employee_id', 0, 1, 'string', 32, 0, '', '', '', '', 'text', '', 120, NULL, '0', '', '', 0, 1, 1, 0, 'group', 'oa_employee_info', 'id', 2, 'admin', '2019-07-19 18:04:41', '2019-07-17 18:54:37', 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `onl_cgform_field` VALUES ('cefb82e2168ab7e3aa57a7e9c3ca950e', '09fd28e4b7184c1a9668496a5c496450', 'order_main_id', '订单ID', NULL, 0, 1, 'string', 32, 0, '', '', '', '', 'text', '', 120, NULL, '0', '', '', 0, 0, 0, 0, 'single', 'ces_order_main', 'id', 12, 'admin', '2020-05-14 21:19:21', '2020-05-08 23:51:49', 'admin', '', '', '', '', '', 'text', '0', NULL, NULL, '0');
INSERT INTO `onl_cgform_field` VALUES ('cf4c5a4c06ae6bac701edfeedfcd16aa', 'd3ae1c692b9640e0a091f8c46e17bb01', 'id', '主键', NULL, 1, 0, 'string', 36, 0, '', '', '', '', 'text', '', 120, NULL, '0', '', '', 0, 0, 0, 0, 'single', '', '', 1, NULL, NULL, '2019-07-24 14:47:30', 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `onl_cgform_field` VALUES ('cfeb6491427aec2b4db9694af867da23', 'e9faf717024b4aae95cff224ae9b6d97', 'create_time', '创建日期', NULL, 0, 1, 'Date', 20, 0, '', '', '', '', 'datetime', '', 120, NULL, '0', '', '', 0, 0, 0, 0, 'single', '', '', 3, 'admin', '2019-07-03 18:23:49', '2019-07-03 18:22:35', 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `onl_cgform_field` VALUES ('d0559db07f05c870860f98313eb0f857', 'cb2d8534a2f544bc9c618dd49da66336', 'create_time', '创建日期', NULL, 0, 1, 'Date', 20, 0, '', '', '', '', 'datetime', '', 120, NULL, '0', '', '', 0, 0, 0, 0, 'single', '', '', 3, 'admin', '2020-02-24 17:22:42', '2020-02-24 15:15:14', 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `onl_cgform_field` VALUES ('d0d1be336726df9c41f2173f8886ba35', '997ee931515a4620bc30a9c1246429a9', 'has_child', '是否有子节点', NULL, 0, 1, 'string', 3, 0, '', 'yn', '', '', 'list', '', 120, NULL, '0', '', '', 0, 0, 0, 0, 'single', '', '', 10, 'admin', '2020-05-03 00:57:44', '2020-05-03 00:56:56', 'admin', '', '', '', '', '', 'text', '0', NULL, NULL, '0');
INSERT INTO `onl_cgform_field` VALUES ('d14e47befe47925b1440d584f4ca56fc', '09fd28e4b7184c1a9668496a5c496450', 'create_time', '创建日期', NULL, 0, 1, 'Date', 20, 0, '', '', '', '', 'datetime', '', 120, NULL, '0', '', '', 0, 0, 0, 0, 'single', '', '', 3, 'admin', '2020-05-14 21:19:21', '2020-05-08 23:51:49', 'admin', '', '', '', '', '', NULL, '0', NULL, NULL, '0');
INSERT INTO `onl_cgform_field` VALUES ('d2551b70dc96a45a73b304bf755a996f', '402860816bff91c0016bff91d8830007', 'workplace', '工作单位', 'workplace', 0, 1, 'string', 255, 0, '', '', '', '', 'text', '', 120, NULL, '0', '', '', 0, 1, 1, 0, 'group', '', '', 6, 'admin', '2019-07-19 18:04:41', '2019-07-17 18:54:37', 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `onl_cgform_field` VALUES ('d29dcdba14ea61808391fff2d927efea', '402860816bff91c0016bff91c0cb0000', 'work_summary', '工作总结', 'work_summary', 0, 1, 'Text', 65535, 0, '', '', '', '', 'textarea', '', 120, NULL, '0', '', '', 0, 1, 1, 0, 'group', '', '', 10, 'admin', '2019-07-19 18:09:01', '2019-07-17 18:54:32', 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `onl_cgform_field` VALUES ('d3a701472d27ca8435d6a781a597038d', 'deea5a8ec619460c9245ba85dbc59e80', 'create_time', '创建日期', NULL, 0, 1, 'Date', 20, 0, '', '', '', '', 'datetime', '', 120, NULL, '0', '', '', 0, 0, 0, 0, 'single', '', '', 3, 'admin', '2020-05-03 01:01:18', '2019-04-20 11:41:19', 'admin', '', '', '', '', '', NULL, '0', NULL, NULL, '0');
INSERT INTO `onl_cgform_field` VALUES ('d3ba5f990e14f9a1a0bbf6aa4cfa26dc', '56efb74326e74064b60933f6f8af30ea', 'xd_date', '下单时间', NULL, 0, 1, 'Date', 0, 0, '', '', '', '', 'datetime', '', 120, NULL, '0', '', '', 0, 1, 1, 0, 'single', '', '', 8, 'admin', '2020-07-10 16:53:27', '2020-05-08 23:45:32', 'admin', '', '', '', '', '', 'text', '0', NULL, NULL, '0');
INSERT INTO `onl_cgform_field` VALUES ('d44b89cc5f1828f7ceb9be196d549665', '4fb8e12a697f4d5bbe9b9fb1e9009486', 'code', '编码', NULL, 0, 1, 'String', 200, 0, '', '', '', '', 'text', '', 120, NULL, '0', '', '${order_num_rule_param}', 0, 1, 1, 0, 'single', '', '', 2, 'admin', '2020-04-10 19:51:27', '2020-04-10 19:47:01', 'admin', '', '', '', '', '', NULL, '0', NULL, NULL, '0');
INSERT INTO `onl_cgform_field` VALUES ('d48bfd2a840f9b1d00bd3b5599dca0f0', '402860816bff91c0016bff91cda80003', 'post', '职务', 'post', 0, 1, 'string', 100, 0, '', '', '', '', 'text', '', 120, NULL, '0', '', '', 0, 1, 1, 0, 'group', '', '', 7, 'admin', '2019-07-19 18:06:36', '2019-07-17 18:54:34', 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `onl_cgform_field` VALUES ('d4d8cae3cd9ea93e378fc14303eee105', 'd35109c3632c4952a19ecc094943dd71', 'create_by', '创建人登录名称', NULL, 0, 1, 'string', 50, 0, '', '', '', '', 'text', '', 120, '', '0', '', '', 0, 0, 0, 0, 'single', '', '', 2, 'admin', '2020-05-06 11:33:01', '2019-03-15 14:24:35', 'admin', '', '', '', '', '', NULL, '0', NULL, NULL, '0');
INSERT INTO `onl_cgform_field` VALUES ('d4dea775487aef5a7aea41791d3a65db', 'e5464aa8fa7b47c580e91593cf9b46dc', 'cycle_time', '发放周期(年)', NULL, 0, 1, 'String', 32, 0, '', '', '', '', 'text', '', 120, NULL, '0', '', '', 0, 1, 1, 0, 'single', '', '', 11, 'admin', '2019-04-24 17:09:49', '2019-04-24 14:09:06', 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `onl_cgform_field` VALUES ('d52c79620e21128fb69b4e8628cf25cc', 'dbf4675875e14676a3f9a8b2b8941140', 'sys_org_code', '所属部门', NULL, 0, 1, 'string', 64, 0, '', '', '', '', 'text', '', 120, NULL, '0', '', '', 0, 0, 0, 0, 'single', '', '', 6, NULL, NULL, '2019-05-27 18:02:07', 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `onl_cgform_field` VALUES ('d530ab1bc3c51e8249a506a25d1003c7', '79091e8277c744158530321513119c68', 'start_time', '会签发起时间', NULL, 0, 1, 'Date', 32, 0, '', '', '', '', 'date', '', 120, NULL, '0', '', '', 0, 1, 1, 0, 'single', '', '', 7, 'admin', '2019-05-11 15:29:47', '2019-05-11 15:27:17', 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `onl_cgform_field` VALUES ('d53e70411c206efecb8dcd00174e907c', '62e29cdb81ac44d1a2d8ff89851b853d', 'create_time', '创建日期', NULL, 0, 1, 'Date', 20, 0, '', '', '', '', 'datetime', '', 120, NULL, '0', '', '', 0, 0, 0, 0, 'single', '', '', 3, NULL, NULL, '2019-05-11 14:01:14', 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `onl_cgform_field` VALUES ('d5df0a35352ee960053686e959e9084b', '1acb6f81a1d9439da6cc4e868617b565', 'wl_unit', '计量单位', NULL, 0, 1, 'String', 100, 0, '', 'air_china_unit', '', '', 'list_multi', '', 120, NULL, '0', '', '', 0, 1, 1, 0, 'single', '', '', 8, 'admin', '2019-06-10 14:47:14', '2019-04-23 22:58:19', 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `onl_cgform_field` VALUES ('d6fad89f4f26d733291863c2dfbc5945', '27fc5f91274344afa7673a732b279939', 'id', '主键', NULL, 1, 0, 'string', 36, 0, '', '', '', '', 'text', '', 120, NULL, '0', '', '', 0, 0, 0, 0, 'single', '', '', 1, 'admin', '2019-07-01 16:28:20', '2019-07-01 16:26:42', 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `onl_cgform_field` VALUES ('d6fb7dcec9256d4044199641cb823e5e', '18f064d1ef424c93ba7a16148851664f', 'sex', '下拉框', NULL, 0, 1, 'string', 32, 0, '', 'sex', '', '', 'list', '', 120, NULL, '0', '', '', 0, 1, 1, 0, 'single', '', '', 8, 'admin', '2020-05-12 20:30:57', '2020-05-12 20:26:01', 'admin', '', '', '', '', '', 'text', '0', NULL, NULL, '0');
INSERT INTO `onl_cgform_field` VALUES ('d7082454dac60103fdda3e00b6557d39', '402880eb71d52dc30171d52dc3a10000', 'update_time', '更新日期', NULL, 0, 1, 'Date', 20, 0, '', '', '', '', 'datetime', '', 120, NULL, '0', '', '', 0, 0, 0, 0, 'single', NULL, NULL, 9, NULL, NULL, '2020-05-02 19:37:58', 'admin', '', '', '', '', '', 'text', '0', NULL, NULL, '0');
INSERT INTO `onl_cgform_field` VALUES ('d766ea5809e2ec9ff2cdbcb18f610ab3', '7ea60a25fa27470e9080d6a921aabbd1', 'id', '主键', NULL, 1, 0, 'string', 36, 0, '', '', '', '', 'text', '', 120, NULL, '0', '', '', 0, 0, 0, 0, 'single', '', '', 1, NULL, NULL, '2019-04-17 00:22:21', 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `onl_cgform_field` VALUES ('d7c3b107f004cbc99dfe1fe6c79894d8', '402860816bff91c0016bffa220a9000b', 'social_insurance_type', '参加社保类型', 'social_insurance_type', 0, 1, 'string', 255, 0, '', '', '', '', 'text', '', 120, NULL, '0', '', '', 0, 1, 1, 0, 'group', '', '', 35, 'admin', '2019-07-22 16:15:32', '2019-07-17 19:12:24', 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `onl_cgform_field` VALUES ('d8234b56acea1a752271a6c911dd91a0', '7ea60a25fa27470e9080d6a921aabbd1', 'age', '年龄', NULL, 0, 1, 'String', 32, 0, '', '', '', '', 'text', '', 120, NULL, '0', '', '', 0, 1, 1, 0, 'single', '', '', 7, NULL, NULL, '2019-04-17 00:22:21', 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `onl_cgform_field` VALUES ('d86909d794b01ad7fbb5e61d28b6603b', '73162c3b8161413e8ecdca7eb288d0c9', 'sys_org_code', '所属部门', NULL, 0, 1, 'string', 64, 0, '', '', '', '', 'text', '', 120, NULL, '0', '', '', 0, 0, 0, 0, 'single', '', '', 6, 'admin', '2019-07-01 14:23:32', '2019-06-10 15:18:34', 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `onl_cgform_field` VALUES ('d949d9157831c2fb7ba9f175081fe036', '402860816bff91c0016bff91cda80003', 'school', '学校', 'school', 0, 1, 'string', 100, 0, '', '', '', '', 'text', '', 120, NULL, '0', '', '', 0, 1, 1, 0, 'group', '', '', 5, 'admin', '2019-07-19 18:06:36', '2019-07-17 18:54:34', 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `onl_cgform_field` VALUES ('d9d308feb95968dbb322c9bff0c18452', '32feeb502544416c9bf41329c10a88f4', 'update_by', '更新人', NULL, 0, 1, 'string', 50, 0, '', '', '', '', 'text', '', 120, NULL, '0', '', '', 0, 0, 0, 0, 'single', '', '', 4, 'admin', '2019-08-23 20:03:40', '2019-07-02 18:23:23', 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `onl_cgform_field` VALUES ('d9dde2f59bb148c6b7e95256acad8972', 'e67d26b610dd414c884c4dbb24e71ce3', 'post_name', '岗位名字', NULL, 0, 1, 'String', 32, 0, '', '', '', '', 'text', '', 120, NULL, '0', '', '', 0, 1, 1, 0, 'single', '', '', 7, 'admin', '2019-04-24 11:03:32', '2019-04-24 11:02:57', 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `onl_cgform_field` VALUES ('d9f9ae6986cb85019a3a4030f62f4d1a', '402860816bff91c0016bff91cfea0004', 'employee_id', '员工ID', 'employee_id', 0, 1, 'string', 32, 0, '', '', '', '', 'text', '', 120, NULL, '0', '', '', 0, 1, 1, 0, 'group', 'oa_employee_info', 'id', 2, 'admin', '2019-07-19 18:05:13', '2019-07-17 18:54:35', 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `onl_cgform_field` VALUES ('db1fb3e12385cb967b63420cfe97cde6', '402860816bff91c0016bff91cda80003', 'employee_id', '员工ID', 'employee_id', 0, 1, 'string', 32, 0, '', '', '', '', 'text', '', 120, NULL, '0', '', '', 0, 1, 1, 0, 'group', 'oa_employee_info', 'id', 2, 'admin', '2019-07-19 18:06:36', '2019-07-17 18:54:34', 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `onl_cgform_field` VALUES ('dc2bec862a4f0d600751c632d246f0ed', '4028839a6de2ebd3016de2ebd3870000', 'update_time', '更新日期', 'update_time', 0, 1, 'Date', 0, 0, NULL, NULL, NULL, NULL, 'date', NULL, 120, NULL, '0', NULL, NULL, 0, 1, 1, 0, 'group', NULL, NULL, 5, NULL, NULL, '2019-10-19 15:29:30', 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `onl_cgform_field` VALUES ('dd3e1e13d7429807b83a00a090e060b7', '402860816bff91c0016bffa220a9000b', 'join_party_info', '入党（团）时间地点', 'join_party_info', 0, 1, 'string', 255, 0, '', '', '', '', 'text', '', 120, NULL, '0', '', '', 0, 1, 1, 0, 'group', '', '', 26, 'admin', '2019-07-22 16:15:32', '2019-07-17 19:12:24', 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `onl_cgform_field` VALUES ('ddc302f84c75a5f056855c664b82202a', '402860816aa5921f016aa5921f480000', 'days', '请假天数', 'days', 0, 1, 'int', 10, 0, '', '', '', '', 'text', '', 120, NULL, '0', '', '', 0, 1, 1, 0, 'group', '', '', 3, 'admin', '2019-05-11 15:31:54', '2019-05-11 14:26:19', 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `onl_cgform_field` VALUES ('ddcc14a2105588982b4ae657f2893d81', '32feeb502544416c9bf41329c10a88f4', 'id', '主键', NULL, 1, 0, 'string', 36, 0, '', '', '', '', 'text', '', 120, NULL, '0', '', '', 0, 0, 0, 0, 'single', '', '', 1, 'admin', '2019-08-23 20:03:40', '2019-07-02 18:23:23', 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `onl_cgform_field` VALUES ('de34e46a66def956437635585db427b7', 'cb2d8534a2f544bc9c618dd49da66336', 'dde', 'ee', NULL, 0, 1, 'String', 32, 0, '', '', '', '', 'text', '', 120, NULL, '0', '', '', 0, 1, 1, 0, 'single', '', '', 10, NULL, NULL, '2020-02-24 17:22:43', 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `onl_cgform_field` VALUES ('de8f510d358f499dcd966e97708f82d4', '997ee931515a4620bc30a9c1246429a9', 'sys_org_code', '所属部门', NULL, 0, 1, 'string', 64, 0, '', '', '', '', 'text', '', 120, NULL, '0', '', '', 0, 0, 0, 0, 'single', '', '', 6, 'admin', '2020-05-03 00:57:44', '2020-05-03 00:56:56', 'admin', '', '', '', '', '', NULL, '0', NULL, NULL, '0');
INSERT INTO `onl_cgform_field` VALUES ('dedb920a5e876e27eb144464209ebe1e', '27fc5f91274344afa7673a732b279939', 'sys_org_code', '所属部门', NULL, 0, 1, 'string', 64, 0, '', '', '', '', 'text', '', 120, NULL, '0', '', '', 0, 0, 0, 0, 'single', '', '', 6, 'admin', '2019-07-01 16:28:20', '2019-07-01 16:26:42', 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `onl_cgform_field` VALUES ('df489194c6008f3bd21b2c1c11fde337', 'fb19fb067cd841f9ae93d4eb3b883dc0', 'create_time', '创建日期', NULL, 0, 1, 'Date', 20, 0, '', '', '', '', 'text', '', 120, '', '0', '', '', 0, 1, 1, 0, 'single', '', '', 2, NULL, NULL, '2019-03-23 11:39:48', 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `onl_cgform_field` VALUES ('df4c5e8411c102f02a6fe6727e858d55', '997ee931515a4620bc30a9c1246429a9', 'pic', '分类图片', NULL, 0, 1, 'string', 500, 0, '', '', '', '', 'image', '', 120, NULL, '0', '', '', 0, 1, 1, 0, 'single', '', '', 8, 'admin', '2020-05-03 00:57:44', '2020-05-03 00:56:56', 'admin', '', '', '', '', '', 'text', '0', NULL, NULL, '0');
INSERT INTO `onl_cgform_field` VALUES ('df58a7b1c3ed8f58a1994c0c0855db16', '4adec929a6594108bef5b35ee9966e9f', 'sys_org_code', '所属部门', NULL, 0, 1, 'string', 64, 0, '', '', '', '', 'text', '', 120, NULL, '0', '', '', 0, 0, 0, 0, 'single', '', '', 11, 'admin', '2020-04-10 19:43:38', '2020-04-10 19:35:58', 'admin', '', '', '', '', '', 'text', '0', NULL, NULL, '0');
INSERT INTO `onl_cgform_field` VALUES ('e09b191e3c37f9c89ae2192c75220b89', 'b81de38db24047b497d476516f8a0865', 'sys_org_code', '所属部门', NULL, 0, 1, 'string', 64, 0, '', '', '', '', 'text', '', 120, NULL, '0', '', '', 0, 0, 0, 0, 'single', '', '', 6, NULL, NULL, '2020-02-24 14:56:08', 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `onl_cgform_field` VALUES ('e0c5d6e483897d5c4e7894dc66dd1aff', '32feeb502544416c9bf41329c10a88f4', 'bpm_status', '流程状态', NULL, 0, 1, 'String', 2, 0, '', 'bpm_status', '', '', 'list', '', 120, NULL, '0', '', '', 0, 1, 1, 0, 'single', '', '', 9, 'admin', '2019-08-23 20:03:40', '2019-07-02 18:23:58', 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `onl_cgform_field` VALUES ('e24de426223dc0271a55eccc1d5457d0', '73162c3b8161413e8ecdca7eb288d0c9', 'create_by', '创建人', NULL, 0, 1, 'string', 50, 0, '', '', '', '', 'text', '', 120, NULL, '0', '', '', 0, 0, 0, 0, 'single', '', '', 2, 'admin', '2019-07-01 14:23:32', '2019-06-10 15:18:34', 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `onl_cgform_field` VALUES ('e2d73ccda7f10f5a1ccce3c48b1e699e', '402860816bff91c0016bffa220a9000b', 'residence_street', '户口所在街道', 'residence_street', 0, 1, 'string', 100, 0, '', '', '', '', 'text', '', 120, NULL, '0', '', '', 0, 1, 1, 0, 'group', '', '', 27, 'admin', '2019-07-22 16:15:32', '2019-07-17 19:12:24', 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `onl_cgform_field` VALUES ('e350986acb670f247df30fe4a44e73df', '09fd28e4b7184c1a9668496a5c496450', 'id', '主键', NULL, 1, 0, 'string', 36, 0, '', '', '', '', 'text', '', 120, NULL, '0', '', '', 0, 0, 0, 1, 'single', '', '', 1, 'admin', '2020-05-14 21:19:21', '2020-05-08 23:51:49', 'admin', '', '', '', '', '', NULL, '0', NULL, NULL, '0');
INSERT INTO `onl_cgform_field` VALUES ('e4914fdff68ac72486ada105e6e9fa36', 'e9faf717024b4aae95cff224ae9b6d97', 'id', '主键', NULL, 1, 0, 'string', 36, 0, '', '', '', '', 'text', '', 120, NULL, '0', '', '', 0, 0, 0, 0, 'single', '', '', 1, 'admin', '2019-07-03 18:23:49', '2019-07-03 18:22:35', 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `onl_cgform_field` VALUES ('e4a4c1d50b7b46678bc14fd5b90ee082', '73162c3b8161413e8ecdca7eb288d0c9', 'create_time', '创建日期', NULL, 0, 1, 'Date', 20, 0, '', '', '', '', 'datetime', '', 120, NULL, '0', '', '', 0, 0, 0, 0, 'single', '', '', 3, 'admin', '2019-07-01 14:23:32', '2019-06-10 15:18:34', 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `onl_cgform_field` VALUES ('e4d7f95340e73a54e8ff3f66b0613513', '56efb74326e74064b60933f6f8af30ea', 'create_time', '创建日期', NULL, 0, 1, 'Date', 20, 0, '', '', '', '', 'datetime', '', 120, NULL, '0', '', '', 0, 0, 0, 0, 'single', '', '', 3, 'admin', '2020-07-10 16:53:27', '2020-05-08 23:45:32', 'admin', '', '', '', '', '', NULL, '0', NULL, NULL, '0');
INSERT INTO `onl_cgform_field` VALUES ('e50b4398731e06572c247993a0dcc38d', 'd35109c3632c4952a19ecc094943dd71', 'name', '用户名', NULL, 0, 1, 'string', 200, 0, '', '', '', '', 'text', '', 120, '*', '0', '', '', 0, 1, 1, 0, 'single', '', '', 6, 'admin', '2020-05-06 11:33:01', '2019-03-15 14:24:35', 'admin', '', '', '', '', '', NULL, '0', NULL, NULL, '0');
INSERT INTO `onl_cgform_field` VALUES ('e53f53766d1f7718b3ee5eabe105b969', '402860816bff91c0016bffa220a9000b', 'social_insurance_time', '五险一金日期', 'social_insurance_time', 0, 1, 'Date', 0, 0, '', '', '', '', 'date', '', 120, NULL, '0', '', '', 0, 1, 1, 0, 'group', '', '', 38, 'admin', '2019-07-22 16:15:32', '2019-07-17 19:12:24', 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `onl_cgform_field` VALUES ('e60a8e496b0f7081dbfe4253b3218546', '402880eb71d61d3d0171d61d3de30000', 'sys_org_code', '所属部门', NULL, 0, 1, 'string', 64, 0, '', '', '', '', 'text', '', 120, NULL, '0', '', '', 0, 0, 0, 0, 'single', '', '', 10, 'admin', '2020-05-03 00:54:16', '2020-05-02 23:59:33', 'admin', '', '', '', '', '', 'text', '0', NULL, NULL, '0');
INSERT INTO `onl_cgform_field` VALUES ('e672d5974a06d5c37b3b4c94a6f29f96', '4028318169e81b970169e81b97650000', 'request_url', '请求路径', 'request_url', 0, 1, 'string', 255, 0, NULL, NULL, NULL, NULL, 'text', NULL, 120, NULL, '0', NULL, NULL, 0, 1, 1, 0, 'group', NULL, NULL, 9, NULL, NULL, '2019-04-04 19:28:36', 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `onl_cgform_field` VALUES ('e7aade12ca722b59c1ec681d14247ff8', '402860816bff91c0016bff91d8830007', 'sys_org_code', '组织机构编码', 'sys_org_code', 0, 1, 'string', 50, 0, '', '', '', '', 'text', '', 120, NULL, '0', '', '', 0, 1, 1, 0, 'group', '', '', 14, 'admin', '2019-07-19 18:04:41', '2019-07-17 18:54:37', 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `onl_cgform_field` VALUES ('e7c367221181aa9d1fc395e09796be8d', '402880eb71d52dc30171d52dc3a10000', 'create_time', '创建日期', NULL, 0, 1, 'Date', 20, 0, '', '', '', '', 'datetime', '', 120, NULL, '0', '', '', 0, 0, 0, 0, 'single', NULL, NULL, 7, NULL, NULL, '2020-05-02 19:37:58', 'admin', '', '', '', '', '', 'text', '0', NULL, NULL, '0');
INSERT INTO `onl_cgform_field` VALUES ('e7e622076bcbe7bf7ea9fd9acb6f19bc', '18f064d1ef424c93ba7a16148851664f', 'pic', '图片', NULL, 0, 1, 'string', 1000, 0, '', '', '', '', 'image', '', 120, NULL, '0', '', '', 0, 1, 1, 0, 'single', '', '', 14, 'admin', '2020-05-12 20:30:57', '2020-05-12 20:26:01', 'admin', '', '', '', '', '', 'text', '0', NULL, NULL, '0');
INSERT INTO `onl_cgform_field` VALUES ('e845925368919482df6dac58e6ed708d', '402860816bff91c0016bff91d8830007', 'phone', '联系方式', 'phone', 0, 1, 'string', 20, 0, '', '', '', '', 'text', '', 120, NULL, '0', '', '', 0, 1, 1, 0, 'group', '', '', 8, 'admin', '2019-07-19 18:04:41', '2019-07-17 18:54:37', 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `onl_cgform_field` VALUES ('e85295979d84bde27615a008d8bbadf1', '3b5e18da618247539481816995b6bf9f', 'sex', '性别', NULL, 0, 1, 'string', 32, 0, '', 'sex', '', '', 'list', '', 120, NULL, '0', '', '', 0, 1, 1, 0, 'single', '', '', 9, 'admin', '2020-07-11 11:27:29', '2020-07-11 11:27:17', 'admin', '', '', '', '', '', 'text', '0', NULL, NULL, '0');
INSERT INTO `onl_cgform_field` VALUES ('e878e70574d0d6180c2f6f0d281c9262', '8d66ea41c7cc4ef9ab3aab9055657fc9', 'pid', '父级节点', NULL, 0, 1, 'string', 32, 0, '', '', '', '', 'text', '', 120, NULL, '0', '', '', 0, 1, 0, 0, 'single', '', '', 10, NULL, NULL, '2020-05-07 22:46:32', 'admin', '', '', '', '', '', 'text', '0', NULL, NULL, '0');
INSERT INTO `onl_cgform_field` VALUES ('e88d328af34dd8a17f51437c52b68a2d', '402860816bff91c0016bff91cfea0004', 'create_by', '创建人', 'create_by', 0, 1, 'string', 100, 0, '', '', '', '', 'text', '', 120, NULL, '0', '', '', 0, 1, 1, 0, 'group', '', '', 9, 'admin', '2019-07-19 18:05:13', '2019-07-17 18:54:35', 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `onl_cgform_field` VALUES ('e99cc08f4d88dd8f788399db8d448ee8', '62e29cdb81ac44d1a2d8ff89851b853d', 'create_by', '创建人', NULL, 0, 1, 'string', 50, 0, '', '', '', '', 'text', '', 120, NULL, '0', '', '', 0, 0, 0, 0, 'single', '', '', 2, NULL, NULL, '2019-05-11 14:01:14', 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `onl_cgform_field` VALUES ('e9fb150adabaaacdd804b1953e6bad24', '05a3a30dada7411c9109306aa4117068', 'create_time', '创建日期', NULL, 0, 1, 'Date', 20, 0, '', '', '', '', 'datetime', '', 120, NULL, '0', '', '', 0, 0, 0, 0, 'single', '', '', 3, 'admin', '2020-05-12 22:39:41', '2020-05-06 11:34:31', 'admin', '', '', '', '', '', NULL, '0', NULL, NULL, '0');
INSERT INTO `onl_cgform_field` VALUES ('ea309f9cd81ede7b9b36a1377a3a40d8', '997ee931515a4620bc30a9c1246429a9', 'update_by', '更新人', NULL, 0, 1, 'string', 50, 0, '', '', '', '', 'text', '', 120, NULL, '0', '', '', 0, 0, 0, 0, 'single', '', '', 4, 'admin', '2020-05-03 00:57:44', '2020-05-03 00:56:56', 'admin', '', '', '', '', '', NULL, '0', NULL, NULL, '0');
INSERT INTO `onl_cgform_field` VALUES ('ea644c4c208739640933ba6e568045c1', 'e2faf977fdaf4b25a524f58c2441a51c', 'ruz_date', '入职时间', NULL, 0, 1, 'Date', 32, 0, '', '', '', '', 'date', '', 120, NULL, '0', '', '', 0, 1, 1, 0, 'single', '', '', 9, 'admin', '2019-06-10 17:27:00', '2019-04-24 17:12:11', 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `onl_cgform_field` VALUES ('ea96d1c33f0f0d7245045e70a5793257', '402860816bff91c0016bffa220a9000b', 'current_postcode', '现居住地邮编', 'current_postcode', 0, 1, 'string', 10, 0, '', '', '', '', 'text', '', 120, NULL, '0', '', '', 0, 1, 1, 0, 'group', '', '', 31, 'admin', '2019-07-22 16:15:32', '2019-07-17 19:12:24', 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `onl_cgform_field` VALUES ('ebc41362681919cc680fcc58bf87fdcb', '1acb6f81a1d9439da6cc4e868617b565', 'price', '单价', NULL, 0, 1, 'double', 10, 2, '', '', '', '', 'text', '', 120, NULL, '0', '', '', 0, 1, 1, 0, 'single', '', '', 12, 'admin', '2019-06-10 14:47:14', '2019-04-23 22:58:19', 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `onl_cgform_field` VALUES ('ec20e66d5eb9b8b7f58de9edc0f7630b', '1acb6f81a1d9439da6cc4e868617b565', 'create_by', '创建人', NULL, 0, 1, 'string', 50, 0, '', '', '', '', 'text', '', 120, NULL, '0', '', '', 0, 0, 0, 0, 'single', '', '', 2, 'admin', '2019-06-10 14:47:14', '2019-04-23 22:58:19', 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `onl_cgform_field` VALUES ('ec5e9cb5809b2f8ce1446df4a27693f0', '27fc5f91274344afa7673a732b279939', 'name', '用户名', NULL, 0, 1, 'String', 32, 0, '', '', '', '', 'text', '', 120, NULL, '0', '', '', 0, 1, 1, 0, 'single', '', '', 7, 'admin', '2019-07-01 16:28:20', '2019-07-01 16:26:42', 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `onl_cgform_field` VALUES ('ed16f23d08e7bcda11a1383fda68057e', '402860816bff91c0016bff91c7010001', 'employee_id', '员工ID', 'employee_id', 0, 1, 'string', 32, 0, '', '', '', '', 'text', '', 120, NULL, '0', '', '', 0, 1, 1, 0, 'group', 'oa_employee_info', 'id', 2, 'admin', '2019-07-19 18:07:47', '2019-07-17 18:54:32', 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `onl_cgform_field` VALUES ('edda30c64e1dccee510d40b77a8ca094', 'fb7125a344a649b990c12949945cb6c1', 'update_by', '更新人', NULL, 0, 1, 'string', 50, 0, '', '', '', '', 'text', '', 120, '', '0', '', '', 0, 0, 0, 0, 'single', '', '', 3, 'admin', '2019-03-26 19:24:11', '2019-03-26 19:01:52', 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `onl_cgform_field` VALUES ('ee09e0e21fa350b9346b70292dcfca00', '79091e8277c744158530321513119c68', 'id', '主键', NULL, 1, 0, 'string', 36, 0, '', '', '', '', 'text', '', 120, NULL, '0', '', '', 0, 0, 0, 0, 'single', '', '', 1, 'admin', '2019-05-11 15:29:47', '2019-05-11 15:27:17', 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `onl_cgform_field` VALUES ('ee4ffe04a25fcf556e78183f1f521546', '402860816aa5921f016aa5921f480000', 'create_by', '创建人id', 'create_by', 0, 1, 'string', 32, 0, '', '', '', '', 'text', '', 120, NULL, '0', '', '', 0, 0, 0, 0, 'group', '', '', 8, 'admin', '2019-05-11 15:31:54', '2019-05-11 14:26:19', 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `onl_cgform_field` VALUES ('ee5803611f63643059b6375166d71567', '402860816bff91c0016bff91c7010001', 'create_time', '创建时间', 'create_time', 0, 1, 'Date', 0, 0, '', '', '', '', 'date', '', 120, NULL, '0', '', '', 0, 1, 1, 0, 'group', '', '', 8, 'admin', '2019-07-19 18:07:47', '2019-07-17 18:54:32', 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `onl_cgform_field` VALUES ('eec5bc01720642ccc635c7fc2e9b1eb8', '86bf17839a904636b7ed96201b2fa6ea', 'update_by', '更新人', NULL, 0, 1, 'string', 50, 0, '', '', '', '', 'text', '', 120, NULL, '0', '', '', 0, 0, 0, 0, 'single', '', '', 4, 'admin', '2020-05-14 21:18:49', '2020-05-08 23:48:31', 'admin', '', '', '', '', '', NULL, '0', NULL, NULL, '0');
INSERT INTO `onl_cgform_field` VALUES ('ef81373c5fd7130d7e23859d90c9eb3e', '402860816bff91c0016bff91cda80003', 'from_time', '开始日期', 'from_time', 0, 1, 'Date', 0, 0, '', '', '', '', 'date', '', 120, NULL, '0', '', '', 0, 1, 1, 0, 'group', '', '', 3, 'admin', '2019-07-19 18:06:36', '2019-07-17 18:54:34', 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `onl_cgform_field` VALUES ('efd1b955a75b5046e9857e00fe94ae2c', 'fbc35f067da94a70adb622ddba259352', 'id', '主键', NULL, 1, 0, 'string', 36, 0, '', '', '', '', 'text', '', 120, NULL, '0', '', '', 0, 0, 0, 0, 'single', '', '', 1, NULL, NULL, '2019-07-03 19:44:23', 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `onl_cgform_field` VALUES ('f015cc2ffdcc2c4be1e9b3622eb69b52', 'fbc35f067da94a70adb622ddba259352', 'sys_org_code', '所属部门', NULL, 0, 1, 'string', 64, 0, '', '', '', '', 'text', '', 120, NULL, '0', '', '', 0, 0, 0, 0, 'single', '', '', 6, NULL, NULL, '2019-07-03 19:44:23', 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `onl_cgform_field` VALUES ('f06b2bb01ea1fae487b7e3c3eb521d5b', 'd3ae1c692b9640e0a091f8c46e17bb01', 'create_by', '创建人', NULL, 0, 1, 'string', 50, 0, '', '', '', '', 'text', '', 120, NULL, '0', '', '', 0, 0, 0, 0, 'single', '', '', 2, NULL, NULL, '2019-07-24 14:47:30', 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `onl_cgform_field` VALUES ('f0967fd139b440f79f21248bf4e4a209', 'd3ae1c692b9640e0a091f8c46e17bb01', 'update_time', '更新日期', NULL, 0, 1, 'Date', 20, 0, '', '', '', '', 'datetime', '', 120, NULL, '0', '', '', 0, 0, 0, 0, 'single', '', '', 5, NULL, NULL, '2019-07-24 14:47:30', 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `onl_cgform_field` VALUES ('f0a453930aa16ca32f2e3be860bfe542', '402860816bff91c0016bffa220a9000b', 'education', '学历', 'education', 0, 1, 'string', 100, 0, '', '', '', '', 'text', '', 120, NULL, '0', '', '', 0, 1, 1, 0, 'group', '', '', 14, 'admin', '2019-07-22 16:15:32', '2019-07-17 19:12:24', 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `onl_cgform_field` VALUES ('f12753b4a3815697a72017a7436fe733', 'e2faf977fdaf4b25a524f58c2441a51c', 'update_time', '更新日期', NULL, 0, 1, 'Date', 20, 0, '', '', '', '', 'datetime', '', 120, NULL, '0', '', '', 0, 0, 0, 0, 'single', '', '', 5, 'admin', '2019-06-10 17:27:00', '2019-04-24 17:12:11', 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `onl_cgform_field` VALUES ('f1ab7d3b55ade57eeac6c55b32ce813a', '1acb6f81a1d9439da6cc4e868617b565', 'update_by', '更新人', NULL, 0, 1, 'string', 50, 0, '', '', '', '', 'text', '', 120, NULL, '0', '', '', 0, 0, 0, 0, 'single', '', '', 4, 'admin', '2019-06-10 14:47:14', '2019-04-23 22:58:19', 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `onl_cgform_field` VALUES ('f1c7fde21f26c7ed64a0ef1095900c52', '4028318169e81b970169e81b97650000', 'request_type', '请求类型', 'request_type', 0, 1, 'string', 10, 0, NULL, NULL, NULL, NULL, 'text', NULL, 120, NULL, '0', NULL, NULL, 0, 1, 1, 0, 'group', NULL, NULL, 11, NULL, NULL, '2019-04-04 19:28:36', 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `onl_cgform_field` VALUES ('f3284a1ce6da9b887dce89091eaa0f6b', '402880e570a5d7000170a5d700f50000', 'pro_type', '产品类型', NULL, 0, 1, 'String', 32, 0, '', 'sex', '', '', 'radio', '', 120, NULL, '0', '', '', 0, 1, 1, 0, 'single', NULL, NULL, 9, NULL, NULL, '2020-03-04 21:58:16', 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `onl_cgform_field` VALUES ('f38319e2add8da6a7223d77359144a22', '402880e5721355dd01721355dd390000', 'update_by', '更新人', NULL, 0, 1, 'string', 50, 0, '', '', '', '', 'text', '', 120, NULL, '0', '', '', 0, 0, 0, 0, 'single', NULL, NULL, 4, NULL, NULL, '2020-05-14 21:18:14', 'admin', '', '', '', '', '', NULL, '0', NULL, NULL, '0');
INSERT INTO `onl_cgform_field` VALUES ('f4647a91a4ac5d6d32bb0692b800bffe', '402860816bff91c0016bff91c0cb0000', 'probation_post', '试用期职位', 'probation_post', 0, 1, 'string', 100, 0, '', '', '', '', 'text', '', 120, NULL, '0', '', '', 0, 1, 1, 0, 'group', '', '', 8, 'admin', '2019-07-19 18:09:01', '2019-07-17 18:54:32', 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `onl_cgform_field` VALUES ('f4b0bc7f3d6562e28d7c5e2d56510ecd', 'e5464aa8fa7b47c580e91593cf9b46dc', 'first_num', '首次数量', NULL, 0, 1, 'String', 32, 0, '', '', '', '', 'text', '', 120, NULL, '0', '', '', 0, 1, 1, 0, 'single', '', '', 8, 'admin', '2019-04-24 17:09:49', '2019-04-24 14:31:31', 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `onl_cgform_field` VALUES ('f558359b06aea79a992c102ce3563a4d', '4028318169e81b970169e81b97650000', 'username', '操作用户名称', 'username', 0, 1, 'string', 100, 0, NULL, NULL, NULL, NULL, 'text', NULL, 120, NULL, '0', NULL, NULL, 0, 1, 1, 0, 'group', NULL, NULL, 6, NULL, NULL, '2019-04-04 19:28:36', 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `onl_cgform_field` VALUES ('f5d2475aec4b9016dfc0c8772e1704ea', '402880eb71d52dc30171d52dc3a10000', 'code', '编码', NULL, 0, 1, 'String', 200, 0, '', '', '', '', 'text', '', 120, NULL, '0', '', '${order_num_rule_param}', 0, 1, 1, 0, 'single', NULL, NULL, 2, NULL, NULL, '2020-05-02 19:37:59', 'admin', '', '', '', '', '', NULL, '0', NULL, NULL, '0');
INSERT INTO `onl_cgform_field` VALUES ('f6afcb7d8ea81879593ff737b55ddcc0', '402860816bff91c0016bff91cda80003', 'id', 'id', 'id', 1, 1, 'string', 32, 0, '', '', '', '', 'text', '', 120, NULL, '0', '', '', 0, 0, 0, 0, 'group', '', '', 1, 'admin', '2019-07-19 18:06:36', '2019-07-17 18:54:34', 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `onl_cgform_field` VALUES ('f6f8aed87ec73994f6a12abbc079dbb1', '402860816bff91c0016bffa220a9000b', 'update_by', '更新人', 'update_by', 0, 1, 'string', 100, 0, '', '', '', '', 'text', '', 120, NULL, '0', '', '', 0, 1, 1, 0, 'group', '', '', 41, 'admin', '2019-07-22 16:15:32', '2019-07-17 19:12:24', 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `onl_cgform_field` VALUES ('f7332af7586c83c87f7b9ea144a5292d', '62e29cdb81ac44d1a2d8ff89851b853d', 'update_by', '更新人', NULL, 0, 1, 'string', 50, 0, '', '', '', '', 'text', '', 120, NULL, '0', '', '', 0, 0, 0, 0, 'single', '', '', 4, NULL, NULL, '2019-05-11 14:01:14', 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `onl_cgform_field` VALUES ('f76719783433487f4710232e2ae0e521', '402860816bff91c0016bff91cfea0004', 'id', 'id', 'id', 1, 1, 'string', 32, 0, '', '', '', '', 'text', '', 120, NULL, '0', '', '', 0, 0, 0, 0, 'group', '', '', 1, 'admin', '2019-07-19 18:05:13', '2019-07-17 18:54:35', 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `onl_cgform_field` VALUES ('f7e7eb84ddc34d7e09d10af213ac6667', '402860816bff91c0016bff91d2810005', 'create_time', '创建时间', 'create_time', 0, 1, 'Date', 0, 0, '', '', '', '', 'date', '', 120, NULL, '0', '', '', 0, 1, 1, 0, 'group', '', '', 11, 'admin', '2019-07-19 18:05:55', '2019-07-17 18:54:35', 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `onl_cgform_field` VALUES ('f81d7103c0c857e5c744cda2bc4c000a', '402860816bff91c0016bff91ca7e0002', 'cert_organizations', '发证机关', 'cert_organizations', 0, 1, 'string', 255, 0, '', '', '', '', 'text', '', 120, NULL, '0', '', '', 0, 1, 1, 0, 'group', '', '', 6, 'admin', '2019-07-19 18:07:13', '2019-07-17 18:54:33', 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `onl_cgform_field` VALUES ('f82db8edef5babf741914b0fa221329b', '4028839a6de2ebd3016de2ebd3870000', 'bpm_status', '流程状态', 'bpm_status', 0, 1, 'string', 2, 0, NULL, NULL, NULL, NULL, 'text', NULL, 120, NULL, '0', NULL, NULL, 0, 1, 1, 0, 'group', NULL, NULL, 16, NULL, NULL, '2019-10-19 15:29:30', 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `onl_cgform_field` VALUES ('f857d4fd2d63c1ad94ed4698f5b173f5', '05a3a30dada7411c9109306aa4117068', 'id', '主键', NULL, 1, 0, 'string', 36, 0, '', '', '', '', 'text', '', 120, NULL, '0', '', '', 0, 0, 0, 1, 'single', '', '', 1, 'admin', '2020-05-12 22:39:41', '2020-05-06 11:34:31', 'admin', '', '', '', '', '', NULL, '0', NULL, NULL, '0');
INSERT INTO `onl_cgform_field` VALUES ('f8a0fd20a1173270afdfed1129d5c669', '402860816bff91c0016bffa220a9000b', 'depart_id', '所在部门id', NULL, 0, 1, 'String', 32, 0, '', '', '', '', 'text', '', 120, NULL, '0', '', '', 0, 1, 1, 0, 'single', '', '', 44, 'admin', '2019-07-22 16:15:32', '2019-07-19 15:33:44', 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `onl_cgform_field` VALUES ('f8cc6be747fec10802eb625ac529c16f', '402860816bff91c0016bff91cfea0004', 'create_time', '创建时间', 'create_time', 0, 1, 'Date', 0, 0, '', '', '', '', 'date', '', 120, NULL, '0', '', '', 0, 1, 1, 0, 'group', '', '', 10, 'admin', '2019-07-19 18:05:13', '2019-07-17 18:54:35', 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `onl_cgform_field` VALUES ('f90bcb38fb89988bd40d1618aa75cea0', '758334cb1e7445e2822b60e807aec4a3', 'id', '主键', NULL, 1, 0, 'string', 36, 0, '', '', '', '', 'text', '', 120, NULL, '0', '', '', 0, 0, 0, 0, 'single', '', '', 1, NULL, NULL, '2019-10-18 18:02:09', 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `onl_cgform_field` VALUES ('f94db83e41c69f407d3c9a81c5892269', '402860816bff91c0016bffa220a9000b', 'first_job_time', '首次工作时间', 'first_job_time', 0, 1, 'Date', 0, 0, '', '', '', '', 'date', '', 120, NULL, '0', '', '', 0, 1, 1, 0, 'group', '', '', 22, 'admin', '2019-07-22 16:15:32', '2019-07-17 19:12:24', 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `onl_cgform_field` VALUES ('f95d2cbefd25444909c83aaf8c4f72fb', '402860816bff91c0016bff91ca7e0002', 'memo', '备注', 'memo', 0, 1, 'string', 255, 0, '', '', '', '', 'text', '', 120, NULL, '0', '', '', 0, 1, 1, 0, 'group', '', '', 7, 'admin', '2019-07-19 18:07:13', '2019-07-17 18:54:33', 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `onl_cgform_field` VALUES ('fa3a12d7abf72b23afe425f8dbd57f86', '1acb6f81a1d9439da6cc4e868617b565', 'size_type', '尺码类型', NULL, 0, 1, 'String', 2, 0, '', 'air_china_size', '', '', 'list', '', 120, NULL, '0', '', '', 0, 1, 1, 0, 'single', '', '', 9, 'admin', '2019-06-10 14:47:14', '2019-04-23 22:58:19', 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `onl_cgform_field` VALUES ('fa8f5a0ba673e0208934567462844eab', '402860816bff91c0016bff91ca7e0002', 'sys_org_code', '组织机构编码', 'sys_org_code', 0, 1, 'string', 50, 0, '', '', '', '', 'text', '', 120, NULL, '0', '', '', 0, 1, 1, 0, 'group', '', '', 13, 'admin', '2019-07-19 18:07:13', '2019-07-17 18:54:33', 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `onl_cgform_field` VALUES ('faaaca21b7b2b16089c885f3224e4dc5', '4fb8e12a697f4d5bbe9b9fb1e9009486', 'main_id', '主表ID', NULL, 0, 1, 'String', 200, 0, '', '', '', '', 'text', '', 120, NULL, '0', '', '', 0, 0, 0, 0, 'single', 'demo_field_def_val_main', 'id', 5, 'admin', '2020-04-10 19:51:27', '2020-04-10 19:47:55', 'admin', '', '', '', '', '', NULL, '0', NULL, NULL, '0');
INSERT INTO `onl_cgform_field` VALUES ('fafb32cf7e63bca93bbd70b0a0ea11fc', '758334cb1e7445e2822b60e807aec4a3', 'sys_org_code', '所属部门', NULL, 0, 1, 'string', 64, 0, '', '', '', '', 'text', '', 120, NULL, '0', '', '', 0, 0, 0, 0, 'single', '', '', 6, NULL, NULL, '2019-10-18 18:02:09', 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `onl_cgform_field` VALUES ('fb56aeb6b3a5a0a974ef62f34727eea6', '402880e5721355dd01721355dd390000', 'sys_org_code', '所属部门', NULL, 0, 1, 'string', 64, 0, '', '', '', '', 'text', '', 120, NULL, '0', '', '', 0, 0, 0, 0, 'single', NULL, NULL, 6, NULL, NULL, '2020-05-14 21:18:14', 'admin', '', '', '', '', '', NULL, '0', NULL, NULL, '0');
INSERT INTO `onl_cgform_field` VALUES ('fc55d467102c2c782286f546d7820c3d', '73162c3b8161413e8ecdca7eb288d0c9', 'pid', '父物料', NULL, 0, 1, 'String', 36, 0, '', '', '', '', 'text', '', 120, NULL, '0', '', '', 0, 1, 1, 0, 'single', '', '', 9, 'admin', '2019-07-01 14:23:32', '2019-06-10 15:18:34', 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `onl_cgform_field` VALUES ('fc76a3832d232829852cae6c66e44f67', '402860816bff91c0016bffa220a9000b', 'identity_no', '身份证号', 'identity_no', 0, 1, 'string', 20, 0, '', '', '', '', 'text', '', 120, NULL, '0', '', '', 0, 1, 1, 0, 'group', '', '', 21, 'admin', '2019-07-22 16:15:32', '2019-07-17 19:12:24', 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `onl_cgform_field` VALUES ('fcd519058d68fa4dab192335602b5d24', '402860816bff91c0016bffa220a9000b', 'real_name', '姓名', 'real_name', 0, 1, 'string', 100, 0, '', '', '', '', 'text', '', 120, NULL, '0', '', '', 0, 1, 1, 0, 'group', '', '', 5, 'admin', '2019-07-22 16:15:32', '2019-07-17 19:12:24', 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `onl_cgform_field` VALUES ('fd0586cae06685959415d9017b2bdf49', '758334cb1e7445e2822b60e807aec4a3', 'create_by', '创建人', NULL, 0, 1, 'string', 50, 0, '', '', '', '', 'text', '', 120, NULL, '0', '', '', 0, 0, 0, 0, 'single', '', '', 2, NULL, NULL, '2019-10-18 18:02:09', 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `onl_cgform_field` VALUES ('fde00160a5d664effaa4b5552e814e74', 'fb7125a344a649b990c12949945cb6c1', 'sex', '性别', NULL, 0, 1, 'string', 32, 0, '', '', '', '', 'text', '', 120, '', '0', '', '', 0, 1, 1, 0, 'single', '', '', 6, 'admin', '2019-03-26 19:24:11', '2019-03-26 19:01:52', 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `onl_cgform_field` VALUES ('fed133a00f57245d4cfb02dd3c3ce7c1', '4adec929a6594108bef5b35ee9966e9f', 'update_by', '更新人', NULL, 0, 1, 'string', 50, 0, '', '', '', '', 'text', '', 120, NULL, '0', '', '', 0, 0, 0, 0, 'single', '', '', 9, 'admin', '2020-04-10 19:43:38', '2020-04-10 19:35:58', 'admin', '', '', '', '', '', 'text', '0', NULL, NULL, '0');
INSERT INTO `onl_cgform_field` VALUES ('fef2add806c5e1ddd01c79c7e84f5a65', 'cb2d8534a2f544bc9c618dd49da66336', 'cc', 'cc', NULL, 0, 1, 'String', 32, 0, '', '', '', '', 'text', '', 120, NULL, '0', '', '', 0, 1, 1, 0, 'single', '', '', 7, NULL, NULL, '2020-02-24 17:22:42', 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `onl_cgform_field` VALUES ('ff2a099186b13d3ecb0764f7f22b1fbf', '09fd28e4b7184c1a9668496a5c496450', 'name', '客户名字', NULL, 0, 1, 'string', 32, 0, '', 'realname,sex,birthday', 'tj_user_report', 'name,sex,birthday', 'popup', '', 120, NULL, '0', '', '', 0, 1, 1, 0, 'single', '', '', 7, 'admin', '2020-05-14 21:19:21', '2020-05-08 23:51:49', 'admin', '', '', '', '', '', 'text', '0', NULL, NULL, '0');
INSERT INTO `onl_cgform_field` VALUES ('ff49b468e54e137032f7e4d976b83b5a', '402860816bff91c0016bffa220a9000b', 'politically_status', '政治面貌', 'politically_status', 0, 1, 'string', 20, 0, '', '', '', '', 'text', '', 120, NULL, '0', '', '', 0, 1, 1, 0, 'group', '', '', 8, 'admin', '2019-07-22 16:15:32', '2019-07-17 19:12:24', 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `onl_cgform_field` VALUES ('ff601f75d0e7ced226748eb8fba2c896', '402860816bff91c0016bff91d8830007', 'relation', '关系', 'relation', 0, 1, 'string', 100, 0, '', '', '', '', 'text', '', 120, NULL, '0', '', '', 0, 1, 1, 0, 'group', '', '', 4, 'admin', '2019-07-19 18:04:41', '2019-07-17 18:54:37', 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `onl_cgform_field` VALUES ('ffacafee9fa46eb297ca3252f95acef9', '402860816bff91c0016bffa220a9000b', 'school', '毕业学校', 'school', 0, 1, 'string', 100, 0, '', '', '', '', 'text', '', 120, NULL, '0', '', '', 0, 1, 1, 0, 'group', '', '', 9, 'admin', '2019-07-22 16:15:32', '2019-07-17 19:12:24', 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `onl_cgform_field` VALUES ('ffcbf379fffabbd13aa2c22ce565ec12', '79091e8277c744158530321513119c68', 'update_by', '更新人', NULL, 0, 1, 'string', 50, 0, '', '', '', '', 'text', '', 120, NULL, '0', '', '', 0, 0, 0, 0, 'single', '', '', 4, 'admin', '2019-05-11 15:29:47', '2019-05-11 15:27:17', 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

-- ----------------------------
-- Table structure for onl_cgform_head
-- ----------------------------
DROP TABLE IF EXISTS `onl_cgform_head`;
CREATE TABLE `onl_cgform_head`  (
  `id` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '主键ID',
  `table_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '表名',
  `table_type` int(11) NOT NULL COMMENT '表类型: 0单表、1主表、2附表',
  `table_version` int(11) NULL DEFAULT 1 COMMENT '表版本',
  `table_txt` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '表说明',
  `is_checkbox` varchar(5) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '是否带checkbox',
  `is_db_synch` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT 'N' COMMENT '同步数据库状态',
  `is_page` varchar(5) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '是否分页',
  `is_tree` varchar(5) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '是否是树',
  `id_sequence` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '主键生成序列',
  `id_type` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '主键类型',
  `query_mode` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '查询模式',
  `relation_type` int(11) NULL DEFAULT NULL COMMENT '映射关系 0一对多  1一对一',
  `sub_table_str` varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '子表',
  `tab_order_num` int(11) NULL DEFAULT NULL COMMENT '附表排序序号',
  `tree_parent_id_field` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '树形表单父id',
  `tree_id_field` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '树表主键字段',
  `tree_fieldname` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '树开表单列字段',
  `form_category` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT 'bdfl_ptbd' COMMENT '表单分类',
  `form_template` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'PC表单模板',
  `form_template_mobile` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '表单模板样式(移动端)',
  `scroll` int(3) NULL DEFAULT 0 COMMENT '是否有横向滚动条',
  `copy_version` int(11) NULL DEFAULT NULL COMMENT '复制版本号',
  `copy_type` int(3) NULL DEFAULT 0 COMMENT '复制表类型1为复制表 0为原始表',
  `physic_id` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '原始表ID',
  `update_by` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '修改人',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '修改时间',
  `create_by` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '创建人',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `theme_template` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '主题模板',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `index_onlineform_table_name`(`table_name`) USING BTREE,
  INDEX `index_form_templdate`(`form_template`) USING BTREE,
  INDEX `index_templdate_mobile`(`form_template_mobile`) USING BTREE,
  INDEX `index_onlineform_table_version`(`table_version`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of onl_cgform_head
-- ----------------------------
INSERT INTO `onl_cgform_head` VALUES ('05a3a30dada7411c9109306aa4117068', 'test_note', 1, 10, '测试请假单', 'Y', 'Y', 'Y', 'N', NULL, 'UUID', 'single', NULL, NULL, NULL, NULL, NULL, NULL, 'temp', '1', NULL, 1, NULL, 0, NULL, 'admin', '2020-05-12 22:39:41', 'admin', '2020-05-06 11:34:31', 'normal');
INSERT INTO `onl_cgform_head` VALUES ('09fd28e4b7184c1a9668496a5c496450', 'ces_order_customer', 3, 5, '订单客户', 'Y', 'Y', 'Y', 'N', NULL, 'UUID', 'single', 1, 'ces_order_customer', 3, NULL, NULL, NULL, 'temp', '1', NULL, 1, NULL, 0, NULL, 'admin', '2020-05-14 21:19:21', 'admin', '2020-05-08 23:51:49', 'normal');
INSERT INTO `onl_cgform_head` VALUES ('18f064d1ef424c93ba7a16148851664f', 'ces_field_kongj', 1, 5, 'online表单控件', 'Y', 'Y', 'Y', 'N', NULL, 'UUID', 'single', NULL, NULL, NULL, NULL, NULL, NULL, 'temp', '2', NULL, 1, NULL, 0, NULL, 'admin', '2020-05-12 20:30:57', 'admin', '2020-05-12 20:26:01', 'normal');
INSERT INTO `onl_cgform_head` VALUES ('3d447fa919b64f6883a834036c14aa67', 'test_enhance_select', 1, 5, 'js增强实现下拉联动效果', 'N', 'Y', 'Y', 'N', NULL, 'UUID', 'single', NULL, NULL, NULL, NULL, NULL, NULL, 'bdfl_include', '1', NULL, 0, NULL, 0, NULL, 'admin', '2020-02-21 17:58:46', 'admin', '2020-02-20 16:19:00', 'normal');
INSERT INTO `onl_cgform_head` VALUES ('402880e570a5d7000170a5d700f50000', 'test_order_product$1', 1, 11, '订单产品明细', 'N', 'N', 'Y', 'N', NULL, 'UUID', 'single', NULL, NULL, NULL, NULL, NULL, NULL, 'bdfl_include', '1', NULL, 0, 1, 1, 'deea5a8ec619460c9245ba85dbc59e80', NULL, NULL, 'admin', '2020-03-04 21:58:16', NULL);
INSERT INTO `onl_cgform_head` VALUES ('402880e5721355dd01721355dd390000', 'ces_order_goods$1', 1, 1, '订单商品', 'Y', 'N', 'Y', 'N', NULL, 'UUID', 'single', NULL, NULL, NULL, NULL, NULL, NULL, 'temp', '1', NULL, 1, 1, 1, '86bf17839a904636b7ed96201b2fa6ea', NULL, NULL, 'admin', '2020-05-14 21:18:14', 'normal');
INSERT INTO `onl_cgform_head` VALUES ('402880eb71d61d3d0171d61d3de30000', 'demo_field_def_val_sub$1', 1, 2, '示例：控件默认值（子表）', 'Y', 'N', 'Y', 'N', NULL, 'UUID', 'single', NULL, NULL, NULL, NULL, NULL, NULL, 'demo', '1', NULL, 1, 1, 1, '4fb8e12a697f4d5bbe9b9fb1e9009486', 'admin', '2020-05-03 00:54:16', 'admin', '2020-05-02 23:59:33', 'normal');
INSERT INTO `onl_cgform_head` VALUES ('4adec929a6594108bef5b35ee9966e9f', 'demo_field_def_val_main', 2, 1, '示例：控件默认值（主表）', 'Y', 'Y', 'Y', 'N', NULL, 'UUID', 'single', NULL, 'demo_field_def_val_sub', NULL, NULL, NULL, NULL, 'demo', '1', NULL, 1, NULL, 0, NULL, 'admin', '2020-05-02 19:37:57', 'admin', '2020-04-10 19:35:57', 'normal');
INSERT INTO `onl_cgform_head` VALUES ('4fb8e12a697f4d5bbe9b9fb1e9009486', 'demo_field_def_val_sub', 3, 1, '示例：控件默认值（子表）', 'Y', 'Y', 'Y', 'N', NULL, 'UUID', 'single', 0, NULL, 1, NULL, NULL, NULL, 'demo', '1', NULL, 1, NULL, 0, NULL, 'admin', '2020-05-02 19:37:54', 'admin', '2020-04-10 19:47:01', 'normal');
INSERT INTO `onl_cgform_head` VALUES ('53a3e82b54b946c2b904f605875a275c', 'ces_shop_goods', 1, 7, '商品', 'Y', 'Y', 'Y', 'N', NULL, 'UUID', 'single', NULL, NULL, NULL, NULL, NULL, NULL, 'temp', '1', NULL, 1, NULL, 0, NULL, 'admin', '2020-05-08 23:42:51', 'admin', '2020-05-07 22:49:47', 'normal');
INSERT INTO `onl_cgform_head` VALUES ('56870166aba54ebfacb20ba6c770bd73', 'test_order_main', 2, 12, '测试订单主表', 'N', 'Y', 'Y', 'N', NULL, 'UUID', 'single', NULL, 'test_order_product', NULL, NULL, NULL, NULL, 'bdfl_include', '2', NULL, 0, NULL, 0, NULL, 'admin', '2020-07-10 17:09:01', 'admin', '2019-04-20 11:38:39', 'innerTable');
INSERT INTO `onl_cgform_head` VALUES ('56efb74326e74064b60933f6f8af30ea', 'ces_order_main', 2, 9, '商城订单表', 'Y', 'Y', 'Y', 'N', NULL, 'UUID', 'single', NULL, 'ces_order_goods,ces_order_customer', NULL, NULL, NULL, NULL, 'temp', '1', NULL, 1, NULL, 0, NULL, 'admin', '2020-07-10 16:53:27', 'admin', '2020-05-08 23:45:32', 'normal');
INSERT INTO `onl_cgform_head` VALUES ('86bf17839a904636b7ed96201b2fa6ea', 'ces_order_goods', 3, 2, '订单商品', 'Y', 'Y', 'Y', 'N', NULL, 'UUID', 'single', 0, NULL, 2, NULL, NULL, NULL, 'temp', '1', NULL, 1, NULL, 0, NULL, 'admin', '2020-05-14 21:18:49', 'admin', '2020-05-08 23:48:31', 'normal');
INSERT INTO `onl_cgform_head` VALUES ('8d66ea41c7cc4ef9ab3aab9055657fc9', 'ces_shop_type', 1, 1, '商品分类', 'Y', 'Y', 'Y', 'Y', NULL, 'UUID', 'single', NULL, NULL, NULL, 'pid', 'has_child', 'name', 'temp', '1', NULL, 1, NULL, 0, NULL, 'admin', '2020-05-07 22:46:40', 'admin', '2020-05-07 22:46:31', 'normal');
INSERT INTO `onl_cgform_head` VALUES ('997ee931515a4620bc30a9c1246429a9', 'test_shoptype_tree', 1, 2, '商品分类', 'Y', 'Y', 'Y', 'Y', NULL, 'UUID', 'single', NULL, NULL, NULL, 'pid', 'has_child', 'type_name', 'temp', '1', NULL, 1, NULL, 0, NULL, 'admin', '2020-05-03 00:57:47', 'admin', '2020-05-03 00:56:56', 'normal');
INSERT INTO `onl_cgform_head` VALUES ('d35109c3632c4952a19ecc094943dd71', 'test_demo', 1, 21, '测试用户表', 'Y', 'Y', 'Y', 'N', NULL, 'UUID', 'single', NULL, NULL, NULL, NULL, NULL, NULL, 'bdfl_include', '1', NULL, 0, NULL, 0, NULL, 'admin', '2020-05-06 11:33:01', 'admin', '2019-03-15 14:24:35', 'normal');
INSERT INTO `onl_cgform_head` VALUES ('deea5a8ec619460c9245ba85dbc59e80', 'test_order_product', 3, 12, '订单产品明细', 'N', 'Y', 'Y', 'N', NULL, 'UUID', 'single', 0, NULL, 1, NULL, NULL, NULL, 'bdfl_include', '1', NULL, 0, NULL, 0, NULL, 'admin', '2020-05-03 01:01:18', 'admin', '2019-04-20 11:41:19', 'normal');

-- ----------------------------
-- Table structure for onl_cgform_index
-- ----------------------------
DROP TABLE IF EXISTS `onl_cgform_index`;
CREATE TABLE `onl_cgform_index`  (
  `id` varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '主键',
  `cgform_head_id` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '主表id',
  `index_name` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '索引名称',
  `index_field` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '索引栏位',
  `index_type` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '索引类型',
  `create_by` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '创建人登录名称',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建日期',
  `update_by` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '更新人登录名称',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新日期',
  `is_db_synch` varchar(2) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT 'N' COMMENT '是否同步数据库 N未同步 Y已同步',
  `del_flag` int(1) NULL DEFAULT 0 COMMENT '是否删除 0未删除 1删除',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `index_table_id`(`cgform_head_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for onl_cgreport_head
-- ----------------------------
DROP TABLE IF EXISTS `onl_cgreport_head`;
CREATE TABLE `onl_cgreport_head`  (
  `id` varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `code` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '报表编码',
  `name` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '报表名字',
  `cgr_sql` varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '报表SQL',
  `return_val_field` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '返回值字段',
  `return_txt_field` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '返回文本字段',
  `return_type` varchar(2) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '1' COMMENT '返回类型，单选或多选',
  `db_source` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '动态数据源',
  `content` varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '描述',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '修改时间',
  `update_by` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '修改人id',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `create_by` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '创建人id',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `index_onlinereport_code`(`code`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of onl_cgreport_head
-- ----------------------------
INSERT INTO `onl_cgreport_head` VALUES ('1256627801873821698', 'report002', '统计登录每日登录次数', 'select DATE_FORMAT(create_time,  \'%Y-%m-%d\') as date,count(*) as num from sys_log group by DATE_FORMAT(create_time, \'%Y-%m-%d\')', NULL, NULL, '1', NULL, NULL, NULL, NULL, '2020-05-03 00:53:10', 'admin');
INSERT INTO `onl_cgreport_head` VALUES ('1260179852088135681', 'tj_user_report', '统一有效系统用户', 'select * from sys_user', NULL, NULL, '1', NULL, NULL, '2020-07-10 17:34:42', 'admin', '2020-05-12 20:07:44', 'admin');
INSERT INTO `onl_cgreport_head` VALUES ('6c7f59741c814347905a938f06ee003c', 'report_user', '统计在线用户', 'select * from sys_user', NULL, NULL, '1', '', NULL, '2020-05-03 02:35:28', 'admin', '2019-03-25 11:20:45', 'admin');
INSERT INTO `onl_cgreport_head` VALUES ('87b55a515d3441b6b98e48e5b35474a6', 'demo', 'Report Demo', 'select * from demo', NULL, NULL, '1', '', NULL, '2020-05-03 01:14:35', 'admin', '2019-03-12 11:25:16', 'admin');

-- ----------------------------
-- Table structure for onl_cgreport_item
-- ----------------------------
DROP TABLE IF EXISTS `onl_cgreport_item`;
CREATE TABLE `onl_cgreport_item`  (
  `id` varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `cgrhead_id` varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '报表ID',
  `field_name` varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '字段名字',
  `field_txt` varchar(300) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '字段文本',
  `field_width` int(3) NULL DEFAULT NULL,
  `field_type` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '字段类型',
  `search_mode` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '查询模式',
  `is_order` int(2) NULL DEFAULT 0 COMMENT '是否排序  0否,1是',
  `is_search` int(2) NULL DEFAULT 0 COMMENT '是否查询  0否,1是',
  `dict_code` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '字典CODE',
  `field_href` varchar(120) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '字段跳转URL',
  `is_show` int(2) NULL DEFAULT 1 COMMENT '是否显示  0否,1显示',
  `order_num` int(11) NULL DEFAULT NULL COMMENT '排序',
  `replace_val` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '取值表达式',
  `create_by` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '创建人',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '修改人',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `index_CGRHEAD_ID`(`cgrhead_id`) USING BTREE,
  INDEX `index_isshow`(`is_show`) USING BTREE,
  INDEX `index_order_num`(`order_num`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of onl_cgreport_item
-- ----------------------------
INSERT INTO `onl_cgreport_item` VALUES ('1256627802020622337', '1256627801873821698', 'date', '日期', NULL, 'String', NULL, 0, 0, '', '', 1, 1, '', 'admin', '2020-05-03 00:53:10', NULL, NULL);
INSERT INTO `onl_cgreport_item` VALUES ('1256627802075148289', '1256627801873821698', 'num', '登录次数', NULL, 'String', NULL, 0, 0, '', '', 1, 2, '', 'admin', '2020-05-03 00:53:10', NULL, NULL);
INSERT INTO `onl_cgreport_item` VALUES ('1260179881129496577', '1260179852088135681', 'id', 'ID', NULL, 'String', NULL, 0, 0, '', '', 0, 1, '', 'admin', '2020-07-10 17:34:42', NULL, NULL);
INSERT INTO `onl_cgreport_item` VALUES ('1260179881129496578', '1260179852088135681', 'username', '账号', NULL, 'String', NULL, 0, 0, '', '', 1, 2, '', 'admin', '2020-07-10 17:34:42', NULL, NULL);
INSERT INTO `onl_cgreport_item` VALUES ('1260179881129496579', '1260179852088135681', 'realname', '用户名字', NULL, 'String', NULL, 0, 0, '', '', 1, 3, '', 'admin', '2020-07-10 17:34:42', NULL, NULL);
INSERT INTO `onl_cgreport_item` VALUES ('1260179881129496584', '1260179852088135681', 'sex', '性别', NULL, 'String', NULL, 0, 1, 'sex', '', 1, 4, '', 'admin', '2020-07-10 17:34:42', NULL, NULL);
INSERT INTO `onl_cgreport_item` VALUES ('1260179881129496585', '1260179852088135681', 'email', '邮箱', NULL, 'String', 'single', 0, 1, '', '', 1, 5, '', 'admin', '2020-07-10 17:34:42', NULL, NULL);
INSERT INTO `onl_cgreport_item` VALUES ('1260179881129496586', '1260179852088135681', 'phone', '电话', NULL, 'String', NULL, 0, 0, '', '', 1, 6, '', 'admin', '2020-07-10 17:34:42', NULL, NULL);
INSERT INTO `onl_cgreport_item` VALUES ('15884396588465896672', '87b55a515d3441b6b98e48e5b35474a6', 'id', 'ID', NULL, 'String', NULL, 0, 0, '', '', 0, 1, '', 'admin', '2020-05-03 01:14:35', NULL, NULL);
INSERT INTO `onl_cgreport_item` VALUES ('15892858611256977947', '1260179852088135681', 'birthday', '生日', NULL, 'Date', NULL, 0, 0, '', '', 1, 7, '', 'admin', '2020-07-10 17:34:42', NULL, NULL);
INSERT INTO `onl_cgreport_item` VALUES ('1740bb02519db90c44cb2cba8b755136', '6c7f59741c814347905a938f06ee003c', 'realname', '用户名称', NULL, 'String', NULL, 0, 0, '', 'https://www.baidu.com', 1, 1, '', 'admin', '2020-05-03 02:35:28', NULL, NULL);
INSERT INTO `onl_cgreport_item` VALUES ('1b181e6d2813bcb263adc39737f9df46', '87b55a515d3441b6b98e48e5b35474a6', 'name', '用户名', NULL, 'String', 'single', 0, 1, '', '', 1, 2, '', 'admin', '2020-05-03 01:14:35', NULL, NULL);
INSERT INTO `onl_cgreport_item` VALUES ('61ef5b323134938fdd07ad5e3ea16cd3', '87b55a515d3441b6b98e48e5b35474a6', 'key_word', '关键词', NULL, 'String', 'single', 0, 1, '', '', 1, 3, '', 'admin', '2020-05-03 01:14:35', NULL, NULL);
INSERT INTO `onl_cgreport_item` VALUES ('627768efd9ba2c41e905579048f21000', '6c7f59741c814347905a938f06ee003c', 'username', '用户账号', NULL, 'String', 'single', 0, 1, '', '', 1, 2, '', 'admin', '2020-05-03 02:35:28', NULL, NULL);
INSERT INTO `onl_cgreport_item` VALUES ('8bb087a9aa2000bcae17a1b3f5768435', '6c7f59741c814347905a938f06ee003c', 'sex', '性别', NULL, 'String', 'single', 0, 1, 'sex', '', 1, 3, '', 'admin', '2020-05-03 02:35:28', NULL, NULL);
INSERT INTO `onl_cgreport_item` VALUES ('90d4fa57d301801abb26a9b86b6b94c4', '6c7f59741c814347905a938f06ee003c', 'birthday', '生日', NULL, 'Date', 'single', 0, 0, '', '', 1, 4, '', 'admin', '2020-05-03 02:35:28', NULL, NULL);
INSERT INTO `onl_cgreport_item` VALUES ('a4ac355f07a05218854e5f23e2930163', '6c7f59741c814347905a938f06ee003c', 'avatar', '头像', NULL, 'String', NULL, 0, 0, '', '', 0, 5, '', 'admin', '2020-05-03 02:35:28', NULL, NULL);
INSERT INTO `onl_cgreport_item` VALUES ('d6e86b5ffd096ddcc445c0f320a45004', '6c7f59741c814347905a938f06ee003c', 'phone', '手机号', NULL, 'String', NULL, 0, 0, '', '', 1, 6, '', 'admin', '2020-05-03 02:35:28', NULL, NULL);
INSERT INTO `onl_cgreport_item` VALUES ('df365cd357699eea96c29763d1dd7f9d', '6c7f59741c814347905a938f06ee003c', 'email', '邮箱', NULL, 'String', NULL, 0, 0, '', '', 1, 7, '', 'admin', '2020-05-03 02:35:28', NULL, NULL);
INSERT INTO `onl_cgreport_item` VALUES ('edf9932912b81ad01dd557d3d593a559', '87b55a515d3441b6b98e48e5b35474a6', 'age', '年龄', NULL, 'String', NULL, 0, 0, '', '', 1, 4, '', 'admin', '2020-05-03 01:14:35', NULL, NULL);
INSERT INTO `onl_cgreport_item` VALUES ('f985883e509a6faaaf62ca07fd24a73c', '87b55a515d3441b6b98e48e5b35474a6', 'birthday', '生日', NULL, 'Date', 'single', 0, 1, '', '', 1, 5, '', 'admin', '2020-05-03 01:14:35', NULL, NULL);
INSERT INTO `onl_cgreport_item` VALUES ('fce83e4258de3e2f114ab3116397670c', '87b55a515d3441b6b98e48e5b35474a6', 'punch_time', '发布时间', NULL, 'String', NULL, 0, 0, '', '', 1, 6, '', 'admin', '2020-05-03 01:14:35', NULL, NULL);

-- ----------------------------
-- Table structure for onl_cgreport_param
-- ----------------------------
DROP TABLE IF EXISTS `onl_cgreport_param`;
CREATE TABLE `onl_cgreport_param`  (
  `id` varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `cgrhead_id` varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '动态报表ID',
  `param_name` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '参数字段',
  `param_txt` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '参数文本',
  `param_value` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '参数默认值',
  `order_num` int(11) NULL DEFAULT NULL COMMENT '排序',
  `create_by` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '创建人登录名称',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建日期',
  `update_by` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '更新人登录名称',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新日期',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_cgrheadid`(`cgrhead_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for oss_file
-- ----------------------------
DROP TABLE IF EXISTS `oss_file`;
CREATE TABLE `oss_file`  (
  `id` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '主键id',
  `file_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '文件名称',
  `url` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '文件地址',
  `create_by` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '创建人登录名称',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建日期',
  `update_by` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '更新人登录名称',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新日期',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = 'Oss File' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for qrtz_blob_triggers
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_blob_triggers`;
CREATE TABLE `qrtz_blob_triggers`  (
  `SCHED_NAME` varchar(120) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `TRIGGER_NAME` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `TRIGGER_GROUP` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `BLOB_DATA` blob NULL,
  PRIMARY KEY (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`) USING BTREE,
  CONSTRAINT `qrtz_blob_triggers_ibfk_1` FOREIGN KEY (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`) REFERENCES `qrtz_triggers` (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = 'InnoDB free: 504832 kB; (`SCHED_NAME` `TRIGGER_NAME` `TRIGGE' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for qrtz_calendars
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_calendars`;
CREATE TABLE `qrtz_calendars`  (
  `SCHED_NAME` varchar(120) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `CALENDAR_NAME` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `CALENDAR` blob NOT NULL,
  PRIMARY KEY (`SCHED_NAME`, `CALENDAR_NAME`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for qrtz_cron_triggers
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_cron_triggers`;
CREATE TABLE `qrtz_cron_triggers`  (
  `SCHED_NAME` varchar(120) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `TRIGGER_NAME` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `TRIGGER_GROUP` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `CRON_EXPRESSION` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `TIME_ZONE_ID` varchar(80) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`) USING BTREE,
  CONSTRAINT `qrtz_cron_triggers_ibfk_1` FOREIGN KEY (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`) REFERENCES `qrtz_triggers` (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = 'InnoDB free: 504832 kB; (`SCHED_NAME` `TRIGGER_NAME` `TRIGGE' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of qrtz_cron_triggers
-- ----------------------------
INSERT INTO `qrtz_cron_triggers` VALUES ('MyScheduler', 'org.jeecg.modules.message.job.SendMsgJob', 'DEFAULT', '0/50 * * * * ? *', 'Asia/Shanghai');
INSERT INTO `qrtz_cron_triggers` VALUES ('MyScheduler', 'org.jeecg.modules.quartz.job.SampleJob', 'DEFAULT', '0/1 * * * * ?', 'Asia/Shanghai');
INSERT INTO `qrtz_cron_triggers` VALUES ('MyScheduler', 'org.jeecg.modules.quartz.job.SampleParamJob', 'DEFAULT', '0/1 * * * * ?', 'Asia/Shanghai');

-- ----------------------------
-- Table structure for qrtz_fired_triggers
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_fired_triggers`;
CREATE TABLE `qrtz_fired_triggers`  (
  `SCHED_NAME` varchar(120) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `ENTRY_ID` varchar(95) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `TRIGGER_NAME` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `TRIGGER_GROUP` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `INSTANCE_NAME` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `FIRED_TIME` bigint(13) NOT NULL,
  `SCHED_TIME` bigint(13) NOT NULL,
  `PRIORITY` int(11) NOT NULL,
  `STATE` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `JOB_NAME` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `JOB_GROUP` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `IS_NONCONCURRENT` varchar(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `REQUESTS_RECOVERY` varchar(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`SCHED_NAME`, `ENTRY_ID`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for qrtz_job_details
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_job_details`;
CREATE TABLE `qrtz_job_details`  (
  `SCHED_NAME` varchar(120) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `JOB_NAME` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `JOB_GROUP` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `DESCRIPTION` varchar(250) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `JOB_CLASS_NAME` varchar(250) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `IS_DURABLE` varchar(1) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `IS_NONCONCURRENT` varchar(1) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `IS_UPDATE_DATA` varchar(1) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `REQUESTS_RECOVERY` varchar(1) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `JOB_DATA` blob NULL,
  PRIMARY KEY (`SCHED_NAME`, `JOB_NAME`, `JOB_GROUP`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of qrtz_job_details
-- ----------------------------
INSERT INTO `qrtz_job_details` VALUES ('MyScheduler', 'org.jeecg.modules.message.job.SendMsgJob', 'DEFAULT', NULL, 'org.jeecg.modules.message.job.SendMsgJob', '0', '0', '0', '0', 0xACED0005737200156F72672E71756172747A2E4A6F62446174614D61709FB083E8BFA9B0CB020000787200266F72672E71756172747A2E7574696C732E537472696E674B65794469727479466C61674D61708208E8C3FBC55D280200015A0013616C6C6F77735472616E7369656E74446174617872001D6F72672E71756172747A2E7574696C732E4469727479466C61674D617013E62EAD28760ACE0200025A000564697274794C00036D617074000F4C6A6176612F7574696C2F4D61703B787001737200116A6176612E7574696C2E486173684D61700507DAC1C31660D103000246000A6C6F6164466163746F724900097468726573686F6C6478703F4000000000000C77080000001000000001740009706172616D65746572707800);
INSERT INTO `qrtz_job_details` VALUES ('MyScheduler', 'org.jeecg.modules.quartz.job.SampleJob', 'DEFAULT', NULL, 'org.jeecg.modules.quartz.job.SampleJob', '0', '0', '0', '0', 0xACED0005737200156F72672E71756172747A2E4A6F62446174614D61709FB083E8BFA9B0CB020000787200266F72672E71756172747A2E7574696C732E537472696E674B65794469727479466C61674D61708208E8C3FBC55D280200015A0013616C6C6F77735472616E7369656E74446174617872001D6F72672E71756172747A2E7574696C732E4469727479466C61674D617013E62EAD28760ACE0200025A000564697274794C00036D617074000F4C6A6176612F7574696C2F4D61703B787001737200116A6176612E7574696C2E486173684D61700507DAC1C31660D103000246000A6C6F6164466163746F724900097468726573686F6C6478703F4000000000000C77080000001000000001740009706172616D65746572707800);
INSERT INTO `qrtz_job_details` VALUES ('MyScheduler', 'org.jeecg.modules.quartz.job.SampleParamJob', 'DEFAULT', NULL, 'org.jeecg.modules.quartz.job.SampleParamJob', '0', '0', '0', '0', 0xACED0005737200156F72672E71756172747A2E4A6F62446174614D61709FB083E8BFA9B0CB020000787200266F72672E71756172747A2E7574696C732E537472696E674B65794469727479466C61674D61708208E8C3FBC55D280200015A0013616C6C6F77735472616E7369656E74446174617872001D6F72672E71756172747A2E7574696C732E4469727479466C61674D617013E62EAD28760ACE0200025A000564697274794C00036D617074000F4C6A6176612F7574696C2F4D61703B787001737200116A6176612E7574696C2E486173684D61700507DAC1C31660D103000246000A6C6F6164466163746F724900097468726573686F6C6478703F4000000000000C77080000001000000001740009706172616D6574657274000573636F74747800);

-- ----------------------------
-- Table structure for qrtz_locks
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_locks`;
CREATE TABLE `qrtz_locks`  (
  `SCHED_NAME` varchar(120) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `LOCK_NAME` varchar(40) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  PRIMARY KEY (`SCHED_NAME`, `LOCK_NAME`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of qrtz_locks
-- ----------------------------
INSERT INTO `qrtz_locks` VALUES ('MyScheduler', 'STATE_ACCESS');
INSERT INTO `qrtz_locks` VALUES ('MyScheduler', 'TRIGGER_ACCESS');
INSERT INTO `qrtz_locks` VALUES ('quartzScheduler', 'TRIGGER_ACCESS');

-- ----------------------------
-- Table structure for qrtz_paused_trigger_grps
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_paused_trigger_grps`;
CREATE TABLE `qrtz_paused_trigger_grps`  (
  `SCHED_NAME` varchar(120) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `TRIGGER_GROUP` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  PRIMARY KEY (`SCHED_NAME`, `TRIGGER_GROUP`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for qrtz_scheduler_state
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_scheduler_state`;
CREATE TABLE `qrtz_scheduler_state`  (
  `SCHED_NAME` varchar(120) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `INSTANCE_NAME` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `LAST_CHECKIN_TIME` bigint(13) NOT NULL,
  `CHECKIN_INTERVAL` bigint(13) NOT NULL,
  PRIMARY KEY (`SCHED_NAME`, `INSTANCE_NAME`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of qrtz_scheduler_state
-- ----------------------------
INSERT INTO `qrtz_scheduler_state` VALUES ('MyScheduler', '5be3c92e95531607172282894', 1607172368550, 10000);

-- ----------------------------
-- Table structure for qrtz_simple_triggers
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_simple_triggers`;
CREATE TABLE `qrtz_simple_triggers`  (
  `SCHED_NAME` varchar(120) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `TRIGGER_NAME` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `TRIGGER_GROUP` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `REPEAT_COUNT` bigint(7) NOT NULL,
  `REPEAT_INTERVAL` bigint(12) NOT NULL,
  `TIMES_TRIGGERED` bigint(10) NOT NULL,
  PRIMARY KEY (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`) USING BTREE,
  CONSTRAINT `qrtz_simple_triggers_ibfk_1` FOREIGN KEY (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`) REFERENCES `qrtz_triggers` (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = 'InnoDB free: 504832 kB; (`SCHED_NAME` `TRIGGER_NAME` `TRIGGE' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for qrtz_simprop_triggers
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_simprop_triggers`;
CREATE TABLE `qrtz_simprop_triggers`  (
  `SCHED_NAME` varchar(120) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `TRIGGER_NAME` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `TRIGGER_GROUP` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `STR_PROP_1` varchar(512) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `STR_PROP_2` varchar(512) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `STR_PROP_3` varchar(512) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `INT_PROP_1` int(11) NULL DEFAULT NULL,
  `INT_PROP_2` int(11) NULL DEFAULT NULL,
  `LONG_PROP_1` bigint(20) NULL DEFAULT NULL,
  `LONG_PROP_2` bigint(20) NULL DEFAULT NULL,
  `DEC_PROP_1` decimal(13, 4) NULL DEFAULT NULL,
  `DEC_PROP_2` decimal(13, 4) NULL DEFAULT NULL,
  `BOOL_PROP_1` varchar(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `BOOL_PROP_2` varchar(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`) USING BTREE,
  CONSTRAINT `qrtz_simprop_triggers_ibfk_1` FOREIGN KEY (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`) REFERENCES `qrtz_triggers` (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = 'InnoDB free: 504832 kB; (`SCHED_NAME` `TRIGGER_NAME` `TRIGGE' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for qrtz_triggers
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_triggers`;
CREATE TABLE `qrtz_triggers`  (
  `SCHED_NAME` varchar(120) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `TRIGGER_NAME` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `TRIGGER_GROUP` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `JOB_NAME` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `JOB_GROUP` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `DESCRIPTION` varchar(250) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `NEXT_FIRE_TIME` bigint(13) NULL DEFAULT NULL,
  `PREV_FIRE_TIME` bigint(13) NULL DEFAULT NULL,
  `PRIORITY` int(11) NULL DEFAULT NULL,
  `TRIGGER_STATE` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `TRIGGER_TYPE` varchar(8) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `START_TIME` bigint(13) NOT NULL,
  `END_TIME` bigint(13) NULL DEFAULT NULL,
  `CALENDAR_NAME` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `MISFIRE_INSTR` smallint(2) NULL DEFAULT NULL,
  `JOB_DATA` blob NULL,
  PRIMARY KEY (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`) USING BTREE,
  INDEX `SCHED_NAME`(`SCHED_NAME`, `JOB_NAME`, `JOB_GROUP`) USING BTREE,
  CONSTRAINT `qrtz_triggers_ibfk_1` FOREIGN KEY (`SCHED_NAME`, `JOB_NAME`, `JOB_GROUP`) REFERENCES `qrtz_job_details` (`SCHED_NAME`, `JOB_NAME`, `JOB_GROUP`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = 'InnoDB free: 504832 kB; (`SCHED_NAME` `JOB_NAME` `JOB_GROUP`' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of qrtz_triggers
-- ----------------------------
INSERT INTO `qrtz_triggers` VALUES ('MyScheduler', 'org.jeecg.modules.message.job.SendMsgJob', 'DEFAULT', 'org.jeecg.modules.message.job.SendMsgJob', 'DEFAULT', NULL, 1607172410000, 1607172360000, 5, 'WAITING', 'CRON', 1588405284000, 0, NULL, 0, '');
INSERT INTO `qrtz_triggers` VALUES ('MyScheduler', 'org.jeecg.modules.quartz.job.SampleJob', 'DEFAULT', 'org.jeecg.modules.quartz.job.SampleJob', 'DEFAULT', NULL, 1588405730000, 1588405729000, 5, 'PAUSED', 'CRON', 1588405237000, 0, NULL, 0, '');
INSERT INTO `qrtz_triggers` VALUES ('MyScheduler', 'org.jeecg.modules.quartz.job.SampleParamJob', 'DEFAULT', 'org.jeecg.modules.quartz.job.SampleParamJob', 'DEFAULT', NULL, 1588405236000, 1588405235000, 5, 'PAUSED', 'CRON', 1588405221000, 0, NULL, 0, '');

-- ----------------------------
-- Table structure for supplier_suppliers
-- ----------------------------
DROP TABLE IF EXISTS `supplier_suppliers`;
CREATE TABLE `supplier_suppliers`  (
  `id` varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `type` varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `datacenter_id` varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `config` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT 'k8s：容器云，ali：阿里云,huawei：华为云,supervision_system：动环',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of supplier_suppliers
-- ----------------------------
INSERT INTO `supplier_suppliers` VALUES ('19d03008-7a5c-47cf-80c7-e44c0b3831c2', 'k8s_200', 'k8s', '36fa22ae-8858-4817-b1b1-c667a5012ddc', '{\"restveleroIp\":\"http://192.168.203.200:2020\",\"clusterId\":\"3680432f-79b7-40ff-928d-412cd5a53038\"}');
INSERT INTO `supplier_suppliers` VALUES ('7c155b62-1722-4ae3-9d3c-453fc48c4222', '共济动环', 'supervision_system', '36fa22ae-8858-4817-b1b1-c667a5012ddc', '{\"hostIp\":\"192.168.198.100\",\"loginCode\":\"BhWMZdFlb/GJu3sJbRDtHGkh3kxapzUQPdOGJQPTTpNDGg7YOB4HYTK7PIci+wYa\"}');
INSERT INTO `supplier_suppliers` VALUES ('9853a61e-3a59-4edd-ae77-71ab000d678b', 'aaaa', 'k8s', 'bd97fa17-36e0-4cff-a746-71e727e56c6a', '{\"restveleroIp\":\"\",\"clusterId\":\"5d3f2c3d-09ae-41ce-a7e3-82fdbfeb0aba\"}');
INSERT INTO `supplier_suppliers` VALUES ('bcb37815-bbc3-4c83-a558-c1434d4fec53', 'k8s_210', 'k8s', '36fa22ae-8858-4817-b1b1-ceqweqweqwe', '{\"restveleroIp\":\"http://192.168.203.210:2020\",\"clusterId\":\"2e910d7f-c9e5-4d05-8e5f-c3c50e853d37\"}');
INSERT INTO `supplier_suppliers` VALUES ('7c155b62-1722-4ae3-9d3c-453fc48c4254', '盈泽动环', 'supervision_system', '36fa22ae-8858-4817-b1b1-c667a5012ddc', '{\"hostIp\":\"111.198.67.231\",\"port\":\"18089\"}');
INSERT INTO `supplier_suppliers` VALUES ('AFBEC8F0-9004-8FCF-B78F-D83E45D37B0A', '阿里智能功耗平台1', 'ali_powerconsumption', '21', '{"hostIp":"192.168.10.92","idToken":"a05734f26e5dd05c7b27f053e7c735c00fc5bd66"}');
INSERT INTO `supplier_suppliers` VALUES ('C34395C9-9106-138C-8AB2-C78017B74959', '阿里智能功耗平台2', 'ali_powerconsumption', '1', '{"hostIp":"192.168.10.97","idToken":"a05734f26e5dd05c7b27f053e7c735c00fc5bd66"}');
-- ----------------------------
-- Table structure for sys_announcement
-- ----------------------------
DROP TABLE IF EXISTS `sys_announcement`;
CREATE TABLE `sys_announcement`  (
  `id` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `titile` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '标题',
  `msg_content` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '内容',
  `start_time` datetime(0) NULL DEFAULT NULL COMMENT '开始时间',
  `end_time` datetime(0) NULL DEFAULT NULL COMMENT '结束时间',
  `sender` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '发布人',
  `priority` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '优先级（L低，M中，H高）',
  `msg_category` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '2' COMMENT '消息类型1:通知公告2:系统消息',
  `msg_type` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '通告对象类型（USER:指定用户，ALL:全体用户）',
  `send_status` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '发布状态（0未发布，1已发布，2已撤销）',
  `send_time` datetime(0) NULL DEFAULT NULL COMMENT '发布时间',
  `cancel_time` datetime(0) NULL DEFAULT NULL COMMENT '撤销时间',
  `del_flag` varchar(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '删除状态（0，正常，1已删除）',
  `bus_type` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '业务类型(email:邮件 bpm:流程)',
  `bus_id` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '业务id',
  `open_type` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '打开方式(组件：component 路由：url)',
  `open_page` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '组件/路由 地址',
  `create_by` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '创建人',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '更新人',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `user_ids` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '指定用户',
  `msg_abstract` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '摘要',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '系统通告表' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of sys_announcement
-- ----------------------------
INSERT INTO `sys_announcement` VALUES ('1256486502931722242', 'JeecgBoot 2.2.0版本发布', '<h5 id=\"h5_5\" style=\"box-sizing: inherit; font-family: \'PingFang SC\', \'Helvetica Neue\', \'Microsoft YaHei UI\', \'Microsoft YaHei\', \'Noto Sans CJK SC\', Sathu, EucrosiaUPC, Arial, Helvetica, sans-serif; line-height: 1.8; margin: 22px 0px 16px; padding: 0px; font-size: 18px; border: none; color: #333333; background-color: #ffffff;\">升级不兼容</h5>\n<ul style=\"box-sizing: inherit; margin: 0px 0px 20px; padding: 0px 0px 0px 20px; color: #333333; font-family: -apple-system, BlinkMacSystemFont, \'Apple Color Emoji\', \'Segoe UI Emoji\', \'Segoe UI Symbol\', \'Segoe UI\', \'PingFang SC\', \'Hiragino Sans GB\', \'Microsoft YaHei\', \'Helvetica Neue\', Helvetica, Arial, sans-serif; font-size: 16px; background-color: #ffffff;\">\n<li style=\"box-sizing: inherit; line-height: 1.875em; margin-top: 0px;\">1、升级 ant-design-vue 到最新版本 1.5.2，会导致使用disabled的组件不可用（需要全局替换disabled 为readOnly；另外原来readOnly不允许全小写）</li>\n<li style=\"box-sizing: inherit; line-height: 1.875em;\">2、JSearchSelectTag.vue行编辑的popup传参做了修改，需要全局替换 orgFieldse 为 orgFields</li>\n<li style=\"box-sizing: inherit; line-height: 1.875em;\">3、 项目删除了sass，全部换成 less，其中涉及/deep/语法得改造</li>\n<li style=\"box-sizing: inherit; line-height: 1.875em; margin-bottom: 0px;\">4、 Online表单的填值规则用法修改了&nbsp;<a style=\"box-sizing: inherit; background-color: transparent; color: #4183c4; text-decoration-line: none;\" href=\"http://doc.jeecg.com/1630378\">文档</a></li>\n</ul>\n<h5 id=\"h5_6\" style=\"box-sizing: inherit; font-family: \'PingFang SC\', \'Helvetica Neue\', \'Microsoft YaHei UI\', \'Microsoft YaHei\', \'Noto Sans CJK SC\', Sathu, EucrosiaUPC, Arial, Helvetica, sans-serif; line-height: 1.8; margin: 22px 0px 16px; padding: 0px; font-size: 18px; border: none; color: #333333; background-color: #ffffff;\">较大改造</h5>\n<ul style=\"box-sizing: inherit; margin: 0px 0px 20px; padding: 0px 0px 0px 20px; color: #333333; font-family: -apple-system, BlinkMacSystemFont, \'Apple Color Emoji\', \'Segoe UI Emoji\', \'Segoe UI Symbol\', \'Segoe UI\', \'PingFang SC\', \'Hiragino Sans GB\', \'Microsoft YaHei\', \'Helvetica Neue\', Helvetica, Arial, sans-serif; font-size: 16px; background-color: #ffffff;\">\n<li style=\"box-sizing: inherit; line-height: 1.875em; margin-top: 0px;\">登录的时候一次性加载系统字典，进行前端缓存，减少ajax重复请求</li>\n<li style=\"box-sizing: inherit; line-height: 1.875em;\">修改定时任务quartz配置</li>\n<li style=\"box-sizing: inherit; line-height: 1.875em;\">logback-spring.xml配置不按照日期和大小生成新日志文件问题修复</li>\n<li style=\"box-sizing: inherit; line-height: 1.875em;\">vue-cli3 编译打包进行zip压缩优化</li>\n<li style=\"box-sizing: inherit; line-height: 1.875em;\">根据index.html页面window._CONFIG[\'domianURL\']，指定 axios的 baseURL（所以vue的代理没有用了）</li>\n<li style=\"box-sizing: inherit; line-height: 1.875em;\">JEditableTable重构，新增获取值时应该把临时id去掉、行编辑修改不直接更改数据源的ID</li>\n<li style=\"box-sizing: inherit; line-height: 1.875em; margin-bottom: 0px;\">Online模块重点优化升级、Online代码生成器重点优化升级</li>\n</ul>\n<h5 id=\"h5_7\" style=\"box-sizing: inherit; font-family: \'PingFang SC\', \'Helvetica Neue\', \'Microsoft YaHei UI\', \'Microsoft YaHei\', \'Noto Sans CJK SC\', Sathu, EucrosiaUPC, Arial, Helvetica, sans-serif; line-height: 1.8; margin: 22px 0px 16px; padding: 0px; font-size: 18px; border: none; color: #333333; background-color: #ffffff;\">ONLINE升级</h5>\n<ul style=\"box-sizing: inherit; margin: 0px 0px 20px; padding: 0px 0px 0px 20px; color: #333333; font-family: -apple-system, BlinkMacSystemFont, \'Apple Color Emoji\', \'Segoe UI Emoji\', \'Segoe UI Symbol\', \'Segoe UI\', \'PingFang SC\', \'Hiragino Sans GB\', \'Microsoft YaHei\', \'Helvetica Neue\', Helvetica, Arial, sans-serif; font-size: 16px; background-color: #ffffff;\">\n<li style=\"box-sizing: inherit; line-height: 1.875em; margin-top: 0px;\">Online访问权限控制实现，如果online表单配置了菜单，则用户需要授权才能访问此表单</li>\n<li style=\"box-sizing: inherit; line-height: 1.875em;\">Online表单新增组件： 省市区地域组件、开关组件、三级联动(级联下拉)组件、markdown组件</li>\n<li style=\"box-sizing: inherit; line-height: 1.875em;\">Online表单弹窗宽度，根据不同的列数显示不同的宽度，实现更好的效果</li>\n<li style=\"box-sizing: inherit; line-height: 1.875em;\">Online 树表单功能一系列优化，比如数据列新增添加下级按钮功能、添加下级不自动展开列表等问题</li>\n<li style=\"box-sizing: inherit; line-height: 1.875em;\">Online JS增强编辑器支持放大，同时JS代码变更，增加版本记忆功能，方便追踪历史</li>\n<li style=\"box-sizing: inherit; line-height: 1.875em;\">Online JS增强支持lodash工具</li>\n<li style=\"box-sizing: inherit; line-height: 1.875em;\">Online控件 默认值表达式功能&nbsp;<a style=\"box-sizing: inherit; background-color: transparent; color: #4183c4; text-decoration-line: none;\" href=\"http://doc.jeecg.com/1630378\">文档</a></li>\n<li style=\"box-sizing: inherit; line-height: 1.875em;\">Online 增加Excel导入导出转换器配置，针对各种控件的导入导出做了统一处理</li>\n<li style=\"box-sizing: inherit; line-height: 1.875em;\">Online 列表字段排序支持配置</li>\n<li style=\"box-sizing: inherit; line-height: 1.875em;\">Online同步数据库，数据库兼容性优化（mysql、oracle11g、SqlServer2017）</li>\n<li style=\"box-sizing: inherit; line-height: 1.875em;\">Online功能列表，高级查询优化，丰富控件的支持（新增：用户选择、部门选择、时间等）</li>\n<li style=\"box-sizing: inherit; line-height: 1.875em;\">Online表单配置，类型Text、date、Blob的字段长度默认设置为0</li>\n<li style=\"box-sizing: inherit; line-height: 1.875em;\">Online自定义button，支持位置自定义（支持右侧和下方）</li>\n<li style=\"box-sizing: inherit; line-height: 1.875em;\">Online表单列表，列表显示那些字段，支持用户个性化设置</li>\n<li style=\"box-sizing: inherit; line-height: 1.875em;\">Online表单删除和移除逻辑优化，防止未同步情况下删除报错</li>\n<li style=\"box-sizing: inherit; line-height: 1.875em;\">Online上传逻辑重构，针对单表、一对多行操作，都支持图片和文件上传，同时支持三种模式（minio、阿里云、本地存储）</li>\n<li style=\"box-sizing: inherit; line-height: 1.875em;\">Online上传逻辑重构， 行编辑模式，支持图片顺序修改</li>\n<li style=\"box-sizing: inherit; line-height: 1.875em;\">Online上传逻辑重构，在列表页面支持图片和文件的展示</li>\n<li style=\"box-sizing: inherit; line-height: 1.875em;\">Online表单索引同步逻辑重构，同时支持选择多个字段，组合索引设置</li>\n<li style=\"box-sizing: inherit; line-height: 1.875em;\">Online表单 excel导出实现数据权限逻辑</li>\n<li style=\"box-sizing: inherit; line-height: 1.875em;\">Online表单列表强制列不换行，自动出现超出滚定条，支持移动端自适应（单表、树和ERP等模型都已修改）</li>\n<li style=\"box-sizing: inherit; line-height: 1.875em;\">Online表单管理列表，回车查询和列表左右拖动移动自适应问题</li>\n<li style=\"box-sizing: inherit; line-height: 1.875em;\">Online表单附表序号必填</li>\n<li style=\"box-sizing: inherit; line-height: 1.875em;\">Online表名加校验不能全是数字，自动trim空格</li>\n<li style=\"box-sizing: inherit; line-height: 1.875em;\">Online 一对多tab必填校验不通过，添加友好提醒</li>\n<li style=\"box-sizing: inherit; line-height: 1.875em;\">Online表单控件默认值示例</li>\n<li style=\"box-sizing: inherit; line-height: 1.875em;\">Online一对多内嵌子表风格，高级查询不好用，过滤不了数据处理</li>\n<li style=\"box-sizing: inherit; line-height: 1.875em;\">Online表单 自定义校验规则正则，录入不成功问题处理</li>\n<li style=\"box-sizing: inherit; line-height: 1.875em;\">Online表单设置不分页，排序后又分页了问题处理</li>\n<li style=\"box-sizing: inherit; line-height: 1.875em;\">Online表单，下拉搜索组件，表字典配置加条件，下拉值出不来问题处理</li>\n<li style=\"box-sizing: inherit; line-height: 1.875em;\">Online表单，一对一子表，下拉选择、时间等组件被遮挡问题处理</li>\n<li style=\"box-sizing: inherit; line-height: 1.875em;\">Online表单树模型和一对多ERP模板支持href</li>\n<li style=\"box-sizing: inherit; line-height: 1.875em;\">Online 报表支持上下文变量表达式</li>\n<li style=\"box-sizing: inherit; line-height: 1.875em;\">Online 报表实现数据权限逻辑</li>\n<li style=\"box-sizing: inherit; line-height: 1.875em;\">Online 报表新增拖动字段顺序</li>\n<li style=\"box-sizing: inherit; line-height: 1.875em;\">Online 报表列表，sql过长截取显示</li>\n<li style=\"box-sizing: inherit; line-height: 1.875em;\">Online 报表，popup回车查询</li>\n<li style=\"box-sizing: inherit; line-height: 1.875em; margin-bottom: 0px;\">Online表单auto页面弹窗改成JModule；online表单移动自适应；</li>\n</ul>\n<h5 id=\"h5_8\" style=\"box-sizing: inherit; font-family: \'PingFang SC\', \'Helvetica Neue\', \'Microsoft YaHei UI\', \'Microsoft YaHei\', \'Noto Sans CJK SC\', Sathu, EucrosiaUPC, Arial, Helvetica, sans-serif; line-height: 1.8; margin: 22px 0px 16px; padding: 0px; font-size: 18px; border: none; color: #333333; background-color: #ffffff;\">Online代码生成器升级</h5>\n<blockquote style=\"box-sizing: inherit; position: relative; margin: 0px 0px 20px; padding: 20px; background-color: #f6f6f6; border-left: 6px solid #e6e6e6; word-break: break-word; color: #333333; font-family: -apple-system, BlinkMacSystemFont, \'Apple Color Emoji\', \'Segoe UI Emoji\', \'Segoe UI Symbol\', \'Segoe UI\', \'PingFang SC\', \'Hiragino Sans GB\', \'Microsoft YaHei\', \'Helvetica Neue\', Helvetica, Arial, sans-serif; font-size: 16px;\">\n<p style=\"box-sizing: inherit; margin: 0px; line-height: inherit;\">后期重点维护ONLINE模式的代码生成器，GUI模式逐步弃用。</p>\n</blockquote>\n<ul style=\"box-sizing: inherit; margin: 0px 0px 20px; padding: 0px 0px 0px 20px; color: #333333; font-family: -apple-system, BlinkMacSystemFont, \'Apple Color Emoji\', \'Segoe UI Emoji\', \'Segoe UI Symbol\', \'Segoe UI\', \'PingFang SC\', \'Hiragino Sans GB\', \'Microsoft YaHei\', \'Helvetica Neue\', Helvetica, Arial, sans-serif; font-size: 16px; background-color: #ffffff;\">\n<li style=\"box-sizing: inherit; line-height: 1.875em; margin-top: 0px;\">新增内嵌Table代码生成器模板</li>\n<li style=\"box-sizing: inherit; line-height: 1.875em;\">升级代码生成器依赖，支持返回生成结果&amp;支持服务器端生成代码config</li>\n<li style=\"box-sizing: inherit; line-height: 1.875em;\">代码生成器列表，列表自适应优化，支持移动效果（强制列不换行，不过有弊端，<a style=\"box-sizing: inherit; background-color: transparent; color: #4183c4; text-decoration-line: none;\" href=\"http://doc.jeecg.com/1607183\">见文档</a>）</li>\n<li style=\"box-sizing: inherit; line-height: 1.875em;\">实体生成，自动带着swagger 注解（支持单表、一对多等模型）</li>\n<li style=\"box-sizing: inherit; line-height: 1.875em;\">代码生成器，列表移动自适应样式修改</li>\n<li style=\"box-sizing: inherit; line-height: 1.875em;\">生成后的代码导入导出bug修复</li>\n<li style=\"box-sizing: inherit; line-height: 1.875em;\">online树代码生成器模板，直接删除一级分类会导致列表乱</li>\n<li style=\"box-sizing: inherit; line-height: 1.875em;\">ERP代码生成器模板升级，子表支持导入导出</li>\n<li style=\"box-sizing: inherit; line-height: 1.875em;\">ERP风格代码生成器模板问题（导入导入路径错误、swagger注解问题）</li>\n<li style=\"box-sizing: inherit; line-height: 1.875em;\">代码生成器，校验为空的话不生成，简化生成的代码</li>\n<li style=\"box-sizing: inherit; line-height: 1.875em;\">代码生成器，支持子表校验生成</li>\n<li style=\"box-sizing: inherit; line-height: 1.875em;\">树形列表代码生成器优化，添加子节点</li>\n<li style=\"box-sizing: inherit; line-height: 1.875em;\">修复bug： online字典未配置，代码生成器报错问题</li>\n<li style=\"box-sizing: inherit; line-height: 1.875em;\">生成的实体字段excel注解，针对系统标准字段，创建人、创建时间等字段不生成</li>\n<li style=\"box-sizing: inherit; line-height: 1.875em;\">生成的表单弹窗统一更换为j-modal，支持放大缩小全屏</li>\n<li style=\"box-sizing: inherit; line-height: 1.875em;\">代码生成，popup参数大小写 驼峰问题修复</li>\n<li style=\"box-sizing: inherit; line-height: 1.875em;\">生成的表单控件类型更丰富，新支持控件：markdown、省市区地域、密码、下拉搜索</li>\n<li style=\"box-sizing: inherit; line-height: 1.875em; margin-bottom: 0px;\">生成的列表查询区域，控件支持加强，新支持控件：省市区地域、下拉搜索、popup、选择用户、选择部门</li>\n</ul>\n<h5 id=\"h5_9\" style=\"box-sizing: inherit; font-family: \'PingFang SC\', \'Helvetica Neue\', \'Microsoft YaHei UI\', \'Microsoft YaHei\', \'Noto Sans CJK SC\', Sathu, EucrosiaUPC, Arial, Helvetica, sans-serif; line-height: 1.8; margin: 22px 0px 16px; padding: 0px; font-size: 18px; border: none; color: #333333; background-color: #ffffff;\">平台基础升级</h5>\n<ul style=\"box-sizing: inherit; margin: 0px 0px 20px; padding: 0px 0px 0px 20px; color: #333333; font-family: -apple-system, BlinkMacSystemFont, \'Apple Color Emoji\', \'Segoe UI Emoji\', \'Segoe UI Symbol\', \'Segoe UI\', \'PingFang SC\', \'Hiragino Sans GB\', \'Microsoft YaHei\', \'Helvetica Neue\', Helvetica, Arial, sans-serif; font-size: 16px; background-color: #ffffff;\">\n<li style=\"box-sizing: inherit; line-height: 1.875em; margin-top: 0px;\">针对auto隐藏路由菜单，实现自动授权，简化online菜单的授权</li>\n<li style=\"box-sizing: inherit; line-height: 1.875em;\">IDE中找不到sun.misc.BASE64Encoder jar包问题</li>\n<li style=\"box-sizing: inherit; line-height: 1.875em;\">删掉作废获取所有用户角色列表接口，接口有性能问题</li>\n<li style=\"box-sizing: inherit; line-height: 1.875em;\">在我的消息里点击已读以后，首页上面的通知红点并没有同步更改问题修复</li>\n<li style=\"box-sizing: inherit; line-height: 1.875em;\">用户与部门取消关联删除关联关系bug修改</li>\n<li style=\"box-sizing: inherit; line-height: 1.875em;\">部门导入未刷新redis缓存</li>\n<li style=\"box-sizing: inherit; line-height: 1.875em;\">修复SysPermissionMapper.xml 的SQL语句不兼容SQLServer的问题</li>\n<li style=\"box-sizing: inherit; line-height: 1.875em;\">在线动态数据源代码重构</li>\n<li style=\"box-sizing: inherit; line-height: 1.875em;\">【二级管理员】部门修改、删除权限时关联删除部门角色数据</li>\n<li style=\"box-sizing: inherit; line-height: 1.875em;\">【二级管理员】我的部门，选中部门只能看当前部门下的角色</li>\n<li style=\"box-sizing: inherit; line-height: 1.875em;\">我的消息支持模糊查询</li>\n<li style=\"box-sizing: inherit; line-height: 1.875em;\">常用示例-对象存储文件上传优化</li>\n<li style=\"box-sizing: inherit; line-height: 1.875em;\">用户相关功能，sql写法不兼容其他数据库问题修复</li>\n<li style=\"box-sizing: inherit; line-height: 1.875em;\">图片上传组件修改</li>\n<li style=\"box-sizing: inherit; line-height: 1.875em;\">导入功能优化，提示准确导入成功失败信息，涉及功能 用户、角色、部门、字典、定时任务等</li>\n<li style=\"box-sizing: inherit; line-height: 1.875em;\">增加生产环境禁用swagger-ui配置</li>\n<li style=\"box-sizing: inherit; line-height: 1.875em;\">第三方登录代码集成</li>\n<li style=\"box-sizing: inherit; line-height: 1.875em;\">系统公告优化</li>\n<li style=\"box-sizing: inherit; line-height: 1.875em;\">MockController 返回json数据，utf-8格式化，防止中文乱码</li>\n<li style=\"box-sizing: inherit; line-height: 1.875em;\">部门删除后删除部门角色等关联</li>\n<li style=\"box-sizing: inherit; line-height: 1.875em; margin-bottom: 0px;\">分类字典支持选择数据，添加下级分类</li>\n</ul>\n<h5 id=\"h5_10\" style=\"box-sizing: inherit; font-family: \'PingFang SC\', \'Helvetica Neue\', \'Microsoft YaHei UI\', \'Microsoft YaHei\', \'Noto Sans CJK SC\', Sathu, EucrosiaUPC, Arial, Helvetica, sans-serif; line-height: 1.8; margin: 22px 0px 16px; padding: 0px; font-size: 18px; border: none; color: #333333; background-color: #ffffff;\">UI组件升级</h5>\n<ul style=\"box-sizing: inherit; margin: 0px 0px 20px; padding: 0px 0px 0px 20px; color: #333333; font-family: -apple-system, BlinkMacSystemFont, \'Apple Color Emoji\', \'Segoe UI Emoji\', \'Segoe UI Symbol\', \'Segoe UI\', \'PingFang SC\', \'Hiragino Sans GB\', \'Microsoft YaHei\', \'Helvetica Neue\', Helvetica, Arial, sans-serif; font-size: 16px; background-color: #ffffff;\">\n<li style=\"box-sizing: inherit; line-height: 1.875em; margin-top: 0px;\">升级 ant-design-vue 为最新版本 1.5.2</li>\n<li style=\"box-sizing: inherit; line-height: 1.875em;\">登录验证码获取失败的时候，不显示空白</li>\n<li style=\"box-sizing: inherit; line-height: 1.875em;\">升级 ant-design-vue，JModal切换全屏按钮因ant升级错位问题修复</li>\n<li style=\"box-sizing: inherit; line-height: 1.875em;\">简化 j-modal 的相关代码</li>\n<li style=\"box-sizing: inherit; line-height: 1.875em;\">封装常用组件示例；JInput新增当 type 变化的时候重新计算值</li>\n<li style=\"box-sizing: inherit; line-height: 1.875em;\">markdown集成</li>\n<li style=\"box-sizing: inherit; line-height: 1.875em;\">富文本支持minio上传</li>\n<li style=\"box-sizing: inherit; line-height: 1.875em;\">选择用户组件列表样式错位；Ellipsis.vue 优化</li>\n<li style=\"box-sizing: inherit; line-height: 1.875em;\">JModal移动端全屏效果</li>\n<li style=\"box-sizing: inherit; line-height: 1.875em;\">高级查询只有在 in 模式下才能多选</li>\n<li style=\"box-sizing: inherit; line-height: 1.875em;\">自定义选择用户组件JSelectMultiUser的宽度和截取显示长度</li>\n<li style=\"box-sizing: inherit; line-height: 1.875em;\">行编辑 JEditableTable移动自适应效果；</li>\n<li style=\"box-sizing: inherit; line-height: 1.875em;\">JEditableTable重构 行编辑新增合计功能</li>\n<li style=\"box-sizing: inherit; line-height: 1.875em;\">JEditableTable重构 行编辑Popup请求数量过多（加缓存机制）</li>\n<li style=\"box-sizing: inherit; line-height: 1.875em;\">JEditableTable重构,行编辑InputNumber扩展支持输入小数和负数</li>\n<li style=\"box-sizing: inherit; line-height: 1.875em;\">JEditableTable.vue disable模式禁用添加删除按钮</li>\n<li style=\"box-sizing: inherit; line-height: 1.875em;\">JEditableTable行编辑， popup返回值，时间赋值有问题</li>\n<li style=\"box-sizing: inherit; line-height: 1.875em;\">JEditableTable行编辑，日期控件显示错位问题</li>\n<li style=\"box-sizing: inherit; line-height: 1.875em;\">高级查询移动自适应效果；</li>\n<li style=\"box-sizing: inherit; line-height: 1.875em;\">高级查询保存的同时也要保存匹配方式（支持and or）</li>\n<li style=\"box-sizing: inherit; line-height: 1.875em;\">高级查询混入统一修改</li>\n<li style=\"box-sizing: inherit; line-height: 1.875em;\">高级查询选择用户组件时，点开用户弹框不选择用户，直接关掉用户弹框，再次点击弹框时，无法点开问题</li>\n<li style=\"box-sizing: inherit; line-height: 1.875em;\">高级查询组件支持，新支持组件，选择人、选择部门、popup、时间等</li>\n<li style=\"box-sizing: inherit; line-height: 1.875em;\">JPopup支持多选</li>\n<li style=\"box-sizing: inherit; line-height: 1.875em;\">JPopup返回值null问题</li>\n<li style=\"box-sizing: inherit; line-height: 1.875em;\">JPopup列表数据不刷新问题</li>\n<li style=\"box-sizing: inherit; line-height: 1.875em;\">JSearchSelectTag.vue匹配不上数字值问题</li>\n<li style=\"box-sizing: inherit; line-height: 1.875em;\">JSelectUserByDep 根据部门选择人组件bug</li>\n<li style=\"box-sizing: inherit; line-height: 1.875em;\">JTreeDict.vue 分类字典组件 清空不了问题</li>\n<li style=\"box-sizing: inherit; line-height: 1.875em;\">常用示例，新增搜索下拉 JSearchSelectTag.vue例子</li>\n<li style=\"box-sizing: inherit; line-height: 1.875em;\">常用示例，新增如何关闭当前页面例子</li>\n<li style=\"box-sizing: inherit; line-height: 1.875em;\">常用示例，省市区三级联动的例子</li>\n<li style=\"box-sizing: inherit; line-height: 1.875em;\">常用示例，增加字典下拉多选组件示例</li>\n<li style=\"box-sizing: inherit; line-height: 1.875em; margin-bottom: 0px;\">示例demo，关于图片的做统一优化</li>\n</ul>\n<h5 id=\"h5_11\" style=\"box-sizing: inherit; font-family: \'PingFang SC\', \'Helvetica Neue\', \'Microsoft YaHei UI\', \'Microsoft YaHei\', \'Noto Sans CJK SC\', Sathu, EucrosiaUPC, Arial, Helvetica, sans-serif; line-height: 1.8; margin: 22px 0px 16px; padding: 0px; font-size: 18px; border: none; color: #333333; background-color: #ffffff;\">Issues处理</h5>\n<ul style=\"box-sizing: inherit; margin: 0px 0px 20px; padding: 0px 0px 0px 20px; color: #333333; font-family: -apple-system, BlinkMacSystemFont, \'Apple Color Emoji\', \'Segoe UI Emoji\', \'Segoe UI Symbol\', \'Segoe UI\', \'PingFang SC\', \'Hiragino Sans GB\', \'Microsoft YaHei\', \'Helvetica Neue\', Helvetica, Arial, sans-serif; font-size: 16px; background-color: #ffffff;\">\n<li style=\"box-sizing: inherit; line-height: 1.875em; margin-top: 0px;\">online form表单项能否支持配置有级联关系的内容&nbsp;<a style=\"box-sizing: inherit; background-color: transparent; color: #4183c4; text-decoration-line: none;\" title=\"#948\" href=\"https://github.com/zhangdaiscott/jeecg-boot/issues/948\">#948</a></li>\n<li style=\"box-sizing: inherit; line-height: 1.875em;\">全局拦截异常错误的提醒文字&nbsp;<a style=\"box-sizing: inherit; background-color: transparent; color: #4183c4; text-decoration-line: none;\" title=\"#768\" href=\"https://github.com/zhangdaiscott/jeecg-boot/issues/768\">#768</a></li>\n<li style=\"box-sizing: inherit; line-height: 1.875em;\">建议：城市级联选择组件&nbsp;<a style=\"box-sizing: inherit; background-color: transparent; color: #4183c4; text-decoration-line: none;\" title=\"#905\" href=\"https://github.com/zhangdaiscott/jeecg-boot/issues/905\">#905</a></li>\n<li style=\"box-sizing: inherit; line-height: 1.875em;\">表单配置中检验字段配置href，报表中该配置字段无法看到链接&nbsp;<a style=\"box-sizing: inherit; background-color: transparent; color: #4183c4; text-decoration-line: none;\" title=\"#961\" href=\"https://github.com/zhangdaiscott/jeecg-boot/issues/961\">#961</a></li>\n<li style=\"box-sizing: inherit; line-height: 1.875em;\">SysUserMapper.xml查询脚本数据兼容问题&nbsp;<a style=\"box-sizing: inherit; background-color: transparent; color: #4183c4; text-decoration-line: none;\" title=\"#962\" href=\"https://github.com/zhangdaiscott/jeecg-boot/issues/962\">#962</a></li>\n<li style=\"box-sizing: inherit; line-height: 1.875em;\">在线表单 提示 ButtonExpHandler is not defined&nbsp;<a style=\"box-sizing: inherit; background-color: transparent; color: #4183c4; text-decoration-line: none;\" title=\"#957\" href=\"https://github.com/zhangdaiscott/jeecg-boot/issues/957\">#957</a></li>\n<li style=\"box-sizing: inherit; line-height: 1.875em;\">online表单中添加自定义按钮无法显示&nbsp;<a style=\"box-sizing: inherit; background-color: transparent; color: #4183c4; text-decoration-line: none;\" title=\"#973\" href=\"https://github.com/zhangdaiscott/jeecg-boot/issues/973\">#973</a></li>\n<li style=\"box-sizing: inherit; line-height: 1.875em;\">online报表不支持按照登录用户过滤数据&nbsp;<a style=\"box-sizing: inherit; background-color: transparent; color: #4183c4; text-decoration-line: none;\" title=\"#934\" href=\"https://github.com/zhangdaiscott/jeecg-boot/issues/934\">#934</a></li>\n<li style=\"box-sizing: inherit; line-height: 1.875em;\">点击新增或者编辑按钮弹框宽度的设置问题&nbsp;<a style=\"box-sizing: inherit; background-color: transparent; color: #4183c4; text-decoration-line: none;\" title=\"#974\" href=\"https://github.com/zhangdaiscott/jeecg-boot/issues/974\">#974</a></li>\n<li style=\"box-sizing: inherit; line-height: 1.875em;\">online报表配置中使用系统参数，报错&nbsp;<a style=\"box-sizing: inherit; background-color: transparent; color: #4183c4; text-decoration-line: none;\" title=\"I1AWNM\" href=\"https://gitee.com/jeecg/jeecg-boot/issues/I1AWNM\">I1AWNM</a></li>\n<li style=\"box-sizing: inherit; line-height: 1.875em;\">online增强SQL变量取数为null,#{sys.sys_date} 还有#{sys.sys_time}&nbsp;<a style=\"box-sizing: inherit; background-color: transparent; color: #4183c4; text-decoration-line: none;\" title=\"#999\" href=\"https://github.com/zhangdaiscott/jeecg-boot/issues/999\">#999</a></li>\n<li style=\"box-sizing: inherit; line-height: 1.875em;\">一个主表，多个付表。1对多。保存后，某些从表行项目内容保存成功，单重新打开数据表现丢失。&nbsp;<a style=\"box-sizing: inherit; background-color: transparent; color: #4183c4; text-decoration-line: none;\" title=\"#997\" href=\"https://github.com/zhangdaiscott/jeecg-boot/issues/997\">#997</a></li>\n<li style=\"box-sizing: inherit; line-height: 1.875em;\">/online/cgform/api/exportXls/{code}接口问题&nbsp;<a style=\"box-sizing: inherit; background-color: transparent; color: #4183c4; text-decoration-line: none;\" title=\"#1012\" href=\"https://github.com/zhangdaiscott/jeecg-boot/issues/1012\">#1012</a></li>\n<li style=\"box-sizing: inherit; line-height: 1.875em;\">打成jar包，访问不到代码生成器模板&nbsp;<a style=\"box-sizing: inherit; background-color: transparent; color: #4183c4; text-decoration-line: none;\" title=\"#1010\" href=\"https://github.com/zhangdaiscott/jeecg-boot/issues/1010\">#1010</a></li>\n<li style=\"box-sizing: inherit; line-height: 1.875em;\">online表单开发，打包后模板找不到&nbsp;<a style=\"box-sizing: inherit; background-color: transparent; color: #4183c4; text-decoration-line: none;\" title=\"#865\" href=\"https://github.com/zhangdaiscott/jeecg-boot/issues/865\">#865</a></li>\n<li style=\"box-sizing: inherit; line-height: 1.875em;\">已生成的代码，子表最下下行添加合计行底部固定问题&nbsp;<a style=\"box-sizing: inherit; background-color: transparent; color: #4183c4; text-decoration-line: none;\" title=\"#936\" href=\"https://github.com/zhangdaiscott/jeecg-boot/issues/936\">#936</a></li>\n<li style=\"box-sizing: inherit; line-height: 1.875em;\">下拉搜索框在编辑时下拉name无法带过来&nbsp;<a style=\"box-sizing: inherit; background-color: transparent; color: #4183c4; text-decoration-line: none;\" title=\"#971\" href=\"https://github.com/zhangdaiscott/jeecg-boot/issues/971\">#971</a></li>\n<li style=\"box-sizing: inherit; line-height: 1.875em;\">建议autoPoi升级，优化数据返回List Map格式下的复合表头导出excel的体验&nbsp;<a style=\"box-sizing: inherit; background-color: transparent; color: #4183c4; text-decoration-line: none;\" title=\"#873\" href=\"https://github.com/zhangdaiscott/jeecg-boot/issues/873\">#873</a></li>\n<li style=\"box-sizing: inherit; line-height: 1.875em;\">配置 Href 跳转，并没有显示为超链接&nbsp;<a style=\"box-sizing: inherit; background-color: transparent; color: #4183c4; text-decoration-line: none;\" title=\"#1020\" href=\"https://github.com/zhangdaiscott/jeecg-boot/issues/1020\">#1020</a></li>\n<li style=\"box-sizing: inherit; line-height: 1.875em;\">首次打开一对多表单，JEditableTable的addDefaultRowNum属性不生效&nbsp;<a style=\"box-sizing: inherit; background-color: transparent; color: #4183c4; text-decoration-line: none;\" title=\"#1003\" href=\"https://github.com/zhangdaiscott/jeecg-boot/issues/1003\">#1003</a></li>\n<li style=\"box-sizing: inherit; line-height: 1.875em;\">autopoi 双表头问题&nbsp;<a style=\"box-sizing: inherit; background-color: transparent; color: #4183c4; text-decoration-line: none;\" title=\"#862\" href=\"https://github.com/zhangdaiscott/jeecg-boot/issues/862\">#862</a></li>\n<li style=\"box-sizing: inherit; line-height: 1.875em;\">应该是权限管理bug&nbsp;<a style=\"box-sizing: inherit; background-color: transparent; color: #4183c4; text-decoration-line: none;\" title=\"#110\" href=\"https://github.com/zhangdaiscott/jeecg-boot/issues/110\">#110</a></li>\n<li style=\"box-sizing: inherit; line-height: 1.875em;\">在线 的online报表报错&nbsp;<a style=\"box-sizing: inherit; background-color: transparent; color: #4183c4; text-decoration-line: none;\" title=\"#1029\" href=\"https://github.com/zhangdaiscott/jeecg-boot/issues/1029\">#1029</a></li>\n<li style=\"box-sizing: inherit; line-height: 1.875em;\">online代码生成器树形表单父节点字段名称问题&nbsp;<a style=\"box-sizing: inherit; background-color: transparent; color: #4183c4; text-decoration-line: none;\" title=\"issues/I1ABGV \" href=\"https://gitee.com/jeecg/jeecg-boot/issues/I1ABGV\">issues/I1ABGV</a></li>\n<li style=\"box-sizing: inherit; line-height: 1.875em;\">点击j-popup,里面有数据，如果选择同名的数据，文本框为空&nbsp;<a style=\"box-sizing: inherit; background-color: transparent; color: #4183c4; text-decoration-line: none;\" title=\"#1044\" href=\"https://github.com/zhangdaiscott/jeecg-boot/issues/1044\">#1044</a></li>\n<li style=\"box-sizing: inherit; line-height: 1.875em;\">关于table列宽可拖动问题&nbsp;<a style=\"box-sizing: inherit; background-color: transparent; color: #4183c4; text-decoration-line: none;\" title=\"#1054\" href=\"https://github.com/zhangdaiscott/jeecg-boot/issues/1054\">#1054</a></li>\n<li style=\"box-sizing: inherit; line-height: 1.875em;\">单表数据导出多表头 auto 的Excel注解 groupName属性<a style=\"box-sizing: inherit; background-color: transparent; color: #4183c4; text-decoration-line: none;\" title=\" #1053\" href=\"https://github.com/zhangdaiscott/jeecg-boot/issues/1053\">&nbsp;#1053</a></li>\n<li style=\"box-sizing: inherit; line-height: 1.875em;\">在线报表和在线online的问题&nbsp;<a style=\"box-sizing: inherit; background-color: transparent; color: #4183c4; text-decoration-line: none;\" title=\"#1030\" href=\"https://github.com/zhangdaiscott/jeecg-boot/issues/1030\">#1030</a></li>\n<li style=\"box-sizing: inherit; line-height: 1.875em;\">J-pop组件&nbsp;<a style=\"box-sizing: inherit; background-color: transparent; color: #4183c4; text-decoration-line: none;\" title=\"#1043\" href=\"https://github.com/zhangdaiscott/jeecg-boot/issues/1043\">#1043</a></li>\n<li style=\"box-sizing: inherit; line-height: 1.875em;\">2.1.4 日志管理---没有记录查询接口参数&nbsp;<a style=\"box-sizing: inherit; background-color: transparent; color: #4183c4; text-decoration-line: none;\" title=\"#1070\" href=\"https://github.com/zhangdaiscott/jeecg-boot/issues/1070\">#1070</a></li>\n<li style=\"box-sizing: inherit; line-height: 1.875em;\">服务器日志超过设置的MaxFileSize时&nbsp;<a style=\"box-sizing: inherit; background-color: transparent; color: #4183c4; text-decoration-line: none;\" title=\"#1130\" href=\"https://github.com/zhangdaiscott/jeecg-boot/issues/1130\">#1130</a></li>\n<li style=\"box-sizing: inherit; line-height: 1.875em;\">online 表单中，在 附表中存在上传图片组件，点击上传图片并提交，显示提交失败&nbsp;<a style=\"box-sizing: inherit; background-color: transparent; color: #4183c4; text-decoration-line: none;\" title=\"#1074\" href=\"https://github.com/zhangdaiscott/jeecg-boot/issues/1074\">#1074</a></li>\n<li style=\"box-sizing: inherit; line-height: 1.875em;\">online表单js增强修改从表控件值问题&nbsp;<a style=\"box-sizing: inherit; background-color: transparent; color: #4183c4; text-decoration-line: none;\" title=\"#1051\" href=\"https://github.com/zhangdaiscott/jeecg-boot/issues/1051\">#1051</a></li>\n<li style=\"box-sizing: inherit; line-height: 1.875em;\">重复代理问题&nbsp;<a style=\"box-sizing: inherit; background-color: transparent; color: #4183c4; text-decoration-line: none;\" title=\"#994\" href=\"https://github.com/zhangdaiscott/jeecg-boot/issues/994\">#994</a></li>\n<li style=\"box-sizing: inherit; line-height: 1.875em;\">系统使用的日志包有问题&nbsp;<a style=\"box-sizing: inherit; background-color: transparent; color: #4183c4; text-decoration-line: none;\" title=\"#887\" href=\"https://github.com/zhangdaiscott/jeecg-boot/issues/887\">#887</a></li>\n<li style=\"box-sizing: inherit; line-height: 1.875em;\">使用sqlserver数据库,用户管理查询出错&nbsp;<a style=\"box-sizing: inherit; background-color: transparent; color: #4183c4; text-decoration-line: none;\" title=\"#1140\" href=\"https://github.com/zhangdaiscott/jeecg-boot/issues/1140\">#1140</a></li>\n<li style=\"box-sizing: inherit; line-height: 1.875em;\">主表和从表 一对一关系表单 TypeError: Cannot read property \'getAll\' of undefined&nbsp;<a style=\"box-sizing: inherit; background-color: transparent; color: #4183c4; text-decoration-line: none;\" title=\"#1129\" href=\"https://github.com/zhangdaiscott/jeecg-boot/issues/1129\">#1129</a></li>\n<li style=\"box-sizing: inherit; line-height: 1.875em;\">ERP表单附表数据编辑后消失&nbsp;<a style=\"box-sizing: inherit; background-color: transparent; color: #4183c4; text-decoration-line: none;\" title=\"issues/I1DN3B\" href=\"https://gitee.com/jeecg/jeecg-boot/issues/I1DN3B\">issues/I1DN3B</a></li>\n<li style=\"box-sizing: inherit; line-height: 1.875em;\">多表的erp模式生成的子表，表单没有导入导出功能&nbsp;<a style=\"box-sizing: inherit; background-color: transparent; color: #4183c4; text-decoration-line: none;\" title=\"issues/I1BB2U\" href=\"https://gitee.com/jeecg/jeecg-boot/issues/I1BB2U\">issues/I1BB2U</a></li>\n<li style=\"box-sizing: inherit; line-height: 1.875em;\">用edge打开首页，整个页面动不了，控制台console打出很多错误&nbsp;<a style=\"box-sizing: inherit; background-color: transparent; color: #4183c4; text-decoration-line: none;\" title=\"issues/I189B5\" href=\"https://gitee.com/jeecg/jeecg-boot/issues/I189B5\">issues/I189B5</a></li>\n<li style=\"box-sizing: inherit; line-height: 1.875em;\">官方的快速开发平台主子表对IE11不兼容&nbsp;<a style=\"box-sizing: inherit; background-color: transparent; color: #4183c4; text-decoration-line: none;\" title=\"issues/I17LEE\" href=\"https://gitee.com/jeecg/jeecg-boot/issues/I17LEE\">issues/I17LEE</a></li>\n<li style=\"box-sizing: inherit; line-height: 1.875em;\">360兼容模式 修改为你说的js之后；单表的数据兼容可以；主子表的不加载；错误如下&nbsp;<a style=\"box-sizing: inherit; background-color: transparent; color: #4183c4; text-decoration-line: none;\" title=\"issues/I17H8L\" href=\"https://gitee.com/jeecg/jeecg-boot/issues/I17H8L\">issues/I17H8L</a></li>\n<li style=\"box-sizing: inherit; line-height: 1.875em; margin-bottom: 0px;\">Online表单开发，点击&ldquo;新增&rdquo;按钮，是否树：选择是，页面控制台报错&nbsp;<a style=\"box-sizing: inherit; background-color: transparent; color: #4183c4; text-decoration-line: none;\" title=\"issues/I1BHXG\" href=\"https://gitee.com/jeecg/jeecg-boot/issues/I1BHXG\">issues/I1BHXG</a></li>\n</ul>', '2020-05-01 15:30:56', '2020-05-30 15:31:00', 'admin', 'H', '1', 'ALL', '1', '2020-05-02 15:31:44', NULL, '0', NULL, NULL, NULL, NULL, 'admin', '2020-05-02 15:31:41', 'admin', '2020-05-02 15:31:44', NULL, '重磅版本发布');
INSERT INTO `sys_announcement` VALUES ('1256486817286418434', '放假通知', '<p>放假通知</p>', '2020-05-02 15:32:38', '2020-05-30 15:32:40', 'admin', 'M', '1', 'USER', '1', '2020-05-02 15:32:59', NULL, '0', NULL, NULL, NULL, NULL, 'admin', '2020-05-02 15:32:56', 'admin', '2020-05-02 15:32:59', 'e9ca23d68d884d4ebb19d07889727dae,a75d45a015c44384a04449ee80dc3503,', '放假通知');
INSERT INTO `sys_announcement` VALUES ('1256529336858685441', '1', '<p>22</p>', '2020-05-02 18:21:41', '2020-05-30 18:21:44', 'admin', 'M', '1', 'ALL', '2', '2020-05-02 18:21:57', '2020-05-02 18:22:07', '0', NULL, NULL, NULL, NULL, 'admin', '2020-05-02 18:21:54', 'admin', '2020-05-02 18:22:07', NULL, '22');
INSERT INTO `sys_announcement` VALUES ('1b714f8ebc3cc33f8b4f906103b6a18d', '5467567', NULL, NULL, NULL, 'admin', NULL, '2', NULL, '1', '2019-03-30 12:40:38', NULL, '1', NULL, NULL, NULL, NULL, 'admin', '2019-02-26 17:23:26', 'admin', '2020-05-02 15:30:42', NULL, NULL);
INSERT INTO `sys_announcement` VALUES ('3d11237ccdf62450d20bb8abdb331178', '111222', NULL, NULL, NULL, NULL, NULL, '2', NULL, '0', NULL, NULL, '1', NULL, NULL, NULL, NULL, 'admin', '2019-03-29 17:19:47', 'admin', '2019-03-29 17:19:50', NULL, NULL);
INSERT INTO `sys_announcement` VALUES ('7ef04e95f8de030b1d5f7a9144090dc6', '111', NULL, '2019-02-06 17:28:10', '2019-03-08 17:28:11', NULL, NULL, '2', NULL, '0', NULL, NULL, '1', NULL, NULL, NULL, NULL, 'admin', '2019-02-26 17:28:17', 'admin', '2019-03-26 19:59:49', NULL, NULL);
INSERT INTO `sys_announcement` VALUES ('93a9060a1c20e4bf98b3f768a02c2ff9', '111', '111', '2019-02-06 17:20:17', '2019-02-21 17:20:20', 'admin', 'M', '2', 'ALL', '1', '2019-02-26 17:24:29', NULL, '1', NULL, NULL, NULL, NULL, 'admin', '2019-02-26 17:16:26', 'admin', '2020-05-02 15:30:42', NULL, NULL);
INSERT INTO `sys_announcement` VALUES ('de1dc57f31037079e1e55c8347fe6ef7', '222', '2222', '2019-02-06 17:28:26', '2019-02-23 17:28:28', 'admin', 'M', '2', 'ALL', '1', '2019-03-29 17:19:56', NULL, '1', NULL, NULL, NULL, NULL, 'admin', '2019-02-26 17:28:36', 'admin', '2019-02-26 17:28:40', NULL, NULL);
INSERT INTO `sys_announcement` VALUES ('e52f3eb6215f139cb2224c52517af3bd', '334', '334', NULL, NULL, NULL, NULL, '2', NULL, '0', NULL, NULL, '1', NULL, NULL, NULL, NULL, 'admin', '2019-03-30 12:40:28', 'admin', '2019-03-30 12:40:32', NULL, NULL);

-- ----------------------------
-- Table structure for sys_announcement_send
-- ----------------------------
DROP TABLE IF EXISTS `sys_announcement_send`;
CREATE TABLE `sys_announcement_send`  (
  `id` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `annt_id` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '通告ID',
  `user_id` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '用户id',
  `read_flag` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '阅读状态（0未读，1已读）',
  `read_time` datetime(0) NULL DEFAULT NULL COMMENT '阅读时间',
  `create_by` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '创建人',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '更新人',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间'
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '用户通告阅读标记表' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of sys_announcement_send
-- ----------------------------
INSERT INTO `sys_announcement_send` VALUES ('646c0c405ec643d4dc4160db2446f8ff', '93a9060a1c20e4bf98b3f768a02c2ff9', 'e9ca23d68d884d4ebb19d07889727dae', '1', '2019-11-21 16:30:01', 'admin', '2019-05-17 11:50:56', 'admin', '2019-11-21 16:30:01');
INSERT INTO `sys_announcement_send` VALUES ('1197434450981543938', '93a9060a1c20e4bf98b3f768a02c2ff9', 'a75d45a015c44384a04449ee80dc3503', '0', NULL, 'jeecg', '2019-11-21 16:39:55', NULL, NULL);
INSERT INTO `sys_announcement_send` VALUES ('1256486817319972866', '1256486817286418434', 'e9ca23d68d884d4ebb19d07889727dae', '1', '2020-05-02 15:33:05', 'admin', '2020-05-02 15:32:56', 'admin', '2020-05-02 15:33:05');
INSERT INTO `sys_announcement_send` VALUES ('1256486817349332993', '1256486817286418434', 'a75d45a015c44384a04449ee80dc3503', '0', '2020-05-02 15:32:56', 'admin', '2020-05-02 15:32:56', NULL, NULL);
INSERT INTO `sys_announcement_send` VALUES ('1256527099214278657', '1256486502931722242', 'e9ca23d68d884d4ebb19d07889727dae', '1', '2020-05-02 18:22:00', 'admin', '2020-05-02 18:12:59', 'admin', '2020-05-02 18:22:00');
INSERT INTO `sys_announcement_send` VALUES ('1260927781673484290', '1256486502931722242', 'a75d45a015c44384a04449ee80dc3503', '0', NULL, 'jeecg', '2020-05-14 21:39:45', NULL, NULL);

-- ----------------------------
-- Table structure for sys_category
-- ----------------------------
DROP TABLE IF EXISTS `sys_category`;
CREATE TABLE `sys_category`  (
  `id` varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `pid` varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '父级节点',
  `name` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '类型名称',
  `code` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '类型编码',
  `create_by` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '创建人',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建日期',
  `update_by` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '更新人',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新日期',
  `sys_org_code` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '所属部门',
  `has_child` varchar(3) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '是否有子节点',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `index_code`(`code`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of sys_category
-- ----------------------------
INSERT INTO `sys_category` VALUES ('1183693424827564034', '0', '物料树', 'B02', 'admin', '2019-10-14 18:37:59', 'admin', '2019-10-14 18:38:15', 'A01', '1');
INSERT INTO `sys_category` VALUES ('1183693491043041282', '1183693424827564034', '上衣', 'B02A01', 'admin', '2019-10-14 18:38:15', 'admin', '2019-10-14 18:38:43', 'A01', '1');
INSERT INTO `sys_category` VALUES ('1183693534173069314', '1183693424827564034', '裤子', 'B02A02', 'admin', '2019-10-14 18:38:25', NULL, NULL, 'A01', NULL);
INSERT INTO `sys_category` VALUES ('1183693610534567937', '1183693491043041282', '秋衣', 'B02A01A01', 'admin', '2019-10-14 18:38:43', NULL, NULL, 'A01', NULL);
INSERT INTO `sys_category` VALUES ('1183693700254924802', '1183693491043041282', '兵装', 'B02A01A02', 'admin', '2019-10-14 18:39:05', NULL, NULL, 'A01', NULL);
INSERT INTO `sys_category` VALUES ('1183693773974011906', '1183693491043041282', '女装', 'B02A01A03', 'admin', '2019-10-14 18:39:22', NULL, NULL, 'A01', NULL);
INSERT INTO `sys_category` VALUES ('1185039122143719425', '0', '电脑产品', 'A01', 'admin', '2019-10-18 11:45:18', 'admin', '2019-10-18 11:45:31', 'A01', '1');
INSERT INTO `sys_category` VALUES ('1185039176799694850', '1185039122143719425', 'thinkpad', 'A01A01', 'admin', '2019-10-18 11:45:31', NULL, NULL, 'A01', NULL);
INSERT INTO `sys_category` VALUES ('1185039255115739138', '1185039122143719425', 'mackbook', 'A01A02', 'admin', '2019-10-18 11:45:50', NULL, NULL, 'A01', NULL);
INSERT INTO `sys_category` VALUES ('1185039299051073537', '1185039122143719425', '华为电脑', 'A01A03', 'admin', '2019-10-18 11:46:01', NULL, NULL, 'A01', NULL);
INSERT INTO `sys_category` VALUES ('1230769196661510146', '0', '省', NULL, 'admin', '2020-02-21 16:20:16', 'admin', '2020-02-21 16:20:31', 'A01A03', '1');
INSERT INTO `sys_category` VALUES ('1230769253267836929', '1230769196661510146', '安徽省', NULL, 'admin', '2020-02-21 16:20:31', 'admin', '2020-02-21 16:20:53', 'A01A03', '1');
INSERT INTO `sys_category` VALUES ('1230769290609725441', '1230769196661510146', '山东省', NULL, 'admin', '2020-02-21 16:20:40', 'admin', '2020-02-21 16:21:23', 'A01A03', '1');
INSERT INTO `sys_category` VALUES ('1230769347157331969', '1230769253267836929', '合肥市', NULL, 'admin', '2020-02-21 16:20:53', 'admin', '2020-02-21 16:21:08', 'A01A03', '1');
INSERT INTO `sys_category` VALUES ('1230769407907631106', '1230769347157331969', '包河区', NULL, 'admin', '2020-02-21 16:21:08', NULL, NULL, 'A01A03', NULL);
INSERT INTO `sys_category` VALUES ('1230769470889299970', '1230769290609725441', '济南市', NULL, 'admin', '2020-02-21 16:21:23', 'admin', '2020-02-21 16:21:41', 'A01A03', '1');
INSERT INTO `sys_category` VALUES ('1230769547519234050', '1230769470889299970', 'A区', NULL, 'admin', '2020-02-21 16:21:41', NULL, NULL, 'A01A03', NULL);
INSERT INTO `sys_category` VALUES ('1230769620021972993', '1230769470889299970', 'B区', NULL, 'admin', '2020-02-21 16:21:58', NULL, NULL, 'A01A03', NULL);
INSERT INTO `sys_category` VALUES ('1230769769930592257', '1230769253267836929', '淮南市', NULL, 'admin', '2020-02-21 16:22:34', 'admin', '2020-02-21 16:22:54', 'A01A03', '1');
INSERT INTO `sys_category` VALUES ('1230769855347593217', '1230769769930592257', 'C区', NULL, 'admin', '2020-02-21 16:22:54', NULL, NULL, 'A01A03', NULL);
INSERT INTO `sys_category` VALUES ('22a50b413c5e1ef661fb8aea9469cf52', 'e9ded10fd33e5753face506f4f1564b5', 'MacBook', 'B01-2-1', 'admin', '2019-06-10 15:43:13', NULL, NULL, 'A01', NULL);
INSERT INTO `sys_category` VALUES ('5c8f68845e57f68ab93a2c8d82d26ae1', '0', '笔记本', 'B01', 'admin', '2019-06-10 15:34:11', 'admin', '2019-06-10 15:34:24', 'A01', '1');
INSERT INTO `sys_category` VALUES ('937fd2e9aa13b8bab1da1ca36d3fd344', 'e9ded10fd33e5753face506f4f1564b5', '台式机', 'B02-2-2', 'admin', '2019-06-10 15:43:32', 'admin', '2019-08-21 12:01:59', 'A01', NULL);
INSERT INTO `sys_category` VALUES ('e9ded10fd33e5753face506f4f1564b5', '5c8f68845e57f68ab93a2c8d82d26ae1', '苹果电脑', 'B01-2', 'admin', '2019-06-10 15:41:14', 'admin', '2019-06-10 15:43:13', 'A01', '1');
INSERT INTO `sys_category` VALUES ('f39a06bf9f390ba4a53d11bc4e0018d7', '5c8f68845e57f68ab93a2c8d82d26ae1', '华为', 'B01-1', 'admin', '2019-06-10 15:34:24', 'admin', '2019-08-21 12:01:56', 'A01', NULL);

-- ----------------------------
-- Table structure for sys_check_rule
-- ----------------------------
DROP TABLE IF EXISTS `sys_check_rule`;
CREATE TABLE `sys_check_rule`  (
  `id` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '主键id',
  `rule_name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '规则名称',
  `rule_code` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '规则Code',
  `rule_json` varchar(1024) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '规则JSON',
  `rule_description` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '规则描述',
  `update_by` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '更新人',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `create_by` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '创建人',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `uni_sys_check_rule_code`(`rule_code`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of sys_check_rule
-- ----------------------------
INSERT INTO `sys_check_rule` VALUES ('1224980593992388610', '通用编码规则', 'common', '[{\"digits\":\"1\",\"pattern\":\"^[a-z|A-Z]$\",\"message\":\"第一位只能是字母\"},{\"digits\":\"*\",\"pattern\":\"^[0-9|a-z|A-Z|_]{0,}$\",\"message\":\"只能填写数字、大小写字母、下划线\"},{\"digits\":\"*\",\"pattern\":\"^.{3,}$\",\"message\":\"最少输入3位数\"},{\"digits\":\"*\",\"pattern\":\"^.{3,12}$\",\"message\":\"最多输入12位数\"}]', '规则：1、首位只能是字母；2、只能填写数字、大小写字母、下划线；3、最少3位数，最多12位数。', 'admin', '2020-02-07 11:25:48', 'admin', '2020-02-05 16:58:27');
INSERT INTO `sys_check_rule` VALUES ('1225001845524004866', '负责的功能测试', 'test', '[{\"digits\":\"*\",\"pattern\":\"^.{3,12}$\",\"message\":\"只能输入3-12位字符\"},{\"digits\":\"3\",\"pattern\":\"^\\\\d{3}$\",\"message\":\"前3位必须是数字\"},{\"digits\":\"*\",\"pattern\":\"^[^pP]*$\",\"message\":\"不能输入P\"},{\"digits\":\"4\",\"pattern\":\"^@{4}$\",\"message\":\"第4-7位必须都为 @\"},{\"digits\":\"2\",\"pattern\":\"^#=$\",\"message\":\"第8-9位必须是 #=\"},{\"digits\":\"1\",\"pattern\":\"^O$\",\"message\":\"第10位必须为大写的O\"},{\"digits\":\"*\",\"pattern\":\"^.*。$\",\"message\":\"必须以。结尾\"}]', '包含长度校验、特殊字符校验等', 'admin', '2020-02-07 11:57:31', 'admin', '2020-02-05 18:22:54');

-- ----------------------------
-- Table structure for sys_data_log
-- ----------------------------
DROP TABLE IF EXISTS `sys_data_log`;
CREATE TABLE `sys_data_log`  (
  `id` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'id',
  `create_by` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '创建人登录名称',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建日期',
  `update_by` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '更新人登录名称',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新日期',
  `data_table` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '表名',
  `data_id` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '数据ID',
  `data_content` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '数据内容',
  `data_version` int(11) NULL DEFAULT NULL COMMENT '版本号',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `sindex`(`data_table`, `data_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of sys_data_log
-- ----------------------------
INSERT INTO `sys_data_log` VALUES ('402880f05ab0d198015ab12274bf0006', 'admin', '2017-03-09 11:35:09', NULL, NULL, 'jeecg_demo', '4028ef81550c1a7901550c1cd6e70001', '{\"mobilePhone\":\"\",\"officePhone\":\"\",\"email\":\"\",\"createDate\":\"Jun 23, 2016 12:00:00 PM\",\"sex\":\"1\",\"depId\":\"402880e447e99cf10147e9a03b320003\",\"userName\":\"9001\",\"status\":\"1\",\"content\":\"111\",\"id\":\"4028ef81550c1a7901550c1cd6e70001\"}', 3);
INSERT INTO `sys_data_log` VALUES ('402880f05ab6d12b015ab700bead0009', 'admin', '2017-03-10 14:56:03', NULL, NULL, 'jeecg_demo', '402880f05ab6d12b015ab700be8d0008', '{\"mobilePhone\":\"\",\"officePhone\":\"\",\"email\":\"\",\"createDate\":\"Mar 10, 2017 2:56:03 PM\",\"sex\":\"0\",\"depId\":\"402880e447e99cf10147e9a03b320003\",\"userName\":\"111\",\"status\":\"0\",\"id\":\"402880f05ab6d12b015ab700be8d0008\"}', 1);
INSERT INTO `sys_data_log` VALUES ('402880f05ab6d12b015ab705a23f000d', 'admin', '2017-03-10 15:01:24', NULL, NULL, 'jeecg_demo', '402880f05ab6d12b015ab705a233000c', '{\"mobilePhone\":\"\",\"officePhone\":\"11\",\"email\":\"\",\"createDate\":\"Mar 10, 2017 3:01:24 PM\",\"sex\":\"0\",\"depId\":\"402880e447e99cf10147e9a03b320003\",\"userName\":\"11\",\"status\":\"0\",\"id\":\"402880f05ab6d12b015ab705a233000c\"}', 1);
INSERT INTO `sys_data_log` VALUES ('402880f05ab6d12b015ab712a6420013', 'admin', '2017-03-10 15:15:37', NULL, NULL, 'jeecg_demo', '402880f05ab6d12b015ab712a6360012', '{\"mobilePhone\":\"\",\"officePhone\":\"\",\"email\":\"\",\"createDate\":\"Mar 10, 2017 3:15:37 PM\",\"sex\":\"0\",\"depId\":\"402880e447e99cf10147e9a03b320003\",\"userName\":\"小王\",\"status\":\"0\",\"id\":\"402880f05ab6d12b015ab712a6360012\"}', 1);
INSERT INTO `sys_data_log` VALUES ('402880f05ab6d12b015ab712d0510015', 'admin', '2017-03-10 15:15:47', NULL, NULL, 'jeecg_demo', '402880f05ab6d12b015ab712a6360012', '{\"mobilePhone\":\"18611788525\",\"officePhone\":\"\",\"email\":\"\",\"createDate\":\"Mar 10, 2017 3:15:37 AM\",\"sex\":\"0\",\"depId\":\"402880e447e99cf10147e9a03b320003\",\"userName\":\"小王\",\"status\":\"0\",\"id\":\"402880f05ab6d12b015ab712a6360012\"}', 2);
INSERT INTO `sys_data_log` VALUES ('402880f05ab6d12b015ab71308240018', 'admin', '2017-03-10 15:16:02', NULL, NULL, 'jeecg_demo', '8a8ab0b246dc81120146dc81860f016f', '{\"mobilePhone\":\"13111111111\",\"officePhone\":\"66666666\",\"email\":\"demo@jeecg.com\",\"age\":12,\"salary\":10.00,\"birthday\":\"Feb 14, 2014 12:00:00 AM\",\"sex\":\"1\",\"depId\":\"402880e447e99cf10147e9a03b320003\",\"userName\":\"小明\",\"status\":\"\",\"content\":\"\",\"id\":\"8a8ab0b246dc81120146dc81860f016f\"}', 1);
INSERT INTO `sys_data_log` VALUES ('402880f05ab6d12b015ab72806c3001b', 'admin', '2017-03-10 15:38:58', NULL, NULL, 'jeecg_demo', '8a8ab0b246dc81120146dc81860f016f', '{\"mobilePhone\":\"18611788888\",\"officePhone\":\"66666666\",\"email\":\"demo@jeecg.com\",\"age\":12,\"salary\":10.00,\"birthday\":\"Feb 14, 2014 12:00:00 AM\",\"sex\":\"1\",\"depId\":\"402880e447e99cf10147e9a03b320003\",\"userName\":\"小明\",\"status\":\"\",\"content\":\"\",\"id\":\"8a8ab0b246dc81120146dc81860f016f\"}', 2);
INSERT INTO `sys_data_log` VALUES ('4028ef815318148a0153181567690001', 'admin', '2016-02-25 18:59:29', NULL, NULL, 'jeecg_demo', '4028ef815318148a0153181566270000', '{\"mobilePhone\":\"13423423423\",\"officePhone\":\"1\",\"email\":\"\",\"age\":1,\"salary\":1,\"birthday\":\"Feb 25, 2016 12:00:00 AM\",\"createDate\":\"Feb 25, 2016 6:59:24 PM\",\"depId\":\"402880e447e9a9570147e9b6a3be0005\",\"userName\":\"1\",\"status\":\"0\",\"id\":\"4028ef815318148a0153181566270000\"}', 1);
INSERT INTO `sys_data_log` VALUES ('4028ef815318148a01531815ec5c0003', 'admin', '2016-02-25 19:00:03', NULL, NULL, 'jeecg_demo', '4028ef815318148a0153181566270000', '{\"mobilePhone\":\"13426498659\",\"officePhone\":\"1\",\"email\":\"\",\"age\":1,\"salary\":1.00,\"birthday\":\"Feb 25, 2016 12:00:00 AM\",\"createDate\":\"Feb 25, 2016 6:59:24 AM\",\"depId\":\"402880e447e9a9570147e9b6a3be0005\",\"userName\":\"1\",\"status\":\"0\",\"id\":\"4028ef815318148a0153181566270000\"}', 2);
INSERT INTO `sys_data_log` VALUES ('4028ef8153c028db0153c0502e6b0003', 'admin', '2016-03-29 10:59:53', NULL, NULL, 'jeecg_demo', '4028ef8153c028db0153c0502d420002', '{\"mobilePhone\":\"18455477548\",\"officePhone\":\"123\",\"email\":\"\",\"createDate\":\"Mar 29, 2016 10:59:53 AM\",\"depId\":\"402880e447e99cf10147e9a03b320003\",\"userName\":\"123\",\"status\":\"0\",\"id\":\"4028ef8153c028db0153c0502d420002\"}', 1);
INSERT INTO `sys_data_log` VALUES ('4028ef8153c028db0153c0509aa40006', 'admin', '2016-03-29 11:00:21', NULL, NULL, 'jeecg_demo', '4028ef8153c028db0153c0509a3e0005', '{\"mobilePhone\":\"13565486458\",\"officePhone\":\"\",\"email\":\"\",\"createDate\":\"Mar 29, 2016 11:00:21 AM\",\"depId\":\"402880e447e99cf10147e9a03b320003\",\"userName\":\"22\",\"status\":\"0\",\"id\":\"4028ef8153c028db0153c0509a3e0005\"}', 1);
INSERT INTO `sys_data_log` VALUES ('4028ef8153c028db0153c051c4a70008', 'admin', '2016-03-29 11:01:37', NULL, NULL, 'jeecg_demo', '4028ef8153c028db0153c0509a3e0005', '{\"mobilePhone\":\"13565486458\",\"officePhone\":\"\",\"email\":\"\",\"createDate\":\"Mar 29, 2016 11:00:21 AM\",\"depId\":\"402880e447e99cf10147e9a03b320003\",\"userName\":\"22\",\"status\":\"0\",\"id\":\"4028ef8153c028db0153c0509a3e0005\"}', 2);
INSERT INTO `sys_data_log` VALUES ('4028ef8153c028db0153c051d4b5000a', 'admin', '2016-03-29 11:01:41', NULL, NULL, 'jeecg_demo', '4028ef8153c028db0153c0502d420002', '{\"mobilePhone\":\"13565486458\",\"officePhone\":\"123\",\"email\":\"\",\"createDate\":\"Mar 29, 2016 10:59:53 AM\",\"depId\":\"402880e447e99cf10147e9a03b320003\",\"userName\":\"123\",\"status\":\"0\",\"id\":\"4028ef8153c028db0153c0502d420002\"}', 2);
INSERT INTO `sys_data_log` VALUES ('4028ef8153c028db0153c07033d8000d', 'admin', '2016-03-29 11:34:52', NULL, NULL, 'jeecg_demo', '4028ef8153c028db0153c0502d420002', '{\"mobilePhone\":\"13565486458\",\"officePhone\":\"123\",\"email\":\"\",\"age\":23,\"createDate\":\"Mar 29, 2016 10:59:53 AM\",\"depId\":\"402880e447e99cf10147e9a03b320003\",\"userName\":\"123\",\"status\":\"0\",\"id\":\"4028ef8153c028db0153c0502d420002\"}', 3);
INSERT INTO `sys_data_log` VALUES ('4028ef8153c028db0153c070492e000f', 'admin', '2016-03-29 11:34:57', NULL, NULL, 'jeecg_demo', '4028ef8153c028db0153c0509a3e0005', '{\"mobilePhone\":\"13565486458\",\"officePhone\":\"\",\"email\":\"\",\"age\":22,\"createDate\":\"Mar 29, 2016 11:00:21 AM\",\"depId\":\"402880e447e99cf10147e9a03b320003\",\"userName\":\"22\",\"status\":\"0\",\"id\":\"4028ef8153c028db0153c0509a3e0005\"}', 3);
INSERT INTO `sys_data_log` VALUES ('4028ef81550c1a7901550c1cd7850002', 'admin', '2016-06-01 21:17:44', NULL, NULL, 'jeecg_demo', '4028ef81550c1a7901550c1cd6e70001', '{\"mobilePhone\":\"\",\"officePhone\":\"\",\"email\":\"\",\"createDate\":\"Jun 1, 2016 9:17:44 PM\",\"sex\":\"1\",\"depId\":\"402880e447e99cf10147e9a03b320003\",\"userName\":\"121221\",\"status\":\"0\",\"id\":\"4028ef81550c1a7901550c1cd6e70001\"}', 1);
INSERT INTO `sys_data_log` VALUES ('4028ef81568c31ec01568c3307080004', 'admin', '2016-08-15 11:16:09', NULL, NULL, 'jeecg_demo', '4028ef81550c1a7901550c1cd6e70001', '{\"mobilePhone\":\"\",\"officePhone\":\"\",\"email\":\"\",\"createDate\":\"Jun 23, 2016 12:00:00 PM\",\"sex\":\"1\",\"depId\":\"402880e447e99cf10147e9a03b320003\",\"userName\":\"9001\",\"status\":\"1\",\"content\":\"111\",\"id\":\"4028ef81550c1a7901550c1cd6e70001\"}', 2);

-- ----------------------------
-- Table structure for sys_data_source
-- ----------------------------
DROP TABLE IF EXISTS `sys_data_source`;
CREATE TABLE `sys_data_source`  (
  `id` varchar(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `code` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '数据源编码',
  `name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '数据源名称',
  `remark` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '备注',
  `db_type` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '数据库类型',
  `db_driver` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '驱动类',
  `db_url` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '数据源地址',
  `db_name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '数据库名称',
  `db_username` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '用户名',
  `db_password` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '密码',
  `create_by` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '创建人',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建日期',
  `update_by` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '更新人',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新日期',
  `sys_org_code` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '所属部门',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `sys_data_source_code_uni`(`code`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of sys_data_source
-- ----------------------------
INSERT INTO `sys_data_source` VALUES ('1209779538310004737', 'local_mysql', 'MySQL5.7', '本地数据库MySQL5.7', '1', 'com.mysql.jdbc.Driver', 'jdbc:mysql://127.0.0.1:3306/jeecg-boot?characterEncoding=UTF-8&useUnicode=true&useSSL=false', 'jeecg-boot', 'root', 'root', 'admin', '2019-12-25 18:14:53', 'admin', '2020-07-10 16:54:42', 'A01');

-- ----------------------------
-- Table structure for sys_depart
-- ----------------------------
DROP TABLE IF EXISTS `sys_depart`;
CREATE TABLE `sys_depart`  (
  `id` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'ID',
  `parent_id` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '父机构ID',
  `depart_name` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '机构/部门名称',
  `depart_name_en` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '英文名',
  `depart_name_abbr` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '缩写',
  `depart_order` int(11) NULL DEFAULT 0 COMMENT '排序',
  `description` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '描述',
  `org_category` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '1' COMMENT '机构类别 1组织机构，2岗位',
  `org_type` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '机构类型 1一级部门 2子部门',
  `org_code` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '机构编码',
  `mobile` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '手机号',
  `fax` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '传真',
  `address` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '地址',
  `memo` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注',
  `status` varchar(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '状态（1启用，0不启用）',
  `del_flag` varchar(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '删除状态（0，正常，1已删除）',
  `create_by` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '创建人',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建日期',
  `update_by` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '更新人',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新日期',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `uniq_depart_org_code`(`org_code`) USING BTREE,
  INDEX `index_depart_parent_id`(`parent_id`) USING BTREE,
  INDEX `index_depart_depart_order`(`depart_order`) USING BTREE,
  INDEX `index_depart_org_code`(`org_code`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '组织机构表' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of sys_depart
-- ----------------------------
INSERT INTO `sys_depart` VALUES ('481ad4d97f62460f9c4a9d2bc3259020', '', '国网二期', NULL, NULL, 1, NULL, '1', '1', 'A03', NULL, NULL, NULL, NULL, NULL, '0', 'admin', '2020-09-02 14:06:24', NULL, NULL);
INSERT INTO `sys_depart` VALUES ('4f1765520d6346f9bd9c79e2479e5b12', 'c6d7cb4deeac411cb3384b1b31278596', '市场部', NULL, NULL, 0, NULL, '1', '2', 'A01A03', NULL, NULL, NULL, NULL, NULL, '0', 'admin', '2019-02-20 17:15:34', 'admin', '2019-02-26 16:36:18');
INSERT INTO `sys_depart` VALUES ('5159cde220114246b045e574adceafe9', '6d35e179cd814e3299bd588ea7daed3f', '研发部', NULL, NULL, 0, NULL, '1', '2', 'A02A02', NULL, NULL, NULL, NULL, NULL, '0', 'admin', '2019-02-26 16:44:38', 'admin', '2019-03-07 09:36:53');
INSERT INTO `sys_depart` VALUES ('57197590443c44f083d42ae24ef26a2c', 'c6d7cb4deeac411cb3384b1b31278596', '研发部', NULL, NULL, 0, NULL, '1', '2', 'A01A05', NULL, NULL, NULL, NULL, NULL, '0', 'admin', '2019-02-21 16:14:41', 'admin', '2019-03-27 19:05:49');
INSERT INTO `sys_depart` VALUES ('63775228b7b041a99825f79760590b7d', '57197590443c44f083d42ae24ef26a2c', '研发经理', NULL, NULL, 0, NULL, '3', '3', 'A01A05A01', NULL, NULL, NULL, NULL, NULL, '0', 'admin', '2020-05-02 15:29:09', NULL, NULL);
INSERT INTO `sys_depart` VALUES ('67fc001af12a4f9b8458005d3f19934a', 'c6d7cb4deeac411cb3384b1b31278596', '财务部', NULL, NULL, 0, NULL, '1', '2', 'A01A04', NULL, NULL, NULL, NULL, NULL, '0', 'admin', '2019-02-21 16:14:35', 'admin', '2019-02-25 12:49:41');
INSERT INTO `sys_depart` VALUES ('6d35e179cd814e3299bd588ea7daed3f', '', '北京卓尔互动', NULL, NULL, 0, NULL, '1', '1', 'A02', NULL, NULL, NULL, NULL, NULL, '0', 'admin', '2019-02-26 16:36:39', 'admin', '2020-05-02 18:21:22');
INSERT INTO `sys_depart` VALUES ('743ba9dbdc114af8953a11022ef3096a', 'f28c6f53abd841ac87ead43afc483433', '财务部', NULL, NULL, 0, NULL, '1', '2', 'A03A01', NULL, NULL, NULL, NULL, NULL, '0', 'admin', '2019-03-22 16:45:43', NULL, NULL);
INSERT INTO `sys_depart` VALUES ('a7d7e77e06c84325a40932163adcdaa6', '6d35e179cd814e3299bd588ea7daed3f', '财务部', NULL, NULL, 0, NULL, '1', '2', 'A02A01', NULL, NULL, NULL, NULL, NULL, '0', 'admin', '2019-02-26 16:36:47', 'admin', '2019-02-26 16:37:25');
INSERT INTO `sys_depart` VALUES ('c6d7cb4deeac411cb3384b1b31278596', '', '北京国炬软件', NULL, NULL, 0, NULL, '1', '1', 'A01', NULL, NULL, NULL, NULL, NULL, '0', 'admin', '2019-02-11 14:21:51', 'admin', '2020-05-02 18:21:27');

-- ----------------------------
-- Table structure for sys_depart_permission
-- ----------------------------
DROP TABLE IF EXISTS `sys_depart_permission`;
CREATE TABLE `sys_depart_permission`  (
  `id` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `depart_id` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '部门id',
  `permission_id` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '权限id',
  `data_rule_ids` varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '数据规则id',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '部门权限表' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of sys_depart_permission
-- ----------------------------
INSERT INTO `sys_depart_permission` VALUES ('1260925131934519297', '6d35e179cd814e3299bd588ea7daed3f', 'f0675b52d89100ee88472b6800754a08', NULL);
INSERT INTO `sys_depart_permission` VALUES ('1260925131947102209', '6d35e179cd814e3299bd588ea7daed3f', '2aeddae571695cd6380f6d6d334d6e7d', NULL);
INSERT INTO `sys_depart_permission` VALUES ('1260925131955490818', '6d35e179cd814e3299bd588ea7daed3f', '020b06793e4de2eee0007f603000c769', NULL);
INSERT INTO `sys_depart_permission` VALUES ('1260925131959685121', '6d35e179cd814e3299bd588ea7daed3f', '1232123780958064642', NULL);
INSERT INTO `sys_depart_permission` VALUES ('1301038977142231041', '481ad4d97f62460f9c4a9d2bc3259020', '1301000181361152001', NULL);
INSERT INTO `sys_depart_permission` VALUES ('1301038977150619649', '481ad4d97f62460f9c4a9d2bc3259020', '1301001912425910273', NULL);
INSERT INTO `sys_depart_permission` VALUES ('1301038977154813953', '481ad4d97f62460f9c4a9d2bc3259020', '1300999947163799554', NULL);
INSERT INTO `sys_depart_permission` VALUES ('1301038977154813954', '481ad4d97f62460f9c4a9d2bc3259020', '1301034658502283266', NULL);
INSERT INTO `sys_depart_permission` VALUES ('1301038977159008257', '481ad4d97f62460f9c4a9d2bc3259020', '1300999295377346562', NULL);
INSERT INTO `sys_depart_permission` VALUES ('1301038977159008258', '481ad4d97f62460f9c4a9d2bc3259020', '1301034916112240641', NULL);
INSERT INTO `sys_depart_permission` VALUES ('1301038977163202562', '481ad4d97f62460f9c4a9d2bc3259020', '1301000639995711490', NULL);
INSERT INTO `sys_depart_permission` VALUES ('1301038977163202563', '481ad4d97f62460f9c4a9d2bc3259020', '1301001622897299458', NULL);
INSERT INTO `sys_depart_permission` VALUES ('1301038977167396866', '481ad4d97f62460f9c4a9d2bc3259020', '1301000852273631234', NULL);
INSERT INTO `sys_depart_permission` VALUES ('1301038977167396867', '481ad4d97f62460f9c4a9d2bc3259020', '1301001284265971713', NULL);
INSERT INTO `sys_depart_permission` VALUES ('1301096418622050306', '481ad4d97f62460f9c4a9d2bc3259020', '1301077101432016897', NULL);
INSERT INTO `sys_depart_permission` VALUES ('1301096418634633218', '481ad4d97f62460f9c4a9d2bc3259020', '1301096112848900098', NULL);
INSERT INTO `sys_depart_permission` VALUES ('1301096418634633219', '481ad4d97f62460f9c4a9d2bc3259020', '1301088665497243650', NULL);
INSERT INTO `sys_depart_permission` VALUES ('1301429842754932738', '481ad4d97f62460f9c4a9d2bc3259020', '1301427338168242177', NULL);
INSERT INTO `sys_depart_permission` VALUES ('1301429842771709954', '481ad4d97f62460f9c4a9d2bc3259020', '1301427916441128962', NULL);
INSERT INTO `sys_depart_permission` VALUES ('1301429842771709955', '481ad4d97f62460f9c4a9d2bc3259020', '1301428144502214658', NULL);
INSERT INTO `sys_depart_permission` VALUES ('1301449130240905218', '481ad4d97f62460f9c4a9d2bc3259020', '1301447721621655554', NULL);
INSERT INTO `sys_depart_permission` VALUES ('1304342703265288193', '481ad4d97f62460f9c4a9d2bc3259020', '1304321289430765570', NULL);
INSERT INTO `sys_depart_permission` VALUES ('1305406593994592257', '481ad4d97f62460f9c4a9d2bc3259020', '1301802633433976834', NULL);
INSERT INTO `sys_depart_permission` VALUES ('1309380273674129410', '481ad4d97f62460f9c4a9d2bc3259020', '1309379979724722178', NULL);
INSERT INTO `sys_depart_permission` VALUES ('1310408081426878466', '481ad4d97f62460f9c4a9d2bc3259020', '1310406951590432769', NULL);
INSERT INTO `sys_depart_permission` VALUES ('1310408081443655681', '481ad4d97f62460f9c4a9d2bc3259020', '1310407190049198081', NULL);
INSERT INTO `sys_depart_permission` VALUES ('1310408081447849986', '481ad4d97f62460f9c4a9d2bc3259020', '1310407413609795585', NULL);
INSERT INTO `sys_depart_permission` VALUES ('1310408081452044289', '481ad4d97f62460f9c4a9d2bc3259020', '1310407620586115073', NULL);
INSERT INTO `sys_depart_permission` VALUES ('1310408081452044290', '481ad4d97f62460f9c4a9d2bc3259020', '1310404050755260417', NULL);
INSERT INTO `sys_depart_permission` VALUES ('1310408081456238593', '481ad4d97f62460f9c4a9d2bc3259020', '1310404258897596417', NULL);
INSERT INTO `sys_depart_permission` VALUES ('1310408081456238594', '481ad4d97f62460f9c4a9d2bc3259020', '1310404620442406913', NULL);
INSERT INTO `sys_depart_permission` VALUES ('1310408081460432897', '481ad4d97f62460f9c4a9d2bc3259020', '1310404991655088130', NULL);
INSERT INTO `sys_depart_permission` VALUES ('1310408081460432898', '481ad4d97f62460f9c4a9d2bc3259020', '1310405261181063170', NULL);
INSERT INTO `sys_depart_permission` VALUES ('1310408081464627201', '481ad4d97f62460f9c4a9d2bc3259020', '1310405668003385346', NULL);
INSERT INTO `sys_depart_permission` VALUES ('1310408081464627202', '481ad4d97f62460f9c4a9d2bc3259020', '1310406050767179777', NULL);
INSERT INTO `sys_depart_permission` VALUES ('1310408081468821506', '481ad4d97f62460f9c4a9d2bc3259020', '1310406264022372354', NULL);
INSERT INTO `sys_depart_permission` VALUES ('1310408081468821507', '481ad4d97f62460f9c4a9d2bc3259020', '1310406614360002562', NULL);
INSERT INTO `sys_depart_permission` VALUES ('1310408081473015809', '481ad4d97f62460f9c4a9d2bc3259020', '1310402513266020353', NULL);
INSERT INTO `sys_depart_permission` VALUES ('1310408081473015810', '481ad4d97f62460f9c4a9d2bc3259020', '1310403041836404738', NULL);
INSERT INTO `sys_depart_permission` VALUES ('1310408081477210114', '481ad4d97f62460f9c4a9d2bc3259020', '1310403230580084737', NULL);
INSERT INTO `sys_depart_permission` VALUES ('1310408081477210115', '481ad4d97f62460f9c4a9d2bc3259020', '1310403473635807234', NULL);
INSERT INTO `sys_depart_permission` VALUES ('1310408081477210116', '481ad4d97f62460f9c4a9d2bc3259020', '1310403837600731138', NULL);
INSERT INTO `sys_depart_permission` VALUES ('1310408081481404417', '481ad4d97f62460f9c4a9d2bc3259020', '1310399637995130881', NULL);
INSERT INTO `sys_depart_permission` VALUES ('1310408081481404418', '481ad4d97f62460f9c4a9d2bc3259020', '1310400302846840834', NULL);
INSERT INTO `sys_depart_permission` VALUES ('1310408081485598721', '481ad4d97f62460f9c4a9d2bc3259020', '1310400531214110721', NULL);
INSERT INTO `sys_depart_permission` VALUES ('1310408081485598722', '481ad4d97f62460f9c4a9d2bc3259020', '1310400808977698817', NULL);
INSERT INTO `sys_depart_permission` VALUES ('1310408081489793025', '481ad4d97f62460f9c4a9d2bc3259020', '1310401085768208385', NULL);
INSERT INTO `sys_depart_permission` VALUES ('1310408081489793026', '481ad4d97f62460f9c4a9d2bc3259020', '1310401229901271042', NULL);
INSERT INTO `sys_depart_permission` VALUES ('1310408081493987330', '481ad4d97f62460f9c4a9d2bc3259020', '1310401446956503042', NULL);
INSERT INTO `sys_depart_permission` VALUES ('1310408081493987331', '481ad4d97f62460f9c4a9d2bc3259020', '1310401620621660162', NULL);
INSERT INTO `sys_depart_permission` VALUES ('1310408081493987332', '481ad4d97f62460f9c4a9d2bc3259020', '1310401771595632641', NULL);
INSERT INTO `sys_depart_permission` VALUES ('1310408081498181634', '481ad4d97f62460f9c4a9d2bc3259020', '1310401950704996353', NULL);
INSERT INTO `sys_depart_permission` VALUES ('1310413664972443650', '481ad4d97f62460f9c4a9d2bc3259020', '1310413060296413185', NULL);
INSERT INTO `sys_depart_permission` VALUES ('1310413664980832258', '481ad4d97f62460f9c4a9d2bc3259020', '1310413263179091969', NULL);
INSERT INTO `sys_depart_permission` VALUES ('1310413664985026561', '481ad4d97f62460f9c4a9d2bc3259020', '1310409534656745473', NULL);
INSERT INTO `sys_depart_permission` VALUES ('1310413664985026562', '481ad4d97f62460f9c4a9d2bc3259020', '1310410737562161153', NULL);
INSERT INTO `sys_depart_permission` VALUES ('1310413664989220865', '481ad4d97f62460f9c4a9d2bc3259020', '1310412463300153345', NULL);
INSERT INTO `sys_depart_permission` VALUES ('1310413664989220866', '481ad4d97f62460f9c4a9d2bc3259020', '1310409822184673282', NULL);
INSERT INTO `sys_depart_permission` VALUES ('1310413664993415169', '481ad4d97f62460f9c4a9d2bc3259020', '1310410227111170050', NULL);
INSERT INTO `sys_depart_permission` VALUES ('1310413664993415170', '481ad4d97f62460f9c4a9d2bc3259020', '1310410534536876033', NULL);
INSERT INTO `sys_depart_permission` VALUES ('1310413664997609473', '481ad4d97f62460f9c4a9d2bc3259020', '1310410017085591553', NULL);
INSERT INTO `sys_depart_permission` VALUES ('1310416996919152642', '481ad4d97f62460f9c4a9d2bc3259020', '1310415795251056642', NULL);
INSERT INTO `sys_depart_permission` VALUES ('1310416996931735554', '481ad4d97f62460f9c4a9d2bc3259020', '1310416188978761730', NULL);
INSERT INTO `sys_depart_permission` VALUES ('1310416996931735555', '481ad4d97f62460f9c4a9d2bc3259020', '1310416488150077442', NULL);
INSERT INTO `sys_depart_permission` VALUES ('1310433456152383489', '481ad4d97f62460f9c4a9d2bc3259020', '1310418138898108418', NULL);
INSERT INTO `sys_depart_permission` VALUES ('1310433456169160705', '481ad4d97f62460f9c4a9d2bc3259020', '1310422936447881217', NULL);
INSERT INTO `sys_depart_permission` VALUES ('1310433456173355010', '481ad4d97f62460f9c4a9d2bc3259020', '1310432505559519233', NULL);
INSERT INTO `sys_depart_permission` VALUES ('1310433456227880961', '481ad4d97f62460f9c4a9d2bc3259020', '1310432897492062210', NULL);
INSERT INTO `sys_depart_permission` VALUES ('1310433456232075266', '481ad4d97f62460f9c4a9d2bc3259020', '1310433274404802561', NULL);
INSERT INTO `sys_depart_permission` VALUES ('1310434014435217410', '481ad4d97f62460f9c4a9d2bc3259020', '1310433915395117057', NULL);

-- ----------------------------
-- Table structure for sys_depart_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_depart_role`;
CREATE TABLE `sys_depart_role`  (
  `id` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `depart_id` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '部门id',
  `role_name` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '部门角色名称',
  `role_code` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '部门角色编码',
  `description` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '描述',
  `create_by` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '创建人',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '更新人',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '部门角色表' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of sys_depart_role
-- ----------------------------
INSERT INTO `sys_depart_role` VALUES ('1260925293226479618', '6d35e179cd814e3299bd588ea7daed3f', 'roless', 'ssss', NULL, 'admin', '2020-05-14 21:29:51', NULL, NULL);

-- ----------------------------
-- Table structure for sys_depart_role_permission
-- ----------------------------
DROP TABLE IF EXISTS `sys_depart_role_permission`;
CREATE TABLE `sys_depart_role_permission`  (
  `id` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `depart_id` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '部门id',
  `role_id` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '角色id',
  `permission_id` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '权限id',
  `data_rule_ids` varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '数据权限ids',
  `operate_date` datetime(0) NULL DEFAULT NULL COMMENT '操作时间',
  `operate_ip` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '操作ip',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `index_group_role_per_id`(`role_id`, `permission_id`) USING BTREE,
  INDEX `index_group_role_id`(`role_id`) USING BTREE,
  INDEX `index_group_per_id`(`permission_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '部门角色权限表' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of sys_depart_role_permission
-- ----------------------------
INSERT INTO `sys_depart_role_permission` VALUES ('1260925328689319938', NULL, '1260925293226479618', '2aeddae571695cd6380f6d6d334d6e7d', NULL, NULL, NULL);
INSERT INTO `sys_depart_role_permission` VALUES ('1260925328706097153', NULL, '1260925293226479618', '020b06793e4de2eee0007f603000c769', NULL, NULL, NULL);

-- ----------------------------
-- Table structure for sys_depart_role_user
-- ----------------------------
DROP TABLE IF EXISTS `sys_depart_role_user`;
CREATE TABLE `sys_depart_role_user`  (
  `id` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '主键id',
  `user_id` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '用户id',
  `drole_id` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '角色id',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '部门角色用户表' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for sys_dict
-- ----------------------------
DROP TABLE IF EXISTS `sys_dict`;
CREATE TABLE `sys_dict`  (
  `id` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `dict_name` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '字典名称',
  `dict_code` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '字典编码',
  `description` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '描述',
  `del_flag` int(1) NULL DEFAULT NULL COMMENT '删除状态',
  `create_by` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '创建人',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '更新人',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `type` int(1) UNSIGNED ZEROFILL NULL DEFAULT 0 COMMENT '字典类型0为string,1为number',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `indextable_dict_code`(`dict_code`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of sys_dict
-- ----------------------------
INSERT INTO `sys_dict` VALUES ('0b5d19e1fce4b2e6647e6b4a17760c14', '通告类型', 'msg_category', '消息类型1:通知公告2:系统消息', 0, 'admin', '2019-04-22 18:01:35', NULL, NULL, 0);
INSERT INTO `sys_dict` VALUES ('1174509082208395266', '职务职级', 'position_rank', '职务表职级字典', 0, 'admin', '2019-09-19 10:22:41', NULL, NULL, 0);
INSERT INTO `sys_dict` VALUES ('1174511106530525185', '机构类型', 'org_category', '机构类型 1公司，2部门 3岗位', 0, 'admin', '2019-09-19 10:30:43', NULL, NULL, 0);
INSERT INTO `sys_dict` VALUES ('1178295274528845826', '表单权限策略', 'form_perms_type', '', 0, 'admin', '2019-09-29 21:07:39', 'admin', '2019-09-29 21:08:26', NULL);
INSERT INTO `sys_dict` VALUES ('1199517671259906049', '紧急程度', 'urgent_level', '日程计划紧急程度', 0, 'admin', '2019-11-27 10:37:53', NULL, NULL, 0);
INSERT INTO `sys_dict` VALUES ('1199518099888414722', '日程计划类型', 'eoa_plan_type', '', 0, 'admin', '2019-11-27 10:39:36', NULL, NULL, 0);
INSERT INTO `sys_dict` VALUES ('1199520177767587841', '分类栏目类型', 'eoa_cms_menu_type', '', 0, 'admin', '2019-11-27 10:47:51', 'admin', '2019-11-27 10:49:35', 0);
INSERT INTO `sys_dict` VALUES ('1199525215290306561', '日程计划状态', 'eoa_plan_status', '', 0, 'admin', '2019-11-27 11:07:52', 'admin', '2019-11-27 11:10:11', 0);
INSERT INTO `sys_dict` VALUES ('1209733563293962241', '数据库类型', 'database_type', '', 0, 'admin', '2019-12-25 15:12:12', NULL, NULL, 0);
INSERT INTO `sys_dict` VALUES ('1232913193820581889', 'Online表单业务分类', 'ol_form_biz_type', '', 0, 'admin', '2020-02-27 14:19:46', 'admin', '2020-02-27 14:20:23', 0);
INSERT INTO `sys_dict` VALUES ('1250687930947620866', '定时任务状态', 'quartz_status', '', 0, 'admin', '2020-04-16 15:30:14', '', NULL, NULL);
INSERT INTO `sys_dict` VALUES ('1280401766745718786', '租户状态', 'tenant_status', '租户状态', 0, 'admin', '2020-07-07 15:22:25', NULL, NULL, 0);
INSERT INTO `sys_dict` VALUES ('1335150161583706113', '迁移策略配置', 'scheduler_strategy_config', NULL, 0, 'sgccyw1', '2020-12-05 17:12:59', NULL, NULL, 0);
INSERT INTO `sys_dict` VALUES ('1335150415955660801', '调度模型工具', 'scheduler_model_tool', NULL, 0, 'sgccyw1', '2020-12-05 17:14:00', NULL, NULL, 0);
INSERT INTO `sys_dict` VALUES ('1335150540656513025', '调度模型', 'scheduler_model', NULL, 0, 'sgccyw1', '2020-12-05 17:14:29', NULL, NULL, 0);
INSERT INTO `sys_dict` VALUES ('1335151234955666492', '调度迁移异常开关', 'scheduler_error_switch', NULL, 0, 'sgccyw1', '2020-12-05 17:14:00', NULL, NULL, 0);
INSERT INTO `sys_dict` VALUES ('1335151234955660218', '监控类型修改配置', 'monitor_type_config', NULL, 0, 'sgccyw1', '2020-12-05 17:14:00', NULL, NULL, 0);
INSERT INTO `sys_dict` VALUES ('1335151234955660219', '监控数据波动配置', 'monitor_statistics_config', NULL, 0, 'sgccyw1', '2020-12-05 17:14:00', NULL, NULL, 0);
INSERT INTO `sys_dict` VALUES ('1335153205469810690', '迁移策略执行规则', 'scheduler_strategy_rule', NULL, 0, 'sgccyw1', '2020-12-05 17:25:05', NULL, NULL, 0);
INSERT INTO `sys_dict` VALUES ('1342732234116886529', '日志配置', 'log_config', NULL, 0, 'zhangzj', '2020-12-26 15:21:26', NULL, NULL, 0);
INSERT INTO `sys_dict` VALUES ('236e8a4baff0db8c62c00dd95632834f', '同步工作流引擎', 'activiti_sync', '同步工作流引擎', 0, 'admin', '2019-05-15 15:27:33', NULL, NULL, 0);
INSERT INTO `sys_dict` VALUES ('2e02df51611a4b9632828ab7e5338f00', '权限策略', 'perms_type', '权限策略', 0, 'admin', '2019-04-26 18:26:55', NULL, NULL, 0);
INSERT INTO `sys_dict` VALUES ('2f0320997ade5dd147c90130f7218c3e', '推送类别', 'msg_type', '', 0, 'admin', '2019-03-17 21:21:32', 'admin', '2019-03-26 19:57:45', 0);
INSERT INTO `sys_dict` VALUES ('3486f32803bb953e7155dab3513dc68b', '删除状态', 'del_flag', NULL, 0, 'admin', '2019-01-18 21:46:26', 'admin', '2019-03-30 11:17:11', 0);
INSERT INTO `sys_dict` VALUES ('3d9a351be3436fbefb1307d4cfb49bf2', '性别', 'sex', NULL, 0, NULL, '2019-01-04 14:56:32', 'admin', '2019-03-30 11:28:27', 1);
INSERT INTO `sys_dict` VALUES ('4274efc2292239b6f000b153f50823ff', '全局权限策略', 'global_perms_type', '全局权限策略', 0, 'admin', '2019-05-10 17:54:05', NULL, NULL, 0);
INSERT INTO `sys_dict` VALUES ('4c03fca6bf1f0299c381213961566349', 'Online图表展示模板', 'online_graph_display_template', 'Online图表展示模板', 0, 'admin', '2019-04-12 17:28:50', NULL, NULL, 0);
INSERT INTO `sys_dict` VALUES ('4c753b5293304e7a445fd2741b46529d', '字典状态', 'dict_item_status', NULL, 0, 'admin', '2020-06-18 23:18:42', 'admin', '2019-03-30 19:33:52', 1);
INSERT INTO `sys_dict` VALUES ('4d7fec1a7799a436d26d02325eff295e', '优先级', 'priority', '优先级', 0, 'admin', '2019-03-16 17:03:34', 'admin', '2019-04-16 17:39:23', 0);
INSERT INTO `sys_dict` VALUES ('4e4602b3e3686f0911384e188dc7efb4', '条件规则', 'rule_conditions', '', 0, 'admin', '2019-04-01 10:15:03', 'admin', '2019-04-01 10:30:47', 0);
INSERT INTO `sys_dict` VALUES ('4f69be5f507accea8d5df5f11346181a', '发送消息类型', 'msgType', NULL, 0, 'admin', '2019-04-11 14:27:09', NULL, NULL, 0);
INSERT INTO `sys_dict` VALUES ('68168534ff5065a152bfab275c2136f8', '有效无效状态', 'valid_status', '有效无效状态', 0, 'admin', '2020-09-26 19:21:14', 'admin', '2019-04-26 19:21:23', 0);
INSERT INTO `sys_dict` VALUES ('6b78e3f59faec1a4750acff08030a79b', '用户类型', 'user_type', NULL, 0, NULL, '2019-01-04 14:59:01', 'admin', '2019-03-18 23:28:18', 0);
INSERT INTO `sys_dict` VALUES ('72cce0989df68887546746d8f09811aa', 'Online表单类型', 'cgform_table_type', '', 0, 'admin', '2019-01-27 10:13:02', 'admin', '2019-03-30 11:37:36', 0);
INSERT INTO `sys_dict` VALUES ('78bda155fe380b1b3f175f1e88c284c6', '流程状态', 'bpm_status', '流程状态', 0, 'admin', '2019-05-09 16:31:52', NULL, NULL, 0);
INSERT INTO `sys_dict` VALUES ('83bfb33147013cc81640d5fd9eda030c', '日志类型', 'log_type', NULL, 0, 'admin', '2019-03-18 23:22:19', NULL, NULL, 1);
INSERT INTO `sys_dict` VALUES ('845da5006c97754728bf48b6a10f79cc', '状态', 'status', NULL, 0, 'admin', '2019-03-18 21:45:25', 'admin', '2019-03-18 21:58:25', 0);
INSERT INTO `sys_dict` VALUES ('880a895c98afeca9d9ac39f29e67c13e', '操作类型', 'operate_type', '操作类型', 0, 'admin', '2019-07-22 10:54:29', NULL, NULL, 0);
INSERT INTO `sys_dict` VALUES ('8dfe32e2d29ea9430a988b3b558bf233', '发布状态', 'send_status', '发布状态', 0, 'admin', '2019-04-16 17:40:42', NULL, NULL, 0);
INSERT INTO `sys_dict` VALUES ('a7adbcd86c37f7dbc9b66945c82ef9e6', '1是0否', 'yn', '', 0, 'admin', '2019-05-22 19:29:29', NULL, NULL, 0);
INSERT INTO `sys_dict` VALUES ('a9d9942bd0eccb6e89de92d130ec4c4a', '消息发送状态', 'msgSendStatus', NULL, 0, 'admin', '2019-04-12 18:18:17', NULL, NULL, 0);
INSERT INTO `sys_dict` VALUES ('ac2f7c0c5c5775fcea7e2387bcb22f01', '菜单类型', 'menu_type', NULL, 0, 'admin', '2020-12-18 23:24:32', 'admin', '2019-04-01 15:27:06', 1);
INSERT INTO `sys_dict` VALUES ('ad7c65ba97c20a6805d5dcdf13cdaf36', 'onlineT类型', 'ceshi_online', NULL, 0, 'admin', '2019-03-22 16:31:49', 'admin', '2019-03-22 16:34:16', 0);
INSERT INTO `sys_dict` VALUES ('bd1b8bc28e65d6feefefb6f3c79f42fd', 'Online图表数据类型', 'online_graph_data_type', 'Online图表数据类型', 0, 'admin', '2019-04-12 17:24:24', 'admin', '2019-04-12 17:24:57', 0);
INSERT INTO `sys_dict` VALUES ('c36169beb12de8a71c8683ee7c28a503', '部门状态', 'depart_status', NULL, 0, 'admin', '2019-03-18 21:59:51', NULL, NULL, 0);
INSERT INTO `sys_dict` VALUES ('c5a14c75172783d72cbee6ee7f5df5d1', 'Online图表类型', 'online_graph_type', 'Online图表类型', 0, 'admin', '2019-04-12 17:04:06', NULL, NULL, 0);
INSERT INTO `sys_dict` VALUES ('d6e1152968b02d69ff358c75b48a6ee1', '流程类型', 'bpm_process_type', NULL, 0, 'admin', '2021-02-22 19:26:54', 'admin', '2019-03-30 18:14:44', 0);
INSERT INTO `sys_dict` VALUES ('fc6cd58fde2e8481db10d3a1e68ce70c', '用户状态', 'user_status', NULL, 0, 'admin', '2019-03-18 21:57:25', 'admin', '2019-03-18 23:11:58', 1);

-- ----------------------------
-- Table structure for sys_dict_item
-- ----------------------------
DROP TABLE IF EXISTS `sys_dict_item`;
CREATE TABLE `sys_dict_item`  (
  `id` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `dict_id` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '字典id',
  `item_text` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '字典项文本',
  `item_value` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '字典项值',
  `description` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '描述',
  `sort_order` int(10) NULL DEFAULT NULL COMMENT '排序',
  `status` int(11) NULL DEFAULT NULL COMMENT '状态（1启用 0不启用）',
  `create_by` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `create_time` datetime(0) NULL DEFAULT NULL,
  `update_by` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `update_time` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `index_table_dict_id`(`dict_id`) USING BTREE,
  INDEX `index_table_sort_order`(`sort_order`) USING BTREE,
  INDEX `index_table_dict_status`(`status`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of sys_dict_item
-- ----------------------------
INSERT INTO `sys_dict_item` VALUES ('0072d115e07c875d76c9b022e2179128', '4d7fec1a7799a436d26d02325eff295e', '低', 'L', '低', 3, 1, 'admin', '2019-04-16 17:04:59', NULL, NULL);
INSERT INTO `sys_dict_item` VALUES ('05a2e732ce7b00aa52141ecc3e330b4e', '3486f32803bb953e7155dab3513dc68b', '已删除', '1', NULL, NULL, 1, 'admin', '2025-10-18 21:46:56', 'admin', '2019-03-28 22:23:20');
INSERT INTO `sys_dict_item` VALUES ('096c2e758d823def3855f6376bc736fb', 'bd1b8bc28e65d6feefefb6f3c79f42fd', 'SQL', 'sql', NULL, 1, 1, 'admin', '2019-04-12 17:26:26', NULL, NULL);
INSERT INTO `sys_dict_item` VALUES ('0c9532916f5cd722017b46bc4d953e41', '2f0320997ade5dd147c90130f7218c3e', '指定用户', 'USER', NULL, NULL, 1, 'admin', '2019-03-17 21:22:19', 'admin', '2019-03-17 21:22:28');
INSERT INTO `sys_dict_item` VALUES ('0ca4beba9efc4f9dd54af0911a946d5c', '72cce0989df68887546746d8f09811aa', '附表', '3', NULL, 3, 1, 'admin', '2019-03-27 10:13:43', NULL, NULL);
INSERT INTO `sys_dict_item` VALUES ('1030a2652608f5eac3b49d70458b8532', '2e02df51611a4b9632828ab7e5338f00', '禁用', '2', '禁用', 2, 1, 'admin', '2021-03-26 18:27:28', 'admin', '2019-04-26 18:39:11');
INSERT INTO `sys_dict_item` VALUES ('1174509082208395266', '1174511106530525185', '岗位', '3', '岗位', 1, 1, 'admin', '2019-09-19 10:31:16', '', NULL);
INSERT INTO `sys_dict_item` VALUES ('1174509601047994369', '1174509082208395266', '员级', '1', '', 1, 1, 'admin', '2019-09-19 10:24:45', 'admin', '2019-09-23 11:46:39');
INSERT INTO `sys_dict_item` VALUES ('1174509667297026049', '1174509082208395266', '助级', '2', '', 2, 1, 'admin', '2019-09-19 10:25:01', 'admin', '2019-09-23 11:46:47');
INSERT INTO `sys_dict_item` VALUES ('1174509713568587777', '1174509082208395266', '中级', '3', '', 3, 1, 'admin', '2019-09-19 10:25:12', 'admin', '2019-09-23 11:46:56');
INSERT INTO `sys_dict_item` VALUES ('1174509788361416705', '1174509082208395266', '副高级', '4', '', 4, 1, 'admin', '2019-09-19 10:25:30', 'admin', '2019-09-23 11:47:06');
INSERT INTO `sys_dict_item` VALUES ('1174509835803189250', '1174509082208395266', '正高级', '5', '', 5, 1, 'admin', '2019-09-19 10:25:41', 'admin', '2019-09-23 11:47:12');
INSERT INTO `sys_dict_item` VALUES ('1174511197735665665', '1174511106530525185', '公司', '1', '公司', 1, 1, 'admin', '2019-09-19 10:31:05', NULL, NULL);
INSERT INTO `sys_dict_item` VALUES ('1174511244036587521', '1174511106530525185', '部门', '2', '部门', 1, 1, 'admin', '2019-09-19 10:31:16', NULL, NULL);
INSERT INTO `sys_dict_item` VALUES ('1178295553450061826', '1178295274528845826', '可编辑(未授权禁用)', '2', '', 2, 1, 'admin', '2019-09-29 21:08:46', 'admin', '2019-09-29 21:09:18');
INSERT INTO `sys_dict_item` VALUES ('1178295639554928641', '1178295274528845826', '可见(未授权不可见)', '1', '', 1, 1, 'admin', '2019-09-29 21:09:06', 'admin', '2019-09-29 21:09:24');
INSERT INTO `sys_dict_item` VALUES ('1199517884758368257', '1199517671259906049', '一般', '1', '', 1, 1, 'admin', '2019-11-27 10:38:44', NULL, NULL);
INSERT INTO `sys_dict_item` VALUES ('1199517914017832962', '1199517671259906049', '重要', '2', '', 1, 1, 'admin', '2019-11-27 10:38:51', NULL, NULL);
INSERT INTO `sys_dict_item` VALUES ('1199517941339529217', '1199517671259906049', '紧急', '3', '', 1, 1, 'admin', '2019-11-27 10:38:58', NULL, NULL);
INSERT INTO `sys_dict_item` VALUES ('1199518186144276482', '1199518099888414722', '日常记录', '1', '', 1, 1, 'admin', '2019-11-27 10:39:56', NULL, NULL);
INSERT INTO `sys_dict_item` VALUES ('1199518214858481666', '1199518099888414722', '本周工作', '2', '', 1, 1, 'admin', '2019-11-27 10:40:03', NULL, NULL);
INSERT INTO `sys_dict_item` VALUES ('1199518235943247874', '1199518099888414722', '下周计划', '3', '', 1, 1, 'admin', '2019-11-27 10:40:08', NULL, NULL);
INSERT INTO `sys_dict_item` VALUES ('1199520817285701634', '1199520177767587841', '列表', '1', '', 1, 1, 'admin', '2019-11-27 10:50:24', NULL, NULL);
INSERT INTO `sys_dict_item` VALUES ('1199520835035996161', '1199520177767587841', '链接', '2', '', 1, 1, 'admin', '2019-11-27 10:50:28', NULL, NULL);
INSERT INTO `sys_dict_item` VALUES ('1199525468672405505', '1199525215290306561', '未开始', '0', '', 1, 1, 'admin', '2019-11-27 11:08:52', NULL, NULL);
INSERT INTO `sys_dict_item` VALUES ('1199525490575060993', '1199525215290306561', '进行中', '1', '', 1, 1, 'admin', '2019-11-27 11:08:58', NULL, NULL);
INSERT INTO `sys_dict_item` VALUES ('1199525506429530114', '1199525215290306561', '已完成', '2', '', 1, 1, 'admin', '2019-11-27 11:09:02', 'admin', '2019-11-27 11:10:02');
INSERT INTO `sys_dict_item` VALUES ('1199607547704647681', '4f69be5f507accea8d5df5f11346181a', '系统', '4', '', 1, 1, 'admin', '2019-11-27 16:35:02', 'admin', '2019-11-27 19:37:46');
INSERT INTO `sys_dict_item` VALUES ('1209733775114702850', '1209733563293962241', 'MySQL', '1', '', 1, 1, 'admin', '2019-12-25 15:13:02', NULL, NULL);
INSERT INTO `sys_dict_item` VALUES ('1209733839933476865', '1209733563293962241', 'Oracle', '2', '', 1, 1, 'admin', '2019-12-25 15:13:18', NULL, NULL);
INSERT INTO `sys_dict_item` VALUES ('1209733903020003330', '1209733563293962241', 'SQLServer', '3', '', 1, 1, 'admin', '2019-12-25 15:13:33', NULL, NULL);
INSERT INTO `sys_dict_item` VALUES ('1232913424813486081', '1232913193820581889', '官方示例', 'demo', '', 1, 1, 'admin', '2020-02-27 14:20:42', 'admin', '2020-02-27 14:21:37');
INSERT INTO `sys_dict_item` VALUES ('1232913493717512194', '1232913193820581889', '流程表单', 'bpm', '', 2, 1, 'admin', '2020-02-27 14:20:58', 'admin', '2020-02-27 14:22:20');
INSERT INTO `sys_dict_item` VALUES ('1232913605382467585', '1232913193820581889', '测试表单', 'temp', '', 4, 1, 'admin', '2020-02-27 14:21:25', 'admin', '2020-02-27 14:22:16');
INSERT INTO `sys_dict_item` VALUES ('1232914232372195330', '1232913193820581889', '导入表单', 'bdfl_include', '', 5, 1, 'admin', '2020-02-27 14:23:54', NULL, NULL);
INSERT INTO `sys_dict_item` VALUES ('1234371726545010689', '4e4602b3e3686f0911384e188dc7efb4', '左模糊', 'LEFT_LIKE', '左模糊', 7, 1, 'admin', '2020-03-02 14:55:27', NULL, NULL);
INSERT INTO `sys_dict_item` VALUES ('1234371809495760898', '4e4602b3e3686f0911384e188dc7efb4', '右模糊', 'RIGHT_LIKE', '右模糊', 7, 1, 'admin', '2020-03-02 14:55:47', NULL, NULL);
INSERT INTO `sys_dict_item` VALUES ('1250688147579228161', '1250687930947620866', '正常', '0', '', 1, 1, 'admin', '2020-04-16 15:31:05', '', NULL);
INSERT INTO `sys_dict_item` VALUES ('1250688201064992770', '1250687930947620866', '停止', '-1', '', 1, 1, 'admin', '2020-04-16 15:31:18', '', NULL);
INSERT INTO `sys_dict_item` VALUES ('1280401815068295170', '1280401766745718786', '正常', '1', '', 1, 1, 'admin', '2020-07-07 15:22:36', NULL, NULL);
INSERT INTO `sys_dict_item` VALUES ('1280401847607705602', '1280401766745718786', '冻结', '0', '', 1, 1, 'admin', '2020-07-07 15:22:44', NULL, NULL);
INSERT INTO `sys_dict_item` VALUES ('1335127356595015682', '1335127356406272002', 'Kubernetes迁移工具', '0', NULL, NULL, NULL, 'sgcc_super_admin', '2020-12-05 15:42:22', NULL, NULL);
INSERT INTO `sys_dict_item` VALUES ('1335136137383022593', '1335136137265582081', '迁移策略执行规则', '{\"cooling\":2,\"beginTime\":\"2020-12-01 11:11:11\",\"endTime\":\"2020-12-03 22:22:22\",\"concurrent\":\"100\"}', NULL, NULL, NULL, 'sgcc_super_admin', '2020-12-05 16:17:15', NULL, NULL);
INSERT INTO `sys_dict_item` VALUES ('1335136268480188418', '1335136268375330818', 'Kubernetes迁移工具', '0', NULL, NULL, NULL, 'sgcc_super_admin', '2020-12-05 16:17:47', NULL, NULL);
INSERT INTO `sys_dict_item` VALUES ('1335136268559880193', '1335136268375330818', '阿里云迁移工具', '1', NULL, NULL, NULL, 'sgcc_super_admin', '2020-12-05 16:17:47', NULL, NULL);
INSERT INTO `sys_dict_item` VALUES ('1335152326729568258', '1335150540656513025', '最优模型', '0', NULL, NULL, NULL, 'sgcc_super_admin', '2020-12-05 17:21:35', 'sgcc_super_admin', '2020-12-05 18:55:59');
INSERT INTO `sys_dict_item` VALUES ('1335178592312659970', '1335150415955660801', 'Kubernetes迁移工具', '0', NULL, NULL, NULL, 'sgcc_super_admin', '2020-12-05 19:05:57', NULL, NULL);
INSERT INTO `sys_dict_item` VALUES ('1335178592392351745', '1335150415955660801', '阿里云迁移工具', '1', NULL, NULL, NULL, 'sgcc_super_admin', '2020-12-05 19:05:58', NULL, NULL);
INSERT INTO `sys_dict_item` VALUES ('1335179151585349634', '1335150161583706113', '阿里云迁移工具', '1', NULL, NULL, NULL, 'sgcc_super_admin', '2020-12-05 19:08:11', NULL, NULL);
INSERT INTO `sys_dict_item` VALUES ('1341712115307679735', '1335151234955666492', '调度迁移异常开关', '0', NULL, NULL, NULL, 'sgcc_super_admin', '2020-12-05 19:05:58', NULL, NULL);
INSERT INTO `sys_dict_item` VALUES ('1341712115307670233', '1335151234955660218', '监控类型修改配置', '3', NULL, NULL, NULL, 'sgcc_super_admin', '2020-12-05 19:05:58', NULL, NULL);
INSERT INTO `sys_dict_item` VALUES ('1341712115307670234', '1335151234955660219', '监控数据波动配置', '0', NULL, NULL, NULL, 'sgcc_super_admin', '2020-12-05 19:05:58', NULL, NULL);
INSERT INTO `sys_dict_item` VALUES ('1335194394730467330', '1335153205469810690', '迁移策略执行规则', '{\"cooling\":5,\"beginTime\":\"2020-12-05 08:08:21\",\"endTime\":\"2020-12-20 08:08:21\",\"concurrent\":55}', NULL, NULL, NULL, 'sgcc_super_admin', '2020-12-05 20:08:45', NULL, NULL);
INSERT INTO `sys_dict_item` VALUES ('1342732234183995393', '1342732234116886529', 'listSwitch', '1', NULL, NULL, NULL, 'zhangzj', '2020-12-26 15:21:26', NULL, NULL);
INSERT INTO `sys_dict_item` VALUES ('1342732234221744129', '1342732234116886529', 'detailSwitch', '1', NULL, NULL, NULL, 'zhangzj', '2020-12-26 15:21:26', NULL, NULL);
INSERT INTO `sys_dict_item` VALUES ('1342732234255298562', '1342732234116886529', 'addSwitch', '1', NULL, NULL, NULL, 'zhangzj', '2020-12-26 15:21:26', NULL, NULL);
INSERT INTO `sys_dict_item` VALUES ('1342732234330796034', '1342732234116886529', 'updateSwitch', '1', NULL, NULL, NULL, 'zhangzj', '2020-12-26 15:21:26', NULL, NULL);
INSERT INTO `sys_dict_item` VALUES ('1342732234372739074', '1342732234116886529', 'deleteSwitch', '1', NULL, NULL, NULL, 'zhangzj', '2020-12-26 15:21:26', NULL, NULL);
INSERT INTO `sys_dict_item` VALUES ('1342732234372739123', '1342732234116886529', 'sqlSwitch', '1', NULL, NULL, NULL, 'zhangzj', '2020-12-26 15:21:26', NULL, NULL);
INSERT INTO `sys_dict_item` VALUES ('1342732234410487809', '1342732234116886529', 'capacity', '10', NULL, NULL, NULL, 'zhangzj', '2020-12-26 15:21:26', NULL, NULL);
INSERT INTO `sys_dict_item` VALUES ('1342732234410487829', '1342732234116886529', 'appQuerySwitch', '1', NULL, NULL, NULL, 'zhangzj', '2020-12-26 15:21:26', NULL, NULL);
INSERT INTO `sys_dict_item` VALUES ('147c48ff4b51545032a9119d13f3222a', 'd6e1152968b02d69ff358c75b48a6ee1', '测试流程', 'test', NULL, 1, 1, 'admin', '2019-03-22 19:27:05', NULL, NULL);
INSERT INTO `sys_dict_item` VALUES ('1543fe7e5e26fb97cdafe4981bedc0c8', '4c03fca6bf1f0299c381213961566349', '单排布局', 'single', NULL, 2, 1, 'admin', '2022-07-12 17:43:39', 'admin', '2019-04-12 17:43:57');
INSERT INTO `sys_dict_item` VALUES ('1ce390c52453891f93514c1bd2795d44', 'ad7c65ba97c20a6805d5dcdf13cdaf36', '000', '00', NULL, 1, 1, 'admin', '2019-03-22 16:34:34', NULL, NULL);
INSERT INTO `sys_dict_item` VALUES ('1db531bcff19649fa82a644c8a939dc4', '4c03fca6bf1f0299c381213961566349', '组合布局', 'combination', '', 4, 1, 'admin', '2019-05-11 16:07:08', NULL, NULL);
INSERT INTO `sys_dict_item` VALUES ('222705e11ef0264d4214affff1fb4ff9', '4f69be5f507accea8d5df5f11346181a', '短信', '1', '', 1, 1, 'admin', '2023-02-28 10:50:36', 'admin', '2019-04-28 10:58:11');
INSERT INTO `sys_dict_item` VALUES ('23a5bb76004ed0e39414e928c4cde155', '4e4602b3e3686f0911384e188dc7efb4', '不等于', '!=', '不等于', 3, 1, 'admin', '2019-04-01 16:46:15', 'admin', '2019-04-01 17:48:40');
INSERT INTO `sys_dict_item` VALUES ('25847e9cb661a7c711f9998452dc09e6', '4e4602b3e3686f0911384e188dc7efb4', '小于等于', '<=', '小于等于', 6, 1, 'admin', '2019-04-01 16:44:34', 'admin', '2019-04-01 17:49:10');
INSERT INTO `sys_dict_item` VALUES ('2d51376643f220afdeb6d216a8ac2c01', '68168534ff5065a152bfab275c2136f8', '有效', '1', '有效', 2, 1, 'admin', '2019-04-26 19:22:01', NULL, NULL);
INSERT INTO `sys_dict_item` VALUES ('308c8aadf0c37ecdde188b97ca9833f5', '8dfe32e2d29ea9430a988b3b558bf233', '已发布', '1', '已发布', 2, 1, 'admin', '2019-04-16 17:41:24', NULL, NULL);
INSERT INTO `sys_dict_item` VALUES ('333e6b2196e01ef9a5f76d74e86a6e33', '8dfe32e2d29ea9430a988b3b558bf233', '未发布', '0', '未发布', 1, 1, 'admin', '2019-04-16 17:41:12', NULL, NULL);
INSERT INTO `sys_dict_item` VALUES ('337ea1e401bda7233f6258c284ce4f50', 'bd1b8bc28e65d6feefefb6f3c79f42fd', 'JSON', 'json', NULL, 1, 1, 'admin', '2019-04-12 17:26:33', NULL, NULL);
INSERT INTO `sys_dict_item` VALUES ('33bc9d9f753cf7dc40e70461e50fdc54', 'a9d9942bd0eccb6e89de92d130ec4c4a', '发送失败', '2', NULL, 3, 1, 'admin', '2019-04-12 18:20:02', NULL, NULL);
INSERT INTO `sys_dict_item` VALUES ('3fbc03d6c994ae06d083751248037c0e', '78bda155fe380b1b3f175f1e88c284c6', '已完成', '3', '已完成', 3, 1, 'admin', '2019-05-09 16:33:25', NULL, NULL);
INSERT INTO `sys_dict_item` VALUES ('41d7aaa40c9b61756ffb1f28da5ead8e', '0b5d19e1fce4b2e6647e6b4a17760c14', '通知公告', '1', NULL, 1, 1, 'admin', '2019-04-22 18:01:57', NULL, NULL);
INSERT INTO `sys_dict_item` VALUES ('41fa1e9571505d643aea87aeb83d4d76', '4e4602b3e3686f0911384e188dc7efb4', '等于', '=', '等于', 4, 1, 'admin', '2019-04-01 16:45:24', 'admin', '2019-04-01 17:49:00');
INSERT INTO `sys_dict_item` VALUES ('43d2295b8610adce9510ff196a49c6e9', '845da5006c97754728bf48b6a10f79cc', '正常', '1', NULL, NULL, 1, 'admin', '2019-03-18 21:45:51', NULL, NULL);
INSERT INTO `sys_dict_item` VALUES ('4f05fb5376f4c61502c5105f52e4dd2b', '83bfb33147013cc81640d5fd9eda030c', '操作日志', '2', NULL, NULL, 1, 'admin', '2019-03-18 23:22:49', NULL, NULL);
INSERT INTO `sys_dict_item` VALUES ('50223341bfb5ba30bf6319789d8d17fe', 'd6e1152968b02d69ff358c75b48a6ee1', '业务办理', 'business', NULL, 3, 1, 'admin', '2023-04-22 19:28:05', 'admin', '2019-03-22 23:24:39');
INSERT INTO `sys_dict_item` VALUES ('51222413e5906cdaf160bb5c86fb827c', 'a7adbcd86c37f7dbc9b66945c82ef9e6', '是', '1', '', 1, 1, 'admin', '2019-05-22 19:29:45', NULL, NULL);
INSERT INTO `sys_dict_item` VALUES ('538fca35afe004972c5f3947c039e766', '2e02df51611a4b9632828ab7e5338f00', '显示', '1', '显示', 1, 1, 'admin', '2025-03-26 18:27:13', 'admin', '2019-04-26 18:39:07');
INSERT INTO `sys_dict_item` VALUES ('5584c21993bde231bbde2b966f2633ac', '4e4602b3e3686f0911384e188dc7efb4', '自定义SQL表达式', 'USE_SQL_RULES', '自定义SQL表达式', 9, 1, 'admin', '2019-04-01 10:45:24', 'admin', '2019-04-01 17:49:27');
INSERT INTO `sys_dict_item` VALUES ('58b73b344305c99b9d8db0fc056bbc0a', '72cce0989df68887546746d8f09811aa', '主表', '2', NULL, 2, 1, 'admin', '2019-03-27 10:13:36', NULL, NULL);
INSERT INTO `sys_dict_item` VALUES ('5b65a88f076b32e8e69d19bbaadb52d5', '2f0320997ade5dd147c90130f7218c3e', '全体用户', 'ALL', NULL, NULL, 1, 'admin', '2020-10-17 21:22:43', 'admin', '2019-03-28 22:17:09');
INSERT INTO `sys_dict_item` VALUES ('5d833f69296f691843ccdd0c91212b6b', '880a895c98afeca9d9ac39f29e67c13e', '修改', '3', '', 3, 1, 'admin', '2019-07-22 10:55:07', 'admin', '2019-07-22 10:55:41');
INSERT INTO `sys_dict_item` VALUES ('5d84a8634c8fdfe96275385075b105c9', '3d9a351be3436fbefb1307d4cfb49bf2', '女', '2', NULL, 2, 1, NULL, '2019-01-04 14:56:56', NULL, '2019-01-04 17:38:12');
INSERT INTO `sys_dict_item` VALUES ('66c952ae2c3701a993e7db58f3baf55e', '4e4602b3e3686f0911384e188dc7efb4', '大于', '>', '大于', 1, 1, 'admin', '2019-04-01 10:45:46', 'admin', '2019-04-01 17:48:29');
INSERT INTO `sys_dict_item` VALUES ('6937c5dde8f92e9a00d4e2ded9198694', 'ad7c65ba97c20a6805d5dcdf13cdaf36', 'easyui', '3', NULL, 1, 1, 'admin', '2019-03-22 16:32:15', NULL, NULL);
INSERT INTO `sys_dict_item` VALUES ('69cacf64e244100289ddd4aa9fa3b915', 'a9d9942bd0eccb6e89de92d130ec4c4a', '未发送', '0', NULL, 1, 1, 'admin', '2019-04-12 18:19:23', NULL, NULL);
INSERT INTO `sys_dict_item` VALUES ('6a7a9e1403a7943aba69e54ebeff9762', '4f69be5f507accea8d5df5f11346181a', '邮件', '2', '', 2, 1, 'admin', '2031-02-28 10:50:44', 'admin', '2019-04-28 10:59:03');
INSERT INTO `sys_dict_item` VALUES ('6c682d78ddf1715baf79a1d52d2aa8c2', '72cce0989df68887546746d8f09811aa', '单表', '1', NULL, 1, 1, 'admin', '2019-03-27 10:13:29', NULL, NULL);
INSERT INTO `sys_dict_item` VALUES ('6d404fd2d82311fbc87722cd302a28bc', '4e4602b3e3686f0911384e188dc7efb4', '模糊', 'LIKE', '模糊', 7, 1, 'admin', '2019-04-01 16:46:02', 'admin', '2019-04-01 17:49:20');
INSERT INTO `sys_dict_item` VALUES ('6d4e26e78e1a09699182e08516c49fc4', '4d7fec1a7799a436d26d02325eff295e', '高', 'H', '高', 1, 1, 'admin', '2019-04-16 17:04:24', NULL, NULL);
INSERT INTO `sys_dict_item` VALUES ('700e9f030654f3f90e9ba76ab0713551', '6b78e3f59faec1a4750acff08030a79b', '333', '333', NULL, NULL, 1, 'admin', '2019-02-21 19:59:47', NULL, NULL);
INSERT INTO `sys_dict_item` VALUES ('7050c1522702bac3be40e3b7d2e1dfd8', 'c5a14c75172783d72cbee6ee7f5df5d1', '柱状图', 'bar', NULL, 1, 1, 'admin', '2019-04-12 17:05:17', NULL, NULL);
INSERT INTO `sys_dict_item` VALUES ('71b924faa93805c5c1579f12e001c809', 'd6e1152968b02d69ff358c75b48a6ee1', 'OA办公', 'oa', NULL, 2, 1, 'admin', '2021-03-22 19:27:17', 'admin', '2019-03-22 23:24:36');
INSERT INTO `sys_dict_item` VALUES ('75b260d7db45a39fc7f21badeabdb0ed', 'c36169beb12de8a71c8683ee7c28a503', '不启用', '0', NULL, NULL, 1, 'admin', '2019-03-18 23:29:41', 'admin', '2019-03-18 23:29:54');
INSERT INTO `sys_dict_item` VALUES ('7688469db4a3eba61e6e35578dc7c2e5', 'c36169beb12de8a71c8683ee7c28a503', '启用', '1', NULL, NULL, 1, 'admin', '2019-03-18 23:29:28', NULL, NULL);
INSERT INTO `sys_dict_item` VALUES ('78ea6cadac457967a4b1c4eb7aaa418c', 'fc6cd58fde2e8481db10d3a1e68ce70c', '正常', '1', NULL, NULL, 1, 'admin', '2019-03-18 23:30:28', NULL, NULL);
INSERT INTO `sys_dict_item` VALUES ('7ccf7b80c70ee002eceb3116854b75cb', 'ac2f7c0c5c5775fcea7e2387bcb22f01', '按钮权限', '2', NULL, NULL, 1, 'admin', '2019-03-18 23:25:40', NULL, NULL);
INSERT INTO `sys_dict_item` VALUES ('81fb2bb0e838dc68b43f96cc309f8257', 'fc6cd58fde2e8481db10d3a1e68ce70c', '冻结', '2', NULL, NULL, 1, 'admin', '2019-03-18 23:30:37', NULL, NULL);
INSERT INTO `sys_dict_item` VALUES ('83250269359855501ec4e9c0b7e21596', '4274efc2292239b6f000b153f50823ff', '可见/可访问(授权后可见/可访问)', '1', '', 1, 1, 'admin', '2019-05-10 17:54:51', NULL, NULL);
INSERT INTO `sys_dict_item` VALUES ('84778d7e928bc843ad4756db1322301f', '4e4602b3e3686f0911384e188dc7efb4', '大于等于', '>=', '大于等于', 5, 1, 'admin', '2019-04-01 10:46:02', 'admin', '2019-04-01 17:49:05');
INSERT INTO `sys_dict_item` VALUES ('848d4da35ebd93782029c57b103e5b36', 'c5a14c75172783d72cbee6ee7f5df5d1', '饼图', 'pie', NULL, 3, 1, 'admin', '2019-04-12 17:05:49', NULL, NULL);
INSERT INTO `sys_dict_item` VALUES ('84dfc178dd61b95a72900fcdd624c471', '78bda155fe380b1b3f175f1e88c284c6', '处理中', '2', '处理中', 2, 1, 'admin', '2019-05-09 16:33:01', NULL, NULL);
INSERT INTO `sys_dict_item` VALUES ('86f19c7e0a73a0bae451021ac05b99dd', 'ac2f7c0c5c5775fcea7e2387bcb22f01', '子菜单', '1', NULL, NULL, 1, 'admin', '2019-03-18 23:25:27', NULL, NULL);
INSERT INTO `sys_dict_item` VALUES ('8bccb963e1cd9e8d42482c54cc609ca2', '4f69be5f507accea8d5df5f11346181a', '微信', '3', NULL, 3, 1, 'admin', '2021-05-11 14:29:12', 'admin', '2019-04-11 14:29:31');
INSERT INTO `sys_dict_item` VALUES ('8c618902365ca681ebbbe1e28f11a548', '4c753b5293304e7a445fd2741b46529d', '启用', '1', '', 0, 1, 'admin', '2020-07-18 23:19:27', 'admin', '2019-05-17 14:51:18');
INSERT INTO `sys_dict_item` VALUES ('8cdf08045056671efd10677b8456c999', '4274efc2292239b6f000b153f50823ff', '可编辑(未授权时禁用)', '2', '', 2, 1, 'admin', '2019-05-10 17:55:38', NULL, NULL);
INSERT INTO `sys_dict_item` VALUES ('8ff48e657a7c5090d4f2a59b37d1b878', '4d7fec1a7799a436d26d02325eff295e', '中', 'M', '中', 2, 1, 'admin', '2019-04-16 17:04:40', NULL, NULL);
INSERT INTO `sys_dict_item` VALUES ('948923658baa330319e59b2213cda97c', '880a895c98afeca9d9ac39f29e67c13e', '添加', '2', '', 2, 1, 'admin', '2019-07-22 10:54:59', 'admin', '2019-07-22 10:55:36');
INSERT INTO `sys_dict_item` VALUES ('9a96c4a4e4c5c9b4e4d0cbf6eb3243cc', '4c753b5293304e7a445fd2741b46529d', '不启用', '0', NULL, 1, 1, 'admin', '2019-03-18 23:19:53', NULL, NULL);
INSERT INTO `sys_dict_item` VALUES ('a1e7d1ca507cff4a480c8caba7c1339e', '880a895c98afeca9d9ac39f29e67c13e', '导出', '6', '', 6, 1, 'admin', '2019-07-22 12:06:50', NULL, NULL);
INSERT INTO `sys_dict_item` VALUES ('a2be752dd4ec980afaec1efd1fb589af', '8dfe32e2d29ea9430a988b3b558bf233', '已撤销', '2', '已撤销', 3, 1, 'admin', '2019-04-16 17:41:39', NULL, NULL);
INSERT INTO `sys_dict_item` VALUES ('aa0d8a8042a18715a17f0a888d360aa4', 'ac2f7c0c5c5775fcea7e2387bcb22f01', '一级菜单', '0', NULL, NULL, 1, 'admin', '2019-03-18 23:24:52', NULL, NULL);
INSERT INTO `sys_dict_item` VALUES ('adcf2a1fe93bb99a84833043f475fe0b', '4e4602b3e3686f0911384e188dc7efb4', '包含', 'IN', '包含', 8, 1, 'admin', '2019-04-01 16:45:47', 'admin', '2019-04-01 17:49:24');
INSERT INTO `sys_dict_item` VALUES ('b029a41a851465332ee4ee69dcf0a4c2', '0b5d19e1fce4b2e6647e6b4a17760c14', '系统消息', '2', NULL, 1, 1, 'admin', '2019-02-22 18:02:08', 'admin', '2019-04-22 18:02:13');
INSERT INTO `sys_dict_item` VALUES ('b2a8b4bb2c8e66c2c4b1bb086337f393', '3486f32803bb953e7155dab3513dc68b', '正常', '0', NULL, NULL, 1, 'admin', '2022-10-18 21:46:48', 'admin', '2019-03-28 22:22:20');
INSERT INTO `sys_dict_item` VALUES ('b57f98b88363188daf38d42f25991956', '6b78e3f59faec1a4750acff08030a79b', '22', '222', NULL, NULL, 0, 'admin', '2019-02-21 19:59:43', 'admin', '2019-03-11 21:23:27');
INSERT INTO `sys_dict_item` VALUES ('b5f3bd5f66bb9a83fecd89228c0d93d1', '68168534ff5065a152bfab275c2136f8', '无效', '0', '无效', 1, 1, 'admin', '2019-04-26 19:21:49', NULL, NULL);
INSERT INTO `sys_dict_item` VALUES ('b9fbe2a3602d4a27b45c100ac5328484', '78bda155fe380b1b3f175f1e88c284c6', '待提交', '1', '待提交', 1, 1, 'admin', '2019-05-09 16:32:35', NULL, NULL);
INSERT INTO `sys_dict_item` VALUES ('ba27737829c6e0e582e334832703d75e', '236e8a4baff0db8c62c00dd95632834f', '同步', '1', '同步', 1, 1, 'admin', '2019-05-15 15:28:15', NULL, NULL);
INSERT INTO `sys_dict_item` VALUES ('bcec04526b04307e24a005d6dcd27fd6', '880a895c98afeca9d9ac39f29e67c13e', '导入', '5', '', 5, 1, 'admin', '2019-07-22 12:06:41', NULL, NULL);
INSERT INTO `sys_dict_item` VALUES ('c53da022b9912e0aed691bbec3c78473', '880a895c98afeca9d9ac39f29e67c13e', '查询', '1', '', 1, 1, 'admin', '2019-07-22 10:54:51', NULL, NULL);
INSERT INTO `sys_dict_item` VALUES ('c5700a71ad08994d18ad1dacc37a71a9', 'a7adbcd86c37f7dbc9b66945c82ef9e6', '否', '0', '', 1, 1, 'admin', '2019-05-22 19:29:55', NULL, NULL);
INSERT INTO `sys_dict_item` VALUES ('cbfcc5b88fc3a90975df23ffc8cbe29c', 'c5a14c75172783d72cbee6ee7f5df5d1', '曲线图', 'line', NULL, 2, 1, 'admin', '2019-05-12 17:05:30', 'admin', '2019-04-12 17:06:06');
INSERT INTO `sys_dict_item` VALUES ('d217592908ea3e00ff986ce97f24fb98', 'c5a14c75172783d72cbee6ee7f5df5d1', '数据列表', 'table', NULL, 4, 1, 'admin', '2019-04-12 17:05:56', NULL, NULL);
INSERT INTO `sys_dict_item` VALUES ('df168368dcef46cade2aadd80100d8aa', '3d9a351be3436fbefb1307d4cfb49bf2', '男', '1', NULL, 1, 1, NULL, '2027-08-04 14:56:49', 'admin', '2019-03-23 22:44:44');
INSERT INTO `sys_dict_item` VALUES ('e6329e3a66a003819e2eb830b0ca2ea0', '4e4602b3e3686f0911384e188dc7efb4', '小于', '<', '小于', 2, 1, 'admin', '2019-04-01 16:44:15', 'admin', '2019-04-01 17:48:34');
INSERT INTO `sys_dict_item` VALUES ('e94eb7af89f1dbfa0d823580a7a6e66a', '236e8a4baff0db8c62c00dd95632834f', '不同步', '0', '不同步', 2, 1, 'admin', '2019-05-15 15:28:28', NULL, NULL);
INSERT INTO `sys_dict_item` VALUES ('f0162f4cc572c9273f3e26b2b4d8c082', 'ad7c65ba97c20a6805d5dcdf13cdaf36', 'booostrap', '1', NULL, 1, 1, 'admin', '2021-08-22 16:32:04', 'admin', '2019-03-22 16:33:57');
INSERT INTO `sys_dict_item` VALUES ('f16c5706f3ae05c57a53850c64ce7c45', 'a9d9942bd0eccb6e89de92d130ec4c4a', '发送成功', '1', NULL, 2, 1, 'admin', '2019-04-12 18:19:43', NULL, NULL);
INSERT INTO `sys_dict_item` VALUES ('f2a7920421f3335afdf6ad2b342f6b5d', '845da5006c97754728bf48b6a10f79cc', '冻结', '2', NULL, NULL, 1, 'admin', '2019-03-18 21:46:02', NULL, NULL);
INSERT INTO `sys_dict_item` VALUES ('f37f90c496ec9841c4c326b065e00bb2', '83bfb33147013cc81640d5fd9eda030c', '登录日志', '1', NULL, NULL, 1, 'admin', '2019-03-18 23:22:37', NULL, NULL);
INSERT INTO `sys_dict_item` VALUES ('f753aff60ff3931c0ecb4812d8b5e643', '4c03fca6bf1f0299c381213961566349', '双排布局', 'double', NULL, 3, 1, 'admin', '2019-04-12 17:43:51', NULL, NULL);
INSERT INTO `sys_dict_item` VALUES ('f80a8f6838215753b05e1a5ba3346d22', '880a895c98afeca9d9ac39f29e67c13e', '删除', '4', '', 4, 1, 'admin', '2019-07-22 10:55:14', 'admin', '2019-07-22 10:55:30');
INSERT INTO `sys_dict_item` VALUES ('fcec03570f68a175e1964808dc3f1c91', '4c03fca6bf1f0299c381213961566349', 'Tab风格', 'tab', NULL, 1, 1, 'admin', '2019-04-12 17:43:31', NULL, NULL);
INSERT INTO `sys_dict_item` VALUES ('fe50b23ae5e68434def76f67cef35d2d', '78bda155fe380b1b3f175f1e88c284c6', '已作废', '4', '已作废', 4, 1, 'admin', '2021-09-09 16:33:43', 'admin', '2019-05-09 16:34:40');

-- ----------------------------
-- Table structure for sys_fill_rule
-- ----------------------------
DROP TABLE IF EXISTS `sys_fill_rule`;
CREATE TABLE `sys_fill_rule`  (
  `id` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '主键ID',
  `rule_name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '规则名称',
  `rule_code` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '规则Code',
  `rule_class` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '规则实现类',
  `rule_params` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '规则参数',
  `update_by` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '修改人',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '修改时间',
  `create_by` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '创建人',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `uni_sys_fill_rule_code`(`rule_code`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of sys_fill_rule
-- ----------------------------
INSERT INTO `sys_fill_rule` VALUES ('1202551334738382850', '机构编码生成', 'org_num_role', 'org.jeecg.modules.system.rule.OrgCodeRule', '{\"parentId\":\"c6d7cb4deeac411cb3384b1b31278596\"}', 'admin', '2019-12-09 10:37:06', 'admin', '2019-12-05 19:32:35');
INSERT INTO `sys_fill_rule` VALUES ('1202787623203065858', '分类字典编码生成', 'category_code_rule', 'org.jeecg.modules.system.rule.CategoryCodeRule', '{\"pid\":\"\"}', 'admin', '2019-12-09 10:36:54', 'admin', '2019-12-06 11:11:31');
INSERT INTO `sys_fill_rule` VALUES ('1260134137920090113', '商城订单流水号', 'shop_order_num', 'org.jeecg.modules.system.rule.OrderNumberRule', '{}', 'admin', '2020-07-11 11:35:00', 'admin', '2020-05-12 17:06:05');

-- ----------------------------
-- Table structure for sys_gateway_route
-- ----------------------------
DROP TABLE IF EXISTS `sys_gateway_route`;
CREATE TABLE `sys_gateway_route`  (
  `id` varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `name` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '服务名',
  `uri` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '服务地址',
  `predicates` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '断言',
  `filters` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '过滤器',
  `retryable` int(3) NULL DEFAULT NULL COMMENT '是否重试:0-否 1-是',
  `strip_prefix` int(3) NULL DEFAULT NULL COMMENT '是否忽略前缀0-否 1-是',
  `persist` int(3) NULL DEFAULT NULL COMMENT '是否为保留数据:0-否 1-是',
  `show_api` int(3) NULL DEFAULT NULL COMMENT '是否在接口文档中展示:0-否 1-是',
  `status` int(3) NULL DEFAULT NULL COMMENT '状态:0-无效 1-有效',
  `create_by` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '创建人',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建日期',
  `update_by` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '更新人',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新日期',
  `sys_org_code` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '所属部门',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of sys_gateway_route
-- ----------------------------
INSERT INTO `sys_gateway_route` VALUES ('jeecg-cloud-demo-biz', 'jeecg-cloud-demo-biz', 'lb://jeecg-cloud-demo-biz', '[{\"args\":{\"_genkey_0\":\"/demo/**\"},\"name\":\"Path\"}]', NULL, NULL, NULL, NULL, NULL, 1, NULL, '2020-05-28 14:01:32', NULL, NULL, NULL);
INSERT INTO `sys_gateway_route` VALUES ('jeecg-cloud-system-biz', 'jeecg-cloud-system-biz', 'lb://jeecg-cloud-system-biz', '[{\"args\":{\"_genkey_0\":\"/sys/**\",\"_genkey_5\":\"/bigscreen/**\",\"_genkey_6\":\"/webSocketApi/**\",\"_genkey_1\":\"/test/**\",\"_genkey_2\":\"/online/**\",\"_genkey_3\":\"/api/**\",\"_genkey_4\":\"/big/screen/**\"},\"name\":\"Path\"}]', NULL, NULL, NULL, NULL, NULL, 1, NULL, '2020-05-28 14:01:32', NULL, NULL, NULL);
INSERT INTO `sys_gateway_route` VALUES ('jeecg-cloud-websocket', 'jeecg-cloud-websocket', 'lb:ws://jeecg-cloud-system-biz', '[{\"args\":{\"_genkey_0\":\"/websocket/**\"},\"name\":\"Path\"}]', NULL, NULL, NULL, NULL, NULL, 1, NULL, '2020-05-28 14:01:32', NULL, NULL, NULL);

-- ----------------------------
-- Table structure for sys_log
-- ----------------------------
DROP TABLE IF EXISTS `sys_log`;
CREATE TABLE `sys_log`  (
  `id` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `log_type` int(2) NULL DEFAULT NULL COMMENT '日志类型（1登录日志，2操作日志）',
  `log_content` varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '日志内容',
  `operate_type` int(2) NULL DEFAULT NULL COMMENT '操作类型',
  `operate_type_name` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '操作类型名称',
  `userid` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '操作用户账号',
  `username` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '操作用户名称',
  `ip` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'IP',
  `module` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '模块名称',
  `method` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '请求java方法',
  `request_url` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '请求路径',
  `request_param` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '请求参数',
  `request_type` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '请求类型',
  `cost_time` bigint(20) NULL DEFAULT NULL COMMENT '耗时',
  `create_by` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '创建人',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '更新人',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `remark` text COMMENT '备注',
  `status` tinyint(4) DEFAULT NULL COMMENT '状态 成功：1 失败： 0',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `index_table_userid`(`userid`) USING BTREE,
  INDEX `index_logt_ype`(`log_type`) USING BTREE,
  INDEX `index_operate_type`(`operate_type`) USING BTREE,
  INDEX `index_log_type`(`log_type`) USING BTREE
) ENGINE = MyISAM CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '系统日志表' ROW_FORMAT = Dynamic;


INSERT INTO `sys_log` VALUES ('1349301589383921858', 4, '精确管控与供电制冷联动-基础资源细粒度感知-资源管理-数据中心', 18, '数据库错误', NULL, NULL, '192.168.215.45', '精确管控与供电制冷联动-基础资源细粒度感知-资源管理-数据中心', NULL, '/profile/pageFeature', NULL, NULL, NULL, 'sgccyw1', '2021-1-20 18:25:42', NULL, NULL, '[Err] 1064 - You have an error in your SQL syntax; check the manual that corresponds to your MySQL server version for the right syntax to use near \'sql:SELECT a.id AS app_id, a.name AS app_name, b.feature_suggest, b.feature_choo\' at line 1', 1);
INSERT INTO `sys_log` VALUES ('1349301628122521101', 4, '精确管控与供电制冷联动-基础资源细粒度感知-资源管理-L4层：应用资源', 18, '数据库错误', NULL, NULL, '192.168.215.45', '精确管控与供电制冷联动-基础资源细粒度感知-资源管理-L4层：应用资源', NULL, '/scheduler/job/page', NULL, NULL, NULL, 'sgccyw1', '2021-1-20 17:25:52', NULL, NULL, '[Err] 1064 - You have an error in your SQL syntax; check the manual that corresponds to your MySQL server version for the right syntax to use near \'sql:SELECT d.* FROM dcs_scheduler_jobs d  WHERE 1=1  ORDER BY d.begin_time DESC \' at line 1', 1);
INSERT INTO `sys_log` VALUES ('1349302058898513121', 4, '精确管控与供电制冷联动-基础资源细粒度感知-资源管理-L2层：IT设备', 18, '数据库错误', NULL, NULL, '192.168.215.45', '精确管控与供电制冷联动-基础资源细粒度感知-资源管理-L2层：IT设备', NULL, '/scheduler/job/page', NULL, NULL, NULL, 'sgccyw1', '2021-1-20 20:27:34', NULL, NULL, '[Err] 1064 - You have an error in your SQL syntax; check the manual that corresponds to your MySQL server version for the right syntax to use near \'sql:select * from `asset_datacenters`\' at line 1', 1);
INSERT INTO `sys_log` VALUES ('1351482891627954412', 4, '【sgccyw1(E0CA3B6207CF483A8C4E9429CC340C2L)】于2021-01-19 18:55在【健康智能监控-数据中心设备亚健康分析与预警-亚健康预警】进行了操作，操作类型为【程序错误】，操作结果为【失败】，操作内容为【查询告警列表】', 16, '程序错误', 'E0CA3B6207CF483A8C4E9429CC340C2L', 'sgccyw1', '192.168.204.81', '健康智能监控-数据中心设备亚健康分析与预警-亚健康预警', 'com.gcloud.mesh.analysis.controller.SubhealthController.pageAlert()', '/subhealth/pageAlert', '[{\"endTime\":1610534341000,\"msgId\":\"ea68d952-0da8-424c-8d01-27bcb857c350\",\"pageNo\":1,\"pageSize\":10,\"startTime\":1610793541000}]', NULL, 0, 'sgccyw1', '2021-1-19 18:55:25', NULL, NULL, '[ERROR] [org.springframework.scheduling.support.TaskUtils$LoggingErrorHandler:] Unexpected error occurred in scheduled task\r\njava.lang.NullPointerException: null\r\nat com.gcloud.mesh.asset.service.impl.AssetServiceImpl.pageNode(AssetServiceImpl.java:728) ~[classes!/:1.0.0]\r\nat com.gcloud.mesh.asset.service.impl.AssetServiceImpl.pageNode(AssetServiceImpl.java:703) ~[classes!/:1.0.0]\r\n	at com.gcloud.mesh.asset.service.impl.AssetServiceImpl$$FastClassBySpringCGLIB$$ebaee86e.invoke(<generated>) ~[classes!/:1.0.0]\r\n	at org.springframework.cglib.proxy.MethodProxy.invoke(MethodProxy.java:218) ~[spring-core-5.2.4.RELEASE.jar!/:5.2.4.RELEASE]\r\n	at org.springframework.aop.framework.CglibAopProxy$DynamicAdvisedInterceptor.intercept(CglibAopProxy.java:685) ~[spring-aop-5.2.4.RELEASE.jar!/:5.2.4.RELEASE]\r\n', 0);
INSERT INTO `sys_log` VALUES ('1351482891627954516', 4, '【sgccyw1(E0CA3B6207CF483A8C4E9429CC340C2L)】于2021-01-19 18:57在【精确管控与供电制冷联动-基础资源粒度感知-诊断告警-诊断告警信息】进行了操作，操作类型为【程序错误】，操作结果为【失败】，操作内容为【更新策略】', 16, '程序错误', 'E0CA3B6207CF483A8C4E9429CC340C2L', 'sgccyw1', '192.168.204.81', '精确管控与供电制冷联动-基础资源粒度感知-诊断告警-诊断告警信息', 'com.gcloud.mesh.alert.controller.PolicyController.update()', '/policy/update', '[{\"endTime\":1610534341000,\"msgId\":\"ea68d952-0da8-424c-8d01-27bcb857c350\",\"pageNo\":1,\"pageSize\":10,\"startTime\":1610793541000}]', NULL, 0, 'sgccyw1', '2021-1-19 18:57:25', NULL, NULL, '[ERROR] [org.jeecg.common.exception.JeecgBootExceptionHandler:] ::阈值取值范围0~999999\r\ncom.gcloud.mesh.header.exception.BaseException:com.gcloud.mesh.alert.service.PolicyService.update(PolicyService.java:290) ~[classes!/:1.0.0]\r\nat com.gcloud.mesh.alert.controller.PolicyController.update(PolicyController.java:94) ~[classes!/:1.0.0]\r\n	at com.gcloud.mesh.alert.controller.PolicyController$$FastClassBySpringCGLIB$$13a0fb60.invoke(<generated>) ~[classes!/:1.0.0]\r\n	at org.springframework.cglib.proxy.MethodProxy.invoke(MethodProxy.java:218) ~[spring-core-5.2.4.RELEASE.jar!/:5.2.4.RELEASE]\r\n', 0);
INSERT INTO `sys_log` VALUES ('1351482891627344189', 4, '【sgccyw1(E0CA3B6207CF483A8C4E9429CC340C2L)】于2021-01-19 18:58在【精确管控与供电制冷联动-基础资源细粒度感知-资源管理-L2层：IT设备】进行了操作，操作类型为【程序错误】，操作结果为【失败】，操作内容为【更新策略】', 16, '程序错误', 'E0CA3B6207CF483A8C4E9429CC340C2L', 'sgccyw1', '192.168.204.81', '精确管控与供电制冷联动-基础资源细粒度感知-资源管理-L2层：IT设备', 'com.gcloud.mesh.alert.controller.PolicyController.update()', '/policy/update', '[{\"endTime\":1610534341000,\"msgId\":\"ea68d952-0da8-424c-8d01-27bcb857c350\",\"pageNo\":1,\"pageSize\":10,\"startTime\":1610793541000}]', NULL, 0, 'sgccyw1', '2021-1-19 18:58:38', NULL, NULL, '[ERROR] [druid.sql.Statement:] {conn-10004, pstmt-27803} execute error. INSERT INTO sys_user (id, username, realname, password, salt, status, del_flag, create_time, third_id, third_type) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)\r\njava.sql.SQLIntegrityConstraintViolationException: Duplicate entry 40286c146127810a01612b3cefc10007 for key PRIMARY\r\n	at com.mysql.cj.jdbc.exceptions.SQLError.createSQLException(SQLError.java:117) ~[mysql-connector-java-8.0.19.jar!/:8.0.19]\r\n	at com.mysql.cj.jdbc.exceptions.SQLError.createSQLException(SQLError.java:97) ~[mysql-connector-java-8.0.19.jar!/:8.0.19]\r\n', 0);


-- INSERT INTO `mesh_controller`.`sys_log` (`id`, `log_type`, `log_content`, `operate_type`, `userid`, `username`, `ip`, `method`, `request_url`, `request_param`, `request_type`, `cost_time`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`, `status`, `module`, `operate_type_name`) VALUES ('1351482891627954412', '4', '【suwen1(E0CA3B6207CF483A8C4E9429CC340C2L)】于2021-01-19 18:55在【健康智能监控-数据中心设备亚健康分析与预警-亚健康预警】进行了操作，操作类型为【程序错误】，操作结果为【失败】，操作内容为【查询告警列表】', '16', 'E0CA3B6207CF483A8C4E9429CC340C2L', 'suwen1', '192.168.204.81', 'com.gcloud.mesh.analysis.controller.SubhealthController.pageAlert()', '/subhealth/pageAlert', '[{\"endTime\":1610534341000,\"msgId\":\"ea68d952-0da8-424c-8d01-27bcb857c350\",\"pageNo\":1,\"pageSize\":10,\"startTime\":1610793541000}]', NULL, '0', 'suwen1', '2021-01-19 18:55:25', NULL, NULL, '[ERROR] [org.springframework.scheduling.support.TaskUtils$LoggingErrorHandler:] Unexpected error occurred in scheduled task\r\njava.lang.NullPointerException: null\r\n	at com.gcloud.mesh.asset.service.impl.AssetServiceImpl.pageNode(AssetServiceImpl.java:728) ~[classes!/:1.0.0]\r\n	at com.gcloud.mesh.asset.service.impl.AssetServiceImpl.pageNode(AssetServiceImpl.java:703) ~[classes!/:1.0.0]\r\n	at com.gcloud.mesh.asset.service.impl.AssetServiceImpl$$FastClassBySpringCGLIB$$ebaee86e.invoke(<generated>) ~[classes!/:1.0.0]\r\n	at org.springframework.cglib.proxy.MethodProxy.invoke(MethodProxy.java:218) ~[spring-core-5.2.4.RELEASE.jar!/:5.2.4.RELEASE]\r\n	at org.springframework.aop.framework.CglibAopProxy$DynamicAdvisedInterceptor.intercept(CglibAopProxy.java:685) ~[spring-aop-5.2.4.RELEASE.jar!/:5.2.4.RELEASE]\r\n', '0', '健康智能监控-数据中心设备亚健康分析与预警-亚健康预警', '查询');
-- INSERT INTO `mesh_controller`.`sys_log` (`id`, `log_type`, `log_content`, `operate_type`, `userid`, `username`, `ip`, `method`, `request_url`, `request_param`, `request_type`, `cost_time`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`, `status`, `module`, `operate_type_name`) VALUES ('1351482891627954516', '4', '【suwen1(E0CA3B6207CF483A8C4E9429CC340C2L)】于2021-01-19 18:57在【精确管控与供电制冷联动-基础资源粒度感知-诊断告警-诊断告警信息】进行了操作，操作类型为【程序错误】，操作结果为【失败】，操作内容为【更新策略】', '16', 'E0CA3B6207CF483A8C4E9429CC340C2L', 'suwen1', '192.168.204.81', 'com.gcloud.mesh.alert.controller.PolicyController.update()', '/policy/update', '[{\"endTime\":1610534341000,\"msgId\":\"ea68d952-0da8-424c-8d01-27bcb857c350\",\"pageNo\":1,\"pageSize\":10,\"startTime\":1610793541000}]', NULL, '0', 'suwen1', '2021-01-19 18:57:25', NULL, NULL, '[ERROR] [org.jeecg.common.exception.JeecgBootExceptionHandler:] ::阈值取值范围0~999999\r\ncom.gcloud.mesh.header.exception.BaseException: ::阈值取值范围0~999999\r\n	at com.gcloud.mesh.alert.service.PolicyService.update(PolicyService.java:290) ~[classes!/:1.0.0]\r\n	at com.gcloud.mesh.alert.controller.PolicyController.update(PolicyController.java:94) ~[classes!/:1.0.0]\r\n	at com.gcloud.mesh.alert.controller.PolicyController$$FastClassBySpringCGLIB$$13a0fb60.invoke(<generated>) ~[classes!/:1.0.0]\r\n	at org.springframework.cglib.proxy.MethodProxy.invoke(MethodProxy.java:218) ~[spring-core-5.2.4.RELEASE.jar!/:5.2.4.RELEASE]\r\n', '0', '精确管控与供电制冷联动-基础资源粒度感知-诊断告警-诊断告警信息', '查询');
-- INSERT INTO `mesh_controller`.`sys_log` (`id`, `log_type`, `log_content`, `operate_type`, `userid`, `username`, `ip`, `method`, `request_url`, `request_param`, `request_type`, `cost_time`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`, `status`, `module`, `operate_type_name`) VALUES ('1351482891627344189', '4', '【suwen1(E0CA3B6207CF483A8C4E9429CC340C2L)】于2021-01-19 18:58在【	精确管控与供电制冷联动-基础资源细粒度感知-资源管理-L2层：IT设备】进行了操作，操作类型为【程序错误】，操作结果为【失败】，操作内容为【更新策略】', '16', 'E0CA3B6207CF483A8C4E9429CC340C2L', 'suwen1', '192.168.204.81', 'com.gcloud.mesh.alert.controller.PolicyController.update()', '/policy/update', '[{\"endTime\":1610534341000,\"msgId\":\"ea68d952-0da8-424c-8d01-27bcb857c350\",\"pageNo\":1,\"pageSize\":10,\"startTime\":1610793541000}]', NULL, '0', 'suwen1', '2021-01-19 18:58:38', NULL, NULL, '[ERROR] [druid.sql.Statement:] {conn-10004, pstmt-27803} execute error. INSERT INTO sys_user (id, username, realname, password, salt, status, del_flag, create_time, third_id, third_type) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)\r\njava.sql.SQLIntegrityConstraintViolationException: Duplicate entry 40286c146127810a01612b3cefc10007 for key PRIMARY\r\n	at com.mysql.cj.jdbc.exceptions.SQLError.createSQLException(SQLError.java:117) ~[mysql-connector-java-8.0.19.jar!/:8.0.19]\r\n	at com.mysql.cj.jdbc.exceptions.SQLError.createSQLException(SQLError.java:97) ~[mysql-connector-java-8.0.19.jar!/:8.0.19]\r\n', '0', '精确管控与供电制冷联动-基础资源细粒度感知-资源管理-L2层：IT设备', '查询');


-- INSERT INTO `mesh_controller`.`sys_log` (`id`, `log_type`, `log_content`, `operate_type`, `userid`, `username`, `ip`, `module`, `method`, `request_url`, `request_param`, `request_type`, `cost_time`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`, `status`, `operate_type_name`) VALUES ('1349301589383921858', '4', '精确管控与供电制冷联动-基础资源细粒度感知-资源管理-数据中心', '18', NULL, NULL, '192.168.215.45', '精确管控与供电制冷联动-基础资源细粒度感知-资源管理-数据中心', NULL, '/profile/pageFeature', NULL, NULL, NULL, 'suwen2', '2021-01-20 18:25:42', NULL, NULL, '[Err] 1064 - You have an error in your SQL syntax; check the manual that corresponds to your MySQL server version for the right syntax to use near \'sql:SELECT a.id AS app_id, a.name AS app_name, b.feature_suggest, b.feature_choo\' at line 1', '1', '查询');
-- INSERT INTO `mesh_controller`.`sys_log` (`id`, `log_type`, `log_content`, `operate_type`, `userid`, `username`, `ip`, `module`, `method`, `request_url`, `request_param`, `request_type`, `cost_time`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`, `status`, `operate_type_name`) VALUES ('1349301628122521101', '4', '精确管控与供电制冷联动-基础资源细粒度感知-资源管理-L4层：云资源列表', '18', NULL, NULL, '192.168.215.45', '精确管控与供电制冷联动-基础资源细粒度感知-资源管理-L4层：云资源列表', NULL, '/scheduler/job/page', NULL, NULL, NULL, 'suwen2', '2021-01-20 17:25:52', NULL, NULL, '[Err] 1064 - You have an error in your SQL syntax; check the manual that corresponds to your MySQL server version for the right syntax to use near \'sql:SELECT d.* FROM dcs_scheduler_jobs d  WHERE 1=1  ORDER BY d.begin_time DESC \' at line 1', '1', '查询');
-- INSERT INTO `mesh_controller`.`sys_log` (`id`, `log_type`, `log_content`, `operate_type`, `userid`, `username`, `ip`, `module`, `method`, `request_url`, `request_param`, `request_type`, `cost_time`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`, `status`, `operate_type_name`) VALUES ('1349302058898513121', '4', '精确管控与供电制冷联动-基础资源细粒度感知-资源管理-L2层：IT设备列表', '18', NULL, NULL, '192.168.215.45', '精确管控与供电制冷联动-基础资源细粒度感知-资源管理-L2层：IT设备列表', NULL, '/scheduler/job/page', NULL, NULL, NULL, 'suwen2', '2021-01-20 20:27:34', NULL, NULL, '[Err] 1064 - You have an error in your SQL syntax; check the manual that corresponds to your MySQL server version for the right syntax to use near \'sql:select * from `asset_datacenters`\' at line 1', '1', '查询');


-- INSERT INTO `mesh_controller`.`sys_log` (`id`, `log_type`, `log_content`, `operate_type`, `userid`, `username`, `ip`, `module`, `method`, `request_url`, `request_param`, `request_type`, `cost_time`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`, `status`, `operate_type_name`) VALUES ('1349308793382072322', '4', '日志管理', '17', 'CA63E716167B4CFBBF8956395251E003', 'suwen37', '192.168.215.45', '精确管控与供电制冷联动-基础资源细粒度感知-权限管理-L2层：IT设备', 'com.gcloud.mesh.dcs.controller.AppProfileController.pageFeature()', '/profile/pageFeature', '[{\"appName\":\"vm应用3\",\"datacenterId\":\"36fa22ae-8858-4817-b1b1-ceqweqweqwe\",\"msgId\":\"31628984-36c7-4b53-86e7-b5eb1150ff91\",\"pageNo\":1,\"pageSize\":10}]', NULL, '0', 'suwen37', '2021-01-20 18:54:20', NULL, NULL, '102020:权限项应用IT设备开关不能为空', '0', '查询');
-- INSERT INTO `mesh_controller`.`sys_log` (`id`, `log_type`, `log_content`, `operate_type`, `userid`, `username`, `ip`, `module`, `method`, `request_url`, `request_param`, `request_type`, `cost_time`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`, `status`, `operate_type_name`) VALUES ('1349308793621147649', '4', '系统登录', '17', '335BBF6FFE8A4408B63CF4193F55182F', 'suwen69', '192.168.215.45', '精确管控与供电制冷联动-基础资源细粒度感知-权限管理-L1层：基础设施', 'com.gcloud.mesh.dcs.controller.SchedulerController.jobPage()', '/scheduler/job/page', '[{\"msgId\":\"7ac70c69-968c-44bf-995c-621c9b87f66f\",\"pageNo\":1,\"pageSize\":10}]', NULL, '0', 'suwen69', '2021-01-20 18:54:17', NULL, NULL, '102021:权限项基础设施资源开关不能为空', '0', '查询');
-- INSERT INTO `mesh_controller`.`sys_log` (`id`, `log_type`, `log_content`, `operate_type`, `userid`, `username`, `ip`, `module`, `method`, `request_url`, `request_param`, `request_type`, `cost_time`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`, `status`, `operate_type_name`) VALUES ('1349308793717616642', '4', '告警诊断', '17', '37309219445647C6B213B3ADA85CE025', 'suwen32', '192.168.215.45', '精确管控与供电制冷联动-基础资源细粒度感知-权限管理-L4层：应用资源', 'com.gcloud.mesh.dcs.controller.AppProfileController.pageFeature()', '/profile/pageFeature', '[{\"appName\":\"vm应用3\",\"datacenterId\":\"36fa22ae-8858-4817-b1b1-ceqweqweqwe\",\"msgId\":\"568b63df-ff3c-4d90-9950-f2e0ae5258db\",\"pageNo\":1,\"pageSize\":10}]', NULL, '0', 'suwen32', '2021-01-20 21:54:20', NULL, NULL, '102022:权限项应用资源开关不能为空', '0', '查询');

-- INSERT INTO `sys_log` VALUES ('1353345268686295041', 3, 'org.springframework.web.bind.MissingRequestHeaderException: Missing request header \'pass\' on config file', 8, 'E0CA3B6207CF483A8C4E9429CC340C2F', 'sgccyp3', '192.168.203.81', 'Isc调试', 'com.gcloud.mesh.common.controller.IscController.getIscUrl()', '/isc/get_isc_url', '', NULL, 6, 'sgccyp3', '2021-01-21 12:13:51', NULL, NULL, NULL, 1);
-- INSERT INTO `sys_log` VALUES ('1353345268686295042', 3, 'at org.jeecg.config.shiro.ShiroRealm.checkUserTokenIsEffect(ShiroRealm.java:135)', 8, 'E0CA3B6207CF483A8C4E9429CC340C2F', 'sgccyp3', '192.168.203.81', 'Isc调试', 'com.gcloud.mesh.common.controller.IscController.getIscUrl()', '/isc/get_isc_url', '', NULL, 6, 'sgccyp3', '2021-01-22 09:11:21', NULL, NULL, NULL, 1);
-- INSERT INTO `sys_log` VALUES ('1353345268686295043', 3, 'at org.apache.catalina.core.ApplicationFilterChain.internalDoFilter(ApplicationFilterChain.java:193)', 8, 'E0CA3B6207CF483A8C4E9429CC340C2F', 'sgccyp3', '192.168.203.81', 'Isc调试', 'com.gcloud.mesh.common.controller.IscController.getIscUrl()', '/isc/get_isc_url', '', NULL, 6, 'sgccyp3', '2021-01-24 18:03:11', NULL, NULL, NULL, 1);
-- INSERT INTO `sys_log` VALUES ('1353339927856709633', 3, 'update `dcs_apps` set `cloud_resource_id`=?,`create_time`=?,`deleted`=?,`from`=?,`name`=?,`type`=? where `id`=?', 24, 'E0CA3B6207CF483A8C4E9429CC340C2F', 'sgccyp3', '192.168.203.81', '精确管控与供电制冷联动-基础资源细粒度感知-资源管理-应用资源', NULL, '/app/update1', NULL, NULL, NULL, 'sgccyp3', '2021-01-21 11:52:31', NULL, NULL, NULL, 1);
-- INSERT INTO `sys_log` VALUES ('1353339927856709634', 3, 'SELECT id,version,resource_id,file_id,create_user_id,create_time,update_user_id,update_time FROM sc_edition', 24, 'E0CA3B6207CF483A8C4E9429CC340C2F', 'sgccyp3', '192.168.203.81', '精确管控与供电制冷联动-基础资源细粒度感知-资源管理-应用资源', NULL, '/app/update1', NULL, NULL, NULL, 'sgccyp3', '2021-01-21 18:12:27', NULL, NULL, NULL, 1);
-- INSERT INTO `sys_log` VALUES ('1353339927856709635', 3, 'SELECT id,name,image_url,status,type,config,content FROM sys_third_platform', 24, 'E0CA3B6207CF483A8C4E9429CC340C2F', 'sgccyp3', '192.168.203.81', '精确管控与供电制冷联动-基础资源细粒度感知-资源管理-应用资源', NULL, '/app/update1', NULL, NULL, NULL, 'sgccyp3', '2021-01-23 22:18:29', NULL, NULL, NULL, 1);
-- INSERT INTO `sys_log` VALUES ('1353339831790370817', 3, 'java.lang.IllegalArgumentException: Source must not be null        at org.springframework.util.Assert.notNull(Assert.java:198) ~[spring-core-5.2.4.RELEASE.jar!/:5.2.4.RELEASE]', 25, 'E0CA3B6207CF483A8C4E9429CC340C2F', 'sgccyp3', '192.168.203.81', '程序权限列表调试', 'com.gcloud.mesh.common.controller.IscController.permissionsCompare()', '/isc/permissions_compare', '', NULL, 961, 'sgccyp3', '2021-01-21 14:13:28', NULL, NULL, NULL, 1);
-- INSERT INTO `sys_log` VALUES ('1353339831790370818', 3, 's.d.s.w.r.operation.CachingOperationNameGenerator:40 - Generating unique operation named: deleteBatchUsingDELETE_6', 25, 'E0CA3B6207CF483A8C4E9429CC340C2F', 'sgccyp3', '192.168.203.81', '程序权限列表调试', 'com.gcloud.mesh.common.controller.IscController.permissionsCompare()', '/isc/permissions_compare', '', NULL, 961, 'sgccyp3', '2021-01-21 18:12:28', NULL, NULL, NULL, 1);
-- INSERT INTO `sys_log` VALUES ('1353339831790370819', 3, 'o.s.scheduling.quartz.LocalDataSourceJobStore:3488 - ClusterManager: Scanning for instance acfdad748fce1611500650272  in-progress jobs.', 25, 'E0CA3B6207CF483A8C4E9429CC340C2F', 'sgccyp3', '192.168.203.81', '程序权限列表调试', 'com.gcloud.mesh.common.controller.IscController.permissionsCompare()', '/isc/permissions_compare', '', NULL, 961, 'sgccyp3', '2021-01-23 18:28:14', NULL, NULL, NULL, 1);

-- ----------------------------
-- Table structure for sys_log_backup
-- ----------------------------
DROP TABLE IF EXISTS `sys_log_backup`;
CREATE TABLE `sys_log_backup`  (
  `id` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `log_type` int(2) NULL DEFAULT NULL COMMENT '日志类型（1登录日志，2操作日志）',
  `log_content` varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '日志内容',
  `operate_type` int(2) NULL DEFAULT NULL COMMENT '操作类型',
  `operate_type_name` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '操作类型名称',
  `userid` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '操作用户账号',
  `username` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '操作用户名称',
  `ip` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'IP',
  `module` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '模块名称',
  `method` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '请求java方法',
  `request_url` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '请求路径',
  `request_param` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '请求参数',
  `request_type` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '请求类型',
  `cost_time` bigint(20) NULL DEFAULT NULL COMMENT '耗时',
  `create_by` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '创建人',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '更新人',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `remark` text COMMENT '备注',
  `status` tinyint(4) DEFAULT NULL COMMENT '状态 成功：1 失败： 0',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `index_table_userid`(`userid`) USING BTREE,
  INDEX `index_logt_ype`(`log_type`) USING BTREE,
  INDEX `index_operate_type`(`operate_type`) USING BTREE,
  INDEX `index_log_type`(`log_type`) USING BTREE
) ENGINE = MyISAM CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '系统日志表' ROW_FORMAT = Dynamic;



-- ----------------------------
-- Table structure for sys_permission
-- ----------------------------
DROP TABLE IF EXISTS `sys_permission`;
CREATE TABLE `sys_permission`  (
  `id` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '主键id',
  `parent_id` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '父id',
  `name` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '菜单标题',
  `url` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '路径',
  `component` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '组件',
  `component_name` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '组件名字',
  `redirect` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '一级菜单跳转地址',
  `menu_type` int(11) NULL DEFAULT NULL COMMENT '菜单类型(0:一级菜单; 1:子菜单:2:按钮权限)',
  `perms` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '菜单权限编码',
  `perms_type` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '0' COMMENT '权限策略1显示2禁用',
  `sort_no` double(8, 2) NULL DEFAULT NULL COMMENT '菜单排序',
  `always_show` tinyint(1) NULL DEFAULT NULL COMMENT '聚合子路由: 1是0否',
  `icon` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '菜单图标',
  `is_route` tinyint(1) NULL DEFAULT 1 COMMENT '是否路由菜单: 0:不是  1:是（默认值1）',
  `is_leaf` tinyint(1) NULL DEFAULT NULL COMMENT '是否叶子节点:    1:是   0:不是',
  `keep_alive` tinyint(1) NULL DEFAULT NULL COMMENT '是否缓存该页面:    1:是   0:不是',
  `hidden` int(2) NULL DEFAULT 0 COMMENT '是否隐藏路由: 0否,1是',
  `description` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '描述',
  `create_by` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '创建人',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '更新人',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `del_flag` int(1) NULL DEFAULT 0 COMMENT '删除状态 0正常 1已删除',
  `rule_flag` int(3) NULL DEFAULT 0 COMMENT '是否添加数据权限1是0否',
  `status` varchar(2) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '按钮权限状态(0无效1有效)',
  `internal_or_external` tinyint(1) NULL DEFAULT NULL COMMENT '外链菜单打开方式 0/内部打开 1/外部打开',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `index_prem_pid`(`parent_id`) USING BTREE,
  INDEX `index_prem_is_route`(`is_route`) USING BTREE,
  INDEX `index_prem_is_leaf`(`is_leaf`) USING BTREE,
  INDEX `index_prem_sort_no`(`sort_no`) USING BTREE,
  INDEX `index_prem_del_flag`(`del_flag`) USING BTREE,
  INDEX `index_menu_type`(`menu_type`) USING BTREE,
  INDEX `index_menu_hidden`(`hidden`) USING BTREE,
  INDEX `index_menu_status`(`status`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '菜单权限表' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of sys_permission
-- ----------------------------
INSERT INTO `sys_permission` VALUES ('00a2a0ae65cdca5e93209cdbde97cbe6', '2e42e3835c2b44ec9f7bc26c146ee531', '成功', '/result/success', 'result/Success', NULL, NULL, 1, NULL, NULL, 1.00, NULL, NULL, 1, 1, NULL, NULL, NULL, NULL, '2018-12-25 20:34:38', NULL, NULL, 0, 0, NULL, NULL);
INSERT INTO `sys_permission` VALUES ('020b06793e4de2eee0007f603000c769', 'f0675b52d89100ee88472b6800754a08', 'ViserChartDemo', '/report/ViserChartDemo', 'jeecg/report/ViserChartDemo', NULL, NULL, 1, NULL, NULL, 3.00, 0, NULL, 1, 1, NULL, 0, NULL, 'admin', '2019-04-03 19:08:53', 'admin', '2019-04-03 19:08:53', 0, 0, NULL, NULL);
INSERT INTO `sys_permission` VALUES ('024f1fd1283dc632458976463d8984e1', '700b7f95165c46cc7a78bf227aa8fed3', 'Tomcat信息', '/monitor/TomcatInfo', 'modules/monitor/TomcatInfo', NULL, NULL, 1, NULL, NULL, 4.00, 0, NULL, 1, 1, NULL, 0, NULL, 'admin', '2019-04-02 09:44:29', 'admin', '2019-05-07 15:19:10', 0, 0, NULL, NULL);
INSERT INTO `sys_permission` VALUES ('043780fa095ff1b2bec4dc406d76f023', '2a470fc0c3954d9dbb61de6d80846549', '表格合计', '/jeecg/tableTotal', 'jeecg/TableTotal', NULL, NULL, 1, NULL, '1', 3.00, 0, NULL, 1, 1, 0, 0, NULL, 'admin', '2019-08-14 10:28:46', NULL, NULL, 0, 0, '1', NULL);
INSERT INTO `sys_permission` VALUES ('05b3c82ddb2536a4a5ee1a4c46b5abef', '540a2936940846cb98114ffb0d145cb8', '用户列表', '/list/user-list', 'list/UserList', NULL, NULL, 1, NULL, NULL, 3.00, NULL, NULL, 1, 1, NULL, NULL, NULL, NULL, '2018-12-25 20:34:38', NULL, NULL, 0, 0, NULL, NULL);
INSERT INTO `sys_permission` VALUES ('0620e402857b8c5b605e1ad9f4b89350', '2a470fc0c3954d9dbb61de6d80846549', '异步树列表Demo', '/jeecg/JeecgTreeTable', 'jeecg/JeecgTreeTable', NULL, NULL, 1, NULL, '0', 3.00, 0, NULL, 1, 1, NULL, 0, NULL, 'admin', '2019-05-13 17:30:30', 'admin', '2019-05-13 17:32:17', 0, 0, '1', NULL);
INSERT INTO `sys_permission` VALUES ('078f9558cdeab239aecb2bda1a8ed0d1', 'fb07ca05a3e13674dbf6d3245956da2e', '搜索列表（文章）', '/list/search/article', 'list/TableList', NULL, NULL, 1, NULL, NULL, 1.00, 0, NULL, 1, 1, NULL, 0, NULL, 'admin', '2019-02-12 14:00:34', 'admin', '2019-02-12 14:17:54', 0, 0, NULL, NULL);
INSERT INTO `sys_permission` VALUES ('08e6b9dc3c04489c8e1ff2ce6f105aa4', '', '系统监控', '/dashboard3', 'layouts/RouteView', NULL, NULL, 0, NULL, NULL, 6.00, 0, 'dashboard', 1, 0, NULL, 0, NULL, NULL, '2018-12-25 20:34:38', 'admin', '2019-03-31 22:19:58', 0, 0, NULL, NULL);
INSERT INTO `sys_permission` VALUES ('0ac2ad938963b6c6d1af25477d5b8b51', '8d4683aacaa997ab86b966b464360338', '代码生成按钮', NULL, NULL, NULL, NULL, 2, 'online:goGenerateCode', '1', 1.00, 0, NULL, 1, 1, NULL, 0, NULL, 'admin', '2019-06-11 14:20:09', NULL, NULL, 0, 0, '1', NULL);
INSERT INTO `sys_permission` VALUES ('109c78a583d4693ce2f16551b7786786', 'e41b69c57a941a3bbcce45032fe57605', 'Online报表配置', '/online/cgreport', 'modules/online/cgreport/OnlCgreportHeadList', NULL, NULL, 1, NULL, NULL, 2.00, 0, NULL, 1, 1, NULL, 0, NULL, 'admin', '2019-03-08 10:51:07', 'admin', '2019-03-30 19:04:28', 0, 0, NULL, NULL);
INSERT INTO `sys_permission` VALUES ('1166535831146504193', '2a470fc0c3954d9dbb61de6d80846549', '对象存储', '/oss/file', 'modules/oss/OSSFileList', NULL, NULL, 1, NULL, '1', 1.00, 0, '', 1, 1, 0, 0, NULL, 'admin', '2019-08-28 02:19:50', 'admin', '2019-08-28 02:20:57', 0, 0, '1', NULL);
INSERT INTO `sys_permission` VALUES ('1170592628746878978', 'd7d6e2e4e2934f2c9385a623fd98c6f3', '菜单管理2', '/isystem/newPermissionList', 'system/NewPermissionList', NULL, NULL, 1, NULL, '1', 100.00, 0, NULL, 1, 1, 0, 0, NULL, 'admin', '2019-09-08 15:00:05', 'admin', '2019-12-25 09:58:18', 0, 0, '1', 0);
INSERT INTO `sys_permission` VALUES ('1174506953255182338', 'd7d6e2e4e2934f2c9385a623fd98c6f3', '职务管理', '/isystem/position', 'system/SysPositionList', NULL, NULL, 1, NULL, '1', 2.00, 0, NULL, 1, 1, 0, 0, NULL, 'admin', '2019-09-19 10:14:13', 'admin', '2019-09-19 10:15:22', 0, 0, '1', 0);
INSERT INTO `sys_permission` VALUES ('1174590283938041857', 'd7d6e2e4e2934f2c9385a623fd98c6f3', '通讯录', '/isystem/addressList', 'system/AddressList', NULL, NULL, 1, NULL, '1', 3.00, 0, NULL, 1, 1, 0, 0, NULL, 'admin', '2019-09-19 15:45:21', NULL, NULL, 0, 0, '1', 0);
INSERT INTO `sys_permission` VALUES ('1192318987661234177', 'e41b69c57a941a3bbcce45032fe57605', '系统编码规则', '/isystem/fillRule', 'system/SysFillRuleList', NULL, NULL, 1, NULL, '1', 3.00, 0, NULL, 1, 1, 0, 0, NULL, 'admin', '2019-11-07 13:52:53', 'admin', '2020-07-10 16:55:03', 0, 0, '1', 0);
INSERT INTO `sys_permission` VALUES ('1205097455226462210', '', '大屏设计', '/big/screen', 'layouts/RouteView', NULL, NULL, 0, NULL, '1', 1.10, 0, 'area-chart', 1, 0, 0, 0, NULL, 'admin', '2019-12-12 20:09:58', 'admin', '2020-02-23 23:17:59', 0, 0, '1', 0);
INSERT INTO `sys_permission` VALUES ('1205098241075453953', '1205097455226462210', '生产销售监控', '{{ window._CONFIG[\'domianURL\'] }}/big/screen/templat/index1', 'layouts/IframePageView', NULL, NULL, 1, NULL, '1', 1.00, 0, NULL, 1, 1, 0, 0, NULL, 'admin', '2019-12-12 20:13:05', 'admin', '2019-12-12 20:15:27', 0, 0, '1', 1);
INSERT INTO `sys_permission` VALUES ('1205306106780364802', '1205097455226462210', '智慧物流监控', '{{ window._CONFIG[\'domianURL\'] }}/big/screen/templat/index2', 'layouts/IframePageView', NULL, NULL, 1, NULL, '1', 2.00, 0, NULL, 1, 1, 0, 0, NULL, 'admin', '2019-12-13 09:59:04', 'admin', '2019-12-25 09:28:03', 0, 0, '1', 0);
INSERT INTO `sys_permission` VALUES ('1209731624921534465', 'e41b69c57a941a3bbcce45032fe57605', '多数据源管理', '/isystem/dataSource', 'system/SysDataSourceList', NULL, NULL, 1, NULL, '1', 6.00, 0, NULL, 1, 1, 0, 0, NULL, 'admin', '2019-12-25 15:04:30', 'admin', '2020-02-23 22:43:37', 0, 0, '1', 0);
INSERT INTO `sys_permission` VALUES ('1224641973866467330', 'e41b69c57a941a3bbcce45032fe57605', '系统校验规则', '/isystem/checkRule', 'system/SysCheckRuleList', NULL, NULL, 1, NULL, '1', 5.00, 0, NULL, 1, 1, 0, 0, NULL, 'admin', '2019-11-07 13:52:53', 'admin', '2020-07-10 16:55:12', 0, 0, '1', 0);
INSERT INTO `sys_permission` VALUES ('1229674163694841857', 'e41b69c57a941a3bbcce45032fe57605', 'AUTO在线表单ERP', '/online/cgformErpList/:code', 'modules/online/cgform/auto/erp/OnlCgformErpList', NULL, NULL, 1, NULL, '1', 5.00, 0, NULL, 1, 1, 0, 1, NULL, 'admin', '2020-02-18 15:49:00', 'admin', '2020-02-18 15:52:25', 0, 0, '1', 0);
INSERT INTO `sys_permission` VALUES ('1232123780958064642', 'f0675b52d89100ee88472b6800754a08', 'Online报表示例', '/online/cgreport/6c7f59741c814347905a938f06ee003c', 'modules/online/cgreport/auto/OnlCgreportAutoList', NULL, NULL, 1, NULL, '1', 4.00, 0, NULL, 0, 1, 0, 0, NULL, 'admin', '2020-02-25 10:02:56', 'admin', '2020-05-02 15:37:30', 0, 0, '1', 0);
INSERT INTO `sys_permission` VALUES ('1235823781053313025', 'e41b69c57a941a3bbcce45032fe57605', 'AUTO在线内嵌子表', '/online/cgformInnerTableList/:code', 'modules/online/cgform/auto/innerTable/OnlCgformInnerTableList', NULL, NULL, 1, NULL, '1', 999.00, 0, NULL, 1, 1, 0, 1, NULL, 'admin', '2020-03-06 15:05:24', 'admin', '2020-03-06 15:07:42', 0, 0, '1', 0);
INSERT INTO `sys_permission` VALUES ('1260922988733255681', '2a470fc0c3954d9dbb61de6d80846549', 'online订单管理', '/online/cgformInnerTableList/56efb74326e74064b60933f6f8af30ea', '111111', NULL, NULL, 1, NULL, '1', 1.00, 0, NULL, 0, 1, 0, 0, NULL, 'admin', '2020-05-14 21:20:42', NULL, NULL, 0, 0, '1', 0);
INSERT INTO `sys_permission` VALUES ('1260923256208216065', '2a470fc0c3954d9dbb61de6d80846549', 'online用户报表', '/online/cgreport/1260179852088135681', '333333', NULL, NULL, 1, NULL, '1', 1.00, 0, NULL, 0, 1, 0, 0, NULL, 'admin', '2020-05-14 21:21:46', NULL, NULL, 0, 0, '1', 0);
INSERT INTO `sys_permission` VALUES ('1260928341675982849', '3f915b2769fc80648e92d04e84ca059d', '添加按钮', NULL, NULL, NULL, NULL, 2, 'user:add', '1', 1.00, 0, NULL, 1, 1, 0, 0, NULL, 'admin', '2020-05-14 21:41:58', NULL, NULL, 0, 0, '1', 0);
INSERT INTO `sys_permission` VALUES ('1260929666434318338', '3f915b2769fc80648e92d04e84ca059d', '用户编辑', NULL, NULL, NULL, NULL, 2, 'user:edit', '1', 1.00, 0, NULL, 1, 1, 0, 0, NULL, 'admin', '2020-05-14 21:47:14', NULL, NULL, 0, 0, '1', 0);
INSERT INTO `sys_permission` VALUES ('1260931366557696001', '3f915b2769fc80648e92d04e84ca059d', '表单性别可见', '', NULL, NULL, NULL, 2, 'user:sex', '1', 1.00, 0, NULL, 1, 1, 0, 0, NULL, 'admin', '2020-05-14 21:53:59', 'admin', '2020-05-14 21:57:00', 0, 0, '1', 0);
INSERT INTO `sys_permission` VALUES ('1260933542969458689', '3f915b2769fc80648e92d04e84ca059d', '禁用生日字段', NULL, NULL, NULL, NULL, 2, 'user:form:birthday', '2', 1.00, 0, NULL, 1, 1, 0, 0, NULL, 'admin', '2020-05-14 22:02:38', NULL, NULL, 0, 0, '1', 0);
INSERT INTO `sys_permission` VALUES ('1265162119913824258', 'd7d6e2e4e2934f2c9385a623fd98c6f3', 'Gateway路由(微服务版)', '/isystem/gatewayroute', 'system/SysGatewayRouteList', NULL, NULL, 1, NULL, '1', 11.00, 0, NULL, 1, 1, 0, 0, NULL, NULL, '2020-05-26 14:05:30', 'admin', '2020-07-10 15:47:19', 0, 0, '1', 0);
INSERT INTO `sys_permission` VALUES ('1280350452934307841', 'd7d6e2e4e2934f2c9385a623fd98c6f3', '租户管理', '/isys/tenant', 'system/TenantList', NULL, NULL, 1, NULL, '1', 10.00, 0, NULL, 1, 1, 0, 0, NULL, 'admin', '2020-07-07 11:58:30', 'admin', '2020-07-10 15:46:35', 0, 0, '1', 0);
INSERT INTO `sys_permission` VALUES ('1280464606292099074', '2a470fc0c3954d9dbb61de6d80846549', '图片裁剪', '/jeecg/ImagCropper', 'jeecg/ImagCropper', NULL, NULL, 1, NULL, '1', 9.00, 0, NULL, 1, 1, 0, 0, NULL, 'admin', '2020-07-07 19:32:06', NULL, NULL, 0, 0, '1', 0);
INSERT INTO `sys_permission` VALUES ('1300999295377346562', 'e62c097f6bcd46c3b7c7c98e571d862b', '精确管控占供电制冷联动', '/f01', 'layouts/RouteView', NULL, NULL, 0, 'f01', '1', 1001.00, 0, '', 0, 0, 0, 0, NULL, 'admin', '2020-09-02 11:29:38', 'sgcc_super_admin', '2020-12-05 20:45:19', 0, 0, '1', 0);
INSERT INTO `sys_permission` VALUES ('1300999947163799554', 'e62c097f6bcd46c3b7c7c98e571d862b', '离线业务多数据中心迁移', '/f02', 'layouts/RouteView', NULL, NULL, 0, NULL, '1', 1002.00, 0, '', 0, 0, 0, 0, NULL, 'admin', '2020-09-02 11:32:13', 'sgcc_super_admin', '2020-12-05 20:45:20', 0, 0, '1', 0);
INSERT INTO `sys_permission` VALUES ('1301000181361152001', 'e62c097f6bcd46c3b7c7c98e571d862b', '多数据中心调度', '/f03', 'layouts/RouteView', NULL, NULL, 0, NULL, '1', 1003.00, 0, '', 0, 0, 0, 0, NULL, 'admin', '2020-09-02 11:33:09', 'sgcc_super_admin', '2020-12-05 20:45:20', 0, 0, '1', 0);
INSERT INTO `sys_permission` VALUES ('1301000639995711490', 'e62c097f6bcd46c3b7c7c98e571d862b', '健康智能监控', '/f04', 'layouts/RouteView', NULL, NULL, 0, NULL, '1', 1004.00, 0, '', 0, 0, 0, 0, NULL, 'admin', '2020-09-02 11:34:58', 'sgcc_super_admin', '2020-12-05 20:45:20', 0, 0, '1', 0);
INSERT INTO `sys_permission` VALUES ('1301000852273631234', 'e62c097f6bcd46c3b7c7c98e571d862b', '系统管理', '/f05', 'layouts/RouteView', NULL, NULL, 0, NULL, '1', 1005.00, 0, '', 0, 0, 0, 0, NULL, 'admin', '2020-09-02 11:35:49', 'sgcc_super_admin', '2020-12-05 20:45:20', 0, 0, '1', 0);
INSERT INTO `sys_permission` VALUES ('1301001284265971713', '1301000852273631234', '日志管理', '/f0501', 'system/LogList', NULL, NULL, 0, NULL, '1', 1.00, 0, 'snippets', 0, 0, 0, 0, NULL, 'admin', '2020-09-02 11:37:32', 'sgcc_super_admin', '2020-12-05 20:45:20', 0, 0, '1', 0);
INSERT INTO `sys_permission` VALUES ('1301001622897299458', '1301000639995711490', '数据中心关键设备节点传导特性拟合', '/f0401', 'layouts/RouteView', NULL, NULL, 0, 'f0401', '1', 1.00, 0, 'snippets', 0, 0, 0, 0, NULL, 'admin', '2020-09-02 11:38:52', 'sgcc_super_admin', '2020-12-05 20:45:20', 0, 0, '1', 0);
INSERT INTO `sys_permission` VALUES ('1301001912425910273', '1301000181361152001', '动态调度机制', '/f0302', 'layouts/RouteView', NULL, NULL, 0, NULL, '1', 2.00, 0, 'snippets', 0, 0, 0, 0, NULL, 'admin', '2020-09-02 11:40:02', 'sgcc_super_admin', '2020-12-05 20:45:20', 0, 0, '1', 0);
INSERT INTO `sys_permission` VALUES ('1301034658502283266', '1300999947163799554', '离线业务负载管理', '/f0201', 'layouts/RouteView', NULL, NULL, 0, NULL, '1', 1.00, 0, 'snippets', 0, 0, 0, 0, NULL, 'admin', '2020-09-02 13:50:09', 'sgcc_super_admin', '2020-12-05 20:45:20', 0, 0, '1', 0);
INSERT INTO `sys_permission` VALUES ('1301034916112240641', '1300999295377346562', '基础资源细粒度感知', '/f0101', 'layouts/RouteView', NULL, NULL, 0, NULL, '1', 1.00, 0, 'snippets', 0, 0, 0, 0, NULL, 'admin', '2020-09-02 13:51:10', 'sgcc_super_admin', '2020-12-05 20:45:19', 0, 0, '1', 0);
INSERT INTO `sys_permission` VALUES ('1301077101432016897', '1301034916112240641', '模型总览图', '/f010101', 'f01/f0101/f010101/F010101', NULL, NULL, 0, NULL, '1', 1.00, 0, NULL, 0, 0, 0, 0, NULL, 'admin', '2020-09-02 16:38:48', 'sgcc_super_admin', '2020-12-05 20:45:19', 0, 0, '1', 0);
INSERT INTO `sys_permission` VALUES ('1301088665497243650', '1300999295377346562', '精确能耗管控', '/f0102', 'layouts/RouteView', NULL, NULL, 0, NULL, '1', 2.00, 0, 'snippets', 0, 0, 0, 0, NULL, 'admin', '2020-09-02 17:24:45', 'sgcc_super_admin', '2020-12-05 20:45:19', 0, 0, '1', 0);
INSERT INTO `sys_permission` VALUES ('1301096112848900098', '1301034916112240641', '资源管理', '/f010102', 'f01/f0101/f010102/F010102', NULL, NULL, 0, 'assetPageDatacenter', '1', 2.00, 0, NULL, 0, 0, 0, 0, NULL, 'admin', '2020-09-02 17:54:21', 'sgcc_super_admin', '2020-12-05 20:45:19', 0, 0, '1', 0);
INSERT INTO `sys_permission` VALUES ('1301427338168242177', '1301000181361152001', '数据跨层感知与智能分析', '/f0301', 'f03/f0301/F0301', NULL, NULL, 0, NULL, '1', 1.00, 0, 'snippets', 0, 0, 0, 0, NULL, 'admin', '2020-09-03 15:50:31', 'sgcc_super_admin', '2020-12-05 20:45:20', 0, 0, '1', 0);
INSERT INTO `sys_permission` VALUES ('1301427916441128962', '1301001912425910273', '调度触发策略设置', '/f030201', 'f03/f0302/f030201/F030201', NULL, NULL, 0, NULL, '1', 1.00, 0, NULL, 0, 0, 0, 0, NULL, 'admin', '2020-09-03 15:52:49', 'sgcc_super_admin', '2020-12-05 20:45:20', 0, 0, '1', 0);
INSERT INTO `sys_permission` VALUES ('1301428144502214658', '1301001912425910273', '调度过程监控', '/f030202', 'f03/f0302/f030202/F030202', NULL, NULL, 0, NULL, '1', 2.00, 0, NULL, 0, 0, 0, 0, NULL, 'admin', '2020-09-03 15:53:43', 'sgcc_super_admin', '2020-12-05 20:45:20', 0, 0, '1', 0);
INSERT INTO `sys_permission` VALUES ('1301447721621655554', '1301034916112240641', '计算管理', '/f010107', 'f01/f0101/f010107/F010107', NULL, NULL, 0, NULL, '1', 7.00, 0, NULL, 0, 0, 0, 0, NULL, 'admin', '2020-09-03 17:11:31', 'sgcc_super_admin', '2020-12-05 20:45:19', 0, 0, '1', 0);
INSERT INTO `sys_permission` VALUES ('1301802633433976834', '1300999295377346562', '供电制冷最优模型', '/f0103', 'layouts/RouteView', NULL, NULL, 0, NULL, '1', 3.00, 0, 'snippets', 0, 0, 0, 0, NULL, 'admin', '2020-09-04 16:41:48', 'sgcc_super_admin', '2020-12-05 20:45:19', 0, 0, '1', 0);
INSERT INTO `sys_permission` VALUES ('1304321289430765570', '1301001912425910273', '调度结果审计', '/f030203', 'f03/f0302/f030203/F030203', NULL, NULL, 0, NULL, '1', 3.00, 0, NULL, 0, 0, 0, 0, NULL, 'admin', '2020-09-11 15:30:03', 'sgcc_super_admin', '2020-12-05 20:45:20', 0, 0, '1', 0);
INSERT INTO `sys_permission` VALUES ('1309379979724722178', '1301034916112240641', '指标监控', '/f010103', 'f01/f0101/f010103/F010103', NULL, NULL, 0, NULL, '1', 3.00, 0, NULL, 0, 0, 0, 0, NULL, 'admin', '2020-09-25 14:31:28', 'sgcc_super_admin', '2020-12-05 20:45:19', 0, 0, '1', 0);
INSERT INTO `sys_permission` VALUES ('1310399637995130881', '1300999295377346562', '统一管理联动平台', '/f0104', 'layouts/RouteView', NULL, NULL, 0, NULL, '1', 4.00, 0, 'snippets', 0, 0, 0, 0, NULL, 'admin', '2020-09-28 10:03:14', 'sgcc_super_admin', '2020-12-05 20:45:19', 0, 0, '1', 0);
INSERT INTO `sys_permission` VALUES ('1310400302846840834', '1310399637995130881', '最优成本模型', '/f010401', 'f01/f0104/f010401/F010401', NULL, NULL, 0, NULL, '1', 1.00, 0, NULL, 0, 0, 0, 0, NULL, 'admin', '2020-09-28 10:05:52', 'sgcc_super_admin', '2020-12-05 20:45:19', 0, 0, '1', 0);
INSERT INTO `sys_permission` VALUES ('1310400531214110721', '1310399637995130881', '最优设备模型', '/f010402', 'f01/f0104/f010402/F010402', NULL, NULL, 0, NULL, '1', 2.00, 0, NULL, 0, 0, 0, 0, NULL, 'admin', '2020-09-28 10:06:47', 'sgcc_super_admin', '2020-12-05 20:45:19', 0, 0, '1', 0);
INSERT INTO `sys_permission` VALUES ('1310400808977698817', '1310399637995130881', '最优策略评估算法', '/f010403', 'f01/f0104/f010403/F010403', NULL, NULL, 0, 'modelBestPredict', '1', 3.00, 0, NULL, 0, 0, 0, 0, NULL, 'admin', '2020-09-28 10:07:53', 'sgcc_super_admin', '2020-12-05 20:45:20', 0, 0, '1', 0);
INSERT INTO `sys_permission` VALUES ('1310401085768208385', '1310399637995130881', '智能异常诊断', '/f010404', 'f01/f0104/f010404/F010404', NULL, NULL, 0, NULL, '1', 4.00, 0, NULL, 0, 0, 0, 0, NULL, 'admin', '2020-09-28 10:08:59', 'sgcc_super_admin', '2020-12-05 20:45:20', 0, 0, '1', 0);
INSERT INTO `sys_permission` VALUES ('1310401229901271042', '1310399637995130881', '业务隔离', '/f010405', 'f01/f0104/f010405/F010405', NULL, NULL, 0, NULL, '1', 5.00, 0, NULL, 0, 0, 0, 0, NULL, 'admin', '2020-09-28 10:09:33', 'sgcc_super_admin', '2020-12-05 20:45:20', 0, 0, '1', 0);
INSERT INTO `sys_permission` VALUES ('1310401446956503042', '1310399637995130881', '设备恢复', '/f010406', 'f01/f0104/f010406/F010406', NULL, NULL, 0, NULL, '1', 6.00, 0, NULL, 0, 0, 0, 0, NULL, 'admin', '2020-09-28 10:10:25', 'sgcc_super_admin', '2020-12-05 20:45:20', 0, 0, '1', 0);
INSERT INTO `sys_permission` VALUES ('1310401620621660162', '1310399637995130881', '动态电压调频', '/f010407', 'f01/f0104/f010407/F010407', NULL, NULL, 0, NULL, '1', 7.00, 0, NULL, 0, 0, 0, 0, NULL, 'admin', '2020-09-28 10:11:07', 'sgcc_super_admin', '2020-12-05 20:45:20', 0, 0, '1', 0);
INSERT INTO `sys_permission` VALUES ('1310401771595632641', '1310399637995130881', '动环节能', '/f010408', 'f01/f0104/f010408/F010408', NULL, NULL, 0, NULL, '1', 8.00, 0, NULL, 0, 0, 0, 0, NULL, 'admin', '2020-09-28 10:11:43', 'sgcc_super_admin', '2020-12-05 20:45:20', 0, 0, '1', 0);
INSERT INTO `sys_permission` VALUES ('1310401950704996353', '1310399637995130881', '资源动态分配', '/f010409', 'f01/f0104/f010409/F010409', NULL, NULL, 0, NULL, '1', 9.00, 0, NULL, 0, 0, 0, 0, NULL, 'admin', '2020-09-28 10:12:25', 'sgcc_super_admin', '2020-12-05 20:45:20', 0, 0, '1', 0);
INSERT INTO `sys_permission` VALUES ('1310402513266020353', '1301802633433976834', '供电制冷设备流量监管', '/f010301', 'f01/f0103/f010301/F010301', NULL, NULL, 0, 'f010301', '1', 1.00, 0, NULL, 0, 0, 0, 0, NULL, 'admin', '2020-09-28 10:14:39', 'sgcc_super_admin', '2020-12-05 20:45:19', 0, 0, '1', 0);
INSERT INTO `sys_permission` VALUES ('1310403041836404738', '1301802633433976834', '供电制冷设备资源监控', '/f010302', 'f01/f0103/f010302/F010302', NULL, NULL, 0, NULL, '1', 2.00, 0, '', 0, 0, 0, 0, NULL, 'admin', '2020-09-28 10:16:45', 'sgcc_super_admin', '2020-12-05 20:45:19', 0, 0, '1', 0);
INSERT INTO `sys_permission` VALUES ('1310403230580084737', '1301802633433976834', '设备指标管理', '/f010303', 'f01/f0103/f010303/F010303', NULL, NULL, 0, 'modelGetModelFactors', '1', 3.00, 0, NULL, 0, 0, 0, 0, NULL, 'admin', '2020-09-28 10:17:30', 'sgcc_super_admin', '2020-12-05 20:45:19', 0, 0, '1', 0);
INSERT INTO `sys_permission` VALUES ('1310403473635807234', '1301802633433976834', '设备告警管理', '/f010304', 'f01/f0103/f010304/F010304', NULL, NULL, 0, NULL, '1', 4.00, 0, NULL, 0, 0, 0, 0, NULL, 'admin', '2020-09-28 10:18:28', 'sgcc_super_admin', '2020-12-05 20:45:19', 0, 0, '1', 0);
INSERT INTO `sys_permission` VALUES ('1310403837600731138', '1301802633433976834', '设备最优成本模型总览', '/f010305', 'f01/f0103/f010305/F010305', NULL, NULL, 0, NULL, '1', 5.00, 0, NULL, 0, 0, 0, 0, NULL, 'admin', '2020-09-28 10:19:55', 'sgcc_super_admin', '2020-12-05 20:45:19', 0, 0, '1', 0);
INSERT INTO `sys_permission` VALUES ('1310404050755260417', '1301088665497243650', '冗余数据清洗', '/f010201', 'f01/f0102/f010201/F010201', NULL, NULL, 0, NULL, '1', 1.00, 0, NULL, 0, 0, 0, 0, NULL, 'admin', '2020-09-28 10:20:46', 'sgcc_super_admin', '2020-12-05 20:45:19', 0, 0, '1', 0);
INSERT INTO `sys_permission` VALUES ('1310404258897596417', '1301088665497243650', '数据归类', '/f010202', 'f01/f0102/f010202/F010202', NULL, NULL, 0, NULL, '1', 1.00, 0, NULL, 0, 0, 0, 0, NULL, 'admin', '2020-09-28 10:21:36', 'sgcc_super_admin', '2020-12-05 20:45:19', 0, 0, '1', 0);
INSERT INTO `sys_permission` VALUES ('1310404620442406913', '1301088665497243650', '数据安全', '/f010203', 'f01/f0102/f010203/F010203', NULL, NULL, 0, 'dataSecurityDetail', '1', 3.00, 0, NULL, 0, 0, 0, 0, NULL, 'admin', '2020-09-28 10:23:02', 'sgcc_super_admin', '2020-12-05 20:45:19', 0, 0, '1', 0);
INSERT INTO `sys_permission` VALUES ('1310404991655088130', '1301088665497243650', '计算节点控制', '/f010204', 'f01/f0102/f010204/F010204', NULL, NULL, 0, NULL, '1', 4.00, 0, NULL, 0, 0, 0, 0, NULL, 'admin', '2020-09-28 10:24:30', 'sgcc_super_admin', '2020-12-05 20:45:19', 0, 0, '1', 0);
INSERT INTO `sys_permission` VALUES ('1310405261181063170', '1301088665497243650', '服务器能耗转换', '/f010205', 'f01/f0102/f010205/F010205', NULL, NULL, 0, 'assetPageNode', '1', 5.00, 0, NULL, 0, 0, 0, 0, NULL, 'admin', '2020-09-28 10:25:35', 'sgcc_super_admin', '2020-12-05 20:45:19', 0, 0, '1', 0);
INSERT INTO `sys_permission` VALUES ('1310405668003385346', '1301088665497243650', '资源动态分配', '/f010206', 'f01/f0102/f010206/F010206', NULL, NULL, 0, NULL, '1', 6.00, 0, NULL, 0, 0, 0, 0, NULL, 'admin', '2020-09-28 10:27:12', 'sgcc_super_admin', '2020-12-05 20:45:19', 0, 0, '1', 0);
INSERT INTO `sys_permission` VALUES ('1310406050767179777', '1301088665497243650', '动态存储', '/f010207', 'f01/f0102/f010207/F010207', NULL, NULL, 0, NULL, '1', 7.00, 0, NULL, 0, 0, 0, 0, NULL, 'admin', '2020-09-28 10:28:43', 'sgcc_super_admin', '2020-12-05 20:45:19', 0, 0, '1', 0);
INSERT INTO `sys_permission` VALUES ('1310406264022372354', '1301088665497243650', '业务计算', '/f010208', 'f01/f0102/f010208/F010208', NULL, NULL, 0, 'profilePageAppProfile', '1', 8.00, 0, NULL, 0, 0, 0, 0, NULL, 'admin', '2020-09-28 10:29:34', 'sgcc_super_admin', '2020-12-05 20:45:19', 0, 0, '1', 0);
INSERT INTO `sys_permission` VALUES ('1310406614360002562', '1301088665497243650', '功耗资源启动推荐方案', '/f010209', 'f01/f0102/f010209/F010209', NULL, NULL, 0, NULL, '1', 9.00, 0, NULL, 0, 0, 0, 0, NULL, 'admin', '2020-09-28 10:30:57', 'sgcc_super_admin', '2020-12-05 20:45:19', 0, 0, '1', 0);
INSERT INTO `sys_permission` VALUES ('1310406951590432769', '1301034916112240641', '诊断告警', '/f010104', 'f01/f0101/f010104/F010104', NULL, NULL, 0, NULL, '1', 4.00, 0, NULL, 0, 0, 0, 0, NULL, 'admin', '2020-09-28 10:32:18', 'sgcc_super_admin', '2020-12-05 20:45:19', 0, 0, '1', 0);
INSERT INTO `sys_permission` VALUES ('1310407190049198081', '1301034916112240641', '权限管理', '/f010105', 'f01/f0101/f010105/F010105', NULL, NULL, 0, NULL, '1', 5.00, 0, NULL, 0, 0, 0, 0, NULL, 'admin', '2020-09-28 10:33:14', 'sgcc_super_admin', '2020-12-05 20:45:19', 0, 0, '1', 0);
INSERT INTO `sys_permission` VALUES ('1310407413609795585', '1301034916112240641', '服务器能耗转换', '/f010106', 'f01/f0101/f010106/F010106', NULL, NULL, 0, NULL, '1', 6.00, 0, NULL, 0, 0, 0, 0, NULL, 'admin', '2020-09-28 10:34:08', 'sgcc_super_admin', '2020-12-05 20:45:19', 0, 0, '1', 0);
INSERT INTO `sys_permission` VALUES ('1310407620586115073', '1301034916112240641', '网络管理', '/f010108', 'f01/f0101/f010108/F010108', NULL, NULL, 0, NULL, '1', 8.00, 0, NULL, 0, 0, 0, 0, NULL, 'admin', '2020-09-28 10:34:57', 'sgcc_super_admin', '2020-12-05 20:45:19', 0, 0, '1', 0);
INSERT INTO `sys_permission` VALUES ('1310409534656745473', '1300999947163799554', '离线业务异步迁移代价', '/f0202', 'layouts/RouteView', NULL, NULL, 0, NULL, '1', 2.00, 0, 'snippets', 0, 0, 0, 0, NULL, 'admin', '2020-09-28 10:42:33', 'sgcc_super_admin', '2020-12-05 20:45:20', 0, 0, '1', 0);
INSERT INTO `sys_permission` VALUES ('1310409822184673282', '1300999947163799554', '多数据中心弹性迁移策略', '/f0203', 'layouts/RouteView', NULL, NULL, 0, NULL, '1', 3.00, 0, 'snippets', 0, 0, 0, 0, NULL, 'admin', '2020-09-28 10:43:42', 'sgcc_super_admin', '2020-12-05 20:45:20', 0, 0, '1', 0);
INSERT INTO `sys_permission` VALUES ('1310410017085591553', '1300999947163799554', '异步迁移模型工具', '/f0204', 'f02/f0204/F0204', NULL, NULL, 0, NULL, '1', 4.00, 0, 'snippets', 0, 0, 0, 0, NULL, 'admin', '2020-09-28 10:44:28', 'sgcc_super_admin', '2020-12-05 20:45:20', 0, 0, '1', 0);
INSERT INTO `sys_permission` VALUES ('1310410227111170050', '1310409822184673282', '迁移策略', '/f020301', 'f02/f0203/f020301/F020301', NULL, NULL, 0, NULL, '1', 1.00, 0, NULL, 0, 0, 0, 0, NULL, 'admin', '2020-09-28 10:45:19', 'sgcc_super_admin', '2020-12-05 20:45:20', 0, 0, '1', 0);
INSERT INTO `sys_permission` VALUES ('1310410534536876033', '1310409822184673282', '迁移策略执行规则', '/f020302', 'f02/f0203/f020302/F020302', NULL, NULL, 0, NULL, '1', 2.00, 0, NULL, 0, 0, 0, 0, NULL, 'admin', '2020-09-28 10:46:32', 'sgcc_super_admin', '2020-12-05 20:45:20', 0, 0, '1', 0);
INSERT INTO `sys_permission` VALUES ('1310410737562161153', '1310409534656745473', '代价模型设置', '/f020201', 'f02/f0202/f020201/F020201', NULL, NULL, 0, NULL, '1', 1.00, 0, NULL, 0, 0, 0, 0, NULL, 'admin', '2020-09-28 10:47:20', 'sgcc_super_admin', '2020-12-05 20:45:20', 0, 0, '1', 0);
INSERT INTO `sys_permission` VALUES ('1310412463300153345', '1310409534656745473', '代价展示', '/f020202', 'f02/f0202/f020202/F020202', NULL, NULL, 0, NULL, '1', 2.00, 0, '', 0, 0, 0, 0, NULL, 'admin', '2020-09-28 10:54:12', 'sgcc_super_admin', '2020-12-05 20:45:20', 0, 0, '1', 0);
INSERT INTO `sys_permission` VALUES ('1310413060296413185', '1301034658502283266', '业务应用画像业务特点管理', '/f020101', 'f02/f0201/f020101/F020101', NULL, NULL, 0, 'profilePageFeature', '1', 1.00, 0, NULL, 0, 0, 0, 0, NULL, 'admin', '2020-09-28 10:56:34', 'sgcc_super_admin', '2020-12-05 20:45:20', 0, 0, '1', 0);
INSERT INTO `sys_permission` VALUES ('1310413263179091969', '1301034658502283266', '业务应用画像应用场景管理', '/f020102', 'f02/f0201/f020102/F020102', NULL, NULL, 0, 'profilePageScene', '1', 2.00, 0, NULL, 0, 0, 0, 0, NULL, 'admin', '2020-09-28 10:57:22', 'sgcc_super_admin', '2020-12-05 20:45:20', 0, 0, '1', 0);
INSERT INTO `sys_permission` VALUES ('1310415795251056642', '1301001912425910273', '调度结果偏差分析', '/f030205', 'f03/f0302/f030205/F030205', NULL, NULL, 0, NULL, '1', 5.00, 0, NULL, 0, 0, 0, 0, NULL, 'admin', '2020-09-28 11:07:26', 'sgcc_super_admin', '2020-12-05 20:45:20', 0, 0, '1', 0);
INSERT INTO `sys_permission` VALUES ('1310416188978761730', '1301001912425910273', '调度效果预测', '/f030204', 'f03/f0302/f030204/F030204', NULL, NULL, 0, NULL, '1', 4.00, 0, NULL, 0, 0, 0, 0, NULL, 'admin', '2020-09-28 11:09:00', 'sgcc_super_admin', '2020-12-05 20:45:20', 0, 0, '1', 0);
INSERT INTO `sys_permission` VALUES ('1310416488150077442', '1301000181361152001', '调度模型管理', '/f0303', 'layouts/RouteView', NULL, NULL, 0, NULL, '1', 3.00, 0, 'snippets', 0, 0, 0, 0, NULL, 'admin', '2020-09-28 11:10:11', 'sgcc_super_admin', '2020-12-05 20:45:20', 0, 0, '1', 0);
INSERT INTO `sys_permission` VALUES ('1310418138898108418', '1301001622897299458', '设备选择', '/f040101', 'f04/f0401/f040101/F040101', NULL, NULL, 0, 'f040101', '1', 1.00, 0, NULL, 0, 0, 0, 0, NULL, 'admin', '2020-09-28 11:16:45', 'sgcc_super_admin', '2020-12-05 20:45:20', 0, 0, '1', 0);
INSERT INTO `sys_permission` VALUES ('1310422936447881217', '1301001622897299458', '关键设备节点传导特效拟合展示', '/f040102', 'f04/f0401/f040102/F040102', NULL, NULL, 0, NULL, '1', 2.00, 0, NULL, 0, 0, 0, 0, NULL, 'admin', '2020-09-28 11:35:49', 'sgcc_super_admin', '2020-12-05 20:45:20', 0, 0, '1', 0);
INSERT INTO `sys_permission` VALUES ('1310432505559519233', '1301000639995711490', '基于能耗传导的数据中心运行仿真', '/f0402', 'f04/f0402/F0402', NULL, NULL, 0, NULL, '1', 2.00, 0, 'snippets', 0, 0, 0, 0, NULL, 'admin', '2020-09-28 12:13:50', 'sgcc_super_admin', '2020-12-05 20:45:20', 0, 0, '1', 0);
INSERT INTO `sys_permission` VALUES ('1310432897492062210', '1301000639995711490', '设备运行情况分析模型的最优策略建议', '/f0403', 'f04/f0403/F0403', NULL, NULL, 0, 'f0403', '1', 3.00, 0, 'snippets', 0, 0, 0, 0, NULL, 'admin', '2020-09-28 12:15:24', 'sgcc_super_admin', '2020-12-05 20:45:20', 0, 0, '1', 0);
INSERT INTO `sys_permission` VALUES ('1310433274404802561', '1301000639995711490', '数据中心设备亚健康分析与预警', '/f0404', 'f04/f0404/F0404', NULL, NULL, 0, NULL, '1', 4.00, 0, 'snippets', 0, 0, 0, 0, NULL, 'admin', '2020-09-28 12:16:53', 'sgcc_super_admin', '2020-12-05 20:45:20', 0, 0, '1', 0);
INSERT INTO `sys_permission` VALUES ('1310433915395117057', '1301000852273631234', '统计报表', '/f0502', 'f05/f0502/F0502', NULL, NULL, 0, NULL, '1', 2.00, 0, 'snippets', 0, 0, 0, 0, NULL, 'admin', '2020-09-28 12:19:26', 'sgcc_super_admin', '2020-12-04 10:24:58', 0, 0, '1', 0);
INSERT INTO `sys_permission` VALUES ('13212d3416eb690c2e1d5033166ff47a', '2e42e3835c2b44ec9f7bc26c146ee531', '失败', '/result/fail', 'result/Error', NULL, NULL, 1, NULL, NULL, 2.00, NULL, NULL, 1, 1, NULL, NULL, NULL, NULL, '2018-12-25 20:34:38', NULL, NULL, 0, 0, NULL, NULL);
INSERT INTO `sys_permission` VALUES ('1331870906774982658', '1310416488150077442', '多数据中心调度对象粒度设置', '/f030301', 'f03/f0303/f030301/F030301', NULL, NULL, 0, NULL, '1', 1.00, 0, NULL, 0, 0, 0, 0, NULL, 'admin', '2020-11-26 16:02:23', 'sgcc_super_admin', '2020-12-05 20:45:20', 0, 0, '1', 0);
INSERT INTO `sys_permission` VALUES ('1331871850078146562', '1310416488150077442', '调度策略设置', '/f030302', 'f03/f0303/f030302/F030302', NULL, NULL, 0, NULL, '1', 2.00, 0, NULL, 0, 0, 0, 0, NULL, 'admin', '2020-11-26 16:06:08', 'sgcc_super_admin', '2020-12-05 20:45:20', 0, 0, '1', 0);
INSERT INTO `sys_permission` VALUES ('1331872709646225410', '1310416488150077442', '调度模型优化设置', '/f030303', 'f03/f0303/f030303/F030303', NULL, NULL, 0, 'modelGetModelFactors', '1', 3.00, 0, NULL, 0, 0, 0, 0, NULL, 'admin', '2020-11-26 16:09:33', 'sgcc_super_admin', '2020-12-05 20:45:20', 0, 0, '1', 0);
INSERT INTO `sys_permission` VALUES ('1367a93f2c410b169faa7abcbad2f77c', '6e73eb3c26099c191bf03852ee1310a1', '基本设置', '/account/settings/BaseSetting', 'account/settings/BaseSetting', 'account-settings-base', NULL, 1, 'BaseSettings', NULL, NULL, 0, NULL, 1, 1, NULL, 1, NULL, NULL, '2018-12-26 18:58:35', 'admin', '2019-03-20 12:57:31', 0, 0, NULL, NULL);
INSERT INTO `sys_permission` VALUES ('190c2b43bec6a5f7a4194a85db67d96a', 'd7d6e2e4e2934f2c9385a623fd98c6f3', '角色管理', '/isystem/roleUserList', 'system/RoleUserList', NULL, NULL, 1, NULL, NULL, 1.20, 0, NULL, 1, 1, 0, 0, NULL, 'admin', '2019-04-17 15:13:56', 'admin', '2019-12-25 09:36:31', 0, 0, NULL, 0);
INSERT INTO `sys_permission` VALUES ('1a0811914300741f4e11838ff37a1d3a', '3f915b2769fc80648e92d04e84ca059d', '手机号禁用', NULL, NULL, NULL, NULL, 2, 'user:form:phone', '2', 1.00, 0, NULL, 0, 1, NULL, 0, NULL, 'admin', '2019-05-11 17:19:30', 'admin', '2019-05-11 18:00:22', 0, 0, '1', NULL);
INSERT INTO `sys_permission` VALUES ('200006f0edf145a2b50eacca07585451', 'fb07ca05a3e13674dbf6d3245956da2e', '搜索列表（应用）', '/list/search/application', 'list/TableList', NULL, NULL, 1, NULL, NULL, 1.00, 0, NULL, 1, 1, NULL, 0, NULL, 'admin', '2019-02-12 14:02:51', 'admin', '2019-02-12 14:14:01', 0, 0, NULL, NULL);
INSERT INTO `sys_permission` VALUES ('22d6a3d39a59dd7ea9a30acfa6bfb0a5', 'e41b69c57a941a3bbcce45032fe57605', 'AUTO动态表单', '/online/df/:table/:id', 'modules/online/cgform/auto/OnlineDynamicForm', NULL, NULL, 1, NULL, NULL, 9.00, 0, NULL, 0, 1, NULL, 1, NULL, 'admin', '2019-04-22 15:15:43', 'admin', '2019-04-30 18:18:26', 0, 0, NULL, NULL);
INSERT INTO `sys_permission` VALUES ('265de841c58907954b8877fb85212622', '2a470fc0c3954d9dbb61de6d80846549', '图片拖拽排序', '/jeecg/imgDragSort', 'jeecg/ImgDragSort', NULL, NULL, 1, NULL, NULL, 4.00, 0, NULL, 1, 1, NULL, 0, NULL, 'admin', '2019-04-25 10:43:08', 'admin', '2019-04-25 10:46:26', 0, 0, NULL, NULL);
INSERT INTO `sys_permission` VALUES ('277bfabef7d76e89b33062b16a9a5020', 'e3c13679c73a4f829bcff2aba8fd68b1', '基础表单', '/form/base-form', 'form/BasicForm', NULL, NULL, 1, NULL, NULL, 1.00, 0, NULL, 1, 0, NULL, 0, NULL, NULL, '2018-12-25 20:34:38', 'admin', '2019-02-26 17:02:08', 0, 0, NULL, NULL);
INSERT INTO `sys_permission` VALUES ('2a470fc0c3954d9dbb61de6d80846549', '', '常见案例', '/jeecg', 'layouts/RouteView', NULL, NULL, 0, NULL, NULL, 7.00, 0, 'qrcode', 1, 0, NULL, 0, NULL, NULL, '2018-12-25 20:34:38', 'admin', '2019-04-02 11:46:42', 0, 0, NULL, NULL);
INSERT INTO `sys_permission` VALUES ('2aeddae571695cd6380f6d6d334d6e7d', 'f0675b52d89100ee88472b6800754a08', '布局统计报表', '/report/ArchivesStatisticst', 'jeecg/report/ArchivesStatisticst', NULL, NULL, 1, NULL, NULL, 1.00, 0, NULL, 1, 1, NULL, 0, NULL, 'admin', '2019-04-03 18:32:48', NULL, NULL, 0, 0, NULL, NULL);
INSERT INTO `sys_permission` VALUES ('2dbbafa22cda07fa5d169d741b81fe12', '08e6b9dc3c04489c8e1ff2ce6f105aa4', '在线文档', '{{ window._CONFIG[\'domianURL\'] }}/doc.html', 'layouts/IframePageView', NULL, NULL, 1, NULL, NULL, 3.00, 0, NULL, 1, 1, NULL, 0, NULL, 'admin', '2019-01-30 10:00:01', 'admin', '2019-03-23 19:44:43', 0, 0, NULL, NULL);
INSERT INTO `sys_permission` VALUES ('2e42e3835c2b44ec9f7bc26c146ee531', '', '结果页', '/result', 'layouts/PageView', NULL, NULL, 0, NULL, NULL, 8.00, 0, 'check-circle-o', 1, 0, NULL, 0, NULL, NULL, '2018-12-25 20:34:38', 'admin', '2019-04-02 11:46:56', 0, 0, NULL, NULL);
INSERT INTO `sys_permission` VALUES ('339329ed54cf255e1f9392e84f136901', '2a470fc0c3954d9dbb61de6d80846549', 'helloworld', '/jeecg/helloworld', 'jeecg/helloworld', NULL, NULL, 1, NULL, NULL, 4.00, 0, NULL, 1, 1, NULL, 0, NULL, NULL, '2018-12-25 20:34:38', 'admin', '2019-02-15 16:24:56', 0, 0, NULL, NULL);
INSERT INTO `sys_permission` VALUES ('3f915b2769fc80648e92d04e84ca059d', 'd7d6e2e4e2934f2c9385a623fd98c6f3', '用户管理', '/isystem/user', 'system/UserList', NULL, NULL, 1, NULL, NULL, 1.10, 0, NULL, 1, 0, 0, 0, NULL, NULL, '2018-12-25 20:34:38', 'admin', '2019-12-25 09:36:24', 0, 0, NULL, 0);
INSERT INTO `sys_permission` VALUES ('3fac0d3c9cd40fa53ab70d4c583821f8', '2a470fc0c3954d9dbb61de6d80846549', '分屏', '/jeecg/splitPanel', 'jeecg/SplitPanel', NULL, NULL, 1, NULL, NULL, 6.00, 0, NULL, 1, 1, NULL, 0, NULL, 'admin', '2019-04-25 16:27:06', NULL, NULL, 0, 0, NULL, NULL);
INSERT INTO `sys_permission` VALUES ('4148ec82b6acd69f470bea75fe41c357', '2a470fc0c3954d9dbb61de6d80846549', '单表模型示例', '/jeecg/jeecgDemoList', 'jeecg/JeecgDemoList', 'DemoList', NULL, 1, NULL, NULL, 1.00, 0, NULL, 1, 1, 0, 0, NULL, NULL, '2018-12-28 15:57:30', 'jeecg', '2020-05-14 22:09:34', 0, 1, NULL, 0);
INSERT INTO `sys_permission` VALUES ('418964ba087b90a84897b62474496b93', '540a2936940846cb98114ffb0d145cb8', '查询表格', '/list/query-list', 'list/TableList', NULL, NULL, 1, NULL, NULL, 1.00, NULL, NULL, 1, 1, NULL, NULL, NULL, NULL, '2018-12-25 20:34:38', NULL, NULL, 0, 0, NULL, NULL);
INSERT INTO `sys_permission` VALUES ('4356a1a67b564f0988a484f5531fd4d9', '2a470fc0c3954d9dbb61de6d80846549', '内嵌Table', '/jeecg/TableExpandeSub', 'jeecg/TableExpandeSub', NULL, NULL, 1, NULL, NULL, 1.00, 0, NULL, 1, 1, NULL, 0, NULL, 'admin', '2019-04-04 22:48:13', NULL, NULL, 0, 0, NULL, NULL);
INSERT INTO `sys_permission` VALUES ('45c966826eeff4c99b8f8ebfe74511fc', 'd7d6e2e4e2934f2c9385a623fd98c6f3', '部门管理', '/isystem/depart', 'system/DepartList', NULL, NULL, 1, NULL, NULL, 1.40, 0, NULL, 1, 1, 0, 0, NULL, 'admin', '2019-01-29 18:47:40', 'admin', '2019-12-25 09:36:47', 0, 0, NULL, 0);
INSERT INTO `sys_permission` VALUES ('4875ebe289344e14844d8e3ea1edd73f', '', '详情页', '/profile', 'layouts/RouteView', NULL, NULL, 0, NULL, NULL, 8.00, 0, 'profile', 1, 0, NULL, 0, NULL, NULL, '2018-12-25 20:34:38', 'admin', '2019-04-02 11:46:48', 0, 0, NULL, NULL);
INSERT INTO `sys_permission` VALUES ('4f66409ef3bbd69c1d80469d6e2a885e', '6e73eb3c26099c191bf03852ee1310a1', '账户绑定', '/account/settings/binding', 'account/settings/Binding', NULL, NULL, 1, 'BindingSettings', NULL, NULL, NULL, NULL, 1, 1, NULL, NULL, NULL, NULL, '2018-12-26 19:01:20', NULL, NULL, 0, 0, NULL, NULL);
INSERT INTO `sys_permission` VALUES ('4f84f9400e5e92c95f05b554724c2b58', '540a2936940846cb98114ffb0d145cb8', '角色列表', '/list/role-list', 'list/RoleList', NULL, NULL, 1, NULL, NULL, 4.00, NULL, NULL, 1, 1, NULL, NULL, NULL, NULL, '2018-12-25 20:34:38', NULL, NULL, 0, 0, NULL, NULL);
INSERT INTO `sys_permission` VALUES ('53a9230444d33de28aa11cc108fb1dba', '5c8042bd6c601270b2bbd9b20bccc68b', '我的消息', '/isps/userAnnouncement', 'system/UserAnnouncementList', NULL, NULL, 1, NULL, NULL, 3.00, 0, NULL, 1, 1, 0, 0, NULL, 'admin', '2019-04-19 10:16:00', 'admin', '2019-12-25 09:54:34', 0, 0, NULL, 0);
INSERT INTO `sys_permission` VALUES ('54097c6a3cf50fad0793a34beff1efdf', 'e41b69c57a941a3bbcce45032fe57605', 'AUTO在线表单', '/online/cgformList/:code', 'modules/online/cgform/auto/OnlCgformAutoList', NULL, NULL, 1, NULL, NULL, 9.00, 0, NULL, 1, 1, NULL, 1, NULL, 'admin', '2019-03-19 16:03:06', 'admin', '2019-04-30 18:19:03', 0, 0, NULL, NULL);
INSERT INTO `sys_permission` VALUES ('540a2936940846cb98114ffb0d145cb8', '', '列表页', '/list', 'layouts/PageView', NULL, '/list/query-list', 0, NULL, NULL, 9.00, 0, 'table', 1, 0, NULL, 0, NULL, NULL, '2018-12-25 20:34:38', 'admin', '2019-03-31 22:20:20', 0, 0, NULL, NULL);
INSERT INTO `sys_permission` VALUES ('54dd5457a3190740005c1bfec55b1c34', 'd7d6e2e4e2934f2c9385a623fd98c6f3', '菜单管理', '/isystem/permission', 'system/PermissionList', NULL, NULL, 1, NULL, NULL, 1.30, 0, NULL, 1, 1, 0, 0, NULL, NULL, '2018-12-25 20:34:38', 'admin', '2019-12-25 09:36:39', 0, 0, NULL, 0);
INSERT INTO `sys_permission` VALUES ('58857ff846e61794c69208e9d3a85466', '08e6b9dc3c04489c8e1ff2ce6f105aa4', '日志管理', '/isystem/log', 'system/LogList', NULL, NULL, 1, NULL, NULL, 1.00, 0, '', 1, 1, NULL, 0, NULL, NULL, '2018-12-26 10:11:18', 'admin', '2019-04-02 11:38:17', 0, 0, NULL, NULL);
INSERT INTO `sys_permission` VALUES ('58b9204feaf07e47284ddb36cd2d8468', '2a470fc0c3954d9dbb61de6d80846549', '图片翻页', '/jeecg/imgTurnPage', 'jeecg/ImgTurnPage', NULL, NULL, 1, NULL, NULL, 4.00, 0, NULL, 1, 1, NULL, 0, NULL, 'admin', '2019-04-25 11:36:42', NULL, NULL, 0, 0, NULL, NULL);
INSERT INTO `sys_permission` VALUES ('5c2f42277948043026b7a14692456828', 'd7d6e2e4e2934f2c9385a623fd98c6f3', '我的部门', '/isystem/departUserList', 'system/DepartUserList', NULL, NULL, 1, NULL, NULL, 2.00, 0, NULL, 1, 1, 0, 0, NULL, 'admin', '2019-04-17 15:12:24', 'admin', '2019-12-25 09:35:26', 0, 0, NULL, 0);
INSERT INTO `sys_permission` VALUES ('5c8042bd6c601270b2bbd9b20bccc68b', '', '消息中心', '/message', 'layouts/RouteView', NULL, NULL, 0, NULL, NULL, 6.00, 0, 'message', 1, 0, NULL, 0, NULL, 'admin', '2019-04-09 11:05:04', 'admin', '2019-04-11 19:47:54', 0, 0, NULL, NULL);
INSERT INTO `sys_permission` VALUES ('6531cf3421b1265aeeeabaab5e176e6d', 'e3c13679c73a4f829bcff2aba8fd68b1', '分步表单', '/form/step-form', 'form/stepForm/StepForm', NULL, NULL, 1, NULL, NULL, 2.00, NULL, NULL, 1, 1, NULL, NULL, NULL, NULL, '2018-12-25 20:34:38', NULL, NULL, 0, 0, NULL, NULL);
INSERT INTO `sys_permission` VALUES ('655563cd64b75dcf52ef7bcdd4836953', '2a470fc0c3954d9dbb61de6d80846549', '图片预览', '/jeecg/ImagPreview', 'jeecg/ImagPreview', NULL, NULL, 1, NULL, NULL, 1.00, 0, NULL, 1, 1, NULL, 0, NULL, 'admin', '2019-04-17 11:18:45', NULL, NULL, 0, 0, NULL, NULL);
INSERT INTO `sys_permission` VALUES ('65a8f489f25a345836b7f44b1181197a', 'c65321e57b7949b7a975313220de0422', '403', '/exception/403', 'exception/403', NULL, NULL, 1, NULL, NULL, 1.00, NULL, NULL, 1, 1, NULL, NULL, NULL, NULL, '2018-12-25 20:34:38', NULL, NULL, 0, 0, NULL, NULL);
INSERT INTO `sys_permission` VALUES ('6ad53fd1b220989a8b71ff482d683a5a', '2a470fc0c3954d9dbb61de6d80846549', '一对多Tab示例', '/jeecg/tablist/JeecgOrderDMainList', 'jeecg/tablist/JeecgOrderDMainList', NULL, NULL, 1, NULL, NULL, 2.00, 0, NULL, 1, 1, NULL, 0, NULL, 'admin', '2019-02-20 14:45:09', 'admin', '2019-02-21 16:26:21', 0, 0, NULL, NULL);
INSERT INTO `sys_permission` VALUES ('6e73eb3c26099c191bf03852ee1310a1', '717f6bee46f44a3897eca9abd6e2ec44', '个人设置', '/account/settings/Index', 'account/settings/Index', NULL, NULL, 1, NULL, NULL, 2.00, 1, NULL, 1, 0, NULL, 0, NULL, NULL, '2018-12-25 20:34:38', 'admin', '2019-04-19 09:41:05', 0, 0, NULL, NULL);
INSERT INTO `sys_permission` VALUES ('700b7f95165c46cc7a78bf227aa8fed3', '08e6b9dc3c04489c8e1ff2ce6f105aa4', '性能监控', '/monitor', 'layouts/RouteView', NULL, NULL, 1, NULL, NULL, 0.00, 0, NULL, 1, 0, NULL, 0, NULL, 'admin', '2019-04-02 11:34:34', 'admin', '2019-05-05 17:49:47', 0, 0, NULL, NULL);
INSERT INTO `sys_permission` VALUES ('717f6bee46f44a3897eca9abd6e2ec44', '', '个人页', '/account', 'layouts/RouteView', NULL, NULL, 0, NULL, NULL, 9.00, 0, 'user', 1, 0, 0, 1, NULL, NULL, '2018-12-25 20:34:38', 'admin', '2020-02-23 22:41:37', 0, 0, NULL, 0);
INSERT INTO `sys_permission` VALUES ('73678f9daa45ed17a3674131b03432fb', '540a2936940846cb98114ffb0d145cb8', '权限列表', '/list/permission-list', 'list/PermissionList', NULL, NULL, 1, NULL, NULL, 5.00, NULL, NULL, 1, 1, NULL, NULL, NULL, NULL, '2018-12-25 20:34:38', NULL, NULL, 0, 0, NULL, NULL);
INSERT INTO `sys_permission` VALUES ('7960961b0063228937da5fa8dd73d371', '2a470fc0c3954d9dbb61de6d80846549', 'JEditableTable示例', '/jeecg/JEditableTable', 'jeecg/JeecgEditableTableExample', NULL, NULL, 1, NULL, NULL, 2.10, 0, NULL, 1, 1, 0, 0, NULL, 'admin', '2019-03-22 15:22:18', 'admin', '2019-12-25 09:48:16', 0, 0, NULL, 0);
INSERT INTO `sys_permission` VALUES ('7ac9eb9ccbde2f7a033cd4944272bf1e', '540a2936940846cb98114ffb0d145cb8', '卡片列表', '/list/card', 'list/CardList', NULL, NULL, 1, NULL, NULL, 7.00, NULL, NULL, 1, 1, NULL, NULL, NULL, NULL, '2018-12-25 20:34:38', NULL, NULL, 0, 0, NULL, NULL);
INSERT INTO `sys_permission` VALUES ('841057b8a1bef8f6b4b20f9a618a7fa6', '08e6b9dc3c04489c8e1ff2ce6f105aa4', '数据日志', '/sys/dataLog-list', 'system/DataLogList', NULL, NULL, 1, NULL, NULL, 1.00, 0, NULL, 1, 1, NULL, 0, NULL, 'admin', '2019-03-11 19:26:49', 'admin', '2019-03-12 11:40:47', 0, 0, NULL, NULL);
INSERT INTO `sys_permission` VALUES ('882a73768cfd7f78f3a37584f7299656', '6e73eb3c26099c191bf03852ee1310a1', '个性化设置', '/account/settings/custom', 'account/settings/Custom', NULL, NULL, 1, 'CustomSettings', NULL, NULL, NULL, NULL, 1, 1, NULL, NULL, NULL, NULL, '2018-12-26 19:00:46', NULL, '2018-12-26 21:13:25', 0, 0, NULL, NULL);
INSERT INTO `sys_permission` VALUES ('8b3bff2eee6f1939147f5c68292a1642', '700b7f95165c46cc7a78bf227aa8fed3', '服务器信息', '/monitor/SystemInfo', 'modules/monitor/SystemInfo', NULL, NULL, 1, NULL, NULL, 4.00, 0, NULL, 1, 1, NULL, 0, NULL, 'admin', '2019-04-02 11:39:19', 'admin', '2019-04-02 15:40:02', 0, 0, NULL, NULL);
INSERT INTO `sys_permission` VALUES ('8d1ebd663688965f1fd86a2f0ead3416', '700b7f95165c46cc7a78bf227aa8fed3', 'Redis监控', '/monitor/redis/info', 'modules/monitor/RedisInfo', NULL, NULL, 1, NULL, NULL, 1.00, 0, NULL, 1, 1, NULL, 0, NULL, 'admin', '2019-04-02 13:11:33', 'admin', '2019-05-07 15:18:54', 0, 0, NULL, NULL);
INSERT INTO `sys_permission` VALUES ('8d4683aacaa997ab86b966b464360338', 'e41b69c57a941a3bbcce45032fe57605', 'Online表单开发', '/online/cgform', 'modules/online/cgform/OnlCgformHeadList', NULL, NULL, 1, NULL, NULL, 1.00, 0, NULL, 1, 0, NULL, 0, NULL, 'admin', '2019-03-12 15:48:14', 'admin', '2019-06-11 14:19:17', 0, 0, NULL, NULL);
INSERT INTO `sys_permission` VALUES ('8fb8172747a78756c11916216b8b8066', '717f6bee46f44a3897eca9abd6e2ec44', '工作台', '/dashboard/workplace', 'dashboard/Workplace', NULL, NULL, 1, NULL, NULL, 3.00, 0, NULL, 1, 1, NULL, 0, NULL, NULL, '2018-12-25 20:34:38', 'admin', '2019-04-02 11:45:02', 0, 0, NULL, NULL);
INSERT INTO `sys_permission` VALUES ('944abf0a8fc22fe1f1154a389a574154', '5c8042bd6c601270b2bbd9b20bccc68b', '消息管理', '/modules/message/sysMessageList', 'modules/message/SysMessageList', NULL, NULL, 1, NULL, NULL, 1.00, 0, NULL, 1, 1, NULL, 0, NULL, 'admin', '2019-04-09 11:27:53', 'admin', '2019-04-09 19:31:23', 0, 0, NULL, NULL);
INSERT INTO `sys_permission` VALUES ('9502685863ab87f0ad1134142788a385', '', '首页', '/dashboard/analysis', 'dashboard/Analysis', NULL, NULL, 0, NULL, NULL, 0.00, 0, 'home', 1, 1, 0, 1, NULL, NULL, '2018-12-25 20:34:38', 'admin', '2020-09-02 13:59:50', 0, 0, NULL, 0);
INSERT INTO `sys_permission` VALUES ('97c8629abc7848eccdb6d77c24bb3ebb', '700b7f95165c46cc7a78bf227aa8fed3', '磁盘监控', '/monitor/Disk', 'modules/monitor/DiskMonitoring', NULL, NULL, 1, NULL, NULL, 6.00, 0, NULL, 1, 1, NULL, 0, NULL, 'admin', '2019-04-25 14:30:06', 'admin', '2019-05-05 14:37:14', 0, 0, NULL, NULL);
INSERT INTO `sys_permission` VALUES ('9a90363f216a6a08f32eecb3f0bf12a3', '2a470fc0c3954d9dbb61de6d80846549', '自定义组件', '/jeecg/SelectDemo', 'jeecg/SelectDemo', NULL, NULL, 1, NULL, NULL, 0.00, 0, NULL, 1, 1, 0, 0, NULL, 'admin', '2019-03-19 11:19:05', 'admin', '2019-12-25 09:47:04', 0, 0, NULL, 0);
INSERT INTO `sys_permission` VALUES ('9cb91b8851db0cf7b19d7ecc2a8193dd', '1939e035e803a99ceecb6f5563570fb2', '我的任务表单', '/modules/bpm/task/form/FormModule', 'modules/bpm/task/form/FormModule', NULL, NULL, 1, NULL, NULL, 1.00, 0, NULL, 1, 1, NULL, 0, NULL, 'admin', '2019-03-08 16:49:05', 'admin', '2019-03-08 18:37:56', 0, 0, NULL, NULL);
INSERT INTO `sys_permission` VALUES ('9fe26464838de2ea5e90f2367e35efa0', 'e41b69c57a941a3bbcce45032fe57605', 'AUTO在线报表', '/online/cgreport/:code', 'modules/online/cgreport/auto/OnlCgreportAutoList', 'onlineAutoList', NULL, 1, NULL, NULL, 9.00, 0, NULL, 1, 1, NULL, 1, NULL, 'admin', '2019-03-12 11:06:48', 'admin', '2019-04-30 18:19:10', 0, 0, NULL, NULL);
INSERT INTO `sys_permission` VALUES ('a400e4f4d54f79bf5ce160ae432231af', '2a470fc0c3954d9dbb61de6d80846549', '百度', 'http://www.baidu.com', 'layouts/IframePageView', NULL, NULL, 1, NULL, NULL, 4.00, 0, NULL, 1, 1, NULL, 0, NULL, 'admin', '2019-01-29 19:44:06', 'admin', '2019-02-15 16:25:02', 0, 0, NULL, NULL);
INSERT INTO `sys_permission` VALUES ('ae4fed059f67086fd52a73d913cf473d', '540a2936940846cb98114ffb0d145cb8', '内联编辑表格', '/list/edit-table', 'list/TableInnerEditList', NULL, NULL, 1, NULL, NULL, 2.00, NULL, NULL, 1, 1, NULL, NULL, NULL, NULL, '2018-12-25 20:34:38', NULL, NULL, 0, 0, NULL, NULL);
INSERT INTO `sys_permission` VALUES ('aedbf679b5773c1f25e9f7b10111da73', '08e6b9dc3c04489c8e1ff2ce6f105aa4', 'SQL监控', '{{ window._CONFIG[\'domianURL\'] }}/druid/', 'layouts/IframePageView', NULL, NULL, 1, NULL, NULL, 1.00, 0, NULL, 1, 1, NULL, 0, NULL, 'admin', '2019-01-30 09:43:22', 'admin', '2019-03-23 19:00:46', 0, 0, NULL, NULL);
INSERT INTO `sys_permission` VALUES ('b1cb0a3fedf7ed0e4653cb5a229837ee', '08e6b9dc3c04489c8e1ff2ce6f105aa4', '定时任务', '/isystem/QuartzJobList', 'system/QuartzJobList', NULL, NULL, 1, NULL, NULL, 3.00, 0, NULL, 1, 1, NULL, 0, NULL, NULL, '2019-01-03 09:38:52', 'admin', '2019-04-02 10:24:13', 0, 0, NULL, NULL);
INSERT INTO `sys_permission` VALUES ('b3c824fc22bd953e2eb16ae6914ac8f9', '4875ebe289344e14844d8e3ea1edd73f', '高级详情页', '/profile/advanced', 'profile/advanced/Advanced', NULL, NULL, 1, NULL, NULL, 2.00, NULL, NULL, 1, 1, NULL, NULL, NULL, NULL, '2018-12-25 20:34:38', NULL, NULL, 0, 0, NULL, NULL);
INSERT INTO `sys_permission` VALUES ('b4dfc7d5dd9e8d5b6dd6d4579b1aa559', 'c65321e57b7949b7a975313220de0422', '500', '/exception/500', 'exception/500', NULL, NULL, 1, NULL, NULL, 3.00, NULL, NULL, 1, 1, NULL, NULL, NULL, NULL, '2018-12-25 20:34:38', NULL, NULL, 0, 0, NULL, NULL);
INSERT INTO `sys_permission` VALUES ('b6bcee2ccc854052d3cc3e9c96d90197', '71102b3b87fb07e5527bbd2c530dd90a', '加班申请', '/modules/extbpm/joa/JoaOvertimeList', 'modules/extbpm/joa/JoaOvertimeList', NULL, NULL, 1, NULL, NULL, 1.00, 0, NULL, 1, 1, NULL, 0, NULL, 'admin', '2019-04-03 15:33:10', 'admin', '2019-04-03 15:34:48', 0, 0, NULL, NULL);
INSERT INTO `sys_permission` VALUES ('c431130c0bc0ec71b0a5be37747bb36a', '2a470fc0c3954d9dbb61de6d80846549', '一对多JEditable', '/jeecg/JeecgOrderMainListForJEditableTable', 'jeecg/JeecgOrderMainListForJEditableTable', NULL, NULL, 1, NULL, NULL, 3.00, 0, NULL, 1, 1, NULL, 0, NULL, 'admin', '2019-03-29 10:51:59', 'admin', '2019-04-04 20:09:39', 0, 0, NULL, NULL);
INSERT INTO `sys_permission` VALUES ('c65321e57b7949b7a975313220de0422', NULL, '异常页', '/exception', 'layouts/RouteView', NULL, NULL, 0, NULL, NULL, 8.00, NULL, 'warning', 1, 0, NULL, NULL, NULL, NULL, '2018-12-25 20:34:38', NULL, NULL, 0, 0, NULL, NULL);
INSERT INTO `sys_permission` VALUES ('c6cf95444d80435eb37b2f9db3971ae6', '2a470fc0c3954d9dbb61de6d80846549', '数据回执模拟', '/jeecg/InterfaceTest', 'jeecg/InterfaceTest', NULL, NULL, 1, NULL, NULL, 6.00, 0, NULL, 1, 1, NULL, 0, NULL, 'admin', '2019-02-19 16:02:23', 'admin', '2019-02-21 16:25:45', 0, 0, NULL, NULL);
INSERT INTO `sys_permission` VALUES ('cc50656cf9ca528e6f2150eba4714ad2', '4875ebe289344e14844d8e3ea1edd73f', '基础详情页', '/profile/basic', 'profile/basic/Index', NULL, NULL, 1, NULL, NULL, 1.00, NULL, NULL, 1, 1, NULL, NULL, NULL, NULL, '2018-12-25 20:34:38', NULL, NULL, 0, 0, NULL, NULL);
INSERT INTO `sys_permission` VALUES ('d07a2c87a451434c99ab06296727ec4f', '700b7f95165c46cc7a78bf227aa8fed3', 'JVM信息', '/monitor/JvmInfo', 'modules/monitor/JvmInfo', NULL, NULL, 1, NULL, NULL, 4.00, 0, NULL, 1, 1, NULL, 0, NULL, 'admin', '2019-04-01 23:07:48', 'admin', '2019-04-02 11:37:16', 0, 0, NULL, NULL);
INSERT INTO `sys_permission` VALUES ('d2bbf9ebca5a8fa2e227af97d2da7548', 'c65321e57b7949b7a975313220de0422', '404', '/exception/404', 'exception/404', NULL, NULL, 1, NULL, NULL, 2.00, NULL, NULL, 1, 1, NULL, NULL, NULL, NULL, '2018-12-25 20:34:38', NULL, NULL, 0, 0, NULL, NULL);
INSERT INTO `sys_permission` VALUES ('d7d6e2e4e2934f2c9385a623fd98c6f3', '', '系统管理', '/isystem', 'layouts/RouteView', NULL, NULL, 0, NULL, NULL, 4.00, 0, 'setting', 1, 0, NULL, 0, NULL, NULL, '2018-12-25 20:34:38', 'admin', '2019-03-31 22:19:52', 0, 0, NULL, NULL);
INSERT INTO `sys_permission` VALUES ('d86f58e7ab516d3bc6bfb1fe10585f97', '717f6bee46f44a3897eca9abd6e2ec44', '个人中心', '/account/center', 'account/center/Index', NULL, NULL, 1, NULL, NULL, 1.00, NULL, NULL, 1, 1, NULL, NULL, NULL, NULL, '2018-12-25 20:34:38', NULL, NULL, 0, 0, NULL, NULL);
INSERT INTO `sys_permission` VALUES ('de13e0f6328c069748de7399fcc1dbbd', 'fb07ca05a3e13674dbf6d3245956da2e', '搜索列表（项目）', '/list/search/project', 'list/TableList', NULL, NULL, 1, NULL, NULL, 1.00, 0, NULL, 1, 1, NULL, 0, NULL, 'admin', '2019-02-12 14:01:40', 'admin', '2019-02-12 14:14:18', 0, 0, NULL, NULL);
INSERT INTO `sys_permission` VALUES ('e08cb190ef230d5d4f03824198773950', 'd7d6e2e4e2934f2c9385a623fd98c6f3', '系统通告', '/isystem/annountCement', 'system/SysAnnouncementList', NULL, NULL, 1, 'annountCement', NULL, 6.00, NULL, '', 1, 1, NULL, NULL, NULL, NULL, '2019-01-02 17:23:01', NULL, '2019-01-02 17:31:23', 0, 0, NULL, NULL);
INSERT INTO `sys_permission` VALUES ('e1979bb53e9ea51cecc74d86fd9d2f64', '2a470fc0c3954d9dbb61de6d80846549', 'PDF预览', '/jeecg/jeecgPdfView', 'jeecg/JeecgPdfView', NULL, NULL, 1, NULL, NULL, 3.00, 0, NULL, 1, 1, NULL, 0, NULL, 'admin', '2019-04-25 10:39:35', NULL, NULL, 0, 0, NULL, NULL);
INSERT INTO `sys_permission` VALUES ('e3c13679c73a4f829bcff2aba8fd68b1', '', '表单页', '/form', 'layouts/PageView', NULL, NULL, 0, NULL, NULL, 9.00, 0, 'form', 1, 0, NULL, 0, NULL, NULL, '2018-12-25 20:34:38', 'admin', '2019-03-31 22:20:14', 0, 0, NULL, NULL);
INSERT INTO `sys_permission` VALUES ('e41b69c57a941a3bbcce45032fe57605', '', '在线开发', '/online', 'layouts/RouteView', NULL, NULL, 0, NULL, NULL, 5.00, 0, 'cloud', 1, 0, NULL, 0, NULL, 'admin', '2019-03-08 10:43:10', 'admin', '2019-05-11 10:36:01', 0, 0, NULL, NULL);
INSERT INTO `sys_permission` VALUES ('e5973686ed495c379d829ea8b2881fc6', 'e3c13679c73a4f829bcff2aba8fd68b1', '高级表单', '/form/advanced-form', 'form/advancedForm/AdvancedForm', NULL, NULL, 1, NULL, NULL, 3.00, NULL, NULL, 1, 1, NULL, NULL, NULL, NULL, '2018-12-25 20:34:38', NULL, NULL, 0, 0, NULL, NULL);
INSERT INTO `sys_permission` VALUES ('e6bfd1fcabfd7942fdd05f076d1dad38', '2a470fc0c3954d9dbb61de6d80846549', '打印测试', '/jeecg/PrintDemo', 'jeecg/PrintDemo', NULL, NULL, 1, NULL, NULL, 3.00, 0, NULL, 1, 1, NULL, 0, NULL, 'admin', '2019-02-19 15:58:48', 'admin', '2019-05-07 20:14:39', 0, 0, NULL, NULL);
INSERT INTO `sys_permission` VALUES ('ebb9d82ea16ad864071158e0c449d186', 'd7d6e2e4e2934f2c9385a623fd98c6f3', '分类字典', '/isys/category', 'system/SysCategoryList', NULL, NULL, 1, NULL, '1', 5.20, 0, NULL, 1, 1, 0, 0, NULL, 'admin', '2019-05-29 18:48:07', 'admin', '2020-02-23 22:45:33', 0, 0, '1', 0);
INSERT INTO `sys_permission` VALUES ('ec8d607d0156e198b11853760319c646', '6e73eb3c26099c191bf03852ee1310a1', '安全设置', '/account/settings/security', 'account/settings/Security', NULL, NULL, 1, 'SecuritySettings', NULL, NULL, NULL, NULL, 1, 1, NULL, NULL, NULL, NULL, '2018-12-26 18:59:52', NULL, NULL, 0, 0, NULL, NULL);
INSERT INTO `sys_permission` VALUES ('f0675b52d89100ee88472b6800754a08', '', '统计报表', '/report', 'layouts/RouteView', NULL, NULL, 0, NULL, NULL, 1.00, 0, 'bar-chart', 1, 0, NULL, 0, NULL, 'admin', '2019-04-03 18:32:02', 'admin', '2019-05-19 18:34:13', 0, 0, NULL, NULL);
INSERT INTO `sys_permission` VALUES ('f1cb187abf927c88b89470d08615f5ac', 'd7d6e2e4e2934f2c9385a623fd98c6f3', '数据字典', '/isystem/dict', 'system/DictList', NULL, NULL, 1, NULL, NULL, 5.00, 0, NULL, 1, 1, 0, 0, NULL, NULL, '2018-12-28 13:54:43', 'admin', '2020-02-23 22:45:25', 0, 0, NULL, 0);
INSERT INTO `sys_permission` VALUES ('f23d9bfff4d9aa6b68569ba2cff38415', '540a2936940846cb98114ffb0d145cb8', '标准列表', '/list/basic-list', 'list/StandardList', NULL, NULL, 1, NULL, NULL, 6.00, NULL, NULL, 1, 1, NULL, NULL, NULL, NULL, '2018-12-25 20:34:38', NULL, NULL, 0, 0, NULL, NULL);
INSERT INTO `sys_permission` VALUES ('f2849d3814fc97993bfc519ae6bbf049', 'e41b69c57a941a3bbcce45032fe57605', 'AUTO复制表单', '/online/copyform/:code', 'modules/online/cgform/OnlCgformCopyList', NULL, NULL, 1, NULL, '1', 1.00, 0, NULL, 1, 1, 0, 1, NULL, 'admin', '2019-08-29 16:05:37', NULL, NULL, 0, 0, '1', NULL);
INSERT INTO `sys_permission` VALUES ('f780d0d3083d849ccbdb1b1baee4911d', '5c8042bd6c601270b2bbd9b20bccc68b', '模板管理', '/modules/message/sysMessageTemplateList', 'modules/message/SysMessageTemplateList', NULL, NULL, 1, NULL, NULL, 1.00, 0, NULL, 1, 1, NULL, 0, NULL, 'admin', '2019-04-09 11:50:31', 'admin', '2019-04-12 10:16:34', 0, 0, NULL, NULL);
INSERT INTO `sys_permission` VALUES ('fb07ca05a3e13674dbf6d3245956da2e', '540a2936940846cb98114ffb0d145cb8', '搜索列表', '/list/search', 'list/search/SearchLayout', NULL, '/list/search/article', 1, NULL, NULL, 8.00, 0, NULL, 1, 0, NULL, 0, NULL, NULL, '2018-12-25 20:34:38', 'admin', '2019-02-12 15:09:13', 0, 0, NULL, NULL);
INSERT INTO `sys_permission` VALUES ('fb367426764077dcf94640c843733985', '2a470fc0c3954d9dbb61de6d80846549', '一对多示例', '/jeecg/JeecgOrderMainList', 'jeecg/JeecgOrderMainList', NULL, NULL, 1, NULL, NULL, 2.00, 0, NULL, 1, 1, NULL, 0, NULL, 'admin', '2019-02-15 16:24:11', 'admin', '2019-02-18 10:50:14', 0, 0, NULL, NULL);
INSERT INTO `sys_permission` VALUES ('fba41089766888023411a978d13c0aa4', 'e41b69c57a941a3bbcce45032fe57605', 'AUTO树表单列表', '/online/cgformTreeList/:code', 'modules/online/cgform/auto/OnlCgformTreeList', NULL, NULL, 1, NULL, '1', 9.00, 0, NULL, 1, 1, NULL, 1, NULL, 'admin', '2019-05-21 14:46:50', 'admin', '2019-06-11 13:52:52', 0, 0, '1', NULL);
INSERT INTO `sys_permission` VALUES ('fc810a2267dd183e4ef7c71cc60f4670', '700b7f95165c46cc7a78bf227aa8fed3', '请求追踪', '/monitor/HttpTrace', 'modules/monitor/HttpTrace', NULL, NULL, 1, NULL, NULL, 4.00, 0, NULL, 1, 1, NULL, 0, NULL, 'admin', '2019-04-02 09:46:19', 'admin', '2019-04-02 11:37:27', 0, 0, NULL, NULL);
INSERT INTO `sys_permission` VALUES ('fedfbf4420536cacc0218557d263dfea', '6e73eb3c26099c191bf03852ee1310a1', '新消息通知', '/account/settings/notification', 'account/settings/Notification', NULL, NULL, 1, 'NotificationSettings', NULL, NULL, NULL, '', 1, 1, NULL, NULL, NULL, NULL, '2018-12-26 19:02:05', NULL, NULL, 0, 0, NULL, NULL);
INSERT INTO `sys_permission` VALUES ('ff8080817608b794017622a194750409', '1309379979724722178', '数据中心', NULL, NULL, NULL, NULL, 2, 'assetPageDatacenter', '0', NULL, 0, NULL, 0, 0, 0, 0, NULL, 'sgcc_super_admin', '2020-12-03 14:21:06', 'sgcc_super_admin', '2020-12-05 20:45:19', 0, 0, NULL, 0);
INSERT INTO `sys_permission` VALUES ('ff8080817608b794017622a2b4b50410', '1309379979724722178', '基础设施', NULL, NULL, NULL, NULL, 1, 'f010103_L1', '0', NULL, 0, NULL, 0, 0, 0, 0, NULL, 'sgcc_super_admin', '2020-12-03 14:21:06', 'sgcc_super_admin', '2020-12-05 20:45:19', 0, 0, NULL, 0);
INSERT INTO `sys_permission` VALUES ('ff8080817608b794017622a30dd00417', '1309379979724722178', 'IT设备', NULL, NULL, NULL, NULL, 1, 'f010103_L2', '0', NULL, 0, NULL, 0, 0, 0, 0, NULL, 'sgcc_super_admin', '2020-12-03 14:21:06', 'sgcc_super_admin', '2020-12-05 20:45:19', 0, 0, NULL, 0);
INSERT INTO `sys_permission` VALUES ('ff8080817608b794017622a3c7b2041e', '1309379979724722178', '云平台资源', NULL, NULL, NULL, NULL, 1, 'f010103_L3', '0', NULL, 0, NULL, 0, 0, 0, 0, NULL, 'sgcc_super_admin', '2020-12-03 14:21:06', 'sgcc_super_admin', '2020-12-05 20:45:19', 0, 0, NULL, 0);
INSERT INTO `sys_permission` VALUES ('ff8080817608b794017622a41de50425', '1309379979724722178', '应用资源', NULL, NULL, NULL, NULL, 1, 'f010103_L4', '0', NULL, 0, NULL, 0, 0, 0, 0, NULL, 'sgcc_super_admin', '2020-12-03 14:21:06', 'sgcc_super_admin', '2020-12-05 20:45:19', 0, 0, NULL, 0);
INSERT INTO `sys_permission` VALUES ('ff8080817608b794017622a4f475042c', 'ff8080817608b794017622a2b4b50410', '查询', NULL, NULL, NULL, NULL, 2, 'assetPageDevice', '0', NULL, 0, NULL, 0, 0, 0, 0, NULL, 'sgcc_super_admin', '2020-12-03 14:21:06', 'sgcc_super_admin', '2020-12-05 20:45:19', 0, 0, NULL, 0);
INSERT INTO `sys_permission` VALUES ('ff8080817608b794017622a5b3190433', 'ff8080817608b794017622a2b4b50410', '监控详情', NULL, NULL, NULL, NULL, 2, 'statisticsLatestSamples', '0', NULL, 0, NULL, 0, 0, 0, 0, NULL, 'sgcc_super_admin', '2020-12-03 14:21:06', 'sgcc_super_admin', '2020-12-05 20:45:19', 0, 0, NULL, 0);
INSERT INTO `sys_permission` VALUES ('ff8080817608b794017622a667f4043a', 'ff8080817608b794017622a30dd00417', '查询', NULL, NULL, NULL, NULL, 2, 'assetPageNode', '0', NULL, 0, NULL, 0, 0, 0, 0, NULL, 'sgcc_super_admin', '2020-12-03 14:21:06', 'sgcc_super_admin', '2020-12-05 20:45:19', 0, 0, NULL, 0);
INSERT INTO `sys_permission` VALUES ('ff8080817608b794017622a6f7330441', 'ff8080817608b794017622a3c7b2041e', '查询', NULL, NULL, NULL, NULL, 2, 'cloud_resourcePage', '0', NULL, 0, NULL, 0, 0, 0, 0, NULL, 'sgcc_super_admin', '2020-12-03 14:21:06', 'sgcc_super_admin', '2020-12-05 20:45:19', 0, 0, NULL, 0);
INSERT INTO `sys_permission` VALUES ('ff8080817608b794017622a78ee90448', 'ff8080817608b794017622a41de50425', '查询', NULL, NULL, NULL, NULL, 2, 'appPage', '0', NULL, 0, NULL, 0, 0, 0, 0, NULL, 'sgcc_super_admin', '2020-12-03 14:21:06', 'sgcc_super_admin', '2020-12-05 20:45:19', 0, 0, NULL, 0);
INSERT INTO `sys_permission` VALUES ('ff8080817608b794017622a81cb7044f', 'ff8080817608b794017622a41de50425', '监控详情', NULL, NULL, NULL, NULL, 2, 'statisticsLatestSample', '0', NULL, 0, NULL, 0, 0, 0, 0, NULL, 'sgcc_super_admin', '2020-12-03 14:21:06', 'sgcc_super_admin', '2020-12-05 20:45:19', 0, 0, NULL, 0);
INSERT INTO `sys_permission` VALUES ('ff8080817608b794017622aa75b10456', '1301096112848900098', '数据中心', NULL, NULL, NULL, NULL, 1, 'f010102', '0', NULL, 0, NULL, 0, 0, 0, 0, NULL, 'sfadmin', '2020-12-03 10:07:49', 'sgcc_super_admin', '2020-12-05 20:45:19', 0, 0, NULL, 0);
INSERT INTO `sys_permission` VALUES ('ff8080817608b794017622aadd20045d', '1301096112848900098', '基础设施', NULL, NULL, NULL, NULL, 1, 'f010102_L1', '0', NULL, 0, NULL, 0, 0, 0, 0, NULL, 'ywyuser', '2020-12-02 19:58:16', 'sgcc_super_admin', '2020-12-05 20:45:19', 0, 0, NULL, 0);
INSERT INTO `sys_permission` VALUES ('ff8080817608b794017622ab44760464', '1301096112848900098', 'IT设备', NULL, NULL, NULL, NULL, 1, 'f010102_L2', '0', NULL, 0, NULL, 0, 0, 0, 0, NULL, 'sfadmin', '2020-12-03 10:07:49', 'sgcc_super_admin', '2020-12-05 20:45:19', 0, 0, NULL, 0);
INSERT INTO `sys_permission` VALUES ('ff8080817608b794017622ab9cea046b', '1301096112848900098', '云平台资源', NULL, NULL, NULL, NULL, 1, 'f010102_L3', '0', NULL, 0, NULL, 0, 0, 0, 0, NULL, 'sfadmin', '2020-12-03 10:07:49', 'sgcc_super_admin', '2020-12-05 20:45:19', 0, 0, NULL, 0);
INSERT INTO `sys_permission` VALUES ('ff8080817608b794017622ac13230472', '1301096112848900098', '应用资源', NULL, NULL, NULL, NULL, 1, 'f010102_L4', '0', NULL, 0, NULL, 0, 0, 0, 0, NULL, 'sfadmin', '2020-12-03 10:07:49', 'sgcc_super_admin', '2020-12-05 20:45:19', 0, 0, NULL, 0);
INSERT INTO `sys_permission` VALUES ('ff8080817608b794017622acfc5d0479', 'ff8080817608b794017622aadd20045d', '添加', NULL, NULL, NULL, NULL, 2, 'assetCreateDevice', '0', NULL, 0, NULL, 0, 0, 0, 0, NULL, 'ywyuser', '2020-12-02 19:58:16', 'sgcc_super_admin', '2020-12-05 20:45:19', 0, 0, NULL, 0);
INSERT INTO `sys_permission` VALUES ('ff8080817608b794017622adde850480', 'ff8080817608b794017622aadd20045d', '查询', NULL, NULL, NULL, NULL, 2, 'assetPageDevice', '0', NULL, 0, NULL, 0, 0, 0, 0, NULL, 'ywyuser', '2020-12-02 19:58:16', 'sgcc_super_admin', '2020-12-05 20:45:19', 0, 0, NULL, 0);
INSERT INTO `sys_permission` VALUES ('ff8080817608b794017622ae6de30487', 'ff8080817608b794017622aadd20045d', '删除', NULL, NULL, NULL, NULL, 2, 'assetDeleteDevice', '0', NULL, 0, NULL, 0, 0, 0, 0, NULL, 'ywyuser', '2020-12-02 19:58:16', 'sgcc_super_admin', '2020-12-05 20:45:19', 0, 0, NULL, 0);
INSERT INTO `sys_permission` VALUES ('ff8080817608b794017622aecbde048e', 'ff8080817608b794017622aadd20045d', '编辑', NULL, NULL, NULL, NULL, 2, 'assetUpdateDevice', '0', NULL, 0, NULL, 0, 0, 0, 0, NULL, 'ywyuser', '2020-12-02 19:58:16', 'sgcc_super_admin', '2020-12-05 20:45:19', 0, 0, NULL, 0);
INSERT INTO `sys_permission` VALUES ('ff8080817608b794017622af287d0495', 'ff8080817608b794017622aadd20045d', '详情', NULL, NULL, NULL, NULL, 2, 'assetDetailDevice', '0', NULL, 0, NULL, 0, 0, 0, 0, NULL, 'ywyuser', '2020-12-02 19:58:16', 'sgcc_super_admin', '2020-12-05 20:45:19', 0, 0, NULL, 0);
INSERT INTO `sys_permission` VALUES ('ff8080817608b794017622b01610049c', 'ff8080817608b794017622ab44760464', '添加服务器', NULL, NULL, NULL, NULL, 2, 'assetCreateNode', '0', NULL, 0, NULL, 0, 0, 0, 0, NULL, 'sfadmin', '2020-12-03 10:18:20', 'sgcc_super_admin', '2020-12-05 20:45:19', 0, 0, NULL, 0);
INSERT INTO `sys_permission` VALUES ('ff8080817608b794017622b0fc3504a3', 'ff8080817608b794017622ab44760464', '删除服务器', NULL, NULL, NULL, NULL, 2, 'assetDeleteNode', '0', NULL, 0, NULL, 0, 0, 0, 0, NULL, 'sfadmin', '2020-12-03 10:18:20', 'sgcc_super_admin', '2020-12-05 20:45:19', 0, 0, NULL, 0);
INSERT INTO `sys_permission` VALUES ('ff8080817608b794017622b168db04aa', 'ff8080817608b794017622ab44760464', '编辑服务器', NULL, NULL, NULL, NULL, 2, 'assetUpdateNode', '0', NULL, 0, NULL, 0, 0, 0, 0, NULL, 'sfadmin', '2020-12-03 10:18:20', 'sgcc_super_admin', '2020-12-05 20:45:19', 0, 0, NULL, 0);
INSERT INTO `sys_permission` VALUES ('ff8080817608b794017622b1ef1f04b1', 'ff8080817608b794017622ab44760464', '服务器详情', NULL, NULL, NULL, NULL, 2, 'assetDetailNode', '0', NULL, 0, NULL, 0, 0, 0, 0, NULL, 'sfadmin', '2020-12-03 10:18:20', 'sgcc_super_admin', '2020-12-05 20:45:19', 0, 0, NULL, 0);
INSERT INTO `sys_permission` VALUES ('ff8080817608b794017622b2a1e404b8', 'ff8080817608b794017622ab44760464', '添加交换机', NULL, NULL, NULL, NULL, 2, 'assetCreateSwitcher', '0', NULL, 0, NULL, 0, 0, 0, 0, NULL, 'sfadmin', '2020-12-03 10:18:20', 'sgcc_super_admin', '2020-12-05 20:45:19', 0, 0, NULL, 0);
INSERT INTO `sys_permission` VALUES ('ff8080817608b794017622b33a5004bf', 'ff8080817608b794017622ab44760464', '编辑交换机', NULL, NULL, NULL, NULL, 2, 'assetUpdateSwitcher', '0', NULL, 0, NULL, 0, 0, 0, 0, NULL, 'sfadmin', '2020-12-03 10:18:20', 'sgcc_super_admin', '2020-12-05 20:45:19', 0, 0, NULL, 0);
INSERT INTO `sys_permission` VALUES ('ff8080817608b794017622b3a2bc04c6', 'ff8080817608b794017622ab44760464', '删除交换机', NULL, NULL, NULL, NULL, 2, 'assetDeleteSwitcher', '0', NULL, 0, NULL, 0, 0, 0, 0, NULL, 'sfadmin', '2020-12-03 10:18:20', 'sgcc_super_admin', '2020-12-05 20:45:19', 0, 0, NULL, 0);
INSERT INTO `sys_permission` VALUES ('ff8080817608b794017622b40d7f04cd', 'ff8080817608b794017622ab44760464', '交换机详情', NULL, NULL, NULL, NULL, 2, 'assetDetailSwitcher', '0', NULL, 0, NULL, 0, 0, 0, 0, NULL, 'sfadmin', '2020-12-03 10:18:20', 'sgcc_super_admin', '2020-12-05 20:45:19', 0, 0, NULL, 0);
INSERT INTO `sys_permission` VALUES ('ff8080817608b794017622b4b9e004d4', 'ff8080817608b794017622ab44760464', '查询服务器', NULL, NULL, NULL, NULL, 2, 'assetPageNode', '0', NULL, 0, NULL, 0, 0, 0, 0, NULL, 'sfadmin', '2020-12-03 10:18:20', 'sgcc_super_admin', '2020-12-05 20:45:19', 0, 0, NULL, 0);
INSERT INTO `sys_permission` VALUES ('ff8080817608b794017622b5129d04db', 'ff8080817608b794017622ab44760464', '查询交换机', NULL, NULL, NULL, NULL, 2, 'assetPageSwitcher', '0', NULL, 0, NULL, 0, 0, 0, 0, NULL, 'sfadmin', '2020-12-03 10:18:20', 'sgcc_super_admin', '2020-12-05 20:45:19', 0, 0, NULL, 0);
INSERT INTO `sys_permission` VALUES ('ff8080817608b794017622b5f5f804e2', 'ff8080817608b794017622ab9cea046b', '添加', NULL, NULL, NULL, NULL, 2, 'cloud_resourceCreate', '0', NULL, 0, NULL, 0, 0, 0, 0, NULL, 'sgcc_super_admin', '2020-12-03 14:21:06', 'sgcc_super_admin', '2020-12-05 20:45:19', 0, 0, NULL, 0);
INSERT INTO `sys_permission` VALUES ('ff8080817608b794017622b65e3b04e9', 'ff8080817608b794017622ab9cea046b', '查询', NULL, NULL, NULL, NULL, 2, 'cloud_resourcePage', '0', NULL, 0, NULL, 0, 0, 0, 0, NULL, 'sgcc_super_admin', '2020-12-03 14:21:06', 'sgcc_super_admin', '2020-12-05 20:45:19', 0, 0, NULL, 0);
INSERT INTO `sys_permission` VALUES ('ff8080817608b794017622b7146504f0', 'ff8080817608b794017622ab9cea046b', '删除', NULL, NULL, NULL, NULL, 2, 'cloud_resourceDelete', '0', NULL, 0, NULL, 0, 0, 0, 0, NULL, 'sgcc_super_admin', '2020-12-03 14:21:06', 'sgcc_super_admin', '2020-12-05 20:45:19', 0, 0, NULL, 0);
INSERT INTO `sys_permission` VALUES ('ff8080817608b794017622b767c004f7', 'ff8080817608b794017622ab9cea046b', '编辑', NULL, NULL, NULL, NULL, 2, 'cloud_resourceUpdate', '0', NULL, 0, NULL, 0, 0, 0, 0, NULL, 'sgcc_super_admin', '2020-12-03 14:21:06', 'sgcc_super_admin', '2020-12-05 20:45:19', 0, 0, NULL, 0);
INSERT INTO `sys_permission` VALUES ('ff8080817608b794017622b8706f04fe', 'ff8080817608b794017622ac13230472', '添加', NULL, NULL, NULL, NULL, 2, 'appCreate', '0', NULL, 0, NULL, 0, 0, 0, 0, NULL, 'sfadmin', '2020-12-03 10:07:49', 'sgcc_super_admin', '2020-12-05 20:45:19', 0, 0, NULL, 0);
INSERT INTO `sys_permission` VALUES ('ff8080817608b794017622b8c8970505', 'ff8080817608b794017622ac13230472', '查询', NULL, NULL, NULL, NULL, 2, 'appPage', '0', NULL, 0, NULL, 0, 0, 0, 0, NULL, 'sfadmin', '2020-12-03 10:07:49', 'sgcc_super_admin', '2020-12-05 20:45:19', 0, 0, NULL, 0);
INSERT INTO `sys_permission` VALUES ('ff8080817608b794017622b92495050c', 'ff8080817608b794017622ac13230472', '编辑', NULL, NULL, NULL, NULL, 2, 'appUpdate', '0', NULL, 0, NULL, 0, 0, 0, 0, NULL, 'sfadmin', '2020-12-03 10:07:49', 'sgcc_super_admin', '2020-12-05 20:45:19', 0, 0, NULL, 0);
INSERT INTO `sys_permission` VALUES ('ff8080817608b794017622ba7fec0513', '1310406951590432769', '诊断告警', NULL, NULL, NULL, NULL, 2, 'historyPage', '0', NULL, 0, NULL, 0, 0, 0, 0, NULL, 'sgcc_super_admin', '2020-12-03 14:21:06', 'sgcc_super_admin', '2020-12-05 20:45:19', 0, 0, NULL, 0);
INSERT INTO `sys_permission` VALUES ('ff8080817608b794017622bb2c40051a', '1310406951590432769', '告警策略', NULL, NULL, NULL, NULL, 2, 'policyPage', '0', NULL, 0, NULL, 0, 0, 0, 0, NULL, 'sgcc_super_admin', '2020-12-03 14:21:06', 'sgcc_super_admin', '2020-12-05 20:45:19', 0, 0, NULL, 0);
INSERT INTO `sys_permission` VALUES ('ff8080817608b794017622bbc3990521', '1310406951590432769', '告警联系人', NULL, NULL, NULL, NULL, 2, 'linkmanList', '0', NULL, 0, NULL, 0, 0, 0, 0, NULL, 'sgcc_super_admin', '2020-12-03 14:21:06', 'sgcc_super_admin', '2020-12-05 20:45:19', 0, 0, NULL, 0);
INSERT INTO `sys_permission` VALUES ('ff8080817608b794017622c0a5fa053d', 'ff8080817608b794017622bbc3990521', '保存设置', NULL, NULL, NULL, NULL, 2, 'linkmanCreate', '0', NULL, 0, NULL, 0, 0, 0, 0, NULL, 'sgcc_config_admin', '2020-12-03 15:24:25', 'sgcc_config_admin', '2020-12-03 17:14:39', 0, 0, NULL, 0);
INSERT INTO `sys_permission` VALUES ('ff8080817608b794017622c3277c0544', '1310407413609795585', '查询', NULL, NULL, NULL, NULL, 2, 'assetPageNode', '0', NULL, 0, NULL, 0, 0, 0, 0, NULL, 'sgcc_super_admin', '2020-12-03 14:21:06', 'sgcc_super_admin', '2020-12-05 20:45:19', 0, 0, NULL, 0);
INSERT INTO `sys_permission` VALUES ('ff8080817608b794017622c3b1b0054b', '1310407620586115073', '查询', NULL, NULL, NULL, NULL, 2, 'assetPageSwitcher', '0', NULL, 0, NULL, 0, 0, 0, 0, NULL, 'sgcc_super_admin', '2020-12-03 14:21:06', 'sgcc_super_admin', '2020-12-05 20:45:19', 0, 0, NULL, 0);
INSERT INTO `sys_permission` VALUES ('ff8080817608b794017622c5d9e40552', '1310404620442406913', '数据安全设置', NULL, NULL, NULL, NULL, 2, 'dataSecuritySet', '0', NULL, 0, NULL, 0, 0, 0, 0, NULL, 'sgcc_super_admin', '2020-12-03 14:21:06', 'sgcc_super_admin', '2020-12-05 20:45:19', 0, 0, NULL, 0);
INSERT INTO `sys_permission` VALUES ('ff8080817608b794017622c7ca4b055b', '1310404991655088130', '查询', NULL, NULL, NULL, NULL, 2, 'fmPage', '0', NULL, 0, NULL, 0, 0, 0, 0, NULL, 'sgcc_super_admin', '2020-12-03 14:21:06', 'sgcc_super_admin', '2020-12-05 20:45:19', 0, 0, NULL, 0);
INSERT INTO `sys_permission` VALUES ('ff8080817608b794017622c83f120562', '1310404991655088130', '控制方式设置', NULL, NULL, NULL, NULL, 2, 'fmSetStrategy', '0', NULL, 0, NULL, 0, 0, 0, 0, NULL, 'sgcc_super_admin', '2020-12-03 14:21:06', 'sgcc_super_admin', '2020-12-05 20:45:19', 0, 0, NULL, 0);
INSERT INTO `sys_permission` VALUES ('ff8080817608b794017622cd7a6c056b', '1310405261181063170', '查询', NULL, NULL, NULL, NULL, 2, 'assetPageNode', '0', NULL, 0, NULL, 0, 0, 0, 0, NULL, 'sgcc_super_admin', '2020-12-03 14:21:06', 'sgcc_super_admin', '2020-12-05 20:45:19', 0, 0, NULL, 0);
INSERT INTO `sys_permission` VALUES ('ff8080817608b794017622ce912b0572', '1310405668003385346', '查询', NULL, NULL, NULL, NULL, 2, 'assetPageNode', '0', NULL, 0, NULL, 0, 0, 0, 0, NULL, 'sgcc_super_admin', '2020-12-03 14:21:06', 'sgcc_super_admin', '2020-12-05 20:45:19', 0, 0, NULL, 0);
INSERT INTO `sys_permission` VALUES ('ff8080817608b794017622cf13600579', '1310405668003385346', '查询2', NULL, NULL, NULL, NULL, 2, 'statisticsLatestSamples', '0', NULL, 0, NULL, 0, 0, 0, 0, NULL, 'sgcc_super_admin', '2020-12-03 14:21:06', 'sgcc_super_admin', '2020-12-05 20:45:19', 0, 0, NULL, 0);
INSERT INTO `sys_permission` VALUES ('ff8080817608b794017622d220310580', '1310406050767179777', '查询', NULL, NULL, NULL, NULL, 2, 'assetPageNode', '0', NULL, 0, NULL, 0, 0, 0, 0, NULL, 'sgcc_super_admin', '2020-12-03 14:21:06', 'sgcc_super_admin', '2020-12-05 20:45:19', 0, 0, NULL, 0);
INSERT INTO `sys_permission` VALUES ('ff8080817608b794017622d319d80589', '1310406614360002562', '查询', NULL, NULL, NULL, NULL, 2, 'assetPageNode', '0', NULL, 0, NULL, 0, 0, 0, 0, NULL, 'sgcc_super_admin', '2020-12-03 14:21:06', 'sgcc_super_admin', '2020-12-05 20:45:19', 0, 0, NULL, 0);
INSERT INTO `sys_permission` VALUES ('ff8080817608b794017622d35cf30590', '1310406614360002562', '查询2', NULL, NULL, NULL, NULL, 2, 'assetPageNode', '0', NULL, 0, NULL, 0, 0, 0, 0, NULL, 'sgcc_super_admin', '2020-12-03 14:21:06', 'sgcc_super_admin', '2020-12-05 20:45:19', 0, 0, NULL, 0);
INSERT INTO `sys_permission` VALUES ('ff8080817608b794017622fb507205ed', '1310402513266020353', '查询', NULL, NULL, NULL, NULL, 2, 'assetPageDevice', '0', NULL, 0, NULL, 0, 0, 0, 0, NULL, 'sgcc_super_admin', '2020-12-03 14:21:06', 'sgcc_super_admin', '2020-12-05 20:45:19', 0, 0, NULL, 0);
INSERT INTO `sys_permission` VALUES ('ff8080817608b794017622fbead705f4', '1310402513266020353', '查询1', NULL, NULL, NULL, NULL, 2, 'statisticsLatestSamples', '0', NULL, 0, NULL, 0, 0, 0, 0, NULL, 'sgcc_super_admin', '2020-12-03 14:21:06', 'sgcc_super_admin', '2020-12-05 20:45:19', 0, 0, NULL, 0);
INSERT INTO `sys_permission` VALUES ('ff8080817608b794017622fc714605fb', '1310403041836404738', '查询', NULL, NULL, NULL, NULL, 2, 'assetPageDevice', '0', NULL, 0, NULL, 0, 0, 0, 0, NULL, 'sgcc_super_admin', '2020-12-03 14:21:06', 'sgcc_super_admin', '2020-12-05 20:45:19', 0, 0, NULL, 0);
INSERT INTO `sys_permission` VALUES ('ff8080817608b794017622fe2a640604', '1310403230580084737', '提交', NULL, NULL, NULL, NULL, 2, 'modelSetModelFactors', '0', NULL, 0, NULL, 0, 0, 0, 0, NULL, 'sgcc_super_admin', '2020-12-03 14:21:07', 'sgcc_super_admin', '2020-12-05 20:45:19', 0, 0, NULL, 0);
INSERT INTO `sys_permission` VALUES ('ff8080817608b794017622ff496b060b', '1310403473635807234', '诊断告警', NULL, NULL, NULL, NULL, 2, 'historyPage', '0', NULL, 0, NULL, 0, 0, 0, 0, NULL, 'sgcc_super_admin', '2020-12-03 14:21:07', 'sgcc_super_admin', '2020-12-05 20:45:19', 0, 0, NULL, 0);
INSERT INTO `sys_permission` VALUES ('ff8080817608b794017623000f7a0612', '1310403473635807234', '告警设置', NULL, NULL, NULL, NULL, 2, 'policyPage', '0', NULL, 0, NULL, 0, 0, 0, 0, NULL, 'sgcc_super_admin', '2020-12-03 14:21:07', 'sgcc_super_admin', '2020-12-05 20:45:19', 0, 0, NULL, 0);
INSERT INTO `sys_permission` VALUES ('ff8080817608b79401762301ef720619', 'ff8080817608b794017623000f7a0612', '新增策略', NULL, NULL, NULL, NULL, 2, 'statisticsListMeter', '0', NULL, 0, NULL, 0, 0, 0, 0, NULL, 'sgcc_super_admin', '2020-12-03 14:21:07', 'sgcc_super_admin', '2020-12-05 20:45:19', 0, 0, NULL, 0);
INSERT INTO `sys_permission` VALUES ('ff8080817608b79401762302f76f0620', '1310403473635807234', '告警联系人', NULL, NULL, NULL, NULL, 2, 'linkmanList', '0', NULL, 0, NULL, 0, 0, 0, 0, NULL, 'sgcc_super_admin', '2020-12-03 14:21:07', 'sgcc_super_admin', '2020-12-05 20:45:19', 0, 0, NULL, 0);
INSERT INTO `sys_permission` VALUES ('ff8080817608b79401762303bd0b0627', 'ff8080817608b79401762302f76f0620', '保存设置', NULL, NULL, NULL, NULL, 2, 'linkmanCreate', '0', NULL, 0, NULL, 0, 0, 0, 0, NULL, 'sgcc_super_admin', '2020-12-03 14:21:07', 'sgcc_super_admin', '2020-12-05 20:45:19', 0, 0, NULL, 0);
INSERT INTO `sys_permission` VALUES ('ff8080817608b794017623079ad1062e', '1310400302846840834', '数据中心列表', NULL, NULL, NULL, NULL, 2, 'assetPageDatacenter', '0', NULL, 0, NULL, 0, 0, 0, 0, NULL, 'sgcc_super_admin', '2020-12-03 14:21:07', 'sgcc_super_admin', '2020-12-05 20:45:19', 0, 0, NULL, 0);
INSERT INTO `sys_permission` VALUES ('ff8080817608b79401762309244b0635', '1310400302846840834', '查询', NULL, NULL, NULL, NULL, 2, 'modelPageModelScore', '0', NULL, 0, NULL, 0, 0, 0, 0, NULL, 'sgcc_super_admin', '2020-12-03 14:21:07', 'sgcc_super_admin', '2020-12-05 20:45:19', 0, 0, NULL, 0);
INSERT INTO `sys_permission` VALUES ('ff8080817608b79401762309ae24063c', '1310400302846840834', '最优成本模型设置', NULL, NULL, NULL, NULL, 2, 'modelSetModelFactors', '0', NULL, 0, NULL, 0, 0, 0, 0, NULL, 'sgcc_super_admin', '2020-12-03 14:21:07', 'sgcc_super_admin', '2020-12-05 20:45:19', 0, 0, NULL, 0);
INSERT INTO `sys_permission` VALUES ('ff8080817608b7940176230a57090643', '1310400531214110721', '数据中心列表', NULL, NULL, NULL, NULL, 2, 'assetPageDatacenter', '0', NULL, 0, NULL, 0, 0, 0, 0, NULL, 'sgcc_super_admin', '2020-12-03 14:21:07', 'sgcc_super_admin', '2020-12-05 20:45:19', 0, 0, NULL, 0);
INSERT INTO `sys_permission` VALUES ('ff8080817608b7940176230ac780064a', '1310400531214110721', '查询', NULL, NULL, NULL, NULL, 2, 'modelPageModelScore', '0', NULL, 0, NULL, 0, 0, 0, 0, NULL, 'sgcc_super_admin', '2020-12-03 14:21:07', 'sgcc_super_admin', '2020-12-05 20:45:20', 0, 0, NULL, 0);
INSERT INTO `sys_permission` VALUES ('ff8080817608b7940176230b42a90651', '1310400531214110721', '最优设备模型设置', NULL, NULL, NULL, NULL, 2, 'modelSetModelFactors', '0', NULL, 0, NULL, 0, 0, 0, 0, NULL, 'sgcc_super_admin', '2020-12-03 14:21:07', 'sgcc_super_admin', '2020-12-05 20:45:20', 0, 0, NULL, 0);
INSERT INTO `sys_permission` VALUES ('ff8080817608b7940176230c9865065a', '1310401085768208385', '数据中心列表', NULL, NULL, NULL, NULL, 2, 'assetPageDatacenter', '0', NULL, 0, NULL, 0, 0, 0, 0, NULL, 'sgcc_super_admin', '2020-12-03 14:21:07', 'sgcc_super_admin', '2020-12-05 20:45:20', 0, 0, NULL, 0);
INSERT INTO `sys_permission` VALUES ('ff8080817608b7940176230cf6ba0661', '1310401085768208385', '查询', NULL, NULL, NULL, NULL, 2, 'modelPageDeviceScore', '0', NULL, 0, NULL, 0, 0, 0, 0, NULL, 'sgcc_super_admin', '2020-12-03 14:21:07', 'sgcc_super_admin', '2020-12-05 20:45:20', 0, 0, NULL, 0);
INSERT INTO `sys_permission` VALUES ('ff8080817608b7940176230d98b80668', '1310401229901271042', '数据中心列表', NULL, NULL, NULL, NULL, 2, 'assetPageDatacenter', '0', NULL, 0, NULL, 0, 0, 0, 0, NULL, 'sgcc_super_admin', '2020-12-03 14:21:07', 'sgcc_super_admin', '2020-12-05 20:45:20', 0, 0, NULL, 0);
INSERT INTO `sys_permission` VALUES ('ff8080817608b7940176230e5b8e066f', '1310401229901271042', '查询', NULL, NULL, NULL, NULL, 2, 'modelPageDeviceScore', '0', NULL, 0, NULL, 0, 0, 0, 0, NULL, 'sgcc_super_admin', '2020-12-03 14:21:07', 'sgcc_super_admin', '2020-12-05 20:45:20', 0, 0, NULL, 0);
INSERT INTO `sys_permission` VALUES ('ff8080817608b7940176230f52030676', '1310401446956503042', '数据中心列表', NULL, NULL, NULL, NULL, 2, 'assetPageDatacenter', '0', NULL, 0, NULL, 0, 0, 0, 0, NULL, 'sgcc_super_admin', '2020-12-03 14:21:07', 'sgcc_super_admin', '2020-12-05 20:45:20', 0, 0, NULL, 0);
INSERT INTO `sys_permission` VALUES ('ff8080817608b794017623127a25067f', '1310401446956503042', '查询', NULL, NULL, NULL, NULL, 2, 'modelPageDeviceScore', '0', NULL, 0, NULL, 0, 0, 0, 0, NULL, 'sgcc_super_admin', '2020-12-03 14:21:07', 'sgcc_super_admin', '2020-12-05 20:45:20', 0, 0, NULL, 0);
INSERT INTO `sys_permission` VALUES ('ff8080817608b794017623131dc70686', '1310401620621660162', '数据中心列表', NULL, NULL, NULL, NULL, 2, 'assetPageDatacenter', '0', NULL, 0, NULL, 0, 0, 0, 0, NULL, 'sgcc_super_admin', '2020-12-03 14:21:07', 'sgcc_super_admin', '2020-12-05 20:45:20', 0, 0, NULL, 0);
INSERT INTO `sys_permission` VALUES ('ff8080817608b794017623135c20068d', '1310401620621660162', '查询', NULL, NULL, NULL, NULL, 2, 'fmPage', '0', NULL, 0, NULL, 0, 0, 0, 0, NULL, 'sgcc_super_admin', '2020-12-03 14:21:07', 'sgcc_super_admin', '2020-12-05 20:45:20', 0, 0, NULL, 0);
INSERT INTO `sys_permission` VALUES ('ff8080817608b79401762313dc060694', '1310401620621660162', '设置', NULL, NULL, NULL, NULL, 2, 'fmSetStrategy', '0', NULL, 0, NULL, 0, 0, 0, 0, NULL, 'sgcc_super_admin', '2020-12-03 14:21:07', 'sgcc_super_admin', '2020-12-05 20:45:20', 0, 0, NULL, 0);
INSERT INTO `sys_permission` VALUES ('ff8080817608b794017623146d78069b', '1310401771595632641', '数据中心列表', NULL, NULL, NULL, NULL, 2, 'assetPageDatacenter', '0', NULL, 0, NULL, 0, 0, 0, 0, NULL, 'sgcc_super_admin', '2020-12-03 14:21:07', 'sgcc_super_admin', '2020-12-05 20:45:20', 0, 0, NULL, 0);
INSERT INTO `sys_permission` VALUES ('ff8080817608b79401762316aaf906a2', '1310401771595632641', '动环节能设置', NULL, NULL, NULL, NULL, 2, 'assetUpdateDatacenter', '0', NULL, 0, NULL, 0, 0, 0, 0, NULL, 'sgcc_super_admin', '2020-12-03 14:21:07', 'sgcc_super_admin', '2020-12-05 20:45:20', 0, 0, NULL, 0);
INSERT INTO `sys_permission` VALUES ('ff8080817608b794017623174dc606a9', '1310401950704996353', '数据中心列表', NULL, NULL, NULL, NULL, 2, 'assetPageDatacenter', '0', NULL, 0, NULL, 0, 0, 0, 0, NULL, 'sgcc_super_admin', '2020-12-03 14:21:07', 'sgcc_super_admin', '2020-12-05 20:45:20', 0, 0, NULL, 0);
INSERT INTO `sys_permission` VALUES ('ff8080817608b79401762317d07206b0', '1310401950704996353', '查询', NULL, NULL, NULL, NULL, 2, 'modelPageDeviceScore', '0', NULL, 0, NULL, 0, 0, 0, 0, NULL, 'sgcc_super_admin', '2020-12-03 14:21:07', 'sgcc_super_admin', '2020-12-05 20:45:20', 0, 0, NULL, 0);
INSERT INTO `sys_permission` VALUES ('ff8080817608b7940176231c8cb806bb', '1310412463300153345', '查询', NULL, NULL, NULL, NULL, 2, 'apiService', '0', NULL, 0, NULL, 0, 0, 0, 0, NULL, 'sgcc_super_admin', '2020-12-03 14:21:07', 'sgcc_super_admin', '2020-12-05 20:45:20', 0, 0, NULL, 0);
INSERT INTO `sys_permission` VALUES ('ff8080817608b7940176231cf7f306c2', '1310412463300153345', '查询1', NULL, NULL, NULL, NULL, 2, 'apiRole', '0', NULL, 0, NULL, 0, 0, 0, 0, NULL, 'sgcc_super_admin', '2020-12-03 14:21:07', 'sgcc_super_admin', '2020-12-05 20:45:20', 0, 0, NULL, 0);
INSERT INTO `sys_permission` VALUES ('ff8080817608b7940176231e34f206c9', '1301427338168242177', '数据中心列表', NULL, NULL, NULL, NULL, 2, 'assetPageDatacenter', '0', NULL, 0, NULL, 0, 0, 0, 0, NULL, 'sgcc_super_admin', '2020-12-03 14:21:07', 'sgcc_super_admin', '2020-12-05 20:45:20', 0, 0, NULL, 0);
INSERT INTO `sys_permission` VALUES ('ff8080817608b79401762320acf806d0', '1301427916441128962', '数据中心列表', NULL, NULL, NULL, NULL, 2, 'assetPageDatacenter', '0', NULL, 0, NULL, 0, 0, 0, 0, NULL, 'sgcc_super_admin', '2020-12-03 14:21:07', 'sgcc_super_admin', '2020-12-05 20:45:20', 0, 0, NULL, 0);
INSERT INTO `sys_permission` VALUES ('ff8080817608b79401762320f71806d7', '1301427916441128962', '查询', NULL, NULL, NULL, NULL, 2, 'appPage', '0', NULL, 0, NULL, 0, 0, 0, 0, NULL, 'sgcc_super_admin', '2020-12-03 14:21:07', 'sgcc_super_admin', '2020-12-05 20:45:20', 0, 0, NULL, 0);
INSERT INTO `sys_permission` VALUES ('ff8080817608b79401762321ae9706de', '1301428144502214658', '查询', NULL, NULL, NULL, NULL, 2, 'schedulerJobPage', '0', NULL, 0, NULL, 0, 0, 0, 0, NULL, 'sgcc_super_admin', '2020-12-03 14:21:07', 'sgcc_super_admin', '2020-12-05 20:45:20', 0, 0, NULL, 0);
INSERT INTO `sys_permission` VALUES ('ff8080817608b79401762322883806e5', '1301428144502214658', '调度过程监控查看', NULL, NULL, NULL, NULL, 2, 'schedulerStepList', '0', NULL, 0, NULL, 0, 0, 0, 0, NULL, 'sgcc_super_admin', '2020-12-03 14:21:07', 'sgcc_super_admin', '2020-12-05 20:45:20', 0, 0, NULL, 0);
INSERT INTO `sys_permission` VALUES ('ff8080817608b79401762323212606ec', '1304321289430765570', '查询', NULL, NULL, NULL, NULL, 2, 'schedulerJobPage', '0', NULL, 0, NULL, 0, 0, 0, 0, NULL, 'sgcc_super_admin', '2020-12-03 14:21:07', 'sgcc_super_admin', '2020-12-05 20:45:20', 0, 0, NULL, 0);
INSERT INTO `sys_permission` VALUES ('ff8080817608b7940176232506e006f5', '1331872709646225410', '提交', NULL, NULL, NULL, NULL, 2, 'modelSetModelFactors', '0', NULL, 0, NULL, 0, 0, 0, 0, NULL, 'sgcc_super_admin', '2020-12-03 14:21:07', 'sgcc_super_admin', '2020-12-05 20:45:20', 0, 0, NULL, 0);
INSERT INTO `sys_permission` VALUES ('ff8080817608b79401762325ce8c06fc', '1310418138898108418', '数据中心列表', NULL, NULL, NULL, NULL, 2, 'assetPageDatacenter', '0', NULL, 0, NULL, 0, 0, 0, 0, NULL, 'sgcc_super_admin', '2020-12-03 14:21:07', 'sgcc_super_admin', '2020-12-05 20:45:20', 0, 0, NULL, 0);
INSERT INTO `sys_permission` VALUES ('ff8080817608b794017623268a7b0703', '1310418138898108418', '查询服务器', NULL, NULL, NULL, NULL, 2, 'analysisPageNodeFitter', '0', NULL, 0, NULL, 0, 0, 0, 0, NULL, 'sgcc_super_admin', '2020-12-03 14:21:07', 'sgcc_super_admin', '2020-12-05 20:45:20', 0, 0, NULL, 0);
INSERT INTO `sys_permission` VALUES ('ff8080817608b79401762326f887070a', '1310418138898108418', '查询空调', NULL, NULL, NULL, NULL, 2, 'analysisPageAirFitter', '0', NULL, 0, NULL, 0, 0, 0, 0, NULL, 'sgcc_super_admin', '2020-12-03 14:21:07', 'sgcc_super_admin', '2020-12-05 20:45:20', 0, 0, NULL, 0);
INSERT INTO `sys_permission` VALUES ('ff8080817608b7940176232897ef0711', '1310418138898108418', '拟合', NULL, NULL, NULL, NULL, 2, 'analysisDoFitter', '0', NULL, 0, NULL, 0, 0, 0, 0, NULL, 'sgcc_super_admin', '2020-12-03 14:21:07', 'sgcc_super_admin', '2020-12-05 20:45:20', 0, 0, NULL, 0);
INSERT INTO `sys_permission` VALUES ('ff8080817608b7940176232901020718', '1310418138898108418', '仿真', NULL, NULL, NULL, NULL, 2, 'analysisDoSimulate', '0', NULL, 0, NULL, 0, 0, 0, 0, NULL, 'sgcc_super_admin', '2020-12-03 14:21:07', 'sgcc_super_admin', '2020-12-05 20:45:20', 0, 0, NULL, 0);
INSERT INTO `sys_permission` VALUES ('ff8080817608b7940176232accf3071f', '1310422936447881217', '运行', NULL, NULL, NULL, NULL, 2, 'analysisDoFitter', '0', NULL, 0, NULL, 0, 0, 0, 0, NULL, 'sgcc_super_admin', '2020-12-03 14:21:07', 'sgcc_super_admin', '2020-12-05 20:45:20', 0, 0, NULL, 0);
INSERT INTO `sys_permission` VALUES ('ff8080817608b7940176232b74900726', '1310432505559519233', '运行', NULL, NULL, NULL, NULL, 2, 'analysisDoSimulate', '0', NULL, 0, NULL, 0, 0, 0, 0, NULL, 'sgcc_super_admin', '2020-12-03 14:21:07', 'sgcc_super_admin', '2020-12-05 20:45:20', 0, 0, NULL, 0);
INSERT INTO `sys_permission` VALUES ('ff8080817608b7940176232e2a7e072d', '1310432897492062210', '历史最优策略建议', NULL, NULL, NULL, NULL, 2, 'analysisOptimum', '0', NULL, 0, NULL, 0, 0, 0, 0, NULL, 'sgcc_super_admin', '2020-12-03 14:21:07', 'sgcc_super_admin', '2020-12-05 20:45:20', 0, 0, NULL, 0);
INSERT INTO `sys_permission` VALUES ('ff8080817608b7940176232eddbf0734', '1310432897492062210', '历史最优策略仿真', NULL, NULL, NULL, NULL, 2, 'analysisPageNodeSimulate', '0', NULL, 0, NULL, 0, 0, 0, 0, NULL, 'sgcc_super_admin', '2020-12-03 14:21:07', 'sgcc_super_admin', '2020-12-05 20:45:20', 0, 0, NULL, 0);
INSERT INTO `sys_permission` VALUES ('ff8080817608b7940176232fe543073b', 'ff8080817608b7940176232e2a7e072d', '查询', NULL, NULL, NULL, NULL, 2, 'analysisPageNodeHistory', '0', NULL, 0, NULL, 0, 0, 0, 0, NULL, 'sgcc_super_admin', '2020-12-03 14:21:07', 'sgcc_super_admin', '2020-12-05 20:45:20', 0, 0, NULL, 0);
INSERT INTO `sys_permission` VALUES ('ff8080817608b7940176233090fe0742', '1310433274404802561', '空调亚健康分析', NULL, NULL, NULL, NULL, 2, 'subhealthPageAir', '0', NULL, 0, NULL, 0, 0, 0, 0, NULL, 'sgcc_super_admin', '2020-12-03 14:21:07', 'sgcc_super_admin', '2020-12-05 20:45:20', 0, 0, NULL, 0);
INSERT INTO `sys_permission` VALUES ('ff8080817608b79401762330faf20749', '1310433274404802561', '机房亚健康分析', NULL, NULL, NULL, NULL, 2, 'subhealthListDatacenter', '0', NULL, 0, NULL, 0, 0, 0, 0, NULL, 'sgcc_super_admin', '2020-12-03 14:21:07', 'sgcc_super_admin', '2020-12-05 20:45:20', 0, 0, NULL, 0);
INSERT INTO `sys_permission` VALUES ('ff8080817608b7940176233197440750', '1310433274404802561', 'UPS亚健康分析', NULL, NULL, NULL, NULL, 2, 'subhealthPageUps', '0', NULL, 0, NULL, 0, 0, 0, 0, NULL, 'sgcc_super_admin', '2020-12-03 14:21:07', 'sgcc_super_admin', '2020-12-05 20:45:20', 0, 0, NULL, 0);
INSERT INTO `sys_permission` VALUES ('ff8080817608b79401762332103a0757', '1310433274404802561', '亚健康阈值设置', NULL, NULL, NULL, NULL, 1, 'f0404_3', '0', NULL, 0, NULL, 0, 0, 0, 0, NULL, 'sgcc_super_admin', '2020-12-03 14:21:07', 'sgcc_super_admin', '2020-12-05 20:45:20', 0, 0, NULL, 0);
INSERT INTO `sys_permission` VALUES ('ff8080817608b7940176233441100760', 'ff8080817608b79401762332103a0757', '空调亚健康阈值设置 ', NULL, NULL, NULL, NULL, 2, 'subhealthPageAir', '0', NULL, 0, NULL, 0, 0, 0, 0, NULL, 'sgcc_super_admin', '2020-12-03 14:21:07', 'sgcc_super_admin', '2020-12-05 20:45:20', 0, 0, NULL, 0);
INSERT INTO `sys_permission` VALUES ('ff8080817608b79401762334aa6c0767', 'ff8080817608b79401762332103a0757', '机房亚健康阈值设置 ', NULL, NULL, NULL, NULL, 2, 'subhealthPageDatacenter', '0', NULL, 0, NULL, 0, 0, 0, 0, NULL, 'sgcc_super_admin', '2020-12-03 14:21:07', 'sgcc_super_admin', '2020-12-05 20:45:20', 0, 0, NULL, 0);
INSERT INTO `sys_permission` VALUES ('ff8080817608b794017623351d64076e', 'ff8080817608b79401762332103a0757', 'UPS亚健康阈值设置', NULL, NULL, NULL, NULL, 2, 'subhealthPageUps', '0', NULL, 0, NULL, 0, 0, 0, 0, NULL, 'sgcc_super_admin', '2020-12-03 14:21:07', 'sgcc_super_admin', '2020-12-05 20:45:20', 0, 0, NULL, 0);
INSERT INTO `sys_permission` VALUES ('ff8080817608b7940176233786410775', 'ff8080817608b79401762332103a0757', '子选项的选择', NULL, NULL, NULL, NULL, 1, 'f0404_3_1', '0', NULL, 0, NULL, 0, 0, 0, 0, NULL, 'sgcc_super_admin', '2020-12-03 14:21:07', 'sgcc_super_admin', '2020-12-05 20:45:20', 0, 0, NULL, 0);
INSERT INTO `sys_permission` VALUES ('ff8080817608b79401762338d44f077c', 'ff8080817608b7940176233786410775', '阈值告警级别', NULL, NULL, NULL, NULL, 2, 'subhealthListA_lertLevel', '0', NULL, 0, NULL, 0, 0, 0, 0, NULL, 'sgcc_super_admin', '2020-12-03 14:21:07', 'sgcc_super_admin', '2020-12-05 20:45:20', 0, 0, NULL, 0);
INSERT INTO `sys_permission` VALUES ('ff8080817608b7940176233ca05a0785', 'ff8080817608b7940176233786410775', '阈值告警项', NULL, NULL, NULL, NULL, 2, 'subhealthListThresholdMeter', '0', NULL, 0, NULL, 0, 0, 0, 0, NULL, 'sgcc_super_admin', '2020-12-03 14:21:07', 'sgcc_super_admin', '2020-12-05 20:45:20', 0, 0, NULL, 0);
INSERT INTO `sys_permission` VALUES ('ff8080817608b7940176233d3687078c', 'ff8080817608b7940176233786410775', '阈值列表', NULL, NULL, NULL, NULL, 2, 'subhealthListThreshold', '0', NULL, 0, NULL, 0, 0, 0, 0, NULL, 'sgcc_super_admin', '2020-12-03 14:21:07', 'sgcc_super_admin', '2020-12-05 20:45:20', 0, 0, NULL, 0);
INSERT INTO `sys_permission` VALUES ('ff8080817608b79401762346c7400795', '1310433274404802561', '亚健康告警', NULL, NULL, NULL, NULL, 1, 'f0404_4', '0', NULL, 0, NULL, 0, 0, 0, 0, NULL, 'sgcc_super_admin', '2020-12-03 14:21:07', 'sgcc_super_admin', '2020-12-05 20:45:20', 0, 0, NULL, 0);
INSERT INTO `sys_permission` VALUES ('ff8080817608b79401762350c02007ae', 'ff8080817608b79401762346c7400795', '告警统计', NULL, NULL, NULL, NULL, 2, 'subhealthA_lertStatistic', '0', NULL, 0, NULL, 0, 0, 0, 0, NULL, 'sgcc_super_admin', '2020-12-03 14:21:07', 'sgcc_super_admin', '2020-12-05 20:45:20', 0, 0, NULL, 0);
INSERT INTO `sys_permission` VALUES ('ff8080817608b7940176235acdcc07b5', 'ff8080817608b79401762346c7400795', '阈值告警级别', NULL, NULL, NULL, NULL, 2, 'ubhealthListA_lertLevel', '0', NULL, 0, NULL, 0, 0, 0, 0, NULL, 'sgcc_super_admin', '2020-12-03 14:21:07', 'sgcc_super_admin', '2020-12-05 20:45:20', 0, 0, NULL, 0);
INSERT INTO `sys_permission` VALUES ('ff8080817608b7940176235b839807bc', 'ff8080817608b79401762346c7400795', '查询', NULL, NULL, NULL, NULL, 2, 'subhealthPageA_lert', '0', NULL, 0, NULL, 0, 0, 0, 0, NULL, 'sgcc_super_admin', '2020-12-03 14:21:07', 'sgcc_super_admin', '2020-12-05 20:45:20', 0, 0, NULL, 0);
INSERT INTO `sys_permission` VALUES ('ff8080817608b7940176237f207007c3', '1310413060296413185', '业务应用画像业务特点管理', NULL, NULL, NULL, NULL, 2, 'profilePageFeature', '0', NULL, 0, NULL, 0, 0, 0, 0, NULL, 'sgcc_super_admin', '2020-12-03 14:21:07', 'sgcc_super_admin', '2020-12-05 20:45:20', 0, 0, NULL, 0);
INSERT INTO `sys_permission` VALUES ('ff8080817608b7940176237f842307ca', '1310413263179091969', '业务应用画像应用场景管理', NULL, NULL, NULL, NULL, 2, 'profilePageScene', '0', NULL, 0, NULL, 0, 0, 0, 0, NULL, 'sgcc_super_admin', '2020-12-03 14:21:07', 'sgcc_super_admin', '2020-12-05 20:45:20', 0, 0, NULL, 0);
INSERT INTO `sys_permission` VALUES ('ff8080817608b7940176238091e707d1', '1331872709646225410', '调度模型优化设置', NULL, NULL, NULL, NULL, 2, 'modelGetModelFactors', '0', NULL, 0, NULL, 0, 0, 0, 0, NULL, 'sgcc_super_admin', '2020-12-03 14:21:07', 'sgcc_super_admin', '2020-12-05 20:45:20', 0, 0, NULL, 0);
INSERT INTO `sys_permission` VALUES ('ff8080817608b79401762642cebb07d8', '1301077101432016897', '数据中心列表', NULL, NULL, NULL, NULL, 2, 'assetPageDatacenter', '0', NULL, 0, NULL, 0, 0, 0, 0, NULL, 'sgcc_super_admin', '2020-12-03 14:21:06', 'sgcc_super_admin', '2020-12-05 20:45:19', 0, 0, NULL, 0);
INSERT INTO `sys_permission` VALUES ('ff8080817608b794017626441d2207df', 'ff8080817608b794017622aa75b10456', '数据中心列表', NULL, NULL, NULL, NULL, 2, 'assetPageDatacenter', '0', NULL, 0, NULL, 0, 0, 0, 0, NULL, 'sfadmin', '2020-12-03 10:11:19', 'sgcc_super_admin', '2020-12-05 20:45:19', 0, 0, NULL, 0);
INSERT INTO `sys_permission` VALUES ('ff8080817608b79401762b92e7130b08', '1301000852273631234', '易分析日志', '/f0502', NULL, NULL, NULL, 0, NULL, '0', NULL, 0, NULL, 0, 0, 0, 0, NULL, 'sgcc_super_admin', '2020-12-04 10:31:41', 'sgcc_super_admin', '2020-12-05 20:45:20', 0, 0, NULL, 0);
INSERT INTO `sys_permission` VALUES ('ff8080817608b79401762b939efd0b0d', '1301000852273631234', '日志统计', '/f0503', NULL, NULL, NULL, 0, NULL, '0', NULL, 0, NULL, 0, 0, 0, 0, NULL, 'sgcc_super_admin', '2020-12-04 10:31:41', 'sgcc_super_admin', '2020-12-05 20:45:20', 0, 0, NULL, 0);
INSERT INTO `sys_permission` VALUES ('ff8080817608b79401762b94184a0b12', '1301000852273631234', '日志配置', '/f0504', NULL, NULL, NULL, 0, NULL, '0', NULL, 0, NULL, 0, 0, 0, 0, NULL, 'sgcc_super_admin', '2020-12-04 10:31:41', 'sgcc_super_admin', '2020-12-05 20:45:20', 0, 0, NULL, 0);
INSERT INTO `sys_permission` VALUES ('ff8080817608b79401762b949a9f0b17', '1301000852273631234', '数据中心统计', '/f0505', NULL, NULL, NULL, 0, NULL, '0', NULL, 0, NULL, 0, 0, 0, 0, NULL, 'sgcc_super_admin', '2020-12-04 10:31:41', 'sgcc_super_admin', '2020-12-05 20:45:20', 0, 0, NULL, 0);

-- ----------------------------
-- Table structure for sys_permission_data_rule
-- ----------------------------
DROP TABLE IF EXISTS `sys_permission_data_rule`;
CREATE TABLE `sys_permission_data_rule`  (
  `id` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'ID',
  `permission_id` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '菜单ID',
  `rule_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '规则名称',
  `rule_column` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '字段',
  `rule_conditions` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '条件',
  `rule_value` varchar(300) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '规则值',
  `status` varchar(3) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '权限有效状态1有0否',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `create_by` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '修改时间',
  `update_by` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '修改人',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `index_fucntionid`(`permission_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of sys_permission_data_rule
-- ----------------------------
INSERT INTO `sys_permission_data_rule` VALUES ('1260935285157511170', '4148ec82b6acd69f470bea75fe41c357', 'createBy', 'createBy', '=', '#{sys_user_code}', '0', '2020-05-14 22:09:34', 'jeecg', '2020-05-14 22:13:52', 'admin');
INSERT INTO `sys_permission_data_rule` VALUES ('1260936345293012993', '4148ec82b6acd69f470bea75fe41c357', '年龄', 'age', '>', '20', '1', '2020-05-14 22:13:46', 'admin', NULL, NULL);
INSERT INTO `sys_permission_data_rule` VALUES ('1260937192290762754', '4148ec82b6acd69f470bea75fe41c357', 'sysOrgCode', 'sysOrgCode', 'RIGHT_LIKE', '#{sys_org_code}', '1', '2020-05-14 22:17:08', 'admin', NULL, NULL);
INSERT INTO `sys_permission_data_rule` VALUES ('32b62cb04d6c788d9d92e3ff5e66854e', '8d4683aacaa997ab86b966b464360338', '000', '00', '!=', '00', '1', '2019-04-02 18:36:08', 'admin', NULL, NULL);
INSERT INTO `sys_permission_data_rule` VALUES ('40283181614231d401614234fe670003', '40283181614231d401614232cd1c0001', 'createBy', 'createBy', '=', '#{sys_user_code}', '1', '2018-01-29 21:57:04', 'admin', NULL, NULL);
INSERT INTO `sys_permission_data_rule` VALUES ('4028318161424e730161424fca6f0004', '4028318161424e730161424f61510002', 'createBy', 'createBy', '=', '#{sys_user_code}', '1', '2018-01-29 22:26:20', 'admin', NULL, NULL);
INSERT INTO `sys_permission_data_rule` VALUES ('402880e6487e661a01487e732c020005', '402889fb486e848101486e93a7c80014', 'SYS_ORG_CODE', 'SYS_ORG_CODE', 'LIKE', '010201%', '1', '2014-09-16 20:32:30', 'admin', NULL, NULL);
INSERT INTO `sys_permission_data_rule` VALUES ('402880e6487e661a01487e8153ee0007', '402889fb486e848101486e93a7c80014', 'create_by', 'create_by', '', '#{SYS_USER_CODE}', '1', '2014-09-16 20:47:57', 'admin', NULL, NULL);
INSERT INTO `sys_permission_data_rule` VALUES ('402880ec5ddec439015ddf9225060038', '40288088481d019401481d2fcebf000d', '复杂关系', '', 'USE_SQL_RULES', 'name like \'%张%\' or age > 10', '1', NULL, NULL, '2017-08-14 15:10:25', 'demo');
INSERT INTO `sys_permission_data_rule` VALUES ('402880ec5ddfdd26015ddfe3e0570011', '4028ab775dca0d1b015dca3fccb60016', '复杂sql配置', '', 'USE_SQL_RULES', 'table_name like \'%test%\' or is_tree = \'Y\'', '1', NULL, NULL, '2017-08-14 16:38:55', 'demo');
INSERT INTO `sys_permission_data_rule` VALUES ('402880f25b1e2ac7015b1e5fdebc0012', '402880f25b1e2ac7015b1e5cdc340010', '只能看自己数据', 'create_by', '=', '#{sys_user_code}', '1', '2017-03-30 16:40:51', 'admin', NULL, NULL);
INSERT INTO `sys_permission_data_rule` VALUES ('402881875b19f141015b19f8125e0014', '40288088481d019401481d2fcebf000d', '可看下属业务数据', 'sys_org_code', 'LIKE', '#{sys_org_code}', '1', NULL, NULL, '2017-08-14 15:04:32', 'demo');
INSERT INTO `sys_permission_data_rule` VALUES ('402881e45394d66901539500a4450001', '402881e54df73c73014df75ab670000f', 'sysCompanyCode', 'sysCompanyCode', '=', '#{SYS_COMPANY_CODE}', '1', '2016-03-21 01:09:21', 'admin', NULL, NULL);
INSERT INTO `sys_permission_data_rule` VALUES ('402881e45394d6690153950177cb0003', '402881e54df73c73014df75ab670000f', 'sysOrgCode', 'sysOrgCode', '=', '#{SYS_ORG_CODE}', '1', '2016-03-21 01:10:15', 'admin', NULL, NULL);
INSERT INTO `sys_permission_data_rule` VALUES ('402881e56266f43101626727aff60067', '402881e56266f43101626724eb730065', '销售自己看自己的数据', 'createBy', '=', '#{sys_user_code}', '1', '2018-03-27 19:11:16', 'admin', NULL, NULL);
INSERT INTO `sys_permission_data_rule` VALUES ('402881e56266f4310162672fb1a70082', '402881e56266f43101626724eb730065', '销售经理看所有下级数据', 'sysOrgCode', 'LIKE', '#{sys_org_code}', '1', '2018-03-27 19:20:01', 'admin', NULL, NULL);
INSERT INTO `sys_permission_data_rule` VALUES ('402881e56266f431016267387c9f0088', '402881e56266f43101626724eb730065', '只看金额大于1000的数据', 'money', '>=', '1000', '1', '2018-03-27 19:29:37', 'admin', NULL, NULL);
INSERT INTO `sys_permission_data_rule` VALUES ('402881f3650de25101650dfb5a3a0010', '402881e56266f4310162671d62050044', '22', '', 'USE_SQL_RULES', '22', '1', '2018-08-06 14:45:01', 'admin', NULL, NULL);
INSERT INTO `sys_permission_data_rule` VALUES ('402889fb486e848101486e913cd6000b', '402889fb486e848101486e8e2e8b0007', 'userName', 'userName', '=', 'admin', '1', '2014-09-13 18:31:25', 'admin', NULL, NULL);
INSERT INTO `sys_permission_data_rule` VALUES ('402889fb486e848101486e98d20d0016', '402889fb486e848101486e93a7c80014', 'title', 'title', '=', '12', '1', NULL, NULL, '2014-09-13 22:18:22', 'scott');
INSERT INTO `sys_permission_data_rule` VALUES ('402889fe47fcb29c0147fcb6b6220001', '8a8ab0b246dc81120146dc8180fe002b', '12', '12', '>', '12', '1', '2014-08-22 15:55:38', '8a8ab0b246dc81120146dc8181950052', NULL, NULL);
INSERT INTO `sys_permission_data_rule` VALUES ('4028ab775dca0d1b015dca4183530018', '4028ab775dca0d1b015dca3fccb60016', '表名限制', 'isDbSynch', '=', 'Y', '1', NULL, NULL, '2017-08-14 16:43:45', 'demo');
INSERT INTO `sys_permission_data_rule` VALUES ('4028ef815595a881015595b0ccb60001', '40288088481d019401481d2fcebf000d', '限只能看自己', 'create_by', '=', '#{sys_user_code}', '1', NULL, NULL, '2017-08-14 15:03:56', 'demo');
INSERT INTO `sys_permission_data_rule` VALUES ('4028ef81574ae99701574aed26530005', '4028ef81574ae99701574aeb97bd0003', '用户名', 'userName', '!=', 'admin', '1', '2016-09-21 12:07:18', 'admin', NULL, NULL);
INSERT INTO `sys_permission_data_rule` VALUES ('f852d85d47f224990147f2284c0c0005', NULL, '小于', 'test', '<=', '11', '1', '2014-08-20 14:43:52', '8a8ab0b246dc81120146dc8181950052', NULL, NULL);

-- ----------------------------
-- Table structure for sys_position
-- ----------------------------
DROP TABLE IF EXISTS `sys_position`;
CREATE TABLE `sys_position`  (
  `id` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `code` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '职务编码',
  `name` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '职务名称',
  `post_rank` varchar(2) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '职级',
  `company_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '公司id',
  `create_by` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '创建人',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '修改人',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '修改时间',
  `sys_org_code` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '组织机构编码',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `uniq_code`(`code`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of sys_position
-- ----------------------------
INSERT INTO `sys_position` VALUES ('1185040064792571906', 'devleader', '研发部经理', '2', NULL, 'admin', '2019-10-18 11:49:03', 'admin', '2020-02-23 22:55:42', 'A01');
INSERT INTO `sys_position` VALUES ('1256485574212153345', '总经理', 'laozong', '5', NULL, 'admin', '2020-05-02 15:28:00', 'admin', '2020-05-02 15:28:03', '北京国炬公司');

-- ----------------------------
-- Table structure for sys_quartz_job
-- ----------------------------
DROP TABLE IF EXISTS `sys_quartz_job`;
CREATE TABLE `sys_quartz_job`  (
  `id` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `create_by` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '创建人',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `del_flag` int(1) NULL DEFAULT NULL COMMENT '删除状态',
  `update_by` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '修改人',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '修改时间',
  `job_class_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '任务类名',
  `cron_expression` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'cron表达式',
  `parameter` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '参数',
  `description` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '描述',
  `status` int(1) NULL DEFAULT NULL COMMENT '状态 0正常 -1停止',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `uniq_job_class_name`(`job_class_name`) USING BTREE
) ENGINE = MyISAM CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_quartz_job
-- ----------------------------
INSERT INTO `sys_quartz_job` VALUES ('df26ecacf0f75d219d746750fe84bbee', NULL, NULL, 0, 'admin', '2020-05-02 15:40:35', 'org.jeecg.modules.quartz.job.SampleParamJob', '0/1 * * * * ?', 'scott', '带参测试 后台将每隔1秒执行输出日志', -1);
INSERT INTO `sys_quartz_job` VALUES ('a253cdfc811d69fa0efc70d052bc8128', 'admin', '2019-03-30 12:44:48', 0, 'admin', '2020-05-02 15:48:49', 'org.jeecg.modules.quartz.job.SampleJob', '0/1 * * * * ?', NULL, NULL, -1);
INSERT INTO `sys_quartz_job` VALUES ('5b3d2c087ad41aa755fc4f89697b01e7', 'admin', '2019-04-11 19:04:21', 0, 'admin', '2020-05-02 15:48:48', 'org.jeecg.modules.message.job.SendMsgJob', '0/50 * * * * ? *', NULL, NULL, 0);

-- ----------------------------
-- Table structure for sys_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_role`;
CREATE TABLE `sys_role`  (
  `id` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '主键id',
  `role_name` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '角色名称',
  `role_code` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '角色编码',
  `description` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '描述',
  `create_by` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '创建人',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '更新人',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `uniq_sys_role_role_code`(`role_code`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '角色表' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of sys_role
-- ----------------------------
INSERT INTO `sys_role` VALUES ('1169504891467464705', '第三方登录角色', 'third_role', '第三方登录角色', 'admin', '2019-09-05 14:57:49', 'admin', '2020-05-02 18:20:58');
INSERT INTO `sys_role` VALUES ('1260924539346472962', '财务出纳', 'caiwu', NULL, 'admin', '2020-05-14 21:26:52', NULL, NULL);
INSERT INTO `sys_role` VALUES ('1301035310028689409', 'guowang', 'guowang', 'guowang', 'admin', '2020-09-02 13:52:44', NULL, NULL);
INSERT INTO `sys_role` VALUES ('40288cd65d556b7d015d565e660d0075', 'ISC通用角色模版', 'aaa', NULL, NULL, '2020-11-18 17:03:33', NULL, '2020-11-23 11:14:06');
INSERT INTO `sys_role` VALUES ('8a6e50b54dc268dc014dc277fba30114', '系统管理员', 'xtadmin', NULL, NULL, '2020-11-23 11:11:08', NULL, NULL);
INSERT INTO `sys_role` VALUES ('8a6e50b54dc268dc014dc2784aff0115', '身份管理员', 'sfadmin', NULL, NULL, '2020-11-18 17:03:33', NULL, '2020-12-03 20:01:08');
INSERT INTO `sys_role` VALUES ('8a6e50b54dc268dc014dc27926170117', '审计管理员', 'sjadmin', NULL, NULL, '2020-11-23 11:13:48', NULL, '2020-11-23 11:14:06');
INSERT INTO `sys_role` VALUES ('e51758fa916c881624b046d26bd09230', '人力资源部', 'hr', NULL, 'admin', '2019-01-21 18:07:24', 'admin', '2019-10-18 11:39:43');
INSERT INTO `sys_role` VALUES ('ee8626f80f7c2619917b6236f3a7f02b', '临时角色', 'test', '这是新建的临时角色123', NULL, '2018-12-20 10:59:04', 'admin', '2019-02-19 15:08:37');
INSERT INTO `sys_role` VALUES ('f6817f48af4fb3af11b9e8bf182f618b', '管理员', 'admin', '管理员', NULL, '2018-12-21 18:03:39', 'admin', '2019-05-20 11:40:26');
INSERT INTO `sys_role` VALUES ('ff80808175d52fbb0175d5f2f7120028', '云计算业务角色', 'putong', NULL, NULL, '2020-11-23 11:11:08', NULL, NULL);
INSERT INTO `sys_role` VALUES ('ff80808175d52fbb0175d5f90670003d', 'shengque', 'sheng', NULL, NULL, '2020-11-20 09:11:57', NULL, '2020-12-03 10:34:25');
INSERT INTO `sys_role` VALUES ('ff80808175dee8890175e31b0c7d001b', '云计算普通用户', 'sysuser', NULL, NULL, '2020-11-20 09:11:57', NULL, '2020-12-03 09:20:53');
INSERT INTO `sys_role` VALUES ('ff8080817608b794017621407ae301ff', '审计管理员', 'sjyadmin', NULL, NULL, '2020-12-02 12:34:32', NULL, '2020-12-02 20:08:36');
INSERT INTO `sys_role` VALUES ('ff8080817608b79401762141af3c0204', '业务配置员', 'ywyadmin', NULL, NULL, '2020-12-02 11:33:09', NULL, '2020-12-03 10:34:25');
INSERT INTO `sys_role` VALUES ('ff8080817608b7940176214221730206', '业务用户', 'ywyuser', NULL, NULL, '2020-12-02 19:58:15', NULL, '2020-12-02 20:03:55');
INSERT INTO `sys_role` VALUES ('ff8080817608b79401762692a4510813', '审计管理员', 'sgcc_audit_admin', NULL, NULL, '2020-12-03 14:42:38', NULL, '2020-12-03 20:14:55');
INSERT INTO `sys_role` VALUES ('ff8080817608b7940176269360550815', '业务配置员', 'sgcc_config_admin', NULL, NULL, '2020-12-03 14:43:33', NULL, '2020-12-03 17:14:39');
INSERT INTO `sys_role` VALUES ('ff8080817608b7940176269516ad0818', '业务用户', 'sgcc_business_user', NULL, NULL, '2020-12-03 14:44:05', NULL, '2020-12-04 16:53:15');
INSERT INTO `sys_role` VALUES ('ff8080817608b79401762695e474081a', '超级管理员', 'sgcc_super_admin', NULL, NULL, '2020-12-03 14:21:05', NULL, '2020-12-05 20:45:18');
INSERT INTO `sys_role` VALUES ('ff8080817608b7940176269806b4081c', '通用角色', 'sgcc_universal_role', NULL, NULL, '2020-12-03 14:21:05', NULL, '2020-12-05 20:45:18');

-- ----------------------------
-- Table structure for sys_role_permission
-- ----------------------------
DROP TABLE IF EXISTS `sys_role_permission`;
CREATE TABLE `sys_role_permission`  (
  `id` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `role_id` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '角色id',
  `permission_id` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '权限id',
  `data_rule_ids` varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '数据权限ids',
  `operate_date` datetime(0) NULL DEFAULT NULL COMMENT '操作时间',
  `operate_ip` varchar(180) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '操作ip',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `index_group_role_per_id`(`role_id`, `permission_id`) USING BTREE,
  INDEX `index_group_role_id`(`role_id`) USING BTREE,
  INDEX `index_group_per_id`(`permission_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '角色权限表' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of sys_role_permission
-- ----------------------------
INSERT INTO `sys_role_permission` VALUES ('00b0748f04d3ea52c8cfa179c1c9d384', '52b0cf022ac4187b2a70dfa4f8b2d940', 'd7d6e2e4e2934f2c9385a623fd98c6f3', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('00b82058779cca5106fbb84783534c9b', 'f6817f48af4fb3af11b9e8bf182f618b', '4148ec82b6acd69f470bea75fe41c357', '', NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('0254c0b25694ad5479e6d6935bbc176e', 'f6817f48af4fb3af11b9e8bf182f618b', '944abf0a8fc22fe1f1154a389a574154', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('09bd4fc30ffe88c4a44ed3868f442719', 'f6817f48af4fb3af11b9e8bf182f618b', 'e6bfd1fcabfd7942fdd05f076d1dad38', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('0c2d2db76ee3aa81a4fe0925b0f31365', 'f6817f48af4fb3af11b9e8bf182f618b', '024f1fd1283dc632458976463d8984e1', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('0c6b8facbb1cc874964c87a8cf01e4b1', 'f6817f48af4fb3af11b9e8bf182f618b', '841057b8a1bef8f6b4b20f9a618a7fa6', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('0c6e1075e422972083c3e854d9af7851', 'f6817f48af4fb3af11b9e8bf182f618b', '08e6b9dc3c04489c8e1ff2ce6f105aa4', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('0d9d14bc66e9d5e99b0280095fdc8587', 'ee8626f80f7c2619917b6236f3a7f02b', '277bfabef7d76e89b33062b16a9a5020', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('0dec36b68c234767cd35466efef3b941', 'ee8626f80f7c2619917b6236f3a7f02b', '54dd5457a3190740005c1bfec55b1c34', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('0e1469997af2d3b97fff56a59ee29eeb', 'f6817f48af4fb3af11b9e8bf182f618b', 'e41b69c57a941a3bbcce45032fe57605', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('0f861cb988fdc639bb1ab943471f3a72', 'f6817f48af4fb3af11b9e8bf182f618b', '97c8629abc7848eccdb6d77c24bb3ebb', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('105c2ac10741e56a618a82cd58c461d7', 'e51758fa916c881624b046d26bd09230', '1663f3faba244d16c94552f849627d84', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('115a6673ae6c0816d3f60de221520274', '21c5a3187763729408b40afb0d0fdfa8', '63b551e81c5956d5c861593d366d8c57', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('1185039870491439105', 'f6817f48af4fb3af11b9e8bf182f618b', '1174506953255182338', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('1185039870529187841', 'f6817f48af4fb3af11b9e8bf182f618b', '1174590283938041857', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('1185039870537576450', 'f6817f48af4fb3af11b9e8bf182f618b', '1166535831146504193', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('1197431682208206850', 'f6817f48af4fb3af11b9e8bf182f618b', '1192318987661234177', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('1197795315916271617', 'f6817f48af4fb3af11b9e8bf182f618b', '109c78a583d4693ce2f16551b7786786', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('1197795316268593154', 'f6817f48af4fb3af11b9e8bf182f618b', '9fe26464838de2ea5e90f2367e35efa0', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('1209423530518761473', 'f6817f48af4fb3af11b9e8bf182f618b', '1205097455226462210', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('1209423530594258945', 'f6817f48af4fb3af11b9e8bf182f618b', '1205098241075453953', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('1209423530606841858', 'f6817f48af4fb3af11b9e8bf182f618b', '1205306106780364802', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('1209423580355481602', 'f6817f48af4fb3af11b9e8bf182f618b', '190c2b43bec6a5f7a4194a85db67d96a', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('1209654501558046722', 'f6817f48af4fb3af11b9e8bf182f618b', 'f2849d3814fc97993bfc519ae6bbf049', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('1231590078632955905', 'f6817f48af4fb3af11b9e8bf182f618b', '1224641973866467330', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('1231590078658121729', 'f6817f48af4fb3af11b9e8bf182f618b', '1209731624921534465', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('1231590078662316033', 'f6817f48af4fb3af11b9e8bf182f618b', '1229674163694841857', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('1232123957949304833', 'ee8626f80f7c2619917b6236f3a7f02b', 'f0675b52d89100ee88472b6800754a08', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('1232123957978664962', 'ee8626f80f7c2619917b6236f3a7f02b', '1232123780958064642', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('1232123957978664963', 'ee8626f80f7c2619917b6236f3a7f02b', '020b06793e4de2eee0007f603000c769', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('1232123957987053570', 'ee8626f80f7c2619917b6236f3a7f02b', '2aeddae571695cd6380f6d6d334d6e7d', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('1232124727411150850', 'f6817f48af4fb3af11b9e8bf182f618b', '1232123780958064642', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('1232125488694104066', 'ee8626f80f7c2619917b6236f3a7f02b', 'e41b69c57a941a3bbcce45032fe57605', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('1260923306820882434', 'f6817f48af4fb3af11b9e8bf182f618b', '1260923256208216065', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('1260923306841853953', 'f6817f48af4fb3af11b9e8bf182f618b', '1260922988733255681', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('1260928399955836929', 'f6817f48af4fb3af11b9e8bf182f618b', '1260928341675982849', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('1260929736852488194', 'ee8626f80f7c2619917b6236f3a7f02b', '1260929666434318338', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('1260931414095937537', 'ee8626f80f7c2619917b6236f3a7f02b', '1260931366557696001', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('126ea9faebeec2b914d6d9bef957afb6', 'f6817f48af4fb3af11b9e8bf182f618b', 'f1cb187abf927c88b89470d08615f5ac', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('1281494164924653569', 'f6817f48af4fb3af11b9e8bf182f618b', '1280350452934307841', NULL, '2020-07-10 15:43:13', '127.0.0.1');
INSERT INTO `sys_role_permission` VALUES ('1281494164945625089', 'f6817f48af4fb3af11b9e8bf182f618b', '1280464606292099074', NULL, '2020-07-10 15:43:13', '127.0.0.1');
INSERT INTO `sys_role_permission` VALUES ('1281494684632473602', 'f6817f48af4fb3af11b9e8bf182f618b', '1265162119913824258', NULL, '2020-07-10 15:45:16', '127.0.0.1');
INSERT INTO `sys_role_permission` VALUES ('1301035471375175681', '1301035310028689409', '1300999295377346562', NULL, '2020-09-02 13:53:23', '192.168.19.151');
INSERT INTO `sys_role_permission` VALUES ('1301035471387758593', '1301035310028689409', '1301034916112240641', NULL, '2020-09-02 13:53:23', '192.168.19.151');
INSERT INTO `sys_role_permission` VALUES ('1301035471391952898', '1301035310028689409', '1300999947163799554', NULL, '2020-09-02 13:53:23', '192.168.19.151');
INSERT INTO `sys_role_permission` VALUES ('1301035471396147202', '1301035310028689409', '1301034658502283266', NULL, '2020-09-02 13:53:23', '192.168.19.151');
INSERT INTO `sys_role_permission` VALUES ('1301035471400341505', '1301035310028689409', '1301000181361152001', NULL, '2020-09-02 13:53:23', '192.168.19.151');
INSERT INTO `sys_role_permission` VALUES ('1301035471404535810', '1301035310028689409', '1301001912425910273', NULL, '2020-09-02 13:53:23', '192.168.19.151');
INSERT INTO `sys_role_permission` VALUES ('1301035471412924417', '1301035310028689409', '1301000639995711490', NULL, '2020-09-02 13:53:23', '192.168.19.151');
INSERT INTO `sys_role_permission` VALUES ('1301035471417118722', '1301035310028689409', '1301001622897299458', NULL, '2020-09-02 13:53:23', '192.168.19.151');
INSERT INTO `sys_role_permission` VALUES ('1301035471421313026', '1301035310028689409', '1301000852273631234', NULL, '2020-09-02 13:53:23', '192.168.19.151');
INSERT INTO `sys_role_permission` VALUES ('1301035471425507329', '1301035310028689409', '1301001284265971713', NULL, '2020-09-02 13:53:23', '192.168.19.151');
INSERT INTO `sys_role_permission` VALUES ('1301096501568606210', '1301035310028689409', '1301077101432016897', NULL, '2020-09-02 17:55:53', '192.168.19.151');
INSERT INTO `sys_role_permission` VALUES ('1301096501589577730', '1301035310028689409', '1301096112848900098', NULL, '2020-09-02 17:55:53', '192.168.19.151');
INSERT INTO `sys_role_permission` VALUES ('1301096501593772034', '1301035310028689409', '1301088665497243650', NULL, '2020-09-02 17:55:53', '192.168.19.151');
INSERT INTO `sys_role_permission` VALUES ('1301429938859020289', '1301035310028689409', '1301427338168242177', NULL, '2020-09-03 16:00:51', '192.168.19.151');
INSERT INTO `sys_role_permission` VALUES ('1301429938875797505', '1301035310028689409', '1301427916441128962', NULL, '2020-09-03 16:00:51', '192.168.19.151');
INSERT INTO `sys_role_permission` VALUES ('1301429938879991809', '1301035310028689409', '1301428144502214658', NULL, '2020-09-03 16:00:51', '192.168.19.151');
INSERT INTO `sys_role_permission` VALUES ('1301449207093137410', '1301035310028689409', '1301447721621655554', NULL, '2020-09-03 17:17:25', '192.168.19.151');
INSERT INTO `sys_role_permission` VALUES ('1305409909881311234', '1301035310028689409', '1304321289430765570', NULL, '2020-09-14 15:35:50', '192.168.19.151,172.17.0.5');
INSERT INTO `sys_role_permission` VALUES ('1305409909902282753', '1301035310028689409', '1301802633433976834', NULL, '2020-09-14 15:35:50', '192.168.19.151,172.17.0.5');
INSERT INTO `sys_role_permission` VALUES ('1309380216367353857', '1301035310028689409', '1309379979724722178', NULL, '2020-09-25 14:32:25', '192.168.19.151,172.17.0.5');
INSERT INTO `sys_role_permission` VALUES ('1310407884021960706', '1301035310028689409', '1310406951590432769', NULL, '2020-09-28 10:36:00', '192.168.19.151,172.17.0.5');
INSERT INTO `sys_role_permission` VALUES ('1310407884030349314', '1301035310028689409', '1310407190049198081', NULL, '2020-09-28 10:36:00', '192.168.19.151,172.17.0.5');
INSERT INTO `sys_role_permission` VALUES ('1310407884030349315', '1301035310028689409', '1310407413609795585', NULL, '2020-09-28 10:36:00', '192.168.19.151,172.17.0.5');
INSERT INTO `sys_role_permission` VALUES ('1310407884034543617', '1301035310028689409', '1310407620586115073', NULL, '2020-09-28 10:36:00', '192.168.19.151,172.17.0.5');
INSERT INTO `sys_role_permission` VALUES ('1310407884034543618', '1301035310028689409', '1310404050755260417', NULL, '2020-09-28 10:36:00', '192.168.19.151,172.17.0.5');
INSERT INTO `sys_role_permission` VALUES ('1310407884034543619', '1301035310028689409', '1310404258897596417', NULL, '2020-09-28 10:36:00', '192.168.19.151,172.17.0.5');
INSERT INTO `sys_role_permission` VALUES ('1310407884038737921', '1301035310028689409', '1310404620442406913', NULL, '2020-09-28 10:36:00', '192.168.19.151,172.17.0.5');
INSERT INTO `sys_role_permission` VALUES ('1310407884038737922', '1301035310028689409', '1310404991655088130', NULL, '2020-09-28 10:36:00', '192.168.19.151,172.17.0.5');
INSERT INTO `sys_role_permission` VALUES ('1310407884042932226', '1301035310028689409', '1310405261181063170', NULL, '2020-09-28 10:36:00', '192.168.19.151,172.17.0.5');
INSERT INTO `sys_role_permission` VALUES ('1310407884047126529', '1301035310028689409', '1310405668003385346', NULL, '2020-09-28 10:36:00', '192.168.19.151,172.17.0.5');
INSERT INTO `sys_role_permission` VALUES ('1310407884047126530', '1301035310028689409', '1310406050767179777', NULL, '2020-09-28 10:36:00', '192.168.19.151,172.17.0.5');
INSERT INTO `sys_role_permission` VALUES ('1310407884051320834', '1301035310028689409', '1310406264022372354', NULL, '2020-09-28 10:36:00', '192.168.19.151,172.17.0.5');
INSERT INTO `sys_role_permission` VALUES ('1310407884051320835', '1301035310028689409', '1310406614360002562', NULL, '2020-09-28 10:36:00', '192.168.19.151,172.17.0.5');
INSERT INTO `sys_role_permission` VALUES ('1310407884055515137', '1301035310028689409', '1310402513266020353', NULL, '2020-09-28 10:36:00', '192.168.19.151,172.17.0.5');
INSERT INTO `sys_role_permission` VALUES ('1310407884055515138', '1301035310028689409', '1310403041836404738', NULL, '2020-09-28 10:36:00', '192.168.19.151,172.17.0.5');
INSERT INTO `sys_role_permission` VALUES ('1310407884059709441', '1301035310028689409', '1310403230580084737', NULL, '2020-09-28 10:36:00', '192.168.19.151,172.17.0.5');
INSERT INTO `sys_role_permission` VALUES ('1310407884059709442', '1301035310028689409', '1310403473635807234', NULL, '2020-09-28 10:36:00', '192.168.19.151,172.17.0.5');
INSERT INTO `sys_role_permission` VALUES ('1310407884063903746', '1301035310028689409', '1310403837600731138', NULL, '2020-09-28 10:36:00', '192.168.19.151,172.17.0.5');
INSERT INTO `sys_role_permission` VALUES ('1310407884063903747', '1301035310028689409', '1310399637995130881', NULL, '2020-09-28 10:36:00', '192.168.19.151,172.17.0.5');
INSERT INTO `sys_role_permission` VALUES ('1310407884068098050', '1301035310028689409', '1310400302846840834', NULL, '2020-09-28 10:36:00', '192.168.19.151,172.17.0.5');
INSERT INTO `sys_role_permission` VALUES ('1310407884068098051', '1301035310028689409', '1310400531214110721', NULL, '2020-09-28 10:36:00', '192.168.19.151,172.17.0.5');
INSERT INTO `sys_role_permission` VALUES ('1310407884068098052', '1301035310028689409', '1310400808977698817', NULL, '2020-09-28 10:36:00', '192.168.19.151,172.17.0.5');
INSERT INTO `sys_role_permission` VALUES ('1310407884072292353', '1301035310028689409', '1310401085768208385', NULL, '2020-09-28 10:36:00', '192.168.19.151,172.17.0.5');
INSERT INTO `sys_role_permission` VALUES ('1310407884072292354', '1301035310028689409', '1310401229901271042', NULL, '2020-09-28 10:36:00', '192.168.19.151,172.17.0.5');
INSERT INTO `sys_role_permission` VALUES ('1310407884076486657', '1301035310028689409', '1310401446956503042', NULL, '2020-09-28 10:36:00', '192.168.19.151,172.17.0.5');
INSERT INTO `sys_role_permission` VALUES ('1310407884076486658', '1301035310028689409', '1310401620621660162', NULL, '2020-09-28 10:36:00', '192.168.19.151,172.17.0.5');
INSERT INTO `sys_role_permission` VALUES ('1310407884080680962', '1301035310028689409', '1310401771595632641', NULL, '2020-09-28 10:36:00', '192.168.19.151,172.17.0.5');
INSERT INTO `sys_role_permission` VALUES ('1310407884080680963', '1301035310028689409', '1310401950704996353', NULL, '2020-09-28 10:36:00', '192.168.19.151,172.17.0.5');
INSERT INTO `sys_role_permission` VALUES ('1310413464514072577', '1301035310028689409', '1310413060296413185', NULL, '2020-09-28 10:58:10', '192.168.19.151,172.17.0.5');
INSERT INTO `sys_role_permission` VALUES ('1310413464522461186', '1301035310028689409', '1310413263179091969', NULL, '2020-09-28 10:58:10', '192.168.19.151,172.17.0.5');
INSERT INTO `sys_role_permission` VALUES ('1310413464526655490', '1301035310028689409', '1310409534656745473', NULL, '2020-09-28 10:58:10', '192.168.19.151,172.17.0.5');
INSERT INTO `sys_role_permission` VALUES ('1310413464530849794', '1301035310028689409', '1310410737562161153', NULL, '2020-09-28 10:58:10', '192.168.19.151,172.17.0.5');
INSERT INTO `sys_role_permission` VALUES ('1310413464530849795', '1301035310028689409', '1310412463300153345', NULL, '2020-09-28 10:58:10', '192.168.19.151,172.17.0.5');
INSERT INTO `sys_role_permission` VALUES ('1310413464530849796', '1301035310028689409', '1310410227111170050', NULL, '2020-09-28 10:58:10', '192.168.19.151,172.17.0.5');
INSERT INTO `sys_role_permission` VALUES ('1310413464535044098', '1301035310028689409', '1310410534536876033', NULL, '2020-09-28 10:58:10', '192.168.19.151,172.17.0.5');
INSERT INTO `sys_role_permission` VALUES ('1310413464535044099', '1301035310028689409', '1310409822184673282', NULL, '2020-09-28 10:58:10', '192.168.19.151,172.17.0.5');
INSERT INTO `sys_role_permission` VALUES ('1310413464539238401', '1301035310028689409', '1310410017085591553', NULL, '2020-09-28 10:58:10', '192.168.19.151,172.17.0.5');
INSERT INTO `sys_role_permission` VALUES ('1310416907517562882', '1301035310028689409', '1310415795251056642', NULL, '2020-09-28 11:11:51', '192.168.19.151,172.17.0.5');
INSERT INTO `sys_role_permission` VALUES ('1310416907525951490', '1301035310028689409', '1310416188978761730', NULL, '2020-09-28 11:11:51', '192.168.19.151,172.17.0.5');
INSERT INTO `sys_role_permission` VALUES ('1310416907525951491', '1301035310028689409', '1310416488150077442', NULL, '2020-09-28 11:11:51', '192.168.19.151,172.17.0.5');
INSERT INTO `sys_role_permission` VALUES ('1310433391086145538', '1301035310028689409', '1310418138898108418', NULL, '2020-09-28 12:17:21', '192.168.19.151,172.17.0.5');
INSERT INTO `sys_role_permission` VALUES ('1310433391102922753', '1301035310028689409', '1310422936447881217', NULL, '2020-09-28 12:17:21', '192.168.19.151,172.17.0.5');
INSERT INTO `sys_role_permission` VALUES ('1310433391107117057', '1301035310028689409', '1310432505559519233', NULL, '2020-09-28 12:17:21', '192.168.19.151,172.17.0.5');
INSERT INTO `sys_role_permission` VALUES ('1310433391107117058', '1301035310028689409', '1310432897492062210', NULL, '2020-09-28 12:17:21', '192.168.19.151,172.17.0.5');
INSERT INTO `sys_role_permission` VALUES ('1310433391111311362', '1301035310028689409', '1310433274404802561', NULL, '2020-09-28 12:17:21', '192.168.19.151,172.17.0.5');
INSERT INTO `sys_role_permission` VALUES ('1310433969174482946', '1301035310028689409', '1310433915395117057', NULL, '2020-09-28 12:19:39', '192.168.19.151,172.17.0.5');
INSERT INTO `sys_role_permission` VALUES ('1331863326669410306', 'f6817f48af4fb3af11b9e8bf182f618b', '1260929666434318338', NULL, '2020-11-26 15:32:16', '127.0.0.1, 192.168.19.151,172.17.0.4');
INSERT INTO `sys_role_permission` VALUES ('1331863326686187521', 'f6817f48af4fb3af11b9e8bf182f618b', '1260931366557696001', NULL, '2020-11-26 15:32:16', '127.0.0.1, 192.168.19.151,172.17.0.4');
INSERT INTO `sys_role_permission` VALUES ('1331863326694576129', 'f6817f48af4fb3af11b9e8bf182f618b', '1260933542969458689', NULL, '2020-11-26 15:32:16', '127.0.0.1, 192.168.19.151,172.17.0.4');
INSERT INTO `sys_role_permission` VALUES ('1331863326694576130', 'f6817f48af4fb3af11b9e8bf182f618b', '1301001284265971713', NULL, '2020-11-26 15:32:16', '127.0.0.1, 192.168.19.151,172.17.0.4');
INSERT INTO `sys_role_permission` VALUES ('1331863326694576131', 'f6817f48af4fb3af11b9e8bf182f618b', '1301001622897299458', NULL, '2020-11-26 15:32:16', '127.0.0.1, 192.168.19.151,172.17.0.4');
INSERT INTO `sys_role_permission` VALUES ('1331863326707159042', 'f6817f48af4fb3af11b9e8bf182f618b', '1301034658502283266', NULL, '2020-11-26 15:32:16', '127.0.0.1, 192.168.19.151,172.17.0.4');
INSERT INTO `sys_role_permission` VALUES ('1331863326707159043', 'f6817f48af4fb3af11b9e8bf182f618b', '1301034916112240641', NULL, '2020-11-26 15:32:16', '127.0.0.1, 192.168.19.151,172.17.0.4');
INSERT INTO `sys_role_permission` VALUES ('1331863326711353346', 'f6817f48af4fb3af11b9e8bf182f618b', '1301077101432016897', NULL, '2020-11-26 15:32:16', '127.0.0.1, 192.168.19.151,172.17.0.4');
INSERT INTO `sys_role_permission` VALUES ('1331863326719741954', 'f6817f48af4fb3af11b9e8bf182f618b', '1301427338168242177', NULL, '2020-11-26 15:32:16', '127.0.0.1, 192.168.19.151,172.17.0.4');
INSERT INTO `sys_role_permission` VALUES ('1331863326719741955', 'f6817f48af4fb3af11b9e8bf182f618b', '1301427916441128962', NULL, '2020-11-26 15:32:16', '127.0.0.1, 192.168.19.151,172.17.0.4');
INSERT INTO `sys_role_permission` VALUES ('1331863326719741956', 'f6817f48af4fb3af11b9e8bf182f618b', '1310400302846840834', NULL, '2020-11-26 15:32:16', '127.0.0.1, 192.168.19.151,172.17.0.4');
INSERT INTO `sys_role_permission` VALUES ('1331863326719741957', 'f6817f48af4fb3af11b9e8bf182f618b', '1310402513266020353', NULL, '2020-11-26 15:32:16', '127.0.0.1, 192.168.19.151,172.17.0.4');
INSERT INTO `sys_role_permission` VALUES ('1331863326732324866', 'f6817f48af4fb3af11b9e8bf182f618b', '1310404050755260417', NULL, '2020-11-26 15:32:16', '127.0.0.1, 192.168.19.151,172.17.0.4');
INSERT INTO `sys_role_permission` VALUES ('1331863326732324867', 'f6817f48af4fb3af11b9e8bf182f618b', '1310404258897596417', NULL, '2020-11-26 15:32:16', '127.0.0.1, 192.168.19.151,172.17.0.4');
INSERT INTO `sys_role_permission` VALUES ('1331863326736519169', 'f6817f48af4fb3af11b9e8bf182f618b', '1310410227111170050', NULL, '2020-11-26 15:32:16', '127.0.0.1, 192.168.19.151,172.17.0.4');
INSERT INTO `sys_role_permission` VALUES ('1331863326736519170', 'f6817f48af4fb3af11b9e8bf182f618b', '1310410737562161153', NULL, '2020-11-26 15:32:16', '127.0.0.1, 192.168.19.151,172.17.0.4');
INSERT INTO `sys_role_permission` VALUES ('1331863326744907778', 'f6817f48af4fb3af11b9e8bf182f618b', '1310413060296413185', NULL, '2020-11-26 15:32:16', '127.0.0.1, 192.168.19.151,172.17.0.4');
INSERT INTO `sys_role_permission` VALUES ('1331863326753296385', 'f6817f48af4fb3af11b9e8bf182f618b', '1310418138898108418', NULL, '2020-11-26 15:32:16', '127.0.0.1, 192.168.19.151,172.17.0.4');
INSERT INTO `sys_role_permission` VALUES ('1331863326753296386', 'f6817f48af4fb3af11b9e8bf182f618b', '1a0811914300741f4e11838ff37a1d3a', NULL, '2020-11-26 15:32:16', '127.0.0.1, 192.168.19.151,172.17.0.4');
INSERT INTO `sys_role_permission` VALUES ('1331863326753296387', 'f6817f48af4fb3af11b9e8bf182f618b', '277bfabef7d76e89b33062b16a9a5020', NULL, '2020-11-26 15:32:16', '127.0.0.1, 192.168.19.151,172.17.0.4');
INSERT INTO `sys_role_permission` VALUES ('1331863326753296388', 'f6817f48af4fb3af11b9e8bf182f618b', 'b6bcee2ccc854052d3cc3e9c96d90197', NULL, '2020-11-26 15:32:16', '127.0.0.1, 192.168.19.151,172.17.0.4');
INSERT INTO `sys_role_permission` VALUES ('1331863326757490689', 'f6817f48af4fb3af11b9e8bf182f618b', '1301001912425910273', NULL, '2020-11-26 15:32:16', '127.0.0.1, 192.168.19.151,172.17.0.4');
INSERT INTO `sys_role_permission` VALUES ('1331863326765879298', 'f6817f48af4fb3af11b9e8bf182f618b', '1301088665497243650', NULL, '2020-11-26 15:32:16', '127.0.0.1, 192.168.19.151,172.17.0.4');
INSERT INTO `sys_role_permission` VALUES ('1331863326765879299', 'f6817f48af4fb3af11b9e8bf182f618b', '1301096112848900098', NULL, '2020-11-26 15:32:16', '127.0.0.1, 192.168.19.151,172.17.0.4');
INSERT INTO `sys_role_permission` VALUES ('1331863326765879300', 'f6817f48af4fb3af11b9e8bf182f618b', '1301428144502214658', NULL, '2020-11-26 15:32:16', '127.0.0.1, 192.168.19.151,172.17.0.4');
INSERT INTO `sys_role_permission` VALUES ('1331863326765879301', 'f6817f48af4fb3af11b9e8bf182f618b', '1310400531214110721', NULL, '2020-11-26 15:32:16', '127.0.0.1, 192.168.19.151,172.17.0.4');
INSERT INTO `sys_role_permission` VALUES ('1331863326770073602', 'f6817f48af4fb3af11b9e8bf182f618b', '1310403041836404738', NULL, '2020-11-26 15:32:16', '127.0.0.1, 192.168.19.151,172.17.0.4');
INSERT INTO `sys_role_permission` VALUES ('1331863326770073603', 'f6817f48af4fb3af11b9e8bf182f618b', '1310409534656745473', NULL, '2020-11-26 15:32:16', '127.0.0.1, 192.168.19.151,172.17.0.4');
INSERT INTO `sys_role_permission` VALUES ('1331863326774267905', 'f6817f48af4fb3af11b9e8bf182f618b', '1310410534536876033', NULL, '2020-11-26 15:32:16', '127.0.0.1, 192.168.19.151,172.17.0.4');
INSERT INTO `sys_role_permission` VALUES ('1331863326774267906', 'f6817f48af4fb3af11b9e8bf182f618b', '1310412463300153345', NULL, '2020-11-26 15:32:16', '127.0.0.1, 192.168.19.151,172.17.0.4');
INSERT INTO `sys_role_permission` VALUES ('1331863326778462209', 'f6817f48af4fb3af11b9e8bf182f618b', '1310413263179091969', NULL, '2020-11-26 15:32:16', '127.0.0.1, 192.168.19.151,172.17.0.4');
INSERT INTO `sys_role_permission` VALUES ('1331863326778462210', 'f6817f48af4fb3af11b9e8bf182f618b', '1310422936447881217', NULL, '2020-11-26 15:32:16', '127.0.0.1, 192.168.19.151,172.17.0.4');
INSERT INTO `sys_role_permission` VALUES ('1331863326786850817', 'f6817f48af4fb3af11b9e8bf182f618b', '1310432505559519233', NULL, '2020-11-26 15:32:16', '127.0.0.1, 192.168.19.151,172.17.0.4');
INSERT INTO `sys_role_permission` VALUES ('1331863326786850818', 'f6817f48af4fb3af11b9e8bf182f618b', '1310433915395117057', NULL, '2020-11-26 15:32:16', '127.0.0.1, 192.168.19.151,172.17.0.4');
INSERT INTO `sys_role_permission` VALUES ('1331863326786850819', 'f6817f48af4fb3af11b9e8bf182f618b', '1301802633433976834', NULL, '2020-11-26 15:32:16', '127.0.0.1, 192.168.19.151,172.17.0.4');
INSERT INTO `sys_role_permission` VALUES ('1331863326791045121', 'f6817f48af4fb3af11b9e8bf182f618b', '1304321289430765570', NULL, '2020-11-26 15:32:16', '127.0.0.1, 192.168.19.151,172.17.0.4');
INSERT INTO `sys_role_permission` VALUES ('1331863326791045122', 'f6817f48af4fb3af11b9e8bf182f618b', '1309379979724722178', NULL, '2020-11-26 15:32:16', '127.0.0.1, 192.168.19.151,172.17.0.4');
INSERT INTO `sys_role_permission` VALUES ('1331863326795239425', 'f6817f48af4fb3af11b9e8bf182f618b', '1310400808977698817', NULL, '2020-11-26 15:32:16', '127.0.0.1, 192.168.19.151,172.17.0.4');
INSERT INTO `sys_role_permission` VALUES ('1331863326795239426', 'f6817f48af4fb3af11b9e8bf182f618b', '1310403230580084737', NULL, '2020-11-26 15:32:16', '127.0.0.1, 192.168.19.151,172.17.0.4');
INSERT INTO `sys_role_permission` VALUES ('1331863326799433730', 'f6817f48af4fb3af11b9e8bf182f618b', '1310404620442406913', NULL, '2020-11-26 15:32:16', '127.0.0.1, 192.168.19.151,172.17.0.4');
INSERT INTO `sys_role_permission` VALUES ('1331863326799433731', 'f6817f48af4fb3af11b9e8bf182f618b', '1310409822184673282', NULL, '2020-11-26 15:32:16', '127.0.0.1, 192.168.19.151,172.17.0.4');
INSERT INTO `sys_role_permission` VALUES ('1331863326803628034', 'f6817f48af4fb3af11b9e8bf182f618b', '1310416488150077442', NULL, '2020-11-26 15:32:16', '127.0.0.1, 192.168.19.151,172.17.0.4');
INSERT INTO `sys_role_permission` VALUES ('1331863326807822337', 'f6817f48af4fb3af11b9e8bf182f618b', '1310432897492062210', NULL, '2020-11-26 15:32:16', '127.0.0.1, 192.168.19.151,172.17.0.4');
INSERT INTO `sys_role_permission` VALUES ('1331863326807822338', 'f6817f48af4fb3af11b9e8bf182f618b', '1310399637995130881', NULL, '2020-11-26 15:32:16', '127.0.0.1, 192.168.19.151,172.17.0.4');
INSERT INTO `sys_role_permission` VALUES ('1331863326807822339', 'f6817f48af4fb3af11b9e8bf182f618b', '1310401085768208385', NULL, '2020-11-26 15:32:16', '127.0.0.1, 192.168.19.151,172.17.0.4');
INSERT INTO `sys_role_permission` VALUES ('1331863326812016642', 'f6817f48af4fb3af11b9e8bf182f618b', '1310403473635807234', NULL, '2020-11-26 15:32:16', '127.0.0.1, 192.168.19.151,172.17.0.4');
INSERT INTO `sys_role_permission` VALUES ('1331863326812016643', 'f6817f48af4fb3af11b9e8bf182f618b', '1310404991655088130', NULL, '2020-11-26 15:32:16', '127.0.0.1, 192.168.19.151,172.17.0.4');
INSERT INTO `sys_role_permission` VALUES ('1331863326816210946', 'f6817f48af4fb3af11b9e8bf182f618b', '1310406951590432769', NULL, '2020-11-26 15:32:16', '127.0.0.1, 192.168.19.151,172.17.0.4');
INSERT INTO `sys_role_permission` VALUES ('1331863326816210947', 'f6817f48af4fb3af11b9e8bf182f618b', '1310410017085591553', NULL, '2020-11-26 15:32:16', '127.0.0.1, 192.168.19.151,172.17.0.4');
INSERT INTO `sys_role_permission` VALUES ('1331863326820405250', 'f6817f48af4fb3af11b9e8bf182f618b', '1310416188978761730', NULL, '2020-11-26 15:32:16', '127.0.0.1, 192.168.19.151,172.17.0.4');
INSERT INTO `sys_role_permission` VALUES ('1331863326820405251', 'f6817f48af4fb3af11b9e8bf182f618b', '1310433274404802561', NULL, '2020-11-26 15:32:16', '127.0.0.1, 192.168.19.151,172.17.0.4');
INSERT INTO `sys_role_permission` VALUES ('1331863326824599553', 'f6817f48af4fb3af11b9e8bf182f618b', '1310401229901271042', NULL, '2020-11-26 15:32:16', '127.0.0.1, 192.168.19.151,172.17.0.4');
INSERT INTO `sys_role_permission` VALUES ('1331863326832988161', 'f6817f48af4fb3af11b9e8bf182f618b', '1310403837600731138', NULL, '2020-11-26 15:32:16', '127.0.0.1, 192.168.19.151,172.17.0.4');
INSERT INTO `sys_role_permission` VALUES ('1331863326832988162', 'f6817f48af4fb3af11b9e8bf182f618b', '1310405261181063170', NULL, '2020-11-26 15:32:16', '127.0.0.1, 192.168.19.151,172.17.0.4');
INSERT INTO `sys_role_permission` VALUES ('1331863326832988163', 'f6817f48af4fb3af11b9e8bf182f618b', '1310407190049198081', NULL, '2020-11-26 15:32:16', '127.0.0.1, 192.168.19.151,172.17.0.4');
INSERT INTO `sys_role_permission` VALUES ('1331863326832988164', 'f6817f48af4fb3af11b9e8bf182f618b', '1310415795251056642', NULL, '2020-11-26 15:32:16', '127.0.0.1, 192.168.19.151,172.17.0.4');
INSERT INTO `sys_role_permission` VALUES ('1331863326837182466', 'f6817f48af4fb3af11b9e8bf182f618b', '1310401446956503042', NULL, '2020-11-26 15:32:16', '127.0.0.1, 192.168.19.151,172.17.0.4');
INSERT INTO `sys_role_permission` VALUES ('1331863326837182467', 'f6817f48af4fb3af11b9e8bf182f618b', '1310405668003385346', NULL, '2020-11-26 15:32:16', '127.0.0.1, 192.168.19.151,172.17.0.4');
INSERT INTO `sys_role_permission` VALUES ('1331863326841376770', 'f6817f48af4fb3af11b9e8bf182f618b', '1310407413609795585', NULL, '2020-11-26 15:32:16', '127.0.0.1, 192.168.19.151,172.17.0.4');
INSERT INTO `sys_role_permission` VALUES ('1331863326841376771', 'f6817f48af4fb3af11b9e8bf182f618b', '1301447721621655554', NULL, '2020-11-26 15:32:16', '127.0.0.1, 192.168.19.151,172.17.0.4');
INSERT INTO `sys_role_permission` VALUES ('1331863326845571073', 'f6817f48af4fb3af11b9e8bf182f618b', '1310401620621660162', NULL, '2020-11-26 15:32:16', '127.0.0.1, 192.168.19.151,172.17.0.4');
INSERT INTO `sys_role_permission` VALUES ('1331863326845571074', 'f6817f48af4fb3af11b9e8bf182f618b', '1310406050767179777', NULL, '2020-11-26 15:32:16', '127.0.0.1, 192.168.19.151,172.17.0.4');
INSERT INTO `sys_role_permission` VALUES ('1331863326849765377', 'f6817f48af4fb3af11b9e8bf182f618b', '1310401771595632641', NULL, '2020-11-26 15:32:16', '127.0.0.1, 192.168.19.151,172.17.0.4');
INSERT INTO `sys_role_permission` VALUES ('1331863326853959681', 'f6817f48af4fb3af11b9e8bf182f618b', '1310406264022372354', NULL, '2020-11-26 15:32:16', '127.0.0.1, 192.168.19.151,172.17.0.4');
INSERT INTO `sys_role_permission` VALUES ('1331863326853959682', 'f6817f48af4fb3af11b9e8bf182f618b', '1310407620586115073', NULL, '2020-11-26 15:32:16', '127.0.0.1, 192.168.19.151,172.17.0.4');
INSERT INTO `sys_role_permission` VALUES ('1331863326858153986', 'f6817f48af4fb3af11b9e8bf182f618b', '1310401950704996353', NULL, '2020-11-26 15:32:16', '127.0.0.1, 192.168.19.151,172.17.0.4');
INSERT INTO `sys_role_permission` VALUES ('1331863326862348290', 'f6817f48af4fb3af11b9e8bf182f618b', '1310406614360002562', NULL, '2020-11-26 15:32:16', '127.0.0.1, 192.168.19.151,172.17.0.4');
INSERT INTO `sys_role_permission` VALUES ('1331863326862348291', 'f6817f48af4fb3af11b9e8bf182f618b', '1170592628746878978', NULL, '2020-11-26 15:32:16', '127.0.0.1, 192.168.19.151,172.17.0.4');
INSERT INTO `sys_role_permission` VALUES ('1331863326866542593', 'f6817f48af4fb3af11b9e8bf182f618b', '1235823781053313025', NULL, '2020-11-26 15:32:16', '127.0.0.1, 192.168.19.151,172.17.0.4');
INSERT INTO `sys_role_permission` VALUES ('1331863326866542594', 'f6817f48af4fb3af11b9e8bf182f618b', '1300999295377346562', NULL, '2020-11-26 15:32:16', '127.0.0.1, 192.168.19.151,172.17.0.4');
INSERT INTO `sys_role_permission` VALUES ('1331863326874931202', 'f6817f48af4fb3af11b9e8bf182f618b', '1300999947163799554', NULL, '2020-11-26 15:32:16', '127.0.0.1, 192.168.19.151,172.17.0.4');
INSERT INTO `sys_role_permission` VALUES ('1331863326874931203', 'f6817f48af4fb3af11b9e8bf182f618b', '1301000181361152001', NULL, '2020-11-26 15:32:16', '127.0.0.1, 192.168.19.151,172.17.0.4');
INSERT INTO `sys_role_permission` VALUES ('1331863326879125505', 'f6817f48af4fb3af11b9e8bf182f618b', '1301000639995711490', NULL, '2020-11-26 15:32:16', '127.0.0.1, 192.168.19.151,172.17.0.4');
INSERT INTO `sys_role_permission` VALUES ('1331863326879125506', 'f6817f48af4fb3af11b9e8bf182f618b', '1301000852273631234', NULL, '2020-11-26 15:32:16', '127.0.0.1, 192.168.19.151,172.17.0.4');
INSERT INTO `sys_role_permission` VALUES ('145eac8dd88eddbd4ce0a800ab40a92c', 'e51758fa916c881624b046d26bd09230', '08e6b9dc3c04489c8e1ff2ce6f105aa4', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('154edd0599bd1dc2c7de220b489cd1e2', 'f6817f48af4fb3af11b9e8bf182f618b', '7ac9eb9ccbde2f7a033cd4944272bf1e', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('165acd6046a0eaf975099f46a3c898ea', 'f6817f48af4fb3af11b9e8bf182f618b', '4f66409ef3bbd69c1d80469d6e2a885e', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('1664b92dff13e1575e3a929caa2fa14d', 'f6817f48af4fb3af11b9e8bf182f618b', 'd2bbf9ebca5a8fa2e227af97d2da7548', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('16ef8ed3865ccc6f6306200760896c50', 'ee8626f80f7c2619917b6236f3a7f02b', 'e8af452d8948ea49d37c934f5100ae6a', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('17ead5b7d97ed365398ab20009a69ea3', '52b0cf022ac4187b2a70dfa4f8b2d940', 'e08cb190ef230d5d4f03824198773950', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('1ac1688ef8456f384091a03d88a89ab1', '52b0cf022ac4187b2a70dfa4f8b2d940', '693ce69af3432bd00be13c3971a57961', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('1af4babaa4227c3cbb830bc5eb513abb', 'ee8626f80f7c2619917b6236f3a7f02b', 'e08cb190ef230d5d4f03824198773950', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('1ba162bbc2076c25561f8622f610d5bf', 'ee8626f80f7c2619917b6236f3a7f02b', 'aedbf679b5773c1f25e9f7b10111da73', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('1c1dbba68ef1817e7fb19c822d2854e8', 'f6817f48af4fb3af11b9e8bf182f618b', 'fb367426764077dcf94640c843733985', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('1c55c4ced20765b8ebab383caa60f0b6', 'e51758fa916c881624b046d26bd09230', 'fb367426764077dcf94640c843733985', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('1e099baeae01b747d67aca06bdfc34d1', 'e51758fa916c881624b046d26bd09230', '6ad53fd1b220989a8b71ff482d683a5a', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('1fe4d408b85f19618c15bcb768f0ec22', '1750a8fb3e6d90cb7957c02de1dc8e59', '9502685863ab87f0ad1134142788a385', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('20e53c87a785688bdc0a5bb6de394ef1', 'f6817f48af4fb3af11b9e8bf182f618b', '540a2936940846cb98114ffb0d145cb8', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('248d288586c6ff3bd14381565df84163', '52b0cf022ac4187b2a70dfa4f8b2d940', '3f915b2769fc80648e92d04e84ca059d', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('25491ecbd5a9b34f09c8bc447a10ede1', 'f6817f48af4fb3af11b9e8bf182f618b', 'd07a2c87a451434c99ab06296727ec4f', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('25f5443f19c34d99718a016d5f54112e', 'ee8626f80f7c2619917b6236f3a7f02b', '6e73eb3c26099c191bf03852ee1310a1', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('27489816708b18859768dfed5945c405', 'a799c3b1b12dd3ed4bd046bfaef5fe6e', '9502685863ab87f0ad1134142788a385', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('296f9c75ca0e172ae5ce4c1022c996df', '646c628b2b8295fbdab2d34044de0354', '732d48f8e0abe99fe6a23d18a3171cd1', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('29fb4d37aa29b9fa400f389237cf9fe7', 'ee8626f80f7c2619917b6236f3a7f02b', '05b3c82ddb2536a4a5ee1a4c46b5abef', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('29fb6b0ad59a7e911c8d27e0bdc42d23', 'f6817f48af4fb3af11b9e8bf182f618b', '9a90363f216a6a08f32eecb3f0bf12a3', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('2ad37346c1b83ddeebc008f6987b2227', 'f6817f48af4fb3af11b9e8bf182f618b', '8d1ebd663688965f1fd86a2f0ead3416', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('2c462293cbb0eab7e8ae0a3600361b5f', '52b0cf022ac4187b2a70dfa4f8b2d940', '9502685863ab87f0ad1134142788a385', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('2c928084762182cf01762182cf5c0002', 'ff8080817608b79401762141af3c0204', '1331870906774982658', NULL, '2020-12-02 11:33:10', '192.168.214.162,172.18.0.6');
INSERT INTO `sys_role_permission` VALUES ('2c928084762182cf01762182cf5c0003', 'ff8080817608b79401762141af3c0204', '1301427916441128962', NULL, '2020-12-02 11:33:10', '192.168.214.162,172.18.0.6');
INSERT INTO `sys_role_permission` VALUES ('2c928084762182cf01762182cf5c0004', 'ff8080817608b79401762141af3c0204', '1331871850078146562', NULL, '2020-12-02 11:33:10', '192.168.214.162,172.18.0.6');
INSERT INTO `sys_role_permission` VALUES ('2c928084762182cf01762182cf5c0005', 'ff8080817608b79401762141af3c0204', '1301001912425910273', NULL, '2020-12-02 11:33:10', '192.168.214.162,172.18.0.6');
INSERT INTO `sys_role_permission` VALUES ('2c928084762182cf01762182cf5c0006', 'ff8080817608b79401762141af3c0204', '1310416488150077442', NULL, '2020-12-02 11:33:10', '192.168.214.162,172.18.0.6');
INSERT INTO `sys_role_permission` VALUES ('2c928084762182cf01762182cf5c0007', 'ff8080817608b79401762141af3c0204', '1331872709646225410', NULL, '2020-12-02 11:33:10', '192.168.214.162,172.18.0.6');
INSERT INTO `sys_role_permission` VALUES ('2c928084762182cf01762182cf5c0008', 'ff8080817608b79401762141af3c0204', '1301000181361152001', NULL, '2020-12-02 11:33:10', '192.168.214.162,172.18.0.6');
INSERT INTO `sys_role_permission` VALUES ('2c9280847621bb00017621bb00630002', 'ff8080817608b794017621407ae301ff', '1301001284265971713', NULL, '2020-12-02 12:34:32', '192.168.214.162,172.18.0.6');
INSERT INTO `sys_role_permission` VALUES ('2c9280847621bb00017621bb00630003', 'ff8080817608b794017621407ae301ff', '1301001284265971713', NULL, '2020-12-02 12:34:32', '192.168.214.162,172.18.0.6');
INSERT INTO `sys_role_permission` VALUES ('2c9280847621bb00017621bb00640004', 'ff8080817608b794017621407ae301ff', '1310433915395117057', NULL, '2020-12-02 12:34:32', '192.168.214.162,172.18.0.6');
INSERT INTO `sys_role_permission` VALUES ('2c9280847621bb00017621bb00640005', 'ff8080817608b794017621407ae301ff', '1310433915395117057', NULL, '2020-12-02 12:34:32', '192.168.214.162,172.18.0.6');
INSERT INTO `sys_role_permission` VALUES ('2c9280847621bb00017621bb00640006', 'ff8080817608b794017621407ae301ff', '1301000852273631234', NULL, '2020-12-02 12:34:32', '192.168.214.162,172.18.0.6');
INSERT INTO `sys_role_permission` VALUES ('2c9280847621bb00017621bb00640007', 'ff8080817608b794017621407ae301ff', '1301000852273631234', NULL, '2020-12-02 12:34:32', '192.168.214.162,172.18.0.6');
INSERT INTO `sys_role_permission` VALUES ('2c928085762756a301762756a4440002', 'ff8080817608b79401762692a4510813', '1301001284265971713', NULL, '2020-12-03 14:42:38', '192.168.214.162,172.18.0.7');
INSERT INTO `sys_role_permission` VALUES ('2c928085762756a301762756a4440003', 'ff8080817608b79401762692a4510813', '1310433915395117057', NULL, '2020-12-03 14:42:38', '192.168.214.162,172.18.0.7');
INSERT INTO `sys_role_permission` VALUES ('2c928085762756a301762756a4440004', 'ff8080817608b79401762692a4510813', '1301000852273631234', NULL, '2020-12-03 14:42:38', '192.168.214.162,172.18.0.7');
INSERT INTO `sys_role_permission` VALUES ('2c928085762756a3017627577bac0015', 'ff8080817608b7940176269360550815', '1331871850078146562', NULL, '2020-12-03 14:43:33', '192.168.214.162,172.18.0.7');
INSERT INTO `sys_role_permission` VALUES ('2c928085762756a3017627577bad0018', 'ff8080817608b7940176269360550815', '1310416488150077442', NULL, '2020-12-03 14:43:33', '192.168.214.162,172.18.0.7');
INSERT INTO `sys_role_permission` VALUES ('2c928085762756a3017627577bad001b', 'ff8080817608b7940176269360550815', '1301000181361152001', NULL, '2020-12-03 14:43:33', '192.168.214.162,172.18.0.7');
INSERT INTO `sys_role_permission` VALUES ('2c92808576275e980176275e98cd0000', 'ff8080817608b7940176269516ad0818', 'ff8080817608b794017622acfc5d0479', NULL, '2020-12-03 14:51:20', '192.168.214.162,172.18.0.7');
INSERT INTO `sys_role_permission` VALUES ('2c92808576275e980176275e98cd0001', 'ff8080817608b7940176269516ad0818', '1301034916112240641', NULL, '2020-12-03 14:51:20', '192.168.214.162,172.18.0.7');
INSERT INTO `sys_role_permission` VALUES ('2c92808576275e980176275e98cd0002', 'ff8080817608b7940176269516ad0818', 'ff8080817608b794017622aadd20045d', NULL, '2020-12-03 14:51:20', '192.168.214.162,172.18.0.7');
INSERT INTO `sys_role_permission` VALUES ('2c92808576275e980176275e98cd0003', 'ff8080817608b7940176269516ad0818', '1301096112848900098', NULL, '2020-12-03 14:51:20', '192.168.214.162,172.18.0.7');
INSERT INTO `sys_role_permission` VALUES ('2c92808576275e980176275e98cd0004', 'ff8080817608b7940176269516ad0818', 'ff8080817608b794017622aecbde048e', NULL, '2020-12-03 14:51:20', '192.168.214.162,172.18.0.7');
INSERT INTO `sys_role_permission` VALUES ('2c92808576275e980176275e98cd0005', 'ff8080817608b7940176269516ad0818', '1300999295377346562', NULL, '2020-12-03 14:51:20', '192.168.214.162,172.18.0.7');
INSERT INTO `sys_role_permission` VALUES ('2c92808576275e98017627647b560006', 'ff8080817608b7940176269516ad0818', 'ff8080817608b794017622aa75b10456', NULL, '2020-12-03 14:57:45', '192.168.214.162,172.18.0.7');
INSERT INTO `sys_role_permission` VALUES ('2c92808576275e98017627647b570007', 'ff8080817608b7940176269516ad0818', 'ff8080817608b794017626441d2207df', NULL, '2020-12-03 14:57:45', '192.168.214.162,172.18.0.7');
INSERT INTO `sys_role_permission` VALUES ('2c92808576275e980176276661490008', 'ff8080817608b7940176269516ad0818', 'ff8080817608b794017622adde850480', NULL, '2020-12-03 14:59:50', '192.168.214.162,172.18.0.7');
INSERT INTO `sys_role_permission` VALUES ('2c92808576277ce40176277ce43a0000', 'ff8080817608b7940176269360550815', '1301034916112240641', NULL, '2020-12-03 15:24:25', '192.168.214.162,172.18.0.7');
INSERT INTO `sys_role_permission` VALUES ('2c92808576277ce40176277ce43a0001', 'ff8080817608b7940176269360550815', 'ff8080817608b794017623146d78069b', NULL, '2020-12-03 15:24:25', '192.168.214.162,172.18.0.7');
INSERT INTO `sys_role_permission` VALUES ('2c92808576277ce40176277ce43a0002', 'ff8080817608b7940176269360550815', 'ff8080817608b794017622c0a5fa053d', NULL, '2020-12-03 15:24:25', '192.168.214.162,172.18.0.7');
INSERT INTO `sys_role_permission` VALUES ('2c92808576277ce40176277ce43a0003', 'ff8080817608b7940176269360550815', '1310404050755260417', NULL, '2020-12-03 15:24:25', '192.168.214.162,172.18.0.7');
INSERT INTO `sys_role_permission` VALUES ('2c92808576277ce40176277ce43a0004', 'ff8080817608b7940176269360550815', 'ff8080817608b79401762316aaf906a2', NULL, '2020-12-03 15:24:25', '192.168.214.162,172.18.0.7');
INSERT INTO `sys_role_permission` VALUES ('2c92808576277ce40176277ce43a0005', 'ff8080817608b7940176269360550815', '1301088665497243650', NULL, '2020-12-03 15:24:25', '192.168.214.162,172.18.0.7');
INSERT INTO `sys_role_permission` VALUES ('2c92808576277ce40176277ce43a0006', 'ff8080817608b7940176269360550815', 'ff8080817608b794017622bbc3990521', NULL, '2020-12-03 15:24:25', '192.168.214.162,172.18.0.7');
INSERT INTO `sys_role_permission` VALUES ('2c92808576277ce40176277ce43a0007', 'ff8080817608b7940176269360550815', '1310399637995130881', NULL, '2020-12-03 15:24:25', '192.168.214.162,172.18.0.7');
INSERT INTO `sys_role_permission` VALUES ('2c92808576277ce40176277ce43a0008', 'ff8080817608b7940176269360550815', '1310406951590432769', NULL, '2020-12-03 15:24:25', '192.168.214.162,172.18.0.7');
INSERT INTO `sys_role_permission` VALUES ('2c92808576277ce40176277ce43a0009', 'ff8080817608b7940176269360550815', '1310407190049198081', NULL, '2020-12-03 15:24:25', '192.168.214.162,172.18.0.7');
INSERT INTO `sys_role_permission` VALUES ('2c92808576277ce40176277ce43a000a', 'ff8080817608b7940176269360550815', '1310401771595632641', NULL, '2020-12-03 15:24:25', '192.168.214.162,172.18.0.7');
INSERT INTO `sys_role_permission` VALUES ('2c92808576277ce40176277ce43a000b', 'ff8080817608b7940176269360550815', '1300999295377346562', NULL, '2020-12-03 15:24:25', '192.168.214.162,172.18.0.7');
INSERT INTO `sys_role_permission` VALUES ('2c9280867622388101762238817c0000', '8a6e50b54dc268dc014dc2784aff0115', '1301034916112240641', NULL, '2020-12-02 14:51:37', '127.0.0.1, 192.168.204.109,172.18.0.7');
INSERT INTO `sys_role_permission` VALUES ('2c9280867622388101762238817d0001', '8a6e50b54dc268dc014dc2784aff0115', '1301077101432016897', NULL, '2020-12-02 14:51:37', '127.0.0.1, 192.168.204.109,172.18.0.7');
INSERT INTO `sys_role_permission` VALUES ('2c9280867622388101762238817d0002', '8a6e50b54dc268dc014dc2784aff0115', '1301088665497243650', NULL, '2020-12-02 14:51:37', '127.0.0.1, 192.168.204.109,172.18.0.7');
INSERT INTO `sys_role_permission` VALUES ('2c9280867622388101762238817d0003', '8a6e50b54dc268dc014dc2784aff0115', '1301096112848900098', NULL, '2020-12-02 14:51:37', '127.0.0.1, 192.168.204.109,172.18.0.7');
INSERT INTO `sys_role_permission` VALUES ('2c9280867622388101762238817d0004', '8a6e50b54dc268dc014dc2784aff0115', '1309379979724722178', NULL, '2020-12-02 14:51:37', '127.0.0.1, 192.168.204.109,172.18.0.7');
INSERT INTO `sys_role_permission` VALUES ('2c9280867622388101762238817d0005', '8a6e50b54dc268dc014dc2784aff0115', '1301802633433976834', NULL, '2020-12-02 14:51:37', '127.0.0.1, 192.168.204.109,172.18.0.7');
INSERT INTO `sys_role_permission` VALUES ('2c9280867622388101762238817d0006', '8a6e50b54dc268dc014dc2784aff0115', '1310406951590432769', NULL, '2020-12-02 14:51:37', '127.0.0.1, 192.168.204.109,172.18.0.7');
INSERT INTO `sys_role_permission` VALUES ('2c9280867622388101762238817d0007', '8a6e50b54dc268dc014dc2784aff0115', '1310399637995130881', NULL, '2020-12-02 14:51:37', '127.0.0.1, 192.168.204.109,172.18.0.7');
INSERT INTO `sys_role_permission` VALUES ('2c9280867622388101762238817d0008', '8a6e50b54dc268dc014dc2784aff0115', '1310407190049198081', NULL, '2020-12-02 14:51:37', '127.0.0.1, 192.168.204.109,172.18.0.7');
INSERT INTO `sys_role_permission` VALUES ('2c9280867622388101762238817d0009', '8a6e50b54dc268dc014dc2784aff0115', '1310407413609795585', NULL, '2020-12-02 14:51:37', '127.0.0.1, 192.168.204.109,172.18.0.7');
INSERT INTO `sys_role_permission` VALUES ('2c9280867622388101762238817d000a', '8a6e50b54dc268dc014dc2784aff0115', '1310407620586115073', NULL, '2020-12-02 14:51:37', '127.0.0.1, 192.168.204.109,172.18.0.7');
INSERT INTO `sys_role_permission` VALUES ('2c928086762238810176223e0e99000c', '8a6e50b54dc268dc014dc2784aff0115', '1301001622897299458', NULL, '2020-12-02 14:57:41', '127.0.0.1, 192.168.19.151,172.18.0.7');
INSERT INTO `sys_role_permission` VALUES ('2c928086762238810176223e0e99000e', '8a6e50b54dc268dc014dc2784aff0115', '1310418138898108418', NULL, '2020-12-02 14:57:41', '127.0.0.1, 192.168.19.151,172.18.0.7');
INSERT INTO `sys_role_permission` VALUES ('2c928086762238810176223e0e99000f', '8a6e50b54dc268dc014dc2784aff0115', '1331870906774982658', NULL, '2020-12-02 14:57:41', '127.0.0.1, 192.168.19.151,172.18.0.7');
INSERT INTO `sys_role_permission` VALUES ('2c928086762238810176223e0e990013', '8a6e50b54dc268dc014dc2784aff0115', '1301427338168242177', NULL, '2020-12-02 14:57:41', '127.0.0.1, 192.168.19.151,172.18.0.7');
INSERT INTO `sys_role_permission` VALUES ('2c928086762238810176223e0e990014', '8a6e50b54dc268dc014dc2784aff0115', '1301427916441128962', NULL, '2020-12-02 14:57:41', '127.0.0.1, 192.168.19.151,172.18.0.7');
INSERT INTO `sys_role_permission` VALUES ('2c928086762238810176223e0e990015', '8a6e50b54dc268dc014dc2784aff0115', '1301001912425910273', NULL, '2020-12-02 14:57:41', '127.0.0.1, 192.168.19.151,172.18.0.7');
INSERT INTO `sys_role_permission` VALUES ('2c928086762238810176223e0e990016', '8a6e50b54dc268dc014dc2784aff0115', '1310422936447881217', NULL, '2020-12-02 14:57:41', '127.0.0.1, 192.168.19.151,172.18.0.7');
INSERT INTO `sys_role_permission` VALUES ('2c928086762238810176223e0e990017', '8a6e50b54dc268dc014dc2784aff0115', '1310432505559519233', NULL, '2020-12-02 14:57:41', '127.0.0.1, 192.168.19.151,172.18.0.7');
INSERT INTO `sys_role_permission` VALUES ('2c928086762238810176223e0e990018', '8a6e50b54dc268dc014dc2784aff0115', '1331871850078146562', NULL, '2020-12-02 14:57:41', '127.0.0.1, 192.168.19.151,172.18.0.7');
INSERT INTO `sys_role_permission` VALUES ('2c928086762238810176223e0e99001d', '8a6e50b54dc268dc014dc2784aff0115', '1301428144502214658', NULL, '2020-12-02 14:57:41', '127.0.0.1, 192.168.19.151,172.18.0.7');
INSERT INTO `sys_role_permission` VALUES ('2c928086762238810176223e0e99001e', '8a6e50b54dc268dc014dc2784aff0115', '1310416488150077442', NULL, '2020-12-02 14:57:41', '127.0.0.1, 192.168.19.151,172.18.0.7');
INSERT INTO `sys_role_permission` VALUES ('2c928086762238810176223e0e99001f', '8a6e50b54dc268dc014dc2784aff0115', '1310432897492062210', NULL, '2020-12-02 14:57:41', '127.0.0.1, 192.168.19.151,172.18.0.7');
INSERT INTO `sys_role_permission` VALUES ('2c928086762238810176223e0e990020', '8a6e50b54dc268dc014dc2784aff0115', '1331872709646225410', NULL, '2020-12-02 14:57:41', '127.0.0.1, 192.168.19.151,172.18.0.7');
INSERT INTO `sys_role_permission` VALUES ('2c928086762238810176223e0e990022', '8a6e50b54dc268dc014dc2784aff0115', '1304321289430765570', NULL, '2020-12-02 14:57:41', '127.0.0.1, 192.168.19.151,172.18.0.7');
INSERT INTO `sys_role_permission` VALUES ('2c928086762238810176223e0e990023', '8a6e50b54dc268dc014dc2784aff0115', '1310416188978761730', NULL, '2020-12-02 14:57:41', '127.0.0.1, 192.168.19.151,172.18.0.7');
INSERT INTO `sys_role_permission` VALUES ('2c928086762238810176223e0e990024', '8a6e50b54dc268dc014dc2784aff0115', '1310433274404802561', NULL, '2020-12-02 14:57:41', '127.0.0.1, 192.168.19.151,172.18.0.7');
INSERT INTO `sys_role_permission` VALUES ('2c928086762238810176223e0e990026', '8a6e50b54dc268dc014dc2784aff0115', '1310415795251056642', NULL, '2020-12-02 14:57:41', '127.0.0.1, 192.168.19.151,172.18.0.7');
INSERT INTO `sys_role_permission` VALUES ('2c928086762238810176223fd5f60027', '8a6e50b54dc268dc014dc2784aff0115', '1310402513266020353', NULL, '2020-12-02 14:59:38', '127.0.0.1, 192.168.19.151,172.18.0.7');
INSERT INTO `sys_role_permission` VALUES ('2c928086762238810176223fd5f60028', '8a6e50b54dc268dc014dc2784aff0115', '1310404050755260417', NULL, '2020-12-02 14:59:38', '127.0.0.1, 192.168.19.151,172.18.0.7');
INSERT INTO `sys_role_permission` VALUES ('2c928086762238810176223fd5f60029', '8a6e50b54dc268dc014dc2784aff0115', '1310404258897596417', NULL, '2020-12-02 14:59:38', '127.0.0.1, 192.168.19.151,172.18.0.7');
INSERT INTO `sys_role_permission` VALUES ('2c928086762238810176223fd5f6002a', '8a6e50b54dc268dc014dc2784aff0115', '1310400302846840834', NULL, '2020-12-02 14:59:38', '127.0.0.1, 192.168.19.151,172.18.0.7');
INSERT INTO `sys_role_permission` VALUES ('2c928086762238810176223fd5f6002b', '8a6e50b54dc268dc014dc2784aff0115', '1310433915395117057', NULL, '2020-12-02 14:59:38', '127.0.0.1, 192.168.19.151,172.18.0.7');
INSERT INTO `sys_role_permission` VALUES ('2c928086762238810176223fd5f6002c', '8a6e50b54dc268dc014dc2784aff0115', '1310403041836404738', NULL, '2020-12-02 14:59:38', '127.0.0.1, 192.168.19.151,172.18.0.7');
INSERT INTO `sys_role_permission` VALUES ('2c928086762238810176223fd5f6002d', '8a6e50b54dc268dc014dc2784aff0115', '1310400531214110721', NULL, '2020-12-02 14:59:38', '127.0.0.1, 192.168.19.151,172.18.0.7');
INSERT INTO `sys_role_permission` VALUES ('2c928086762238810176223fd5f6002e', '8a6e50b54dc268dc014dc2784aff0115', '1310403230580084737', NULL, '2020-12-02 14:59:38', '127.0.0.1, 192.168.19.151,172.18.0.7');
INSERT INTO `sys_role_permission` VALUES ('2c928086762238810176223fd5f6002f', '8a6e50b54dc268dc014dc2784aff0115', '1310404620442406913', NULL, '2020-12-02 14:59:38', '127.0.0.1, 192.168.19.151,172.18.0.7');
INSERT INTO `sys_role_permission` VALUES ('2c928086762238810176223fd5f60030', '8a6e50b54dc268dc014dc2784aff0115', '1310400808977698817', NULL, '2020-12-02 14:59:38', '127.0.0.1, 192.168.19.151,172.18.0.7');
INSERT INTO `sys_role_permission` VALUES ('2c928086762238810176223fd5f60031', '8a6e50b54dc268dc014dc2784aff0115', '1310403473635807234', NULL, '2020-12-02 14:59:38', '127.0.0.1, 192.168.19.151,172.18.0.7');
INSERT INTO `sys_role_permission` VALUES ('2c928086762238810176223fd5f60032', '8a6e50b54dc268dc014dc2784aff0115', '1310404991655088130', NULL, '2020-12-02 14:59:38', '127.0.0.1, 192.168.19.151,172.18.0.7');
INSERT INTO `sys_role_permission` VALUES ('2c928086762238810176223fd5f60033', '8a6e50b54dc268dc014dc2784aff0115', '1310401085768208385', NULL, '2020-12-02 14:59:38', '127.0.0.1, 192.168.19.151,172.18.0.7');
INSERT INTO `sys_role_permission` VALUES ('2c928086762238810176223fd5f60034', '8a6e50b54dc268dc014dc2784aff0115', '1310405261181063170', NULL, '2020-12-02 14:59:38', '127.0.0.1, 192.168.19.151,172.18.0.7');
INSERT INTO `sys_role_permission` VALUES ('2c928086762238810176223fd5f60035', '8a6e50b54dc268dc014dc2784aff0115', '1310403837600731138', NULL, '2020-12-02 14:59:38', '127.0.0.1, 192.168.19.151,172.18.0.7');
INSERT INTO `sys_role_permission` VALUES ('2c928086762238810176223fd5f60036', '8a6e50b54dc268dc014dc2784aff0115', '1310401229901271042', NULL, '2020-12-02 14:59:38', '127.0.0.1, 192.168.19.151,172.18.0.7');
INSERT INTO `sys_role_permission` VALUES ('2c928086762238810176223fd5f60037', '8a6e50b54dc268dc014dc2784aff0115', '1310405668003385346', NULL, '2020-12-02 14:59:38', '127.0.0.1, 192.168.19.151,172.18.0.7');
INSERT INTO `sys_role_permission` VALUES ('2c928086762238810176223fd5f60038', '8a6e50b54dc268dc014dc2784aff0115', '1310401446956503042', NULL, '2020-12-02 14:59:38', '127.0.0.1, 192.168.19.151,172.18.0.7');
INSERT INTO `sys_role_permission` VALUES ('2c928086762238810176223fd5f60039', '8a6e50b54dc268dc014dc2784aff0115', '1310401620621660162', NULL, '2020-12-02 14:59:38', '127.0.0.1, 192.168.19.151,172.18.0.7');
INSERT INTO `sys_role_permission` VALUES ('2c928086762238810176223fd5f6003a', '8a6e50b54dc268dc014dc2784aff0115', '1310406050767179777', NULL, '2020-12-02 14:59:38', '127.0.0.1, 192.168.19.151,172.18.0.7');
INSERT INTO `sys_role_permission` VALUES ('2c928086762238810176223fd5f6003b', '8a6e50b54dc268dc014dc2784aff0115', '1310401771595632641', NULL, '2020-12-02 14:59:38', '127.0.0.1, 192.168.19.151,172.18.0.7');
INSERT INTO `sys_role_permission` VALUES ('2c928086762238810176223fd5f6003c', '8a6e50b54dc268dc014dc2784aff0115', '1310406264022372354', NULL, '2020-12-02 14:59:38', '127.0.0.1, 192.168.19.151,172.18.0.7');
INSERT INTO `sys_role_permission` VALUES ('2c928086762238810176223fd5f6003d', '8a6e50b54dc268dc014dc2784aff0115', '1310401950704996353', NULL, '2020-12-02 14:59:38', '127.0.0.1, 192.168.19.151,172.18.0.7');
INSERT INTO `sys_role_permission` VALUES ('2c928086762238810176223fd5f6003e', '8a6e50b54dc268dc014dc2784aff0115', '1310406614360002562', NULL, '2020-12-02 14:59:38', '127.0.0.1, 192.168.19.151,172.18.0.7');
INSERT INTO `sys_role_permission` VALUES ('2c9280867622388101762240b909003f', '8a6e50b54dc268dc014dc2784aff0115', '1301001284265971713', NULL, '2020-12-02 15:00:36', '127.0.0.1, 192.168.19.151,172.18.0.7');
INSERT INTO `sys_role_permission` VALUES ('2c928086762251740176225174e20000', '8a6e50b54dc268dc014dc2784aff0115', '1301034658502283266', NULL, '2020-12-02 15:18:52', '127.0.0.1, 192.168.19.151,172.18.0.7');
INSERT INTO `sys_role_permission` VALUES ('2c928086762251740176225174e20001', '8a6e50b54dc268dc014dc2784aff0115', '1310409534656745473', NULL, '2020-12-02 15:18:52', '127.0.0.1, 192.168.19.151,172.18.0.7');
INSERT INTO `sys_role_permission` VALUES ('2c928086762251740176225174e20002', '8a6e50b54dc268dc014dc2784aff0115', '1310409822184673282', NULL, '2020-12-02 15:18:52', '127.0.0.1, 192.168.19.151,172.18.0.7');
INSERT INTO `sys_role_permission` VALUES ('2c928086762251740176225174e20003', '8a6e50b54dc268dc014dc2784aff0115', '1310410017085591553', NULL, '2020-12-02 15:18:52', '127.0.0.1, 192.168.19.151,172.18.0.7');
INSERT INTO `sys_role_permission` VALUES ('2c92808676225ac10176225ac1980000', '8a6e50b54dc268dc014dc2784aff0115', '1310410227111170050', NULL, '2020-12-02 15:29:02', '127.0.0.1, 192.168.19.151,172.18.0.7');
INSERT INTO `sys_role_permission` VALUES ('2c92808676225ac10176225ac1980001', '8a6e50b54dc268dc014dc2784aff0115', '1310410737562161153', NULL, '2020-12-02 15:29:02', '127.0.0.1, 192.168.19.151,172.18.0.7');
INSERT INTO `sys_role_permission` VALUES ('2c92808676225ac10176225ac1980002', '8a6e50b54dc268dc014dc2784aff0115', '1310413060296413185', NULL, '2020-12-02 15:29:02', '127.0.0.1, 192.168.19.151,172.18.0.7');
INSERT INTO `sys_role_permission` VALUES ('2c92808676225ac10176225ac1980003', '8a6e50b54dc268dc014dc2784aff0115', '1310410534536876033', NULL, '2020-12-02 15:29:02', '127.0.0.1, 192.168.19.151,172.18.0.7');
INSERT INTO `sys_role_permission` VALUES ('2c92808676225ac10176225ac1980004', '8a6e50b54dc268dc014dc2784aff0115', '1310412463300153345', NULL, '2020-12-02 15:29:02', '127.0.0.1, 192.168.19.151,172.18.0.7');
INSERT INTO `sys_role_permission` VALUES ('2c92808676225ac10176225ac1980005', '8a6e50b54dc268dc014dc2784aff0115', '1310413263179091969', NULL, '2020-12-02 15:29:02', '127.0.0.1, 192.168.19.151,172.18.0.7');
INSERT INTO `sys_role_permission` VALUES ('2c928087762310e101762310e1d50000', '8a6e50b54dc268dc014dc2784aff0115', '1301447721621655554', NULL, '2020-12-02 18:47:58', '127.0.0.1, 192.168.19.151,172.18.0.5');
INSERT INTO `sys_role_permission` VALUES ('2c9280877623513c017623513d090002', 'ff8080817608b7940176214221730206', '1301427916441128962', NULL, '2020-12-02 19:58:15', '192.168.214.162,172.18.0.5');
INSERT INTO `sys_role_permission` VALUES ('2c9280877623513c017623513d090003', 'ff8080817608b7940176214221730206', 'ff8080817608b794017622acfc5d0479', NULL, '2020-12-02 19:58:15', '192.168.214.162,172.18.0.5');
INSERT INTO `sys_role_permission` VALUES ('2c9280877623513c017623513d090004', 'ff8080817608b7940176214221730206', '1301034916112240641', NULL, '2020-12-02 19:58:15', '192.168.214.162,172.18.0.5');
INSERT INTO `sys_role_permission` VALUES ('2c9280877623513c017623513d090005', 'ff8080817608b7940176214221730206', 'ff8080817608b794017622aadd20045d', NULL, '2020-12-02 19:58:15', '192.168.214.162,172.18.0.5');
INSERT INTO `sys_role_permission` VALUES ('2c9280877623513c017623513d090006', 'ff8080817608b7940176214221730206', '1301096112848900098', NULL, '2020-12-02 19:58:15', '192.168.214.162,172.18.0.5');
INSERT INTO `sys_role_permission` VALUES ('2c9280877623513c017623513d090007', 'ff8080817608b7940176214221730206', '1301001912425910273', NULL, '2020-12-02 19:58:15', '192.168.214.162,172.18.0.5');
INSERT INTO `sys_role_permission` VALUES ('2c9280877623513c017623513d090008', 'ff8080817608b7940176214221730206', '1301428144502214658', NULL, '2020-12-02 19:58:15', '192.168.214.162,172.18.0.5');
INSERT INTO `sys_role_permission` VALUES ('2c9280877623513c017623513d090009', 'ff8080817608b7940176214221730206', 'ff8080817608b794017622adde850480', NULL, '2020-12-02 19:58:15', '192.168.214.162,172.18.0.5');
INSERT INTO `sys_role_permission` VALUES ('2c9280877623513c017623513d09000a', 'ff8080817608b7940176214221730206', '1304321289430765570', NULL, '2020-12-02 19:58:15', '192.168.214.162,172.18.0.5');
INSERT INTO `sys_role_permission` VALUES ('2c9280877623513c017623513d09000b', 'ff8080817608b7940176214221730206', 'ff8080817608b794017622ae6de30487', NULL, '2020-12-02 19:58:15', '192.168.214.162,172.18.0.5');
INSERT INTO `sys_role_permission` VALUES ('2c9280877623513c017623513d09000c', 'ff8080817608b7940176214221730206', 'ff8080817608b794017622aecbde048e', NULL, '2020-12-02 19:58:15', '192.168.214.162,172.18.0.5');
INSERT INTO `sys_role_permission` VALUES ('2c9280877623513c017623513d09000d', 'ff8080817608b7940176214221730206', '1310416188978761730', NULL, '2020-12-02 19:58:15', '192.168.214.162,172.18.0.5');
INSERT INTO `sys_role_permission` VALUES ('2c9280877623513c017623513d09000e', 'ff8080817608b7940176214221730206', 'ff8080817608b794017622af287d0495', NULL, '2020-12-02 19:58:15', '192.168.214.162,172.18.0.5');
INSERT INTO `sys_role_permission` VALUES ('2c9280877623513c017623513d09000f', 'ff8080817608b7940176214221730206', '1310415795251056642', NULL, '2020-12-02 19:58:15', '192.168.214.162,172.18.0.5');
INSERT INTO `sys_role_permission` VALUES ('2c9280877623513c017623513d090010', 'ff8080817608b7940176214221730206', '1300999295377346562', NULL, '2020-12-02 19:58:15', '192.168.214.162,172.18.0.5');
INSERT INTO `sys_role_permission` VALUES ('2c9280877623513c017623513d090011', 'ff8080817608b7940176214221730206', '1301000181361152001', NULL, '2020-12-02 19:58:15', '192.168.214.162,172.18.0.5');
INSERT INTO `sys_role_permission` VALUES ('2c9280877626819601762681961b0000', '8a6e50b54dc268dc014dc2784aff0115', 'ff8080817608b794017622b01610049c', NULL, '2020-12-03 10:49:55', '192.168.214.162,172.18.0.5');
INSERT INTO `sys_role_permission` VALUES ('2c9280877626819601762681961b0001', '8a6e50b54dc268dc014dc2784aff0115', 'ff8080817608b794017626441d2207df', NULL, '2020-12-03 10:49:55', '192.168.214.162,172.18.0.5');
INSERT INTO `sys_role_permission` VALUES ('2c9280877626819601762681961b0002', '8a6e50b54dc268dc014dc2784aff0115', 'ff8080817608b794017622aa75b10456', NULL, '2020-12-03 10:49:55', '192.168.214.162,172.18.0.5');
INSERT INTO `sys_role_permission` VALUES ('2c9280877626819601762681961b0003', '8a6e50b54dc268dc014dc2784aff0115', 'ff8080817608b794017622acfc5d0479', NULL, '2020-12-03 10:49:55', '192.168.214.162,172.18.0.5');
INSERT INTO `sys_role_permission` VALUES ('2c9280877626819601762681961b0004', '8a6e50b54dc268dc014dc2784aff0115', 'ff8080817608b794017622b0fc3504a3', NULL, '2020-12-03 10:49:55', '192.168.214.162,172.18.0.5');
INSERT INTO `sys_role_permission` VALUES ('2c9280877626819601762681961b0005', '8a6e50b54dc268dc014dc2784aff0115', 'ff8080817608b794017622aadd20045d', NULL, '2020-12-03 10:49:55', '192.168.214.162,172.18.0.5');
INSERT INTO `sys_role_permission` VALUES ('2c9280877626819601762681961b0006', '8a6e50b54dc268dc014dc2784aff0115', 'ff8080817608b794017622adde850480', NULL, '2020-12-03 10:49:55', '192.168.214.162,172.18.0.5');
INSERT INTO `sys_role_permission` VALUES ('2c9280877626819601762681961b0007', '8a6e50b54dc268dc014dc2784aff0115', 'ff8080817608b794017622ae6de30487', NULL, '2020-12-03 10:49:55', '192.168.214.162,172.18.0.5');
INSERT INTO `sys_role_permission` VALUES ('2c9280877626819601762681961b0008', '8a6e50b54dc268dc014dc2784aff0115', 'ff8080817608b794017622b168db04aa', NULL, '2020-12-03 10:49:55', '192.168.214.162,172.18.0.5');
INSERT INTO `sys_role_permission` VALUES ('2c9280877626819601762681961b0009', '8a6e50b54dc268dc014dc2784aff0115', 'ff8080817608b794017622ab44760464', NULL, '2020-12-03 10:49:55', '192.168.214.162,172.18.0.5');
INSERT INTO `sys_role_permission` VALUES ('2c9280877626819601762681961b000a', '8a6e50b54dc268dc014dc2784aff0115', 'ff8080817608b794017622aecbde048e', NULL, '2020-12-03 10:49:55', '192.168.214.162,172.18.0.5');
INSERT INTO `sys_role_permission` VALUES ('2c9280877626819601762681961b000b', '8a6e50b54dc268dc014dc2784aff0115', 'ff8080817608b794017622b1ef1f04b1', NULL, '2020-12-03 10:49:55', '192.168.214.162,172.18.0.5');
INSERT INTO `sys_role_permission` VALUES ('2c9280877626819601762681961b000c', '8a6e50b54dc268dc014dc2784aff0115', 'ff8080817608b794017622af287d0495', NULL, '2020-12-03 10:49:55', '192.168.214.162,172.18.0.5');
INSERT INTO `sys_role_permission` VALUES ('2c9280877626819601762681961b000d', '8a6e50b54dc268dc014dc2784aff0115', 'ff8080817608b794017622b2a1e404b8', NULL, '2020-12-03 10:49:55', '192.168.214.162,172.18.0.5');
INSERT INTO `sys_role_permission` VALUES ('2c9280877626819601762681961b000e', '8a6e50b54dc268dc014dc2784aff0115', 'ff8080817608b794017622b33a5004bf', NULL, '2020-12-03 10:49:55', '192.168.214.162,172.18.0.5');
INSERT INTO `sys_role_permission` VALUES ('2c9280877626819601762681961b000f', '8a6e50b54dc268dc014dc2784aff0115', 'ff8080817608b794017622b3a2bc04c6', NULL, '2020-12-03 10:49:55', '192.168.214.162,172.18.0.5');
INSERT INTO `sys_role_permission` VALUES ('2c9280877626819601762681961b0010', '8a6e50b54dc268dc014dc2784aff0115', 'ff8080817608b794017622b40d7f04cd', NULL, '2020-12-03 10:49:55', '192.168.214.162,172.18.0.5');
INSERT INTO `sys_role_permission` VALUES ('2c9280877626819601762681961b0011', '8a6e50b54dc268dc014dc2784aff0115', 'ff8080817608b794017622b4b9e004d4', NULL, '2020-12-03 10:49:55', '192.168.214.162,172.18.0.5');
INSERT INTO `sys_role_permission` VALUES ('2c9280877626819601762681961b0012', '8a6e50b54dc268dc014dc2784aff0115', 'ff8080817608b794017622b5129d04db', NULL, '2020-12-03 10:49:55', '192.168.214.162,172.18.0.5');
INSERT INTO `sys_role_permission` VALUES ('2c928087762742e801762742ea350002', 'ff8080817608b79401762695e474081a', 'ff8080817608b7940176231c8cb806bb', NULL, '2020-12-03 14:21:05', '192.168.214.162,172.18.0.5');
INSERT INTO `sys_role_permission` VALUES ('2c928087762742e801762742ea360003', 'ff8080817608b79401762695e474081a', 'ff8080817608b79401762321ae9706de', NULL, '2020-12-03 14:21:05', '192.168.214.162,172.18.0.5');
INSERT INTO `sys_role_permission` VALUES ('2c928087762742e801762742ea360004', 'ff8080817608b79401762695e474081a', 'ff8080817608b79401762323212606ec', NULL, '2020-12-03 14:21:05', '192.168.214.162,172.18.0.5');
INSERT INTO `sys_role_permission` VALUES ('2c928087762742e801762742ea360005', 'ff8080817608b79401762695e474081a', 'ff8080817608b7940176232506e006f5', NULL, '2020-12-03 14:21:05', '192.168.214.162,172.18.0.5');
INSERT INTO `sys_role_permission` VALUES ('2c928087762742e801762742ea360006', 'ff8080817608b79401762695e474081a', 'ff8080817608b79401762325ce8c06fc', NULL, '2020-12-03 14:21:05', '192.168.214.162,172.18.0.5');
INSERT INTO `sys_role_permission` VALUES ('2c928087762742e801762742ea360007', 'ff8080817608b79401762695e474081a', 'ff8080817608b7940176237f207007c3', NULL, '2020-12-03 14:21:05', '192.168.214.162,172.18.0.5');
INSERT INTO `sys_role_permission` VALUES ('2c928087762742e801762742ea360008', 'ff8080817608b79401762695e474081a', 'ff8080817608b7940176237f842307ca', NULL, '2020-12-03 14:21:05', '192.168.214.162,172.18.0.5');
INSERT INTO `sys_role_permission` VALUES ('2c928087762742e801762742ea360009', 'ff8080817608b79401762695e474081a', 'ff8080817608b794017622acfc5d0479', NULL, '2020-12-03 14:21:05', '192.168.214.162,172.18.0.5');
INSERT INTO `sys_role_permission` VALUES ('2c928087762742e801762742ea36000a', 'ff8080817608b79401762695e474081a', 'ff8080817608b794017622b01610049c', NULL, '2020-12-03 14:21:05', '192.168.214.162,172.18.0.5');
INSERT INTO `sys_role_permission` VALUES ('2c928087762742e801762742ea36000b', 'ff8080817608b79401762695e474081a', 'ff8080817608b794017622d319d80589', NULL, '2020-12-03 14:21:05', '192.168.214.162,172.18.0.5');
INSERT INTO `sys_role_permission` VALUES ('2c928087762742e801762742ea36000c', 'ff8080817608b79401762695e474081a', 'ff8080817608b79401762301ef720619', NULL, '2020-12-03 14:21:05', '192.168.214.162,172.18.0.5');
INSERT INTO `sys_role_permission` VALUES ('2c928087762742e801762742ea36000d', 'ff8080817608b79401762695e474081a', 'ff8080817608b79401762303bd0b0627', NULL, '2020-12-03 14:21:05', '192.168.214.162,172.18.0.5');
INSERT INTO `sys_role_permission` VALUES ('2c928087762742e801762742ea36000e', 'ff8080817608b79401762695e474081a', 'ff8080817608b794017623079ad1062e', NULL, '2020-12-03 14:21:05', '192.168.214.162,172.18.0.5');
INSERT INTO `sys_role_permission` VALUES ('2c928087762742e801762742ea36000f', 'ff8080817608b79401762695e474081a', 'ff8080817608b7940176230a57090643', NULL, '2020-12-03 14:21:05', '192.168.214.162,172.18.0.5');
INSERT INTO `sys_role_permission` VALUES ('2c928087762742e801762742ea360010', 'ff8080817608b79401762695e474081a', 'ff8080817608b794017623174dc606a9', NULL, '2020-12-03 14:21:05', '192.168.214.162,172.18.0.5');
INSERT INTO `sys_role_permission` VALUES ('2c928087762742e801762742ea360011', 'ff8080817608b79401762695e474081a', 'ff8080817608b7940176231e34f206c9', NULL, '2020-12-03 14:21:05', '192.168.214.162,172.18.0.5');
INSERT INTO `sys_role_permission` VALUES ('2c928087762742e801762742ea360012', 'ff8080817608b79401762695e474081a', 'ff8080817608b79401762320acf806d0', NULL, '2020-12-03 14:21:05', '192.168.214.162,172.18.0.5');
INSERT INTO `sys_role_permission` VALUES ('2c928087762742e801762742ea360013', 'ff8080817608b79401762695e474081a', '1310400302846840834', NULL, '2020-12-03 14:21:05', '192.168.214.162,172.18.0.5');
INSERT INTO `sys_role_permission` VALUES ('2c928087762742e801762742ea360014', 'ff8080817608b79401762695e474081a', 'ff8080817608b7940176232accf3071f', NULL, '2020-12-03 14:21:05', '192.168.214.162,172.18.0.5');
INSERT INTO `sys_role_permission` VALUES ('2c928087762742e801762742ea360015', 'ff8080817608b79401762695e474081a', 'ff8080817608b7940176232b74900726', NULL, '2020-12-03 14:21:05', '192.168.214.162,172.18.0.5');
INSERT INTO `sys_role_permission` VALUES ('2c928087762742e801762742ea360016', 'ff8080817608b79401762695e474081a', 'ff8080817608b7940176232e2a7e072d', NULL, '2020-12-03 14:21:05', '192.168.214.162,172.18.0.5');
INSERT INTO `sys_role_permission` VALUES ('2c928087762742e801762742ea360017', 'ff8080817608b79401762695e474081a', 'ff8080817608b7940176232fe543073b', NULL, '2020-12-03 14:21:05', '192.168.214.162,172.18.0.5');
INSERT INTO `sys_role_permission` VALUES ('2c928087762742e801762742ea360018', 'ff8080817608b79401762695e474081a', 'ff8080817608b7940176233090fe0742', NULL, '2020-12-03 14:21:05', '192.168.214.162,172.18.0.5');
INSERT INTO `sys_role_permission` VALUES ('2c928087762742e801762742ea360019', 'ff8080817608b79401762695e474081a', '1310404050755260417', NULL, '2020-12-03 14:21:05', '192.168.214.162,172.18.0.5');
INSERT INTO `sys_role_permission` VALUES ('2c928087762742e801762742ea36001a', 'ff8080817608b79401762695e474081a', '1310404258897596417', NULL, '2020-12-03 14:21:05', '192.168.214.162,172.18.0.5');
INSERT INTO `sys_role_permission` VALUES ('2c928087762742e801762742ea36001b', 'ff8080817608b79401762695e474081a', 'ff8080817608b7940176233441100760', NULL, '2020-12-03 14:21:05', '192.168.214.162,172.18.0.5');
INSERT INTO `sys_role_permission` VALUES ('2c928087762742e801762742ea36001c', 'ff8080817608b79401762695e474081a', 'ff8080817608b7940176233786410775', NULL, '2020-12-03 14:21:05', '192.168.214.162,172.18.0.5');
INSERT INTO `sys_role_permission` VALUES ('2c928087762742e801762742ea36001d', 'ff8080817608b79401762695e474081a', '1301001284265971713', NULL, '2020-12-03 14:21:05', '192.168.214.162,172.18.0.5');
INSERT INTO `sys_role_permission` VALUES ('2c928087762742e801762742ea36001e', 'ff8080817608b79401762695e474081a', '1301001622897299458', NULL, '2020-12-03 14:21:05', '192.168.214.162,172.18.0.5');
INSERT INTO `sys_role_permission` VALUES ('2c928087762742e801762742ea36001f', 'ff8080817608b79401762695e474081a', '1301034658502283266', NULL, '2020-12-03 14:21:05', '192.168.214.162,172.18.0.5');
INSERT INTO `sys_role_permission` VALUES ('2c928087762742e801762742ea360020', 'ff8080817608b79401762695e474081a', '1301034916112240641', NULL, '2020-12-03 14:21:05', '192.168.214.162,172.18.0.5');
INSERT INTO `sys_role_permission` VALUES ('2c928087762742e801762742ea360021', 'ff8080817608b79401762695e474081a', '1301077101432016897', NULL, '2020-12-03 14:21:05', '192.168.214.162,172.18.0.5');
INSERT INTO `sys_role_permission` VALUES ('2c928087762742e801762742ea360022', 'ff8080817608b79401762695e474081a', '1301427338168242177', NULL, '2020-12-03 14:21:05', '192.168.214.162,172.18.0.5');
INSERT INTO `sys_role_permission` VALUES ('2c928087762742e801762742ea360023', 'ff8080817608b79401762695e474081a', 'ff8080817608b79401762338d44f077c', NULL, '2020-12-03 14:21:05', '192.168.214.162,172.18.0.5');
INSERT INTO `sys_role_permission` VALUES ('2c928087762742e801762742ea360024', 'ff8080817608b79401762695e474081a', '1301427916441128962', NULL, '2020-12-03 14:21:05', '192.168.214.162,172.18.0.5');
INSERT INTO `sys_role_permission` VALUES ('2c928087762742e801762742ea360025', 'ff8080817608b79401762695e474081a', '1310418138898108418', NULL, '2020-12-03 14:21:05', '192.168.214.162,172.18.0.5');
INSERT INTO `sys_role_permission` VALUES ('2c928087762742e801762742ea360026', 'ff8080817608b79401762695e474081a', 'ff8080817608b79401762350c02007ae', NULL, '2020-12-03 14:21:05', '192.168.214.162,172.18.0.5');
INSERT INTO `sys_role_permission` VALUES ('2c928087762742e801762742ea360027', 'ff8080817608b79401762695e474081a', 'ff8080817608b79401762642cebb07d8', NULL, '2020-12-03 14:21:05', '192.168.214.162,172.18.0.5');
INSERT INTO `sys_role_permission` VALUES ('2c928087762742e801762742ea360028', 'ff8080817608b79401762695e474081a', 'ff8080817608b794017626441d2207df', NULL, '2020-12-03 14:21:05', '192.168.214.162,172.18.0.5');
INSERT INTO `sys_role_permission` VALUES ('2c928087762742e801762742ea360029', 'ff8080817608b79401762695e474081a', '1331870906774982658', NULL, '2020-12-03 14:21:05', '192.168.214.162,172.18.0.5');
INSERT INTO `sys_role_permission` VALUES ('2c928087762742e801762742ea36002a', 'ff8080817608b79401762695e474081a', '1310402513266020353', NULL, '2020-12-03 14:21:05', '192.168.214.162,172.18.0.5');
INSERT INTO `sys_role_permission` VALUES ('2c928087762742e801762742ea36002b', 'ff8080817608b79401762695e474081a', '1310410227111170050', NULL, '2020-12-03 14:21:05', '192.168.214.162,172.18.0.5');
INSERT INTO `sys_role_permission` VALUES ('2c928087762742e801762742ea36002c', 'ff8080817608b79401762695e474081a', '1310410737562161153', NULL, '2020-12-03 14:21:05', '192.168.214.162,172.18.0.5');
INSERT INTO `sys_role_permission` VALUES ('2c928087762742e801762742ea36002d', 'ff8080817608b79401762695e474081a', '1310413060296413185', NULL, '2020-12-03 14:21:05', '192.168.214.162,172.18.0.5');
INSERT INTO `sys_role_permission` VALUES ('2c928087762742e801762742ea36002e', 'ff8080817608b79401762695e474081a', 'ff8080817608b794017622aa75b10456', NULL, '2020-12-03 14:21:05', '192.168.214.162,172.18.0.5');
INSERT INTO `sys_role_permission` VALUES ('2c928087762742e801762742ea36002f', 'ff8080817608b79401762695e474081a', 'ff8080817608b794017622b5f5f804e2', NULL, '2020-12-03 14:21:05', '192.168.214.162,172.18.0.5');
INSERT INTO `sys_role_permission` VALUES ('2c928087762742e801762742ea360030', 'ff8080817608b79401762695e474081a', 'ff8080817608b794017622b8706f04fe', NULL, '2020-12-03 14:21:05', '192.168.214.162,172.18.0.5');
INSERT INTO `sys_role_permission` VALUES ('2c928087762742e801762742ea360031', 'ff8080817608b79401762695e474081a', 'ff8080817608b794017622ba7fec0513', NULL, '2020-12-03 14:21:05', '192.168.214.162,172.18.0.5');
INSERT INTO `sys_role_permission` VALUES ('2c928087762742e801762742ea360032', 'ff8080817608b79401762695e474081a', 'ff8080817608b794017622a194750409', NULL, '2020-12-03 14:21:05', '192.168.214.162,172.18.0.5');
INSERT INTO `sys_role_permission` VALUES ('2c928087762742e801762742ea360033', 'ff8080817608b79401762695e474081a', 'ff8080817608b794017622a4f475042c', NULL, '2020-12-03 14:21:05', '192.168.214.162,172.18.0.5');
INSERT INTO `sys_role_permission` VALUES ('2c928087762742e801762742ea360034', 'ff8080817608b79401762695e474081a', 'ff8080817608b794017622a667f4043a', NULL, '2020-12-03 14:21:05', '192.168.214.162,172.18.0.5');
INSERT INTO `sys_role_permission` VALUES ('2c928087762742e801762742ea360035', 'ff8080817608b79401762695e474081a', 'ff8080817608b794017622a6f7330441', NULL, '2020-12-03 14:21:05', '192.168.214.162,172.18.0.5');
INSERT INTO `sys_role_permission` VALUES ('2c928087762742e801762742ea360036', 'ff8080817608b79401762695e474081a', 'ff8080817608b794017622a78ee90448', NULL, '2020-12-03 14:21:05', '192.168.214.162,172.18.0.5');
INSERT INTO `sys_role_permission` VALUES ('2c928087762742e801762742ea360037', 'ff8080817608b79401762695e474081a', 'ff8080817608b794017622c3277c0544', NULL, '2020-12-03 14:21:05', '192.168.214.162,172.18.0.5');
INSERT INTO `sys_role_permission` VALUES ('2c928087762742e801762742ea360038', 'ff8080817608b79401762695e474081a', 'ff8080817608b794017622c3b1b0054b', NULL, '2020-12-03 14:21:05', '192.168.214.162,172.18.0.5');
INSERT INTO `sys_role_permission` VALUES ('2c928087762742e801762742ea370039', 'ff8080817608b79401762695e474081a', 'ff8080817608b794017622c5d9e40552', NULL, '2020-12-03 14:21:05', '192.168.214.162,172.18.0.5');
INSERT INTO `sys_role_permission` VALUES ('2c928087762742e801762742ea37003a', 'ff8080817608b79401762695e474081a', 'ff8080817608b794017622c7ca4b055b', NULL, '2020-12-03 14:21:05', '192.168.214.162,172.18.0.5');
INSERT INTO `sys_role_permission` VALUES ('2c928087762742e801762742ea37003b', 'ff8080817608b79401762695e474081a', 'ff8080817608b794017622cd7a6c056b', NULL, '2020-12-03 14:21:05', '192.168.214.162,172.18.0.5');
INSERT INTO `sys_role_permission` VALUES ('2c928087762742e801762742ea37003c', 'ff8080817608b79401762695e474081a', 'ff8080817608b794017622ce912b0572', NULL, '2020-12-03 14:21:05', '192.168.214.162,172.18.0.5');
INSERT INTO `sys_role_permission` VALUES ('2c928087762742e801762742ea37003d', 'ff8080817608b79401762695e474081a', 'ff8080817608b794017622d220310580', NULL, '2020-12-03 14:21:05', '192.168.214.162,172.18.0.5');
INSERT INTO `sys_role_permission` VALUES ('2c928087762742e801762742ea37003e', 'ff8080817608b79401762695e474081a', 'ff8080817608b794017622fb507205ed', NULL, '2020-12-03 14:21:05', '192.168.214.162,172.18.0.5');
INSERT INTO `sys_role_permission` VALUES ('2c928087762742e801762742ea37003f', 'ff8080817608b79401762695e474081a', 'ff8080817608b794017622fc714605fb', NULL, '2020-12-03 14:21:05', '192.168.214.162,172.18.0.5');
INSERT INTO `sys_role_permission` VALUES ('2c928087762742e801762742ea370040', 'ff8080817608b79401762695e474081a', 'ff8080817608b794017622fe2a640604', NULL, '2020-12-03 14:21:05', '192.168.214.162,172.18.0.5');
INSERT INTO `sys_role_permission` VALUES ('2c928087762742e801762742ea370041', 'ff8080817608b79401762695e474081a', 'ff8080817608b794017622ff496b060b', NULL, '2020-12-03 14:21:05', '192.168.214.162,172.18.0.5');
INSERT INTO `sys_role_permission` VALUES ('2c928087762742e801762742ea370042', 'ff8080817608b79401762695e474081a', 'ff8080817608b7940176230c9865065a', NULL, '2020-12-03 14:21:05', '192.168.214.162,172.18.0.5');
INSERT INTO `sys_role_permission` VALUES ('2c928087762742e801762742ea370043', 'ff8080817608b79401762695e474081a', 'ff8080817608b7940176230d98b80668', NULL, '2020-12-03 14:21:05', '192.168.214.162,172.18.0.5');
INSERT INTO `sys_role_permission` VALUES ('2c928087762742e801762742ea370044', 'ff8080817608b79401762695e474081a', 'ff8080817608b7940176230f52030676', NULL, '2020-12-03 14:21:05', '192.168.214.162,172.18.0.5');
INSERT INTO `sys_role_permission` VALUES ('2c928087762742e801762742ea370045', 'ff8080817608b79401762695e474081a', 'ff8080817608b794017623131dc70686', NULL, '2020-12-03 14:21:05', '192.168.214.162,172.18.0.5');
INSERT INTO `sys_role_permission` VALUES ('2c928087762742e801762742ea370046', 'ff8080817608b79401762695e474081a', 'ff8080817608b794017623146d78069b', NULL, '2020-12-03 14:21:05', '192.168.214.162,172.18.0.5');
INSERT INTO `sys_role_permission` VALUES ('2c928087762742e801762742ea370047', 'ff8080817608b79401762695e474081a', 'ff8080817608b7940176233ca05a0785', NULL, '2020-12-03 14:21:05', '192.168.214.162,172.18.0.5');
INSERT INTO `sys_role_permission` VALUES ('2c928087762742e801762742ea370048', 'ff8080817608b79401762695e474081a', 'ff8080817608b7940176235acdcc07b5', NULL, '2020-12-03 14:21:05', '192.168.214.162,172.18.0.5');
INSERT INTO `sys_role_permission` VALUES ('2c928087762742e801762742ea370049', 'ff8080817608b79401762695e474081a', 'ff8080817608b7940176238091e707d1', NULL, '2020-12-03 14:21:05', '192.168.214.162,172.18.0.5');
INSERT INTO `sys_role_permission` VALUES ('2c928087762742e801762742ea37004a', 'ff8080817608b79401762695e474081a', 'ff8080817608b794017622b0fc3504a3', NULL, '2020-12-03 14:21:05', '192.168.214.162,172.18.0.5');
INSERT INTO `sys_role_permission` VALUES ('2c928087762742e801762742ea37004b', 'ff8080817608b79401762695e474081a', 'ff8080817608b794017622d35cf30590', NULL, '2020-12-03 14:21:05', '192.168.214.162,172.18.0.5');
INSERT INTO `sys_role_permission` VALUES ('2c928087762742e801762742ea37004c', 'ff8080817608b79401762695e474081a', 'ff8080817608b79401762309244b0635', NULL, '2020-12-03 14:21:05', '192.168.214.162,172.18.0.5');
INSERT INTO `sys_role_permission` VALUES ('2c928087762742e801762742ea37004d', 'ff8080817608b79401762695e474081a', 'ff8080817608b7940176230ac780064a', NULL, '2020-12-03 14:21:05', '192.168.214.162,172.18.0.5');
INSERT INTO `sys_role_permission` VALUES ('2c928087762742e801762742ea37004e', 'ff8080817608b79401762695e474081a', 'ff8080817608b79401762316aaf906a2', NULL, '2020-12-03 14:21:05', '192.168.214.162,172.18.0.5');
INSERT INTO `sys_role_permission` VALUES ('2c928087762742e801762742ea37004f', 'ff8080817608b79401762695e474081a', 'ff8080817608b79401762317d07206b0', NULL, '2020-12-03 14:21:05', '192.168.214.162,172.18.0.5');
INSERT INTO `sys_role_permission` VALUES ('2c928087762742e801762742ea370050', 'ff8080817608b79401762695e474081a', '1310400531214110721', NULL, '2020-12-03 14:21:05', '192.168.214.162,172.18.0.5');
INSERT INTO `sys_role_permission` VALUES ('2c928087762742e801762742ea370051', 'ff8080817608b79401762695e474081a', 'ff8080817608b7940176232eddbf0734', NULL, '2020-12-03 14:21:05', '192.168.214.162,172.18.0.5');
INSERT INTO `sys_role_permission` VALUES ('2c928087762742e801762742ea370052', 'ff8080817608b79401762695e474081a', 'ff8080817608b79401762330faf20749', NULL, '2020-12-03 14:21:05', '192.168.214.162,172.18.0.5');
INSERT INTO `sys_role_permission` VALUES ('2c928087762742e801762742ea370053', 'ff8080817608b79401762695e474081a', '1310409534656745473', NULL, '2020-12-03 14:21:05', '192.168.214.162,172.18.0.5');
INSERT INTO `sys_role_permission` VALUES ('2c928087762742e801762742ea370054', 'ff8080817608b79401762695e474081a', 'ff8080817608b794017623000f7a0612', NULL, '2020-12-03 14:21:05', '192.168.214.162,172.18.0.5');
INSERT INTO `sys_role_permission` VALUES ('2c928087762742e801762742ea370055', 'ff8080817608b79401762695e474081a', 'ff8080817608b79401762334aa6c0767', NULL, '2020-12-03 14:21:05', '192.168.214.162,172.18.0.5');
INSERT INTO `sys_role_permission` VALUES ('2c928087762742e801762742ea370056', 'ff8080817608b79401762695e474081a', '1301001912425910273', NULL, '2020-12-03 14:21:05', '192.168.214.162,172.18.0.5');
INSERT INTO `sys_role_permission` VALUES ('2c928087762742e801762742ea370057', 'ff8080817608b79401762695e474081a', '1301088665497243650', NULL, '2020-12-03 14:21:05', '192.168.214.162,172.18.0.5');
INSERT INTO `sys_role_permission` VALUES ('2c928087762742e801762742ea370058', 'ff8080817608b79401762695e474081a', '1301096112848900098', NULL, '2020-12-03 14:21:05', '192.168.214.162,172.18.0.5');
INSERT INTO `sys_role_permission` VALUES ('2c928087762742e801762742ea370059', 'ff8080817608b79401762695e474081a', '1301428144502214658', NULL, '2020-12-03 14:21:05', '192.168.214.162,172.18.0.5');
INSERT INTO `sys_role_permission` VALUES ('2c928087762742e801762742ea37005a', 'ff8080817608b79401762695e474081a', '1310422936447881217', NULL, '2020-12-03 14:21:05', '192.168.214.162,172.18.0.5');
INSERT INTO `sys_role_permission` VALUES ('2c928087762742e801762742ea37005b', 'ff8080817608b79401762695e474081a', '1310432505559519233', NULL, '2020-12-03 14:21:05', '192.168.214.162,172.18.0.5');
INSERT INTO `sys_role_permission` VALUES ('2c928087762742e801762742ea37005d', 'ff8080817608b79401762695e474081a', '1331871850078146562', NULL, '2020-12-03 14:21:05', '192.168.214.162,172.18.0.5');
INSERT INTO `sys_role_permission` VALUES ('2c928087762742e801762742ea37005e', 'ff8080817608b79401762695e474081a', '1310403041836404738', NULL, '2020-12-03 14:21:05', '192.168.214.162,172.18.0.5');
INSERT INTO `sys_role_permission` VALUES ('2c928087762742e801762742ea37005f', 'ff8080817608b79401762695e474081a', '1310410534536876033', NULL, '2020-12-03 14:21:05', '192.168.214.162,172.18.0.5');
INSERT INTO `sys_role_permission` VALUES ('2c928087762742e801762742ea370060', 'ff8080817608b79401762695e474081a', '1310412463300153345', NULL, '2020-12-03 14:21:05', '192.168.214.162,172.18.0.5');
INSERT INTO `sys_role_permission` VALUES ('2c928087762742e801762742ea370061', 'ff8080817608b79401762695e474081a', '1310413263179091969', NULL, '2020-12-03 14:21:05', '192.168.214.162,172.18.0.5');
INSERT INTO `sys_role_permission` VALUES ('2c928087762742e801762742ea370062', 'ff8080817608b79401762695e474081a', 'ff8080817608b794017622aadd20045d', NULL, '2020-12-03 14:21:05', '192.168.214.162,172.18.0.5');
INSERT INTO `sys_role_permission` VALUES ('2c928087762742e801762742ea370063', 'ff8080817608b79401762695e474081a', 'ff8080817608b794017622adde850480', NULL, '2020-12-03 14:21:05', '192.168.214.162,172.18.0.5');
INSERT INTO `sys_role_permission` VALUES ('2c928087762742e801762742ea370064', 'ff8080817608b79401762695e474081a', 'ff8080817608b794017622b65e3b04e9', NULL, '2020-12-03 14:21:05', '192.168.214.162,172.18.0.5');
INSERT INTO `sys_role_permission` VALUES ('2c928087762742e801762742ea370065', 'ff8080817608b79401762695e474081a', 'ff8080817608b794017622b8c8970505', NULL, '2020-12-03 14:21:05', '192.168.214.162,172.18.0.5');
INSERT INTO `sys_role_permission` VALUES ('2c928087762742e801762742ea370066', 'ff8080817608b79401762695e474081a', 'ff8080817608b794017622bb2c40051a', NULL, '2020-12-03 14:21:05', '192.168.214.162,172.18.0.5');
INSERT INTO `sys_role_permission` VALUES ('2c928087762742e801762742ea370067', 'ff8080817608b79401762695e474081a', 'ff8080817608b794017622a2b4b50410', NULL, '2020-12-03 14:21:05', '192.168.214.162,172.18.0.5');
INSERT INTO `sys_role_permission` VALUES ('2c928087762742e801762742ea370068', 'ff8080817608b79401762695e474081a', 'ff8080817608b794017622a5b3190433', NULL, '2020-12-03 14:21:05', '192.168.214.162,172.18.0.5');
INSERT INTO `sys_role_permission` VALUES ('2c928087762742e801762742ea370069', 'ff8080817608b79401762695e474081a', 'ff8080817608b794017622a81cb7044f', NULL, '2020-12-03 14:21:05', '192.168.214.162,172.18.0.5');
INSERT INTO `sys_role_permission` VALUES ('2c928087762742e801762742ea37006a', 'ff8080817608b79401762695e474081a', 'ff8080817608b794017622c83f120562', NULL, '2020-12-03 14:21:05', '192.168.214.162,172.18.0.5');
INSERT INTO `sys_role_permission` VALUES ('2c928087762742e801762742ea37006b', 'ff8080817608b79401762695e474081a', 'ff8080817608b794017622cf13600579', NULL, '2020-12-03 14:21:05', '192.168.214.162,172.18.0.5');
INSERT INTO `sys_role_permission` VALUES ('2c928087762742e801762742ea37006c', 'ff8080817608b79401762695e474081a', 'ff8080817608b794017622fbead705f4', NULL, '2020-12-03 14:21:05', '192.168.214.162,172.18.0.5');
INSERT INTO `sys_role_permission` VALUES ('2c928087762742e801762742ea37006d', 'ff8080817608b79401762695e474081a', 'ff8080817608b7940176230cf6ba0661', NULL, '2020-12-03 14:21:05', '192.168.214.162,172.18.0.5');
INSERT INTO `sys_role_permission` VALUES ('2c928087762742e801762742ea37006e', 'ff8080817608b79401762695e474081a', 'ff8080817608b7940176230e5b8e066f', NULL, '2020-12-03 14:21:05', '192.168.214.162,172.18.0.5');
INSERT INTO `sys_role_permission` VALUES ('2c928087762742e801762742ea37006f', 'ff8080817608b79401762695e474081a', 'ff8080817608b794017623127a25067f', NULL, '2020-12-03 14:21:05', '192.168.214.162,172.18.0.5');
INSERT INTO `sys_role_permission` VALUES ('2c928087762742e801762742ea370070', 'ff8080817608b79401762695e474081a', 'ff8080817608b794017623135c20068d', NULL, '2020-12-03 14:21:05', '192.168.214.162,172.18.0.5');
INSERT INTO `sys_role_permission` VALUES ('2c928087762742e801762742ea370071', 'ff8080817608b79401762695e474081a', 'ff8080817608b7940176231cf7f306c2', NULL, '2020-12-03 14:21:05', '192.168.214.162,172.18.0.5');
INSERT INTO `sys_role_permission` VALUES ('2c928087762742e801762742ea370072', 'ff8080817608b79401762695e474081a', 'ff8080817608b79401762320f71806d7', NULL, '2020-12-03 14:21:05', '192.168.214.162,172.18.0.5');
INSERT INTO `sys_role_permission` VALUES ('2c928087762742e801762742ea380073', 'ff8080817608b79401762695e474081a', 'ff8080817608b79401762322883806e5', NULL, '2020-12-03 14:21:05', '192.168.214.162,172.18.0.5');
INSERT INTO `sys_role_permission` VALUES ('2c928087762742e801762742ea380074', 'ff8080817608b79401762695e474081a', 'ff8080817608b794017623268a7b0703', NULL, '2020-12-03 14:21:05', '192.168.214.162,172.18.0.5');
INSERT INTO `sys_role_permission` VALUES ('2c928087762742e801762742ea380075', 'ff8080817608b79401762695e474081a', 'ff8080817608b7940176233d3687078c', NULL, '2020-12-03 14:21:05', '192.168.214.162,172.18.0.5');
INSERT INTO `sys_role_permission` VALUES ('2c928087762742e801762742ea380076', 'ff8080817608b79401762695e474081a', 'ff8080817608b7940176235b839807bc', NULL, '2020-12-03 14:21:05', '192.168.214.162,172.18.0.5');
INSERT INTO `sys_role_permission` VALUES ('2c928087762742e801762742ea380077', 'ff8080817608b79401762695e474081a', 'ff8080817608b794017622b168db04aa', NULL, '2020-12-03 14:21:05', '192.168.214.162,172.18.0.5');
INSERT INTO `sys_role_permission` VALUES ('2c928087762742e801762742ea380078', 'ff8080817608b79401762695e474081a', 'ff8080817608b79401762302f76f0620', NULL, '2020-12-03 14:21:05', '192.168.214.162,172.18.0.5');
INSERT INTO `sys_role_permission` VALUES ('2c928087762742e801762742ea380079', 'ff8080817608b79401762695e474081a', 'ff8080817608b79401762309ae24063c', NULL, '2020-12-03 14:21:05', '192.168.214.162,172.18.0.5');
INSERT INTO `sys_role_permission` VALUES ('2c928087762742e801762742ea38007a', 'ff8080817608b79401762695e474081a', 'ff8080817608b7940176230b42a90651', NULL, '2020-12-03 14:21:05', '192.168.214.162,172.18.0.5');
INSERT INTO `sys_role_permission` VALUES ('2c928087762742e801762742ea38007b', 'ff8080817608b79401762695e474081a', '1310400808977698817', NULL, '2020-12-03 14:21:05', '192.168.214.162,172.18.0.5');
INSERT INTO `sys_role_permission` VALUES ('2c928087762742e801762742ea38007c', 'ff8080817608b79401762695e474081a', '1310403230580084737', NULL, '2020-12-03 14:21:05', '192.168.214.162,172.18.0.5');
INSERT INTO `sys_role_permission` VALUES ('2c928087762742e801762742ea38007d', 'ff8080817608b79401762695e474081a', '1310404620442406913', NULL, '2020-12-03 14:21:05', '192.168.214.162,172.18.0.5');
INSERT INTO `sys_role_permission` VALUES ('2c928087762742e801762742ea38007e', 'ff8080817608b79401762695e474081a', '1310409822184673282', NULL, '2020-12-03 14:21:05', '192.168.214.162,172.18.0.5');
INSERT INTO `sys_role_permission` VALUES ('2c928087762742e801762742ea38007f', 'ff8080817608b79401762695e474081a', 'ff8080817608b7940176233197440750', NULL, '2020-12-03 14:21:05', '192.168.214.162,172.18.0.5');
INSERT INTO `sys_role_permission` VALUES ('2c928087762742e801762742ea380080', 'ff8080817608b79401762695e474081a', 'ff8080817608b794017623351d64076e', NULL, '2020-12-03 14:21:05', '192.168.214.162,172.18.0.5');
INSERT INTO `sys_role_permission` VALUES ('2c928087762742e801762742ea380081', 'ff8080817608b79401762695e474081a', '1310416488150077442', NULL, '2020-12-03 14:21:05', '192.168.214.162,172.18.0.5');
INSERT INTO `sys_role_permission` VALUES ('2c928087762742e801762742ea390082', 'ff8080817608b79401762695e474081a', '1304321289430765570', NULL, '2020-12-03 14:21:05', '192.168.214.162,172.18.0.5');
INSERT INTO `sys_role_permission` VALUES ('2c928087762742e801762742ea390083', 'ff8080817608b79401762695e474081a', '1309379979724722178', NULL, '2020-12-03 14:21:05', '192.168.214.162,172.18.0.5');
INSERT INTO `sys_role_permission` VALUES ('2c928087762742e801762742ea390084', 'ff8080817608b79401762695e474081a', '1310432897492062210', NULL, '2020-12-03 14:21:05', '192.168.214.162,172.18.0.5');
INSERT INTO `sys_role_permission` VALUES ('2c928087762742e801762742ea390085', 'ff8080817608b79401762695e474081a', '1301802633433976834', NULL, '2020-12-03 14:21:05', '192.168.214.162,172.18.0.5');
INSERT INTO `sys_role_permission` VALUES ('2c928087762742e801762742ea390086', 'ff8080817608b79401762695e474081a', '1331872709646225410', NULL, '2020-12-03 14:21:05', '192.168.214.162,172.18.0.5');
INSERT INTO `sys_role_permission` VALUES ('2c928087762742e801762742ea390087', 'ff8080817608b79401762695e474081a', 'ff8080817608b794017622ab44760464', NULL, '2020-12-03 14:21:05', '192.168.214.162,172.18.0.5');
INSERT INTO `sys_role_permission` VALUES ('2c928087762742e801762742ea390088', 'ff8080817608b79401762695e474081a', 'ff8080817608b794017622ae6de30487', NULL, '2020-12-03 14:21:05', '192.168.214.162,172.18.0.5');
INSERT INTO `sys_role_permission` VALUES ('2c928087762742e801762742ea390089', 'ff8080817608b79401762695e474081a', 'ff8080817608b794017622b7146504f0', NULL, '2020-12-03 14:21:05', '192.168.214.162,172.18.0.5');
INSERT INTO `sys_role_permission` VALUES ('2c928087762742e801762742ea39008a', 'ff8080817608b79401762695e474081a', 'ff8080817608b794017622b92495050c', NULL, '2020-12-03 14:21:05', '192.168.214.162,172.18.0.5');
INSERT INTO `sys_role_permission` VALUES ('2c928087762742e801762742ea39008b', 'ff8080817608b79401762695e474081a', 'ff8080817608b794017622bbc3990521', NULL, '2020-12-03 14:21:05', '192.168.214.162,172.18.0.5');
INSERT INTO `sys_role_permission` VALUES ('2c928087762742e801762742ea39008c', 'ff8080817608b79401762695e474081a', 'ff8080817608b794017622a30dd00417', NULL, '2020-12-03 14:21:05', '192.168.214.162,172.18.0.5');
INSERT INTO `sys_role_permission` VALUES ('2c928087762742e801762742ea39008d', 'ff8080817608b79401762695e474081a', 'ff8080817608b79401762313dc060694', NULL, '2020-12-03 14:21:05', '192.168.214.162,172.18.0.5');
INSERT INTO `sys_role_permission` VALUES ('2c928087762742e801762742ea39008e', 'ff8080817608b79401762695e474081a', 'ff8080817608b79401762326f887070a', NULL, '2020-12-03 14:21:05', '192.168.214.162,172.18.0.5');
INSERT INTO `sys_role_permission` VALUES ('2c928087762742e801762742ea39008f', 'ff8080817608b79401762695e474081a', 'ff8080817608b794017622b767c004f7', NULL, '2020-12-03 14:21:05', '192.168.214.162,172.18.0.5');
INSERT INTO `sys_role_permission` VALUES ('2c928087762742e801762742ea390090', 'ff8080817608b79401762695e474081a', 'ff8080817608b794017622ab9cea046b', NULL, '2020-12-03 14:21:05', '192.168.214.162,172.18.0.5');
INSERT INTO `sys_role_permission` VALUES ('2c928087762742e801762742ea390091', 'ff8080817608b79401762695e474081a', '1310404991655088130', NULL, '2020-12-03 14:21:05', '192.168.214.162,172.18.0.5');
INSERT INTO `sys_role_permission` VALUES ('2c928087762742e801762742ea390092', 'ff8080817608b79401762695e474081a', 'ff8080817608b794017622b1ef1f04b1', NULL, '2020-12-03 14:21:05', '192.168.214.162,172.18.0.5');
INSERT INTO `sys_role_permission` VALUES ('2c928087762742e801762742ea390093', 'ff8080817608b79401762695e474081a', '1310403473635807234', NULL, '2020-12-03 14:21:05', '192.168.214.162,172.18.0.5');
INSERT INTO `sys_role_permission` VALUES ('2c928087762742e801762742ea390094', 'ff8080817608b79401762695e474081a', '1310401085768208385', NULL, '2020-12-03 14:21:05', '192.168.214.162,172.18.0.5');
INSERT INTO `sys_role_permission` VALUES ('2c928087762742e801762742ea390095', 'ff8080817608b79401762695e474081a', 'ff8080817608b79401762332103a0757', NULL, '2020-12-03 14:21:05', '192.168.214.162,172.18.0.5');
INSERT INTO `sys_role_permission` VALUES ('2c928087762742e801762742ea390096', 'ff8080817608b79401762695e474081a', 'ff8080817608b7940176232897ef0711', NULL, '2020-12-03 14:21:05', '192.168.214.162,172.18.0.5');
INSERT INTO `sys_role_permission` VALUES ('2c928087762742e801762742ea390097', 'ff8080817608b79401762695e474081a', '1310406951590432769', NULL, '2020-12-03 14:21:05', '192.168.214.162,172.18.0.5');
INSERT INTO `sys_role_permission` VALUES ('2c928087762742e801762742ea390098', 'ff8080817608b79401762695e474081a', 'ff8080817608b794017622aecbde048e', NULL, '2020-12-03 14:21:05', '192.168.214.162,172.18.0.5');
INSERT INTO `sys_role_permission` VALUES ('2c928087762742e801762742ea390099', 'ff8080817608b79401762695e474081a', '1310416188978761730', NULL, '2020-12-03 14:21:05', '192.168.214.162,172.18.0.5');
INSERT INTO `sys_role_permission` VALUES ('2c928087762742e801762742ea39009a', 'ff8080817608b79401762695e474081a', '1310410017085591553', NULL, '2020-12-03 14:21:05', '192.168.214.162,172.18.0.5');
INSERT INTO `sys_role_permission` VALUES ('2c928087762742e801762742ea39009b', 'ff8080817608b79401762695e474081a', '1310433274404802561', NULL, '2020-12-03 14:21:05', '192.168.214.162,172.18.0.5');
INSERT INTO `sys_role_permission` VALUES ('2c928087762742e801762742ea39009c', 'ff8080817608b79401762695e474081a', '1310399637995130881', NULL, '2020-12-03 14:21:05', '192.168.214.162,172.18.0.5');
INSERT INTO `sys_role_permission` VALUES ('2c928087762742e801762742ea39009d', 'ff8080817608b79401762695e474081a', 'ff8080817608b794017622a3c7b2041e', NULL, '2020-12-03 14:21:05', '192.168.214.162,172.18.0.5');
INSERT INTO `sys_role_permission` VALUES ('2c928087762742e801762742ea39009e', 'ff8080817608b79401762695e474081a', '1310405261181063170', NULL, '2020-12-03 14:21:05', '192.168.214.162,172.18.0.5');
INSERT INTO `sys_role_permission` VALUES ('2c928087762742e801762742ea39009f', 'ff8080817608b79401762695e474081a', '1310403837600731138', NULL, '2020-12-03 14:21:05', '192.168.214.162,172.18.0.5');
INSERT INTO `sys_role_permission` VALUES ('2c928087762742e801762742ea3900a0', 'ff8080817608b79401762695e474081a', 'ff8080817608b7940176232901020718', NULL, '2020-12-03 14:21:05', '192.168.214.162,172.18.0.5');
INSERT INTO `sys_role_permission` VALUES ('2c928087762742e801762742ea3900a1', 'ff8080817608b79401762695e474081a', '1310401229901271042', NULL, '2020-12-03 14:21:05', '192.168.214.162,172.18.0.5');
INSERT INTO `sys_role_permission` VALUES ('2c928087762742e801762742ea3900a2', 'ff8080817608b79401762695e474081a', '1310415795251056642', NULL, '2020-12-03 14:21:05', '192.168.214.162,172.18.0.5');
INSERT INTO `sys_role_permission` VALUES ('2c928087762742e801762742ea3900a3', 'ff8080817608b79401762695e474081a', 'ff8080817608b794017622b2a1e404b8', NULL, '2020-12-03 14:21:05', '192.168.214.162,172.18.0.5');
INSERT INTO `sys_role_permission` VALUES ('2c928087762742e801762742ea3900a4', 'ff8080817608b79401762695e474081a', 'ff8080817608b794017622ac13230472', NULL, '2020-12-03 14:21:05', '192.168.214.162,172.18.0.5');
INSERT INTO `sys_role_permission` VALUES ('2c928087762742e801762742ea3900a5', 'ff8080817608b79401762695e474081a', 'ff8080817608b794017622a41de50425', NULL, '2020-12-03 14:21:05', '192.168.214.162,172.18.0.5');
INSERT INTO `sys_role_permission` VALUES ('2c928087762742e801762742ea3900a6', 'ff8080817608b79401762695e474081a', 'ff8080817608b794017622af287d0495', NULL, '2020-12-03 14:21:05', '192.168.214.162,172.18.0.5');
INSERT INTO `sys_role_permission` VALUES ('2c928087762742e801762742ea3900a7', 'ff8080817608b79401762695e474081a', '1310407190049198081', NULL, '2020-12-03 14:21:05', '192.168.214.162,172.18.0.5');
INSERT INTO `sys_role_permission` VALUES ('2c928087762742e801762742ea3900a8', 'ff8080817608b79401762695e474081a', 'ff8080817608b79401762346c7400795', NULL, '2020-12-03 14:21:05', '192.168.214.162,172.18.0.5');
INSERT INTO `sys_role_permission` VALUES ('2c928087762742e801762742ea3900a9', 'ff8080817608b79401762695e474081a', 'ff8080817608b794017622b33a5004bf', NULL, '2020-12-03 14:21:05', '192.168.214.162,172.18.0.5');
INSERT INTO `sys_role_permission` VALUES ('2c928087762742e801762742ea3900aa', 'ff8080817608b79401762695e474081a', '1310407413609795585', NULL, '2020-12-03 14:21:05', '192.168.214.162,172.18.0.5');
INSERT INTO `sys_role_permission` VALUES ('2c928087762742e801762742ea3900ab', 'ff8080817608b79401762695e474081a', '1310401446956503042', NULL, '2020-12-03 14:21:05', '192.168.214.162,172.18.0.5');
INSERT INTO `sys_role_permission` VALUES ('2c928087762742e801762742ea3900ac', 'ff8080817608b79401762695e474081a', '1310405668003385346', NULL, '2020-12-03 14:21:05', '192.168.214.162,172.18.0.5');
INSERT INTO `sys_role_permission` VALUES ('2c928087762742e801762742ea3900ad', 'ff8080817608b79401762695e474081a', 'ff8080817608b794017622b3a2bc04c6', NULL, '2020-12-03 14:21:05', '192.168.214.162,172.18.0.5');
INSERT INTO `sys_role_permission` VALUES ('2c928087762742e801762742ea3900ae', 'ff8080817608b79401762695e474081a', '1310406050767179777', NULL, '2020-12-03 14:21:05', '192.168.214.162,172.18.0.5');
INSERT INTO `sys_role_permission` VALUES ('2c928087762742e801762742ea3900af', 'ff8080817608b79401762695e474081a', '1310401620621660162', NULL, '2020-12-03 14:21:05', '192.168.214.162,172.18.0.5');
INSERT INTO `sys_role_permission` VALUES ('2c928087762742e801762742ea3900b0', 'ff8080817608b79401762695e474081a', '1301447721621655554', NULL, '2020-12-03 14:21:05', '192.168.214.162,172.18.0.5');
INSERT INTO `sys_role_permission` VALUES ('2c928087762742e801762742ea3900b1', 'ff8080817608b79401762695e474081a', '1310406264022372354', NULL, '2020-12-03 14:21:05', '192.168.214.162,172.18.0.5');
INSERT INTO `sys_role_permission` VALUES ('2c928087762742e801762742ea3900b2', 'ff8080817608b79401762695e474081a', 'ff8080817608b794017622b40d7f04cd', NULL, '2020-12-03 14:21:05', '192.168.214.162,172.18.0.5');
INSERT INTO `sys_role_permission` VALUES ('2c928087762742e801762742ea3900b3', 'ff8080817608b79401762695e474081a', '1310407620586115073', NULL, '2020-12-03 14:21:05', '192.168.214.162,172.18.0.5');
INSERT INTO `sys_role_permission` VALUES ('2c928087762742e801762742ea3a00b4', 'ff8080817608b79401762695e474081a', '1310401771595632641', NULL, '2020-12-03 14:21:05', '192.168.214.162,172.18.0.5');
INSERT INTO `sys_role_permission` VALUES ('2c928087762742e801762742ea3a00b5', 'ff8080817608b79401762695e474081a', '1310401950704996353', NULL, '2020-12-03 14:21:05', '192.168.214.162,172.18.0.5');
INSERT INTO `sys_role_permission` VALUES ('2c928087762742e801762742ea3a00b6', 'ff8080817608b79401762695e474081a', '1310406614360002562', NULL, '2020-12-03 14:21:05', '192.168.214.162,172.18.0.5');
INSERT INTO `sys_role_permission` VALUES ('2c928087762742e801762742ea3a00b7', 'ff8080817608b79401762695e474081a', 'ff8080817608b794017622b4b9e004d4', NULL, '2020-12-03 14:21:05', '192.168.214.162,172.18.0.5');
INSERT INTO `sys_role_permission` VALUES ('2c928087762742e801762742ea3a00b8', 'ff8080817608b79401762695e474081a', 'ff8080817608b794017622b5129d04db', NULL, '2020-12-03 14:21:05', '192.168.214.162,172.18.0.5');
INSERT INTO `sys_role_permission` VALUES ('2c928087762742e801762742ea3a00b9', 'ff8080817608b79401762695e474081a', '1300999295377346562', NULL, '2020-12-03 14:21:05', '192.168.214.162,172.18.0.5');
INSERT INTO `sys_role_permission` VALUES ('2c928087762742e801762742ea3a00ba', 'ff8080817608b79401762695e474081a', '1300999947163799554', NULL, '2020-12-03 14:21:05', '192.168.214.162,172.18.0.5');
INSERT INTO `sys_role_permission` VALUES ('2c928087762742e801762742ea3a00bb', 'ff8080817608b79401762695e474081a', '1301000181361152001', NULL, '2020-12-03 14:21:05', '192.168.214.162,172.18.0.5');
INSERT INTO `sys_role_permission` VALUES ('2c928087762742e801762742ea3a00bc', 'ff8080817608b79401762695e474081a', '1301000639995711490', NULL, '2020-12-03 14:21:05', '192.168.214.162,172.18.0.5');
INSERT INTO `sys_role_permission` VALUES ('2c928087762742e801762742ea3a00bd', 'ff8080817608b79401762695e474081a', '1301000852273631234', NULL, '2020-12-03 14:21:05', '192.168.214.162,172.18.0.5');
INSERT INTO `sys_role_permission` VALUES ('2c928087762b973801762b9738480000', 'ff8080817608b79401762695e474081a', 'ff8080817608b79401762b92e7130b08', NULL, '2020-12-04 10:31:39', '127.0.0.1, 192.168.19.151,172.18.0.5');
INSERT INTO `sys_role_permission` VALUES ('2c928087762b973801762b9738490001', 'ff8080817608b79401762695e474081a', 'ff8080817608b79401762b939efd0b0d', NULL, '2020-12-04 10:31:39', '127.0.0.1, 192.168.19.151,172.18.0.5');
INSERT INTO `sys_role_permission` VALUES ('2c928087762b973801762b9738490002', 'ff8080817608b79401762695e474081a', 'ff8080817608b79401762b94184a0b12', NULL, '2020-12-04 10:31:39', '127.0.0.1, 192.168.19.151,172.18.0.5');
INSERT INTO `sys_role_permission` VALUES ('2c928087762b973801762b9738490003', 'ff8080817608b79401762695e474081a', 'ff8080817608b79401762b949a9f0b17', NULL, '2020-12-04 10:31:39', '127.0.0.1, 192.168.19.151,172.18.0.5');
INSERT INTO `sys_role_permission` VALUES ('2c928087762cd5d201762cd5d2a00000', 'ff8080817608b7940176269516ad0818', '1310402513266020353', NULL, '2020-12-04 16:19:39', '192.168.19.124,172.18.0.5');
INSERT INTO `sys_role_permission` VALUES ('2c928087762cd5d201762cd5d2a00001', 'ff8080817608b7940176269516ad0818', '1301077101432016897', NULL, '2020-12-04 16:19:39', '192.168.19.124,172.18.0.5');
INSERT INTO `sys_role_permission` VALUES ('2c928087762cd5d201762cd5d2a00002', 'ff8080817608b7940176269516ad0818', '1310404258897596417', NULL, '2020-12-04 16:19:39', '192.168.19.124,172.18.0.5');
INSERT INTO `sys_role_permission` VALUES ('2c928087762cd5d201762cd5d2a00003', 'ff8080817608b7940176269516ad0818', '1301427338168242177', NULL, '2020-12-04 16:19:39', '192.168.19.124,172.18.0.5');
INSERT INTO `sys_role_permission` VALUES ('2c928087762cd5d201762cd5d2a00004', 'ff8080817608b7940176269516ad0818', '1310400302846840834', NULL, '2020-12-04 16:19:39', '192.168.19.124,172.18.0.5');
INSERT INTO `sys_role_permission` VALUES ('2c928087762cd5d201762cd5d2a00005', 'ff8080817608b7940176269516ad0818', 'ff8080817608b794017622ba7fec0513', NULL, '2020-12-04 16:19:39', '192.168.19.124,172.18.0.5');
INSERT INTO `sys_role_permission` VALUES ('2c928087762cd5d201762cd5d2a00006', 'ff8080817608b7940176269516ad0818', 'ff8080817608b794017622c3277c0544', NULL, '2020-12-04 16:19:39', '192.168.19.124,172.18.0.5');
INSERT INTO `sys_role_permission` VALUES ('2c928087762cd5d201762cd5d2a10007', 'ff8080817608b7940176269516ad0818', 'ff8080817608b794017622c3b1b0054b', NULL, '2020-12-04 16:19:39', '192.168.19.124,172.18.0.5');
INSERT INTO `sys_role_permission` VALUES ('2c928087762cd5d201762cd5d2a10008', 'ff8080817608b7940176269516ad0818', 'ff8080817608b794017622c5d9e40552', NULL, '2020-12-04 16:19:39', '192.168.19.124,172.18.0.5');
INSERT INTO `sys_role_permission` VALUES ('2c928087762cd5d201762cd5d2a10009', 'ff8080817608b7940176269516ad0818', 'ff8080817608b794017622c7ca4b055b', NULL, '2020-12-04 16:19:39', '192.168.19.124,172.18.0.5');
INSERT INTO `sys_role_permission` VALUES ('2c928087762cd5d201762cd5d2a1000a', 'ff8080817608b7940176269516ad0818', 'ff8080817608b7940176237f207007c3', NULL, '2020-12-04 16:19:39', '192.168.19.124,172.18.0.5');
INSERT INTO `sys_role_permission` VALUES ('2c928087762cd5d201762cd5d2a1000b', 'ff8080817608b7940176269516ad0818', 'ff8080817608b7940176237f842307ca', NULL, '2020-12-04 16:19:39', '192.168.19.124,172.18.0.5');
INSERT INTO `sys_role_permission` VALUES ('2c928087762cd5d201762cd5d2a1000c', 'ff8080817608b7940176269516ad0818', 'ff8080817608b79401762642cebb07d8', NULL, '2020-12-04 16:19:39', '192.168.19.124,172.18.0.5');
INSERT INTO `sys_role_permission` VALUES ('2c928087762cd5d201762cd5d2a1000d', 'ff8080817608b7940176269516ad0818', 'ff8080817608b794017622ff496b060b', NULL, '2020-12-04 16:19:39', '192.168.19.124,172.18.0.5');
INSERT INTO `sys_role_permission` VALUES ('2c928087762cd5d201762cd5d2a1000e', 'ff8080817608b7940176269516ad0818', 'ff8080817608b794017623079ad1062e', NULL, '2020-12-04 16:19:39', '192.168.19.124,172.18.0.5');
INSERT INTO `sys_role_permission` VALUES ('2c928087762cd5d201762cd5d2a1000f', 'ff8080817608b7940176269516ad0818', 'ff8080817608b794017623131dc70686', NULL, '2020-12-04 16:19:39', '192.168.19.124,172.18.0.5');
INSERT INTO `sys_role_permission` VALUES ('2c928087762cd5d201762cd5d2a10010', 'ff8080817608b7940176269516ad0818', 'ff8080817608b794017623146d78069b', NULL, '2020-12-04 16:19:39', '192.168.19.124,172.18.0.5');
INSERT INTO `sys_role_permission` VALUES ('2c928087762cd5d201762cd5d2a10011', 'ff8080817608b7940176269516ad0818', 'ff8080817608b794017623174dc606a9', NULL, '2020-12-04 16:19:39', '192.168.19.124,172.18.0.5');
INSERT INTO `sys_role_permission` VALUES ('2c928087762cd5d201762cd5d2a10012', 'ff8080817608b7940176269516ad0818', 'ff8080817608b794017622a194750409', NULL, '2020-12-04 16:19:39', '192.168.19.124,172.18.0.5');
INSERT INTO `sys_role_permission` VALUES ('2c928087762cd5d201762cd5d2a10013', 'ff8080817608b7940176269516ad0818', 'ff8080817608b794017622a4f475042c', NULL, '2020-12-04 16:19:39', '192.168.19.124,172.18.0.5');
INSERT INTO `sys_role_permission` VALUES ('2c928087762cd5d201762cd5d2a10014', 'ff8080817608b7940176269516ad0818', 'ff8080817608b794017622a667f4043a', NULL, '2020-12-04 16:19:39', '192.168.19.124,172.18.0.5');
INSERT INTO `sys_role_permission` VALUES ('2c928087762cd5d201762cd5d2a10015', 'ff8080817608b7940176269516ad0818', 'ff8080817608b794017622a6f7330441', NULL, '2020-12-04 16:19:39', '192.168.19.124,172.18.0.5');
INSERT INTO `sys_role_permission` VALUES ('2c928087762cd5d201762cd5d2a10016', 'ff8080817608b7940176269516ad0818', 'ff8080817608b794017622a78ee90448', NULL, '2020-12-04 16:19:39', '192.168.19.124,172.18.0.5');
INSERT INTO `sys_role_permission` VALUES ('2c928087762cd5d201762cd5d2a10017', 'ff8080817608b7940176269516ad0818', '1310410227111170050', NULL, '2020-12-04 16:19:39', '192.168.19.124,172.18.0.5');
INSERT INTO `sys_role_permission` VALUES ('2c928087762cd5d201762cd5d2a10018', 'ff8080817608b7940176269516ad0818', '1310410737562161153', NULL, '2020-12-04 16:19:39', '192.168.19.124,172.18.0.5');
INSERT INTO `sys_role_permission` VALUES ('2c928087762cd5d201762cd5d2a10019', 'ff8080817608b7940176269516ad0818', '1310413060296413185', NULL, '2020-12-04 16:19:39', '192.168.19.124,172.18.0.5');
INSERT INTO `sys_role_permission` VALUES ('2c928087762cd5d201762cd5d2a1001a', 'ff8080817608b7940176269516ad0818', 'ff8080817608b794017622cd7a6c056b', NULL, '2020-12-04 16:19:39', '192.168.19.124,172.18.0.5');
INSERT INTO `sys_role_permission` VALUES ('2c928087762cd5d201762cd5d2a1001b', 'ff8080817608b7940176269516ad0818', 'ff8080817608b794017622ce912b0572', NULL, '2020-12-04 16:19:39', '192.168.19.124,172.18.0.5');
INSERT INTO `sys_role_permission` VALUES ('2c928087762cd5d201762cd5d2a1001c', 'ff8080817608b7940176269516ad0818', 'ff8080817608b794017622d220310580', NULL, '2020-12-04 16:19:39', '192.168.19.124,172.18.0.5');
INSERT INTO `sys_role_permission` VALUES ('2c928087762cd5d201762cd5d2a1001d', 'ff8080817608b7940176269516ad0818', 'ff8080817608b794017622d319d80589', NULL, '2020-12-04 16:19:39', '192.168.19.124,172.18.0.5');
INSERT INTO `sys_role_permission` VALUES ('2c928087762cd5d201762cd5d2a1001e', 'ff8080817608b7940176269516ad0818', 'ff8080817608b794017622fb507205ed', NULL, '2020-12-04 16:19:39', '192.168.19.124,172.18.0.5');
INSERT INTO `sys_role_permission` VALUES ('2c928087762cd5d201762cd5d2a1001f', 'ff8080817608b7940176269516ad0818', 'ff8080817608b794017622fc714605fb', NULL, '2020-12-04 16:19:39', '192.168.19.124,172.18.0.5');
INSERT INTO `sys_role_permission` VALUES ('2c928087762cd5d201762cd5d2a10020', 'ff8080817608b7940176269516ad0818', 'ff8080817608b794017622fe2a640604', NULL, '2020-12-04 16:19:39', '192.168.19.124,172.18.0.5');
INSERT INTO `sys_role_permission` VALUES ('2c928087762cd5d201762cd5d2a10021', 'ff8080817608b7940176269516ad0818', 'ff8080817608b7940176231e34f206c9', NULL, '2020-12-04 16:19:39', '192.168.19.124,172.18.0.5');
INSERT INTO `sys_role_permission` VALUES ('2c928087762cd5d201762cd5d2a10022', 'ff8080817608b7940176269516ad0818', 'ff8080817608b79401762321ae9706de', NULL, '2020-12-04 16:19:39', '192.168.19.124,172.18.0.5');
INSERT INTO `sys_role_permission` VALUES ('2c928087762cd5d201762cd5d2a10023', 'ff8080817608b7940176269516ad0818', 'ff8080817608b79401762323212606ec', NULL, '2020-12-04 16:19:39', '192.168.19.124,172.18.0.5');
INSERT INTO `sys_role_permission` VALUES ('2c928087762cd5d201762cd5d2a10024', 'ff8080817608b7940176269516ad0818', 'ff8080817608b7940176230a57090643', NULL, '2020-12-04 16:19:39', '192.168.19.124,172.18.0.5');
INSERT INTO `sys_role_permission` VALUES ('2c928087762cd5d201762cd5d2a10025', 'ff8080817608b7940176269516ad0818', 'ff8080817608b7940176230c9865065a', NULL, '2020-12-04 16:19:39', '192.168.19.124,172.18.0.5');
INSERT INTO `sys_role_permission` VALUES ('2c928087762cd5d201762cd5d2a10026', 'ff8080817608b7940176269516ad0818', 'ff8080817608b7940176230d98b80668', NULL, '2020-12-04 16:19:39', '192.168.19.124,172.18.0.5');
INSERT INTO `sys_role_permission` VALUES ('2c928087762cd5d201762cd5d2a10027', 'ff8080817608b7940176269516ad0818', 'ff8080817608b7940176230f52030676', NULL, '2020-12-04 16:19:39', '192.168.19.124,172.18.0.5');
INSERT INTO `sys_role_permission` VALUES ('2c928087762cd5d201762cd5d2a10028', 'ff8080817608b7940176269516ad0818', '1301034658502283266', NULL, '2020-12-04 16:19:39', '192.168.19.124,172.18.0.5');
INSERT INTO `sys_role_permission` VALUES ('2c928087762cd5d201762cd5d2a10029', 'ff8080817608b7940176269516ad0818', '1310404050755260417', NULL, '2020-12-04 16:19:39', '192.168.19.124,172.18.0.5');
INSERT INTO `sys_role_permission` VALUES ('2c928087762cd5d201762cd5d2a1002a', 'ff8080817608b7940176269516ad0818', '1310409534656745473', NULL, '2020-12-04 16:19:39', '192.168.19.124,172.18.0.5');
INSERT INTO `sys_role_permission` VALUES ('2c928087762cd5d201762cd5d2a1002b', 'ff8080817608b7940176269516ad0818', '1310403041836404738', NULL, '2020-12-04 16:19:39', '192.168.19.124,172.18.0.5');
INSERT INTO `sys_role_permission` VALUES ('2c928087762cd5d201762cd5d2a1002c', 'ff8080817608b7940176269516ad0818', '1301428144502214658', NULL, '2020-12-04 16:19:39', '192.168.19.124,172.18.0.5');
INSERT INTO `sys_role_permission` VALUES ('2c928087762cd5d201762cd5d2a1002d', 'ff8080817608b7940176269516ad0818', '1310400531214110721', NULL, '2020-12-04 16:19:39', '192.168.19.124,172.18.0.5');
INSERT INTO `sys_role_permission` VALUES ('2c928087762cd5d201762cd5d2a1002e', 'ff8080817608b7940176269516ad0818', 'ff8080817608b794017622bb2c40051a', NULL, '2020-12-04 16:19:39', '192.168.19.124,172.18.0.5');
INSERT INTO `sys_role_permission` VALUES ('2c928087762cd5d201762cd5d2a1002f', 'ff8080817608b7940176269516ad0818', 'ff8080817608b794017623000f7a0612', NULL, '2020-12-04 16:19:39', '192.168.19.124,172.18.0.5');
INSERT INTO `sys_role_permission` VALUES ('2c928087762cd5d201762cd5d2a10030', 'ff8080817608b7940176269516ad0818', 'ff8080817608b79401762309244b0635', NULL, '2020-12-04 16:19:39', '192.168.19.124,172.18.0.5');
INSERT INTO `sys_role_permission` VALUES ('2c928087762cd5d201762cd5d2a10031', 'ff8080817608b7940176269516ad0818', 'ff8080817608b794017623127a25067f', NULL, '2020-12-04 16:19:39', '192.168.19.124,172.18.0.5');
INSERT INTO `sys_role_permission` VALUES ('2c928087762cd5d201762cd5d2a10032', 'ff8080817608b7940176269516ad0818', 'ff8080817608b794017623135c20068d', NULL, '2020-12-04 16:19:39', '192.168.19.124,172.18.0.5');
INSERT INTO `sys_role_permission` VALUES ('2c928087762cd5d201762cd5d2a10033', 'ff8080817608b7940176269516ad0818', 'ff8080817608b79401762316aaf906a2', NULL, '2020-12-04 16:19:39', '192.168.19.124,172.18.0.5');
INSERT INTO `sys_role_permission` VALUES ('2c928087762cd5d201762cd5d2a10034', 'ff8080817608b7940176269516ad0818', 'ff8080817608b79401762317d07206b0', NULL, '2020-12-04 16:19:39', '192.168.19.124,172.18.0.5');
INSERT INTO `sys_role_permission` VALUES ('2c928087762cd5d201762cd5d2a10035', 'ff8080817608b7940176269516ad0818', 'ff8080817608b794017622a2b4b50410', NULL, '2020-12-04 16:19:39', '192.168.19.124,172.18.0.5');
INSERT INTO `sys_role_permission` VALUES ('2c928087762cd5d201762cd5d2a10036', 'ff8080817608b7940176269516ad0818', 'ff8080817608b794017622a5b3190433', NULL, '2020-12-04 16:19:39', '192.168.19.124,172.18.0.5');
INSERT INTO `sys_role_permission` VALUES ('2c928087762cd5d201762cd5d2a10037', 'ff8080817608b7940176269516ad0818', 'ff8080817608b794017622a81cb7044f', NULL, '2020-12-04 16:19:39', '192.168.19.124,172.18.0.5');
INSERT INTO `sys_role_permission` VALUES ('2c928087762cd5d201762cd5d2a10038', 'ff8080817608b7940176269516ad0818', '1310410534536876033', NULL, '2020-12-04 16:19:39', '192.168.19.124,172.18.0.5');
INSERT INTO `sys_role_permission` VALUES ('2c928087762cd5d201762cd5d2a10039', 'ff8080817608b7940176269516ad0818', '1310412463300153345', NULL, '2020-12-04 16:19:39', '192.168.19.124,172.18.0.5');
INSERT INTO `sys_role_permission` VALUES ('2c928087762cd5d201762cd5d2a1003a', 'ff8080817608b7940176269516ad0818', '1310413263179091969', NULL, '2020-12-04 16:19:39', '192.168.19.124,172.18.0.5');
INSERT INTO `sys_role_permission` VALUES ('2c928087762cd5d201762cd5d2a2003b', 'ff8080817608b7940176269516ad0818', 'ff8080817608b794017622c83f120562', NULL, '2020-12-04 16:19:39', '192.168.19.124,172.18.0.5');
INSERT INTO `sys_role_permission` VALUES ('2c928087762cd5d201762cd5d2a2003c', 'ff8080817608b7940176269516ad0818', 'ff8080817608b794017622cf13600579', NULL, '2020-12-04 16:19:39', '192.168.19.124,172.18.0.5');
INSERT INTO `sys_role_permission` VALUES ('2c928087762cd5d201762cd5d2a2003d', 'ff8080817608b7940176269516ad0818', 'ff8080817608b794017622d35cf30590', NULL, '2020-12-04 16:19:39', '192.168.19.124,172.18.0.5');
INSERT INTO `sys_role_permission` VALUES ('2c928087762cd5d201762cd5d2a2003e', 'ff8080817608b7940176269516ad0818', 'ff8080817608b794017622fbead705f4', NULL, '2020-12-04 16:19:39', '192.168.19.124,172.18.0.5');
INSERT INTO `sys_role_permission` VALUES ('2c928087762cd5d201762cd5d2a2003f', 'ff8080817608b7940176269516ad0818', 'ff8080817608b79401762322883806e5', NULL, '2020-12-04 16:19:39', '192.168.19.124,172.18.0.5');
INSERT INTO `sys_role_permission` VALUES ('2c928087762cd5d201762cd5d2a20040', 'ff8080817608b7940176269516ad0818', 'ff8080817608b7940176230ac780064a', NULL, '2020-12-04 16:19:39', '192.168.19.124,172.18.0.5');
INSERT INTO `sys_role_permission` VALUES ('2c928087762cd5d201762cd5d2a20041', 'ff8080817608b7940176269516ad0818', 'ff8080817608b7940176230cf6ba0661', NULL, '2020-12-04 16:19:39', '192.168.19.124,172.18.0.5');
INSERT INTO `sys_role_permission` VALUES ('2c928087762cd5d201762cd5d2a20042', 'ff8080817608b7940176269516ad0818', 'ff8080817608b7940176230e5b8e066f', NULL, '2020-12-04 16:19:39', '192.168.19.124,172.18.0.5');
INSERT INTO `sys_role_permission` VALUES ('2c928087762cd5d201762cd5d2a20043', 'ff8080817608b7940176269516ad0818', '1301001912425910273', NULL, '2020-12-04 16:19:39', '192.168.19.124,172.18.0.5');
INSERT INTO `sys_role_permission` VALUES ('2c928087762cd5d201762cd5d2a20044', 'ff8080817608b7940176269516ad0818', '1301088665497243650', NULL, '2020-12-04 16:19:39', '192.168.19.124,172.18.0.5');
INSERT INTO `sys_role_permission` VALUES ('2c928087762cd5d201762cd5d2a20045', 'ff8080817608b7940176269516ad0818', '1310403230580084737', NULL, '2020-12-04 16:19:39', '192.168.19.124,172.18.0.5');
INSERT INTO `sys_role_permission` VALUES ('2c928087762cd5d201762cd5d2a20046', 'ff8080817608b7940176269516ad0818', '1310404620442406913', NULL, '2020-12-04 16:19:39', '192.168.19.124,172.18.0.5');
INSERT INTO `sys_role_permission` VALUES ('2c928087762cd5d201762cd5d2a20047', 'ff8080817608b7940176269516ad0818', 'ff8080817608b794017622a30dd00417', NULL, '2020-12-04 16:19:39', '192.168.19.124,172.18.0.5');
INSERT INTO `sys_role_permission` VALUES ('2c928087762cd5d201762cd5d2a20048', 'ff8080817608b7940176269516ad0818', 'ff8080817608b79401762313dc060694', NULL, '2020-12-04 16:19:39', '192.168.19.124,172.18.0.5');
INSERT INTO `sys_role_permission` VALUES ('2c928087762cd5d201762cd5d2a20049', 'ff8080817608b7940176269516ad0818', 'ff8080817608b7940176230b42a90651', NULL, '2020-12-04 16:19:39', '192.168.19.124,172.18.0.5');
INSERT INTO `sys_role_permission` VALUES ('2c928087762cd5d201762cd5d2a2004a', 'ff8080817608b7940176269516ad0818', 'ff8080817608b79401762309ae24063c', NULL, '2020-12-04 16:19:39', '192.168.19.124,172.18.0.5');
INSERT INTO `sys_role_permission` VALUES ('2c928087762cd5d201762cd5d2a2004b', 'ff8080817608b7940176269516ad0818', '1301802633433976834', NULL, '2020-12-04 16:19:39', '192.168.19.124,172.18.0.5');
INSERT INTO `sys_role_permission` VALUES ('2c928087762cd5d201762cd5d2a2004c', 'ff8080817608b7940176269516ad0818', 'ff8080817608b794017622bbc3990521', NULL, '2020-12-04 16:19:39', '192.168.19.124,172.18.0.5');
INSERT INTO `sys_role_permission` VALUES ('2c928087762cd5d201762cd5d2a2004d', 'ff8080817608b7940176269516ad0818', '1310400808977698817', NULL, '2020-12-04 16:19:39', '192.168.19.124,172.18.0.5');
INSERT INTO `sys_role_permission` VALUES ('2c928087762cd5d201762cd5d2a2004e', 'ff8080817608b7940176269516ad0818', '1309379979724722178', NULL, '2020-12-04 16:19:39', '192.168.19.124,172.18.0.5');
INSERT INTO `sys_role_permission` VALUES ('2c928087762cd5d201762cd5d2a2004f', 'ff8080817608b7940176269516ad0818', '1310409822184673282', NULL, '2020-12-04 16:19:39', '192.168.19.124,172.18.0.5');
INSERT INTO `sys_role_permission` VALUES ('2c928087762cd5d201762cd5d2a20050', 'ff8080817608b7940176269516ad0818', '1304321289430765570', NULL, '2020-12-04 16:19:39', '192.168.19.124,172.18.0.5');
INSERT INTO `sys_role_permission` VALUES ('2c928087762cd5d201762cd5d2a20051', 'ff8080817608b7940176269516ad0818', 'ff8080817608b79401762302f76f0620', NULL, '2020-12-04 16:19:39', '192.168.19.124,172.18.0.5');
INSERT INTO `sys_role_permission` VALUES ('2c928087762cd5d201762cd5d2a20052', 'ff8080817608b7940176269516ad0818', '1310406951590432769', NULL, '2020-12-04 16:19:39', '192.168.19.124,172.18.0.5');
INSERT INTO `sys_role_permission` VALUES ('2c928087762cd5d201762cd5d2a20053', 'ff8080817608b7940176269516ad0818', '1310416188978761730', NULL, '2020-12-04 16:19:39', '192.168.19.124,172.18.0.5');
INSERT INTO `sys_role_permission` VALUES ('2c928087762cd5d201762cd5d2a20054', 'ff8080817608b7940176269516ad0818', '1310410017085591553', NULL, '2020-12-04 16:19:39', '192.168.19.124,172.18.0.5');
INSERT INTO `sys_role_permission` VALUES ('2c928087762cd5d201762cd5d2a20055', 'ff8080817608b7940176269516ad0818', 'ff8080817608b794017622a3c7b2041e', NULL, '2020-12-04 16:19:39', '192.168.19.124,172.18.0.5');
INSERT INTO `sys_role_permission` VALUES ('2c928087762cd5d201762cd5d2a20056', 'ff8080817608b7940176269516ad0818', '1310401085768208385', NULL, '2020-12-04 16:19:39', '192.168.19.124,172.18.0.5');
INSERT INTO `sys_role_permission` VALUES ('2c928087762cd5d201762cd5d2a20057', 'ff8080817608b7940176269516ad0818', '1310399637995130881', NULL, '2020-12-04 16:19:39', '192.168.19.124,172.18.0.5');
INSERT INTO `sys_role_permission` VALUES ('2c928087762cd5d201762cd5d2a20058', 'ff8080817608b7940176269516ad0818', '1310404991655088130', NULL, '2020-12-04 16:19:39', '192.168.19.124,172.18.0.5');
INSERT INTO `sys_role_permission` VALUES ('2c928087762cd5d201762cd5d2a20059', 'ff8080817608b7940176269516ad0818', '1310403473635807234', NULL, '2020-12-04 16:19:39', '192.168.19.124,172.18.0.5');
INSERT INTO `sys_role_permission` VALUES ('2c928087762cd5d201762cd5d2a2005a', 'ff8080817608b7940176269516ad0818', '1310403837600731138', NULL, '2020-12-04 16:19:39', '192.168.19.124,172.18.0.5');
INSERT INTO `sys_role_permission` VALUES ('2c928087762cd5d201762cd5d2a2005b', 'ff8080817608b7940176269516ad0818', '1310401229901271042', NULL, '2020-12-04 16:19:39', '192.168.19.124,172.18.0.5');
INSERT INTO `sys_role_permission` VALUES ('2c928087762cd5d201762cd5d2a2005c', 'ff8080817608b7940176269516ad0818', '1310415795251056642', NULL, '2020-12-04 16:19:39', '192.168.19.124,172.18.0.5');
INSERT INTO `sys_role_permission` VALUES ('2c928087762cd5d201762cd5d2a2005d', 'ff8080817608b7940176269516ad0818', '1310405261181063170', NULL, '2020-12-04 16:19:39', '192.168.19.124,172.18.0.5');
INSERT INTO `sys_role_permission` VALUES ('2c928087762cd5d201762cd5d2a2005e', 'ff8080817608b7940176269516ad0818', 'ff8080817608b794017622a41de50425', NULL, '2020-12-04 16:19:39', '192.168.19.124,172.18.0.5');
INSERT INTO `sys_role_permission` VALUES ('2c928087762cd5d201762cd5d2a2005f', 'ff8080817608b7940176269516ad0818', '1310407190049198081', NULL, '2020-12-04 16:19:39', '192.168.19.124,172.18.0.5');
INSERT INTO `sys_role_permission` VALUES ('2c928087762cd5d201762cd5d2a20060', 'ff8080817608b7940176269516ad0818', '1310401446956503042', NULL, '2020-12-04 16:19:39', '192.168.19.124,172.18.0.5');
INSERT INTO `sys_role_permission` VALUES ('2c928087762cd5d201762cd5d2a20061', 'ff8080817608b7940176269516ad0818', '1310407413609795585', NULL, '2020-12-04 16:19:39', '192.168.19.124,172.18.0.5');
INSERT INTO `sys_role_permission` VALUES ('2c928087762cd5d201762cd5d2a20062', 'ff8080817608b7940176269516ad0818', '1310405668003385346', NULL, '2020-12-04 16:19:39', '192.168.19.124,172.18.0.5');
INSERT INTO `sys_role_permission` VALUES ('2c928087762cd5d201762cd5d2a20063', 'ff8080817608b7940176269516ad0818', '1310406050767179777', NULL, '2020-12-04 16:19:39', '192.168.19.124,172.18.0.5');
INSERT INTO `sys_role_permission` VALUES ('2c928087762cd5d201762cd5d2a20064', 'ff8080817608b7940176269516ad0818', '1301447721621655554', NULL, '2020-12-04 16:19:39', '192.168.19.124,172.18.0.5');
INSERT INTO `sys_role_permission` VALUES ('2c928087762cd5d201762cd5d2a20065', 'ff8080817608b7940176269516ad0818', '1310401620621660162', NULL, '2020-12-04 16:19:39', '192.168.19.124,172.18.0.5');
INSERT INTO `sys_role_permission` VALUES ('2c928087762cd5d201762cd5d2a20066', 'ff8080817608b7940176269516ad0818', '1310401771595632641', NULL, '2020-12-04 16:19:39', '192.168.19.124,172.18.0.5');
INSERT INTO `sys_role_permission` VALUES ('2c928087762cd5d201762cd5d2a20067', 'ff8080817608b7940176269516ad0818', '1310407620586115073', NULL, '2020-12-04 16:19:39', '192.168.19.124,172.18.0.5');
INSERT INTO `sys_role_permission` VALUES ('2c928087762cd5d201762cd5d2a20068', 'ff8080817608b7940176269516ad0818', '1310406264022372354', NULL, '2020-12-04 16:19:39', '192.168.19.124,172.18.0.5');
INSERT INTO `sys_role_permission` VALUES ('2c928087762cd5d201762cd5d2a20069', 'ff8080817608b7940176269516ad0818', '1310406614360002562', NULL, '2020-12-04 16:19:39', '192.168.19.124,172.18.0.5');
INSERT INTO `sys_role_permission` VALUES ('2c928087762cd5d201762cd5d2a2006a', 'ff8080817608b7940176269516ad0818', '1310401950704996353', NULL, '2020-12-04 16:19:39', '192.168.19.124,172.18.0.5');
INSERT INTO `sys_role_permission` VALUES ('2c928087762cd5d201762cd5d2a2006b', 'ff8080817608b7940176269516ad0818', '1300999947163799554', NULL, '2020-12-04 16:19:39', '192.168.19.124,172.18.0.5');
INSERT INTO `sys_role_permission` VALUES ('2c928087762cd5d201762cd5d2a2006c', 'ff8080817608b7940176269516ad0818', '1301000181361152001', NULL, '2020-12-04 16:19:39', '192.168.19.124,172.18.0.5');
INSERT INTO `sys_role_permission` VALUES ('2c93808275da98460175da9846ac0002', '40288cd65d556b7d015d565e660d0075', '8a6e4e905b274da4015b2759f457000a', NULL, '2020-11-18 17:03:34', '192.168.214.162,172.17.0.4');
INSERT INTO `sys_role_permission` VALUES ('2c93808275da98460175da9846ac0003', '40288cd65d556b7d015d565e660d0075', '8a6e4e905b8430c3015b84bd360f000c', NULL, '2020-11-18 17:03:34', '192.168.214.162,172.17.0.4');
INSERT INTO `sys_role_permission` VALUES ('2c93808275da98460175da9846ac0004', '40288cd65d556b7d015d565e660d0075', '8a6e4e905b8430c3015b84e6acea001e', NULL, '2020-11-18 17:03:34', '192.168.214.162,172.17.0.4');
INSERT INTO `sys_role_permission` VALUES ('2c93808275da98460175da9846ac0005', '40288cd65d556b7d015d565e660d0075', '8a6e4e905b274da4015b2757a94d0004', NULL, '2020-11-18 17:03:34', '192.168.214.162,172.17.0.4');
INSERT INTO `sys_role_permission` VALUES ('2c93808275da98460175da9846ac0006', '40288cd65d556b7d015d565e660d0075', '8a6e4e905b5ac753015b5acd5f440003', NULL, '2020-11-18 17:03:34', '192.168.214.162,172.17.0.4');
INSERT INTO `sys_role_permission` VALUES ('2c93808275da98460175da9847b30009', '8a6e50b54dc268dc014dc2784aff0115', '8a6e502f4d7b764d014d7bc35a920007', NULL, '2020-11-18 17:03:34', '192.168.214.162,172.17.0.4');
INSERT INTO `sys_role_permission` VALUES ('2c93808275da98460175da9847b3000c', '8a6e50b54dc268dc014dc2784aff0115', '402800e63e8c7c69013e8c8167a50007', NULL, '2020-11-18 17:03:34', '192.168.214.162,172.17.0.4');
INSERT INTO `sys_role_permission` VALUES ('2c93808275da98460175da9847b3000e', '8a6e50b54dc268dc014dc2784aff0115', '8a6e4e2c439ec3ee01439ec6022d0000', NULL, '2020-11-18 17:03:34', '192.168.214.162,172.17.0.4');
INSERT INTO `sys_role_permission` VALUES ('2c93808275da98460175da9847b30010', '8a6e50b54dc268dc014dc2784aff0115', '8a6e4e3843aea7bf0143aeaad0a30000', NULL, '2020-11-18 17:03:34', '192.168.214.162,172.17.0.4');
INSERT INTO `sys_role_permission` VALUES ('2c93808275da98460175da9847b30012', '8a6e50b54dc268dc014dc2784aff0115', '8a6e502f4d7b764d014d7bc517500010', NULL, '2020-11-18 17:03:34', '192.168.214.162,172.17.0.4');
INSERT INTO `sys_role_permission` VALUES ('2c93808275da98460175da9847b3001b', '8a6e50b54dc268dc014dc2784aff0115', '8a6e4e2c45b1844f0145b189c1230004', NULL, '2020-11-18 17:03:34', '192.168.214.162,172.17.0.4');
INSERT INTO `sys_role_permission` VALUES ('2c93808275da98460175da9847b30023', '8a6e50b54dc268dc014dc2784aff0115', '8a8281855b5aa892015b5ff783023bb7', NULL, '2020-11-18 17:03:34', '192.168.214.162,172.17.0.4');
INSERT INTO `sys_role_permission` VALUES ('2c93808275da98460175da9847b30026', '8a6e50b54dc268dc014dc2784aff0115', '8a8281855d07a64c015d8405a3660e15', NULL, '2020-11-18 17:03:34', '192.168.214.162,172.17.0.4');
INSERT INTO `sys_role_permission` VALUES ('2c93808275da98460175da9847b30028', '8a6e50b54dc268dc014dc2784aff0115', '8a6e4e3843ae8f6a0143aea6e1340000', NULL, '2020-11-18 17:03:34', '192.168.214.162,172.17.0.4');
INSERT INTO `sys_role_permission` VALUES ('2c93808275da98460175da9847b3002a', '8a6e50b54dc268dc014dc2784aff0115', '8a6e4e905b5ac753015b5acd5f440003', NULL, '2020-11-18 17:03:34', '192.168.214.162,172.17.0.4');
INSERT INTO `sys_role_permission` VALUES ('2c93808275da98460175da9847b3002b', '8a6e50b54dc268dc014dc2784aff0115', '402800573e8d4463013e8deda5761111', NULL, '2020-11-18 17:03:34', '192.168.214.162,172.17.0.4');
INSERT INTO `sys_role_permission` VALUES ('2c93808275da98460175da9847b3002c', '8a6e50b54dc268dc014dc2784aff0115', '402800573e8d4463013e8deda5762222', NULL, '2020-11-18 17:03:34', '192.168.214.162,172.17.0.4');
INSERT INTO `sys_role_permission` VALUES ('2c93808275da98460175da9847b3002d', '8a6e50b54dc268dc014dc2784aff0115', '1300999295377346562', NULL, '2020-11-18 17:03:34', '192.168.214.162,172.17.0.4');
INSERT INTO `sys_role_permission` VALUES ('2c93808275da98460175da9847b3002e', '8a6e50b54dc268dc014dc2784aff0115', '1300999947163799554', NULL, '2020-11-18 17:03:34', '192.168.214.162,172.17.0.4');
INSERT INTO `sys_role_permission` VALUES ('2c93808275da98460175da9847b3002f', '8a6e50b54dc268dc014dc2784aff0115', '1301000181361152001', NULL, '2020-11-18 17:03:34', '192.168.214.162,172.17.0.4');
INSERT INTO `sys_role_permission` VALUES ('2c93808275da98460175da9847b30030', '8a6e50b54dc268dc014dc2784aff0115', '1301000639995711490', NULL, '2020-11-18 17:03:34', '192.168.214.162,172.17.0.4');
INSERT INTO `sys_role_permission` VALUES ('2c93808275da98460175da9847b30031', '8a6e50b54dc268dc014dc2784aff0115', '1301000852273631234', NULL, '2020-11-18 17:03:34', '192.168.214.162,172.17.0.4');
INSERT INTO `sys_role_permission` VALUES ('2c93808275da98460175da9847b30032', '8a6e50b54dc268dc014dc2784aff0115', '402881c53c1fdb18013c20072fa9006f', NULL, '2020-11-18 17:03:34', '192.168.214.162,172.17.0.4');
INSERT INTO `sys_role_permission` VALUES ('2c93808275da98460175da9847b30033', '8a6e50b54dc268dc014dc2784aff0115', '402881c53c1fdb18013c2000c3530033', NULL, '2020-11-18 17:03:34', '192.168.214.162,172.17.0.4');
INSERT INTO `sys_role_permission` VALUES ('2c93808275da98460175da9847b30034', '8a6e50b54dc268dc014dc2784aff0115', '402881c53c1fdb18013c1ffce9a00012', NULL, '2020-11-18 17:03:34', '192.168.214.162,172.17.0.4');
INSERT INTO `sys_role_permission` VALUES ('2c93808275da98460175da9847b30035', '8a6e50b54dc268dc014dc2784aff0115', '402881c53c1fdb18013c1ffa2b120000', NULL, '2020-11-18 17:03:34', '192.168.214.162,172.17.0.4');
INSERT INTO `sys_role_permission` VALUES ('2c93808275da98460175da9847b30036', '8a6e50b54dc268dc014dc2784aff0115', '402881c53c1fdb18013c1ffff2fb002d', NULL, '2020-11-18 17:03:34', '192.168.214.162,172.17.0.4');
INSERT INTO `sys_role_permission` VALUES ('2c93808275da98460175da9847b30037', '8a6e50b54dc268dc014dc2784aff0115', '402881c53c1fdb18013c2001746c0039', NULL, '2020-11-18 17:03:34', '192.168.214.162,172.17.0.4');
INSERT INTO `sys_role_permission` VALUES ('2c93808275da98460175da9847b30038', '8a6e50b54dc268dc014dc2784aff0115', '8a6e4e905ba99a4d015ba99bbfb9005d', NULL, '2020-11-18 17:03:34', '192.168.214.162,172.17.0.4');
INSERT INTO `sys_role_permission` VALUES ('2c93808275da98460175da9847b30039', '8a6e50b54dc268dc014dc2784aff0115', '8a6e4e905ba99a4d015ba99bc0b20089', NULL, '2020-11-18 17:03:34', '192.168.214.162,172.17.0.4');
INSERT INTO `sys_role_permission` VALUES ('2c93808275da98460175da9847b3003a', '8a6e50b54dc268dc014dc2784aff0115', '8a6e4e905ba99a4d015ba99bc21f00e1', NULL, '2020-11-18 17:03:34', '192.168.214.162,172.17.0.4');
INSERT INTO `sys_role_permission` VALUES ('2c93808275da98460175da9847b3003b', '8a6e50b54dc268dc014dc2784aff0115', '8a6e4e905ba99a4d015ba99bc28500fa', NULL, '2020-11-18 17:03:34', '192.168.214.162,172.17.0.4');
INSERT INTO `sys_role_permission` VALUES ('2c93808275da98460175da9847b3003c', '8a6e50b54dc268dc014dc2784aff0115', '8a6e4e905ba99a4d015ba99bc2aa00fc', NULL, '2020-11-18 17:03:34', '192.168.214.162,172.17.0.4');
INSERT INTO `sys_role_permission` VALUES ('2c93808275da98460175da9847b3003d', '8a6e50b54dc268dc014dc2784aff0115', '402800573ec072cd013ec0d079200004', NULL, '2020-11-18 17:03:34', '192.168.214.162,172.17.0.4');
INSERT INTO `sys_role_permission` VALUES ('2c93808275da98460175da9847b3003e', '8a6e50b54dc268dc014dc2784aff0115', '402800573ec072cd013ec0d079200003', NULL, '2020-11-18 17:03:34', '192.168.214.162,172.17.0.4');
INSERT INTO `sys_role_permission` VALUES ('2c93808275da98460175da9847b3003f', '8a6e50b54dc268dc014dc2784aff0115', '402800573ec072cd013ec0d079200002', NULL, '2020-11-18 17:03:34', '192.168.214.162,172.17.0.4');
INSERT INTO `sys_role_permission` VALUES ('2c93808275da98460175da9847b30040', '8a6e50b54dc268dc014dc2784aff0115', '402800573ec072cd013ec0d079200001', NULL, '2020-11-18 17:03:34', '192.168.214.162,172.17.0.4');
INSERT INTO `sys_role_permission` VALUES ('2c93808275da98460175da9847b30041', '8a6e50b54dc268dc014dc2784aff0115', '402800573ec072cd013ec0d079100004', NULL, '2020-11-18 17:03:34', '192.168.214.162,172.17.0.4');
INSERT INTO `sys_role_permission` VALUES ('2c93808275da98460175da9847b30042', '8a6e50b54dc268dc014dc2784aff0115', '402800573ec072cd013ec0d079100003', NULL, '2020-11-18 17:03:34', '192.168.214.162,172.17.0.4');
INSERT INTO `sys_role_permission` VALUES ('2c93808275da98460175da9847b30043', '8a6e50b54dc268dc014dc2784aff0115', '402800573ec072cd013ec0d079100002', NULL, '2020-11-18 17:03:34', '192.168.214.162,172.17.0.4');
INSERT INTO `sys_role_permission` VALUES ('2c93808275da98460175da9847b30044', '8a6e50b54dc268dc014dc2784aff0115', '402800573ec072cd013ec0d079100001', NULL, '2020-11-18 17:03:34', '192.168.214.162,172.17.0.4');
INSERT INTO `sys_role_permission` VALUES ('2c93808275da98460175da9847b30045', '8a6e50b54dc268dc014dc2784aff0115', '402800573ec072cd013ec0d079200008', NULL, '2020-11-18 17:03:34', '192.168.214.162,172.17.0.4');
INSERT INTO `sys_role_permission` VALUES ('2c93808275da98460175da9847b30046', '8a6e50b54dc268dc014dc2784aff0115', '402800573ec072cd013ec0d079100008', NULL, '2020-11-18 17:03:34', '192.168.214.162,172.17.0.4');
INSERT INTO `sys_role_permission` VALUES ('2c93808275da98460175da9847b30047', '8a6e50b54dc268dc014dc2784aff0115', 'DDBED3F37E7034EAE040A8C058006EF4', NULL, '2020-11-18 17:03:34', '192.168.214.162,172.17.0.4');
INSERT INTO `sys_role_permission` VALUES ('2c93808275da98460175da9847b30048', '8a6e50b54dc268dc014dc2784aff0115', '402881c53c1fdb18013c200cb9e1wwsw', NULL, '2020-11-18 17:03:34', '192.168.214.162,172.17.0.4');
INSERT INTO `sys_role_permission` VALUES ('2c93808275da98460175da9847b30049', '8a6e50b54dc268dc014dc2784aff0115', '402881c53c1fdb18013c200cb9e1sjdd', NULL, '2020-11-18 17:03:34', '192.168.214.162,172.17.0.4');
INSERT INTO `sys_role_permission` VALUES ('2c93808275da98460175da9847b3004a', '8a6e50b54dc268dc014dc2784aff0115', 'D2EEF7144DF3A123E040A8C0580045A7', NULL, '2020-11-18 17:03:34', '192.168.214.162,172.17.0.4');
INSERT INTO `sys_role_permission` VALUES ('2c93808275da98460175da9847b3004b', '8a6e50b54dc268dc014dc2784aff0115', '402881c53c1fdb18013c200b2f9a0096', NULL, '2020-11-18 17:03:34', '192.168.214.162,172.17.0.4');
INSERT INTO `sys_role_permission` VALUES ('2c93808275da98460175da9847b3004c', '8a6e50b54dc268dc014dc2784aff0115', '402881c53c1fdb18013c1fff3a080027', NULL, '2020-11-18 17:03:34', '192.168.214.162,172.17.0.4');
INSERT INTO `sys_role_permission` VALUES ('2c93808275da98460175da9847b3004d', '8a6e50b54dc268dc014dc2784aff0115', '402881c53c1fdb18013c20031573004b', NULL, '2020-11-18 17:03:34', '192.168.214.162,172.17.0.4');
INSERT INTO `sys_role_permission` VALUES ('2c93808275da98460175da9847b3004e', '8a6e50b54dc268dc014dc2784aff0115', '402881c53c1fdb18013c20043b650057', NULL, '2020-11-18 17:03:34', '192.168.214.162,172.17.0.4');
INSERT INTO `sys_role_permission` VALUES ('2c93808275da98460175da9847b3004f', '8a6e50b54dc268dc014dc2784aff0115', '402881c53c1fdb18013c2004ebd8005d', NULL, '2020-11-18 17:03:34', '192.168.214.162,172.17.0.4');
INSERT INTO `sys_role_permission` VALUES ('2c93808275f3156b0175f3156d380003', '8a6e50b54dc268dc014dc277fba30114', '8a6e4e38439f94c50143a00a8fe201d4', NULL, '2020-11-23 11:11:09', '192.168.214.162,172.17.0.4');
INSERT INTO `sys_role_permission` VALUES ('2c93808275f3156b0175f3156d380004', '8a6e50b54dc268dc014dc277fba30114', '8a6e4e2c458389d6014583c4b964019e', NULL, '2020-11-23 11:11:09', '192.168.214.162,172.17.0.4');
INSERT INTO `sys_role_permission` VALUES ('2c93808275f3156b0175f3156d380005', '8a6e50b54dc268dc014dc277fba30114', '402800573fa90aff013fa90d2d7b0000', NULL, '2020-11-23 11:11:09', '192.168.214.162,172.17.0.4');
INSERT INTO `sys_role_permission` VALUES ('2c93808275f3156b0175f3156d380006', '8a6e50b54dc268dc014dc277fba30114', '8a8281855b5aa892015b6483796d52a0', NULL, '2020-11-23 11:11:09', '192.168.214.162,172.17.0.4');
INSERT INTO `sys_role_permission` VALUES ('2c93808275f3156b0175f3156d380007', '8a6e50b54dc268dc014dc277fba30114', '8a6e4fb354271fda015427ea0d860037', NULL, '2020-11-23 11:11:09', '192.168.214.162,172.17.0.4');
INSERT INTO `sys_role_permission` VALUES ('2c93808275f3156b0175f3156d380008', '8a6e50b54dc268dc014dc277fba30114', '402800573feb1d51013fefa7a818008d', NULL, '2020-11-23 11:11:09', '192.168.214.162,172.17.0.4');
INSERT INTO `sys_role_permission` VALUES ('2c93808275f3156b0175f3156d380009', '8a6e50b54dc268dc014dc277fba30114', '4028005540c80ff20140c81ac1930005', NULL, '2020-11-23 11:11:09', '192.168.214.162,172.17.0.4');
INSERT INTO `sys_role_permission` VALUES ('2c93808275f3156b0175f3156d38000a', '8a6e50b54dc268dc014dc277fba30114', '8a6e4e38439f94c50143a00b067901d9', NULL, '2020-11-23 11:11:09', '192.168.214.162,172.17.0.4');
INSERT INTO `sys_role_permission` VALUES ('2c93808275f3156b0175f3156d38000b', '8a6e50b54dc268dc014dc277fba30114', '8a6e4fb354271fda015427ea7b520041', NULL, '2020-11-23 11:11:09', '192.168.214.162,172.17.0.4');
INSERT INTO `sys_role_permission` VALUES ('2c93808275f3156b0175f3156d38000c', '8a6e50b54dc268dc014dc277fba30114', '8a6e4e2c458389d6014583c537a901a4', NULL, '2020-11-23 11:11:09', '192.168.214.162,172.17.0.4');
INSERT INTO `sys_role_permission` VALUES ('2c93808275f3156b0175f3156d38000d', '8a6e50b54dc268dc014dc277fba30114', '8a6e4fb354271fda015427eacdba004b', NULL, '2020-11-23 11:11:09', '192.168.214.162,172.17.0.4');
INSERT INTO `sys_role_permission` VALUES ('2c93808275f3156b0175f3156d38000e', '8a6e50b54dc268dc014dc277fba30114', '297e91503fef4af5013fefadba980006', NULL, '2020-11-23 11:11:09', '192.168.214.162,172.17.0.4');
INSERT INTO `sys_role_permission` VALUES ('2c93808275f3156b0175f3156d38000f', '8a6e50b54dc268dc014dc277fba30114', '8a6e4e38439f94c50143a00b7ba501de', NULL, '2020-11-23 11:11:09', '192.168.214.162,172.17.0.4');
INSERT INTO `sys_role_permission` VALUES ('2c93808275f3156b0175f3156d380010', '8a6e50b54dc268dc014dc277fba30114', '8a6e4e2c454acb2801454acd7d9c0000', NULL, '2020-11-23 11:11:09', '192.168.214.162,172.17.0.4');
INSERT INTO `sys_role_permission` VALUES ('2c93808275f3156b0175f3156d380011', '8a6e50b54dc268dc014dc277fba30114', '297e91503fef4af5013fefdd30cf01f9', NULL, '2020-11-23 11:11:09', '192.168.214.162,172.17.0.4');
INSERT INTO `sys_role_permission` VALUES ('2c93808275f3156b0175f3156d380012', '8a6e50b54dc268dc014dc277fba30114', '8a6e4fb354271fda015427eb4aa30055', NULL, '2020-11-23 11:11:09', '192.168.214.162,172.17.0.4');
INSERT INTO `sys_role_permission` VALUES ('2c93808275f3156b0175f3156d380013', '8a6e50b54dc268dc014dc277fba30114', '297e91503fef4af5013fefdd97fe01fe', NULL, '2020-11-23 11:11:09', '192.168.214.162,172.17.0.4');
INSERT INTO `sys_role_permission` VALUES ('2c93808275f3156b0175f3156d380014', '8a6e50b54dc268dc014dc277fba30114', '8a6e4fb354271fda015427eb9c49005f', NULL, '2020-11-23 11:11:09', '192.168.214.162,172.17.0.4');
INSERT INTO `sys_role_permission` VALUES ('2c93808275f3156b0175f3156d380015', '8a6e50b54dc268dc014dc277fba30114', '297e91503fef4af5013feff239af0203', NULL, '2020-11-23 11:11:09', '192.168.214.162,172.17.0.4');
INSERT INTO `sys_role_permission` VALUES ('2c93808275f3156b0175f3156d380016', '8a6e50b54dc268dc014dc277fba30114', '8a6e4fb354271fda015427ec09ac0069', NULL, '2020-11-23 11:11:09', '192.168.214.162,172.17.0.4');
INSERT INTO `sys_role_permission` VALUES ('2c93808275f3156b0175f3156d390017', '8a6e50b54dc268dc014dc277fba30114', '297e91503fef4af5013feff28a1e0208', NULL, '2020-11-23 11:11:09', '192.168.214.162,172.17.0.4');
INSERT INTO `sys_role_permission` VALUES ('2c93808275f3156b0175f3156d390018', '8a6e50b54dc268dc014dc277fba30114', '297e91503fef4af5013feff43bc8020d', NULL, '2020-11-23 11:11:09', '192.168.214.162,172.17.0.4');
INSERT INTO `sys_role_permission` VALUES ('2c93808275f3156b0175f3156d390019', '8a6e50b54dc268dc014dc277fba30114', '297e91503fef4af5013feff481230212', NULL, '2020-11-23 11:11:09', '192.168.214.162,172.17.0.4');
INSERT INTO `sys_role_permission` VALUES ('2c93808275f3156b0175f3156d39001a', '8a6e50b54dc268dc014dc277fba30114', '4028005540c7ffe30140c80eda000000', NULL, '2020-11-23 11:11:09', '192.168.214.162,172.17.0.4');
INSERT INTO `sys_role_permission` VALUES ('2c93808275f3156b0175f3156d39001b', '8a6e50b54dc268dc014dc277fba30114', '8a6e4e905b5ac753015b5acd5f440003', NULL, '2020-11-23 11:11:09', '192.168.214.162,172.17.0.4');
INSERT INTO `sys_role_permission` VALUES ('2c93808275f3156b0175f3156d39001c', '8a6e50b54dc268dc014dc277fba30114', '15274DDE0DC8F36EE050EE0A10D06F5F', NULL, '2020-11-23 11:11:09', '192.168.214.162,172.17.0.4');
INSERT INTO `sys_role_permission` VALUES ('2c93808275f3156b0175f3156d39001d', '8a6e50b54dc268dc014dc277fba30114', '8a6e4f184ebdc7d3014ebdfb85bd0000', NULL, '2020-11-23 11:11:09', '192.168.214.162,172.17.0.4');
INSERT INTO `sys_role_permission` VALUES ('2c93808275f3156b0175f3156d39001e', '8a6e50b54dc268dc014dc277fba30114', '8a6e4f184ebe1280014ebe1728420001', NULL, '2020-11-23 11:11:09', '192.168.214.162,172.17.0.4');
INSERT INTO `sys_role_permission` VALUES ('2c93808275f3156b0175f3156d39001f', '8a6e50b54dc268dc014dc277fba30114', '402881c53c1fdb18013c204bb807035d', NULL, '2020-11-23 11:11:09', '192.168.214.162,172.17.0.4');
INSERT INTO `sys_role_permission` VALUES ('2c93808275f3156b0175f3156d390020', '8a6e50b54dc268dc014dc277fba30114', '402881c53c1fdb18013c204c81440363', NULL, '2020-11-23 11:11:09', '192.168.214.162,172.17.0.4');
INSERT INTO `sys_role_permission` VALUES ('2c93808275f3156b0175f3156d390021', '8a6e50b54dc268dc014dc277fba30114', '402881c53c1fdb18013c2010bf6600c3', NULL, '2020-11-23 11:11:09', '192.168.214.162,172.17.0.4');
INSERT INTO `sys_role_permission` VALUES ('2c93808275f3156b0175f3156d390022', '8a6e50b54dc268dc014dc277fba30114', '402881c53c1fdb18013c201197bd00cc', NULL, '2020-11-23 11:11:09', '192.168.214.162,172.17.0.4');
INSERT INTO `sys_role_permission` VALUES ('2c93808275f3156b0175f3156d390023', '8a6e50b54dc268dc014dc277fba30114', '402881c53c1fdb18013c204db5a10375', NULL, '2020-11-23 11:11:09', '192.168.214.162,172.17.0.4');
INSERT INTO `sys_role_permission` VALUES ('2c93808275f3156b0175f3156d390024', '8a6e50b54dc268dc014dc277fba30114', '402881c53c1fdb18013c204e17d4037e', NULL, '2020-11-23 11:11:09', '192.168.214.162,172.17.0.4');
INSERT INTO `sys_role_permission` VALUES ('2c93808275f3156b0175f3156d390025', '8a6e50b54dc268dc014dc277fba30114', '402881c53c1fdb18013c204e7dd90384', NULL, '2020-11-23 11:11:09', '192.168.214.162,172.17.0.4');
INSERT INTO `sys_role_permission` VALUES ('2c93808275f3156b0175f3156d390026', '8a6e50b54dc268dc014dc277fba30114', '402881c53c1fdb18013c204ed407038a', NULL, '2020-11-23 11:11:09', '192.168.214.162,172.17.0.4');
INSERT INTO `sys_role_permission` VALUES ('2c93808275f3156b0175f3156d390027', '8a6e50b54dc268dc014dc277fba30114', '8a6e4e905ba99a4d015ba99bc22200e3', NULL, '2020-11-23 11:11:09', '192.168.214.162,172.17.0.4');
INSERT INTO `sys_role_permission` VALUES ('2c93808275f3156b0175f3156d390028', '8a6e50b54dc268dc014dc277fba30114', '8a6e4e905ba99a4d015ba99bc22a00ea', NULL, '2020-11-23 11:11:09', '192.168.214.162,172.17.0.4');
INSERT INTO `sys_role_permission` VALUES ('2c93808275f3156b0175f3156d390029', '8a6e50b54dc268dc014dc277fba30114', '8a6e4e905ba99a4d015ba99bc2b90106', NULL, '2020-11-23 11:11:09', '192.168.214.162,172.17.0.4');
INSERT INTO `sys_role_permission` VALUES ('2c93808275f3156b0175f3156d39002a', '8a6e50b54dc268dc014dc277fba30114', '8a6e4e905ba99a4d015ba99bc2bc0108', NULL, '2020-11-23 11:11:09', '192.168.214.162,172.17.0.4');
INSERT INTO `sys_role_permission` VALUES ('2c93808275f3156b0175f3156d39002b', '8a6e50b54dc268dc014dc277fba30114', '402881c53c1fdb18013c201d7f2a0150', NULL, '2020-11-23 11:11:09', '192.168.214.162,172.17.0.4');
INSERT INTO `sys_role_permission` VALUES ('2c93808275f3156b0175f3156d39002c', '8a6e50b54dc268dc014dc277fba30114', '402881c53c1fdb18013c201dfa110156', NULL, '2020-11-23 11:11:09', '192.168.214.162,172.17.0.4');
INSERT INTO `sys_role_permission` VALUES ('2c93808275f3156b0175f3156d39002d', '8a6e50b54dc268dc014dc277fba30114', '402881c53c1fdb18013c201e8095015c', NULL, '2020-11-23 11:11:09', '192.168.214.162,172.17.0.4');
INSERT INTO `sys_role_permission` VALUES ('2c93808275f3156b0175f3156d39002e', '8a6e50b54dc268dc014dc277fba30114', '402881c53c1fdb18013c201f03270162', NULL, '2020-11-23 11:11:09', '192.168.214.162,172.17.0.4');
INSERT INTO `sys_role_permission` VALUES ('2c93808275f3156b0175f3156d39002f', '8a6e50b54dc268dc014dc277fba30114', '402881c53c1fdb18013c201524cf00f6', NULL, '2020-11-23 11:11:09', '192.168.214.162,172.17.0.4');
INSERT INTO `sys_role_permission` VALUES ('2c93808275f3156b0175f3156d390030', '8a6e50b54dc268dc014dc277fba30114', '402881c53c1fdb18013c2015985b00fc', NULL, '2020-11-23 11:11:09', '192.168.214.162,172.17.0.4');
INSERT INTO `sys_role_permission` VALUES ('2c93808275f3156b0175f3156d390031', '8a6e50b54dc268dc014dc277fba30114', '402881c53c1fdb18013c2016101e0102', NULL, '2020-11-23 11:11:09', '192.168.214.162,172.17.0.4');
INSERT INTO `sys_role_permission` VALUES ('2c93808275f3156b0175f3156d390032', '8a6e50b54dc268dc014dc277fba30114', '402881c53c1fdb18013c2017969d010e', NULL, '2020-11-23 11:11:09', '192.168.214.162,172.17.0.4');
INSERT INTO `sys_role_permission` VALUES ('2c93808275f3156b0175f3156d390033', '8a6e50b54dc268dc014dc277fba30114', '402881c53c1fdb18013c20180d750114', NULL, '2020-11-23 11:11:09', '192.168.214.162,172.17.0.4');
INSERT INTO `sys_role_permission` VALUES ('2c93808275f3156b0175f3156d390034', '8a6e50b54dc268dc014dc277fba30114', '402881c53c1fdb18013c20188d24011a', NULL, '2020-11-23 11:11:09', '192.168.214.162,172.17.0.4');
INSERT INTO `sys_role_permission` VALUES ('2c93808275f3156b0175f3156d390035', '8a6e50b54dc268dc014dc277fba30114', '402881c53c1fdb18013c204854620330', NULL, '2020-11-23 11:11:09', '192.168.214.162,172.17.0.4');
INSERT INTO `sys_role_permission` VALUES ('2c93808275f3156b0175f3156d390036', '8a6e50b54dc268dc014dc277fba30114', '402881c53c1fdb18013c2048c1080336', NULL, '2020-11-23 11:11:09', '192.168.214.162,172.17.0.4');
INSERT INTO `sys_role_permission` VALUES ('2c93808275f3156b0175f3156d390037', '8a6e50b54dc268dc014dc277fba30114', '402881c53c1fdb18013c20492110033c', NULL, '2020-11-23 11:11:09', '192.168.214.162,172.17.0.4');
INSERT INTO `sys_role_permission` VALUES ('2c93808275f3156b0175f3156d390038', '8a6e50b54dc268dc014dc277fba30114', '402881c53c1fdb18013c2049ddca0342', NULL, '2020-11-23 11:11:09', '192.168.214.162,172.17.0.4');
INSERT INTO `sys_role_permission` VALUES ('2c93808275f3156b0175f3156d390039', '8a6e50b54dc268dc014dc277fba30114', '402881c53c1fdb18013c204a4f7b034b', NULL, '2020-11-23 11:11:09', '192.168.214.162,172.17.0.4');
INSERT INTO `sys_role_permission` VALUES ('2c93808275f3156b0175f3156d39003a', '8a6e50b54dc268dc014dc277fba30114', '402881c53c1fdb18013c2051b99203a8', NULL, '2020-11-23 11:11:09', '192.168.214.162,172.17.0.4');
INSERT INTO `sys_role_permission` VALUES ('2c93808275f3156b0175f3156d39003b', '8a6e50b54dc268dc014dc277fba30114', '402881c53c1fdb18013c20522baa03b1', NULL, '2020-11-23 11:11:09', '192.168.214.162,172.17.0.4');
INSERT INTO `sys_role_permission` VALUES ('2c93808275f3156b0175f3156d39003c', '8a6e50b54dc268dc014dc277fba30114', '402881c53c1fdb18013c20529cde03b7', NULL, '2020-11-23 11:11:09', '192.168.214.162,172.17.0.4');
INSERT INTO `sys_role_permission` VALUES ('2c93808275f3156b0175f3156d39003d', '8a6e50b54dc268dc014dc277fba30114', '402881c53c1fdb18013c2052f2fd03bd', NULL, '2020-11-23 11:11:09', '192.168.214.162,172.17.0.4');
INSERT INTO `sys_role_permission` VALUES ('2c93808275f3156b0175f3156d39003e', '8a6e50b54dc268dc014dc277fba30114', '402881c53c1fdb18013c205375bd03c3', NULL, '2020-11-23 11:11:09', '192.168.214.162,172.17.0.4');
INSERT INTO `sys_role_permission` VALUES ('2c93808275f3156b0175f3156d39003f', '8a6e50b54dc268dc014dc277fba30114', '402881c53c1fdb18013c201217f500d2', NULL, '2020-11-23 11:11:09', '192.168.214.162,172.17.0.4');
INSERT INTO `sys_role_permission` VALUES ('2c93808275f3156b0175f3156d390040', '8a6e50b54dc268dc014dc277fba30114', '402881c53c1fdb18013c2012942300d8', NULL, '2020-11-23 11:11:09', '192.168.214.162,172.17.0.4');
INSERT INTO `sys_role_permission` VALUES ('2c93808275f3156b0175f3156d390041', '8a6e50b54dc268dc014dc277fba30114', '402881c53c1fdb18013c20131a5000de', NULL, '2020-11-23 11:11:09', '192.168.214.162,172.17.0.4');
INSERT INTO `sys_role_permission` VALUES ('2c93808275f3156b0175f3156d390042', '8a6e50b54dc268dc014dc277fba30114', '402881c53c1fdb18013c2013986d00e4', NULL, '2020-11-23 11:11:09', '192.168.214.162,172.17.0.4');
INSERT INTO `sys_role_permission` VALUES ('2c93808275f3156b0175f3156d390043', '8a6e50b54dc268dc014dc277fba30114', '402881c53c1fdb18013c20140f6200ea', NULL, '2020-11-23 11:11:09', '192.168.214.162,172.17.0.4');
INSERT INTO `sys_role_permission` VALUES ('2c93808275f3156b0175f3156d390044', '8a6e50b54dc268dc014dc277fba30114', '402881c53c1fdb18013c201498e300f0', NULL, '2020-11-23 11:11:09', '192.168.214.162,172.17.0.4');
INSERT INTO `sys_role_permission` VALUES ('2c93808275f3156b0175f3156d390045', '8a6e50b54dc268dc014dc277fba30114', '402881c53c1fdb18013c201928090120', NULL, '2020-11-23 11:11:09', '192.168.214.162,172.17.0.4');
INSERT INTO `sys_role_permission` VALUES ('2c93808275f3156b0175f3156d390046', '8a6e50b54dc268dc014dc277fba30114', '402881c53c1fdb18013c2019928e0126', NULL, '2020-11-23 11:11:09', '192.168.214.162,172.17.0.4');
INSERT INTO `sys_role_permission` VALUES ('2c93808275f3156b0175f3156d390047', '8a6e50b54dc268dc014dc277fba30114', '402881c53c1fdb18013c201a0b86012c', NULL, '2020-11-23 11:11:09', '192.168.214.162,172.17.0.4');
INSERT INTO `sys_role_permission` VALUES ('2c93808275f3156b0175f3156d390048', '8a6e50b54dc268dc014dc277fba30114', '402881c53c1fdb18013c201aba9c0132', NULL, '2020-11-23 11:11:09', '192.168.214.162,172.17.0.4');
INSERT INTO `sys_role_permission` VALUES ('2c93808275f3156b0175f3156d390049', '8a6e50b54dc268dc014dc277fba30114', '402881c53c1fdb18013c201b29ae0138', NULL, '2020-11-23 11:11:09', '192.168.214.162,172.17.0.4');
INSERT INTO `sys_role_permission` VALUES ('2c93808275f3156b0175f3156d39004a', '8a6e50b54dc268dc014dc277fba30114', '402881c53c1fdb18013c201c15c6013e', NULL, '2020-11-23 11:11:09', '192.168.214.162,172.17.0.4');
INSERT INTO `sys_role_permission` VALUES ('2c93808275f3156b0175f3156d39004b', '8a6e50b54dc268dc014dc277fba30114', '402881c53c1fdb18013c201cd9050147', NULL, '2020-11-23 11:11:09', '192.168.214.162,172.17.0.4');
INSERT INTO `sys_role_permission` VALUES ('2c93808275f3156b0175f3156d39004c', '8a6e50b54dc268dc014dc277fba30114', '402881c53c1fdb18013c1ffb1d760009', NULL, '2020-11-23 11:11:09', '192.168.214.162,172.17.0.4');
INSERT INTO `sys_role_permission` VALUES ('2c93808275f3156b0175f3156d39004d', '8a6e50b54dc268dc014dc277fba30114', '8a6e4e905ba99a4d015ba99bbf4c0040', NULL, '2020-11-23 11:11:09', '192.168.214.162,172.17.0.4');
INSERT INTO `sys_role_permission` VALUES ('2c93808275f3156b0175f3156d39004e', '8a6e50b54dc268dc014dc277fba30114', '8a6e4e905ba99a4d015ba99bbf58004a', NULL, '2020-11-23 11:11:09', '192.168.214.162,172.17.0.4');
INSERT INTO `sys_role_permission` VALUES ('2c93808275f3156b0175f3156d39004f', '8a6e50b54dc268dc014dc277fba30114', '8a6e4e905ba99a4d015ba99bbf8f0053', NULL, '2020-11-23 11:11:09', '192.168.214.162,172.17.0.4');
INSERT INTO `sys_role_permission` VALUES ('2c93808275f3156b0175f3156d390050', '8a6e50b54dc268dc014dc277fba30114', '8a6e4e905ba99a4d015ba99bbfe80061', NULL, '2020-11-23 11:11:09', '192.168.214.162,172.17.0.4');
INSERT INTO `sys_role_permission` VALUES ('2c93808275f3156b0175f3156d390051', '8a6e50b54dc268dc014dc277fba30114', '8a6e4e905ba99a4d015ba99bbff4006b', NULL, '2020-11-23 11:11:09', '192.168.214.162,172.17.0.4');
INSERT INTO `sys_role_permission` VALUES ('2c93808275f3156b0175f3156d390052', '8a6e50b54dc268dc014dc277fba30114', '8a6e4e905ba99a4d015ba99bc12d00ac', NULL, '2020-11-23 11:11:09', '192.168.214.162,172.17.0.4');
INSERT INTO `sys_role_permission` VALUES ('2c93808275f3156b0175f3156d390053', '8a6e50b54dc268dc014dc277fba30114', '8a6e4e905ba99a4d015ba99bc16a00b8', NULL, '2020-11-23 11:11:09', '192.168.214.162,172.17.0.4');
INSERT INTO `sys_role_permission` VALUES ('2c93808275f3156b0175f3156d390054', '8a6e50b54dc268dc014dc277fba30114', '8a6e4e905ba99a4d015ba99bc1a000c1', NULL, '2020-11-23 11:11:09', '192.168.214.162,172.17.0.4');
INSERT INTO `sys_role_permission` VALUES ('2c93808275f3156b0175f3156d390055', '8a6e50b54dc268dc014dc277fba30114', '402881c53c1fdb18013c20468fb10318', NULL, '2020-11-23 11:11:09', '192.168.214.162,172.17.0.4');
INSERT INTO `sys_role_permission` VALUES ('2c93808275f3156b0175f3156d390056', '8a6e50b54dc268dc014dc277fba30114', '402881c53c1fdb18013c204746ee0321', NULL, '2020-11-23 11:11:09', '192.168.214.162,172.17.0.4');
INSERT INTO `sys_role_permission` VALUES ('2c93808275f3156b0175f3156d390057', '8a6e50b54dc268dc014dc277fba30114', '402881c53c1fdb18013c2047db22032a', NULL, '2020-11-23 11:11:09', '192.168.214.162,172.17.0.4');
INSERT INTO `sys_role_permission` VALUES ('2c93808275f3156b0175f3156d390058', '8a6e50b54dc268dc014dc277fba30114', '402881c53c1fdb18013c20547a6a03c9', NULL, '2020-11-23 11:11:09', '192.168.214.162,172.17.0.4');
INSERT INTO `sys_role_permission` VALUES ('2c93808275f3156b0175f3156d390059', '8a6e50b54dc268dc014dc277fba30114', 'D2EEF7144DF3A123E040A8C0580045A7', NULL, '2020-11-23 11:11:09', '192.168.214.162,172.17.0.4');
INSERT INTO `sys_role_permission` VALUES ('2c93808275f3156b0175f3156d39005a', '8a6e50b54dc268dc014dc277fba30114', 'DDBED3F37E7034EAE040A8C058006EF4', NULL, '2020-11-23 11:11:09', '192.168.214.162,172.17.0.4');
INSERT INTO `sys_role_permission` VALUES ('2c93808275f3156b0175f3156d39005b', '8a6e50b54dc268dc014dc277fba30114', 'ff80808145ef93de0145efacee040002', NULL, '2020-11-23 11:11:09', '192.168.214.162,172.17.0.4');
INSERT INTO `sys_role_permission` VALUES ('2c93808275f3156b0175f3156d39005c', '8a6e50b54dc268dc014dc277fba30114', '402881c53c1fdb18013c204f2e420390', NULL, '2020-11-23 11:11:09', '192.168.214.162,172.17.0.4');
INSERT INTO `sys_role_permission` VALUES ('2c93808275f3156b0175f3156d39005d', '8a6e50b54dc268dc014dc277fba30114', '402881c53c1fdb18013c204f9e940396', NULL, '2020-11-23 11:11:09', '192.168.214.162,172.17.0.4');
INSERT INTO `sys_role_permission` VALUES ('2c93808275f3156b0175f3156d39005e', '8a6e50b54dc268dc014dc277fba30114', '402881c53c1fdb18013c2050142e039c', NULL, '2020-11-23 11:11:09', '192.168.214.162,172.17.0.4');
INSERT INTO `sys_role_permission` VALUES ('2c93808275f3156b0175f3156d39005f', '8a6e50b54dc268dc014dc277fba30114', '402881c53c1fdb18013c2050a07e03a2', NULL, '2020-11-23 11:11:09', '192.168.214.162,172.17.0.4');
INSERT INTO `sys_role_permission` VALUES ('2c93808275f3156b0175f3156d390060', '8a6e50b54dc268dc014dc277fba30114', '402881c53c1fdb18013c204acf4c0351', NULL, '2020-11-23 11:11:09', '192.168.214.162,172.17.0.4');
INSERT INTO `sys_role_permission` VALUES ('2c93808275f3156b0175f3156d390061', '8a6e50b54dc268dc014dc277fba30114', '402881c53c1fdb18013c204b3a170357', NULL, '2020-11-23 11:11:09', '192.168.214.162,172.17.0.4');
INSERT INTO `sys_role_permission` VALUES ('2c93808275f3156b0175f3156dc40062', 'ff80808175d52fbb0175d5f2f7120028', '1310410227111170050', NULL, '2020-11-23 11:11:09', '192.168.214.162,172.17.0.4');
INSERT INTO `sys_role_permission` VALUES ('2c93808275f3156b0175f3156dc40063', 'ff80808175d52fbb0175d5f2f7120028', '1310404258897596417', NULL, '2020-11-23 11:11:09', '192.168.214.162,172.17.0.4');
INSERT INTO `sys_role_permission` VALUES ('2c93808275f3156b0175f3156dc40064', 'ff80808175d52fbb0175d5f2f7120028', '1310410737562161153', NULL, '2020-11-23 11:11:09', '192.168.214.162,172.17.0.4');
INSERT INTO `sys_role_permission` VALUES ('2c93808275f3156b0175f3156dc40065', 'ff80808175d52fbb0175d5f2f7120028', '1310413060296413185', NULL, '2020-11-23 11:11:09', '192.168.214.162,172.17.0.4');
INSERT INTO `sys_role_permission` VALUES ('2c93808275f3156b0175f3156dc40066', 'ff80808175d52fbb0175d5f2f7120028', '1310402513266020353', NULL, '2020-11-23 11:11:09', '192.168.214.162,172.17.0.4');
INSERT INTO `sys_role_permission` VALUES ('2c93808275f3156b0175f3156dc40067', 'ff80808175d52fbb0175d5f2f7120028', '1301077101432016897', NULL, '2020-11-23 11:11:09', '192.168.214.162,172.17.0.4');
INSERT INTO `sys_role_permission` VALUES ('2c93808275f3156b0175f3156dc40068', 'ff80808175d52fbb0175d5f2f7120028', '1310404050755260417', NULL, '2020-11-23 11:11:09', '192.168.214.162,172.17.0.4');
INSERT INTO `sys_role_permission` VALUES ('2c93808275f3156b0175f3156dc40069', 'ff80808175d52fbb0175d5f2f7120028', '1301001284265971713', NULL, '2020-11-23 11:11:09', '192.168.214.162,172.17.0.4');
INSERT INTO `sys_role_permission` VALUES ('2c93808275f3156b0175f3156dc4006a', 'ff80808175d52fbb0175d5f2f7120028', '1310400302846840834', NULL, '2020-11-23 11:11:09', '192.168.214.162,172.17.0.4');
INSERT INTO `sys_role_permission` VALUES ('2c93808275f3156b0175f3156dc4006b', 'ff80808175d52fbb0175d5f2f7120028', '1301427916441128962', NULL, '2020-11-23 11:11:09', '192.168.214.162,172.17.0.4');
INSERT INTO `sys_role_permission` VALUES ('2c93808275f3156b0175f3156dc4006c', 'ff80808175d52fbb0175d5f2f7120028', '1301427338168242177', NULL, '2020-11-23 11:11:09', '192.168.214.162,172.17.0.4');
INSERT INTO `sys_role_permission` VALUES ('2c93808275f3156b0175f3156dc4006d', 'ff80808175d52fbb0175d5f2f7120028', '1310418138898108418', NULL, '2020-11-23 11:11:09', '192.168.214.162,172.17.0.4');
INSERT INTO `sys_role_permission` VALUES ('2c93808275f3156b0175f3156dc4006e', 'ff80808175d52fbb0175d5f2f7120028', '1301034658502283266', NULL, '2020-11-23 11:11:09', '192.168.214.162,172.17.0.4');
INSERT INTO `sys_role_permission` VALUES ('2c93808275f3156b0175f3156dc4006f', 'ff80808175d52fbb0175d5f2f7120028', '1301034916112240641', NULL, '2020-11-23 11:11:09', '192.168.214.162,172.17.0.4');
INSERT INTO `sys_role_permission` VALUES ('2c93808275f3156b0175f3156dc40070', 'ff80808175d52fbb0175d5f2f7120028', '1301001622897299458', NULL, '2020-11-23 11:11:09', '192.168.214.162,172.17.0.4');
INSERT INTO `sys_role_permission` VALUES ('2c93808275f3156b0175f3156dc40071', 'ff80808175d52fbb0175d5f2f7120028', '1310409534656745473', NULL, '2020-11-23 11:11:09', '192.168.214.162,172.17.0.4');
INSERT INTO `sys_role_permission` VALUES ('2c93808275f3156b0175f3156dc40072', 'ff80808175d52fbb0175d5f2f7120028', '1310433915395117057', NULL, '2020-11-23 11:11:09', '192.168.214.162,172.17.0.4');
INSERT INTO `sys_role_permission` VALUES ('2c93808275f3156b0175f3156dc40073', 'ff80808175d52fbb0175d5f2f7120028', '1310432505559519233', NULL, '2020-11-23 11:11:09', '192.168.214.162,172.17.0.4');
INSERT INTO `sys_role_permission` VALUES ('2c93808275f3156b0175f3156dc40074', 'ff80808175d52fbb0175d5f2f7120028', '1310422936447881217', NULL, '2020-11-23 11:11:09', '192.168.214.162,172.17.0.4');
INSERT INTO `sys_role_permission` VALUES ('2c93808275f3156b0175f3156dc40075', 'ff80808175d52fbb0175d5f2f7120028', '1301096112848900098', NULL, '2020-11-23 11:11:09', '192.168.214.162,172.17.0.4');
INSERT INTO `sys_role_permission` VALUES ('2c93808275f3156b0175f3156dc40076', 'ff80808175d52fbb0175d5f2f7120028', '1301088665497243650', NULL, '2020-11-23 11:11:09', '192.168.214.162,172.17.0.4');
INSERT INTO `sys_role_permission` VALUES ('2c93808275f3156b0175f3156dc40077', 'ff80808175d52fbb0175d5f2f7120028', '1301001912425910273', NULL, '2020-11-23 11:11:09', '192.168.214.162,172.17.0.4');
INSERT INTO `sys_role_permission` VALUES ('2c93808275f3156b0175f3156dc40078', 'ff80808175d52fbb0175d5f2f7120028', '1310410534536876033', NULL, '2020-11-23 11:11:09', '192.168.214.162,172.17.0.4');
INSERT INTO `sys_role_permission` VALUES ('2c93808275f3156b0175f3156dc40079', 'ff80808175d52fbb0175d5f2f7120028', '1310400531214110721', NULL, '2020-11-23 11:11:09', '192.168.214.162,172.17.0.4');
INSERT INTO `sys_role_permission` VALUES ('2c93808275f3156b0175f3156dc4007a', 'ff80808175d52fbb0175d5f2f7120028', '1301428144502214658', NULL, '2020-11-23 11:11:09', '192.168.214.162,172.17.0.4');
INSERT INTO `sys_role_permission` VALUES ('2c93808275f3156b0175f3156dc4007b', 'ff80808175d52fbb0175d5f2f7120028', '1310403041836404738', NULL, '2020-11-23 11:11:09', '192.168.214.162,172.17.0.4');
INSERT INTO `sys_role_permission` VALUES ('2c93808275f3156b0175f3156dc4007c', 'ff80808175d52fbb0175d5f2f7120028', '1310413263179091969', NULL, '2020-11-23 11:11:09', '192.168.214.162,172.17.0.4');
INSERT INTO `sys_role_permission` VALUES ('2c93808275f3156b0175f3156dc4007d', 'ff80808175d52fbb0175d5f2f7120028', '1310412463300153345', NULL, '2020-11-23 11:11:09', '192.168.214.162,172.17.0.4');
INSERT INTO `sys_role_permission` VALUES ('2c93808275f3156b0175f3156dc4007e', 'ff80808175d52fbb0175d5f2f7120028', '1310416488150077442', NULL, '2020-11-23 11:11:09', '192.168.214.162,172.17.0.4');
INSERT INTO `sys_role_permission` VALUES ('2c93808275f3156b0175f3156dc4007f', 'ff80808175d52fbb0175d5f2f7120028', '1310409822184673282', NULL, '2020-11-23 11:11:09', '192.168.214.162,172.17.0.4');
INSERT INTO `sys_role_permission` VALUES ('2c93808275f3156b0175f3156dc40080', 'ff80808175d52fbb0175d5f2f7120028', '1309379979724722178', NULL, '2020-11-23 11:11:09', '192.168.214.162,172.17.0.4');
INSERT INTO `sys_role_permission` VALUES ('2c93808275f3156b0175f3156dc40081', 'ff80808175d52fbb0175d5f2f7120028', '1304321289430765570', NULL, '2020-11-23 11:11:09', '192.168.214.162,172.17.0.4');
INSERT INTO `sys_role_permission` VALUES ('2c93808275f3156b0175f3156dc40082', 'ff80808175d52fbb0175d5f2f7120028', '1301802633433976834', NULL, '2020-11-23 11:11:09', '192.168.214.162,172.17.0.4');
INSERT INTO `sys_role_permission` VALUES ('2c93808275f3156b0175f3156dc40083', 'ff80808175d52fbb0175d5f2f7120028', '1310400808977698817', NULL, '2020-11-23 11:11:09', '192.168.214.162,172.17.0.4');
INSERT INTO `sys_role_permission` VALUES ('2c93808275f3156b0175f3156dc40084', 'ff80808175d52fbb0175d5f2f7120028', '1310404620442406913', NULL, '2020-11-23 11:11:09', '192.168.214.162,172.17.0.4');
INSERT INTO `sys_role_permission` VALUES ('2c93808275f3156b0175f3156dc40085', 'ff80808175d52fbb0175d5f2f7120028', '1310432897492062210', NULL, '2020-11-23 11:11:09', '192.168.214.162,172.17.0.4');
INSERT INTO `sys_role_permission` VALUES ('2c93808275f3156b0175f3156dc40086', 'ff80808175d52fbb0175d5f2f7120028', '1310403230580084737', NULL, '2020-11-23 11:11:09', '192.168.214.162,172.17.0.4');
INSERT INTO `sys_role_permission` VALUES ('2c93808275f3156b0175f3156dc40087', 'ff80808175d52fbb0175d5f2f7120028', '1310404991655088130', NULL, '2020-11-23 11:11:09', '192.168.214.162,172.17.0.4');
INSERT INTO `sys_role_permission` VALUES ('2c93808275f3156b0175f3156dc50088', 'ff80808175d52fbb0175d5f2f7120028', '1310410017085591553', NULL, '2020-11-23 11:11:09', '192.168.214.162,172.17.0.4');
INSERT INTO `sys_role_permission` VALUES ('2c93808275f3156b0175f3156dc50089', 'ff80808175d52fbb0175d5f2f7120028', '1310433274404802561', NULL, '2020-11-23 11:11:09', '192.168.214.162,172.17.0.4');
INSERT INTO `sys_role_permission` VALUES ('2c93808275f3156b0175f3156dc5008a', 'ff80808175d52fbb0175d5f2f7120028', '1310401085768208385', NULL, '2020-11-23 11:11:09', '192.168.214.162,172.17.0.4');
INSERT INTO `sys_role_permission` VALUES ('2c93808275f3156b0175f3156dc5008b', 'ff80808175d52fbb0175d5f2f7120028', '1310399637995130881', NULL, '2020-11-23 11:11:09', '192.168.214.162,172.17.0.4');
INSERT INTO `sys_role_permission` VALUES ('2c93808275f3156b0175f3156dc5008c', 'ff80808175d52fbb0175d5f2f7120028', '1310403473635807234', NULL, '2020-11-23 11:11:09', '192.168.214.162,172.17.0.4');
INSERT INTO `sys_role_permission` VALUES ('2c93808275f3156b0175f3156dc5008d', 'ff80808175d52fbb0175d5f2f7120028', '1310415795251056642', NULL, '2020-11-23 11:11:09', '192.168.214.162,172.17.0.4');
INSERT INTO `sys_role_permission` VALUES ('2c93808275f3156b0175f3156dc5008e', 'ff80808175d52fbb0175d5f2f7120028', '1310406951590432769', NULL, '2020-11-23 11:11:09', '192.168.214.162,172.17.0.4');
INSERT INTO `sys_role_permission` VALUES ('2c93808275f3156b0175f3156dc5008f', 'ff80808175d52fbb0175d5f2f7120028', '1310405261181063170', NULL, '2020-11-23 11:11:09', '192.168.214.162,172.17.0.4');
INSERT INTO `sys_role_permission` VALUES ('2c93808275f3156b0175f3156dc50090', 'ff80808175d52fbb0175d5f2f7120028', '1310407190049198081', NULL, '2020-11-23 11:11:09', '192.168.214.162,172.17.0.4');
INSERT INTO `sys_role_permission` VALUES ('2c93808275f3156b0175f3156dc50091', 'ff80808175d52fbb0175d5f2f7120028', '1310401229901271042', NULL, '2020-11-23 11:11:09', '192.168.214.162,172.17.0.4');
INSERT INTO `sys_role_permission` VALUES ('2c93808275f3156b0175f3156dc50092', 'ff80808175d52fbb0175d5f2f7120028', '1310403837600731138', NULL, '2020-11-23 11:11:09', '192.168.214.162,172.17.0.4');
INSERT INTO `sys_role_permission` VALUES ('2c93808275f3156b0175f3156dc50093', 'ff80808175d52fbb0175d5f2f7120028', '1310416188978761730', NULL, '2020-11-23 11:11:09', '192.168.214.162,172.17.0.4');
INSERT INTO `sys_role_permission` VALUES ('2c93808275f3156b0175f3156dc50094', 'ff80808175d52fbb0175d5f2f7120028', '1310405668003385346', NULL, '2020-11-23 11:11:09', '192.168.214.162,172.17.0.4');
INSERT INTO `sys_role_permission` VALUES ('2c93808275f3156b0175f3156dc50095', 'ff80808175d52fbb0175d5f2f7120028', '1310407413609795585', NULL, '2020-11-23 11:11:09', '192.168.214.162,172.17.0.4');
INSERT INTO `sys_role_permission` VALUES ('2c93808275f3156b0175f3156dc50096', 'ff80808175d52fbb0175d5f2f7120028', '1310401446956503042', NULL, '2020-11-23 11:11:09', '192.168.214.162,172.17.0.4');
INSERT INTO `sys_role_permission` VALUES ('2c93808275f3156b0175f3156dc50097', 'ff80808175d52fbb0175d5f2f7120028', '1310401620621660162', NULL, '2020-11-23 11:11:09', '192.168.214.162,172.17.0.4');
INSERT INTO `sys_role_permission` VALUES ('2c93808275f3156b0175f3156dc50098', 'ff80808175d52fbb0175d5f2f7120028', '1301447721621655554', NULL, '2020-11-23 11:11:09', '192.168.214.162,172.17.0.4');
INSERT INTO `sys_role_permission` VALUES ('2c93808275f3156b0175f3156dc50099', 'ff80808175d52fbb0175d5f2f7120028', '1310406050767179777', NULL, '2020-11-23 11:11:09', '192.168.214.162,172.17.0.4');
INSERT INTO `sys_role_permission` VALUES ('2c93808275f3156b0175f3156dc5009a', 'ff80808175d52fbb0175d5f2f7120028', '1310406264022372354', NULL, '2020-11-23 11:11:09', '192.168.214.162,172.17.0.4');
INSERT INTO `sys_role_permission` VALUES ('2c93808275f3156b0175f3156dc5009b', 'ff80808175d52fbb0175d5f2f7120028', '1310401771595632641', NULL, '2020-11-23 11:11:09', '192.168.214.162,172.17.0.4');
INSERT INTO `sys_role_permission` VALUES ('2c93808275f3156b0175f3156dc5009c', 'ff80808175d52fbb0175d5f2f7120028', '1310407620586115073', NULL, '2020-11-23 11:11:09', '192.168.214.162,172.17.0.4');
INSERT INTO `sys_role_permission` VALUES ('2c93808275f3156b0175f3156dc5009d', 'ff80808175d52fbb0175d5f2f7120028', '1310406614360002562', NULL, '2020-11-23 11:11:09', '192.168.214.162,172.17.0.4');
INSERT INTO `sys_role_permission` VALUES ('2c93808275f3156b0175f3156dc5009e', 'ff80808175d52fbb0175d5f2f7120028', '1310401950704996353', NULL, '2020-11-23 11:11:09', '192.168.214.162,172.17.0.4');
INSERT INTO `sys_role_permission` VALUES ('2c93808275f3156b0175f3156dc5009f', 'ff80808175d52fbb0175d5f2f7120028', '1300999295377346562', NULL, '2020-11-23 11:11:09', '192.168.214.162,172.17.0.4');
INSERT INTO `sys_role_permission` VALUES ('2c93808275f3156b0175f3156dc500a0', 'ff80808175d52fbb0175d5f2f7120028', '1300999947163799554', NULL, '2020-11-23 11:11:09', '192.168.214.162,172.17.0.4');
INSERT INTO `sys_role_permission` VALUES ('2c93808275f3156b0175f3156dc500a1', 'ff80808175d52fbb0175d5f2f7120028', '1301000181361152001', NULL, '2020-11-23 11:11:09', '192.168.214.162,172.17.0.4');
INSERT INTO `sys_role_permission` VALUES ('2c93808275f3156b0175f3156dc500a2', 'ff80808175d52fbb0175d5f2f7120028', '1301000639995711490', NULL, '2020-11-23 11:11:09', '192.168.214.162,172.17.0.4');
INSERT INTO `sys_role_permission` VALUES ('2c93808275f3156b0175f3156dc500a3', 'ff80808175d52fbb0175d5f2f7120028', '1301000852273631234', NULL, '2020-11-23 11:11:09', '192.168.214.162,172.17.0.4');
INSERT INTO `sys_role_permission` VALUES ('2c93808275f3156b0175f317ddf800c8', '8a6e50b54dc268dc014dc27926170117', '8a8281855b5aa892015b60298e77432e', NULL, '2020-11-23 11:13:49', '192.168.214.162,172.17.0.4');
INSERT INTO `sys_role_permission` VALUES ('2c93808275f3156b0175f317ddf800c9', '8a6e50b54dc268dc014dc27926170117', '8a6e4f1f4ed42653014ed42c25f70000', NULL, '2020-11-23 11:13:49', '192.168.214.162,172.17.0.4');
INSERT INTO `sys_role_permission` VALUES ('2c93808275f3156b0175f317ddf800ca', '8a6e50b54dc268dc014dc27926170117', '8a8082876001041d01600113fe460002', NULL, '2020-11-23 11:13:49', '192.168.214.162,172.17.0.4');
INSERT INTO `sys_role_permission` VALUES ('2c93808275f3156b0175f317ddf800cb', '8a6e50b54dc268dc014dc27926170117', '8a8281855b5aa892015b60144b6f3f8d', NULL, '2020-11-23 11:13:49', '192.168.214.162,172.17.0.4');
INSERT INTO `sys_role_permission` VALUES ('2c93808275f3156b0175f317ddf800cc', '8a6e50b54dc268dc014dc27926170117', '8a6e4e905b2790e3015b2841f9070020', NULL, '2020-11-23 11:13:49', '192.168.214.162,172.17.0.4');
INSERT INTO `sys_role_permission` VALUES ('2c93808275f3156b0175f317ddf800cd', '8a6e50b54dc268dc014dc27926170117', '8a8281855b5aa892015b6015e23f3fd3', NULL, '2020-11-23 11:13:49', '192.168.214.162,172.17.0.4');
INSERT INTO `sys_role_permission` VALUES ('2c93808275f3156b0175f317ddf800ce', '8a6e50b54dc268dc014dc27926170117', '8a8281855b5aa892015b60189a5b4055', NULL, '2020-11-23 11:13:49', '192.168.214.162,172.17.0.4');
INSERT INTO `sys_role_permission` VALUES ('2c93808275f3156b0175f317ddf800cf', '8a6e50b54dc268dc014dc27926170117', '8a8281855b5aa892015b601958064073', NULL, '2020-11-23 11:13:49', '192.168.214.162,172.17.0.4');
INSERT INTO `sys_role_permission` VALUES ('2c93808275f3156b0175f317ddf800d0', '8a6e50b54dc268dc014dc27926170117', '8a8281855b5aa892015b6021fe6e41d7', NULL, '2020-11-23 11:13:49', '192.168.214.162,172.17.0.4');
INSERT INTO `sys_role_permission` VALUES ('2c93808275f3156b0175f317ddf800d1', '8a6e50b54dc268dc014dc27926170117', '8a6e502f4d7b764d014d7bc35a921237', NULL, '2020-11-23 11:13:49', '192.168.214.162,172.17.0.4');
INSERT INTO `sys_role_permission` VALUES ('2c93808275f3156b0175f317ddf800d2', '8a6e50b54dc268dc014dc27926170117', '8a6e4f1f4ece66b3014ece7066d60000', NULL, '2020-11-23 11:13:49', '192.168.214.162,172.17.0.4');
INSERT INTO `sys_role_permission` VALUES ('2c93808275f3156b0175f317ddf800d3', '8a6e50b54dc268dc014dc27926170117', '8a6e4e905b5ac753015b5acd5f440003', NULL, '2020-11-23 11:13:49', '192.168.214.162,172.17.0.4');
INSERT INTO `sys_role_permission` VALUES ('2c93808275f3156b0175f317ddf800d4', '8a6e50b54dc268dc014dc27926170117', '8a6e4e8b6001fa28016001fd72fc0000', NULL, '2020-11-23 11:13:49', '192.168.214.162,172.17.0.4');
INSERT INTO `sys_role_permission` VALUES ('2c93808275f3156b0175f317ddf800d5', '8a6e50b54dc268dc014dc27926170117', '402881c53c1fdb18013c205f2c3c0450', NULL, '2020-11-23 11:13:49', '192.168.214.162,172.17.0.4');
INSERT INTO `sys_role_permission` VALUES ('2c93808275f3156b0175f317ddf800d6', '8a6e50b54dc268dc014dc27926170117', '402881c53c1fdb18013c205e8d8f0447', NULL, '2020-11-23 11:13:49', '192.168.214.162,172.17.0.4');
INSERT INTO `sys_role_permission` VALUES ('2c93808275f3156b0175f317ddf800d7', '8a6e50b54dc268dc014dc27926170117', '402881c53c1fdb18013c20600e48045c', NULL, '2020-11-23 11:13:49', '192.168.214.162,172.17.0.4');
INSERT INTO `sys_role_permission` VALUES ('2c93808275f3156b0175f317ddf800d8', '8a6e50b54dc268dc014dc27926170117', '402881c53c1fdb18013c2060ad300462', NULL, '2020-11-23 11:13:49', '192.168.214.162,172.17.0.4');
INSERT INTO `sys_role_permission` VALUES ('2c93808275f3156b0175f317ddf800d9', '8a6e50b54dc268dc014dc27926170117', '402881c53c1fdb18013c20615fec0468', NULL, '2020-11-23 11:13:49', '192.168.214.162,172.17.0.4');
INSERT INTO `sys_role_permission` VALUES ('2c93808275f3156b0175f317ddf800da', '8a6e50b54dc268dc014dc27926170117', '402881c53c1fdb18013c2061e9870471', NULL, '2020-11-23 11:13:49', '192.168.214.162,172.17.0.4');
INSERT INTO `sys_role_permission` VALUES ('2c93808275f3156b0175f317ddf800db', '8a6e50b54dc268dc014dc27926170117', '402881c53c1fdb18013c205dded40441', NULL, '2020-11-23 11:13:49', '192.168.214.162,172.17.0.4');
INSERT INTO `sys_role_permission` VALUES ('2c93808275f3156b0175f317ddf800dc', '8a6e50b54dc268dc014dc27926170117', '8a6e4e905ba99a4d015ba99bc3ba0139', NULL, '2020-11-23 11:13:49', '192.168.214.162,172.17.0.4');
INSERT INTO `sys_role_permission` VALUES ('2c93808275f3156b0175f317ddf800dd', '8a6e50b54dc268dc014dc27926170117', '8a6e4e905ba99a4d015ba99bc2b30102', NULL, '2020-11-23 11:13:49', '192.168.214.162,172.17.0.4');
INSERT INTO `sys_role_permission` VALUES ('2c93808275f3156b0175f317ddf800de', '8a6e50b54dc268dc014dc27926170117', '8a6e4e905ba99a4d015ba99bc2ae00fe', NULL, '2020-11-23 11:13:49', '192.168.214.162,172.17.0.4');
INSERT INTO `sys_role_permission` VALUES ('2c93808275f3156b0175f317ddf800df', '8a6e50b54dc268dc014dc27926170117', '402881c53c1fdb18013c205aac2c0414', NULL, '2020-11-23 11:13:49', '192.168.214.162,172.17.0.4');
INSERT INTO `sys_role_permission` VALUES ('2c93808275f3156b0175f317ddf800e0', '8a6e50b54dc268dc014dc27926170117', '402881c53c1fdb18013c205a201b040b', NULL, '2020-11-23 11:13:49', '192.168.214.162,172.17.0.4');
INSERT INTO `sys_role_permission` VALUES ('2c93808275f3156b0175f317ddf800e1', '8a6e50b54dc268dc014dc27926170117', '402881c53c1fdb18013c20596dc30405', NULL, '2020-11-23 11:13:49', '192.168.214.162,172.17.0.4');
INSERT INTO `sys_role_permission` VALUES ('2c93808275f3156b0175f317ddf800e2', '8a6e50b54dc268dc014dc27926170117', '402881c53c1fdb18013c2058d8a503ff', NULL, '2020-11-23 11:13:49', '192.168.214.162,172.17.0.4');
INSERT INTO `sys_role_permission` VALUES ('2c93808275f3156b0175f317ddf800e3', '8a6e50b54dc268dc014dc27926170117', '402881c53c1fdb18013c205884e803f9', NULL, '2020-11-23 11:13:49', '192.168.214.162,172.17.0.4');
INSERT INTO `sys_role_permission` VALUES ('2c93808275f3156b0175f317ddf800e4', '8a6e50b54dc268dc014dc27926170117', '402881c53c1fdb18013c205b0fc2041a', NULL, '2020-11-23 11:13:49', '192.168.214.162,172.17.0.4');
INSERT INTO `sys_role_permission` VALUES ('2c93808275f3156b0175f317ddf800e5', '8a6e50b54dc268dc014dc27926170117', '402881c53c1fdb18013c205b85ba0420', NULL, '2020-11-23 11:13:49', '192.168.214.162,172.17.0.4');
INSERT INTO `sys_role_permission` VALUES ('2c93808275f3156b0175f317ddf800e6', '8a6e50b54dc268dc014dc27926170117', '402881c53c1fdb18013c205c337e0426', NULL, '2020-11-23 11:13:49', '192.168.214.162,172.17.0.4');
INSERT INTO `sys_role_permission` VALUES ('2c93808275f3156b0175f317ddf800e7', '8a6e50b54dc268dc014dc27926170117', '402881c53c1fdb18013c205c9f5d042f', NULL, '2020-11-23 11:13:49', '192.168.214.162,172.17.0.4');
INSERT INTO `sys_role_permission` VALUES ('2c93808275f3156b0175f317ddf800e8', '8a6e50b54dc268dc014dc27926170117', '402881c53c1fdb18013c205d03190435', NULL, '2020-11-23 11:13:49', '192.168.214.162,172.17.0.4');
INSERT INTO `sys_role_permission` VALUES ('2c93808275f3156b0175f317ddf800e9', '8a6e50b54dc268dc014dc27926170117', '8a6e4e905ba99a4d015ba99bc02c0075', NULL, '2020-11-23 11:13:49', '192.168.214.162,172.17.0.4');
INSERT INTO `sys_role_permission` VALUES ('2c93808275f3156b0175f317ddf800ea', '8a6e50b54dc268dc014dc27926170117', '8a6e4e905ba99a4d015ba99bc0540077', NULL, '2020-11-23 11:13:49', '192.168.214.162,172.17.0.4');
INSERT INTO `sys_role_permission` VALUES ('2c93808275f3156b0175f317ddf800eb', '8a6e50b54dc268dc014dc27926170117', '8a6e4e905ba99a4d015ba99bc05e0079', NULL, '2020-11-23 11:13:49', '192.168.214.162,172.17.0.4');
INSERT INTO `sys_role_permission` VALUES ('2c93808275f3156b0175f317ddf800ec', '8a6e50b54dc268dc014dc27926170117', '402881c53c1fdb18013c20559c7e03d2', NULL, '2020-11-23 11:13:49', '192.168.214.162,172.17.0.4');
INSERT INTO `sys_role_permission` VALUES ('2c93808275f3156b0175f317ddf800ed', '8a6e50b54dc268dc014dc27926170117', '402881c53c1fdb18013c20563e6a03db', NULL, '2020-11-23 11:13:49', '192.168.214.162,172.17.0.4');
INSERT INTO `sys_role_permission` VALUES ('2c93808275f3156b0175f317ddf800ee', '8a6e50b54dc268dc014dc27926170117', '402881c53c1fdb18013c2056d2a703e4', NULL, '2020-11-23 11:13:49', '192.168.214.162,172.17.0.4');
INSERT INTO `sys_role_permission` VALUES ('2c93808275f3156b0175f317ddf800ef', '8a6e50b54dc268dc014dc27926170117', '402881c53c1fdb18013c20573e9d03ea', NULL, '2020-11-23 11:13:49', '192.168.214.162,172.17.0.4');
INSERT INTO `sys_role_permission` VALUES ('2c93808275f3156b0175f317ddf800f0', '8a6e50b54dc268dc014dc27926170117', 'D2EEF7144DF3A123E040A8C0580045A7', NULL, '2020-11-23 11:13:49', '192.168.214.162,172.17.0.4');
INSERT INTO `sys_role_permission` VALUES ('2c93808275f3156b0175f317ddf800f1', '8a6e50b54dc268dc014dc27926170117', 'DDBED3F37E7034EAE040A8C058006EF4', NULL, '2020-11-23 11:13:49', '192.168.214.162,172.17.0.4');
INSERT INTO `sys_role_permission` VALUES ('2c93808275f3156b0175f317ddf800f2', '8a6e50b54dc268dc014dc27926170117', '8a6e4e905ba99a4d015ba99bbe370012', NULL, '2020-11-23 11:13:49', '192.168.214.162,172.17.0.4');
INSERT INTO `sys_role_permission` VALUES ('2c93808275f3156b0175f317ddf800f3', '8a6e50b54dc268dc014dc27926170117', '8a6e4e905ba99a4d015ba99bbe5f0014', NULL, '2020-11-23 11:13:49', '192.168.214.162,172.17.0.4');
INSERT INTO `sys_role_permission` VALUES ('2c93808275f3156b0175f317ddf800f4', '8a6e50b54dc268dc014dc27926170117', '402881c53c1fdb18013c2058008403f0', NULL, '2020-11-23 11:13:49', '192.168.214.162,172.17.0.4');
INSERT INTO `sys_role_permission` VALUES ('2c93808275f3156b0175f317ddf800f5', '8a6e50b54dc268dc014dc27926170117', '402881c53c1fdb18013c205f99290456', NULL, '2020-11-23 11:13:49', '192.168.214.162,172.17.0.4');
INSERT INTO `sys_role_permission` VALUES ('2dc1a0eb5e39aaa131ddd0082a492d76', 'ee8626f80f7c2619917b6236f3a7f02b', '08e6b9dc3c04489c8e1ff2ce6f105aa4', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('2ea2382af618ba7d1e80491a0185fb8a', 'ee8626f80f7c2619917b6236f3a7f02b', 'f23d9bfff4d9aa6b68569ba2cff38415', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('2fcfa2ac3dcfadc7c67107dae9a0de6d', 'ee8626f80f7c2619917b6236f3a7f02b', '73678f9daa45ed17a3674131b03432fb', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('2fdaed22dfa4c8d4629e44ef81688c6a', '52b0cf022ac4187b2a70dfa4f8b2d940', 'aedbf679b5773c1f25e9f7b10111da73', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('300c462b7fec09e2ff32574ef8b3f0bd', '52b0cf022ac4187b2a70dfa4f8b2d940', '2a470fc0c3954d9dbb61de6d80846549', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('326181da3248a62a05e872119a462be1', 'ee8626f80f7c2619917b6236f3a7f02b', '3f915b2769fc80648e92d04e84ca059d', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('3369650f5072b330543f8caa556b1b33', 'e51758fa916c881624b046d26bd09230', 'a400e4f4d54f79bf5ce160ae432231af', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('35a7e156c20e93aa7e825fe514bf9787', 'e51758fa916c881624b046d26bd09230', 'c6cf95444d80435eb37b2f9db3971ae6', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('35ac7cae648de39eb56213ca1b649713', '52b0cf022ac4187b2a70dfa4f8b2d940', 'b1cb0a3fedf7ed0e4653cb5a229837ee', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('37112f4d372541e105473f18da3dc50d', 'ee8626f80f7c2619917b6236f3a7f02b', 'a400e4f4d54f79bf5ce160ae432231af', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('37789f70cd8bd802c4a69e9e1f633eaa', 'ee8626f80f7c2619917b6236f3a7f02b', 'ae4fed059f67086fd52a73d913cf473d', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('381504a717cb3ce77dcd4070c9689a7e', 'ee8626f80f7c2619917b6236f3a7f02b', '4f84f9400e5e92c95f05b554724c2b58', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('38a2e55db0960262800576e34b3af44c', 'f6817f48af4fb3af11b9e8bf182f618b', '5c2f42277948043026b7a14692456828', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('38dd7a19711e7ffe864232954c06fae9', 'e51758fa916c881624b046d26bd09230', 'd2bbf9ebca5a8fa2e227af97d2da7548', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('3b1886f727ac503c93fecdd06dcb9622', 'f6817f48af4fb3af11b9e8bf182f618b', 'c431130c0bc0ec71b0a5be37747bb36a', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('3e4e38f748b8d87178dd62082e5b7b60', 'f6817f48af4fb3af11b9e8bf182f618b', '7960961b0063228937da5fa8dd73d371', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('3e563751942b0879c88ca4de19757b50', '1750a8fb3e6d90cb7957c02de1dc8e59', '58857ff846e61794c69208e9d3a85466', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('3f1d04075e3c3254666a4138106a4e51', 'f6817f48af4fb3af11b9e8bf182f618b', '3fac0d3c9cd40fa53ab70d4c583821f8', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('412e2de37a35b3442d68db8dd2f3c190', '52b0cf022ac4187b2a70dfa4f8b2d940', 'f1cb187abf927c88b89470d08615f5ac', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('4204f91fb61911ba8ce40afa7c02369f', 'f6817f48af4fb3af11b9e8bf182f618b', '3f915b2769fc80648e92d04e84ca059d', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('444126230885d5d38b8fa6072c9f43f8', 'f6817f48af4fb3af11b9e8bf182f618b', 'f780d0d3083d849ccbdb1b1baee4911d', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('445656dd187bd8a71605f4bbab1938a3', 'f6817f48af4fb3af11b9e8bf182f618b', '020b06793e4de2eee0007f603000c769', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('44b5a73541bcb854dd5d38c6d1fb93a1', 'ee8626f80f7c2619917b6236f3a7f02b', '418964ba087b90a84897b62474496b93', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('455cdb482457f529b79b479a2ff74427', 'f6817f48af4fb3af11b9e8bf182f618b', 'e1979bb53e9ea51cecc74d86fd9d2f64', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('459aa2e7021b435b4d65414cfbc71c66', 'e51758fa916c881624b046d26bd09230', '4148ec82b6acd69f470bea75fe41c357', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('45a358bb738782d1a0edbf7485e81459', 'f6817f48af4fb3af11b9e8bf182f618b', '0ac2ad938963b6c6d1af25477d5b8b51', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('4c0940badae3ef9231ee9d042338f2a4', 'e51758fa916c881624b046d26bd09230', '2a470fc0c3954d9dbb61de6d80846549', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('4d56ce2f67c94b74a1d3abdbea340e42', 'ee8626f80f7c2619917b6236f3a7f02b', 'd86f58e7ab516d3bc6bfb1fe10585f97', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('4e0a37ed49524df5f08fc6593aee875c', 'f6817f48af4fb3af11b9e8bf182f618b', 'f23d9bfff4d9aa6b68569ba2cff38415', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('4ea403fc1d19feb871c8bdd9f94a4ecc', 'f6817f48af4fb3af11b9e8bf182f618b', '2e42e3835c2b44ec9f7bc26c146ee531', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('4faad8ff93cb2b5607cd3d07c1b624ee', 'a799c3b1b12dd3ed4bd046bfaef5fe6e', '70b8f33da5f39de1981bf89cf6c99792', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('520b5989e6fe4a302a573d4fee12a40a', 'f6817f48af4fb3af11b9e8bf182f618b', '6531cf3421b1265aeeeabaab5e176e6d', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('54fdf85e52807bdb32ce450814abc256', 'f6817f48af4fb3af11b9e8bf182f618b', 'cc50656cf9ca528e6f2150eba4714ad2', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('57c0b3a547b815ea3ec8e509b08948b3', '1750a8fb3e6d90cb7957c02de1dc8e59', '3f915b2769fc80648e92d04e84ca059d', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('593ee05c4fe4645c7826b7d5e14f23ec', '52b0cf022ac4187b2a70dfa4f8b2d940', '8fb8172747a78756c11916216b8b8066', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('5affc85021fcba07d81c09a6fdfa8dc6', 'ee8626f80f7c2619917b6236f3a7f02b', '078f9558cdeab239aecb2bda1a8ed0d1', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('5d230e6cd2935c4117f6cb9a7a749e39', 'f6817f48af4fb3af11b9e8bf182f618b', 'fc810a2267dd183e4ef7c71cc60f4670', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('5e4015a9a641cbf3fb5d28d9f885d81a', 'f6817f48af4fb3af11b9e8bf182f618b', '2dbbafa22cda07fa5d169d741b81fe12', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('5e634a89f75b7a421c02aecfd520325f', 'e51758fa916c881624b046d26bd09230', '339329ed54cf255e1f9392e84f136901', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('5e74637c4bec048d1880ad0bd1b00552', 'e51758fa916c881624b046d26bd09230', 'a400e4f4d54f79bf5ce160a3432231af', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('5fc194b709336d354640fe29fefd65a3', 'a799c3b1b12dd3ed4bd046bfaef5fe6e', '9ba60e626bf2882c31c488aba62b89f0', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('60eda4b4db138bdb47edbe8e10e71675', 'f6817f48af4fb3af11b9e8bf182f618b', 'fb07ca05a3e13674dbf6d3245956da2e', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('61835e48f3e675f7d3f5c9dd3a10dcf3', 'f6817f48af4fb3af11b9e8bf182f618b', 'f0675b52d89100ee88472b6800754a08', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('6451dac67ba4acafb570fd6a03f47460', 'ee8626f80f7c2619917b6236f3a7f02b', 'e3c13679c73a4f829bcff2aba8fd68b1', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('660fbc40bcb1044738f7cabdf1708c28', 'f6817f48af4fb3af11b9e8bf182f618b', 'b3c824fc22bd953e2eb16ae6914ac8f9', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('66b202f8f84fe766176b3f51071836ef', 'f6817f48af4fb3af11b9e8bf182f618b', '1367a93f2c410b169faa7abcbad2f77c', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('6b605c261ffbc8ac8a98ae33579c8c78', 'f6817f48af4fb3af11b9e8bf182f618b', 'fba41089766888023411a978d13c0aa4', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('6c43fd3f10fdaf2a0646434ae68709b5', 'ee8626f80f7c2619917b6236f3a7f02b', '540a2936940846cb98114ffb0d145cb8', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('6c74518eb6bb9a353f6a6c459c77e64b', 'f6817f48af4fb3af11b9e8bf182f618b', 'b4dfc7d5dd9e8d5b6dd6d4579b1aa559', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('6fb4c2142498dd6d5b6c014ef985cb66', 'f6817f48af4fb3af11b9e8bf182f618b', '6e73eb3c26099c191bf03852ee1310a1', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('71a5f54a90aa8c7a250a38b7dba39f6f', 'ee8626f80f7c2619917b6236f3a7f02b', '8fb8172747a78756c11916216b8b8066', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('737d35f582036cd18bfd4c8e5748eaa4', 'e51758fa916c881624b046d26bd09230', '693ce69af3432bd00be13c3971a57961', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('75002588591820806', '16457350655250432', '5129710648430592', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('75002588604403712', '16457350655250432', '5129710648430593', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('75002588612792320', '16457350655250432', '40238597734928384', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('75002588625375232', '16457350655250432', '57009744761589760', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('75002588633763840', '16457350655250432', '16392452747300864', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('75002588637958144', '16457350655250432', '16392767785668608', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('75002588650541056', '16457350655250432', '16439068543946752', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('76a54a8cc609754360bf9f57e7dbb2db', 'f6817f48af4fb3af11b9e8bf182f618b', 'c65321e57b7949b7a975313220de0422', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('77277779875336192', '496138616573952', '5129710648430592', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('77277780043108352', '496138616573952', '5129710648430593', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('77277780055691264', '496138616573952', '15701400130424832', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('77277780064079872', '496138616573952', '16678126574637056', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('77277780072468480', '496138616573952', '15701915807518720', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('77277780076662784', '496138616573952', '15708892205944832', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('77277780085051392', '496138616573952', '16678447719911424', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('77277780089245696', '496138616573952', '25014528525733888', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('77277780097634304', '496138616573952', '56898976661639168', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('77277780135383040', '496138616573952', '40238597734928384', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('77277780139577344', '496138616573952', '45235621697949696', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('77277780147965952', '496138616573952', '45235787867885568', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('77277780156354560', '496138616573952', '45235939278065664', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('77277780164743168', '496138616573952', '43117268627886080', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('77277780168937472', '496138616573952', '45236734832676864', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('77277780181520384', '496138616573952', '45237010692050944', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('77277780189908992', '496138616573952', '45237170029465600', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('77277780198297600', '496138616573952', '57009544286441472', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('77277780206686208', '496138616573952', '57009744761589760', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('77277780215074816', '496138616573952', '57009981228060672', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('77277780219269120', '496138616573952', '56309618086776832', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('77277780227657728', '496138616573952', '57212882168844288', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('77277780236046336', '496138616573952', '61560041605435392', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('77277780244434944', '496138616573952', '61560275261722624', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('77277780257017856', '496138616573952', '61560480518377472', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('77277780265406464', '496138616573952', '44986029924421632', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('77277780324126720', '496138616573952', '45235228800716800', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('77277780332515328', '496138616573952', '45069342940860416', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('77277780340903937', '496138616573952', '5129710648430594', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('77277780349292544', '496138616573952', '16687383932047360', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('77277780357681152', '496138616573952', '16689632049631232', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('77277780366069760', '496138616573952', '16689745006432256', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('77277780370264064', '496138616573952', '16689883993083904', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('77277780374458369', '496138616573952', '16690313745666048', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('77277780387041280', '496138616573952', '5129710648430595', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('77277780395429888', '496138616573952', '16694861252005888', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('77277780403818496', '496138616573952', '16695107491205120', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('77277780412207104', '496138616573952', '16695243126607872', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('77277780420595712', '496138616573952', '75002207560273920', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('77277780428984320', '496138616573952', '76215889006956544', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('77277780433178624', '496138616573952', '76216071333351424', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('77277780441567232', '496138616573952', '76216264070008832', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('77277780449955840', '496138616573952', '76216459709124608', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('77277780458344448', '496138616573952', '76216594207870976', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('77277780466733056', '496138616573952', '76216702639017984', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('77277780475121664', '496138616573952', '58480609315524608', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('77277780483510272', '496138616573952', '61394706252173312', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('77277780491898880', '496138616573952', '61417744146370560', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('77277780496093184', '496138616573952', '76606430504816640', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('77277780504481792', '496138616573952', '76914082455752704', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('77277780508676097', '496138616573952', '76607201262702592', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('77277780517064704', '496138616573952', '39915540965232640', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('77277780525453312', '496138616573952', '41370251991977984', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('77277780538036224', '496138616573952', '45264987354042368', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('77277780546424832', '496138616573952', '45265487029866496', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('77277780554813440', '496138616573952', '45265762415284224', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('77277780559007744', '496138616573952', '45265886315024384', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('77277780567396352', '496138616573952', '45266070000373760', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('77277780571590656', '496138616573952', '41363147411427328', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('77277780579979264', '496138616573952', '41363537456533504', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('77277780588367872', '496138616573952', '41364927394353152', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('77277780596756480', '496138616573952', '41371711400054784', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('77277780605145088', '496138616573952', '41469219249852416', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('77277780613533696', '496138616573952', '39916171171991552', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('77277780621922304', '496138616573952', '39918482854252544', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('77277780630310912', '496138616573952', '41373430515240960', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('77277780718391296', '496138616573952', '41375330996326400', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('77277780722585600', '496138616573952', '63741744973352960', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('77277780730974208', '496138616573952', '42082442672082944', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('77277780739362816', '496138616573952', '41376192166629376', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('77277780747751424', '496138616573952', '41377034236071936', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('77277780756140032', '496138616573952', '56911328312299520', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('77277780764528640', '496138616573952', '41378916912336896', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('77277780768722944', '496138616573952', '63482475359244288', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('77277780772917249', '496138616573952', '64290663792906240', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('77277780785500160', '496138616573952', '66790433014943744', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('77277780789694464', '496138616573952', '42087054753927168', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('77277780798083072', '496138616573952', '67027338952445952', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('77277780806471680', '496138616573952', '67027909637836800', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('77277780810665985', '496138616573952', '67042515441684480', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('77277780823248896', '496138616573952', '67082402312228864', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('77277780827443200', '496138616573952', '16392452747300864', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('77277780835831808', '496138616573952', '16392767785668608', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('77277780840026112', '496138616573952', '16438800255291392', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('77277780844220417', '496138616573952', '16438962738434048', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('77277780852609024', '496138616573952', '16439068543946752', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('77277860062040064', '496138616573953', '5129710648430592', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('77277860070428672', '496138616573953', '5129710648430593', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('77277860078817280', '496138616573953', '40238597734928384', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('77277860091400192', '496138616573953', '43117268627886080', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('77277860099788800', '496138616573953', '57009744761589760', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('77277860112371712', '496138616573953', '56309618086776832', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('77277860120760320', '496138616573953', '44986029924421632', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('77277860129148928', '496138616573953', '5129710648430594', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('77277860141731840', '496138616573953', '5129710648430595', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('77277860150120448', '496138616573953', '75002207560273920', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('77277860158509056', '496138616573953', '58480609315524608', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('77277860162703360', '496138616573953', '76606430504816640', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('77277860171091968', '496138616573953', '76914082455752704', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('77277860179480576', '496138616573953', '76607201262702592', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('77277860187869184', '496138616573953', '39915540965232640', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('77277860196257792', '496138616573953', '41370251991977984', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('77277860204646400', '496138616573953', '41363147411427328', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('77277860208840704', '496138616573953', '41371711400054784', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('77277860213035009', '496138616573953', '39916171171991552', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('77277860221423616', '496138616573953', '39918482854252544', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('77277860225617920', '496138616573953', '41373430515240960', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('77277860234006528', '496138616573953', '41375330996326400', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('77277860242395136', '496138616573953', '63741744973352960', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('77277860250783744', '496138616573953', '42082442672082944', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('77277860254978048', '496138616573953', '41376192166629376', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('77277860263366656', '496138616573953', '41377034236071936', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('77277860271755264', '496138616573953', '56911328312299520', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('77277860313698304', '496138616573953', '41378916912336896', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('77277860322086912', '496138616573953', '63482475359244288', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('77277860326281216', '496138616573953', '64290663792906240', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('77277860334669824', '496138616573953', '66790433014943744', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('77277860343058432', '496138616573953', '42087054753927168', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('77277860347252736', '496138616573953', '67027338952445952', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('77277860351447041', '496138616573953', '67027909637836800', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('77277860359835648', '496138616573953', '67042515441684480', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('77277860364029952', '496138616573953', '67082402312228864', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('77277860368224256', '496138616573953', '16392452747300864', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('77277860372418560', '496138616573953', '16392767785668608', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('77277860376612865', '496138616573953', '16438800255291392', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('77277860385001472', '496138616573953', '16438962738434048', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('77277860389195776', '496138616573953', '16439068543946752', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('7750f9be48ee09cd561fce718219a3e2', 'ee8626f80f7c2619917b6236f3a7f02b', '882a73768cfd7f78f3a37584f7299656', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('7a5d31ba48fe3fb1266bf186dc5f7ba7', '52b0cf022ac4187b2a70dfa4f8b2d940', '58857ff846e61794c69208e9d3a85466', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('7a6bca9276c128309c80d21e795c66c6', 'f6817f48af4fb3af11b9e8bf182f618b', '54097c6a3cf50fad0793a34beff1efdf', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('7ca833caa5eac837b7200d8b6de8b2e3', 'f6817f48af4fb3af11b9e8bf182f618b', 'fedfbf4420536cacc0218557d263dfea', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('7d2ea745950be3357747ec7750c31c57', 'ee8626f80f7c2619917b6236f3a7f02b', '2a470fc0c3954d9dbb61de6d80846549', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('7de42bdc0b8c5446b7d428c66a7abc12', '52b0cf022ac4187b2a70dfa4f8b2d940', '54dd5457a3190740005c1bfec55b1c34', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('7e19d90cec0dd87aaef351b9ff8f4902', '646c628b2b8295fbdab2d34044de0354', 'f9d3f4f27653a71c52faa9fb8070fbe7', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('7f862c47003eb20e8bad05f506371f92', 'ee8626f80f7c2619917b6236f3a7f02b', 'd7d6e2e4e2934f2c9385a623fd98c6f3', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('812ed54661b1a24b81b58974691a73f5', 'e51758fa916c881624b046d26bd09230', 'e6bfd1fcabfd7942fdd05f076d1dad38', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('83f704524b21b6a3ae324b8736c65333', 'ee8626f80f7c2619917b6236f3a7f02b', '7ac9eb9ccbde2f7a033cd4944272bf1e', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('84d32474316a43b01256d6644e6e7751', 'ee8626f80f7c2619917b6236f3a7f02b', 'ec8d607d0156e198b11853760319c646', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('85755a6c0bdff78b3860b52d35310c7f', 'e51758fa916c881624b046d26bd09230', 'c65321e57b7949b7a975313220de0422', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('8703a2410cddb713c33232ce16ec04b9', 'ee8626f80f7c2619917b6236f3a7f02b', '1367a93f2c410b169faa7abcbad2f77c', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('884f147c20e003cc80ed5b7efa598cbe', 'f6817f48af4fb3af11b9e8bf182f618b', 'e5973686ed495c379d829ea8b2881fc6', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('885c1a827383e5b2c6c4f8ca72a7b493', 'ee8626f80f7c2619917b6236f3a7f02b', '4148ec82b6acd69f470bea75fe41c357', '', NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('8a60df8d8b4c9ee5fa63f48aeee3ec00', '1750a8fb3e6d90cb7957c02de1dc8e59', 'd7d6e2e4e2934f2c9385a623fd98c6f3', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('8b09925bdc194ab7f3559cd3a7ea0507', 'f6817f48af4fb3af11b9e8bf182f618b', 'ebb9d82ea16ad864071158e0c449d186', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('8b1e326791375f325d3e6b797753b65e', 'ee8626f80f7c2619917b6236f3a7f02b', '2dbbafa22cda07fa5d169d741b81fe12', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('8ce1022dac4e558ff9694600515cf510', '1750a8fb3e6d90cb7957c02de1dc8e59', '08e6b9dc3c04489c8e1ff2ce6f105aa4', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('8d154c2382a8ae5c8d1b84bd38df2a93', 'f6817f48af4fb3af11b9e8bf182f618b', 'd86f58e7ab516d3bc6bfb1fe10585f97', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('8d848ca7feec5b7ebb3ecb32b2c8857a', '52b0cf022ac4187b2a70dfa4f8b2d940', '4148ec82b6acd69f470bea75fe41c357', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('8dd64f65a1014196078d0882f767cd85', 'f6817f48af4fb3af11b9e8bf182f618b', 'e3c13679c73a4f829bcff2aba8fd68b1', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('8e3dc1671abad4f3c83883b194d2e05a', 'f6817f48af4fb3af11b9e8bf182f618b', 'b1cb0a3fedf7ed0e4653cb5a229837ee', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('8eec2c510f1ac9c5eee26c041b1f00ca', 'ee8626f80f7c2619917b6236f3a7f02b', '58857ff846e61794c69208e9d3a85466', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('8f762ff80253f634b08cf59a77742ba4', 'ee8626f80f7c2619917b6236f3a7f02b', '9502685863ab87f0ad1134142788a385', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('903b790e6090414343502c6dc393b7c9', 'ee8626f80f7c2619917b6236f3a7f02b', 'de13e0f6328c069748de7399fcc1dbbd', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('905bf419332ebcb83863603b3ebe30f0', 'f6817f48af4fb3af11b9e8bf182f618b', '8fb8172747a78756c11916216b8b8066', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('90996d56357730e173e636b99fc48bea', 'ee8626f80f7c2619917b6236f3a7f02b', 'fb07ca05a3e13674dbf6d3245956da2e', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('90e1c607a0631364eec310f3cc4acebd', 'ee8626f80f7c2619917b6236f3a7f02b', '4f66409ef3bbd69c1d80469d6e2a885e', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('9264104cee9b10c96241d527b2d0346d', '1750a8fb3e6d90cb7957c02de1dc8e59', '54dd5457a3190740005c1bfec55b1c34', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('9380121ca9cfee4b372194630fce150e', 'f6817f48af4fb3af11b9e8bf182f618b', '65a8f489f25a345836b7f44b1181197a', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('94911fef73a590f6824105ebf9b6cab3', 'f6817f48af4fb3af11b9e8bf182f618b', '8b3bff2eee6f1939147f5c68292a1642', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('9700d20dbc1ae3cbf7de1c810b521fe6', 'f6817f48af4fb3af11b9e8bf182f618b', 'ec8d607d0156e198b11853760319c646', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('980171fda43adfe24840959b1d048d4d', 'f6817f48af4fb3af11b9e8bf182f618b', 'd7d6e2e4e2934f2c9385a623fd98c6f3', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('987c23b70873bd1d6dca52f30aafd8c2', 'f6817f48af4fb3af11b9e8bf182f618b', '00a2a0ae65cdca5e93209cdbde97cbe6', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('98f02353f91dd569e3c6b8fd6b4f4034', 'ee8626f80f7c2619917b6236f3a7f02b', '6531cf3421b1265aeeeabaab5e176e6d', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('9b2ad767f9861e64a20b097538feafd3', 'f6817f48af4fb3af11b9e8bf182f618b', '73678f9daa45ed17a3674131b03432fb', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('9d8772c310b675ae43eacdbc6c7fa04a', 'a799c3b1b12dd3ed4bd046bfaef5fe6e', '1663f3faba244d16c94552f849627d84', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('9d980ec0489040e631a9c24a6af42934', 'f6817f48af4fb3af11b9e8bf182f618b', '05b3c82ddb2536a4a5ee1a4c46b5abef', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('9f8311ecccd44e079723098cf2ffe1cc', '1750a8fb3e6d90cb7957c02de1dc8e59', '693ce69af3432bd00be13c3971a57961', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('a098e2acc3f90316f161f6648d085640', 'ee8626f80f7c2619917b6236f3a7f02b', 'e6bfd1fcabfd7942fdd05f076d1dad38', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('a307a9349ad64a2eff8ab69582fa9be4', 'f6817f48af4fb3af11b9e8bf182f618b', '0620e402857b8c5b605e1ad9f4b89350', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('a5d25fdb3c62904a8474182706ce11a0', 'f6817f48af4fb3af11b9e8bf182f618b', '418964ba087b90a84897b62474496b93', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('a66feaaf128417ad762e946abccf27ec', 'ee8626f80f7c2619917b6236f3a7f02b', 'c6cf95444d80435eb37b2f9db3971ae6', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('a72c31a3913c736d4eca11d13be99183', 'e51758fa916c881624b046d26bd09230', 'a44c30db536349e91106223957e684eb', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('a7ab87eac0f8fafa2efa4b1f9351923f', 'ee8626f80f7c2619917b6236f3a7f02b', 'fedfbf4420536cacc0218557d263dfea', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('abdc324a2df9f13ee6e73d44c6e62bc8', 'ee8626f80f7c2619917b6236f3a7f02b', 'f1cb187abf927c88b89470d08615f5ac', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('ae1852fb349d8513eb3fdc173da3ee56', 'f6817f48af4fb3af11b9e8bf182f618b', '8d4683aacaa997ab86b966b464360338', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('aefc8c22e061171806e59cd222f6b7e1', '52b0cf022ac4187b2a70dfa4f8b2d940', 'e8af452d8948ea49d37c934f5100ae6a', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('af60ac8fafd807ed6b6b354613b9ccbc', 'f6817f48af4fb3af11b9e8bf182f618b', '58857ff846e61794c69208e9d3a85466', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('b0c8a20800b8bf1ebdd7be473bceb44f', 'f6817f48af4fb3af11b9e8bf182f618b', '58b9204feaf07e47284ddb36cd2d8468', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('b128ebe78fa5abb54a3a82c6689bdca3', 'f6817f48af4fb3af11b9e8bf182f618b', 'aedbf679b5773c1f25e9f7b10111da73', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('b131ebeafcfd059f3c7e542606ea9ff5', 'ee8626f80f7c2619917b6236f3a7f02b', 'e5973686ed495c379d829ea8b2881fc6', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('b21b07951bb547b09cc85624a841aea0', 'f6817f48af4fb3af11b9e8bf182f618b', '4356a1a67b564f0988a484f5531fd4d9', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('b2b2dcfff6986d3d7f890ea62d474651', 'ee8626f80f7c2619917b6236f3a7f02b', '200006f0edf145a2b50eacca07585451', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('b495a46fa0e0d4637abe0db7fd12fe1a', 'ee8626f80f7c2619917b6236f3a7f02b', '717f6bee46f44a3897eca9abd6e2ec44', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('b64c4ab9cd9a2ea8ac1e9db5fb7cf522', 'f6817f48af4fb3af11b9e8bf182f618b', '2aeddae571695cd6380f6d6d334d6e7d', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('bbec16ad016efec9ea2def38f4d3d9dc', 'f6817f48af4fb3af11b9e8bf182f618b', '13212d3416eb690c2e1d5033166ff47a', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('bd30561f141f07827b836878137fddd8', 'e51758fa916c881624b046d26bd09230', '65a8f489f25a345836b7f44b1181197a', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('be8e5a9080569e59863f20c4c57a8e22', 'f6817f48af4fb3af11b9e8bf182f618b', '22d6a3d39a59dd7ea9a30acfa6bfb0a5', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('bea2986432079d89203da888d99b3f16', 'f6817f48af4fb3af11b9e8bf182f618b', '54dd5457a3190740005c1bfec55b1c34', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('c09373ebfc73fb5740db5ff02cba4f91', 'f6817f48af4fb3af11b9e8bf182f618b', '339329ed54cf255e1f9392e84f136901', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('c56fb1658ee5f7476380786bf5905399', 'f6817f48af4fb3af11b9e8bf182f618b', 'de13e0f6328c069748de7399fcc1dbbd', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('c689539d20a445b0896270290c58d01f', 'e51758fa916c881624b046d26bd09230', '13212d3416eb690c2e1d5033166ff47a', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('c6fee38d293b9d0596436a0cbd205070', 'f6817f48af4fb3af11b9e8bf182f618b', '4f84f9400e5e92c95f05b554724c2b58', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('c8571839e6b14796e661f3e2843b80b6', 'ee8626f80f7c2619917b6236f3a7f02b', '45c966826eeff4c99b8f8ebfe74511fc', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('c90b0b01c7ca454d2a1cb7408563e696', 'f6817f48af4fb3af11b9e8bf182f618b', '882a73768cfd7f78f3a37584f7299656', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('c9d35261cccd67ab2932107a0967a7d7', 'e51758fa916c881624b046d26bd09230', 'b4dfc7d5dd9e8d5b6dd6d4579b1aa559', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('ced80e43584ce15e97bb07298e93020d', 'e51758fa916c881624b046d26bd09230', '45c966826eeff4c99b8f8ebfe74511fc', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('cf2ef620217673e4042f695743294f01', 'f6817f48af4fb3af11b9e8bf182f618b', '717f6bee46f44a3897eca9abd6e2ec44', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('cf43895aef7fc684669483ab00ef2257', 'f6817f48af4fb3af11b9e8bf182f618b', '700b7f95165c46cc7a78bf227aa8fed3', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('d03d792b0f312e7b490afc5cec3dd6c5', 'e51758fa916c881624b046d26bd09230', '8fb8172747a78756c11916216b8b8066', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('d281a95b8f293d0fa2a136f46c4e0b10', 'f6817f48af4fb3af11b9e8bf182f618b', '5c8042bd6c601270b2bbd9b20bccc68b', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('d37ad568e26f46ed0feca227aa9c2ffa', 'f6817f48af4fb3af11b9e8bf182f618b', '9502685863ab87f0ad1134142788a385', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('d3ddcacee1acdfaa0810618b74e38ef2', 'f6817f48af4fb3af11b9e8bf182f618b', 'c6cf95444d80435eb37b2f9db3971ae6', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('d3fe195d59811531c05d31d8436f5c8b', '1750a8fb3e6d90cb7957c02de1dc8e59', 'e8af452d8948ea49d37c934f5100ae6a', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('d5267597a4450f06d49d2fb63859641a', 'e51758fa916c881624b046d26bd09230', '2dbbafa22cda07fa5d169d741b81fe12', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('d83282192a69514cfe6161b3087ff962', 'f6817f48af4fb3af11b9e8bf182f618b', '53a9230444d33de28aa11cc108fb1dba', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('d8a5c9079df12090e108e21be94b4fd7', 'f6817f48af4fb3af11b9e8bf182f618b', '078f9558cdeab239aecb2bda1a8ed0d1', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('de8f43229e351d34af3c95b1b9f0a15d', 'f6817f48af4fb3af11b9e8bf182f618b', 'a400e4f4d54f79bf5ce160ae432231af', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('e258ca8bf7ee168b93bfee739668eb15', 'ee8626f80f7c2619917b6236f3a7f02b', 'fb367426764077dcf94640c843733985', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('e339f7db7418a4fd2bd2c113f1182186', 'ee8626f80f7c2619917b6236f3a7f02b', 'b1cb0a3fedf7ed0e4653cb5a229837ee', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('e3e922673f4289b18366bb51b6200f17', '52b0cf022ac4187b2a70dfa4f8b2d940', '45c966826eeff4c99b8f8ebfe74511fc', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('e7467726ee72235baaeb47df04a35e73', 'f6817f48af4fb3af11b9e8bf182f618b', 'e08cb190ef230d5d4f03824198773950', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('eaef4486f1c9b0408580bbfa2037eb66', 'f6817f48af4fb3af11b9e8bf182f618b', '2a470fc0c3954d9dbb61de6d80846549', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('ec4bc97829ab56afd83f428b6dc37ff6', 'f6817f48af4fb3af11b9e8bf182f618b', '200006f0edf145a2b50eacca07585451', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('ec93bb06f5be4c1f19522ca78180e2ef', 'f6817f48af4fb3af11b9e8bf182f618b', '265de841c58907954b8877fb85212622', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('ecdd72fe694e6bba9c1d9fc925ee79de', 'f6817f48af4fb3af11b9e8bf182f618b', '45c966826eeff4c99b8f8ebfe74511fc', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('edefd8d468f5727db465cf1b860af474', 'f6817f48af4fb3af11b9e8bf182f618b', '6ad53fd1b220989a8b71ff482d683a5a', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('ef8bdd20d29447681ec91d3603e80c7b', 'f6817f48af4fb3af11b9e8bf182f618b', 'ae4fed059f67086fd52a73d913cf473d', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('f12b6c90e8913183d7ca547c66600891', 'e51758fa916c881624b046d26bd09230', 'aedbf679b5773c1f25e9f7b10111da73', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('f17ab8ad1e71341140857ef4914ef297', '21c5a3187763729408b40afb0d0fdfa8', '732d48f8e0abe99fe6a23d18a3171cd1', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('f99f99cc3bc27220cdd4f5aced33b7d7', 'f6817f48af4fb3af11b9e8bf182f618b', '655563cd64b75dcf52ef7bcdd4836953', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('fafe73c4448b977fe42880a6750c3ee8', 'f6817f48af4fb3af11b9e8bf182f618b', '9cb91b8851db0cf7b19d7ecc2a8193dd', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('fced905c7598973b970d42d833f73474', 'f6817f48af4fb3af11b9e8bf182f618b', '4875ebe289344e14844d8e3ea1edd73f', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('fd86f6b08eb683720ba499f9d9421726', 'ee8626f80f7c2619917b6236f3a7f02b', '693ce69af3432bd00be13c3971a57961', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('fd97963dc5f144d3aecfc7045a883427', 'f6817f48af4fb3af11b9e8bf182f618b', '043780fa095ff1b2bec4dc406d76f023', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('fed41a4671285efb266cd404f24dd378', '52b0cf022ac4187b2a70dfa4f8b2d940', '00a2a0ae65cdca5e93209cdbde97cbe6', NULL, NULL, NULL);

-- ----------------------------
-- Table structure for sys_sms
-- ----------------------------
DROP TABLE IF EXISTS `sys_sms`;
CREATE TABLE `sys_sms`  (
  `id` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'ID',
  `es_title` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '消息标题',
  `es_type` varchar(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '发送方式：1短信 2邮件 3微信',
  `es_receiver` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '接收人',
  `es_param` varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '发送所需参数Json格式',
  `es_content` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '推送内容',
  `es_send_time` datetime(0) NULL DEFAULT NULL COMMENT '推送时间',
  `es_send_status` varchar(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '推送状态 0未推送 1推送成功 2推送失败 -1失败不再发送',
  `es_send_num` int(11) NULL DEFAULT NULL COMMENT '发送次数 超过5次不再发送',
  `es_result` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '推送失败原因',
  `remark` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注',
  `create_by` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '创建人登录名称',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建日期',
  `update_by` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '更新人登录名称',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新日期',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `index_type`(`es_type`) USING BTREE,
  INDEX `index_receiver`(`es_receiver`) USING BTREE,
  INDEX `index_sendtime`(`es_send_time`) USING BTREE,
  INDEX `index_status`(`es_send_status`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;


-- ----------------------------
-- Table structure for sys_sms_template
-- ----------------------------
DROP TABLE IF EXISTS `sys_sms_template`;
CREATE TABLE `sys_sms_template`  (
  `id` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '主键',
  `template_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '模板标题',
  `template_code` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '模板CODE',
  `template_type` varchar(1) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '模板类型：1短信 2邮件 3微信',
  `template_content` varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '模板内容',
  `template_test_json` varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '模板测试json',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建日期',
  `create_by` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '创建人登录名称',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新日期',
  `update_by` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '更新人登录名称',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `uniq_templatecode`(`template_code`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of sys_sms_template
-- ----------------------------
INSERT INTO `sys_sms_template` VALUES ('1199606397416775681', '系统消息通知', 'sys_ts_note', '4', '<h1>&nbsp; &nbsp; 系统通知</h1>\n<ul>\n<li>通知时间：&nbsp; ${ts_date}</li>\n<li>通知内容：&nbsp; ${ts_content}</li>\n</ul>', NULL, '2019-11-27 16:30:27', 'admin', '2019-11-27 19:36:50', 'admin');
INSERT INTO `sys_sms_template` VALUES ('1199615897335095298', '流程催办', 'bpm_cuiban', '4', '<h1>&nbsp; &nbsp;流程催办提醒</h1>\n<ul>\n<li>流程名称：&nbsp; ${bpm_name}</li>\n<li>催办任务：&nbsp; ${bpm_task}</li>\n<li>催办时间 :&nbsp; &nbsp; ${datetime}</li>\n<li>催办内容 :&nbsp; &nbsp; ${remark}</li>\n</ul>', NULL, '2019-11-27 17:08:12', 'admin', '2019-11-27 19:36:45', 'admin');
INSERT INTO `sys_sms_template` VALUES ('1199648914107625473', '流程办理超时提醒', 'bpm_chaoshi_tip', '4', '<h1>&nbsp; &nbsp;流程办理超时提醒</h1>\n<ul>\n<li>&nbsp; &nbsp;超时提醒信息：&nbsp; &nbsp; 您有待处理的超时任务，请尽快处理！</li>\n<li>&nbsp; &nbsp;超时任务标题：&nbsp; &nbsp; ${title}</li>\n<li>&nbsp; &nbsp;超时任务节点：&nbsp; &nbsp; ${task}</li>\n<li>&nbsp; &nbsp;任务处理人：&nbsp; &nbsp; &nbsp; &nbsp;${user}</li>\n<li>&nbsp; &nbsp;任务开始时间：&nbsp; &nbsp; ${time}</li>\n</ul>', NULL, '2019-11-27 19:19:24', 'admin', '2019-11-27 19:36:37', 'admin');
INSERT INTO `sys_sms_template` VALUES ('4028608164691b000164693108140003', '催办：${taskName}', 'SYS001', '3', '${userName}，您好！\r\n请前待办任务办理事项！${taskName}\r\n\r\n\r\n===========================\r\n此消息由系统发出', '{\r\n\"taskName\":\"HR审批\",\r\n\"userName\":\"admin\"\r\n}', '2018-07-05 14:46:18', 'admin', '2018-07-05 18:31:34', 'admin');

-- ----------------------------
-- Table structure for sys_tenant
-- ----------------------------
DROP TABLE IF EXISTS `sys_tenant`;
CREATE TABLE `sys_tenant`  (
  `id` int(5) NOT NULL COMMENT '租户编码',
  `name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '租户名称',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `create_by` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '创建人',
  `begin_date` datetime(0) NULL DEFAULT NULL COMMENT '开始时间',
  `end_date` datetime(0) NULL DEFAULT NULL COMMENT '结束时间',
  `status` int(1) NULL DEFAULT NULL COMMENT '状态 1正常 0冻结',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '多租户信息表' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of sys_tenant
-- ----------------------------
INSERT INTO `sys_tenant` VALUES (1, '北京租户001', '2020-07-10 15:43:32', 'admin', NULL, NULL, 1);

-- ----------------------------
-- Table structure for sys_user
-- ----------------------------
DROP TABLE IF EXISTS `sys_user`;
CREATE TABLE `sys_user`  (
  `id` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '主键id',
  `username` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '登录账号',
  `realname` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '真实姓名',
  `password` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '密码',
  `salt` varchar(45) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'md5密码盐',
  `avatar` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '头像',
  `birthday` datetime(0) NULL DEFAULT NULL COMMENT '生日',
  `sex` tinyint(1) NULL DEFAULT NULL COMMENT '性别(0-默认未知,1-男,2-女)',
  `email` varchar(45) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '电子邮件',
  `phone` varchar(45) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '电话',
  `org_code` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '机构编码',
  `status` tinyint(1) NULL DEFAULT NULL COMMENT '性别(1-正常,2-冻结)',
  `del_flag` tinyint(1) NULL DEFAULT NULL COMMENT '删除状态(0-正常,1-已删除)',
  `third_id` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '第三方登录的唯一标识',
  `third_type` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '第三方类型',
  `activiti_sync` tinyint(1) NULL DEFAULT NULL COMMENT '同步工作流引擎(1-同步,0-不同步)',
  `work_no` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '工号，唯一键',
  `post` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '职务，关联职务表',
  `telephone` varchar(45) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '座机号',
  `create_by` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '创建人',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '更新人',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `user_identity` tinyint(1) NULL DEFAULT NULL COMMENT '身份（1普通成员 2上级）',
  `depart_ids` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '负责部门',
  `rel_tenant_ids` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '多租户标识',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `index_user_name`(`username`) USING BTREE,
  UNIQUE INDEX `uniq_sys_user_work_no`(`work_no`) USING BTREE,
  UNIQUE INDEX `uniq_sys_user_username`(`username`) USING BTREE,
  INDEX `index_user_status`(`status`) USING BTREE,
  INDEX `index_user_del_flag`(`del_flag`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '用户表' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of sys_user
-- ----------------------------
INSERT INTO `sys_user` VALUES ('1260928527571730433', '1222', 'ddd', '066185c464da20fb', 'b1nEJIUx', NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, NULL, NULL, 1, 'ddd', NULL, NULL, 'admin', '2020-05-14 21:42:43', NULL, NULL, 1, '', NULL);
INSERT INTO `sys_user` VALUES ('1301035729186459649', 'guowang', 'guowang', '59086f750c2cc5eb', 'JvtdtYYd', NULL, NULL, NULL, NULL, NULL, 'A03', 1, 0, NULL, NULL, 1, 'guowang', '', NULL, 'admin', '2020-09-02 13:54:24', 'admin', '2020-09-02 14:06:51', 1, '', '');
INSERT INTO `sys_user` VALUES ('1321620605000060929', NULL, NULL, NULL, 'M1EgX2lU', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-10-29 09:11:21', NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_user` VALUES ('249a89d3d9ff44b394cf5fb1060aa628', 'jeecg11', '11', '6b06ea67e5a6fa53', 'm55IZVpx', NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, NULL, 1, '1111', '总经理', NULL, 'jeecg', '2020-09-10 16:52:33', NULL, NULL, 1, '', '');
INSERT INTO `sys_user` VALUES ('3d464b4ea0d2491aab8a7bde74c57e95', 'zhangsan', '张三1', '02ea098224c7d0d2077c14b9a3a1ed16', 'x5xRdeKB', NULL, NULL, NULL, NULL, NULL, '财务部', 1, 0, NULL, NULL, 1, '0005', '总经理', NULL, 'admin', '2020-05-14 21:26:24', 'admin', '2020-05-14 21:39:29', 1, '', NULL);
INSERT INTO `sys_user` VALUES ('40286c146127810a01612b3cefc10005', 'sgcc_audit_admin', '审计管理员', '2e7725488bfae8f9e54b6631a83ee7fd4b11587861ab3011', 'HwiQMFfg', NULL, NULL, NULL, 'sgcc_audit_admin@isc.com', '15281085573', '1', 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-11-18 00:00:00', NULL, '2020-12-03 20:14:55', NULL, NULL, NULL);
INSERT INTO `sys_user` VALUES ('40286c146127810a01612b3cefc10006', 'sgcc_config_admin', '业务配置员', '11f775f4ab7d5efcb1590caddadf42e2f24365459b0eab3e', 'Vop6GozK', NULL, NULL, NULL, 'sgcc_config_admin@isc.com', '15281085574', '1', 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-11-18 00:00:00', NULL, '2020-12-03 17:14:39', NULL, NULL, NULL);
INSERT INTO `sys_user` VALUES ('40286c146127810a01612b3cefc10007', 'sgcc_business_user', '业务用户', 'edd074e93e283bc9c407e05237ae23c72ea67ffb9421fb85', 'XOiNQgB6', NULL, NULL, NULL, 'sgcc_business_user@isc.com', '15281085575', '1', 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-11-18 00:00:00', NULL, '2020-12-04 16:53:14', NULL, NULL, NULL);
INSERT INTO `sys_user` VALUES ('40286c146127810a01612b3cefc10099', 'sgcc_super_admin', '超级管理员', '6fe6b01d47568467eaa924dc807c2d849b767a9a066683c7', '7PyYDqig', NULL, NULL, NULL, 'sgcc_super_admin@isc.com', '15281085572', '1', 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-11-18 00:00:00', NULL, '2020-12-05 20:45:17', NULL, NULL, NULL);
INSERT INTO `sys_user` VALUES ('40286c146127810a01612b3cefc33333', 'puser', '云计算普通用户', 'f1b7a2d69d8114a7', 'zjvxEbu3', NULL, NULL, NULL, 'admin@isc.com', '15281085571', '1', 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-11-20 00:00:00', NULL, '2020-12-03 09:20:53', NULL, NULL, NULL);
INSERT INTO `sys_user` VALUES ('8a6e50b54dc19a24014dc1af740a0008', 'sjadmin', '审计管理员', '8c9b6e77abd5b73c', 'WKN3VDYN', NULL, NULL, NULL, 'ywadmin@isc.com', '18823458976', '1', 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-11-18 00:00:00', NULL, '2020-11-23 11:14:06', NULL, NULL, NULL);
INSERT INTO `sys_user` VALUES ('a75d45a015c44384a04449ee80dc3503', 'jeecg', 'jeecg', '3dd8371f3cf8240e', 'vDDkDzrK', 'user/20190220/e1fe9925bc315c60addea1b98eb1cb1349547719_1550656892940.jpg', NULL, 1, NULL, NULL, 'A02A01', 1, 0, NULL, NULL, 1, '00002', 'devleader', NULL, 'admin', '2019-02-13 16:02:36', 'admin', '2020-05-02 15:34:30', 1, '', NULL);
INSERT INTO `sys_user` VALUES ('a7ca47e7cde445bdbd5d15cbd44ac47f', 'xtadmin', '系统管理员', '327fd478b9b0a499', 'I1tWyaTk', NULL, NULL, NULL, 'xtadmin@isc.com', '18782290021', '1', 1, 0, NULL, NULL, NULL, NULL, NULL, '031125456893', NULL, '2020-11-18 00:00:00', NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_user` VALUES ('dd04d885c6aa461eb39eeba4c6bc8a7f', 'sfadmin', '身份管理员', 'fa57dcca3dda63cf', 'N9nS97Kb', NULL, NULL, NULL, 'sfadmin@isc.com', '15877779999', '1', 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-11-18 00:00:00', NULL, '2020-12-04 14:37:46', NULL, NULL, NULL);
INSERT INTO `sys_user` VALUES ('e9ca23d68d884d4ebb19d07889727dae', 'admin', '管理员', 'cb362cfeefbf3d8d', 'RCGTeGiH', 'http://minio.jeecg.com/otatest/temp/lgo33_1583397323099.png', '2018-12-05 00:00:00', 1, 'jeecg@163.com', '18611111111', 'A01', 1, 0, NULL, NULL, 1, '00001', '总经理', NULL, NULL, '2038-06-21 17:54:10', 'admin', '2020-07-10 15:27:10', 2, 'c6d7cb4deeac411cb3384b1b31278596', '');
INSERT INTO `sys_user` VALUES ('f0019fdebedb443c98dcb17d88222c38', 'zhagnxiao', '张小红', 'f898134e5e52ae11a2ffb2c3b57a4e90', 'go3jJ4zX', 'user/20190401/20180607175028Fn1Lq7zw_1554118444672.png', '2019-04-01 00:00:00', NULL, NULL, NULL, '研发部,财务部', 1, 0, NULL, NULL, 1, '00003', '', NULL, 'admin', '2023-10-01 19:34:10', 'admin', '2020-05-02 15:34:51', 1, '', NULL);

-- ----------------------------
-- Table structure for sys_user_agent
-- ----------------------------
DROP TABLE IF EXISTS `sys_user_agent`;
CREATE TABLE `sys_user_agent`  (
  `id` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '序号',
  `user_name` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '用户名',
  `agent_user_name` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '代理人用户名',
  `start_time` datetime(0) NULL DEFAULT NULL COMMENT '代理开始时间',
  `end_time` datetime(0) NULL DEFAULT NULL COMMENT '代理结束时间',
  `status` varchar(2) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '状态0无效1有效',
  `create_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '创建人名称',
  `create_by` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '创建人登录名称',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建日期',
  `update_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '更新人名称',
  `update_by` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '更新人登录名称',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新日期',
  `sys_org_code` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '所属部门',
  `sys_company_code` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '所属公司',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `uniq_username`(`user_name`) USING BTREE,
  INDEX `statux_index`(`status`) USING BTREE,
  INDEX `begintime_index`(`start_time`) USING BTREE,
  INDEX `endtime_index`(`end_time`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '用户代理人设置' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for sys_user_depart
-- ----------------------------
DROP TABLE IF EXISTS `sys_user_depart`;
CREATE TABLE `sys_user_depart`  (
  `ID` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'id',
  `user_id` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '用户id',
  `dep_id` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '部门id',
  PRIMARY KEY (`ID`) USING BTREE,
  INDEX `index_depart_groupk_userid`(`user_id`) USING BTREE,
  INDEX `index_depart_groupkorgid`(`dep_id`) USING BTREE,
  INDEX `index_depart_groupk_uidanddid`(`user_id`, `dep_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of sys_user_depart
-- ----------------------------
INSERT INTO `sys_user_depart` VALUES ('1301038863296237570', '1301035729186459649', '481ad4d97f62460f9c4a9d2bc3259020');
INSERT INTO `sys_user_depart` VALUES ('1303979666471481345', '249a89d3d9ff44b394cf5fb1060aa628', '6d35e179cd814e3299bd588ea7daed3f');
INSERT INTO `sys_user_depart` VALUES ('1260927717722931201', '3d464b4ea0d2491aab8a7bde74c57e95', 'a7d7e77e06c84325a40932163adcdaa6');
INSERT INTO `sys_user_depart` VALUES ('1256487210695356418', 'a75d45a015c44384a04449ee80dc3503', 'a7d7e77e06c84325a40932163adcdaa6');
INSERT INTO `sys_user_depart` VALUES ('1f3a0267811327b9eca86b0cc2b956f3', 'bcbe1290783a469a83ae3bd8effe15d4', '5159cde220114246b045e574adceafe9');
INSERT INTO `sys_user_depart` VALUES ('1281490128540393474', 'e9ca23d68d884d4ebb19d07889727dae', 'c6d7cb4deeac411cb3384b1b31278596');
INSERT INTO `sys_user_depart` VALUES ('1256487300096946177', 'f0019fdebedb443c98dcb17d88222c38', '57197590443c44f083d42ae24ef26a2c');
INSERT INTO `sys_user_depart` VALUES ('1256487300122112001', 'f0019fdebedb443c98dcb17d88222c38', '67fc001af12a4f9b8458005d3f19934a');

-- ----------------------------
-- Table structure for sys_user_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_user_role`;
CREATE TABLE `sys_user_role`  (
  `id` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '主键id',
  `user_id` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '用户id',
  `role_id` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '角色id',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `index2_groupuu_user_id`(`user_id`) USING BTREE,
  INDEX `index2_groupuu_ole_id`(`role_id`) USING BTREE,
  INDEX `index2_groupuu_useridandroleid`(`user_id`, `role_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '用户角色表' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of sys_user_role
-- ----------------------------
INSERT INTO `sys_user_role` VALUES ('b3ffd9311a1ca296c44e2409b547384f', '01b802058ea94b978a2c96f4807f6b48', '1');
INSERT INTO `sys_user_role` VALUES ('1301038863170408449', '1301035729186459649', '1301035310028689409');
INSERT INTO `sys_user_role` VALUES ('1303979666224017410', '249a89d3d9ff44b394cf5fb1060aa628', '1260924539346472962');
INSERT INTO `sys_user_role` VALUES ('1303979666278543361', '249a89d3d9ff44b394cf5fb1060aa628', 'e51758fa916c881624b046d26bd09230');
INSERT INTO `sys_user_role` VALUES ('1303979666354040833', '249a89d3d9ff44b394cf5fb1060aa628', 'ee8626f80f7c2619917b6236f3a7f02b');
INSERT INTO `sys_user_role` VALUES ('1303979666320486402', '249a89d3d9ff44b394cf5fb1060aa628', 'f6817f48af4fb3af11b9e8bf182f618b');
INSERT INTO `sys_user_role` VALUES ('1260927717454495745', '3d464b4ea0d2491aab8a7bde74c57e95', 'ee8626f80f7c2619917b6236f3a7f02b');
INSERT INTO `sys_user_role` VALUES ('0ede6d23d53bc7dc990346ff14faabee', '3db4cf42353f4e868b7ccfeef90505d2', 'ee8626f80f7c2619917b6236f3a7f02b');
INSERT INTO `sys_user_role` VALUES ('e78d210d24aaff48e0a736e2ddff4cdc', '3e177fede453430387a8279ced685679', 'ee8626f80f7c2619917b6236f3a7f02b');
INSERT INTO `sys_user_role` VALUES ('2c928085762756a301762756a3c00001', '40286c146127810a01612b3cefc10005', 'ff8080817608b79401762692a4510813');
INSERT INTO `sys_user_role` VALUES ('2c928085762756a301762756a3bf0000', '40286c146127810a01612b3cefc10005', 'ff8080817608b7940176269806b4081c');
INSERT INTO `sys_user_role` VALUES ('2c928085762756a3017627577b070006', '40286c146127810a01612b3cefc10006', 'ff8080817608b7940176269360550815');
INSERT INTO `sys_user_role` VALUES ('2c928085762756a3017627577b070005', '40286c146127810a01612b3cefc10006', 'ff8080817608b7940176269806b4081c');
INSERT INTO `sys_user_role` VALUES ('2c928085762756a301762757f8d5001d', '40286c146127810a01612b3cefc10007', 'ff8080817608b7940176269516ad0818');
INSERT INTO `sys_user_role` VALUES ('2c928085762756a301762757f8d5001c', '40286c146127810a01612b3cefc10007', 'ff8080817608b7940176269806b4081c');
INSERT INTO `sys_user_role` VALUES ('2c928087762742e801762742e8400000', '40286c146127810a01612b3cefc10099', 'ff8080817608b79401762695e474081a');
INSERT INTO `sys_user_role` VALUES ('2c928087762742e801762742e8400001', '40286c146127810a01612b3cefc10099', 'ff8080817608b7940176269806b4081c');
INSERT INTO `sys_user_role` VALUES ('2c93808275e335380175e33538510001', '40286c146127810a01612b3cefc33333', 'ff80808175d52fbb0175d5f90670003d');
INSERT INTO `sys_user_role` VALUES ('2c93808275e335380175e33538510000', '40286c146127810a01612b3cefc33333', 'ff80808175dee8890175e31b0c7d001b');
INSERT INTO `sys_user_role` VALUES ('f2de3ae7b5efd8345581aa802a6675d6', '41b1be8d4c52023b0798f51164ca682d', 'e51758fa916c881624b046d26bd09230');
INSERT INTO `sys_user_role` VALUES ('f2922a38ba24fb53749e45a0c459adb3', '439ae3e9bcf7418583fcd429cadb1d72', '1');
INSERT INTO `sys_user_role` VALUES ('f72c6190b0722e798147e73c776c6ac9', '439ae3e9bcf7418583fcd429cadb1d72', 'ee8626f80f7c2619917b6236f3a7f02b');
INSERT INTO `sys_user_role` VALUES ('ee45d0343ecec894b6886effc92cb0b7', '4d8fef4667574b24a9ccfedaf257810c', 'f6817f48af4fb3af11b9e8bf182f618b');
INSERT INTO `sys_user_role` VALUES ('be2639167ede09379937daca7fc3bb73', '526f300ab35e44faaed54a9fb0742845', 'ee8626f80f7c2619917b6236f3a7f02b');
INSERT INTO `sys_user_role` VALUES ('31af310584bd5795f76b1fe8c38294a0', '70f5dcf03f36471dabba81381919291f', 'e51758fa916c881624b046d26bd09230');
INSERT INTO `sys_user_role` VALUES ('8d7846ec783e157174e4ce2949231a65', '7ee6630e89d17afbf6d12150197b578d', 'e51758fa916c881624b046d26bd09230');
INSERT INTO `sys_user_role` VALUES ('2c93808275f3156b0175f317dd4a00c7', '8a6e50b54dc19a24014dc1af740a0008', '40288cd65d556b7d015d565e660d0075');
INSERT INTO `sys_user_role` VALUES ('2c93808275f3156b0175f317dd4a00c6', '8a6e50b54dc19a24014dc1af740a0008', '8a6e50b54dc268dc014dc27926170117');
INSERT INTO `sys_user_role` VALUES ('79d66ef7aa137cfa9957081a1483009d', '9a668858c4c74cf5a2b25ad9608ba095', 'ee8626f80f7c2619917b6236f3a7f02b');
INSERT INTO `sys_user_role` VALUES ('1256487210544361473', 'a75d45a015c44384a04449ee80dc3503', 'ee8626f80f7c2619917b6236f3a7f02b');
INSERT INTO `sys_user_role` VALUES ('2c93808275f3156b0175f3156bdc0000', 'a7ca47e7cde445bdbd5d15cbd44ac47f', '40288cd65d556b7d015d565e660d0075');
INSERT INTO `sys_user_role` VALUES ('2c93808275f3156b0175f3156bdc0001', 'a7ca47e7cde445bdbd5d15cbd44ac47f', '8a6e50b54dc268dc014dc277fba30114');
INSERT INTO `sys_user_role` VALUES ('2c93808275f3156b0175f3156bdc0002', 'a7ca47e7cde445bdbd5d15cbd44ac47f', 'ff80808175d52fbb0175d5f2f7120028');
INSERT INTO `sys_user_role` VALUES ('2c93808275da98460175da9846300001', 'dd04d885c6aa461eb39eeba4c6bc8a7f', '8a6e50b54dc268dc014dc2784aff0115');
INSERT INTO `sys_user_role` VALUES ('1281490128242597889', 'e9ca23d68d884d4ebb19d07889727dae', 'f6817f48af4fb3af11b9e8bf182f618b');
INSERT INTO `sys_user_role` VALUES ('1256487299962728449', 'f0019fdebedb443c98dcb17d88222c38', 'ee8626f80f7c2619917b6236f3a7f02b');

-- ----------------------------
-- Table structure for test_demo
-- ----------------------------
DROP TABLE IF EXISTS `test_demo`;
CREATE TABLE `test_demo`  (
  `id` varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '主键',
  `create_by` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '创建人登录名称',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建日期',
  `update_by` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '更新人登录名称',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新日期',
  `name` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '用户名',
  `sex` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '性别',
  `age` int(32) NULL DEFAULT NULL COMMENT '年龄',
  `descc` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '描述',
  `birthday` datetime(0) NULL DEFAULT NULL COMMENT '生日',
  `user_code` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '用户编码',
  `file_kk` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '附件',
  `top_pic` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '头像',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of test_demo
-- ----------------------------
INSERT INTO `test_demo` VALUES ('1257875943743102977', 'admin', '2020-05-06 11:32:49', NULL, NULL, '33', '1', NULL, NULL, NULL, NULL, '', '');
INSERT INTO `test_demo` VALUES ('1260475632225185794', 'admin', '2020-05-13 15:43:03', NULL, NULL, '111', NULL, NULL, NULL, NULL, NULL, '', '');
INSERT INTO `test_demo` VALUES ('1260475644145397761', 'admin', '2020-05-13 15:43:06', NULL, NULL, '222', NULL, NULL, NULL, NULL, NULL, '', '');
INSERT INTO `test_demo` VALUES ('1260475656346628097', 'admin', '2020-05-13 15:43:09', NULL, NULL, '3333', NULL, NULL, NULL, NULL, NULL, '', '');
INSERT INTO `test_demo` VALUES ('4028810c6aed99e1016aed9b31b40002', NULL, NULL, 'admin', '2019-10-19 15:37:27', 'jeecg', '2', 55, '5', '2019-05-15 00:00:00', NULL, '', '');
INSERT INTO `test_demo` VALUES ('4028810c6b02cba2016b02cba21f0000', 'admin', '2019-05-29 16:53:48', 'admin', '2019-08-23 23:45:21', '张小红', '1', 8222, '8', '2019-04-01 00:00:00', NULL, '', '');
INSERT INTO `test_demo` VALUES ('4028810c6b40244b016b4030a0e40001', 'admin', '2019-06-10 15:00:57', 'admin', '2020-05-03 01:28:34', '小芳', '2', 0, NULL, '2019-04-01 00:00:00', NULL, '', '11_1582482670686.jpg');
INSERT INTO `test_demo` VALUES ('fa1d1c249461498d90f405b94f60aae0', '', NULL, 'admin', '2019-05-15 12:30:28', '战三', '2', 222, NULL, NULL, NULL, NULL, NULL);

-- ----------------------------
-- Table structure for test_enhance_select
-- ----------------------------
DROP TABLE IF EXISTS `test_enhance_select`;
CREATE TABLE `test_enhance_select`  (
  `id` varchar(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `province` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '省份',
  `city` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '市',
  `area` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '区',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建日期',
  `create_by` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '创建人',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of test_enhance_select
-- ----------------------------
INSERT INTO `test_enhance_select` VALUES ('1256614575282958338', '1230769290609725441', '1230769470889299970', '1230769620021972993', '2020-05-03 00:00:36', 'admin');
INSERT INTO `test_enhance_select` VALUES ('402880e570801ffe01708053743c002e', '1230769253267836929', '1230769769930592257', '1230769855347593217', '2020-02-26 15:08:37', 'admin');
INSERT INTO `test_enhance_select` VALUES ('402880e570801ffe017080538b24002f', '1230769290609725441', '1230769470889299970', '1230769620021972993', '2020-02-26 15:08:43', 'admin');
INSERT INTO `test_enhance_select` VALUES ('402880e570801ffe01708053b2b10030', '1230769253267836929', '1230769347157331969', '1230769407907631106', '2020-02-26 15:08:53', 'admin');

-- ----------------------------
-- Table structure for test_note
-- ----------------------------
DROP TABLE IF EXISTS `test_note`;
CREATE TABLE `test_note`  (
  `id` varchar(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `create_by` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '创建人',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建日期',
  `update_by` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '更新人',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新日期',
  `sys_org_code` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '所属部门',
  `name` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '用户名',
  `age` int(11) NULL DEFAULT NULL COMMENT '年龄',
  `sex` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '性别',
  `birthday` datetime(0) NULL DEFAULT NULL COMMENT '生日',
  `contents` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '请假原因',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of test_note
-- ----------------------------
INSERT INTO `test_note` VALUES ('1257876639515222017', 'admin', '2020-05-06 11:35:35', NULL, NULL, 'A01', '不同意', 20, '1', '2020-05-06 00:00:00', '999');
INSERT INTO `test_note` VALUES ('1260208702503366657', 'admin', '2020-05-12 22:02:23', 'admin', '2020-07-11 11:40:24', 'A01', 'jeecg', 22233, '2', '2020-05-12 00:00:00', 'sss');

-- ----------------------------
-- Table structure for test_order_main
-- ----------------------------
DROP TABLE IF EXISTS `test_order_main`;
CREATE TABLE `test_order_main`  (
  `id` varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `create_by` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '创建人',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建日期',
  `update_by` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '更新人',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新日期',
  `order_code` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '订单编码',
  `order_date` datetime(0) NULL DEFAULT NULL COMMENT '下单时间',
  `descc` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '描述',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of test_order_main
-- ----------------------------
INSERT INTO `test_order_main` VALUES ('1256629667445714946', 'admin', '2020-05-03 01:00:34', 'admin', '2020-07-11 11:36:36', '鼠标', '2020-05-03 00:00:00', '15');
INSERT INTO `test_order_main` VALUES ('4028810c6b40244b016b40686dfb0003', 'admin', '2019-06-10 16:01:54', 'admin', '2020-02-24 02:31:59', 'B002', '2019-06-10 00:00:00', '123');
INSERT INTO `test_order_main` VALUES ('4028810c6b40244b016b4068ef890006', 'admin', '2019-06-10 16:02:27', 'admin', '2020-05-03 01:00:17', '00001', '2019-06-10 00:00:00', '购买产品BOOT');

-- ----------------------------
-- Table structure for test_order_product
-- ----------------------------
DROP TABLE IF EXISTS `test_order_product`;
CREATE TABLE `test_order_product`  (
  `id` varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '主键',
  `create_by` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '创建人',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建日期',
  `update_by` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '更新人',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新日期',
  `product_name` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '产品名字',
  `price` double(32, 0) NULL DEFAULT NULL COMMENT '价格',
  `num` int(32) NULL DEFAULT NULL COMMENT '数量',
  `descc` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '描述',
  `order_fk_id` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '订单外键ID',
  `pro_type` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '产品类型',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of test_order_product
-- ----------------------------
INSERT INTO `test_order_product` VALUES ('15665749852471', 'admin', '2020-05-03 01:00:17', NULL, NULL, '樱桃键盘', 200, 22, '', '4028810c6b40244b016b4068ef890006', '2');
INSERT INTO `test_order_product` VALUES ('15665749948861', 'admin', '2020-02-24 02:05:38', NULL, NULL, '333', 33, NULL, '', '402831816a38e7fd016a38e825c90003', '');
INSERT INTO `test_order_product` VALUES ('15884355700882143174', 'admin', '2020-05-03 01:00:17', NULL, NULL, '雷蛇鼠标', 100, NULL, '', '4028810c6b40244b016b4068ef890006', '1');
INSERT INTO `test_order_product` VALUES ('15884355711373624689', 'admin', '2020-05-03 01:00:17', NULL, NULL, 'AOC显示器', 900, NULL, '', '4028810c6b40244b016b4068ef890006', '1');
INSERT INTO `test_order_product` VALUES ('15884388229280883233', 'admin', '2020-07-11 11:36:36', NULL, NULL, '华为手机', 25, 35, '345', '1256629667445714946', '1');
INSERT INTO `test_order_product` VALUES ('15884388231401967996', 'admin', '2020-07-11 11:36:36', NULL, NULL, '低代码平台', 25, 35, '445', '1256629667445714946', '2');
INSERT INTO `test_order_product` VALUES ('15884388463052345317', 'admin', '2020-07-11 11:36:36', NULL, NULL, '表单设计器', 55, 55, '55', '1256629667445714946', '1');
INSERT INTO `test_order_product` VALUES ('402831816a38e7fd016a38e7fdeb0001', 'admin', '2019-04-20 12:01:29', NULL, NULL, '笔记本', 100, 10, NULL, '402831816a38e7fd016a38e7fddf0000', NULL);
INSERT INTO `test_order_product` VALUES ('402831816a38e7fd016a38e7fdf10002', 'admin', '2019-04-20 12:01:29', NULL, NULL, '显示器', 300, 1, NULL, '402831816a38e7fd016a38e7fddf0000', NULL);
INSERT INTO `test_order_product` VALUES ('4028810c6b40244b016b40686e050004', 'admin', '2020-02-24 02:31:59', NULL, NULL, '笔记本', 2000, 2, '123', '4028810c6b40244b016b40686dfb0003', '2');
INSERT INTO `test_order_product` VALUES ('4028810c6b40244b016b406884080005', 'admin', '2020-02-24 02:05:38', NULL, NULL, '333', NULL, 33, '', '402831816a38e7fd016a38e825c90003', '');

-- ----------------------------
-- Table structure for test_person
-- ----------------------------
DROP TABLE IF EXISTS `test_person`;
CREATE TABLE `test_person`  (
  `id` varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `create_by` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '创建人',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建日期',
  `update_by` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '更新人',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新日期',
  `sex` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '性别',
  `name` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '用户名',
  `content` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '请假原因',
  `be_date` datetime(0) NULL DEFAULT NULL COMMENT '请假时间',
  `qj_days` int(11) NULL DEFAULT NULL COMMENT '请假天数',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of test_person
-- ----------------------------
INSERT INTO `test_person` VALUES ('8ca668defdae47df8649a5477ae08b05', 'admin', '2019-04-12 09:51:37', NULL, NULL, '1', 'zhangdaiscott', 'dsdsd', '2019-04-12 00:00:00', 2);

-- ----------------------------
-- Table structure for test_shoptype_tree
-- ----------------------------
DROP TABLE IF EXISTS `test_shoptype_tree`;
CREATE TABLE `test_shoptype_tree`  (
  `id` varchar(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '主键',
  `create_by` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '创建人',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建日期',
  `update_by` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '更新人',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新日期',
  `sys_org_code` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '所属部门',
  `type_name` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '商品分类',
  `pic` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '分类图片',
  `pid` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '父级节点',
  `has_child` varchar(3) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '是否有子节点',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of test_shoptype_tree
-- ----------------------------
INSERT INTO `test_shoptype_tree` VALUES ('1256628820489908225', 'admin', '2020-05-03 00:57:12', NULL, NULL, 'A01', '衣服', NULL, '0', '0');
INSERT INTO `test_shoptype_tree` VALUES ('1256628843097206786', 'admin', '2020-05-03 00:57:17', NULL, NULL, 'A01', '电子产品', NULL, '0', '0');
INSERT INTO `test_shoptype_tree` VALUES ('1256629035921944577', 'admin', '2020-05-03 00:58:03', NULL, NULL, 'A01', '三星显示器', 'gh_f28e66390fc9_344 (shop)_1588438682583.jpg', '1256628864848867329', '0');
INSERT INTO `test_shoptype_tree` VALUES ('1256629093740425218', 'admin', '2020-05-03 00:58:17', 'admin', '2020-05-03 22:32:37', 'A01', '手机', '', '0', '1');
INSERT INTO `test_shoptype_tree` VALUES ('1256629139206680578', 'admin', '2020-05-03 00:58:28', NULL, NULL, 'A01', 'iPhone', 'e1fe9925bc315c60addea1b98eb1cb1349547719_1588438707727.jpg', '1256629093740425218', '0');
INSERT INTO `test_shoptype_tree` VALUES ('1256629188993069058', 'admin', '2020-05-03 00:58:40', 'admin', '2020-05-03 00:58:55', 'A01', '华为手机', 'jeewxshop测试号_1588438719823.jpg', '1256629093740425218', '0');

-- ----------------------------
-- Procedure structure for add_vote_memory
-- ----------------------------
DROP PROCEDURE IF EXISTS `add_vote_memory`;
delimiter ;;
CREATE DEFINER=`root`@`%` PROCEDURE `add_vote_memory`()
BEGIN
	DECLARE i INT DEFAULT 0;
	WHILE i < 3 DO
		SET i = i+1;
	end WHILE;
END
;;
delimiter ;

-- ----------------------------
-- Procedure structure for test
-- ----------------------------
DROP PROCEDURE IF EXISTS `test`;
delimiter ;;
CREATE DEFINER=`root`@`%` PROCEDURE `test`()
BEGIN
	DECLARE i INT DEFAULT 0;
	WHILE i < 3 DO
		SET i = i+1;
	end WHILE;
END
;;
delimiter ;

SET FOREIGN_KEY_CHECKS = 1;


# 对接阿里功耗平台的表
create table dcs_ali_power_nodes
(
    id                  varchar(50) not null
        primary key,
    esn                 varchar(50) not null comment '设备id',
    device_model        varchar(50) null,
    device_manufacturer varchar(50) null,
    datacenter_id       varchar(50) null,
    create_time         datetime    null,
    mgm_ip              varchar(50) null,
    ipmi_ip             varchar(50) null,
    hostname            varchar(50) null,
    power_status        int         null comment '电源状态，1为运行，0为关闭',
    power_consumption   float       null comment '功耗'
)
    comment '对接阿里功耗平台服务器表';
INSERT INTO `dcs_ali_power_nodes`VALUES ('3c14bbf6-77bc-4bd4-b2a0-4dc52c6fac73', 'sn78', '1', 'inspur', '21', '2021-05-07 15:03:58', '192.168.10.78', '100.1.2.78', 'node78', 0, 168);
INSERT INTO `dcs_ali_power_nodes` VALUES ('4b051d3b-ca13-4dc9-91a4-db7033577b05', 'sn94', '1', 'inspur', '21', '2021-05-07 15:03:58', '192.168.10.94', '100.1.2.94', 'node94', 0, 144);
INSERT INTO `dcs_ali_power_nodes` VALUES ('4f43a8df-c66a-4da0-9849-61479db49e80', 'sn77', '1', 'inspur', '21', '2021-05-07 15:03:58', '192.168.10.77', '100.1.2.77', 'node77', 0, 168);
INSERT INTO `dcs_ali_power_nodes` VALUES ('51de05c6-1dfa-41c2-8b5c-3d364b079c93', 'sn97', '1', 'inspur', '21', '2021-05-07 15:03:58', '192.168.10.97', '100.1.2.97', 'node97', 0, 168);
INSERT INTO `dcs_ali_power_nodes` VALUES ('7c199e6c-1821-4361-a9b7-972d141f4755', 'sn93', '1', 'inspur', '1', '2021-04-07 01:39:33', '192.168.10.93', '100.1.2.3', 'node93', 0, 144);
INSERT INTO `dcs_ali_power_nodes` VALUES ('dd0d1cf9-76b0-4fca-a27e-73ba876729dc', 'sn95', '1', 'inspur', '1', '2021-04-07 02:16:25', '192.168.10.95', '100.1.2.4', 'node95', 0, 144);
INSERT INTO `dcs_ali_power_nodes` VALUES ('de6e05c2-e127-4f5b-86f9-cd6b94419c14', 'sn96', '1', 'inspur', '21', '2021-05-07 15:03:58', '192.168.10.96', '100.1.2.96', 'node96', 0, 144);


# 增加云平台资源配置表
-- ----------------------------
-- Table structure for `asset_cloud_resource_config`
-- ----------------------------
DROP TABLE IF EXISTS `asset_cloud_resource_config`;
CREATE TABLE `asset_cloud_resource_config` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(36) DEFAULT NULL,
  `datacenter_id` varchar(36) DEFAULT NULL,
  `config` text,
  `cloud_resource_id` varchar(36) DEFAULT NULL,
  `cluster_id` varchar(36) DEFAULT NULL,
  `insecure_command` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4;

# 增加应用实例表
-- ----------------------------
-- Table structure for `dcs_app_instance`
-- ----------------------------
DROP TABLE IF EXISTS `dcs_app_instance`;
CREATE TABLE `dcs_app_instance` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `instance_id` varchar(36) DEFAULT NULL COMMENT '实例id',
  `name` varchar(36) DEFAULT NULL COMMENT '实例名称',
  `create_time` datetime DEFAULT NULL COMMENT '同步时间',
  `app_id` varchar(36) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=121 DEFAULT CHARSET=utf8mb4;

# 增加调度建议表

-- ----------------------------
-- Table structure for `dcs_scheduler_advice`
-- ----------------------------
DROP TABLE IF EXISTS `dcs_scheduler_advice`;
CREATE TABLE `dcs_scheduler_advice` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `app_id` varchar(36) NOT NULL COMMENT '应用ID',
  `app_name` varchar(64) NOT NULL COMMENT '应用名称',
  `cloud_resource_id` varchar(36) DEFAULT NULL COMMENT '源云平台资源ID',
  `dst_cloud_resource_id` varchar(36) DEFAULT NULL COMMENT '目标云平台资源ID',
  `dst_datacenter_id` varchar(36) NOT NULL COMMENT '目标数据中心ID',
  `src_datacenter_id` varchar(36) NOT NULL COMMENT '源数据中心ID',
  `scheduler_model_tool` int(2) DEFAULT NULL COMMENT '调度模型工具',
  `scheduler_strategy_config` int(2) DEFAULT NULL COMMENT '调度策略',
  `scheduler_model` int(2) DEFAULT NULL,
  `instance_id` varchar(36) NOT NULL COMMENT '实例ID',
  `instance_name` varchar(64) NOT NULL COMMENT '调度实例名称，虚拟机或k8s的namespace',
  `create_time` datetime DEFAULT NULL COMMENT '生成记录的时间',
  `src_node_id` varchar(32) DEFAULT NULL,
  `dst_node_id` varchar(32) DEFAULT NULL,
  `cloud_resource_type` int(2) DEFAULT NULL COMMENT '资源类型，阿里（2），华为（3），k8s（1）',
  `status` int(2) DEFAULT NULL,
  `cloud_resource_name` varchar(36) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=25271 DEFAULT CHARSET=utf8mb4;



