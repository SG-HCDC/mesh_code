package com.gcloud.mesh.dcs.dao;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.gcloud.framework.db.dao.impl.JdbcBaseDaoImpl;
import com.gcloud.mesh.dcs.entity.SchedulerImagineEntity;
import com.gcloud.mesh.header.enums.ModelType;

@Repository
public class SchedulerImagineDao extends JdbcBaseDaoImpl<SchedulerImagineEntity, String> {

	public void updateImagine(SchedulerImagineEntity imagine, double difference, double temperatureImagine, double distributionBoxVoltageImagine, double datacenterEnergyImagine, double businessRespondImagine) {
		List<String> fields = new ArrayList<>();
		fields.add("difference");
		imagine.setDifference(difference);
		fields.add("temperature_imagine");
		imagine.setTemperatureImagine(temperatureImagine);
		fields.add("distribution_box_voltage_imagine");
		imagine.setDistributionBoxVoltageImagine(distributionBoxVoltageImagine);
		fields.add("datacenter_energy_imagine");
		imagine.setDatacenterEnergyImagine(datacenterEnergyImagine);
		fields.add("business_respond_imagine");
		imagine.setBusinessRespondImagine(businessRespondImagine);
		fields.add("update_time");
		imagine.setUpdateTime(new Date());
		this.update(imagine, fields);
	}
	
	public void updateAfter(SchedulerImagineEntity imagine, String values) {
		List<String> fields = new ArrayList<>();
		fields.add("values");
		imagine.setValues(values);
		fields.add("update_time");
		imagine.setUpdateTime(new Date());
		this.update(imagine, fields);
	}

	public <E> E get(ModelType modelType, String appId, String sourceNodeId, String destNodeId, Class<E> clazz) {
		Map<String, Object> props = new HashMap<>();
		props.put("model_type", modelType.name());
		props.put("app_id", appId);
		props.put("source_node_id", sourceNodeId);
		props.put("dest_node_id", destNodeId);
		return this.findUniqueByProperties(props, clazz);
	}
}
