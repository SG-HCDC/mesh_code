package com.gcloud.mesh.dcs.strategy;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import lombok.Data;
@Component
@Data
public class NameSpaceFilterConfig {
	@Value("${mesh.scheduler.systemns:null}")
	private String systemns;
	
	public boolean contains(String namespace) {
		if (!"null".equals(systemns) && StringUtils.isNotBlank(systemns)) {
			String[] urls = systemns.split(",");
			for (String urlStr : urls) {
				if(namespace.equals(urlStr.trim())){
					return true;
				}
			}
		}
		return false;
	}
}
