package com.gcloud.mesh.dcs.common;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.springframework.context.annotation.DependsOn;
import org.springframework.stereotype.Component;

import com.gcloud.mesh.dcs.enums.DeviceThresholdType;
import com.gcloud.mesh.dcs.service.IDeviceThresholdService;
import com.gcloud.mesh.framework.core.SpringUtil;

@Component
@DependsOn("springUtil")
public class DeviceThresholdTypes {

    private static final Map<DeviceThresholdType, IDeviceThresholdService> DEVICETHRESHOLDTYPES = new HashMap<>();

    @PostConstruct
    private void init() {
        for (IDeviceThresholdService device : SpringUtil.getBeans(IDeviceThresholdService.class)) {
        	DEVICETHRESHOLDTYPES.put(device.device(), device);
        }
    }

    public IDeviceThresholdService get(String value) {
        return DEVICETHRESHOLDTYPES.get(DeviceThresholdType.getDeviceThresholdTypeByName(value));
    }

}
