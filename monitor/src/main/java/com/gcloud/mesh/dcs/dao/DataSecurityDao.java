
package com.gcloud.mesh.dcs.dao;

import org.springframework.stereotype.Repository;

import com.gcloud.framework.db.dao.impl.JdbcBaseDaoImpl;
import com.gcloud.mesh.dcs.entity.DataSecurityEntity;

@Repository
public class DataSecurityDao extends JdbcBaseDaoImpl<DataSecurityEntity, String> {

}
