package com.gcloud.mesh.dcs.service;

import com.gcloud.mesh.dcs.enums.DeviceThresholdType;

public interface IDeviceThresholdService {

	DeviceThresholdType device();

	Boolean checkScheduler(String datacenterId,String operation, Double value);

}
