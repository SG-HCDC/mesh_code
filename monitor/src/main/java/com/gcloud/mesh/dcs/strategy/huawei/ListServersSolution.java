package com.gcloud.mesh.dcs.strategy.huawei;



import com.huaweicloud.sdk.core.auth.ICredential;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;

import com.huaweicloud.sdk.core.auth.GlobalCredentials;
import com.huaweicloud.sdk.core.exception.ConnectionException;
import com.huaweicloud.sdk.core.exception.RequestTimeoutException;
import com.huaweicloud.sdk.core.exception.ServiceResponseException;
import com.huaweicloud.sdk.core.http.HttpConfig;
import com.huaweicloud.sdk.sms.v3.*;
import com.huaweicloud.sdk.sms.v3.region.SmsRegion;

import lombok.extern.slf4j.Slf4j;

import com.huaweicloud.sdk.sms.v3.model.*;

@Slf4j
@Component
public class ListServersSolution {

    public static void main(String[] args) {
    	ListServersSolution listServer = new ListServersSolution();
    	listServer.listServers(null, null);
    }
    public ListServersResponse listServers( String ak, String sk) {
    	ListServersResponse response = null;
    	
      	if(StringUtils.isBlank(ak)) {
     		 ak = "8QVISVT4XJHUNMIEPZBQ";
  	   	}
  	   	if(StringUtils.isBlank(sk)) {
  	   		sk = "GingsSlHInSkQ3x47jMSEjFT0uumSJgplZs926Cw";
  	   	}
        //String domainId = "gcloudlink";
  	    HttpConfig config = HttpConfig.getDefaultHttpConfig();
  	    config.withIgnoreSSLVerification(true);
        ICredential auth = new GlobalCredentials()
                //.withDomainId(domainId)
                .withAk(ak)
                .withSk(sk);

        SmsClient client = SmsClient.newBuilder()
        		.withHttpConfig(config)
                .withCredential(auth)
                .withRegion(SmsRegion.valueOf("ap-southeast-1"))
                .build();
        ListServersRequest request = new ListServersRequest();
        try {
            response = client.listServers(request);
           // System.out.println(response.toString());
        }  catch (ConnectionException e) {
        	e.printStackTrace();
        	log.error("迁移获取源服务器任务失败："+ e);
        } catch (RequestTimeoutException e) {
        	log.error("迁移获取源服务器任务超时："+ e);
            e.printStackTrace();
        } catch (ServiceResponseException e) {
        	log.error("迁移获取源服务器任务异常："+ e.getErrorMsg());
        }
        return response;
    }
}