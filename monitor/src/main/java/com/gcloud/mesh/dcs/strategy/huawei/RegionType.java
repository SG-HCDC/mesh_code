package com.gcloud.mesh.dcs.strategy.huawei;

public enum RegionType {
	BJ("0549615a7f00264d2fd7c003a74b8a4b","cn-north-1","华北-北京一","6175cbfe-31e5-4780-bcb1-a3cbb78f3a53"),
	BJ4("0c01b305c580f4422f29c003fb34237c","cn-north-4","华北-北京四","67532fa0-77ba-4028-89e0-e2594c918197"),
	SH1("0858319a7780f2692f34c003e1105425","cn-east-3","华东-上海一","f2e6d79f-d0ec-44bb-b0b5-136f577cde28"),
	SH2("0549615868000f6e2f2ac003d3b82d95","cn-east-2","华东-上海二-","6e8a7116-2b76-485c-95be-4ad71e716058"),
	GZ("0a8a8ceac300f5fc2fddc00317ac8464","cn-south-1","华南-广州","4765d5ca-adef-40cb-bd18-42b0d80d52ea"),
	XG("0bf21cabe800f3312f62c003316382b0","ap-southeast-1","亚太-香港","fc9dbc08-7c22-48d6-87b2-103755967305");
	private String projectId;
	private String regionName;
	private String regionId;
	private String templateId;
	RegionType(String projectId,  String regionId, String regionName, String templateId){
		this.projectId = projectId;
		this.regionName = regionName;
		this.regionId = regionId;
		this.templateId = templateId;
	}
	public String getProjectId() {
		return projectId;
	}
	public void setProjectId(String projectId) {
		this.projectId = projectId;
	}
	public String getRegionName() {
		return regionName;
	}
	public void setRegionName(String regionName) {
		this.regionName = regionName;
	}
	public String getRegionId() {
		return regionId;
	}
	public void setRegionId(String regionId) {
		this.regionId = regionId;
	}
	
	public static RegionType getRegionType(String regionId) {
		for(RegionType type :RegionType.values()) {
			if(type.getRegionId().equals(regionId)) {
				return type;
			}
		}
		return null;
	}
	public String getTemplateId() {
		return templateId;
	}
	public void setTemplateId(String templateId) {
		this.templateId = templateId;
	}
}
