package com.gcloud.mesh.dcs.enums;

import java.util.stream.Stream;

public enum AppType {
	STATELESS(0, "无状态"),
	STATE(1, "有状态");

	/* 编号 */
	private Integer type;
	/* 名字 */
	private String name;

	AppType(int type, String name) {
		this.type = type;
		this.name = name;
	}

	public Integer getType() {
		return type;
	}

	public String getName() {
		return name;
	}
	
	public static AppType getByType(Integer type) {
		return Stream.of(AppType.values()).filter(s -> s.getType() == type).findFirst().orElse(null);
	}

}
