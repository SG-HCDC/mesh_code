package com.gcloud.mesh.dcs.strategy.huawei;


import com.huaweicloud.sdk.core.auth.ICredential;

import org.springframework.stereotype.Component;

import com.gcloud.mesh.dcs.annotation.SchedulerLog;
import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.exception.ConnectionException;
import com.huaweicloud.sdk.core.exception.RequestTimeoutException;
import com.huaweicloud.sdk.core.exception.ServiceResponseException;
import com.huaweicloud.sdk.core.http.HttpConfig;
import com.huaweicloud.sdk.ecs.v2.*;
import com.huaweicloud.sdk.ecs.v2.region.EcsRegion;

import me.zhyd.oauth.log.Log;

import com.huaweicloud.sdk.ecs.v2.model.*;

@Component
public class UpdateServerSolution {

	@SchedulerLog(stepName="修改目标服务器主机名") 
    public UpdateServerResponse updateServer(String jobId, String ak, String sk, String appid, String name, String regionId) {
        ICredential auth = new BasicCredentials()
                .withAk(ak)
                .withSk(sk);
  	    HttpConfig config = HttpConfig.getDefaultHttpConfig();
  	    config.withIgnoreSSLVerification(true);
        EcsClient client = EcsClient.newBuilder()
        		.withHttpConfig(config)
                .withCredential(auth)
                .withRegion(EcsRegion.valueOf(regionId))
                .build();
        UpdateServerRequest request = new UpdateServerRequest();
        request.withServerId(appid);
        UpdateServerRequestBody body = new UpdateServerRequestBody();
        UpdateServerOption serverbody = new UpdateServerOption();
        serverbody.withName(name)
            .withHostname(name);
        body.withServer(serverbody);
        request.withBody(body);
        UpdateServerResponse response =  null;
        try {
            response = client.updateServer(request);
        } catch (ConnectionException e) {
        	Log.error("修改主机名失败",e);
        } catch (RequestTimeoutException e) {
        	Log.error("修改主机名失败",e);
        } catch (ServiceResponseException e) {
            Log.error("修改主机名失败",e);
      
        }
        return response;
    }
}