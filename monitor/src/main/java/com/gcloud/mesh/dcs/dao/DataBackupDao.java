
package com.gcloud.mesh.dcs.dao;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Repository;

import com.gcloud.framework.db.PageResult;
import com.gcloud.framework.db.dao.impl.JdbcBaseDaoImpl;
import com.gcloud.mesh.dcs.entity.DataBackupEntity;

@Repository
public class DataBackupDao extends JdbcBaseDaoImpl<DataBackupEntity, String> {

	public <E> PageResult<E> page(String name, String policy, int pageNo, int pageSize, Class<E> clazz) {
		List<Object> values = new ArrayList<>();

		StringBuffer sql = new StringBuffer();
		sql.append("select * from dcs_data_backups ");
		sql.append(" where 1 = 1");

		if (name != null) {
			sql.append(" and name like concat(?, '%') ");
			values.add(name);
		}
		
		if (policy != null) {
			sql.append(" and policy = ? ");
			values.add(policy);
		}
		
		sql.append(" order by create_time desc ");
		return this.findBySql(sql.toString(), values, pageNo, pageSize, clazz);
	}
}
