
package com.gcloud.mesh.dcs.service;


import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.jeecg.common.exception.ParamException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.gcloud.mesh.dcs.dao.AuthorityDao;
import com.gcloud.mesh.dcs.entity.AuthorityEntity;
import com.gcloud.mesh.header.enums.AuthorityResourceClassification;
import com.gcloud.mesh.header.enums.AuthorityResourceType;
import com.gcloud.mesh.header.exception.AuthorityErrorCode;
import com.gcloud.mesh.header.exception.BaseException;
import com.gcloud.mesh.header.msg.dcs.ListByClassificationMsg;
import com.gcloud.mesh.header.msg.dcs.SetAuthorityMsg;
import com.gcloud.mesh.header.msg.dcs.SetEnabledMsg;
import com.gcloud.mesh.header.vo.dcs.AuthorityItemVo;
import com.gcloud.mesh.header.vo.dcs.AuthorityVo;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class AuthorityService {
	
	@Autowired
	private AuthorityDao authorityDao;
	
    public void enabled(SetEnabledMsg msg) throws Exception {
    	AuthorityEntity authority = authorityDao.getById(msg.getType());
    	if(authority != null) {
//        	if(authority.getEnabled() == true) {
//        		throw new BaseException();
//        	}
        	List<String> updates = new ArrayList<String>();
        	authority.setEnabled(true);
        	updates.add("enabled");
        	authority.setUpdateTime(new Date());
        	updates.add("update_time");
        	try {
        		authorityDao.update(authority, updates);
        	}catch(Exception e) {
        		throw new BaseException();
        	}
    	}else {
    		throw new BaseException();
    	}
    }
	
    public void disabled(SetEnabledMsg msg) throws Exception {
    	AuthorityEntity authority = authorityDao.getById(msg.getType());
    	if(authority != null) {
//        	if(authority.getEnabled() == false) {
//        		throw new BaseException();
//        	}
        	List<String> updates = new ArrayList<String>();
        	authority.setEnabled(false);
        	updates.add("enabled");
        	authority.setUpdateTime(new Date());
        	updates.add("update_time");
        	try {
        		authorityDao.update(authority, updates);
        	}catch(Exception e) {
        		throw new BaseException();
        	}
    	}else {
    		throw new BaseException();
    	}
    }
	
    public List<AuthorityVo> listByClassification(ListByClassificationMsg msg) throws Exception {
    	if(AuthorityResourceClassification.getByName(msg.getClassification()) == null) {
    		throw new ParamException(AuthorityErrorCode.CLASSIFICATION_FORMAT_ERROR);
    	};
    	List<AuthorityVo> res = authorityDao.listByClassification(msg.getClassification(), AuthorityVo.class);
    	return res;
    }
    
    public void set(SetAuthorityMsg msg) throws Exception {
    	List<AuthorityItemVo> items = msg.getItems();
    	if(items != null) {
    		for(AuthorityItemVo item: items) {
    	    	AuthorityEntity authority = authorityDao.getById(item.getType());
    	    	if(authority != null) {
    	        	List<String> updates = new ArrayList<String>();
    	        	authority.setEnabled(item.getEnabled());
    	        	updates.add("enabled");
    	        	authority.setUpdateTime(new Date());
    	        	updates.add("update_time");
    	        	try {
    	        		authorityDao.update(authority, updates);
    	        	}catch(Exception e) {
    	        		log.error("[AuthorityService][set] 数据保存失败{}", e.getMessage());
    	        	}
    	    	}else {
    	    		throw new ParamException(AuthorityErrorCode.TYPE_FORMAT_ERROR);
    	    	}
        	}
    	}
    }
    
    public boolean checkResourceAuthority(AuthorityResourceType type) {
    	AuthorityEntity authority = authorityDao.getById(type.getName());
    	if(authority != null) {
    		return authority.getEnabled();
    	}
    	return false;
    }
}
