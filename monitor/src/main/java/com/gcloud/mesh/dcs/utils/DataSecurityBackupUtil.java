package com.gcloud.mesh.dcs.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

import com.gcloud.mesh.utils.CmdExecuteUtil;
import com.gcloud.mesh.utils.FileUtil;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class DataSecurityBackupUtil {
	
	
	public static void backupFull(boolean enable, String path, String host, String port, String user, String password) {
    	List<String> commonCmds = backupCommonCmd(host, port, user, password);
    	List<String> cmds = new ArrayList<String>();
    	cmds.add(path);
    	commonCmds.addAll(cmds);
    	String[] cmdArr = commonCmds.toArray(new String[] {});
    	String result = null;
    	try {
    		if(enable) {
//        		result = String.join(" ", cmdArr);
        		result = CmdExecuteUtil.runAndGetValue(cmdArr);
    		}
    		log.info("[DataSecurityService][backupFull] 全量备份任务执行...【{}】", String.join(" ", cmdArr));
    	}catch(Exception e) {
    		
    	}
    	
    }
    
    public static void backupIncrement(boolean enable, String incrementalDir, String baseDir, String host, String port, String user, String password) {
    	
    	List<String> commonCmds = backupCommonCmd(host, port, user, password);
    	List<String> cmds = new ArrayList<String>();
    	cmds.add("--incremental");
    	cmds.add(incrementalDir);
    	cmds.add("--incremental-basedir");
    	cmds.add(baseDir);
    	commonCmds.addAll(cmds);
    	String[] cmdArr = commonCmds.toArray(new String[] {});
    	String result = null;
    	try {
    		if(enable) {
//        		result = String.join(" ", cmdArr);
        		result = CmdExecuteUtil.runAndGetValue(cmdArr);
    		}
    		log.info("[DataSecurityService][backupIncrement] 增量备份任务结束【{}】", String.join(" ", cmdArr));
    	}catch(Exception e) {
    		
    	}
    }
    
    public static List<String> backupCommonCmd(String host, String port, String user, String password) {
    	List<String> cmds = new ArrayList<String>();
    	cmds.add("innobackupex");
    	cmds.add("--defaults-file=/etc/my.cnf");
    	cmds.add("--host");
    	cmds.add(host);
    	cmds.add("--port");
    	cmds.add(port);
    	cmds.add("--user");
    	cmds.add(user);
    	cmds.add("--password");
    	cmds.add(password);
    	return cmds;
    }
    
	public static String getBaseDir(String path) {
		String res = null;
		try {
			List<String> dirs = FileUtil.listDirs(path);
			if(dirs != null && dirs.size() > 0) {
				Collections.sort(dirs, new Comparator<String>() {
					
					SimpleDateFormat sdf = new SimpleDateFormat();
			
					@Override
					public int compare(String o1, String o2) {
						// TODO Auto-generated method stub
						int result = 0;
						try {
							Date time1 = sdf.parse(o1);
							Date time2 = sdf.parse(o2);
							return time1.after(time2) ? -1: 1;						
						} catch (ParseException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}		
						return result;
					}
					
				});
				return dirs.get(0);
			}
		}catch(Exception e) {
			log.info("[DataSecurityBackupUtil][getBaseDir] 执行异常【{}】", e.getMessage());
		}
		return res;
	}
}
