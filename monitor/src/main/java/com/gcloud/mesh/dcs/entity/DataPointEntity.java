package com.gcloud.mesh.dcs.entity;

import java.util.Date;

import com.gcloud.framework.db.jdbc.annotation.ID;
import com.gcloud.framework.db.jdbc.annotation.Table;

import lombok.Data;

@Table(name = "dcs_classification_data")
@Data
public class DataPointEntity {

	@ID
	private String id;
	private Date timestamp;
	private String database;
	private String table;
	private String level;
	private Long count;
	
}
