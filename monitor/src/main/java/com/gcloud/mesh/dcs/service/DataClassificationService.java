package com.gcloud.mesh.dcs.service;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.UUID;
import java.util.stream.Collectors;

import org.jeecg.common.exception.ParamException;
import org.jeecg.common.util.dataclassification.DataClassificationUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import com.gcloud.mesh.config.DataClassificationConfig;
import com.gcloud.mesh.dcs.dao.DataClassificationPointDao;
import com.gcloud.mesh.dcs.entity.DataClassificationPointEntity;
import com.gcloud.mesh.header.enums.DataClassificationLevel;
import com.gcloud.mesh.header.enums.PeriodType;
import com.gcloud.mesh.header.exception.BaseException;
import com.gcloud.mesh.header.exception.DataClassificationErrorCode;
import com.gcloud.mesh.header.msg.dcs.ListCountTrendMsg;
import com.gcloud.mesh.header.msg.dcs.StatisticsOverviewMsg;
import com.gcloud.mesh.header.vo.dcs.DataClassificationPointVo;
import com.gcloud.mesh.header.vo.dcs.DataItemVo;
import com.gcloud.mesh.header.vo.dcs.DataPointVo;
import com.gcloud.mesh.header.vo.dcs.DataSourceItemVo;
import com.gcloud.mesh.header.vo.dcs.ListCountTrendVo;
import com.gcloud.mesh.header.vo.dcs.StatisticTypeVo;
import com.gcloud.mesh.header.vo.dcs.StatisticsOverviewVo;
import com.gcloud.mesh.utils.TimestampUtil;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class DataClassificationService {
	
	@Autowired
	private DataClassificationConfig classificationConfig;
	
	@Autowired
	private DataClassificationPointDao dataPointDao;
	
	private Map<String, Object> lastData = new HashMap<String, Object>();

//    public long count() throws Exception {
////    	List<DataSourceItemVo> dataSources = classificationConfig.getSource();
//    	long sum = 0;
////    	if(dataSources != null) {
////        	for(DataSourceItemVo item: dataSources) {
////        		long sum = DataClassificationUtil.getStatisticsByDb(classificationConfig.getHost(), classificationConfig.getPort(), 
////        				classificationConfig.getUser(), classificationConfig.getPassword(), item.getDatabase())
////        				.stream()
////        				.filter( s -> filterTable(item.getDatabase(), s.getTable()))
////        				.mapToLong(DataItemVo::getCount)
////        				.sum();
////        		count = count + sum;
////        	}
////    	}
//    	Object generalObj = lastData.get(DataClassificationLevel.GENERAL.getName());
//    	Object vitalObj = lastData.get(DataClassificationLevel.VITAL.getName());
//    	if(generalObj != null && vitalObj != null) {
//    		sum = (Long)generalObj + (Long)vitalObj;
//    	}else {
//    		List<StatisticTypeVo> statistics = statisticType();
//    		sum = statistics.stream().mapToLong( s -> s.getCount()).sum();
//    		synchronized(this) {
//    			Map<String, Long> dataMap = statistics.stream().collect(Collectors.toMap(StatisticTypeVo::getLabel, s -> s.getCount()));
//    			lastData.put(DataClassificationLevel.GENERAL.getName(), dataMap.get(DataClassificationLevel.GENERAL.getName()));
//    			lastData.put(DataClassificationLevel.VITAL.getName(), dataMap.get(DataClassificationLevel.VITAL.getName()));
//    		}
//    	}
//    	
//    	return sum;
//    }
	
//    public List<StatisticTypeVo> statisticType() throws Exception {
//    	Map<DataClassificationLevel, Long> typeCountMap = new HashMap<DataClassificationLevel, Long>();
//    	List<DataSourceItemVo> dataSources = classificationConfig.getSource();
//    	
//    	if(dataSources != null) {
//        	for(DataSourceItemVo item: dataSources) {
//        		List<DataItemVo> dataItems = DataClassificationUtil.getStatisticsByDb(classificationConfig.getHost(), classificationConfig.getPort(), 
//        				classificationConfig.getUser(), classificationConfig.getPassword(), item.getDatabase());
//        		long generalSum = dataItems
//        				.stream()
//        				.filter( s -> filterTable(item.getDatabase(), s.getTable(), DataClassificationLevel.GENERAL))
//        				.mapToLong(DataItemVo::getCount)
//        				.sum();
//        		long vitalSum = dataItems
//        				.stream()
//        				.filter( s -> filterTable(item.getDatabase(), s.getTable(), DataClassificationLevel.VITAL))
//        				.mapToLong(DataItemVo::getCount)
//        				.sum();
//        		Long generalCount = typeCountMap.getOrDefault(DataClassificationLevel.GENERAL, Long.valueOf(0));
//        		Long vitalCount = typeCountMap.getOrDefault(DataClassificationLevel.VITAL, Long.valueOf(0));
//        		vitalCount = vitalCount + vitalSum;
//        		generalCount = generalCount + generalSum;
//        		typeCountMap.put(DataClassificationLevel.GENERAL, generalCount);
//        		typeCountMap.put(DataClassificationLevel.VITAL, vitalCount);
//        	}
//    	}
//    	synchronized(this) {
//			lastData.put(DataClassificationLevel.GENERAL.getName(), typeCountMap.get(DataClassificationLevel.GENERAL.getName()));
//			lastData.put(DataClassificationLevel.VITAL.getName(), typeCountMap.get(DataClassificationLevel.VITAL.getName()));
//		}
//    	List<StatisticTypeVo> res = typeCountMap.entrySet()
//    								.stream()
//    								.map( s -> new StatisticTypeVo(s.getKey().getName(), s.getKey().getCnName(), 
//    										s.getValue()))
//    								.collect(Collectors.toList());
//    	
//		return res;
//    }
	
//    public List<StatisticTypeVo> statisticSource(StatisticSourceMsg msg) throws Exception {
//    	
//    	if(DataClassificationLevel.getByName(msg.getLevel()) == null) {
//    		throw new BaseException(DataClassificationErrorCode.LEVEL_FORMAT_ERROR);
//    	}
//    	Map<String, Long> typeCountMap = new HashMap<String, Long>();
//    	List<DataSourceItemVo> dataSources = classificationConfig.getSource();
//    	if(dataSources != null) {
//        	for(DataSourceItemVo item: dataSources) {
//        		DataClassificationUtil.getStatisticsByDb(classificationConfig.getHost(), classificationConfig.getPort(), 
//        				classificationConfig.getUser(), classificationConfig.getPassword(), item.getDatabase())
//    				.stream()
//    				.filter( s -> filterTable(item.getDatabase(), s.getTable(),  DataClassificationLevel.getByName(msg.getLevel())))
//    				.forEach( s -> {
//    					String label = String.format("%s.%s", item.getDatabase(), s.getTable());
//    					typeCountMap.put(label, s.getCount());
//    				});
//        	}
//    	}
//    	List<StatisticTypeVo> res = typeCountMap.entrySet()
//    								.stream()
//    								.map( s -> new StatisticTypeVo(s.getKey(), null, s.getValue()))
//    								.collect(Collectors.toList());
//    	
//    	//按数量逆序
//    	Collections.sort(res, new Comparator<StatisticTypeVo>() {
//
//			@Override
//			public int compare(StatisticTypeVo o1, StatisticTypeVo o2) {
//				// TODO Auto-generated method stub
//				return o1.getCount() < o2.getCount()? 1: -1;
//			}
//    		
//    	});
//    	
//    	//其它总数计算
//    	List<StatisticTypeVo> finalRes = new ArrayList<StatisticTypeVo>();
//    	long sum = 0;
//    	for(int i=0; i<res.size(); i++) {
//    		if(i < 5) {
//    			finalRes.add(res.get(i));
//    		}else {
//    			sum = sum + res.get(i).getCount();
//    		}
//    	}
//    	StatisticTypeVo other = new StatisticTypeVo("other", null, sum);
//    	finalRes.add(other);
//    	
//		return finalRes;
//    }
    
    public StatisticsOverviewVo statisticSource(StatisticsOverviewMsg msg) throws Exception {
    	
    	if(msg.getPeriod() == null) {
    		msg.setPeriod(PeriodType.HOUR.getName());
    	}
    	
    	StatisticsOverviewVo res = new StatisticsOverviewVo();
    	Map<DataClassificationLevel, Long> typeCountMap = new HashMap<DataClassificationLevel, Long>();
    	Map<String, Long> tableCountMap = new HashMap<String, Long>();
    	List<DataSourceItemVo> dataSources = classificationConfig.getSource();    	
    	if(dataSources != null) {
        	for(DataSourceItemVo item: dataSources) {
        		List<DataItemVo> dataItems = DataClassificationUtil.getStatisticsByDb(classificationConfig.getHost(), classificationConfig.getPort(), 
        				classificationConfig.getUser(), classificationConfig.getPassword(), item.getDatabase());
        		long generalSum = dataItems
        				.stream()
        				.filter( s -> filterTable(item.getDatabase(), s.getTable(), DataClassificationLevel.GENERAL))
        				.mapToLong(DataItemVo::getCount)
        				.sum();
        		long vitalSum = dataItems
        				.stream()
        				.filter( s -> filterTable(item.getDatabase(), s.getTable(), DataClassificationLevel.VITAL))
        				.mapToLong(DataItemVo::getCount)
        				.sum();
        		dataItems.stream()
				.filter( s -> filterTable(item.getDatabase(), s.getTable(),  DataClassificationLevel.getByName(msg.getLevel())))
				.forEach( s -> {
					String label = String.format("%s.%s", item.getDatabase(), s.getTable());
					tableCountMap.put(label, s.getCount());
				});
        		Long generalCount = typeCountMap.getOrDefault(DataClassificationLevel.GENERAL, Long.valueOf(0));
        		Long vitalCount = typeCountMap.getOrDefault(DataClassificationLevel.VITAL, Long.valueOf(0));
        		vitalCount = vitalCount + vitalSum;
        		generalCount = generalCount + generalSum;
        		typeCountMap.put(DataClassificationLevel.GENERAL, generalCount);
        		typeCountMap.put(DataClassificationLevel.VITAL, vitalCount);
        	}
    	}
    	res.setCount(typeCountMap.get(DataClassificationLevel.GENERAL) + typeCountMap.get(DataClassificationLevel.VITAL));
    	List<StatisticTypeVo> statisticType = typeCountMap.entrySet()
				.stream()
				.map( s -> new StatisticTypeVo(s.getKey().getName(), s.getKey().getCnName(), 
						s.getValue()))
				.collect(Collectors.toList());
    	res.setStatisticType(statisticType);
    	
//    	List<StatisticTypeVo> statisticSource = tableCountMap.entrySet()
//				.stream()
//				.map( s -> new StatisticTypeVo(s.getKey(), null, s.getValue()))
//				.collect(Collectors.toList());
//    	sortDescByCount(statisticSource);
//    	List<StatisticTypeVo> finalStatisticSource = countOther(statisticSource);
//    	res.setStatisticSource(finalStatisticSource);
    	
    	PeriodType period = PeriodType.getByName(msg.getPeriod());
    	ListCountTrendVo listCountTrend = listCountTrend(period);
    	updateLastData(listCountTrend, period, new Date(), typeCountMap.get(DataClassificationLevel.GENERAL), typeCountMap.get(DataClassificationLevel.VITAL));
    	res.setListCountTrend(listCountTrend);
    	
    	return res;
    }
	
    public ListCountTrendVo listCountTrend(ListCountTrendMsg msg) throws Exception {
    	ListCountTrendVo res = new ListCountTrendVo();
    	PeriodType period = PeriodType.getByName(msg.getPeriod());
		if(period == null) {
			throw new ParamException(DataClassificationErrorCode.PERIOD_FORMAT_ERROR);
		}
		
		List<StatisticTypeVo> generals = listByLevel(period, DataClassificationLevel.GENERAL);
		res.setGenerals(generals);
		List<StatisticTypeVo> vitals = listByLevel(period, DataClassificationLevel.VITAL);
		res.setVitals(vitals);
		return res;
    }
    
    //每60分钟执行一次，定时更新数据库
//    @Scheduled(initialDelay = 0, fixedDelay = 5 * 60 * 1000)
//    private void updateCount() {
//    	List<DataSourceItemVo> dataSources = classificationConfig.getSource();
//    	if(dataSources != null) {
//        	for(DataSourceItemVo item: dataSources) {
//        		List<DataItemVo> data = DataClassificationUtil.getStatisticsByDb(classificationConfig.getHost(), classificationConfig.getPort(), 
//        				classificationConfig.getUser(), classificationConfig.getPassword(), item.getDatabase());  		
//        		data.stream()
//    				.filter( s -> filterTable(item.getDatabase(), s.getTable(), DataClassificationLevel.GENERAL))
//    				.forEach( s -> {
//    					savePoint(item.getDatabase(), s.getTable(), DataClassificationLevel.GENERAL.getName(), s.getCount());
//    				});
//        		data.stream()
//    				.filter( s -> filterTable(item.getDatabase(), s.getTable(), DataClassificationLevel.VITAL))
//    				.forEach( s -> {
//    					savePoint(item.getDatabase(), s.getTable(), DataClassificationLevel.VITAL.getName(), s.getCount());
//    				});
//        	}
//    	}
//    }
    
    //每60分钟执行一次，定时更新数据库
    @Scheduled(initialDelay = 0, fixedDelay = 5 * 60 * 1000)
    private void updateLevelCount() {
    	List<DataSourceItemVo> dataSources = classificationConfig.getSource();
    	Map<DataClassificationLevel, Long> typeCountMap = new HashMap<DataClassificationLevel, Long>();
    	if(dataSources != null) {
        	for(DataSourceItemVo item: dataSources) {		
        		List<DataItemVo> dataItems = DataClassificationUtil.getStatisticsByDb(classificationConfig.getHost(), classificationConfig.getPort(), 
        				classificationConfig.getUser(), classificationConfig.getPassword(), item.getDatabase());
        		long generalSum = dataItems
        				.stream()
        				.filter( s -> filterTable(item.getDatabase(), s.getTable(), DataClassificationLevel.GENERAL))
        				.mapToLong(DataItemVo::getCount)
        				.sum();
        		long vitalSum = dataItems
        				.stream()
        				.filter( s -> filterTable(item.getDatabase(), s.getTable(), DataClassificationLevel.VITAL))
        				.mapToLong(DataItemVo::getCount)
        				.sum();
        		Long generalCount = typeCountMap.getOrDefault(DataClassificationLevel.GENERAL, Long.valueOf(0));
        		Long vitalCount = typeCountMap.getOrDefault(DataClassificationLevel.VITAL, Long.valueOf(0));
        		vitalCount = vitalCount + vitalSum;
        		generalCount = generalCount + generalSum;
        		typeCountMap.put(DataClassificationLevel.GENERAL, generalCount);
        		typeCountMap.put(DataClassificationLevel.VITAL, vitalCount);
        	}
    	}
    	savePoint(typeCountMap.get(DataClassificationLevel.GENERAL), typeCountMap.get(DataClassificationLevel.VITAL));
    }
    
//    private void savePoint(String database, String table, String level, Long count) {
//    	DataPointEntity point = new DataPointEntity();
//    	point.setId(UUID.randomUUID().toString());
//    	point.setTimestamp(new Date());
//    	point.setDatabase(database);
//    	point.setTable(table);
//    	point.setLevel(level);
//    	point.setCount(count);
//    	try {
//			dataPointDao.save(point);
//		} catch (Exception e) {
//			// TODO Auto-generated catch block
//			log.error("[DataClassificationService][savePoint] 数据保存失败：{}", e.getMessage());
//		}
//    }
    
    private void savePoint(Long generalCount, Long vitalCount) {
    	DataClassificationPointEntity point = new DataClassificationPointEntity();
    	point.setId(UUID.randomUUID().toString());
    	point.setTimestamp(new Date());
    	point.setGeneralCount(generalCount);
    	point.setVitalCount(vitalCount);
    	try {
			dataPointDao.save(point);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			log.error("[DataClassificationService][savePoint] 数据保存失败：{}", e.getMessage());
		}
    }
    
    private boolean filterTable(String database, String table, DataClassificationLevel level) {
    	List<DataSourceItemVo> dataSources = classificationConfig.getSource();
    	
    	if(dataSources != null) {

        	for(DataSourceItemVo item: dataSources) { 		
        		if(item.getDatabase().equals(database)) {
        			List<String> tables = item.getTables(); 
            		List<String> excludes = item.getExcludes();
            		//重要数据
            		if(level != null && DataClassificationLevel.VITAL.equals(level)) {
            			// 若重要数据表为null, 则直接过滤掉
            			if(tables == null || tables.size() == 0) {
            				return false;
            				
            			}else if(tables != null && tables.contains(table) && !excludes.contains(table)) {
            				return true;
            			}
            			return false;
            		}
            		//一般业务数据
            		if(level != null && DataClassificationLevel.GENERAL.equals(level)) {
            			if(!tables.contains(table) && !excludes.contains(table)) {
            				return true;
            			}else {
            				return false;
            			}
            		}
            		//排除数据
        			if(excludes != null && !excludes.contains(table)) {
        				return true;
        			}
        		}
        	}
    	}
    	return false;
    }
    
    private List<StatisticTypeVo> listByLevel(PeriodType period, DataClassificationLevel level) {
    	List<StatisticTypeVo> res = new ArrayList<StatisticTypeVo>();
		List<DataPointVo> generalRes = dataPointDao.listByPeriod(period, DataPointVo.class);
		Map<String, List<DataPointVo>> labelGroupMap = generalRes.stream()
				.collect(Collectors.groupingBy(DataPointVo::getLabel));
		for(Entry<String, List<DataPointVo>> entry: labelGroupMap.entrySet()) {
			String label = entry.getKey();
			Map<String, List<DataPointVo>> sourceGroupMap = entry.getValue().stream().collect(Collectors.groupingBy(DataPointVo::getSource));
			List<DataPointVo> generals = sourceGroupMap.entrySet().stream()
												.filter( s -> s.getValue() != null && s.getValue().size() > 0)
//												.sorted()
												.map( s -> s.getValue().get(0))
												.collect(Collectors.toList());
			long sum = generals.stream().mapToLong(DataPointVo::getCount).sum();
			StatisticTypeVo vo = new StatisticTypeVo();
			vo.setLabel(label);
			vo.setCount(sum);
			res.add(vo);
		}
		Collections.sort(res, new Comparator<StatisticTypeVo>() {

			@Override
			public int compare(StatisticTypeVo o1, StatisticTypeVo o2) {
				// TODO Auto-generated method stub
				return o1.getLabel().compareTo(o2.getLabel());
			}
			
		});
		if(PeriodType.HOUR.equals(period)) {
			for(StatisticTypeVo vo: res) {
				vo.setLabel(convertHourLable(vo.getLabel()));
			}
		}
		
		return res;
    	
    }
    
    private String convertHourLable(String label) {
    	label = label + ":00:00";
    	return label;
    }
    
    private  void sortDescByCount(List<StatisticTypeVo> res) {
    	//按数量逆序
    	Collections.sort(res, new Comparator<StatisticTypeVo>() {

			@Override
			public int compare(StatisticTypeVo o1, StatisticTypeVo o2) {
				// TODO Auto-generated method stub
				return o1.getCount() < o2.getCount()? 1: -1;
			}
    		
    	});
    }
    
    private List<StatisticTypeVo> countOther(List<StatisticTypeVo> res) {
    	//其它总数计算
    	List<StatisticTypeVo> finalRes = new ArrayList<StatisticTypeVo>();
    	long sum = 0;
    	for(int i=0; i<res.size(); i++) {
    		if(i < 5) {
    			finalRes.add(res.get(i));
    		}else {
    			sum = sum + res.get(i).getCount();
    		}
    	}
    	StatisticTypeVo other = new StatisticTypeVo("other", null, sum);
    	finalRes.add(other);
    	return finalRes;
    }
    
    private ListCountTrendVo listCountTrend(PeriodType period) throws Exception {
    	ListCountTrendVo res = new ListCountTrendVo();   	
		if(period == null) {
			throw new ParamException(DataClassificationErrorCode.PERIOD_FORMAT_ERROR);
		}	
		List<DataClassificationPointVo> points = dataPointDao.listByPeriod(period, DataClassificationPointVo.class);
		List<StatisticTypeVo> generals = new ArrayList<StatisticTypeVo>();
		List<StatisticTypeVo> vitals = new ArrayList<StatisticTypeVo>();
		
		if(period.getName().equals("hour")){
			// 假数据 - huanglz
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			DataClassificationPointVo endPoint = points.get(points.size() - 1);
			Date endTime = sdf.parse(endPoint.getLabel());
			long labelTime = endTime.getTime() - 1000 * 60 * 5 * (points.size() + 1);
			for(int i = 0; i < points.size(); i++) {
				
				labelTime = labelTime + 1000 * 60 * 5;
				String label = sdf.format(new Date(labelTime));
				StatisticTypeVo generalVo = new StatisticTypeVo();
				generalVo.setLabel(label);
				generalVo.setCount(points.get(i).getGeneralCount());
				generals.add(generalVo);
				StatisticTypeVo vitalVo = new StatisticTypeVo();
				vitalVo.setLabel(label);
				vitalVo.setCount(points.get(i).getVitalCount());
				vitals.add(vitalVo);
			}
		}else {
			for(DataClassificationPointVo point: points) {
				StatisticTypeVo generalVo = new StatisticTypeVo();
				generalVo.setLabel(point.getLabel());
				generalVo.setCount(point.getGeneralCount());
				generals.add(generalVo);
				StatisticTypeVo vitalVo = new StatisticTypeVo();
				vitalVo.setLabel(point.getLabel());
				vitalVo.setCount(point.getVitalCount());
				vitals.add(vitalVo);
			}
		}
		
		res.setGenerals(generals);
		res.setVitals(vitals);
		return res;
    }
  
    private void updateLastData(ListCountTrendVo res, PeriodType period, Date timestamp, Long generalCount, Long vitalCount) throws Exception {
    	String label = null;
		if(PeriodType.HOUR.equals(period)) {
			label = TimestampUtil.dateToStr(timestamp, "yyyy-MM-dd HH:mm:ss");
		}
		if(PeriodType.DAY.equals(period)) {
			label = TimestampUtil.dateToStr(timestamp, "yyyy-MM-dd");
		}
	    if(PeriodType.MONTH.equals(period)) {
			label = TimestampUtil.dateToStr(timestamp, "yyyy-MM");
		}
	    if(label != null) {
	    	updateLastData(res, period, label, generalCount, vitalCount);	
	    }
    	
    }
    
    private void updateLastData(ListCountTrendVo res, PeriodType period, String label, Long generalCount, Long vitalCount) throws Exception {
    	List<StatisticTypeVo> generals = res.getGenerals();
    	List<StatisticTypeVo> vitals = res.getVitals();
    	if(generals != null && vitals != null) {
    		StatisticTypeVo generalVo = null;
    		StatisticTypeVo vitalVo = null;
//    		String label = null;
    		if(PeriodType.HOUR.equals(period)) {
    			generalVo = new StatisticTypeVo();
        		vitalVo = new StatisticTypeVo();
//    			label = TimestampUtil.dateToStr(timestamp, "HH:mm:ss");
    			generalVo.setLabel(label);
    			generalVo.setCount(generalCount);
        		vitalVo.setCount(vitalCount);
        		vitalVo.setLabel(label);
        		generals.add(generalVo);
        		vitals.add(vitalVo);
    		}
    		if(PeriodType.DAY.equals(period) || PeriodType.MONTH.equals(period)) {
//    			label = TimestampUtil.dateToStr(timestamp, "yyyy-MM-dd");
    			generalVo = generals.stream().filter(s -> s.getLabel().equals(label)).findFirst().orElse(null);
    			vitalVo = vitals.stream().filter(s -> s.getLabel().equals(label)).findFirst().orElse(null);
    			if(generalVo != null && vitalVo != null) {
    				generalVo.setCount(generalCount);
            		vitalVo.setCount(vitalCount);
    			}else {
    				generalVo = new StatisticTypeVo();
            		vitalVo = new StatisticTypeVo();
            		generalVo.setLabel(label);
        			generalVo.setCount(generalCount);
            		vitalVo.setCount(vitalCount);
            		vitalVo.setLabel(label);
            		generals.add(generalVo);
            		vitals.add(vitalVo);
    			}
    		}
    		
    	}
    }
    
    
    
}
