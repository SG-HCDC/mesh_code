package com.gcloud.mesh.dcs.task;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.jeecg.common.util.RedisUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;

import com.gcloud.mesh.asset.dao.CloudResourceDao;
import com.gcloud.mesh.asset.entity.CloudResourceEntity;
import com.gcloud.mesh.asset.enums.CloudResourceType;
import com.gcloud.mesh.dcs.dao.AppDao;
import com.gcloud.mesh.dcs.entity.AppEntity;
import com.gcloud.mesh.dcs.service.AppProfileService;
import com.gcloud.mesh.framework.core.SpringUtil;
import com.gcloud.mesh.framework.core.util.DateUtil;
import com.gcloud.mesh.header.enums.ResourceSourceType;
import com.gcloud.mesh.header.vo.supplier.SupplierVo;
import com.gcloud.mesh.sdk.GceSDK;
import com.gcloud.mesh.sdk.gce.ClusterVo;
import com.gcloud.mesh.sdk.gce.GcePageReply;
import com.gcloud.mesh.sdk.gce.NamespaceVo;
import com.gcloud.mesh.supplier.service.SupplierService;

import lombok.extern.slf4j.Slf4j;

@Slf4j
//@Component
public class SyncAppTask {

//	@Autowired
//	private GceSDK gceSdk;

	@Autowired
	private AppDao appDao;

	@Autowired
	private SupplierService supplierService;

	@Autowired
	private AppProfileService appProfileService;

	@Autowired
	private CloudResourceDao cloudResourceDao;
	@Autowired
	private RedisUtil redisUtil;


	@Scheduled(fixedDelay = 1000 * 60)
	public void work() {
		log.debug("[App] 同步应用定时器开始");

		GceSDK gceSdk = SpringUtil.getBean(GceSDK.class);

		GcePageReply<ClusterVo> cluster = gceSdk.pageCluster(1, 9999);
		if (cluster != null && cluster.getData() != null) {
			log.debug("[App] 同步应用定时器，集群数量{}", cluster.getData().size());
			for (ClusterVo c : cluster.getData()) {

				// TODO k8s集群查找对应的数据中心ID
				SupplierVo supplier = supplierService.getByClusterId(c.getId());
				if (supplier == null ) {
					continue;
				}

				//TODO 这里有问题的 一对多关系 后面弃用
				CloudResourceEntity cloudResourceEntity = cloudResourceDao.findOneByProperty("datacenterId", supplier.getDatacenterId(), CloudResourceEntity.class);
				if(cloudResourceEntity==null) continue;
				Map<String, AppEntity> appsDBMap = new HashMap<>();

				List<AppEntity> appsDB = appDao.findByProperty("cloudResourceId", cloudResourceEntity.getId());
				if (appsDB != null && !appsDB.isEmpty()) {
					for (AppEntity app : appsDB) {
						appsDBMap.put(app.getId(), app);
					}
				}

				GcePageReply<NamespaceVo> namespace = gceSdk.pageNamespace(c.getId(), 1, 9999);
				Map<String, AppEntity> appsMap = new HashMap<>();

				if (namespace != null && namespace.getData() != null) {
					for (NamespaceVo n : namespace.getData()) {
						// TODO mo
						boolean result = appDao.isSyncProcess(n.getId());
						if(result) continue;

						AppEntity app = appDao.getById(n.getId());
						if (app == null) {
							app = new AppEntity();
							app.setName(n.getName());
							app.setId(n.getId());
							app.setFrom(ResourceSourceType.SYNC.getName());
							app.setType(CloudResourceType.getByType(cloudResourceEntity.getType()).getType());
							app.setCloudResourceId(cloudResourceEntity.getId());
							//app.setZoneId(c.getId());
							app.setCreateTime(DateUtil.stringToDate(n.getCreateAt()));
							//	app.setType("namespace");
							//app.setDatacenterId(supplier.getDatacenterId());
							appDao.save(app);
							log.debug("[App] 同步应用定时器，新增成功{}", app.getId());

							// 初始化应用画像建议
							appProfileService.initAppProfile(n.getId());
						} else {
							app.setName(n.getName());
							appDao.update(app);
							log.debug("[App] 同步应用定时器，修改成功{}", app.getId());
						}
						appsMap.put(app.getId(), app);
					}
				}

				for (String appId : appsDBMap.keySet()) {
					boolean result = appDao.isSyncProcess(appId);
					if(result) continue;
					// 删除后台不存在的应用
					if (appsMap.get(appId) == null) {
						appDao.deleteById(appId);
					}
				}
			}
		} else {
			log.debug("[App] 同步应用定时器，集群数量0");
		}
		log.debug("[App] 同步应用定时器结束");
	}

}
