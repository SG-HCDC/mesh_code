package com.gcloud.mesh.dcs.dao;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.springframework.stereotype.Repository;

import com.gcloud.framework.db.dao.impl.JdbcBaseDaoImpl;
import com.gcloud.mesh.dcs.entity.DataCleanConfigEntity;
import com.gcloud.mesh.header.vo.dcs.DbSizeVo;

@Repository
public class DataCleanDao extends JdbcBaseDaoImpl<DataCleanConfigEntity, String> {

//	public void cleanAlertHistory(Date beforeDate) {
//
//		StringBuilder sb = new StringBuilder();
//		sb.append("DELETE FROM alert_history WHERE create_time < '" + this.convertDateToString(beforeDate) + "' ");
//
//		this.jdbcTemplate.execute(sb.toString());
//	}
//
//	private String convertDateToString(Date date) {
//		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//		return dateFormat.format(date);
//	}
//
	public String dbSize() {
		StringBuilder sb = new StringBuilder();
		sb.append(
				"select truncate(sum(data_length)/1024/1024,2) as data_size from information_schema.tables where TABLE_SCHEMA = 'mesh_controller'");
		List<DbSizeVo> res = this.findBySql(sb.toString(), DbSizeVo.class);
		return res.get(0).getDataSize() + "";
	}
}
