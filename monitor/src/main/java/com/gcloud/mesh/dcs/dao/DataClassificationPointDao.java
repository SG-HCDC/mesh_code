
package com.gcloud.mesh.dcs.dao;

import java.util.Date;
import java.util.List;

import org.springframework.stereotype.Repository;

import com.gcloud.framework.db.dao.impl.JdbcBaseDaoImpl;
import com.gcloud.mesh.dcs.entity.DataClassificationPointEntity;
import com.gcloud.mesh.dcs.entity.DataPointEntity;
import com.gcloud.mesh.header.enums.PeriodType;
import com.gcloud.mesh.utils.TimestampUtil;

@Repository
public class DataClassificationPointDao extends JdbcBaseDaoImpl<DataClassificationPointEntity, String> {

//	public DataPointEntity getLastPointByCondition(String database, String table) {
//		StringBuffer sql = new StringBuffer();
//		sql.append(" select * from dcs_classification_data ");
//		sql.append(" where 1 = 1");
//		
//        if(database != null) {
//        	sql.append(" and database = '" + database + "'");
//        }
//        if(database != null) {
//        	sql.append(" and table = '" + table + "'");
//        }
//        sql.append(" order by timestamp desc ");
//        
//        return this.findBySql(sql.toString()) != null ? this.findBySql(sql.toString()).get(0) : null;
//	}
	
//	public <E> List<E> listByPeriod(PeriodType period, Class<E> clazz) {
//		StringBuffer sql = new StringBuffer();
//		String template = "select date_format(timestamp, '%s') label, count(id) count from `dcs_classification_data` d group by label ";
//        if(PeriodType.DAY.equals(period)) {
////        	sql.append(" select date_format(create_time,'%Y%m%d') days,count(id) count from `dcs_subhealth_alerts` group by days ");
//        	sql.append(String.format(template, "%Y-%m-%d"));
//        }
//        if(PeriodType.MONTH.equals(period)) {
////        	sql.append(" select date_format(create_time,'%Y%m%d') months,count(id) count from `dcs_subhealth_alerts` group by days ");
//        	sql.append(String.format(template, "%Y-%m"));
//        }
//        if(PeriodType.HOUR.equals(period)) {
//        	sql.append(String.format(template, "%Y-%m-%dT%H"));
//        }
//        return this.findBySql(sql.toString(), clazz);
//	}
	
//	public <E> List<E> listByPeriod(PeriodType period, List<String> filters, Class<E> clazz) {
//		StringBuffer sql = new StringBuffer();
//		StringBuffer filterTemplate = new StringBuffer();
//		String template = "select cte.label, , cte.label_table, cte.count from ( "
//				+ "select d.*, date_format(timestamp, '%s') label, concat(d.database, '.', d.table) label_table from `dcs_classification_data` d "		
//				+ "order by timestamp desc "
//				+ ") cte cte.label where like = %s";
//		if(filters != null) {
//			for(String filter: filters) {
//				filterTemplate.append(",'" + filter + "'");
//			}
//			template = template + " and cte.label_table in (" + filterTemplate.toString().substring(1) + ")";
//		}
//		
//		if(PeriodType.DAY.equals(period)) {
//        	sql.append(String.format(template, "%Y-%m-%d", filterDate(period)));
//        }
//        if(PeriodType.MONTH.equals(period)) {
//        	sql.append(String.format(template, "%Y-%m", filterDate(period)));
//        }
//        if(PeriodType.HOUR.equals(period)) {
//        	sql.append(String.format(template, "%Y-%m-%dT%H", filterDate(period)));
//        }
//        return this.findBySql(sql.toString(), clazz);
//	}
	
	
	public <E> List<E> listByPeriod(PeriodType period, String level, Class<E> clazz) {
		StringBuffer sql = new StringBuffer();
		String template = "select cte.label, cte.source, cte.count, cte.timestamp, cte.level from ( "
				+ "select d.*, date_format(timestamp, '%s') label, concat(d.database, '.', d.table) source from `dcs_classification_data` d "		
				+ "order by timestamp desc "
				+ ") cte  where cte.label like '%s%%' and cte.level = '%s'";

		if(PeriodType.DAY.equals(period)) {
	    	sql.append(String.format(template, "%Y-%m-%d", filterDate(period), level));
	    }
	    if(PeriodType.MONTH.equals(period)) {
	    	sql.append(String.format(template, "%Y-%m", filterDate(period), level));
	    }
	    if(PeriodType.HOUR.equals(period)) {
	    	sql.append(String.format(template, "%Y-%m-%dT%H", filterDate(period), level));
	    }
	    return this.findBySql(sql.toString(), clazz);
	}
	
	public <E> List<E> listByPeriod(PeriodType period, Class<E> clazz) {
		StringBuffer sql = new StringBuffer();
		String template = "select cte.label, cte.general_count, cte.vital_count, cte.timestamp, max(cte.timestamp) max from "
				+ "( select d.*, date_format(timestamp, '%s') label from `dcs_classification_data` d order by d.`timestamp` desc limit 10000000000 ) cte "
				+ " where cte.label like '%s%%' "
				+ " group by cte.label";

		if(PeriodType.DAY.equals(period)) {
	    	sql.append(String.format(template, "%Y-%m-%d", filterDate(period)));
	    }
	    if(PeriodType.MONTH.equals(period)) {
	    	sql.append(String.format(template, "%Y-%m", filterDate(period)));
	    }
	    if(PeriodType.HOUR.equals(period)) {
	    	sql.append(String.format(template, "%Y-%m-%d %H:%i:%S", filterDate(period)));
	    }
	    return this.findBySql(sql.toString(), clazz);
	}
	
	private String filterDate(PeriodType period) {
		Date date = new Date();
		if(PeriodType.DAY.equals(period)) {
			return TimestampUtil.formatDate("yyyy-MM", date);
        }
        if(PeriodType.MONTH.equals(period)) {
        	return TimestampUtil.formatDate("yyyy", date);
        }
        if(PeriodType.HOUR.equals(period)) {
        	return TimestampUtil.formatDate("yyyy-MM-dd", date);
        }
        return null;
		
	}
	
}
