package com.gcloud.mesh.dcs.enums;

public enum SchedulerStrategyConfig {

	ALL(0, "全量迁移"),
	TEMPLATE(1, "模版迁移");

	private Integer value;
	private String name;

	SchedulerStrategyConfig(Integer value, String name) {
		this.value = value;
		this.name = name;
	}

	public static SchedulerStrategyConfig getByType(Integer value) {
		if (value != null) {
			for (SchedulerStrategyConfig driver : SchedulerStrategyConfig.values()) {
				if (driver.getValue().equals(value)) {
					return driver;
				}
			}
		}
		return null;
	}

	public Integer getValue() {
		return value;
	}

	public String getName() {
		return name;
	}

}
