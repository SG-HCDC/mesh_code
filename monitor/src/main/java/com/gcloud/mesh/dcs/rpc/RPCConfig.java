package com.gcloud.mesh.dcs.rpc;

import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

@Configuration
@Slf4j
public class RPCConfig {
    @Value("${mesh.rpc.controller.port}")
    private int controllerPort;

    @Value("${mesh.rpc.server.port}")
    private int serverPort;

    private boolean enable = false;

    public ManagedChannel getManagedChannel(String ip) {
        if (!enable) {
            log.info("[RPCConfig]rpc已关闭");
            return null;
        }
        return ManagedChannelBuilder.forAddress(ip, serverPort).usePlaintext().build();
    }

    public int getControllerPort() {
        return controllerPort;
    }

    public int getServerPort() {
        return serverPort;
    }

    public boolean isEnable() {
        return enable;
    }
}
