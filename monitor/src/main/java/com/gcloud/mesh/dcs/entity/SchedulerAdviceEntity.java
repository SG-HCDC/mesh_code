
package com.gcloud.mesh.dcs.entity;

import java.io.Serializable;
import java.util.Date;

import com.gcloud.framework.db.jdbc.annotation.ID;
import com.gcloud.framework.db.jdbc.annotation.Table;

import lombok.Data;

@Table(name = "dcs_scheduler_advice")
@Data
public class SchedulerAdviceEntity implements Serializable {

    @ID
    private Long id;
    private String appId;
    private Integer status;
    private String srcDatacenterId;
    private String dstDatacenterId;
    private Integer schedulerModelTool;
    private Integer schedulerStrategyConfig;
    private Integer schedulerModel;
    private String appName;
    private Integer cloudResourceType;
    private String cloudResourceId;
    private String cloudResourceName;
    private String instanceId;
    private String instanceName;
    private String dstCloudResourceId;
    private Date createTime;
    private String srcNodeId;
    private String dstNodeId;
}
