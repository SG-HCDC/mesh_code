
package com.gcloud.mesh.dcs.dao;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.stereotype.Repository;

import com.gcloud.framework.db.PageResult;
import com.gcloud.framework.db.dao.impl.JdbcBaseDaoImpl;
import com.gcloud.mesh.dcs.entity.SchedulerJobEntity;
import com.gcloud.mesh.dcs.enums.SchedulerJobStatus;
import com.gcloud.mesh.header.msg.dcs.PageSchedulerJobMsg;
import com.gcloud.mesh.header.vo.dcs.SchedulerJobVo;
import com.gcloud.mesh.header.vo.dcs.SchedulerOverview;

@Repository
public class SchedulerJobDao extends JdbcBaseDaoImpl<SchedulerJobEntity, String> {

	public PageResult<SchedulerJobVo> page(PageSchedulerJobMsg msg) {
		List<Object> values = new ArrayList<>();

		StringBuilder sb = new StringBuilder();
		sb.append("SELECT (case when st.step_count is null then 0 else st.step_count end) as step_count,d.* FROM dcs_scheduler_jobs d " +
				" left join (select count(*) as step_count,scheduler_job_id from  dcs_scheduler_steps t where  t.status = 3 group by t.scheduler_job_id) as st " +
				" on st.scheduler_job_id = d.id " +
				" WHERE 1=1 ");

		if (msg.getStatus() != null) {
			sb.append(" AND d.status = ? ");
			values.add(msg.getStatus());
		}
		if (msg.getName() != null) {
			sb.append(" AND d.app_name LIKE concat('%', ?, '%')");
			values.add(msg.getName());
		}
		if (msg.getDatacenterId() != null) {
			sb.append(" AND d.src_datacenter_id = ? ");
			values.add(msg.getDatacenterId());
		}
		sb.append(" ORDER BY if(isnull(d.begin_time),0,1),d.begin_time desc,d.end_time DESC");
		
		return this.findBySql(sb.toString(), values, msg.getPageNo(), msg.getPageSize(), SchedulerJobVo.class);
	}

	public void updateJobStatus(SchedulerJobEntity job, SchedulerJobStatus status) {

		List<String> fields = new ArrayList<>();
		fields.add("status");
		job.setStatus(status.getValue());
		if (status.getValue() == 2 || status.getValue() ==3){
			fields.add("endTime");
			job.setEndTime(new Date());
		}
		this.update(job, fields);
	}
	
	public int count(String datacenterId) {
		StringBuilder sql = new StringBuilder("SELECT COUNT(*) FROM dcs_scheduler_jobs WHERE 1=1");
		if (datacenterId != null) {
			sql.append(" AND src_datacenter_id=").append(datacenterId);
		}
		return this.countBySql(sql.toString(), new ArrayList<Object>());
	}

	public void updateJobStatusById(String schedulerJobId, SchedulerJobStatus status) {
		if (status==SchedulerJobStatus.PROGRESS){
			super.jdbcTemplate.update("update dcs_scheduler_jobs set status = ?,end_time = null where id = ? ",status.getValue(),schedulerJobId);
		}else{
			super.jdbcTemplate.update("update dcs_scheduler_jobs set status = ?,end_time = ? where id = ? ",status.getValue(),new Date(),schedulerJobId);
		}
	}

	public int updateJobInstanceId(String jobId, String destInstanceId) {
		
		return super.jdbcTemplate.update("update dcs_scheduler_jobs set instance_id = ? where id = ? ",destInstanceId,jobId);
	}

	public SchedulerOverview overview(String dateString) {
		List<Object> value = new ArrayList<>();
		value.add(dateString);
		value.add(dateString);
		value.add(dateString);
		StringBuffer buffer = new StringBuffer();
		buffer.append("select (select count(id) from dcs_scheduler_jobs where begin_time > ? ) as total,");
		buffer.append("(select count(id) from dcs_scheduler_jobs where begin_time > ? and (status = 0 or status = 1)) as working,");
		buffer.append("(select count(id) from dcs_scheduler_jobs where begin_time > ? and status = 3) as finished");
		List<SchedulerOverview> overviews = findBySql(buffer.toString(), value, SchedulerOverview.class);
		if(overviews == null || overviews.isEmpty()) {
			return new SchedulerOverview();
		}else {
			return overviews.get(0);
		}
	}
}
