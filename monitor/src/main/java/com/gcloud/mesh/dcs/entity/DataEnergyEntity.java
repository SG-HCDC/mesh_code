package com.gcloud.mesh.dcs.entity;

import java.util.Date;

import com.gcloud.framework.db.jdbc.annotation.ID;
import com.gcloud.framework.db.jdbc.annotation.Table;

import lombok.Data;

@Table(name = "dcs_energy_data")
@Data
public class DataEnergyEntity {

	@ID
	private String id;
	private Date timestamp;
	private Double value;
	private Long count;
	private String datacenterId;
	
}
