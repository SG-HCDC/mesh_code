
package com.gcloud.mesh.dcs.entity;

import java.util.Date;

import com.gcloud.framework.db.jdbc.annotation.ID;
import com.gcloud.framework.db.jdbc.annotation.Table;

import lombok.Data;

@Table(name = "dcs_authorities")
@Data
public class AuthorityEntity {

    @ID
    private String id;
    private String name;
    private String classification;
    private Boolean enabled;
    private Date updateTime;

}
