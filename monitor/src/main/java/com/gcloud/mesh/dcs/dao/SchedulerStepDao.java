
package com.gcloud.mesh.dcs.dao;

import java.util.Date;
import java.util.List;

import com.gcloud.mesh.dcs.enums.SchedulerStepStatus;
import com.google.api.client.util.Lists;
import org.springframework.stereotype.Repository;

import com.gcloud.framework.db.dao.impl.JdbcBaseDaoImpl;
import com.gcloud.mesh.dcs.entity.SchedulerStepEntity;

@Repository
public class SchedulerStepDao extends JdbcBaseDaoImpl<SchedulerStepEntity, String> {

    public <E> List<E> list(String schedulerJobId, Class<E> clazz) {
        StringBuilder sb = new StringBuilder();
        sb.append("SELECT * FROM dcs_scheduler_steps");
        sb.append(" WHERE 1=1");
        sb.append(" AND scheduler_job_id='").append(schedulerJobId).append("'");
        sb.append(" ORDER BY id asc ");
//        sb.append(" ORDER BY begin_time asc,if(isnull(end_time),1,0),end_time");
        return this.findBySql(sb.toString(), clazz);
    }

    public void updateLastSetpName(String myJobId, String name) {
        List<Object> p = Lists.newArrayList();
        p.add(myJobId);
        List<SchedulerStepEntity> sse = super.findBySql("select * from dcs_scheduler_steps where scheduler_job_id = ? order by begin_time desc limit 1", p, SchedulerStepEntity.class);
        SchedulerStepEntity schedulerStepEntity = sse.get(0);
        super.jdbcTemplate.update("update dcs_scheduler_steps set name = ? where id = ?",name,schedulerStepEntity.getId());
    }

    public void updateStepStatusById(String schedulerStepId, SchedulerStepStatus status,String result) {
        super.jdbcTemplate.update("update dcs_scheduler_steps set status = ?,end_time = ?,result_msg = ? where id = ? ",status.getValue(),new Date(),result,schedulerStepId);
    }

    public void updateAttachBySchedulerJobId(String schedulerJobId, String attach) {
        super.jdbcTemplate.update("update dcs_scheduler_steps set attach = ?  where scheduler_job_id = ? and end_time is null",attach,schedulerJobId);
    }
}
