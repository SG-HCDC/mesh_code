
package com.gcloud.mesh.dcs.entity;

import com.gcloud.framework.db.jdbc.annotation.ID;
import com.gcloud.framework.db.jdbc.annotation.Table;

import lombok.Data;

@Table(name = "dcs_scheduler_model_types")
@Data
public class SchedulerModelTypeEntity {

    @ID
    private String id;
    private String name;
    

}
