package com.gcloud.mesh.dcs.enums;

public enum SchedulerModelType {

	DATACENTER_COST(0, "最优成本模型"),
	DATACENTER_DEVICE(1, "最优设备模型"),
	DATACENTER_POWER(2, "最优能耗代价模型"),
	DATACENTER_ELECTRIC(3, "最优配电子网均衡模型");

	private Integer value;
	private String name;

	SchedulerModelType(Integer value, String name) {
		this.value = value;
		this.name = name;
	}

	public static SchedulerModelType getByType(Integer value) {
		if (value != null) {
			for (SchedulerModelType driver : SchedulerModelType.values()) {
				if (driver.getValue().equals(value)) {
					return driver;
				}
			}
		}
		return null;
	}
	public static SchedulerModelType getByName(String name) {
		if (name != null) {
			for (SchedulerModelType driver : SchedulerModelType.values()) {
				if (driver.name().equals(name)) {
					return driver;
				}
			}
		}
		return null;
	}
	

	public Integer getValue() {
		return value;
	}

	public String getName() {
		return name;
	}
    public static SchedulerModelType[] getSchedulerModelType() {
    	SchedulerModelType[] types = new SchedulerModelType[] {SchedulerModelType.DATACENTER_COST, SchedulerModelType.DATACENTER_DEVICE, SchedulerModelType.DATACENTER_POWER, SchedulerModelType.DATACENTER_ELECTRIC};
    	return types;
    }
}
