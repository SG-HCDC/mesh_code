package com.gcloud.mesh.config;

/**
 * 数据安全专用定时器配置
 * <br>废弃</br>，有缺陷，动态修改后必须等到当前周期计划执行完，才能更新周期计划
 * @author lcarr
 *
 */
//@Configuration
//@EnableScheduling
//@Slf4j
//public class DynamicScheduleConfig implements SchedulingConfigurer {
//	
//	@Autowired
//	private DataSecurityService service;
//	
//	private boolean enabled;
//	
//	private String cron = "0/5 * * * * ?";
//	
//	private String policy;
//	
//	@Override
//	public void configureTasks(ScheduledTaskRegistrar taskRegistrar) {
//		// TODO Auto-generated method stub
//		ThreadPoolTaskScheduler taskScheduler = new ThreadPoolTaskScheduler();
//		taskScheduler.setPoolSize(10);
//		taskScheduler.initialize();
//		taskRegistrar.setTaskScheduler(taskScheduler);
//
//		Runnable runnable = () -> {
//			if(enabled) {
//				log.info("[DynamicScheduleConfig][configureTasks] 定时任务运行【{}】 :{}", DataSecurityPolicy.getByName(policy), cron);
//				service.execute();
//			}else {
//				log.info("[DynamicScheduleConfig][configureTasks] 数据安全定时任务运行正常");
//			}
//		};
//		Trigger trigger = triggerContext -> {
//			DetailDataSecurityMsg msg = new DetailDataSecurityMsg();
//			DataSecurityVo vo = null;
//			try {
//				vo = service.detail(msg);
//			} catch(Exception e) {
//				log.info("[DynamicScheduleConfig][configureTasks] DataSecurityService中detail方法运行异常 :{}", e.getMessage());
//			}
//            cron = service.getCron();
//            enabled = vo.getEnabled();
//            policy = vo.getPolicy();
//            return new CronTrigger(cron).nextExecutionTime(triggerContext);
//        };
//		taskRegistrar.addTriggerTask(runnable, trigger);
//		
//	}
//
//}
