package com.gcloud.mesh.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import lombok.Data;


@Data
@Component
public class DnsConfig {
	@Value("${mesh.dns.server:sgcc-dns}")
	private String server;
	@Value("${mesh.dns.domain:demo.nginx.com}")
	private String domain;

}
