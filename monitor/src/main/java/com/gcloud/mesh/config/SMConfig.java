package com.gcloud.mesh.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import lombok.Data;

//@ConfigurationProperties(prefix = "mesh.sm")
@Data
//@Component
public class SMConfig {

	private String secretKey;
}
