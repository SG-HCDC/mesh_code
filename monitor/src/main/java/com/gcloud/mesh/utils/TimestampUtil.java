package com.gcloud.mesh.utils;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class TimestampUtil {
	
	public static final String BEFORE = "before";
	
	public static final String AFTER = "after";
	
	public static final String DEFAULT = "yyyy-MM-dd HH:mm:ss";

	public static String dateToStr(Date date) {
		String format =  DEFAULT;
		return TimestampUtil.dateToStr(date, format);
	}
	
	public static String dateToStr(Date date, String format) {
		String result = null;
		if(date != null && format != null) {
			SimpleDateFormat sdf =  new SimpleDateFormat(format);
			try {
				result = sdf.format(date);
			} catch(Exception e) {
				log.error(e.getMessage());
			}
		}
		return result;
	}
	
	public static Date strToDate(String date) {
		String format =  DEFAULT;
		return TimestampUtil.strToDate(date, format);
	}
	
	public static Date strToDate(String date, String format) {
		Date result = null;
		if(date != null && format != null) {
			SimpleDateFormat sdf =  new SimpleDateFormat(format);
			try {
				result = sdf.parse(date);
			} catch(Exception e) {
				log.error(e.getMessage());
			}
		}
		return result;
	}
	
	public static Date BeforeDayNow(int day) {
		return diffDate(BEFORE, new Date(), 0, 0, day, 0, 0, 0, 0);
	}
	
	public static Date BeforeMinuteNow(int minute) {
		return diffDate(BEFORE, new Date(), 0, 0, 0, 0, minute, 0, 0);
	}
	
	public static Date BeforeHourNow(int hour) {
		return diffDate(BEFORE, new Date(), 0, 0, 0, hour, 0, 0, 0);
	}
	
	public static Date BeforeSecondNow(int second) {
		return diffDate(BEFORE, new Date(), 0, 0, 0, 0, 0, second, 0);
	}
	
	public static Date BeforeSecondDate(Date date, int second) {
		return diffDate(BEFORE, date, 0, 0, 0, 0, 0, second, 0);
	}
	
	public static Date diffDate(String diff, Date date, int year, int month, int day, int hour, int min, int second, int millisecond) {
		Calendar base = Calendar.getInstance();
		base.setTime(date);
		if(BEFORE.equals(diff)) {
			year = -year;
			month = -month;
			day = -day;
			hour = -hour;
			min = -min;
			second = -second;
		}	
		base.add(Calendar.YEAR, year);
		base.add(Calendar.MONDAY, month);
		base.add(Calendar.DAY_OF_MONTH, day);
		base.add(Calendar.HOUR, hour);
		base.add(Calendar.MINUTE, min);
		base.add(Calendar.SECOND, second);
		base.add(Calendar.MILLISECOND, millisecond);
		return base.getTime();
	}
	
	public static int compare(String time1, String time2, String format) {
		SimpleDateFormat sdf =  new SimpleDateFormat(format);
		return 0;
	}
	
	public static String formatDate(String format, Date date) {
		SimpleDateFormat sdf =  new SimpleDateFormat(format); 
		return sdf.format(date);
	}
	
	public static Calendar get(String format, String dateStr) {
		Date date = strToDate(format, dateStr);
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		return cal;
	}
	
	public static String formatDateStr(String format, String dateStr) {
		Date date = strToDate(format, dateStr);
		SimpleDateFormat sdf =  new SimpleDateFormat(format); 
		return sdf.format(date);
	}
	
}
