package com.gcloud.mesh.monitor.util;

public class CompareUtil {
	public static boolean compare(double statisticsValue,double threshold,Integer comparisonOperator){
    	switch(comparisonOperator){
    		case 0: return statisticsValue >= threshold;
    		case 1: return statisticsValue > threshold;
    		case 2: return statisticsValue <= threshold;
    		case 3: return statisticsValue < threshold;
    		case 4: return statisticsValue == threshold;
    		case 5: return statisticsValue != threshold;
    		default: return statisticsValue != threshold;
    	}
    }
}
