package com.gcloud.mesh.monitor.cloud;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.alibaba.fastjson.JSONObject;
import com.gcloud.mesh.asset.dao.CloudResourceConfigDao;
import com.gcloud.mesh.asset.entity.CloudResourceConfigEntity;
import com.gcloud.mesh.dcs.dao.AppInstanceDao;
import com.gcloud.mesh.dcs.entity.AppEntity;
import com.gcloud.mesh.dcs.entity.AppInstanceEntity;
import com.gcloud.mesh.dcs.strategy.HuaweiQueryInstance;
import com.gcloud.mesh.dcs.strategy.huawei.BatchListMetricDataSolution;
import com.gcloud.mesh.header.param.HuaweiMigrationParam;
import com.gcloud.mesh.header.vo.dcs.DescribeMetricVO;
import com.google.common.collect.Lists;
import com.huaweicloud.sdk.ces.v1.model.BatchListMetricDataResponse;
import com.huaweicloud.sdk.ces.v1.model.BatchMetricData;
@Service
public class HuaweiMonitor extends VMCloudMonitor implements ICloudBaseMonitor{
    @Autowired
    private CloudResourceConfigDao cloudResourceConfigDao;
    @Autowired
    private AppInstanceDao instanceDao;

	
	public List<DescribeMetricVO> queryDescribeMetricLastList(String cloudResourceId) {
		// huaweiQueryInstance.queryServerList(cloudResourceId);
		CloudResourceConfigEntity cloudResource = cloudResourceConfigDao.findOneByProperty("cloud_resource_id", cloudResourceId);
        if(cloudResource==null) return Lists.newArrayList();

        HuaweiMigrationParam huaweiMigrationParam = JSONObject.toJavaObject(JSONObject.parseObject(cloudResource.getConfig()), HuaweiMigrationParam.class);

        // List<AppEntity> appList = huaweiQueryInstance.queryServerList(huaweiMigrationParam);
        List<AppInstanceEntity> instances = instanceDao.findByCloudResourceId(cloudResourceId);
    	long from = System.currentTimeMillis()-60000;//60秒钟检查一次
    	long to = System.currentTimeMillis();
        // Map<String,String> appMap = Maps.newHashMap();
        String accessKeyId = huaweiMigrationParam.getAccessKey();
        String accessKeySecret = huaweiMigrationParam.getSecretKey();
        String regionId = huaweiMigrationParam.getRegionId();
        BatchListMetricDataSolution listMetric = new BatchListMetricDataSolution();
        List<DescribeMetricVO> vos = new ArrayList<>();
        if(instances!=null && !instances.isEmpty()){
        	for(AppInstanceEntity app : instances) {
                if(huaweiMigrationParam!=null){
                	Double mem = 0.0;
                	Double cpu = 0.0;
                	BatchListMetricDataResponse res = listMetric.listMetricData(app.getInstanceId(), regionId, accessKeyId, accessKeySecret, from, to);
                	List<BatchMetricData>  datas = res.getMetrics();
                	if(res.getMetrics() != null) {
                		DescribeMetricVO vo = new DescribeMetricVO();
                		vo.setInstanceId(app.getInstanceId());
                		vo.setInstanceName(app.getName());
                		vo.setMemVal("0");
                		vo.setCupVal("0");
                		for(BatchMetricData data : datas) {
                			if("mem_usedPercent".equals(data.getMetricName())) {
                				if(data.getDatapoints()!= null && !data.getDatapoints().isEmpty()) {
                					// log.info("监控项[HUAWEI_MEM_UTIL] menVal: {}",data.getDatapoints().get(0).getAverage());
                					mem =  data.getDatapoints().get(0).getAverage();
                					vo.setMemVal((mem == null ? "0.0":String.valueOf(mem)));
                				}
                			}
                			if("cpu_usage".equals(data.getMetricName())) {
                				if(data.getDatapoints()!= null && !data.getDatapoints().isEmpty()) {
                					cpu =  data.getDatapoints().get(0).getAverage();
                					vo.setCupVal((cpu == null ? "0.0":String.valueOf(cpu)));
                				}
                			}
                			
                		}
                		vos.add(vo);
                	}

                }
        	}

        }

        return vos;
	}
	
}
