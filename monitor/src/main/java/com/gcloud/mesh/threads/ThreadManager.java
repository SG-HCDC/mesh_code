package com.gcloud.mesh.threads;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.Future;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import org.springframework.stereotype.Component;

import com.google.common.util.concurrent.ListenableFuture;
import com.google.common.util.concurrent.ListeningExecutorService;
import com.google.common.util.concurrent.MoreExecutors;

@Component
public class ThreadManager {
	
    private static final ListeningExecutorService EXECUTOR = MoreExecutors.listeningDecorator(
            new ThreadPoolExecutor(5, 20, 60, TimeUnit.SECONDS, new ArrayBlockingQueue<Runnable>(15), new ThreadPoolExecutor.DiscardOldestPolicy()));

    public static Future<?> submit(Runnable runnable) {
        return EXECUTOR.submit(runnable);
    }

    public static Future<?> submit(Runnable runnable, Runnable listener) {
        ListenableFuture<?> future = EXECUTOR.submit(runnable);
        future.addListener(listener, EXECUTOR);
        return future;
    }

}
