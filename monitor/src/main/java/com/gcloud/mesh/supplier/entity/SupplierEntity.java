
package com.gcloud.mesh.supplier.entity;

import com.gcloud.framework.db.jdbc.annotation.ID;
import com.gcloud.framework.db.jdbc.annotation.Table;

import lombok.Data;
import lombok.experimental.Accessors;

@Table(name = "supplier_suppliers")
@Data
@Accessors(chain = true)
public class SupplierEntity {

    @ID
    private String id;
    private String name;
    private String type;
    private String datacenterId;
//    private String ip;
//    private Integer port;
    private String config;
    private String insecureCommand;
    private String clusterId;

}
