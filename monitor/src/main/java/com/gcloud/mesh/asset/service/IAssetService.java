package com.gcloud.mesh.asset.service;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.gcloud.framework.db.PageResult;
import com.gcloud.mesh.asset.entity.IaasEntity;
import com.gcloud.mesh.header.exception.BaseException;
import com.gcloud.mesh.header.msg.asset.CreateDatacenterMsg;
import com.gcloud.mesh.header.msg.asset.CreateDeviceMsg;
import com.gcloud.mesh.header.msg.asset.CreateNodeMsg;
import com.gcloud.mesh.header.msg.asset.CreateNodeSmMsg;
import com.gcloud.mesh.header.msg.asset.CreateSwitcherMsg;
import com.gcloud.mesh.header.msg.asset.ListDatacenterMsg;
import com.gcloud.mesh.header.msg.asset.ListDeviceMsg;
import com.gcloud.mesh.header.msg.asset.UpdateDatacenterMsg;
import com.gcloud.mesh.header.msg.asset.UpdateDeviceMsg;
import com.gcloud.mesh.header.msg.asset.UpdateNodeMsg;
import com.gcloud.mesh.header.msg.asset.UpdateNodeSmMsg;
import com.gcloud.mesh.header.msg.asset.UpdateSwitcherMsg;
import com.gcloud.mesh.header.vo.asset.CountDeviceVo;
import com.gcloud.mesh.header.vo.asset.DatacenterItemVo;
import com.gcloud.mesh.header.vo.asset.DeviceItemVo;
import com.gcloud.mesh.header.vo.asset.NodeItemVo;
import com.gcloud.mesh.header.vo.asset.SwitcherItemVo;

public interface IAssetService {

	public String createDevice(CreateDeviceMsg msg, String userId) throws BaseException;
	
	public PageResult<DeviceItemVo> pageDevice(int pageNum, int pageSize, Integer type, String name, String userId) throws BaseException;
	
	public PageResult<DeviceItemVo> pageDevice(int pageNum, int pageSize, String datacenterId, Integer type, String name, String userId) throws BaseException;
	
	public List<DeviceItemVo> listDevice(ListDeviceMsg msg) throws BaseException;
	
	public CountDeviceVo countDevice(String datacenterId) throws BaseException;
	
	public DatacenterItemVo getDatacenter(String datacenterId) throws BaseException;
	
	public DeviceItemVo getDeviceByDeviceId(String deviceId) throws BaseException;
	
	public DeviceItemVo detailDevice(String id, String userId) throws BaseException;
	
	public IaasEntity deleteDevice(String id, String userId) throws BaseException;
	
	public String updateDevice(UpdateDeviceMsg msg, String userId) throws BaseException;
	
	public String createNode(CreateNodeSmMsg msg, String userId) throws BaseException;
	
	public String updateNode(UpdateNodeSmMsg msg, String userId) throws BaseException;
	
	public String updateNode(UpdateNodeMsg msg, String userId) throws BaseException;
	
	public NodeItemVo detailNode(String id, String userId) throws BaseException;
	
	public String createNode(CreateNodeSmMsg msg, String userId, HttpServletRequest request, HttpServletResponse response) throws BaseException;
	
	public String updateNode(UpdateNodeSmMsg msg, String userId, HttpServletRequest request, HttpServletResponse response) throws BaseException;
	
	public NodeItemVo detailNode(String id, String userId, HttpServletRequest request, HttpServletResponse response) throws BaseException;
	
	public PageResult<NodeItemVo> pageNode(int pageNum, int pageSize, String name, String order, String userId) throws BaseException;
	
	public PageResult<NodeItemVo> pageNode(int pageNum, int pageSize, String datacenterId, String name, String order, String userId) throws BaseException;
	
	public List<NodeItemVo> listNode(String datacenterId, String userId) throws BaseException;
	
	public IaasEntity deleteNode(String id, String userId) throws BaseException;
	
	public String createSwitcher(CreateSwitcherMsg msg, String userId) throws BaseException;
	
	public String updateSwitcher(UpdateSwitcherMsg msg, String userId) throws BaseException;
	
	public SwitcherItemVo detailSwitcher(String id, String userId) throws BaseException;
	
	public String createSwitcher(CreateSwitcherMsg msg, String userId, HttpServletRequest request, HttpServletResponse response) throws BaseException;
	
	public String updateSwitcher(UpdateSwitcherMsg msg, String userId, HttpServletRequest request, HttpServletResponse response) throws BaseException;
	
	public SwitcherItemVo detailSwitcher(String id, String userId, HttpServletRequest request, HttpServletResponse response) throws BaseException;
	
	public PageResult<SwitcherItemVo> pageSwitcher(int pageNum, int pageSize, String name, String userId) throws BaseException;
	
	public PageResult<SwitcherItemVo> pageSwitcher(int pageNum, int pageSize, String name, String userId, HttpServletRequest request, HttpServletResponse response) throws BaseException;
	
	public IaasEntity deleteSwitcher(String id, String userId) throws BaseException;
	
	public String createDatacenter(CreateDatacenterMsg msg, String userId) throws BaseException;
	
	public String updateDatacenter(UpdateDatacenterMsg msg, String userId) throws BaseException;
	
	public PageResult<DatacenterItemVo> pageDatacenter(int pageNum, int pageSize, String id, String name, String ip, String userId) throws BaseException;
	
	public PageResult<DatacenterItemVo> pageDatacenter(int pageNum, int pageSize, String id, String name, String ip, String userId, Boolean hasNode) throws BaseException;
	
	public PageResult<DatacenterItemVo> pageDatacenter(int pageNum, int pageSize, String name, String ip, String userId) throws BaseException;
	
	public DatacenterItemVo detailDatacenter(String id, String userId) throws BaseException;
	
	public String deleteDatacenter(String id, String userId) throws BaseException;
	
	public List<DatacenterItemVo> listDatacenter(ListDatacenterMsg msg) throws BaseException;
	
	public PageResult<DeviceItemVo> pageItDevice(int pageNum, int pageSize, String name, String userId) throws BaseException;
	
	public PageResult<DeviceItemVo> pageItDevice(int pageNum, int pageSize, String datacenterId, String name, Integer type) throws BaseException;
	
	public List<DeviceItemVo> listDevice(String datacenterId, Integer type, String userId) throws BaseException;
	
	public String syncDevice() throws BaseException;
	
	public String syncNodeSwitcher() throws BaseException;
	
	public int countDatacenter() throws BaseException;
}
