package com.gcloud.mesh.asset.enums;

import java.util.stream.Stream;

public enum DeviceType {

	SIMPLIFY(-1, "simplify", "精简列表类型"),
	GENERAL(0, "general", "通用"),
	AIR_CONDITION(1, "air_condition", "空调"),
	UPS(2, "ups", "UPS"),
	PDU(3, "pdu", "PDU"),
	TEMPER_SENSOR(4, "temperature_sensor", "温度传感器"),
	CABINET(5, "cabinet", "机柜"),
	SERVER(6, "server", "服务器"),
	SWITCHER(7, "switcher", "交换机"),
	DISTRIBUTION_BOX(8, "distribution_box", "配电箱");
	
	private int no;
	private String name;
	private String cnName;
	
	DeviceType(int no, String name, String cnName) {
		this.no = no;
		this.name = name;
		this.cnName = cnName;
	}
	
	public static DeviceType getDeviceTypeByNo(int no) {
		return Stream.of(DeviceType.values())
				.filter( s -> s.getNo() == no)
				.findFirst()
				.orElse(null);
	}
	
	public static DeviceType getDeviceTypeByName(String name) {
		return Stream.of(DeviceType.values())
				.filter( s -> s.getName().equals(name))
				.findFirst()
				.orElse(null);
	}
	
	public int getNo() {
		return no;
	}
	
	public String getName() {
		return name;
	}
	
	public String getCnName() {
		return cnName;
	}
	
}
