package com.gcloud.mesh.asset.enums;

import java.util.stream.Stream;

import com.gcloud.mesh.header.enums.AuthorityResourceType;

public enum AuthorityCloudResourceType {

	VM1(AuthorityResourceType.VM, CloudResourceType.HUAWEI),
	VM2(AuthorityResourceType.VM, CloudResourceType.ALI),
	K8S(AuthorityResourceType.K8S, CloudResourceType.K8S);
	
	private AuthorityResourceType resource;
	private CloudResourceType cloudResource;
	
	AuthorityCloudResourceType(AuthorityResourceType resource, CloudResourceType cloudResource) {
		this.resource = resource;
		this.cloudResource = cloudResource;
	}

	public static AuthorityCloudResourceType getByResource(AuthorityResourceType resource) {
		return Stream.of(AuthorityCloudResourceType.values())
				.filter( s -> s.getResource().equals(resource))
				.findFirst()
				.orElse(null);
	}
	
	public static AuthorityCloudResourceType getByCloudResource(CloudResourceType device) {
		return Stream.of(AuthorityCloudResourceType.values())
				.filter( s -> s.getResource().equals(device))
				.findFirst()
				.orElse(null);
	}
	
	public AuthorityResourceType getResource() {
		return resource;
	}

	public CloudResourceType getCloudResource() {
		return cloudResource;
	}


}
