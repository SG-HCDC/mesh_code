package com.gcloud.mesh.asset.enums;

import java.util.stream.Stream;

public enum XbrotherDeviceType {

	GENERAL(0, "general", "自动上架设备", DeviceType.GENERAL),
	SERVER(1, "IT设施>服务器>机架式服务器", "机架式空调", DeviceType.SERVER),
	SWITCHER(2, "IT设施>网络与安全>交换机", "交换机", DeviceType.SWITCHER);
	
	private int no;
	private String name;
	private String cnName;
	private DeviceType deviceType;
	
	XbrotherDeviceType(int no, String name, String cnName, DeviceType deviceType) {
		this.no = no;
		this.name = name;
		this.cnName = cnName;
		this.deviceType = deviceType;
	}
	
	public static XbrotherDeviceType getDeviceTypeByNo(int no) {
		return Stream.of(XbrotherDeviceType.values())
				.filter( s -> s.getNo() == no)
				.findFirst()
				.orElse(null);
	}
	
	public static XbrotherDeviceType getDeviceTypeByName(String name) {
		return Stream.of(XbrotherDeviceType.values())
				.filter( s -> s.getName().equals(name))
				.findFirst()
				.orElse(null);
	}
	
	public static XbrotherDeviceType getDeviceTypeByCnName(String cnName) {
		return Stream.of(XbrotherDeviceType.values())
				.filter( s -> s.getCnName().equals(cnName))
				.findFirst()
				.orElse(null);
	}
	
	public int getNo() {
		return no;
	}
	
	public String getName() {
		return name;
	}
	
	public String getCnName() {
		return cnName;
	}
	
	public DeviceType getDeviceType() {
		return deviceType;
	}
}
