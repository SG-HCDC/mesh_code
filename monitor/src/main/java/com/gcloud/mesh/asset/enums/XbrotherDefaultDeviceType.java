package com.gcloud.mesh.asset.enums;

import java.util.stream.Stream;

public enum XbrotherDefaultDeviceType {

	GENERAL(0, "general", "自动上架设备", DeviceType.GENERAL),
	AIR_CONDITION(1, "air_condition", "机架式空调", DeviceType.AIR_CONDITION),
	UPS(2, "ups", "UPS", DeviceType.UPS),
	DISTRIBUTION_BOX(3, "pdu", "配电箱", DeviceType.DISTRIBUTION_BOX);
	
	private int no;
	private String name;
	private String cnName;
	private DeviceType deviceType;
	
	XbrotherDefaultDeviceType(int no, String name, String cnName, DeviceType deviceType) {
		this.no = no;
		this.name = name;
		this.cnName = cnName;
		this.deviceType = deviceType;
	}
	
	public static XbrotherDefaultDeviceType getDeviceTypeByNo(int no) {
		return Stream.of(XbrotherDefaultDeviceType.values())
				.filter( s -> s.getNo() == no)
				.findFirst()
				.orElse(null);
	}
	
	public static XbrotherDefaultDeviceType getDeviceTypeByName(String name) {
		return Stream.of(XbrotherDefaultDeviceType.values())
				.filter( s -> s.getName().equals(name))
				.findFirst()
				.orElse(null);
	}
	
	public static XbrotherDefaultDeviceType getDeviceTypeByCnName(String cnName) {
		return Stream.of(XbrotherDefaultDeviceType.values())
				.filter( s -> s.getCnName().equals(cnName))
				.findFirst()
				.orElse(null);
	}
	
	public int getNo() {
		return no;
	}
	
	public String getName() {
		return name;
	}
	
	public String getCnName() {
		return cnName;
	}
	
	public DeviceType getDeviceType() {
		return deviceType;
	}
}
