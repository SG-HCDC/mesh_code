package com.gcloud.mesh.asset.enums;

import java.util.stream.Stream;

public enum NetworkStatus {
	
	OFFLINE(0, "off_line", "离线"),
	ONLINE(1, "on_line", "在线");
	
	private int no;
	private String name;
	private String cnName;
	
	NetworkStatus(int no, String name, String cnName) {
		this.no = no;
		this.name = name;
		this.cnName = cnName;
	}
	
	public static NetworkStatus getStatusByNo(int no) {
		return Stream.of(NetworkStatus.values())
			.filter( s -> s.getNo() == no)
			.findFirst()
			.orElse(null);
	}
	
	public static NetworkStatus getStatusByName(String name) {
		return Stream.of(NetworkStatus.values())
			.filter( s -> s.getName().equals(name))
			.findFirst()
			.orElse(null);
	}
	
	public int getNo() {
		return no;
	}
	
	public String getName() {
		return name;
	}
	
	public String getCnName() {
		return cnName;
	}

}
