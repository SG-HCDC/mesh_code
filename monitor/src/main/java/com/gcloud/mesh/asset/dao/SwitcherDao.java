package com.gcloud.mesh.asset.dao;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.gcloud.framework.db.PageResult;
import com.gcloud.framework.db.dao.impl.JdbcBaseDaoImpl;
import com.gcloud.mesh.asset.entity.SwitcherEntity;
import com.gcloud.mesh.dcs.service.AuthorityService;
import com.gcloud.mesh.header.enums.AuthorityResourceClassification;
import com.gcloud.mesh.header.enums.AuthorityResourceType;
import com.gcloud.mesh.header.msg.dcs.ListByClassificationMsg;
import com.gcloud.mesh.header.vo.dcs.AuthorityVo;
import io.micrometer.core.instrument.util.StringUtils;

@Repository
public class SwitcherDao extends JdbcBaseDaoImpl<SwitcherEntity, String> {
	
	@Autowired
	private AuthorityService authorityService;

	public <E> E getById(String id, String userId, Class<E> clazz) {
		StringBuffer sql = new StringBuffer();
		sql.append(" select i.*, "
				+ " s.ip ip, s.community community, s.status status, s.cabinet_id cabinet_id,"
				+ " d.name datacenter_name, d.ip datacenter_ip"
				+ " from asset_iaas i");
		sql.append(" left join asset_switchers s on i.device_id = s.id");
		sql.append(" left join asset_datacenters d on i.datacenter_id = d.id");
		sql.append(" where 1 = 1");
		List<Object> values = new ArrayList<Object>();
		if(StringUtils.isNotEmpty(id)) {	
			sql.append(" and i.id = ?");
			values.add(id);
		}
		if(StringUtils.isNotEmpty(userId)) {
			sql.append(" and i.creator = ?");
			values.add(userId);
		}
		return findBySql(sql.toString(), values, clazz).size() > 0? findBySql(sql.toString(), values, clazz).get(0): null;
	}
	
	public <E> PageResult<E> page(int pageNum, int pageSize, String name, String userId, Class<E> clazz) {
		StringBuffer sql = new StringBuffer();
		sql.append(" select i.*, d.name datacenter_name, "
				+ " s.ip ip, s.community community, s.status status, s.cabinet_id cabinet_id"
				+ " from asset_iaas i");
		sql.append(" left join asset_switchers s on i.device_id = s.id");
		sql.append(" left join asset_datacenters d on i.datacenter_id = d.id");
		sql.append(" where 1 = 1");
		sql.append(" and i.type = 7");
		
		checkResourceAuthority(sql);
		
		List<Object> values = new ArrayList<Object>();
//		if(StringUtils.isNotEmpty(userId)) {
//			sql.append(" and i.creator = ?");
//			values.add(userId);
//		}
		if(StringUtils.isNotEmpty(name)) {
			sql.append(" and i.name like concat('%', ?, '%')");
			values.add(name);
		}
		
		sql.append(" order by i.create_time");
		return this.findBySql(sql.toString(), values, pageNum, pageSize, clazz);	
		
	}
	
	public <E> List<E> list(String name, Class<E> clazz) {
		StringBuffer sql = new StringBuffer();
		sql.append(" select i.*, d.name datacenter_name, "
				+ " s.ip ip, s.community community, s.status status, s.cabinet_id cabinet_id"
				+ " from asset_iaas i");
		sql.append(" left join asset_switchers s on i.device_id = s.id");
		sql.append(" left join asset_datacenters d on i.datacenter_id = d.id");
		sql.append(" where 1 = 1");
		sql.append(" and i.type = 7");
		
		List<Object> values = new ArrayList<Object>();
		if(StringUtils.isNotEmpty(name)) {
			sql.append(" and i.name like concat('%', ?, '%')");
			values.add(name);
		}
		
		sql.append(" order by i.create_time");
		return this.findBySql(sql.toString(), clazz);	
		
	}
	
	private StringBuffer checkResourceAuthority(StringBuffer sql) {

		ListByClassificationMsg msg = new ListByClassificationMsg();
		msg.setClassification(AuthorityResourceClassification.IT_DEVICE.getName());
		List<AuthorityVo> authorities = null;
		try {
			authorities = authorityService.listByClassification(msg);
		}catch(Exception e) {
			
		}
		if(authorities != null) {
			for(AuthorityVo vo: authorities) {
				if(AuthorityResourceType.SWITCHER.equals(AuthorityResourceType.getByName(vo.getId())) && !vo.getEnabled()) {
					sql.append(" and i.from != 'sync'");
					break;
				}
			}
			
		}

		return sql;
	}
}
