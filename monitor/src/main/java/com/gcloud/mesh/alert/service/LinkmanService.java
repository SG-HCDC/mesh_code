package com.gcloud.mesh.alert.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import org.jeecg.common.exception.ParamException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.gcloud.framework.db.PageResult;
import com.gcloud.mesh.alert.dao.LinkmanDao;
import com.gcloud.mesh.alert.dao.PolicyLinkmanDao;
import com.gcloud.mesh.alert.entity.LinkmanEntity;
import com.gcloud.mesh.alert.entity.PolicyLinkmanEntity;
import com.gcloud.mesh.header.exception.BaseException;
import com.gcloud.mesh.header.msg.alert.CreateLinkmanMsg;
import com.gcloud.mesh.header.msg.alert.DeleteLinkmanMsg;
import com.gcloud.mesh.header.msg.alert.DeleteLinkmanReplyMsg;
import com.gcloud.mesh.header.msg.alert.DetailLinkmanMsg;
import com.gcloud.mesh.header.msg.alert.PageLinkmanMsg;
import com.gcloud.mesh.header.msg.alert.UpdateLinkmanMsg;
import com.gcloud.mesh.header.msg.alert.UpdateLinkmanReplyMsg;
import com.gcloud.mesh.header.vo.alert.LinkmanVo;

@Service
public class LinkmanService {
	@Autowired
	private LinkmanDao linkmanDao;

	@Autowired
	PolicyLinkmanDao policyLinkmanDao;

	public String create(CreateLinkmanMsg msg) {
		List<LinkmanEntity> result = linkmanDao.findByProperty("userName", msg.getUserName());
		if (result.size() > 0) {
			throw new ParamException("UserNameIsExist", "用户名已经存在");
		}

		result = linkmanDao.findByProperty("email", msg.getEmail());
		if (result.size() > 0) {
			throw new ParamException("EmailIsExist", "邮箱地址已经存在");
		}

		result = linkmanDao.findByProperty("phoneNumber", msg.getPhoneNumber());
		if (result.size() > 0) {
			throw new ParamException("PhoneNumberIsExist", "手机号码已经存在");
		}

		LinkmanEntity entity = new LinkmanEntity();
		entity.setCreateTime(new Date());
		entity.setEmail(msg.getEmail());
		entity.setId(UUID.randomUUID().toString());
		entity.setPhoneNumber(msg.getPhoneNumber());
		entity.setUserName(msg.getUserName());
		linkmanDao.save(entity);

		return entity.getId();
	}

	public PageResult<LinkmanVo> page(PageLinkmanMsg request) {
		return linkmanDao.page(request.getPageNo(), request.getPageSize(), LinkmanVo.class);
	}

	public UpdateLinkmanReplyMsg update(UpdateLinkmanMsg request) {
		LinkmanEntity linkman = linkmanDao.getById(request.getLinkmanId());
		if (linkman == null) {
			throw new ParamException("LinkmanNotExist", "联系人不存在");
		}

		Date createTime = linkman.getCreateTime();

		LinkmanEntity entity = new LinkmanEntity();
		entity.setCreateTime(createTime);
		entity.setEmail(request.getEmail());
		entity.setId(request.getLinkmanId());
		entity.setPhoneNumber(request.getPhoneNumber());
		entity.setUserName(request.getUserName());
		linkmanDao.update(entity);
		return new UpdateLinkmanReplyMsg();
	}

	public DeleteLinkmanReplyMsg delete(DeleteLinkmanMsg request) {
		List<LinkmanEntity> result = linkmanDao.findByProperty("id", request.getLinkmanId());
		if (result.size() <= 0) {
			throw new ParamException("LinkmanNotExist", "联系人不存在");
		}

		List<PolicyLinkmanEntity> policyLinkmanResult = policyLinkmanDao.findByProperty("linkmanId",
				request.getLinkmanId());

		if (policyLinkmanResult.size() > 0) {
			throw new ParamException("LinkmanIsUsing", "联系人正在使用");
		}

		LinkmanEntity entity = new LinkmanEntity();
		entity.setId(request.getLinkmanId());
		linkmanDao.delete(entity);
		StringBuffer name = new StringBuffer();
		DeleteLinkmanReplyMsg man =  new DeleteLinkmanReplyMsg();
		for(LinkmanEntity en : result) {
			name.append(en.getUserName()+" ");
		}
		man.setLinkManName(name.toString());
		return new DeleteLinkmanReplyMsg();
	}

	public List<LinkmanVo> list() {
		List<LinkmanVo> linkmanVos = new ArrayList<LinkmanVo>();

		for (LinkmanEntity entity : linkmanDao.findAll()) {
			LinkmanVo vo = new LinkmanVo();
			vo.setCreateTime(entity.getCreateTime());
			vo.setEmail(entity.getEmail());
			vo.setId(entity.getId());
			vo.setPhoneNumber(entity.getPhoneNumber());
			vo.setUserName(entity.getUserName());

			linkmanVos.add(vo);
		}

		return linkmanVos;
	}

	public LinkmanVo detail(DetailLinkmanMsg request) {
		LinkmanEntity linkman = linkmanDao.getById(request.getLinkmanId());
		if (linkman == null) {
			throw new ParamException("LinkmanNotExist", "联系人不存在");
		}

		LinkmanVo vo = new LinkmanVo();
		vo.setCreateTime(linkman.getCreateTime());
		vo.setEmail(linkman.getEmail());
		vo.setId(linkman.getId());
		vo.setPhoneNumber(linkman.getPhoneNumber());
		vo.setUserName(linkman.getUserName());

		return vo;
	}

	public List<LinkmanVo> getLinkmanVos(List<String> linkmanIds) {
		List<LinkmanVo> linkmanVos = new ArrayList<LinkmanVo>();
		for (String linkmanId : linkmanIds) {
			LinkmanVo vo = new LinkmanVo();
			LinkmanEntity entity = linkmanDao.getById(linkmanId);
			vo.setCreateTime(entity.getCreateTime());
			vo.setEmail(entity.getEmail());
			vo.setId(entity.getId());
			vo.setPhoneNumber(entity.getPhoneNumber());
			vo.setUserName(entity.getUserName());

			linkmanVos.add(vo);
		}

		return linkmanVos;
	}

}
