package com.gcloud.mesh.alert.entity;

import com.gcloud.framework.db.jdbc.annotation.ID;
import com.gcloud.framework.db.jdbc.annotation.Table;

import lombok.Data;

@Data
@Table(name = "alert_templates")
public class NotifyTemplateEntity {
	
	@ID
    private String id;
    private String name;
    private String title;
    private String content;
    private String recoverTitle;
    private String recoverContent;
}
