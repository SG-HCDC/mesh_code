package com.gcloud.mesh.alert.proxy;

import java.util.Map;

import org.springframework.stereotype.Service;

import com.gcloud.mesh.header.vo.alert.HistoryVo;
import com.gcloud.mesh.header.vo.monitor.StatisticsResourceVo;

@Service("HostAlarmProxyImpl")
public class HostAlarmProxyImpl implements IAlarmProxy {

	@Override
	public void setAlarmHistory4ImportData(HistoryVo alarmHistoryVo, StatisticsResourceVo statistics) {
		alarmHistoryVo.setHost(statistics.getHost());
		alarmHistoryVo.setResourceName(statistics.getResourceName());
		alarmHistoryVo.setResourceInstance(statistics.getInstance());
		alarmHistoryVo.setResourceId(statistics.getResourceId());
	}

	@Override
	public void setTemplatesParams(Map<String, String> params, StatisticsResourceVo statistics) {
		params.put("host", statistics.getHost());
    	params.put("instance", statistics.getInstance());
	}

	@Override
	public void setRecoverTemplatesParams(Map<String, String> params, StatisticsResourceVo statistics) {
		params.put("host", statistics.getHost());
    	params.put("instance", statistics.getInstance());
	}
	
}
