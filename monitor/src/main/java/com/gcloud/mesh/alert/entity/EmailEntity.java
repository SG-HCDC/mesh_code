package com.gcloud.mesh.alert.entity;

import com.gcloud.framework.db.jdbc.annotation.ID;
import com.gcloud.framework.db.jdbc.annotation.Table;

import lombok.Data;

@Data
@Table(name = "alert_email")
public class EmailEntity {

	@ID
	private String id;
	private String emailServer;
	private String emailProtocol;
	private String emailSenderAccount;
	private String emailSenderPasswd;
	private String emailSenderName;
}
