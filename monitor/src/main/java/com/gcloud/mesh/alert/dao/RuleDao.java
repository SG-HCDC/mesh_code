package com.gcloud.mesh.alert.dao;

import org.springframework.stereotype.Repository;

import com.gcloud.framework.db.dao.impl.JdbcBaseDaoImpl;
import com.gcloud.mesh.alert.entity.RuleEntity;

@Repository
public class RuleDao extends JdbcBaseDaoImpl<RuleEntity, String> {

}
