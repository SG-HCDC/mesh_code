package com.gcloud.mesh.alert.entity;

import java.util.Date;

import com.gcloud.framework.db.jdbc.annotation.ID;
import com.gcloud.framework.db.jdbc.annotation.Table;

import lombok.Data;

@Data
@Table(name = "alert_linkman")
public class LinkmanEntity {

    @ID
    private String id;
    private String userName;
    private String email;
    private String phoneNumber;
    private Date createTime;
}