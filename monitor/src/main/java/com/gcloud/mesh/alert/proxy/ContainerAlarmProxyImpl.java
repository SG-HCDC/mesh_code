package com.gcloud.mesh.alert.proxy;

import java.util.Map;

import org.springframework.stereotype.Service;

import com.gcloud.mesh.header.vo.alert.HistoryVo;
import com.gcloud.mesh.header.vo.monitor.StatisticsResourceVo;

@Service("ContainerAlarmProxyImpl")
public class ContainerAlarmProxyImpl implements IAlarmProxy {

	@Override
	public void setAlarmHistory4ImportData(HistoryVo alarmHistoryVo, StatisticsResourceVo statistics) {
		alarmHistoryVo.setResourceId(statistics.getResourceId());
		alarmHistoryVo.setResourceName(statistics.getResourceName());
	}

	@Override
	public void setTemplatesParams(Map<String, String> params, StatisticsResourceVo statistics) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setRecoverTemplatesParams(Map<String, String> params, StatisticsResourceVo statistics) {
		// TODO Auto-generated method stub
		
	}

}
