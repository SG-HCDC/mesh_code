package org.jeecg.modules.monitor.controller;

import java.util.HashMap;
import java.util.Map;

import org.jeecg.common.api.vo.Result;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
@RequestMapping("/actuator")
public class ActuatorHealthController {
    @GetMapping("/health")
    public Object health() throws Exception {
        Map<String, String> map = new HashMap<>();
        map.put("status", "UP");
        return map;
    }


  	
}
