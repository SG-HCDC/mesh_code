package org.jeecg.modules.system.filter;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class IscSSOFilter extends MyIscSSOAuthenticationFilter {

	private static List<String> exceptUri = new ArrayList<>();

	static {
		exceptUri.add("/mesh-controller/isc/isc_server_url");
		exceptUri.add("/mesh-controller/isc/permissions_compare");
		exceptUri.add("/mesh-controller/webservice/iscImsService");
		exceptUri.add("/mesh-controller/sys/logout");
		exceptUri.add("/mesh-controller/cas/client/getCurrentUserName");
		exceptUri.add("/mesh-controller/cas/client/validateLogin");
		
		
		// TODO
//		exceptUri.add("/mesh-controller/dcEnergy/reportByYear");
//		exceptUri.add("/mesh-controller/dcEnergy/reportByQuarter");
//		exceptUri.add("/mesh-controller/dcEnergy/reportByMonth");
	}

	public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
		HttpServletRequest request = (HttpServletRequest) servletRequest;
		HttpServletResponse response = (HttpServletResponse) servletResponse;

		String url = request.getRequestURI();
		if (!exceptUri.contains(url)) {
			super.doFilter(servletRequest, servletResponse, filterChain);
		} else {
			filterChain.doFilter(servletRequest, servletResponse);
		}

//		if ("/mesh-controller/cas/client/validateLogin".equals(url) && response.getStatus() == 302) {
//			response.setStatus(200);
//			response.sendRedirect(null);
//			log.info("/mesh-controller/cas/client/validateLogin response.getStatus():" + response.getStatus() + "  url:" + url);
//		}
		
		log.debug("response.getStatus():" + response.getStatus() + "  url:" + url);
	}
}
