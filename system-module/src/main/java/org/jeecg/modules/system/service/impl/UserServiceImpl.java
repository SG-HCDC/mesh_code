package org.jeecg.modules.system.service.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.jeecg.common.api.vo.Result;
import org.jeecg.common.constant.CacheConstant;
import org.jeecg.common.constant.CommonConstant;
import org.jeecg.common.constant.enums.SysLogOperateType;
import org.jeecg.common.constant.enums.SysLogType;
import org.jeecg.common.system.api.ISysBaseAPI;
import org.jeecg.common.system.util.JwtUtil;
import org.jeecg.common.system.vo.LoginUser;
import org.jeecg.common.system.vo.SysUserCacheInfo;
import org.jeecg.common.util.PasswordUtil;
import org.jeecg.common.util.RedisUtil;
import org.jeecg.common.util.UUIDGenerator;
import org.jeecg.common.util.oConvertUtils;
import org.jeecg.modules.system.entity.SysDepart;
import org.jeecg.modules.system.entity.SysDepartRole;
import org.jeecg.modules.system.entity.SysDepartRoleUser;
import org.jeecg.modules.system.entity.SysPermission;
import org.jeecg.modules.system.entity.SysRole;
import org.jeecg.modules.system.entity.SysUser;
import org.jeecg.modules.system.entity.SysUserDepart;
import org.jeecg.modules.system.entity.SysUserRole;
import org.jeecg.modules.system.mapper.SysDepartMapper;
import org.jeecg.modules.system.mapper.SysDepartRoleMapper;
import org.jeecg.modules.system.mapper.SysDepartRoleUserMapper;
import org.jeecg.modules.system.mapper.SysPermissionMapper;
import org.jeecg.modules.system.mapper.SysRoleMapper;
import org.jeecg.modules.system.mapper.SysUserDepartMapper;
import org.jeecg.modules.system.mapper.SysUserMapper;
import org.jeecg.modules.system.mapper.SysUserRoleMapper;
import org.jeecg.modules.system.model.SysLoginModel;
import org.jeecg.modules.system.model.SysUserSysDepartModel;
import org.jeecg.modules.system.service.ISysDepartService;
import org.jeecg.modules.system.service.ISysDictService;
import org.jeecg.modules.system.service.IUserService;
import org.jeecg.modules.system.vo.SysUserDepVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.sgcc.isc.core.orm.identity.User;

import lombok.extern.slf4j.Slf4j;

/**
 * <p>
 * 用户表 服务实现类
 * </p>
 *
 * @Author: scott
 * @Date: 2018-12-20
 */
@Service
@Slf4j
@ConditionalOnProperty(name = "mesh.login.auth-impl", havingValue = "local")
public class UserServiceImpl extends ServiceImpl<SysUserMapper, SysUser> implements IUserService {

	@Autowired
	private SysUserMapper userMapper;
	@Autowired
	private SysPermissionMapper sysPermissionMapper;
	@Autowired
	private SysUserRoleMapper sysUserRoleMapper;
	@Autowired
	private SysUserDepartMapper sysUserDepartMapper;
	@Autowired
	private ISysBaseAPI sysBaseAPI;
	@Autowired
	private SysDepartMapper sysDepartMapper;
	@Autowired
	private SysRoleMapper sysRoleMapper;
	@Autowired
	private SysDepartRoleUserMapper departRoleUserMapper;
	@Autowired
	private SysDepartRoleMapper sysDepartRoleMapper;
	@Autowired
	private IUserService sysUserService;
	@Autowired
	private ISysDepartService sysDepartService;
	@Autowired
	private RedisUtil redisUtil;
	@Autowired
	private ISysDictService sysDictService;

	@Override
	public Result<JSONObject> login(HttpServletRequest request, HttpServletResponse response, SysLoginModel sysLoginModel) {
		log.info("UserServiceImpl 登录方式");
		Result<JSONObject> result;
		String username = sysLoginModel.getUsername();
		String password = sysLoginModel.getPassword();
		// update-begin--Author:scott Date:20190805 for：暂时注释掉密码加密逻辑，有点问题
		// 前端密码加密，后端进行密码解密
		// password =
		// AesEncryptUtil.desEncrypt(sysLoginModel.getPassword().replaceAll("%2B",
		// "\\+")).trim();//密码解密
		// update-begin--Author:scott Date:20190805 for：暂时注释掉密码加密逻辑，有点问题

		// update-begin-author:taoyan date:20190828 for:校验验证码

		/**
		 * String captcha = sysLoginModel.getCaptcha(); if(captcha==null){
		 * result.error500("验证码无效"); return result; }
		 * 
		 * 
		 * String lowerCaseCaptcha = captcha.toLowerCase(); String realKey =
		 * MD5Util.MD5Encode(lowerCaseCaptcha+sysLoginModel.getCheckKey(),
		 * "utf-8"); Object checkCode = redisUtil.get(realKey);
		 * if(checkCode==null || !checkCode.equals(lowerCaseCaptcha)) {
		 * result.error500("验证码错误"); return result; }
		 **/
		// update-end-author:taoyan date:20190828 for:校验验证码

		// 1. 校验用户是否有效
		// update-begin-author:wangshuai date:20200601 for:
		// 登录代码验证用户是否注销bug，if条件永远为false
		LambdaQueryWrapper<SysUser> queryWrapper = new LambdaQueryWrapper<>();
		queryWrapper.eq(SysUser::getUsername, username);
		SysUser sysUser = sysUserService.getOne(queryWrapper);
		// update-end-author:wangshuai date:20200601 for:
		// 登录代码验证用户是否注销bug，if条件永远为false
		result = sysUserService.checkUserIsEffective(sysUser);
		if (!result.isSuccess()) {
			return result;
		}

		// 2. 校验用户名或密码是否正确
		String userpassword = PasswordUtil.encrypt(username, password, sysUser.getSalt());
		String syspassword = sysUser.getPassword();
		if (!syspassword.equals(userpassword)) {
			result.error500("用户名或密码错误");
			return result;
		}

		// 用户登录信息
		userInfo(sysUser, result);
		sysBaseAPI.addLog("用户名: " + username + ",登录成功！", SysLogType.LOGIN, SysLogOperateType.LOGIN, sysUser.getId());

		return result;
	}

	@Override
	public Result<JSONObject> userInfo(SysUser sysUser, Result<JSONObject> result) {
		String syspassword = sysUser.getPassword();
		String username = sysUser.getUsername();
		// 生成token
		String token = JwtUtil.sign(username, syspassword);
		// 设置token缓存有效时间
		redisUtil.set(CommonConstant.PREFIX_USER_TOKEN +username+"_"+token, token);
		redisUtil.expire(CommonConstant.PREFIX_USER_TOKEN + username+"_"+token, JwtUtil.EXPIRE_TIME * 2 / 1000);

		// 获取用户部门信息
		JSONObject obj = new JSONObject();
		List<SysDepart> departs = sysDepartService.queryUserDeparts(sysUser.getId());
		obj.put("departs", departs);
		if (departs == null || departs.size() == 0) {
			obj.put("multi_depart", 0);
		} else if (departs.size() == 1) {
			sysUserService.updateUserDepart(username, departs.get(0).getOrgCode());
			obj.put("multi_depart", 1);
		} else {
			obj.put("multi_depart", 2);
		}
		obj.put("token", token);
		obj.put("userInfo", sysUser);
		obj.put("sysAllDictItems", sysDictService.queryAllDictItems());
		result.setResult(obj);
		result.success("登录成功");
		return result;
	}

	@Override
	public Result<JSONObject> logout(LoginUser sysUser) {
		return null;
	}

	@Override
	@CacheEvict(value = { CacheConstant.SYS_USERS_CACHE }, allEntries = true)
	public Result<?> resetPassword(String username, String oldpassword, String newpassword, String confirmpassword) {
		SysUser user = userMapper.getUserByName(username);
		String passwordEncode = PasswordUtil.encrypt(username, oldpassword, user.getSalt());
		if (!user.getPassword().equals(passwordEncode)) {
			return Result.error("旧密码输入错误!");
		}
		if (oConvertUtils.isEmpty(newpassword)) {
			return Result.error("新密码不允许为空!");
		}
		if (!newpassword.equals(confirmpassword)) {
			return Result.error("两次输入密码不一致!");
		}
		String password = PasswordUtil.encrypt(username, newpassword, user.getSalt());
		this.userMapper.update(new SysUser().setPassword(password), new LambdaQueryWrapper<SysUser>().eq(SysUser::getId, user.getId()));
		return Result.ok("密码重置成功!");
	}

	@Override
	@CacheEvict(value = { CacheConstant.SYS_USERS_CACHE }, allEntries = true)
	public Result<?> changePassword(SysUser sysUser) {
		String salt = oConvertUtils.randomGen(8);
		sysUser.setSalt(salt);
		String password = sysUser.getPassword();
		String passwordEncode = PasswordUtil.encrypt(sysUser.getUsername(), password, salt);
		sysUser.setPassword(passwordEncode);
		this.userMapper.updateById(sysUser);
		return Result.ok("密码修改成功!");
	}

	@Override
	@CacheEvict(value = { CacheConstant.SYS_USERS_CACHE }, allEntries = true)
	@Transactional(rollbackFor = Exception.class)
	public boolean deleteUser(String userId) {
		// 1.删除用户
		this.removeById(userId);
		return false;
	}

	@Override
	@CacheEvict(value = { CacheConstant.SYS_USERS_CACHE }, allEntries = true)
	@Transactional(rollbackFor = Exception.class)
	public boolean deleteBatchUsers(String userIds) {
		// 1.删除用户
		this.removeByIds(Arrays.asList(userIds.split(",")));
		return false;
	}

	@Override
	public SysUser getUserByName(String username) {
		return userMapper.getUserByName(username);
	}

	@Override
	@Transactional
	public void addUserWithRole(SysUser user, String roles) {
		this.save(user);
		if (oConvertUtils.isNotEmpty(roles)) {
			String[] arr = roles.split(",");
			for (String roleId : arr) {
				SysUserRole userRole = new SysUserRole(user.getId(), roleId);
				sysUserRoleMapper.insert(userRole);
			}
		}
	}

	@Override
	@CacheEvict(value = { CacheConstant.SYS_USERS_CACHE }, allEntries = true)
	@Transactional
	public void editUserWithRole(SysUser user, String roles) {
		this.updateById(user);
		// 先删后加
		sysUserRoleMapper.delete(new QueryWrapper<SysUserRole>().lambda().eq(SysUserRole::getUserId, user.getId()));
		if (oConvertUtils.isNotEmpty(roles)) {
			String[] arr = roles.split(",");
			for (String roleId : arr) {
				SysUserRole userRole = new SysUserRole(user.getId(), roleId);
				sysUserRoleMapper.insert(userRole);
			}
		}
	}

	@Override
	public List<String> getRole(String username) {
		return sysUserRoleMapper.getRoleByUserName(username);
	}

	/**
	 * 通过用户名获取用户角色集合
	 * 
	 * @param username
	 *            用户名
	 * @return 角色集合
	 */
	@Override
	public Set<String> getUserRolesSet(String username) {
		// 查询用户拥有的角色集合
		List<String> roles = sysUserRoleMapper.getRoleByUserName(username);
		log.info("-------通过数据库读取用户拥有的角色Rules------username： " + username + ",Roles size: " + (roles == null ? 0 : roles.size()));
		return new HashSet<>(roles);
	}

	/**
	 * 通过用户名获取用户权限集合
	 *
	 * @param username
	 *            用户名
	 * @return 权限集合
	 */
	@Override
	public Set<String> getUserPermissionsSet(String username) {
		Set<String> permissionSet = new HashSet<>();
		List<SysPermission> permissionList = sysPermissionMapper.queryByUser(username);
		for (SysPermission po : permissionList) {
			// // TODO URL规则有问题？
			// if (oConvertUtils.isNotEmpty(po.getUrl())) {
			// permissionSet.add(po.getUrl());
			// }
			if (oConvertUtils.isNotEmpty(po.getPerms())) {
				permissionSet.add(po.getPerms());
			}
		}
		log.info("-------通过数据库读取用户拥有的权限Perms------username： " + username + ",Perms size: " + (permissionSet == null ? 0 : permissionSet.size()));
		return permissionSet;
	}

	@Override
	public SysUserCacheInfo getCacheUser(String username) {
		SysUserCacheInfo info = new SysUserCacheInfo();
		info.setOneDepart(true);
		// SysUser user = userMapper.getUserByName(username);
		// info.setSysUserCode(user.getUsername());
		// info.setSysUserName(user.getRealname());

		LoginUser user = sysBaseAPI.getUserByName(username);
		if (user != null) {
			info.setSysUserCode(user.getUsername());
			info.setSysUserName(user.getRealname());
			info.setSysOrgCode(user.getOrgCode());
		}

		// 多部门支持in查询
		List<SysDepart> list = sysDepartMapper.queryUserDeparts(user.getId());
		List<String> sysMultiOrgCode = new ArrayList<String>();
		if (list == null || list.size() == 0) {
			// 当前用户无部门
			// sysMultiOrgCode.add("0");
		} else if (list.size() == 1) {
			sysMultiOrgCode.add(list.get(0).getOrgCode());
		} else {
			info.setOneDepart(false);
			for (SysDepart dpt : list) {
				sysMultiOrgCode.add(dpt.getOrgCode());
			}
		}
		info.setSysMultiOrgCode(sysMultiOrgCode);

		return info;
	}

	// 根据部门Id查询
	@Override
	public IPage<SysUser> getUserByDepId(Page<SysUser> page, String departId, String username) {
		return userMapper.getUserByDepId(page, departId, username);
	}

	@Override
	public IPage<SysUser> getUserByDepIds(Page<SysUser> page, List<String> departIds, String username) {
		return userMapper.getUserByDepIds(page, departIds, username);
	}

	@Override
	public Map<String, String> getDepNamesByUserIds(List<String> userIds) {
		List<SysUserDepVo> list = this.baseMapper.getDepNamesByUserIds(userIds);

		Map<String, String> res = new HashMap<String, String>();
		list.forEach(item -> {
			if (res.get(item.getUserId()) == null) {
				res.put(item.getUserId(), item.getDepartName());
			} else {
				res.put(item.getUserId(), res.get(item.getUserId()) + "," + item.getDepartName());
			}
		});
		return res;
	}

	@Override
	public IPage<SysUser> getUserByDepartIdAndQueryWrapper(Page<SysUser> page, String departId, QueryWrapper<SysUser> queryWrapper) {
		LambdaQueryWrapper<SysUser> lambdaQueryWrapper = queryWrapper.lambda();

		lambdaQueryWrapper.eq(SysUser::getDelFlag, "0");
		lambdaQueryWrapper.inSql(SysUser::getId, "SELECT user_id FROM sys_user_depart WHERE dep_id = '" + departId + "'");

		return userMapper.selectPage(page, lambdaQueryWrapper);
	}

	@Override
	public IPage<SysUserSysDepartModel> queryUserByOrgCode(String orgCode, SysUser userParams, IPage page) {
		List<SysUserSysDepartModel> list = baseMapper.getUserByOrgCode(page, orgCode, userParams);
		Integer total = baseMapper.getUserByOrgCodeTotal(orgCode, userParams);

		IPage<SysUserSysDepartModel> result = new Page<>(page.getCurrent(), page.getSize(), total);
		result.setRecords(list);

		return result;
	}

	// 根据角色Id查询
	@Override
	public IPage<SysUser> getUserByRoleId(Page<SysUser> page, String roleId, String username) {
		return userMapper.getUserByRoleId(page, roleId, username);
	}

	@Override
	@CacheEvict(value = { CacheConstant.SYS_USERS_CACHE }, key = "#username")
	public void updateUserDepart(String username, String orgCode) {
		baseMapper.updateUserDepart(username, orgCode);
	}

	@Override
	public SysUser getUserByPhone(String phone) {
		return userMapper.getUserByPhone(phone);
	}

	@Override
	public SysUser getUserByEmail(String email) {
		return userMapper.getUserByEmail(email);
	}

	@Override
	@Transactional
	public void addUserWithDepart(SysUser user, String selectedParts) {
		// this.save(user); //保存角色的时候已经添加过一次了
		if (oConvertUtils.isNotEmpty(selectedParts)) {
			String[] arr = selectedParts.split(",");
			for (String deaprtId : arr) {
				SysUserDepart userDeaprt = new SysUserDepart(user.getId(), deaprtId);
				sysUserDepartMapper.insert(userDeaprt);
			}
		}
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	@CacheEvict(value = { CacheConstant.SYS_USERS_CACHE }, allEntries = true)
	public void editUserWithDepart(SysUser user, String departs) {
		this.updateById(user); // 更新角色的时候已经更新了一次了，可以再跟新一次
		String[] arr = {};
		if (oConvertUtils.isNotEmpty(departs)) {
			arr = departs.split(",");
		}
		// 查询已关联部门
		List<SysUserDepart> userDepartList = sysUserDepartMapper.selectList(new QueryWrapper<SysUserDepart>().lambda().eq(SysUserDepart::getUserId, user.getId()));
		if (userDepartList != null && userDepartList.size() > 0) {
			for (SysUserDepart depart : userDepartList) {
				// 修改已关联部门删除部门用户角色关系
				if (!Arrays.asList(arr).contains(depart.getDepId())) {
					List<SysDepartRole> sysDepartRoleList = sysDepartRoleMapper.selectList(new QueryWrapper<SysDepartRole>().lambda().eq(SysDepartRole::getDepartId, depart.getDepId()));
					List<String> roleIds = sysDepartRoleList.stream().map(SysDepartRole::getId).collect(Collectors.toList());
					if (roleIds != null && roleIds.size() > 0) {
						departRoleUserMapper.delete(new QueryWrapper<SysDepartRoleUser>().lambda().eq(SysDepartRoleUser::getUserId, user.getId()).in(SysDepartRoleUser::getDroleId, roleIds));
					}
				}
			}
		}
		// 先删后加
		sysUserDepartMapper.delete(new QueryWrapper<SysUserDepart>().lambda().eq(SysUserDepart::getUserId, user.getId()));
		if (oConvertUtils.isNotEmpty(departs)) {
			for (String departId : arr) {
				SysUserDepart userDepart = new SysUserDepart(user.getId(), departId);
				sysUserDepartMapper.insert(userDepart);
			}
		}
	}

	/**
	 * 校验用户是否有效
	 * 
	 * @param sysUser
	 * @return
	 */
	@Override
	public Result<?> checkUserIsEffective(SysUser sysUser) {
		Result<?> result = new Result<Object>();
		// 情况1：根据用户信息查询，该用户不存在
		if (sysUser == null) {
			result.error500("该用户不存在，请注册");
			sysBaseAPI.addLog("用户登录失败，用户不存在！", CommonConstant.LOG_TYPE_LOGIN, null);
			return result;
		}
		// 根据用户信息查询，该用户已注销
		// update-begin---author:王帅 Date:20200601
		// for：if条件永远为falsebug------------
		if (CommonConstant.DEL_FLAG_1 == sysUser.getDelFlag()) {
			// update-end---author:王帅 Date:20200601
			// for：if条件永远为falsebug------------
			sysBaseAPI.addLog("用户登录失败，用户名:" + sysUser.getUsername() + "已注销！", CommonConstant.LOG_TYPE_LOGIN, null);
			result.error500("该用户已注销");
			return result;
		}
		// 根据用户信息查询，该用户已禁用
		if (CommonConstant.USER_FREEZE.equals(sysUser.getStatus())) {
			sysBaseAPI.addLog("用户登录失败，用户名:" + sysUser.getUsername() + "已禁用！", CommonConstant.LOG_TYPE_LOGIN, null);
			result.error500("该用户已禁用");
			return result;
		}
		// 用户密码已过期
		if (CommonConstant.PASSWORD_EXPIRED.equals(sysUser.getStatus())) {
			sysBaseAPI.addLog("用户登录失败，用户名:" + sysUser.getUsername() + "密码已过期！", CommonConstant.LOG_TYPE_LOGIN, null);
			result.error500("该用户密码已过期");
			return result;
		}
		// 用户已过期
		if (CommonConstant.USER_EXPIRED.equals(sysUser.getStatus())) {
			sysBaseAPI.addLog("用户登录失败，用户名:" + sysUser.getUsername() + "用户已过期！", CommonConstant.LOG_TYPE_LOGIN, null);
			result.error500("该用户已过期");
		}
		return result;
	}

	@Override
	public List<SysUser> queryLogicDeleted() {
		return this.queryLogicDeleted(null);
	}

	@Override
	public List<SysUser> queryLogicDeleted(LambdaQueryWrapper<SysUser> wrapper) {
		if (wrapper == null) {
			wrapper = new LambdaQueryWrapper<>();
		}
		wrapper.eq(SysUser::getDelFlag, "1");
		return userMapper.selectLogicDeleted(wrapper);
	}

	@Override
	public boolean revertLogicDeleted(List<String> userIds, SysUser updateEntity) {
		String ids = String.format("'%s'", String.join("','", userIds));
		return userMapper.revertLogicDeleted(ids, updateEntity) > 0;
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public boolean removeLogicDeleted(List<String> userIds) {
		String ids = String.format("'%s'", String.join("','", userIds));
		// 1. 删除用户
		int line = userMapper.deleteLogicDeleted(ids);
		// 2. 删除用户部门关系
		line += sysUserDepartMapper.delete(new LambdaQueryWrapper<SysUserDepart>().in(SysUserDepart::getUserId, userIds));
		// 3. 删除用户角色关系
		line += sysUserRoleMapper.delete(new LambdaQueryWrapper<SysUserRole>().in(SysUserRole::getUserId, userIds));
		return line != 0;
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public boolean updateNullPhoneEmail() {
		userMapper.updateNullByEmptyString("email");
		userMapper.updateNullByEmptyString("phone");
		return true;
	}

	@Override
	public void saveThirdUser(SysUser sysUser) {
		// 保存用户
		String userid = UUIDGenerator.generate();
		sysUser.setId(userid);
		baseMapper.insert(sysUser);
		// 获取第三方角色
		SysRole sysRole = sysRoleMapper.selectOne(new LambdaQueryWrapper<SysRole>().eq(SysRole::getRoleCode, "third_role"));
		// 保存用户角色
		SysUserRole userRole = new SysUserRole();
		userRole.setRoleId(sysRole.getId());
		userRole.setUserId(userid);
		sysUserRoleMapper.insert(userRole);
	}

	@Override
	public List<SysUser> queryByDepIds(List<String> departIds, String username) {
		return userMapper.queryByDepIds(departIds, username);
	}

	@Override
	public void maintainSysUserRole(List<SysRole> sysRoles, String userId) {
		// TODO Auto-generated method stub
	}

	@Override
	public void maintainSysRolePermission(List<SysRole> sysRoles) {
		// TODO Auto-generated method stub

	}

	@Override
	public List<SysRole> maintainSysRole(SysUser sysUser) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public SysUser maintainSysUser(User userDb, Result<?> result) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public SysUser syncUser(String userId, String username) {
		// TODO Auto-generated method stub
		return null;
	}

}
