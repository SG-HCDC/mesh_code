package org.jeecg.modules.system.vo;

import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.hibernate.validator.constraints.Length;
import org.jeecg.common.constant.enums.SysLogOperateType;
import org.jeecg.common.constant.enums.SysLogType;

import com.gcloud.mesh.header.exception.CommonErrorCode;
import com.gcloud.mesh.header.exception.SpecialCode;

import lombok.Data;

@Data
public class SysLogStatisticsReq {

	@Length( min = 1, max = 10, message = "::维护人员"+CommonErrorCode.LENGTH10)
	@SpecialCode(message = "::维护人员"+CommonErrorCode.SPECIAL)
	private String username;

	private String startTime;

	private String endTime;
	
	private Integer operateType;
	
	private List<Integer> excludeOperateType;

	@Override
	public String toString() {
		StringBuffer buffer = new StringBuffer();
		buffer.append("【查询条件：");
		StringBuffer condition = new StringBuffer();
		if(operateType != null ) {
			Integer operType = operateType;
			SysLogOperateType operateType = SysLogOperateType.getByType(operType);
			condition.append("操作类型："+operateType.getName()+",");
		}
		if(StringUtils.isNotBlank(username)) {
			condition.append("维护人员："+username+",");
		}
		String con = condition.toString();
		if(con.endsWith(",")) {
			con = condition.substring(0,con.length()-1);
		}
		buffer.append(con);
		buffer.append("】");
		return buffer.toString();
	}
	
	
	
	
}
