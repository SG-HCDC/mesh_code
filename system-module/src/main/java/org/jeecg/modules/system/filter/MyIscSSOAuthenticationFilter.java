package org.jeecg.modules.system.filter;


import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;

import com.sgcc.isc.ualogin.client.util.ISCAuthRouter;

public class MyIscSSOAuthenticationFilter extends MyAuthenticationFilter
{
  protected void initInternal(FilterConfig filterConfig)
    throws ServletException
  {
    String iscSSOServerLoginUrl = getPropertyFromInitParams(filterConfig, "serverLoginUrl", null);
    super.setCasServerLoginUrl(iscSSOServerLoginUrl);

    boolean iscSSOrenew = Boolean.parseBoolean(getPropertyFromInitParams(filterConfig, "renew", null));
    super.setRenew(iscSSOrenew);

    boolean iscSSOgateway = Boolean.parseBoolean(getPropertyFromInitParams(filterConfig, "gateway", null));
    super.setGateway(iscSSOgateway);

    String iscSSOserverName = getPropertyFromInitParams(filterConfig, "serverName", null);
    super.setServerName(iscSSOserverName);
  }

  protected boolean routeToLocal(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain)
    throws IOException, ServletException
  {
    ISCAuthRouter router = ISCAuthRouter.getRouter();
    return router.isRouteToLocal(servletRequest, servletResponse, filterChain);
  }
}