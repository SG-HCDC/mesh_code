package org.jeecg.modules.system.vo;

import java.util.List;

import javax.validation.constraints.Pattern;

import org.apache.commons.lang3.StringUtils;
import org.hibernate.validator.constraints.Length;
import org.jeecg.common.constant.enums.SysLogOperateType;
import org.jeecg.common.constant.enums.SysLogType;

import com.gcloud.mesh.header.constant.RegexpPattern;
import com.gcloud.mesh.header.exception.CommonErrorCode;
import com.gcloud.mesh.header.exception.SpecialCode;

import lombok.Data;

@Data
public class SysLogPageReq {

	private String title;
	
	@Length( min = 1, max = 10, message = "::服务名称"+CommonErrorCode.LENGTH10)
	@SpecialCode(message = "::服务名称"+CommonErrorCode.SPECIAL)
	private String logContent;
	
	@Pattern( regexp = RegexpPattern.IP, message = "::IP格式错误")
	private String ip;
	
	private List<Integer> logType;
	
	private List<Integer> operateType;
	
	private Integer eventType;
	
	private List<Integer> excludeOperateType;
	
	private List<String> roleCode;

	private String startTime;
	private String endTime;
	
	@Length( min = 1, max = 10, message = "::维护人员"+CommonErrorCode.LENGTH10)
	@SpecialCode(message = "::维护人员"+CommonErrorCode.SPECIAL)
	private String username;
	
	private Integer status;
	private Integer orderType;
	
	private Integer isAq = 1;
	
	@Override
	public String toString() {
		StringBuffer buffer = new StringBuffer();
//		buffer.append("【日志查询条件参数：");
//		StringBuffer condition = new StringBuffer();
//		if(ip != null) {
//			condition.append("IP："+ip+"，");
//		}
//		if(logType == null) {
//			condition.append("日志类型："+"全部"+"，");
//		}else if(logType != null && logType.size() > 1) {
//			condition.append("日志类型："+"全部"+"，");
//		}else if(logType != null && logType.size() > 0) {
//			Integer type = logType.get(0);
//			SysLogType logtype = SysLogType.getByType(type);
//			condition.append("日志类型："+logtype.getName()+"，");
//		}
//		if(StringUtils.isNotBlank(startTime)) {
//			condition.append("开始时间："+startTime+"，");
//		}
//		if(StringUtils.isNotBlank(endTime)) {
//			condition.append("结束时间："+endTime+"，");
//		}
//		if(operateType != null  && operateType.size() > 1) {
//			condition.append("操作类型："+"全部"+"，");
//		}else if(operateType != null && operateType.size() > 0) {
//			Integer operType = operateType.get(0);
//			SysLogOperateType operateType = SysLogOperateType.getByType(operType);
//			condition.append("操作类型："+operateType.getName()+"，");
//		}
//		if(StringUtils.isNotBlank(username)) {
//			condition.append("用户名："+username+"，");
//		}
//		String con = condition.toString();
//		if(con.endsWith("，")) {
//			con = condition.substring(0,con.length()-1);
//		}
//		buffer.append(con);
//		buffer.append("】");
		if(orderType != null) {
			String orderStr = "";
			switch(orderType){
		    case 0 :
		       orderStr = "【时间降序】";
		       break; 
		    case 1 :
		       //语句
		       orderStr = "【时间升序】";
		       break; //可选
		    case 2 :
			       //语句
			       orderStr = "【IP降序】";
			       break; //可选
		    case 3 :
			       //语句
			       orderStr = "【IP升序】";
			       break; //可选
		    case 4 :
			       //语句
			       orderStr = "【用户名降序】";
			       break; //可选
		    case 5 :
			       //语句
			       orderStr = "【用户名升序】";
			       break; //可选
		    case 6 :
			       //语句
			       orderStr = "【操作类型升序】";
			       break; //可选
		    case 7 :
			       //语句
			       orderStr = "【操作类型降序】";
			       break; //可选
		    default : //可选
			       //语句
			}
			buffer.append(orderStr);
		}
		return buffer.toString();
		
	}
	
	
}
