package org.jeecg.modules.system.vo;

import java.util.List;

import lombok.Data;

@Data
public class SysLogOperateTypeReq {

	private Integer logType;
	private Boolean isSystem;
	private List<Integer> excludeOperateType;
	
	private List<Integer> includeOperateType;
	

}
