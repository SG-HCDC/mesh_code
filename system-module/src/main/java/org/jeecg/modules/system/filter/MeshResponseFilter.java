package org.jeecg.modules.system.filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class MeshResponseFilter implements Filter {

	public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {

		HttpServletRequest request = (HttpServletRequest) servletRequest;
		HttpServletResponse response = (HttpServletResponse) servletResponse;

		long start = System.currentTimeMillis();
		filterChain.doFilter(servletRequest, servletResponse);
		long time = System.currentTimeMillis() - start;
		if (time > 1000) {
			log.info(String.format("[api_response_time_warn]  api:%sms   time:%s", request.getRequestURL().toString(), time));
		}
	}

}
