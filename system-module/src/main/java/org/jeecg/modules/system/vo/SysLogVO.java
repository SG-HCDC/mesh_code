package org.jeecg.modules.system.vo;

import org.jeecg.common.constant.enums.SysLogStatus;
import org.jeecg.common.constant.enums.SysLogType;
import org.jeecg.modules.system.entity.SysLog;

import lombok.Data;

@Data
public class SysLogVO extends SysLog {
	private static final long serialVersionUID = 1L;

	private String statusName;

	private String logTypeName;

	private Integer count = 1;

	public String getStatusName() {
		SysLogStatus status = SysLogStatus.getByType(this.getStatus());
		if (status != null) {
			return status.getName();
		} else {
			return null;
		}
	}

	public String getLogTypeName() {
		SysLogType type = SysLogType.getByType(this.getLogType());
		if (type != null) {
			return type.getName();
		} else {
			return null;
		}
	}

}
