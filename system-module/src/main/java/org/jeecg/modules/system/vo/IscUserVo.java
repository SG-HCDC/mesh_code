package org.jeecg.modules.system.vo;

import lombok.Data;
import org.jeecg.modules.system.model.ResExtModel;

import java.util.Date;
@Data
public class IscUserVo {
    private ResExtModel resExt;
    private String id;
    private String baseOrgId;
    private String name;
    private String userName;
    private String state;
    private String unicode;
    private String mobile;
    private String email;
    private String isCached;
    private String dispOrder;
    private String config;
    private String dataId;
    private String sysMark;
    private Date startUsefulLife;
    private Date endUsefulLife;
    private String spell;
    private String isSyncToSap;
    private String sapHrCode;
    private String employFormId;
    private String employNatureId;
    private String employCategoryId;
    private String title;
    private String maritalStatusId;
    private Date birthDate;
    private String birthPlace;
    private String sexCode;
    private String givenName;
    private String formerName;
    private String homePostalAddress;
    private String homePostalCode;
    private String homePhone;
    private String postalAddress;
    private String postalCode;
    private String fax;
    private String telephoneNumber;
    private String privateIdentityId;
    private String nationCode;
    private String remark;
    private String nativePlace;
    private String orgAnisationName;
    private String photo;
    private String password;

}
