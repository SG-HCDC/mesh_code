package org.jeecg.modules.system.vo;

import lombok.Data;

@Data
public class SysLogOperateTypeVO {

	private String name;

	private Integer value;
	
	private Integer eventType; 

}
