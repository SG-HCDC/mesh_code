package org.jeecg.modules.system.mapper;

import org.jeecg.modules.system.entity.SysLog;
import org.jeecg.modules.system.entity.SysLogBackup;
import org.jeecg.modules.system.vo.SysLogPageReq;
import org.jeecg.modules.system.vo.SysLogVO;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;

/**
 * <p>
 * 系统日志表 Mapper 接口
 * </p>
 *
 * @Author zhangweijian
 * @since 2018-12-26
 */
public interface SysLogBackupMapper extends BaseMapper<SysLogBackup> {

	/**
	 * @功能：清空所有日志记录
	 */
	void removeAll();

	IPage<SysLogVO> queryPage(Page<SysLog> page, SysLogPageReq sysLogPageReq, String userId);
}
