package org.jeecg.modules.system.vo;

import org.jeecg.common.constant.enums.SysLogOperateType;

import lombok.Data;

@Data
public class SysLogColumnarStatisticsVO {

	private Integer operateType;

	private String operateTypeName;

	private Integer total;

	private Integer success;

	private Integer fail;

	public String getOperateTypeName ( ) {
		SysLogOperateType type = SysLogOperateType.getByType(this.getOperateType());
		if (type != null) {
			return type.getName();
		} else {
			return null;
		}
	}
	
}
