package org.jeecg.modules.system.util;

import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.jeecg.common.cache.LogModuleCache;
import org.jeecg.common.constant.DictCode;
import org.jeecg.common.constant.enums.SysLogConfig;
import org.jeecg.common.constant.enums.SysLogOperateType;
import org.jeecg.common.system.vo.DictModel;
import org.jeecg.common.util.DateUtils;
import org.jeecg.common.util.SpringContextUtils;
import org.jeecg.modules.system.entity.SysLog;
import org.jeecg.modules.system.mapper.SysDictMapper;
import org.springframework.web.bind.annotation.RequestMapping;

public class LogUtil {

	public static boolean checkLogConfig(SysLog sysLog) {
		SysDictMapper sysDictMapper = SpringContextUtils.getBean(SysDictMapper.class);
		List<DictModel> configValue = sysDictMapper.queryDictItemsByCode(DictCode.LOG_CONFIG.getCode());
		Map<String, String> valueMap = new HashMap<>();
		if (valueMap != null) {
			for (DictModel dm : configValue) {
				valueMap.put(dm.getText(), dm.getValue());
			}
		}

		// 查询日志开关
		String listSwitch = valueMap.get(SysLogConfig.LIST_SWITCH.getName());
		if ("0".equals(listSwitch) && (sysLog.getOperateType().equals(SysLogOperateType.QUERY.getType()))) {
			return false;
		}

		// 查看日志开关
		String detailSwitch = valueMap.get(SysLogConfig.DETAIL_SWITCH.getName());
		if ("0".equals(detailSwitch) && sysLog.getOperateType().equals(SysLogOperateType.DETAIL.getType())) {
			return false;
		}

		// 新增日志开关
		String addSwitch = valueMap.get(SysLogConfig.ADD_SWITCH.getName());
		if ("0".equals(addSwitch) && sysLog.getOperateType().equals(SysLogOperateType.ADD.getType())) {
			return false;
		}

		// 删除日志开关
		String delelteSwitch = valueMap.get(SysLogConfig.DELETE_SWITCH.getName());
		if ("0".equals(delelteSwitch) && sysLog.getOperateType().equals(SysLogOperateType.DELETE.getType())) {
			return false;
		}

		// 修改日志开关
		String updateSwitch = valueMap.get(SysLogConfig.UPDATE_SWITCH.getName());
		if ("0".equals(updateSwitch) && sysLog.getOperateType().equals(SysLogOperateType.UPDATE.getType())) {
			return false;
		}

		// 应用查询开关
		String appQuerySwitch = valueMap.get(SysLogConfig.APPQUERY_SWITCH.getName());
		if (("0".equals(appQuerySwitch) && "查询应用列表".equals(sysLog.getLogContent()))) {
			return false;
		}
		return true;
	}

	public static void handleUrlAndModule(Method method, String module, SysLog sysLog) {
		// 新增URL
		if (method != null) {
			Class<?> controllerClass = method.getDeclaringClass();
			RequestMapping requestController = controllerClass.getAnnotation(RequestMapping.class);
			if (requestController != null) {
	
				RequestMapping requestMapper = method.getAnnotation(RequestMapping.class);
				if (requestMapper != null) {
					String url = "";
					if (requestMapper.value()[0].startsWith("/")) {
						url = requestController.value()[0] + requestMapper.value()[0];
					} else {
						url = requestController.value()[0] + "/" + requestMapper.value()[0];
					}
					sysLog.setRequestUrl(url);
				}
			}
		}

		// 新增模块
		if (StringUtils.isNotBlank(module)) {
			sysLog.setModule(module);
		} else {
			if (StringUtils.isNotBlank(sysLog.getUserid())) {
				Map<String, String> map = LogModuleCache.getByUserId(sysLog.getUserid());
				if (map == null) {
					map = IscUtil.getLogModule(sysLog.getUserid());
					if (map != null) {
						LogModuleCache.put(sysLog.getUserid(), map);
					}
				}

				if (map != null) {
					if (StringUtils.isNotBlank(sysLog.getRequestUrl())) {
						sysLog.setModule(map.get(sysLog.getRequestUrl()));
					}
				}
			}
		}
	}

	public static String handleLogContent(SysLog sysLog) {
		Integer operateLog = sysLog.getOperateType();
		SysLogOperateType sysLogOperateType = null;
		if (operateLog != null) {
			sysLogOperateType = SysLogOperateType.getByType(operateLog);
		}
		if (SysLogOperateType.UNAUTHORIZED.getType().equals(sysLog.getOperateType())) {
			return String.format("【%s(%s)】于%s 在尝试越权操作【%s】，操作结果为【%s】", sysLog.getUsername(), sysLog.getUserid(), DateUtils.formatTime(sysLog.getCreateTime().getTime()), sysLog.getLogContent(), sysLog.getStatus() == 1 ? "成功" : "失败");
		} else if (SysLogOperateType.RUN.getType().equals(sysLog.getOperateType())) {
			return sysLog.getLogContent();
		} else if (SysLogOperateType.LOGIN.getType().equals(sysLog.getOperateType())) {
			return String.format("【%s(%s)】于%s 登录系统，操作类型为【%s】，操作结果为【%s】，操作内容为【登录系统】", sysLog.getUsername(), sysLog.getUserid(), DateUtils.formatTime(sysLog.getCreateTime().getTime()), sysLogOperateType != null ? sysLogOperateType.getName() : "未知操作", sysLog.getStatus() == 1 ? "成功" : "失败");
		} else if (SysLogOperateType.LOGOUT.getType().equals(sysLog.getOperateType())) {
			return String.format("【%s(%s)】于%s 退出系统，操作类型为【%s】，操作结果为【%s】，操作内容为【退出系统】", sysLog.getUsername(), sysLog.getUserid(), DateUtils.formatTime(sysLog.getCreateTime().getTime()), sysLogOperateType != null ? sysLogOperateType.getName() : "未知操作", sysLog.getStatus() == 1 ? "成功" : "失败");
		} else if(SysLogOperateType.DB_DEBUG.getType().equals(sysLog.getOperateType()) || SysLogOperateType.CONFIG_DEBUG.getType().equals(sysLog.getOperateType()) || SysLogOperateType.PROCEDURE_DEBUG.getType().equals(sysLog.getOperateType())) {
			return String.format("%s", sysLog.getLogContent());
		}else {
			return String.format("【%s(%s)】于%s在【%s】进行了操作，操作类型为【%s】，操作结果为【%s】，操作内容为【%s】", sysLog.getUsername(), sysLog.getUserid(), DateUtils.formatTime(sysLog.getCreateTime().getTime()), sysLog.getModule(), sysLogOperateType != null ? sysLogOperateType.getName() : "未知操作", sysLog.getStatus() == 1 ? "成功" : "失败", sysLog.getLogContent());
		}
	}
	

}
