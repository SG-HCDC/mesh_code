package org.jeecg.modules.system.vo;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;

import org.hibernate.validator.constraints.Range;

import lombok.Data;

@Data
public class SysLogConfigReq {

	private Integer listSwitch;
	
	private Integer detailSwitch;
	
	private Integer addSwitch;
	
	private Integer updateSwitch;
	
	private Integer deleteSwitch;
	
	private Integer sqlSwitch;
	
	private Integer appQuerySwitch;
	@Max(value = 100, message = "::容量大小范围： 1～100")
	@Min(value = 1, message = "::容量大小范围： 1～100")
	// @Range(min = 1, max= 100, message = "容量大小范围： 1～100")
	private Integer capacity;

	@Override
	public String toString() {
		StringBuffer buff = new StringBuffer();
		buff.append("设置日志开关参数:");
		if(listSwitch != null ) {
			String s = listSwitch == 1?"开启":"关闭";
			buff.append(" 查询日志开关=").append(s).append(",");
		}
		if(detailSwitch != null ) {
			String s = detailSwitch == 1?"开启":"关闭";
			buff.append(" 查看日志开关=").append(s).append(",");
		}
		if(addSwitch != null ) {
			String s = addSwitch == 1?"开启":"关闭";
			buff.append(" 新增日志开关=").append(s).append(",");
		}
		if(updateSwitch != null ) {
			String s = updateSwitch == 1?"开启":"关闭";
			buff.append(" 更新日志开关=").append(s).append(",");
		}
		if(deleteSwitch != null ) {
			String s = deleteSwitch == 1?"开启":"关闭";
			buff.append(" 删除日志开关=").append(s).append(",");
		}
		if(appQuerySwitch != null ) {
			String s = appQuerySwitch == 1?"开启":"关闭";
			buff.append(" 应用查询日志开关=").append(s).append(",");
		}
		if(capacity != null) {
			buff.append(" 日志容量=").append(capacity).append("GB").append(",");
		}
		String con = buff.toString();
		if(con.endsWith(",")) {
			con = con.substring(0,con.length()-1);
		}
		return con;
	}
	
	
	

}
