package org.jeecg.modules.system.filter;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;

import com.sgcc.isc.ualogin.client.util.ISCAuthRouter;

public class MyIscSSO20ProxyReceivingTicketValidationFilter extends MyCas20ProxyReceivingTicketValidationFilter {
	
	protected boolean preFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
		ISCAuthRouter router = ISCAuthRouter.getRouter();
		if (router.isRouteToLocal(servletRequest, servletResponse, filterChain))
			return false;
		return super.preFilter(servletRequest, servletResponse, filterChain);
	}
}
 