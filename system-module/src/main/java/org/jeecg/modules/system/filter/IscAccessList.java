package org.jeecg.modules.system.filter;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class IscAccessList {
	@Value("${mesh.login.accessUrls:/linkman/update}")
	private String urls;

	public String getUrls() {
		return urls;
	}

	public void setUrls(String urls) {
		this.urls = urls;
	}
	public List<String> getUrlList(){
		List<String> list = new ArrayList<>();
		if(urls != null && urls.contains(",")) {
			String[] ss = urls.split(",");
			for(String s : ss) {
				list.add(s);
			}
		}else {
			list.add(urls);
		}
		return list;
	}
	
}
