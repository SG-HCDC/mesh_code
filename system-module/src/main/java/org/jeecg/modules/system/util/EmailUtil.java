package org.jeecg.modules.system.util;

import java.util.Date;

import org.apache.commons.lang.StringUtils;
import org.jeecg.modules.message.entity.SysMessage;
import org.jeecg.modules.message.handle.enums.SendMsgStatusEnum;
import org.jeecg.modules.message.service.ISysMessageService;
import org.jeecg.modules.system.mapper.SysUserMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class EmailUtil {

	@Autowired
	private ISysMessageService sysMessageService;

	@Autowired
	private SysUserMapper sysUserMapper;

	public Boolean sendEmail(String title, String content) {

		String sentTo = sysUserMapper.getLinkmanEmail();
		if (StringUtils.isNotBlank(sentTo)) {
			log.info(String.format("告警联系人: %s, title: %s, content: %s ", sentTo, title, content));
			SysMessage sysMessage = new SysMessage();
			sysMessage.setEsType("2");
			sysMessage.setEsReceiver(sentTo);
			sysMessage.setEsTitle(title);
			sysMessage.setEsContent(content);
			sysMessage.setEsSendTime(new Date());
			sysMessage.setEsSendStatus(SendMsgStatusEnum.WAIT.getCode());
			sysMessage.setEsSendNum(0);
			if (sysMessageService.save(sysMessage)) {
				return true;
			}
		} else {
			log.error(String.format("没有设置告警联系人, title: %s, content: %s ", title, content));
		}

		return false;
	}

}