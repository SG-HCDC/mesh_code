package org.jeecg.modules.shiro.authc;

import org.apache.shiro.authz.AuthorizationException;
import org.apache.shiro.authz.ModularRealmAuthorizer;
import org.apache.shiro.authz.UnauthorizedException;
import org.apache.shiro.subject.PrincipalCollection;
import org.springframework.stereotype.Component;

import lombok.extern.slf4j.Slf4j;

/**
 * @Description: 用户登录鉴权和获取用户授权
 * @Author: Scott
 * @Date: 2019-4-23 8:13
 * @Version: 1.1
 */
@Component
@Slf4j
public class ShiroModularRealmAuthorizer extends ModularRealmAuthorizer {

	public void checkPermission(PrincipalCollection principals, String permission) throws AuthorizationException {
		log.info("=====================checkPermissionZzj start");
        assertRealmsConfigured();
        if (!isPermitted(principals, permission)) {
            throw new UnauthorizedException("Subject does not have permission [" + permission + "]");
        }
        log.info("=====================checkPermissionZzj end");
    }

}
