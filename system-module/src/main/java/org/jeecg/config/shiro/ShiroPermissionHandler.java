package org.jeecg.config.shiro;

import com.alibaba.fastjson.JSON;
import lombok.extern.slf4j.Slf4j;
import org.apache.shiro.authz.AuthorizationException;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.apache.shiro.authz.aop.PermissionAnnotationHandler;

import java.lang.annotation.Annotation;

@Slf4j
public class ShiroPermissionHandler extends PermissionAnnotationHandler {
    public ShiroPermissionHandler() {
        super();
        log.info("================开启【跳过shiro权限验证模式】================");
    }

    /**
     * 重写权限认证方法,仅仅打印log,不做拦截处理
     *
     * @param a 注解
     * @throws AuthorizationException 一个不可能抛出的异常
     */
    @Override
    public void assertAuthorized(Annotation a) throws AuthorizationException {
        if (!(a instanceof RequiresPermissions)) return;
        log.warn("警告!!   跳过了权限:{}", JSON.toJSONString(getAnnotationValue(a)));
    }
}
