package org.jeecg.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class LogSingleConfig {
	@Value("${mesh.loginSingle:false}")
	private Boolean loginSingle;

	public Boolean getLoginSingle() {
		return loginSingle;
	}

	public void setLoginSingle(Boolean loginSingle) {
		this.loginSingle = loginSingle;
	}
	
}
