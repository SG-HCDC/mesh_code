package org.jeecg.config;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;

import org.jeecg.modules.system.filter.IscAccessInterceptFilter;
import org.jeecg.modules.system.filter.IscSSOFilter;
import org.jeecg.modules.system.filter.MeshResponseFilter;
import org.jeecg.modules.system.filter.MyIscSSO20ProxyReceivingTicketValidationFilter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.boot.web.servlet.ServletContextInitializer;
import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.sgcc.isc.ualogin.client.filter.IscSSOHttpServletRequestWrapperFilter;
import com.sgcc.isc.ualogin.client.filter.IscSingleSignOutFilter;
import com.sgcc.isc.ualogin.client.filter.IsoSSOAssertionThreadLocalFilter;
import com.sgcc.isc.ualogin.client.servlet.IscSingleSignOutServlet;

@Configuration
@ConditionalOnProperty(prefix = "mesh.login",name = "auth-impl",havingValue = "isc")
public class IscConfig {

	private static String iscServer;
	private static String iscFilterServer;
	private static String iscAppId;
	private static String serverName;
	private static String iscUsernamePrefix;

	public static String getIscAppId() {
		return iscAppId;
	}

	@Value("${mesh.login.isc-appId}")
	public void setIscAppId(String iscAppId) {
		IscConfig.iscAppId = iscAppId;
	}

	public static String getIscUsernamePrefix() {
		return iscUsernamePrefix;
	}

	@Value("${mesh.login.isc-username-prefix}")
	public void setIscUsernamePrefix(String iscUsernamePrefix) {
		IscConfig.iscUsernamePrefix = iscUsernamePrefix;
	}
	
	@Value("${mesh.login.isc-filter-server}")
	public void setIscFilterServer(String iscFilterServer) {
		IscConfig.iscFilterServer = iscFilterServer;
	}

	@Value("${mesh.login.isc-server}")
	public void setIscServer(String iscServer) {
		IscConfig.iscServer = iscServer;
	}

	@Value("${mesh.login.serverName}")
	public void setServerName(String serverName) {
		IscConfig.serverName = serverName;
	}

	public static String getServerName() {
		return serverName;
	}

	public static String getIscServer() {
		return iscServer;
	}
	
	public static String getIscFilterServer() {
		return iscFilterServer;
	}

	public static String getIscSsoPath() {
		return iscServer + "/isc_sso";
	}

	public static String getIscSsoLoginPath() {
		return iscServer + "/isc_sso/login";
	}

	/**
	 * context-param
	 * 
	 * @return
	 */
	@Bean
	public ServletContextInitializer initializer() {
		return new ServletContextInitializer() {
			@Override
			public void onStartup(ServletContext servletContext) throws ServletException {
				servletContext.setInitParameter("appid", IscConfig.getIscAppId());
				servletContext.setInitParameter("ssoServerUrl", IscConfig.getIscSsoPath());
			}
		};
	}

	@Bean
	public FilterRegistrationBean meshResponseFilter() {
		final FilterRegistrationBean registration = new FilterRegistrationBean();
		registration.setFilter(new MeshResponseFilter());
		// 设定匹配的路径
		registration.addUrlPatterns("/*");
		// 设定加载的顺序
		registration.setOrder(8);
		return registration;
	}

	@Bean
	public FilterRegistrationBean filterSingleRegistration() {
		final FilterRegistrationBean registration = new FilterRegistrationBean();
		registration.setFilter(new IscSingleSignOutFilter());
		// 设定匹配的路径
		registration.addUrlPatterns("/*");
		// 设定加载的顺序
		registration.setOrder(2);
		return registration;
	}

	/**
	 * 配置授权过滤器
	 * 
	 * @return
	 */
	@Bean
	public FilterRegistrationBean filterAuthenticationRegistration() {
		final FilterRegistrationBean registration = new FilterRegistrationBean();
		registration.setFilter(new IscSSOFilter());
		// 设定匹配的路径
		registration.addUrlPatterns("/*");
		Map<String, String> initParameters = new HashMap<String, String>();
		initParameters.put("serverLoginUrl", IscConfig.getIscSsoLoginPath());
		initParameters.put("serverName", serverName);

		registration.setInitParameters(initParameters);
		// 设定加载的顺序
		registration.setOrder(3);
		return registration;
	}

	/**
	 * 配置过滤验证器 这里用的是IscSSO20ProxyReceivingTicketValidationFilter
	 * 
	 * @return
	 */
	@Bean
	public FilterRegistrationBean filterValidationRegistration() {
		final FilterRegistrationBean registration = new FilterRegistrationBean();
		registration.setFilter(new MyIscSSO20ProxyReceivingTicketValidationFilter());
		// 设定匹配的路径
		registration.addUrlPatterns("/*");
		Map<String, String> initParameters = new HashMap<String, String>();
		initParameters.put("serverUrlPrefix", IscConfig.getIscSsoPath());
		initParameters.put("serverName", serverName);
		
		registration.setInitParameters(initParameters);
		// 设定加载的顺序
		registration.setOrder(4);
		return registration;
	}

	/**
	 * request wraper过滤器
	 * 
	 * @return
	 */
	@Bean
	public FilterRegistrationBean filterWrapperRegistration() {
		final FilterRegistrationBean registration = new FilterRegistrationBean();
		registration.setFilter(new IscSSOHttpServletRequestWrapperFilter());
		// 设定匹配的路径
		registration.addUrlPatterns("/*");
		// 设定加载的顺序
		registration.setOrder(5);
		return registration;
	}

	/**
	 * request wraper过滤器
	 * 
	 * @return
	 */
	@Bean
	public FilterRegistrationBean assertionThreadLocalFilter() {
		final FilterRegistrationBean registration = new FilterRegistrationBean();
		registration.setFilter(new IsoSSOAssertionThreadLocalFilter());
		// 设定匹配的路径
		registration.addUrlPatterns("/*");
		// 设定加载的顺序
		registration.setOrder(6);
		return registration;
	}

	/**
	 * request 越权过滤器
	 * 
	 * @return
	 */
	@Bean
	public FilterRegistrationBean accessInterceptFilter() {
		final FilterRegistrationBean registration = new FilterRegistrationBean();
		registration.setFilter(new IscAccessInterceptFilter());

		Map<String, String> initParameters = new HashMap<String, String>();
		initParameters.put("iscFilterServer", IscConfig.getIscFilterServer() + "/isc_frontmv_serv");
		initParameters.put("iscKickBackUrl", IscConfig.getServerName());
		initParameters.put("excludeSuffix", "js,css,jar,jpg,gif,png,htm,as,ico,swf,eot,xsd");
		initParameters.put("exclude", "/model/vm_detail,/model/k8s_detail,/model/migrate_vm,/model/migrate_k8s,/model/latestSamples,/model/detailNode,/model/detailDevice,/model/powerRate,/model/setStrategy");
		registration.setInitParameters(initParameters);

		// 设定匹配的路径
		List<String> url = new ArrayList<>();
		url.add("/history/*");
		url.add("/linkman/*");
		url.add("/policy/*");
		url.add("/rule/*");
		url.add("/analysis/*");
		url.add("/subhealth/*");
		url.add("/asset/*");
		url.add("/cloud_resource/*");
		url.add("/app/*");
		url.add("/profile/*");
		url.add("/authority/*");
		url.add("/dcEnergy/*");
		url.add("/dataClassification/*");
		url.add("/dataClean/*");
		url.add("/dataSecurity/*");
		url.add("/model/*");
		url.add("/scheduler/*");
		url.add("/step/*");
		url.add("/fm/*");
		url.add("/statistics/*");
		url.add("/supplier/*");
		url.add("/sys/log/*");
		
		registration.addUrlPatterns(url.toArray(new String[url.size()]));
		// 设定加载的顺序
		registration.setOrder(7);
		return registration;
	}

	// @Bean
	public ServletRegistrationBean servletRegistrationBean() {
		IscSingleSignOutServlet servlet = new IscSingleSignOutServlet();
		ServletRegistrationBean bean = new ServletRegistrationBean(servlet);
		bean.setOrder(7);
		bean.setName("iscSingleSignOutServlet");
		List<String> urlPattern = new ArrayList<>();
		urlPattern.add("/signout");
		bean.setUrlMappings(urlPattern);

		Map<String, String> initParameters = new HashMap<String, String>();
		initParameters.put("serverLogoutUrl", IscConfig.getIscSsoPath() + "/logout");
		initParameters.put("serverName", serverName);
		bean.setInitParameters(initParameters);
		return bean;
	}
	/**
	 * 注册拦截器
	 */
	

}
