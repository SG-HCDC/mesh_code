package com.gcloud.mesh.header.exception;

public class DataClassificationErrorCode {
	
	public static final String LEVEL_NOT_BLANK = "090101";
	public static final String LEVEL_FORMAT_ERROR = "090102";
	public static final String PERIOD_NOT_BLANK = "090103";
	public static final String PERIOD_FORMAT_ERROR = "090104";
}
