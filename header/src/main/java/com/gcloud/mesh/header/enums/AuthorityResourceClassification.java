package com.gcloud.mesh.header.enums;

import java.util.stream.Stream;

public enum AuthorityResourceClassification {
	
	IAAS("iaas", "基础设施"),
	IT_DEVICE("device", "IT设备"),
	CLOUD("cloud", "云平台资源"),
	APP("app", "应用资源");

	
	private String name;
	private String cnName;
	
	AuthorityResourceClassification(String name, String cnName) {
		this.name = name;
		this.cnName = cnName;
	}
	
	public static AuthorityResourceClassification getByName(String name) {
		return Stream.of(AuthorityResourceClassification.values())
				.filter( s -> s.getName().equals(name))
				.findFirst()
				.orElse(null);
	}

	public String getName() {
		return name;
	}

	public String getCnName() {
		return cnName;
	}
	
	

}
