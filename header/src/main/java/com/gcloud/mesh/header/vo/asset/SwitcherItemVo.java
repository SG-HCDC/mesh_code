package com.gcloud.mesh.header.vo.asset;

import java.util.Date;

import lombok.Data;

@Data
public class SwitcherItemVo {

	private String id;
	private String name;
	private String esn;
	private Integer type;
	private String typeCnName;
	private String deviceId;
	private String deviceModel;
	private String deviceManufacturer;
	private String datacenterId;
	private String datacenterName;
	private String datacenterIp;
	private String creator;
	private Date createTime;
	private String ip;
	private String community;
	private Integer status;
	private String statusCnName;
	private String from;
	private String cabinetId;
}
