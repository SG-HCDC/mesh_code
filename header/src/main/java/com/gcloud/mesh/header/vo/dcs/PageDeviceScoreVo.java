package com.gcloud.mesh.header.vo.dcs;

import lombok.Data;

@Data
public class PageDeviceScoreVo {
	private String id;
	private String name;
	private String datacenterId;
	private String datacenterName;
	private Integer costScore;
	private Integer deviceScore;
	private Boolean isolation;
	private Float energyEfficiencyRatio;
}
