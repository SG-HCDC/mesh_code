package com.gcloud.mesh.header.msg.analysis;

import com.gcloud.mesh.header.exception.AnalysisErrorCode;
import com.gcloud.mesh.header.msg.BaseMsg;
import javax.validation.constraints.NotBlank;

import org.apache.commons.lang3.StringUtils;

import lombok.Data;

@Data
public class OptimumMsg extends BaseMsg {
	
//	@NotBlank( message = AnalysisErrorCode.DATACENTERID_NOT_NULL)
	private String datacenterId;

	@Override
	public String toString() {
		StringBuffer buffer = new StringBuffer();
		if(StringUtils.isNotBlank(datacenterId)) {
			buffer.append(" 数据中心ID："+datacenterId);
		}
		return buffer.toString();
	}
	
	

}
