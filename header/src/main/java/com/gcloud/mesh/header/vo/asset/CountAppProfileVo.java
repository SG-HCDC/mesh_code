package com.gcloud.mesh.header.vo.asset;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class CountAppProfileVo {

	@ApiModelProperty(value = "应用总数")
	private int total;
	
	@ApiModelProperty(value = "人工智能型")
	private int aiNum;
	
	@ApiModelProperty(value = "CPU密集型")
	private int cpuNum;
	
	@ApiModelProperty(value = "IO密集型")
	private int ioNum;
	
	@ApiModelProperty(value = "其他")
	private int unknownNum;
	
	@ApiModelProperty(value = "离线型")
	private int offlineNum;
	
	@ApiModelProperty(value = "时序型")
	private int sequentialNum;
	
	@ApiModelProperty(value = "实时型")
	private int realtimeNum;
}
