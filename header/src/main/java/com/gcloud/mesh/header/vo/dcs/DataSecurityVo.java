package com.gcloud.mesh.header.vo.dcs;

import lombok.Data;

@Data
public class DataSecurityVo {

    private String policy;
    private String policyCnName;
    private String frequency;
    private String frequencyCnName;
    private String time;
    private Boolean enabled;

}
