package com.gcloud.mesh.header.vo.dcs;

import java.util.List;

import lombok.Data;

@Data
public class ListCountTrendVo {
	
	private List<StatisticTypeVo> generals;
	private List<StatisticTypeVo> vitals;

}
