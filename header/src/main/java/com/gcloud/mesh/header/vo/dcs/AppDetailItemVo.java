
package com.gcloud.mesh.header.vo.dcs;

import java.util.Date;
import java.util.List;

import lombok.Data;

@Data
public class AppDetailItemVo {

	private String id;
	private String name;
	private String cloudResourceId;
	private String cloudResourceName;
	private String typeName;
	private Integer type;
	private Date createTime;
	private List<AppInstanceVo> instances;
	
	
}
