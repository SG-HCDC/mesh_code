
package com.gcloud.mesh.header.msg.dvfs;

import lombok.Data;

@Data
public class DvfsVo {

    private String id;
    private String hostname;
    private String driver;
    private String availableStrategy;
    private String currentStrategy;
    private Float frequencyLowerLimit;
    private Float frequencyUpperLimit;
    private Float currentFrequency;

}
