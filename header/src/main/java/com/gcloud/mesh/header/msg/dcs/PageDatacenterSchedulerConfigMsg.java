
package com.gcloud.mesh.header.msg.dcs;

import com.gcloud.mesh.header.msg.BaseQueryMsg;

import lombok.Data;

@Data
public class PageDatacenterSchedulerConfigMsg extends BaseQueryMsg {

	private String name;
	private String datacenterId;
	
}
