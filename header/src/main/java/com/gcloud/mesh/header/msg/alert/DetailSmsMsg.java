package com.gcloud.mesh.header.msg.alert;

import com.gcloud.mesh.header.msg.BaseMsg;

import lombok.Data;

@Data
public class DetailSmsMsg extends BaseMsg {
	private String platform;
}
