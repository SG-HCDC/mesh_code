package com.gcloud.mesh.header.vo.supplier;

import lombok.Data;

@Data
public class YingZeAssetItemVo {

	private String DeviceID;
	private String name;
	private String comment;
	private String LibName;
	private String ComPort;
	private String Address;
}
