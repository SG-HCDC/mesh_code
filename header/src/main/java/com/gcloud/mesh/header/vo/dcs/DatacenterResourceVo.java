package com.gcloud.mesh.header.vo.dcs;

import lombok.Data;

@Data
public class DatacenterResourceVo {
	private String datacenterId;
	private String datacenterName;
	private String cloudResourceId;
	private String cloudResourceName;
}
