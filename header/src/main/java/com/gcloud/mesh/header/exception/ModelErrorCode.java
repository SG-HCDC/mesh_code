package com.gcloud.mesh.header.exception;

public class ModelErrorCode {

	public static final String MODEL_TYPE_NOT_BLANK = "130101";
	public static final String MODEL_TYPE_FORMAT_ERROR = "130102";
	public static final String FACTORS_NOT_NULL = "130103";
	public static final String FACTORS_INCOMPLETE = "130104";
	
	public static final String WEIGHT_SUM_LESS_THEN_100 = "130105";
	
	public static final String SCORE_SAMPLE_0_99999 = "130106";
	
	public static final String MODEL_FACTOR_ID_ERROR = "130107";
	
}
