package com.gcloud.mesh.header.vo.dcs;

import lombok.Data;

@Data
public class SchedulerOverview {
	private Integer total;
	private Integer working;
	private Integer finished;
}
