package com.gcloud.mesh.header.msg.dcs;

import com.gcloud.mesh.header.msg.BaseQueryMsg;

import lombok.Data;

@Data
public class PageBackupMsg extends BaseQueryMsg {

	private String name;
	private String policy;
	
}
