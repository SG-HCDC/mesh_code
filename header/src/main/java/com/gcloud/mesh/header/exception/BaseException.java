
package com.gcloud.mesh.header.exception;

/**
 * 异常基础类
 *
 */
@SuppressWarnings("serial")
public class BaseException extends RuntimeException {

    private String errorCode;///错误代码
    private String errorMsg;///错误信息

    public BaseException() {
        super();
    }

    public BaseException(String errorCode) {
        super(errorCode);
        this.errorCode = errorCode;
    }

    public BaseException(String errorCode, String errorMsg) {
        super(errorCode);
        this.errorCode = errorCode;
        this.errorMsg = errorMsg;
    }

    public BaseException(Throwable throwable) {
        super(throwable);
    }

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public String getErrorMsg() {
        return errorMsg;
    }

    public void setErrorMsg(String errorMsg) {
        this.errorMsg = errorMsg;
    }

}
