package com.gcloud.mesh.header.enums;

import java.util.stream.Stream;

public enum DataSecurityFrequency {
	
	DALIY("daily", "* * ?", "每天一次"),	
	WEEKLY("weekly", "? * 1", "每周一次"),
	MONTHLY("monthly", "1 * ?", "每月一次");
	
	private String name;
	private String cron;
	private String cnName;
	
	DataSecurityFrequency(String name, String cron, String cnName) {
		this.name = name;
		this.cron = cron;
		this.cnName = name;
	}
	
	public static DataSecurityFrequency getByName(String name) {
		return Stream.of(DataSecurityFrequency.values())
				.filter( s -> s.getName().equals(name))
				.findFirst()
				.orElse(null);
	}
	
	public static DataSecurityFrequency getByCron(String cron) {
		return Stream.of(DataSecurityFrequency.values())
				.filter( s -> s.getCron().equals(cron))
				.findFirst()
				.orElse(null);
	}
	
	public String getName() {
		return name;
	}
	
	public String getCnName() {
		return cnName;
	}
	
	public String getCron() {
		return cron;
	}

}
