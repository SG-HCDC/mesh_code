
package com.gcloud.mesh.header.msg.dvfs;

import com.gcloud.mesh.header.msg.MqBaseMsg;
import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class ReportDvfsMsg extends MqBaseMsg {

	private String hostname;
    private String driver;
    private String availableStrategy;
    private String currentStrategy;
    private Float frequencyLowerLimit;
    private Float frequencyUpperLimit;
    private Float currentFrequency;

}
