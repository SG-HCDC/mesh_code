package com.gcloud.mesh.header.msg.analysis;

import com.gcloud.mesh.header.msg.BaseMsg;

import lombok.Data;

@Data
public class ListSubhealthThresholdMsg extends BaseMsg {

//	private String resourceId;
	private String type;
}
