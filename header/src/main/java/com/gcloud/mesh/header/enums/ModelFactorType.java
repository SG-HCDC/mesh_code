
package com.gcloud.mesh.header.enums;

import com.gcloud.mesh.header.exception.BaseException;

public enum ModelFactorType {

    POWER_CONSUMPTION, // 设备功耗
    TEMPERATURE, // 设备温度
    POWER_RATING, // 额定功率
    PLANT_RATE, // 设备负荷
    RESPONSE_TIME, // 业务响应时间
    CPU_LOAD, // CPU负载
    MEMORY_PERCENTAGE; // 内存使用率


    public static ModelFactorType checkAndGet(String type) throws BaseException {
        if (type != null) {
            for (ModelFactorType t : ModelFactorType.values()) {
                System.out.println(t.name());
                if (t.name().equals(type)) {
                    return t;
                }
            }
        }
        throw new BaseException("not found");
    }

    public static ModelFactorType get(String type) {
        if (type != null) {
            for (ModelFactorType t : ModelFactorType.values()) {
                System.out.println(t.name());
                if (t.name().equals(type)) {
                    return t;
                }
            }
        }
        return null;
    }

}
