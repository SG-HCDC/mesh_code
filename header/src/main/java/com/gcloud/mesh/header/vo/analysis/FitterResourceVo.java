package com.gcloud.mesh.header.vo.analysis;

import lombok.Data;

@Data
public class FitterResourceVo {

	private String resourceId;
	private String datacenterId;
	
}
