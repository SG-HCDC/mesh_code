
package com.gcloud.mesh.header.msg.dcs;

import com.gcloud.mesh.header.msg.BaseMsg;
import com.gcloud.mesh.header.valid.ValidId;
import com.gcloud.mesh.header.valid.ValidName;

import lombok.Data;

@Data
public class CreateSchedulerJobMsg extends BaseMsg {

    @ValidName
    private String name;
    @ValidId
    private String datacenterId;
    private String type;
    private String resourceIds;
    private String resourceNames;

}
