package com.gcloud.mesh.header.vo.dcs;

import java.util.List;

import lombok.Data;

@Data
public class DataSourceItemVo {
	
    private String database;
//    private String level;
    private List<String> tables; 
    private List<String> excludes;
    
}
