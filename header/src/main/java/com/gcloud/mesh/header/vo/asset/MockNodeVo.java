package com.gcloud.mesh.header.vo.asset;

import java.util.Date;

import lombok.Data;

@Data
public class MockNodeVo {
	
	private String id;
	private String name;
	private String esn;
	private Integer type;
	private String deviceModel;
	private String deviceManufacturer;
	private String datacenterId;
	private String creator;
	private String createTime;
	private String from;
	private String deviceId;
	private String mgmIp;
	private String ipmiIp;
	private String ipmiUser;
	private String ipmiPassword;
	private Integer networkStatus;
	private Integer powerStatus;
	private String hostname;
	private Float ratedPower;
	private Float powerConsumption; 
	private Float energyEfficiencyRatio;
	private Float storageCapacity;
	private Boolean isolation;
	private String ratedPowerSm;
	private String ratedVoltageSm;
	private String smData;

}
