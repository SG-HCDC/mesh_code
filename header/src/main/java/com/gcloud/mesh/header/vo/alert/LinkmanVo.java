package com.gcloud.mesh.header.vo.alert;

import java.util.Date;

import lombok.Data;

@Data
public class LinkmanVo {
	private String id;

	private String userName;

	private String email;

	private String phoneNumber;

	private Date createTime;

}
