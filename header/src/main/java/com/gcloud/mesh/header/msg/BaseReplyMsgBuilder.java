
package com.gcloud.mesh.header.msg;

import com.gcloud.mesh.header.msg.BaseReplyMsg;

public class BaseReplyMsgBuilder {

    public static BaseReplyMsg buildSuccessResult() {
        BaseReplyMsg result = new BaseReplyMsg();
        return result;
    }

    public static BaseReplyMsg buildSuccessResult(Object data) {
        BaseReplyMsg result = new BaseReplyMsg();
        result.setData(data);
        return result;
    }

}
