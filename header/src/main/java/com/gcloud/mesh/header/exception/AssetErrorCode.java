package com.gcloud.mesh.header.exception;

public class AssetErrorCode {

	public static final String NAME_NOT_BLANK = "030101";
	public static final String NAME_FORMAT_ERROR = "030102";
	public static final String NAME_LENGTH_ERROR = "030103";
	public static final String IP_NOT_BLANK = "030104";
	public static final String IP_FORAMT_ERROR = "030105";
	
	public static final String ID_LENGTH_ERROR = "030106";
	public static final String ID_FORMAT_ERROR = "030107";
	public static final String ID_NOT_BLANK = "030108";
	
	public static final String ESN_NOT_BLANK = "030109";
	public static final String ESN_LENGTH_ERROR = "030110";
	public static final String ESN_FORMAT_ERROR = "030111";

	public static final String TYPE_FORMAT_ERROR = "030112";
	
	public static final String MODEL_NOT_BLANK = "030113";
	public static final String MODEL_LENGTH_ERROR = "030114";
	public static final String MODEL_FORMAT_ERROR = "030115";
	
	public static final String MANUFACTURER_NOT_BLANK = "030116";
	public static final String MANUFACTURER_LENGTH_ERROR = "030117";
	public static final String MANUFACTURER_FORMAT_ERROR = "030118";

	public static final String DATACENTER_ID_NOT_BLANK = "030119";
	public static final String DATACENTER_ID_LENGTH_ERROR  = "030120";
	public static final String DATACENTER_ID_FORMAT_ERROR = "030121";
	
	public static final String COMMUNITY_NOT_BLANK = "030122";
	public static final String COMMUNITY_LENGTH_ERROR = "030123";
	
	public static final String MGM_IP_NOT_BLANK = "030124";
	public static final String MGM_IP_FORMAT_ERROR = "030125";

	public static final String IPMI_USER_NOT_BLANK = "030126";
	public static final String IPMI_AUTH_NOT_BLANK = "030127";
	public static final String IPMI_IP_NOT_BLANK = "030128";
	public static final String IPMI_IP_FORMAT_ERROR = "030129";
	public static final String IPMI_USER_FORMAT_ERROR = "030130";
	public static final String IPMI_USER_LENGTH_ERROR = "030131";
	public static final String IPMI_AUTH_LENGTH_ERROR = "030132";
	
	public static final String HOSTNAME_NOT_BLANK = "030133";
	public static final String HOSTNAME_LENGTH_ERROR = "030134";
	public static final String HOSTNAME_FORMAT_ERROR = "030135";
	public static final String THRESHOLD_OUT_RANGE = "030136";
	public static final String ORDER_FORMAT_ERROR = "030137";
	
	public static final String RESOURSE_NOT_EXIST = "030201";
	public static final String RESOURSE_NAME_EXIST = "030202";
	public static final String RESOURSE_TYPE_NOT_EXIST = "030203";
	public static final String RESOURSE_TYPE_ERROR = "030204";
	public static final String DATACENTER_NOT_EXIST = "030205";
	public static final String DATACENTER_IS_USING = "030206";
	public static final String RESOURSE_DB_SAVE = "030207";
	public static final String RESOURSE_DB_UPDATE = "030208";
	public static final String RESOURSE_DB_DELETE = "030209";
	public static final String RESOURSE_NO_AUTH = "030210";
	public static final String RESOURSE_SYNC_NOT_UPDATE = "030211";
	public static final String DEVICE_NAME_EXIST = "030212";
	public static final String NODE_NAME_EXIST = "030213";
	public static final String SWITCHER_NAME_EXIST = "030214";
	
	
}
