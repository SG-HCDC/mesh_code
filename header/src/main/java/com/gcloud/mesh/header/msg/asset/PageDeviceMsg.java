package com.gcloud.mesh.header.msg.asset;

import javax.validation.constraints.Pattern;

import org.hibernate.validator.constraints.Length;
import org.springframework.lang.Nullable;

import com.gcloud.mesh.header.constant.RegexpPattern;
import com.gcloud.mesh.header.exception.AssetErrorCode;
import com.gcloud.mesh.header.exception.CommonErrorCode;
import com.gcloud.mesh.header.exception.SpecialCode;
import com.gcloud.mesh.header.msg.BaseQueryMsg;

import lombok.Data;

@Data
public class PageDeviceMsg extends BaseQueryMsg {
	
	@Nullable
	@Length( min = 1, max = 10, message = "::设备名称"+CommonErrorCode.LENGTH10)
//	@Pattern( regexp = "^[0-9a-zA-Z_]{1,}$", message = AssetErrorCode.NAME_FORMAT_ERROR)
	@SpecialCode(message = "::设备名称"+CommonErrorCode.SPECIAL)
	private String name;
	
	@Nullable
	@Pattern( regexp = RegexpPattern.IP, message = AssetErrorCode.IP_FORAMT_ERROR)
	private String ip;
	
	@Nullable
	private Integer type;

	private String datacenterId;
	
}
