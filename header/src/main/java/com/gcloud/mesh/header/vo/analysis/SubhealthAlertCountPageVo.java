package com.gcloud.mesh.header.vo.analysis;

import java.util.List;

import lombok.Data;

@Data
public class SubhealthAlertCountPageVo {
	
	private List<AlertStatisticVo> alertCounts;
	private List<SubhealthAlertVo> pageResult;
	private int pageNo;
	private int pageSize;
	private int totalCount;

}
