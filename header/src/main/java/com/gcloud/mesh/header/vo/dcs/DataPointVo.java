package com.gcloud.mesh.header.vo.dcs;

import java.util.Date;

import lombok.Data;

@Data
public class DataPointVo {
	
	private String label;
	private Date timestamp;
	private String level;
	private String source;
	private Long count;
	
}

