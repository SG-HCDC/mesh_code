package com.gcloud.mesh.header.param;

import lombok.Data;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;

@Accessors(chain=true)
@Data
public class HuaweiMigrationParam implements Serializable {

    private String projectId;

    private String myJobId;
    private String vmName; // 源 hostname
    private String vmId; //yuan appid
    //原数据中心ID
    @NotBlank(message = "数据中心ID不能为空")
    private String regionId;//原 regionId

    @NotBlank(message = "访问KEY不能为空")
    private String accessKey;
    @NotBlank(message = "访问秘钥不能为空")
    private String secretKey;

    //目标数据中心ID
    private String destRegionId;//目标 regionId
}
