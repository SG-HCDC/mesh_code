package com.gcloud.mesh.header.msg.dcs;

import javax.validation.constraints.NotBlank;

import com.gcloud.mesh.header.exception.AuthorityErrorCode;
import com.gcloud.mesh.header.msg.BaseMsg;

import lombok.Data;

@Data
public class ListByClassificationMsg extends BaseMsg {

	@NotBlank(message = AuthorityErrorCode.CLASSIFICATION_NOT_BLANK)
	private String classification;
	
}
