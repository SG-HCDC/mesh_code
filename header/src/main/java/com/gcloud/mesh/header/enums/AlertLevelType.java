package com.gcloud.mesh.header.enums;

public enum AlertLevelType {
	
	INFO("一般", 1),
	WARNING("重要", 2),
	CRITICAL("严重", 3);
	private String cnName;
	private int level;
	AlertLevelType(String cnName, int level){
		this.cnName = cnName;
		this.level = level;
	}
	public String getCnName() {
		return cnName;
	}
	public void setCnName(String cnName) {
		this.cnName = cnName;
	}
	public int getLevel() {
		return level;
	}
	public void setLevel(int level) {
		this.level = level;
	}
	public static String getCnName(Integer level) {
		if(level == null) {
			return null;
		}
		for(AlertLevelType type : AlertLevelType.values()){
			if(level.equals(type.level)) {
				return type.getCnName();
			}
		}
		return null;
	}
	
}
