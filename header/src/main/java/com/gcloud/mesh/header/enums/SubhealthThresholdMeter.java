package com.gcloud.mesh.header.enums;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public enum SubhealthThresholdMeter {

	AIR_IN_TEMPERATURE("air_in_temperature", "空调回风温度", "°C", SubhealthDeviceType.AIR, MonitorMeter.AIR_IN_TEMPERATURE),
	AIR_OUT_TEMPERATURE("air_out_temperature", "空调出风温度", "°C", SubhealthDeviceType.AIR, MonitorMeter.AIR_OUT_TEMPERATURE),
//	AIR_SETTING_TEMPERATURE("air_setting_temperature", "空调控制温度", SubhealthDeviceType.AIR, MonitorMeter.AIR_SETTING_TEMPERATURE),
	AIR_IN_HUMIDITY("air_in_humidity", "空调回风湿度", "%RH", SubhealthDeviceType.AIR, MonitorMeter.AIR_IN_HUMIDITY),
	AIR_OUT_HUMIDITY("air_out_humidity", "空调出风湿度", "%RH", SubhealthDeviceType.AIR, MonitorMeter.AIR_OUT_HUMIDITY),
	UPS_IN_VOLTAGE("ups_in_voltage", "UPS输入电压", "V", SubhealthDeviceType.UPS, MonitorMeter.UPS_IN_VOLTAGE),
	UPS_OUT_VOLTAGE("ups_out_voltage", "UPS输出电压", "V",SubhealthDeviceType.UPS, MonitorMeter.UPS_OUT_VOLTAGE),
	UPS_TEMPERATURE("ups_temperature", "UPS温度", "°C", SubhealthDeviceType.UPS, MonitorMeter.UPS_TEMPERATURE),
//	DATACENTER_SERVER("datacenter_server", "服务器亚健康数量", SubhealthDeviceType.DATACENTER, null),
	DATACENTER_AIR("datacenter_air", "空调亚健康数量", "台", SubhealthDeviceType.DATACENTER, null),
	DATACENTER_UPS("datacenter_ups", "UPS亚健康数量", "台", SubhealthDeviceType.DATACENTER, null);
	
	private String name;
	private String cnName;
	private String unit;
	private SubhealthDeviceType type;
	private MonitorMeter meter;
	
	SubhealthThresholdMeter(String name, String cnName, String unit, SubhealthDeviceType type, MonitorMeter meter) {
		this.name = name;
		this.cnName = cnName;
		this.unit = unit;
		this.type = type;
		this.meter = meter;
	}
	
	public static SubhealthThresholdMeter getByName(String name) {
		return Stream.of(SubhealthThresholdMeter.values())
				.filter( s -> s.getName().equals(name))
				.findFirst()
				.orElse(null);
	}
	
	public static SubhealthThresholdMeter getByMeter(String meter) {
		if(SubhealthThresholdMeter.DATACENTER_AIR.getName().equals(meter) 
				|| SubhealthThresholdMeter.DATACENTER_UPS.getName().equals(meter) ) {
			return getByName(meter);
		}	
		return Stream.of(SubhealthThresholdMeter.values())
				.filter( s -> {
					if(s.getMeter() != null) {
						return s.getMeter().getMeter().equals(meter);
					}else {
						return false;
					}
				})
				.findFirst()
				.orElse(null);
	}
	
	public static List<SubhealthThresholdMeter> getListByType(SubhealthDeviceType type) {
		return Stream.of(SubhealthThresholdMeter.values())
				.filter( s -> s.getType().equals(type))
				.collect(Collectors.toList());
	}
	
	public static SubhealthThresholdMeter getByNameAndType(SubhealthDeviceType type, String name) {
		List<SubhealthThresholdMeter> thresholdMeters = getListByType(type);
		return thresholdMeters.stream()
				.filter( s -> s.getName().equals(name))
				.findFirst()
				.orElse(null);
	}

	public static SubhealthThresholdMeter getByMeterAndType(SubhealthDeviceType type, String meter) {
		List<SubhealthThresholdMeter> thresholdMeters = getListByType(type);
		if(SubhealthThresholdMeter.DATACENTER_AIR.getName().equals(meter) 
				|| SubhealthThresholdMeter.DATACENTER_UPS.getName().equals(meter) ) {
			return thresholdMeters.stream()
					.filter( s -> s.getName().equals(meter))
					.findFirst()
					.orElse(null);
		}		
		return thresholdMeters.stream()
				.filter( s -> s.getMeter().getMeter().equals(meter))
				.findFirst()
				.orElse(null);
	}
	
	public String getName() {
		return name;
	}

	public String getCnName() {
		return cnName;
	}

	public SubhealthDeviceType getType() {
		return type;
	}

	public MonitorMeter getMeter() {
		return meter;
	}

	public String getUnit() {
		return unit;
	}
 
	
}
