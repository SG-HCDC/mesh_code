package com.gcloud.mesh.header.msg.asset;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;

import org.hibernate.validator.constraints.Length;

import com.gcloud.mesh.header.constant.RegexpPattern;
import com.gcloud.mesh.header.exception.AssetErrorCode;
import com.gcloud.mesh.header.exception.CommonErrorCode;
import com.gcloud.mesh.header.exception.SpecialCode;
import com.gcloud.mesh.header.msg.BaseMsg;

import lombok.Data;

@Data
public class CreateSwitcherMsg extends BaseMsg {
	
	@NotBlank( message = AssetErrorCode.NAME_NOT_BLANK)
	@Length( min = 1, max = 10, message = AssetErrorCode.NAME_LENGTH_ERROR)
	@SpecialCode(message = "::设备名称"+CommonErrorCode.SPECIAL)
//	@Pattern( regexp = "^[0-9a-zA-Z_]{1,}$", message = AssetErrorCode.NAME_FORMAT_ERROR)
	private String name;
	
	@NotBlank( message = AssetErrorCode.ESN_NOT_BLANK)
	@Length( min = 1, max = 10, message = AssetErrorCode.ESN_LENGTH_ERROR)
	@SpecialCode(message = "::设备识别号"+CommonErrorCode.SPECIAL)
//	@Pattern( regexp = RegexpPattern.STAND_ID, message = AssetErrorCode.ESN_FORMAT_ERROR)
	private String esn;
	
	@NotBlank( message = AssetErrorCode.MODEL_NOT_BLANK)
	@Length( min = 1, max = 10, message = AssetErrorCode.MODEL_LENGTH_ERROR)
	@SpecialCode(message = "::设备型号"+CommonErrorCode.SPECIAL)
//	@Pattern( regexp = RegexpPattern.STAND_ID, message = AssetErrorCode.MODEL_FORMAT_ERROR)
	private String deviceModel;
	
	@NotBlank( message = AssetErrorCode.MANUFACTURER_NOT_BLANK)
	@Length( min = 1, max = 10, message = AssetErrorCode.MANUFACTURER_LENGTH_ERROR)
//	@Pattern( regexp = "^[0-9a-zA-Z_]{1,}$", message = AssetErrorCode.MANUFACTURER_FORMAT_ERROR)
	private String deviceManufacturer;
	
	@NotBlank( message = AssetErrorCode.DATACENTER_ID_NOT_BLANK)
	@Length( min = 1, max = 60, message = AssetErrorCode.DATACENTER_ID_LENGTH_ERROR)
	@Pattern( regexp = RegexpPattern.STAND_ID, message = AssetErrorCode.DATACENTER_ID_FORMAT_ERROR)
	private String datacenterId;
	
	@NotBlank( message = AssetErrorCode.IP_NOT_BLANK)
	@Pattern( regexp = RegexpPattern.IP, message = AssetErrorCode.IP_FORAMT_ERROR)
	private String ip;
	
	@NotBlank( message = AssetErrorCode.COMMUNITY_NOT_BLANK)
//	@Length( min = 1, max = 10, message = AssetErrorCode.COMMUNITY_LENGTH_ERROR)
	private String community;
	
	private String cabinetId;
	
	@Override
	public String toString() {
		return " 名称=" + name + " 设备识别号=" + esn + " 设备型号=" + deviceModel
				+ " 设备厂商=" + deviceManufacturer + " ip=" + ip + "";
	}
	
	

}
