package com.gcloud.mesh.header.msg.alert;

import java.util.List;

import com.gcloud.mesh.header.exception.AlertErrorCode;
import com.gcloud.mesh.header.exception.CommonErrorCode;
import com.gcloud.mesh.header.exception.SpecialCode;
import com.gcloud.mesh.header.msg.BaseMsg;
import com.gcloud.mesh.header.vo.alert.RuleVo;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Length;

import lombok.Data;

@Data
public class CreateRuleMsg extends BaseMsg {
	@Length( min = 1, max = 60, message = "::策略ID"+CommonErrorCode.LENGTH60)
	@SpecialCode(message = "::策略ID"+CommonErrorCode.SPECIAL)
    private String policyId;
	private List<RuleVo> ruleVos;
}
