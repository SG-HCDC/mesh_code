
package com.gcloud.mesh.header.msg;

import com.gcloud.mesh.header.vo.RoleVo;

import lombok.Data;

@Data
public class MqBaseMsg extends BaseMsg {

    private RoleVo msgFrom;

    private Object data;

}
