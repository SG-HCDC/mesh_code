package com.gcloud.mesh.header.vo.dcs;

import java.util.Date;

import lombok.Data;
@Data
public class AppInstanceType {
	private String id;
	private String name;
	private String cloudResourceId;
	private Date createTime;
	private Integer type;
	private String from;
	private Boolean deleted;
	private Boolean selected;
	private Boolean disable;
}
