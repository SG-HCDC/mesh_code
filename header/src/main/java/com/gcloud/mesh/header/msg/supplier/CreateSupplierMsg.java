
package com.gcloud.mesh.header.msg.supplier;

import java.util.List;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

import org.hibernate.validator.constraints.Length;

import com.gcloud.mesh.header.constant.RegexpPattern;
import com.gcloud.mesh.header.exception.AssetErrorCode;
import com.gcloud.mesh.header.exception.CommonErrorCode;
import com.gcloud.mesh.header.exception.SpecialCode;
import com.gcloud.mesh.header.exception.SupplierErrorCode;
import com.gcloud.mesh.header.msg.BaseMsg;
import com.gcloud.mesh.header.valid.ValidName;

import lombok.Data;

@Data
public class CreateSupplierMsg extends BaseMsg {

	@NotBlank( message = AssetErrorCode.NAME_NOT_BLANK)
	@Length( min = 1, max = 60, message = AssetErrorCode.NAME_LENGTH_ERROR)
	@Pattern( regexp = RegexpPattern.STAND_NAME, message = AssetErrorCode.NAME_FORMAT_ERROR)
	@SpecialCode(message = "::设备名称"+CommonErrorCode.SPECIAL)
	private String name;
    
	@NotBlank( message = AssetErrorCode.NAME_NOT_BLANK)
    private String type;
    
    @NotBlank( message = AssetErrorCode.DATACENTER_ID_NOT_BLANK)
	@Length( min = 1, max = 60, message = AssetErrorCode.DATACENTER_ID_LENGTH_ERROR)
	@Pattern( regexp = RegexpPattern.STAND_ID, message = AssetErrorCode.DATACENTER_ID_FORMAT_ERROR)
    @SpecialCode(message = "::数据中心ID"+CommonErrorCode.SPECIAL)
    private String datacenterId;
    
    private String config;

    private String insecureCommand;

	private String clusterId;

}
