package com.gcloud.mesh.header.vo.alert;

import java.io.Serializable;
import java.util.Date;

import lombok.Data;

@Data
public class HistoryVo implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private String id;
	private String title;
	private String resourceId;
	private String resourceType;
	private String meter;
	private Integer level;
	private String resourceName;
	private Date createTime;
	private Integer notifyCount;
	private Date alertTime;
	private String host;
	private String platformType;
	private String regionId;
	private Integer status;
	private String content;
	private boolean recover;
	private String resourceInstance;
	private String levelCnName;
}
