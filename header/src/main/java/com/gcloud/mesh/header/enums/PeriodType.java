package com.gcloud.mesh.header.enums;

import java.util.stream.Stream;

public enum PeriodType {
	
	HOUR("hour", "按小时统计", "yyyy-MM-dd"),
	DAY("day", "按天统计", "yyyy-MM"),
	MONTH("month", "按年统计", "yyyy");
	
	private String name;
	private String cnName;
	private String template;
	
	PeriodType(String name, String cnName, String template) {
		this.name = name;
		this.cnName = cnName;
		this.template = template;
	}
	
	public static PeriodType getByName(String name) {
		return Stream.of(PeriodType.values())
				.filter( s -> s.getName().equals(name))
				.findFirst()
				.orElse(null);
	}

	public String getName() {
		return name;
	}

	public String getCnName() {
		return cnName;
	}

	public String getTemplate() {
		return template;
	}
	
	
}
