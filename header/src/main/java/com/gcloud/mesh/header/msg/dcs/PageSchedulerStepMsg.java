
package com.gcloud.mesh.header.msg.dcs;

import com.gcloud.mesh.header.msg.BaseQueryMsg;

import lombok.Data;

@Data
public class PageSchedulerStepMsg extends BaseQueryMsg {

	private String schedulerJobId;
	
}
