
package com.gcloud.mesh.header.constant;

import java.util.stream.Stream;

public enum AppSceneType {

    OFFLINE,
    REALTIME,
	SEQUENTIAL;
	
	public static AppSceneType getByName(String name) {
		return Stream.of(AppSceneType.values())
				.filter( s -> s.name().equals(name))
				.findFirst()
				.orElse(null);
	}
}
