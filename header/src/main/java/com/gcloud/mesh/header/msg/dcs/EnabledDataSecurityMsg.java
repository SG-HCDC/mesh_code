package com.gcloud.mesh.header.msg.dcs;

import com.gcloud.mesh.header.msg.BaseMsg;

import lombok.Data;

@Data
public class EnabledDataSecurityMsg extends BaseMsg {
	
	private Boolean enabled;

}
