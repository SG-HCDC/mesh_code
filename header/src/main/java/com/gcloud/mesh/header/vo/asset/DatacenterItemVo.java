package com.gcloud.mesh.header.vo.asset;

import java.util.Date;
import java.util.Map;

import lombok.Data;

@Data
public class DatacenterItemVo {
	
	private String id;
	private String name;
	private String ip;
	private String creator;
	private Date createTime;
	private Integer power;
	private Boolean powerConservation;
	private Integer threshold;
	private Boolean isScheduler;
	private String clusterId;
	private String spType;
	private String structure;
	private String rooms;
}
