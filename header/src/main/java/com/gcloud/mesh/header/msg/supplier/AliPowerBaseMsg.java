package com.gcloud.mesh.header.msg.supplier;

import lombok.Data;

import java.util.Map;

@Data
public class AliPowerBaseMsg<T> {
    private String err_code;
    private String err_msg;
    private Map<String, T> data;
}
