package com.gcloud.mesh.header.vo.monitor;

import java.util.List;
import lombok.Data;

@Data
public class StatisticsVo {
	private List<StatisticsResourceVo> list;

}
