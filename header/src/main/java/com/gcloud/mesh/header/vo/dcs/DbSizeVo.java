package com.gcloud.mesh.header.vo.dcs;

import lombok.Data;

@Data
public class DbSizeVo {
	private String dataSize;
}
