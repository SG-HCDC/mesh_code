
package com.gcloud.mesh.header.vo.restvelero;

import lombok.Data;

@Data
public class RestveleroRestoreReply {

	private Integer status;

	private RestveleroRestoreMessageVo message;

}
