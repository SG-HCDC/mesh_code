package com.gcloud.mesh.header.vo.monitor;

import java.util.List;
import lombok.Data;

@Data
public class StatisticsResourceVo {
	private String resourceId;
	private String resourceName;
	private String host;
	private String instance;
	private List<StatisticsPointVo> points;
}
