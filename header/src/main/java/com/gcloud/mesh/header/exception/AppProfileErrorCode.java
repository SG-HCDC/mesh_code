package com.gcloud.mesh.header.exception;

public class AppProfileErrorCode {

	public static final String APPID_NOT_BLANK = "150101";
	public static final String FEATURECHOOSE_NOT_BLANK = "150102";
	public static final String SCENECHOOSE_NOT_BLANK = "150103";
	
	
}
