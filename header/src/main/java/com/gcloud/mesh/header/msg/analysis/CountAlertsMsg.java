
package com.gcloud.mesh.header.msg.analysis;

import com.gcloud.mesh.header.msg.BaseMsg;

import lombok.Data;

@Data
public class CountAlertsMsg extends BaseMsg {

    private String deviceType;

}
