
package com.gcloud.mesh.header.vo;

import lombok.Data;

@Data
public class StatsCountVo {

    private String name;
    private int count;

}
