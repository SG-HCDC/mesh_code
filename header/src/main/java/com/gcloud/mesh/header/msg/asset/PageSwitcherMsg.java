package com.gcloud.mesh.header.msg.asset;

import org.hibernate.validator.constraints.Length;

import com.gcloud.mesh.header.exception.CommonErrorCode;
import com.gcloud.mesh.header.exception.SpecialCode;
import com.gcloud.mesh.header.msg.BaseQueryMsg;

import lombok.Data;

@Data
public class PageSwitcherMsg extends BaseQueryMsg {
	
	@Length( min = 1, max = 10, message = "::交换机名称"+CommonErrorCode.LENGTH10)
	@SpecialCode(message = "::交换机名称"+CommonErrorCode.SPECIAL)
	private String name;
	
	private String ip;

}
