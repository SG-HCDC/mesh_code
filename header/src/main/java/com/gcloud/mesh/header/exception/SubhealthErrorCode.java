package com.gcloud.mesh.header.exception;

public class SubhealthErrorCode {
	
	public static final String DATA_TYPE_ERROR = "070101";
    public static final String AIR_HUMIDITY_DATA_ERROR = "070102";
    public static final String AIR_TEMPERATURE_DATA_ERROR = "070103";
    public static final String UPS_TEMPERATURE_DATA_ERROR = "070104";
    public static final String UPS_VOLITAGE_DATA_ERROR = "070105";
    public static final String DATACENTER_UPS_DATA_ERROR = "070106";
    public static final String DATACENTER_DATA_LENGTH_ERROR = "070107";
    public static final String THRESHOLD_UPPER_LOWER_ERROR = "070108";
    public static final String DATACENTER_AIR_DATA_ERROR = "070109";

}
