
package com.gcloud.mesh.header.vo.dcs;

import java.util.Date;

import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class AppVo {

	private String id;
	private String name;
	private String cloudResourceId;
	private String datacenterId;
	private Integer type;
	private Date createTime;
	
}
