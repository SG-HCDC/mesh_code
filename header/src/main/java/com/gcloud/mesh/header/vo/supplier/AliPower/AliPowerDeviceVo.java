package com.gcloud.mesh.header.vo.supplier.AliPower;


import lombok.Data;

import java.util.List;

@Data
public class AliPowerDeviceVo {
    private String id;
    private String type;
    private String address;
    private String owner;
    private String price;
    private List<String> props;
}
