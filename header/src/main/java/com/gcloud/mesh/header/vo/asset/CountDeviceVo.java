package com.gcloud.mesh.header.vo.asset;

import lombok.Data;

@Data
public class CountDeviceVo {
	
	private int total;
	private int generalNum;
	private int pduNum;
	private int cabinetNum;
	private int serverNum;
	private int switcherNum;
	private int distributionBoxNum;
	private int upsNum;
	private int airConditionNum;
	private int temperatureSensorNum;
}
