
package com.gcloud.mesh.header.vo.dcs;

import java.util.Date;

import lombok.Data;

@Data
public class PageAppFeatureVo {

    private String appId;
    private String appName;
    private String datacenterName;
    private Date createTime;
    private String featureSuggest;
    private String featureChoose;

}
