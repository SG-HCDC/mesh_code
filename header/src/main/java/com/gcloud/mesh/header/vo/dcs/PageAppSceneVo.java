
package com.gcloud.mesh.header.vo.dcs;

import java.util.Date;

import lombok.Data;

@Data
public class PageAppSceneVo {

    private String appId;
    private String appName;
    private String datacenterName;
    private Date createTime;
    private String sceneSuggest;
    private String sceneChoose;

}
