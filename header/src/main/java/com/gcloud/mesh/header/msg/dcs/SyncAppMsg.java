
package com.gcloud.mesh.header.msg.dcs;

import com.gcloud.mesh.header.msg.BaseMsg;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;

@Data
public class SyncAppMsg extends BaseMsg {

	@ApiModelProperty(name = "appIds", value = "应用ID")
	private String[] appIds;

	@ApiModelProperty(name = "datacenterId", value = "数据中心ID")
	private String datacenterId;

	@ApiModelProperty(name = "cloudResourceId", value = "云平台资源ID")
	@NotBlank( message = "190107")
	private String cloudResourceId;


}
