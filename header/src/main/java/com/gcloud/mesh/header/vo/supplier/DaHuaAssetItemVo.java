package com.gcloud.mesh.header.vo.supplier;

import lombok.Data;

@Data
public class DaHuaAssetItemVo {

	private Integer id; //设备ID
	private String districtName; //所属行政区划名称
	private String roomName; //所属机房名称
	private String shelvesName; //设备机柜名称
	private String typeName; //设备型号名称
	private String serverName; //通讯服务名称
	private String deviceCategroy;
	private String deviceName; //设备名称
	private String state; //设备运行状态
	private String deviceAddress; //设备地址/标识
	private String deviceCode; //设备代码
	private String offset1; //遥信偏移量
	private String offset2; //遥测偏移量
	private String catname; //类型名

}
