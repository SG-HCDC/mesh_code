
package com.gcloud.mesh.header.msg;

import java.util.UUID;

import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.Data;

@Data
public class BaseMsg {

    private String msgId = UUID.randomUUID().toString();
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String title;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    protected String content;
}
