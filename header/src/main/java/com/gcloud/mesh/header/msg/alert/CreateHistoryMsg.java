package com.gcloud.mesh.header.msg.alert;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.Date;

import com.gcloud.mesh.header.exception.AlertErrorCode;
import com.gcloud.mesh.header.msg.BaseMsg;

import lombok.Data;

@Data
public class CreateHistoryMsg extends BaseMsg {

	private String title;

	private String resourceId;
	private String resourceType;
	private String meter;
	private Integer level;

	private String resourceName;
	private Integer notifyCount;
	private Date alertTime;

	private String regionId;
	private Integer status;
	private String content;
	private String resourceInstance;
	private String platformType;
}
