package com.gcloud.mesh.header.vo.dcs;

import java.util.List;

import lombok.Data;

@Data
public class SchedulerEffectPredictionVo {

	private String id;
	private List<SchedulerEffectPredictionFactorVo> factors;
	private Double temperatureDifference;
	private Double distributionBoxVoltageDifference;
	private Double datacenterEnergyDifference;
	private Double businessRespondDifference;
	
	private Double temperatureBefore;
	private Double distributionBoxVoltageBefore;
	private Double datacenterEnergyBefore;
	private Double businessRespondBefore;
	
	private Double temperatureImagine;
	private Double distributionBoxVoltageImagine;
	private Double datacenterEnergyImagine;
	private Double businessRespondImagine;

	private Double temperatureAfter;
	private Double distributionBoxVoltageAfter;
	private Double datacenterEnergyAfter;
	private Double businessRespondAfter;
}
