package com.gcloud.mesh.header.msg.alert;

import com.gcloud.mesh.header.exception.AlertErrorCode;
import com.gcloud.mesh.header.exception.CommonErrorCode;
import com.gcloud.mesh.header.exception.SpecialCode;
import com.gcloud.mesh.header.msg.BaseMsg;
import javax.validation.constraints.NotBlank;

import org.hibernate.validator.constraints.Length;

import lombok.Data;

@Data
public class DeleteRuleMsg extends BaseMsg {
	@NotBlank( message = AlertErrorCode.RULEID_NOT_NULL)
	@Length( min = 1, max = 60, message = "::告警规则ID"+CommonErrorCode.LENGTH60)
	@SpecialCode(message = "::告警规则ID"+CommonErrorCode.SPECIAL)
	private String ruleId;
}
