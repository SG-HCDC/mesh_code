package com.gcloud.mesh.header.enums;

import java.util.stream.Stream;

public enum DataSecurityPolicy {
	
	FULL("full", "全量备份"),
	INCREMENT("increment", "增量备份");
	
	private String name;
	private String cnName;
	
	DataSecurityPolicy(String name, String cnName) {
		this.name = name;
		this.cnName = cnName;
	}

	public static DataSecurityPolicy getByName(String name) {
		return Stream.of(DataSecurityPolicy.values())
				.filter( s -> s.getName().equals(name))
				.findFirst()
				.orElse(null);
	}
	
	public String getName() {
		return this.name;
	}
	
	public String getCnName() {
		return this.cnName;
	}
}
