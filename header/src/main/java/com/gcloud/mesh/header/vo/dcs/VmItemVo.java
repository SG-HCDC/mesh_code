
package com.gcloud.mesh.header.vo.dcs;

import java.util.Date;

import lombok.Data;

@Data
public class VmItemVo {

	private String id;
	private String name;
	private String cloudResourceId;
	private String cloudResourceName;
	private String typeName;
	private Integer resourceType;
	private Date createTime;
	private String datacenterId;
	private String datacenterName;
	private String instanceId;
	private String instanceName;
	
}
