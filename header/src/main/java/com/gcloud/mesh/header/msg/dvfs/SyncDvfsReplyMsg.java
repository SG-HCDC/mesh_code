
package com.gcloud.mesh.header.msg.dvfs;

import com.gcloud.mesh.header.msg.MqBaseMsg;
import com.gcloud.mesh.header.msg.MqBaseReplyMsg;
import lombok.Data;
import lombok.experimental.Accessors;

@Data
public class SyncDvfsReplyMsg extends MqBaseReplyMsg {
}
