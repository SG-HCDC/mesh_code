package com.gcloud.mesh.header.vo.analysis;

import lombok.Data;

@Data
public class AlertStatisticVo {

	private String name;
	private String type;
	private Integer value;
}
