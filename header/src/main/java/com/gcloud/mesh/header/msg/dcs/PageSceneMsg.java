
package com.gcloud.mesh.header.msg.dcs;

import org.apache.commons.lang3.StringUtils;
import org.hibernate.validator.constraints.Length;

import com.gcloud.mesh.header.exception.CommonErrorCode;
import com.gcloud.mesh.header.exception.SpecialCode;
import com.gcloud.mesh.header.msg.BaseQueryMsg;

import lombok.Data;

@Data
public class PageSceneMsg extends BaseQueryMsg {

	@Length( min = 1, max = 10, message = "::应用名称"+CommonErrorCode.LENGTH10)
	@SpecialCode(message = "::应用名称"+CommonErrorCode.SPECIAL)
	private String appName;
	
	@Length( min = 1, max = 60, message = "::数据中心ID"+CommonErrorCode.LENGTH60)
	@SpecialCode(message = "::数据中心ID"+CommonErrorCode.SPECIAL)
	private String datacenterId;

	@Override
	public String toString() {
		StringBuffer buffer = new StringBuffer();
		if(StringUtils.isNotBlank(appName)) {
			buffer.append(" 应用名称："+appName);
		}
		if(StringUtils.isNotBlank(datacenterId)) {
			buffer.append(" 数据中心ID："+datacenterId);
		}
		return buffer.toString();
	}
	
	
}
