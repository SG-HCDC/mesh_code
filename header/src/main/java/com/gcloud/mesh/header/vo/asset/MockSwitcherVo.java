package com.gcloud.mesh.header.vo.asset;

import java.util.Date;

import lombok.Data;

@Data
public class MockSwitcherVo {
	
	private String id;
	private String name;
	private String esn;
	private Integer type;
	private String deviceId;
	private String deviceModel;
	private String deviceManufacturer;
	private String datacenterId;
	private String creator;
	private String createTime;
	private String from;
	private String ip;
	private String community;
	private Integer status;

}
