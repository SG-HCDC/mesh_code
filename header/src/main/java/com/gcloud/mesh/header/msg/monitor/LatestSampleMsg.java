package com.gcloud.mesh.header.msg.monitor;

import javax.validation.constraints.NotNull;

import com.gcloud.mesh.header.msg.BaseMsg;

import lombok.Data;

@Data
public class LatestSampleMsg extends BaseMsg {

	@NotNull(message = "MeterCannotNull")
	private String meter;
	
	private String resourceId;
}
