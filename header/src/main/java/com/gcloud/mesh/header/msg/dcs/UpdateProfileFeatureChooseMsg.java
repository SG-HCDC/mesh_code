
package com.gcloud.mesh.header.msg.dcs;

import javax.validation.constraints.NotBlank;

import org.apache.commons.lang3.StringUtils;
import org.hibernate.validator.constraints.Length;

import com.gcloud.mesh.header.exception.AppProfileErrorCode;
import com.gcloud.mesh.header.exception.CommonErrorCode;
import com.gcloud.mesh.header.exception.SpecialCode;
import com.gcloud.mesh.header.msg.BaseMsg;
import com.gcloud.mesh.header.valid.ValidId;

import lombok.Data;

@Data
public class UpdateProfileFeatureChooseMsg extends BaseMsg {

    @ValidId
    @NotBlank( message = AppProfileErrorCode.APPID_NOT_BLANK)
    @Length( min = 1, max = 60, message = "::应用ID"+CommonErrorCode.LENGTH60)
	//@SpecialCode(message = "::应用ID"+CommonErrorCode.SPECIAL)
    private String appId;
    
    @NotBlank( message = AppProfileErrorCode.FEATURECHOOSE_NOT_BLANK)
    private String featureChoose;

	@Override
	public String toString() {
		StringBuffer buffer = new StringBuffer();
		if(StringUtils.isNotBlank(featureChoose)) {
			switch(featureChoose){
			case "CPU" :
		       buffer.append(" 业务特点=计算密集型");
		       break; //可选
		    case "IO" :
		       buffer.append(" 业务特点=磁盘IO密集型");
		       break; //可选
		    case "AI" :
		       buffer.append(" 业务特点=人工智能型");
		       break; //可选
		    default : //可选
		}
	}
		return buffer.toString();
	}
    
    

}
