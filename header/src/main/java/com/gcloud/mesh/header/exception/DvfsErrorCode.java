package com.gcloud.mesh.header.exception;

public class DvfsErrorCode {
    public static final String STRATEGY_NOT_SUPPORT= "050101";
    public static final String STRATEGY_NOT_EXIST="050102";

    public static final String DEVICE_NOT_EXIST="050201";

    public static final String IPMI_NOT_EXIST="050301";

    public static final String DVFS_NOT_EXIST="050401";

    public static final String PING_ERROR="050501";

    public static final String CONTROL_TYPE_NOT_SUPPORTED="050601";

    public static final String ID_FORMAT_ERROR = "050701";
    public static final String ID_LENGTH_ERROR = "050702";

    public static final String NAME_FORMAT_ERROR="050801";
    public static final String NAME_LENGTH_ERROR="050802";
}
