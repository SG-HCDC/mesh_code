
package com.gcloud.mesh.header.vo.dcs;

import java.util.Date;

import lombok.Data;

@Data
public class DataBackupVo {

    private String id;
    private String name;
    private String policy;
    private String policyCnName;
    private Date createTime;

}
