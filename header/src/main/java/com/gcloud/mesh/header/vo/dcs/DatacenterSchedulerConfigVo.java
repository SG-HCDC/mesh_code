
package com.gcloud.mesh.header.vo.dcs;

import java.util.Date;
import java.util.List;
import java.util.Map;

import com.gcloud.mesh.header.msg.dcs.MyUpdateAppSchedulerConfig;
import com.gcloud.mesh.header.msg.dcs.MyUpdateAppSchedulerConfigMsg;
import lombok.Data;

@Data
public class DatacenterSchedulerConfigVo {

	private String id;
	private String name;
	private Date createTime;
	private Boolean isSchedulerConfig;

	private String thresholdKey;
	private String thresholdOperation;
	private Double thresholdValue;

	private Map<String, String> capacity;
	private Map<String, String> requested;


	private List<DescribeMetricVO> describeMetricVO;

	private String spType;

	private List<MyUpdateAppSchedulerConfig> schedulerConfigList;


}
