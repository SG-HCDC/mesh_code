
package com.gcloud.mesh.header.msg.dcs;

import com.gcloud.mesh.header.exception.AppErrorCode;
import com.gcloud.mesh.header.exception.CommonErrorCode;
import com.gcloud.mesh.header.exception.SpecialCode;
import com.gcloud.mesh.header.msg.BaseMsg;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.Length;
import javax.validation.constraints.NotBlank;
import java.util.List;

@Data
public class MyUpdateAppSchedulerConfigMsg extends BaseMsg {

	@ApiModelProperty(name = "datacenterId", value = "数据中心ID")
	@NotBlank( message = AppErrorCode.DATACENTERID_NOT_BLANK)
	@Length( min = 1, max = 60, message = "::数据中心ID"+CommonErrorCode.LENGTH60)
	@SpecialCode(message = "::数据中心ID"+CommonErrorCode.SPECIAL)
	private String datacenterId;

	private List<MyUpdateAppSchedulerConfig> schedulerConfigList;

}
