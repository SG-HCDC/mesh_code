
package com.gcloud.mesh.header.msg.dcs;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Length;

import com.gcloud.mesh.header.exception.AppErrorCode;
import com.gcloud.mesh.header.exception.CommonErrorCode;
import com.gcloud.mesh.header.exception.SpecialCode;
import com.gcloud.mesh.header.msg.BaseMsg;

import lombok.Data;

@Data
public class UpdateAppMsg extends BaseMsg {

	@NotBlank( message = AppErrorCode.APPID_NOT_BLANK)
	@Length( min = 1, max = 60, message = "::应用ID"+CommonErrorCode.LENGTH60)
	@SpecialCode(message = "::应用ID"+CommonErrorCode.SPECIAL)
	private String appId;
	
	@SpecialCode(message = "::应用名称"+CommonErrorCode.SPECIAL)
	@Length( min = 1, max = 10, message = "::应用名称"+CommonErrorCode.LENGTH10)
	private String name;
	
    private Integer type;

	@Override
	public String toString() {
		return "更新应用 参数[应用名称=" + name + "]";
	}
    
    

}
