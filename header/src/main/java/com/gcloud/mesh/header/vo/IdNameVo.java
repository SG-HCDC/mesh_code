
package com.gcloud.mesh.header.vo;

import lombok.Data;

@Data
public class IdNameVo {

    private String id;
    private String name;

}
