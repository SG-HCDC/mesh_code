package com.gcloud.mesh.header.msg.dcs;

import lombok.Data;

@Data
public class AppInstance {
	private String instanceId;
	private String instanceName;
}
