package com.gcloud.mesh.header.vo.alert;

import lombok.Data;

@Data
public class EmailVo {
	
	private String id;
	private String emailServer;
	private String emailProtocol;
	private String emailSenderAccount;
	private String emailSenderPasswd;
	private String emailSenderName;
}
