package com.gcloud.mesh.header.msg.alert;

import com.gcloud.mesh.header.exception.AlertErrorCode;
import com.gcloud.mesh.header.exception.CommonErrorCode;
import com.gcloud.mesh.header.exception.SpecialCode;
import com.gcloud.mesh.header.msg.BaseMsg;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;

import org.hibernate.validator.constraints.Length;

import lombok.Data;

@Data
public class UpdateLinkmanMsg extends BaseMsg {

	@NotBlank( message = AlertErrorCode.LINKMANID_NOT_NULL)
	@Length( min = 1, max = 60, message = "::告警联系人ID"+CommonErrorCode.LENGTH60)
	@SpecialCode(message = "::告警联系人ID"+CommonErrorCode.SPECIAL)
	private String linkmanId;
	@NotBlank( message = AlertErrorCode.USERNAME_NOT_NULL)
	@Length( min = 1, max = 10, message = "::告警联系人"+CommonErrorCode.LENGTH10)
	@SpecialCode(message = "::告警联系人"+CommonErrorCode.SPECIAL)
	private String userName;
	
	@NotBlank( message = AlertErrorCode.PAGELINKMSG_EMAIL_NOT_NULL)
	@Length( min = 1, max = 26 , message = "::告警邮箱"+CommonErrorCode.LENGTH26)
	@Pattern( regexp = "^[a-zA-Z0-9_-]+@[a-zA-Z0-9_-]+(\\.[a-zA-Z0-9_-]+)+$", message = "::请输入正确的联系邮箱")
	private String email;
	private String phoneNumber;
	@Override
	public String toString() {
		StringBuffer buff = new StringBuffer();
		//buff.append("更新邮箱参数:[");
		buff.append(" 联系人名称：").append(userName);
		buff.append(" 邮箱：").append(email).append("");
		return buff.toString();
	}
	
	

}
