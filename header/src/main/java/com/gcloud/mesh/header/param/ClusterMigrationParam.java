package com.gcloud.mesh.header.param;

import lombok.Data;

import javax.validation.constraints.NotBlank;

@Data
public class ClusterMigrationParam {

    private String clusterId;

    private String clusterName;

    @NotBlank( message = "190101")
    private String restveleroIp;
}
