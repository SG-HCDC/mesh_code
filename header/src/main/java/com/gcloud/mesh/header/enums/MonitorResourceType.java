package com.gcloud.mesh.header.enums;

import com.gcloud.mesh.header.exception.BaseException;

public enum MonitorResourceType {
	
	HOST,
	VM,
	CEPH,
	LOCALSTORAGE,
	TRUST,
	SITE,
	PROCESS,
	APPLICATION,
	DOCKER,
	MLU,
	SNMP,
	CONTAINER;

	public static MonitorResourceType check(String typeStr) throws BaseException {
		try {
			return MonitorResourceType.valueOf(typeStr.toUpperCase());
		} catch (Exception e) {
			throw new BaseException("RESOURCETYPE_NOT_EXISTS");
		}
	}
}
