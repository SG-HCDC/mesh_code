package com.gcloud.mesh.header.vo.analysis;

import lombok.Data;

@Data
public class SubhealthAlertCountVo {
	
	 private String deviceType;
	 private Integer count;
}
