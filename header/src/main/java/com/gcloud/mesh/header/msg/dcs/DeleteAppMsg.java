
package com.gcloud.mesh.header.msg.dcs;

import javax.validation.constraints.NotBlank;

import org.hibernate.validator.constraints.Length;

import com.gcloud.mesh.header.exception.AppErrorCode;
import com.gcloud.mesh.header.exception.CommonErrorCode;
import com.gcloud.mesh.header.exception.SpecialCode;
import com.gcloud.mesh.header.msg.BaseMsg;

import lombok.Data;

@Data
public class DeleteAppMsg extends BaseMsg {

	@NotBlank( message = AppErrorCode.APPID_NOT_BLANK)
	@Length( min = 1, max = 60, message = "::应用ID"+CommonErrorCode.LENGTH60)
	@SpecialCode(message = "::应用ID"+CommonErrorCode.SPECIAL)
    private String appId;

}
