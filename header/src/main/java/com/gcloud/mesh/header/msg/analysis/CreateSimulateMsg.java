package com.gcloud.mesh.header.msg.analysis;

import org.hibernate.validator.constraints.Length;

import com.gcloud.mesh.header.exception.CommonErrorCode;
import com.gcloud.mesh.header.exception.SpecialCode;
import com.gcloud.mesh.header.msg.BaseMsg;

import lombok.Data;

@Data
public class CreateSimulateMsg extends BaseMsg {
	@Length( min = 1, max = 60, message = "::拟合名称"+CommonErrorCode.LENGTH60)
	@SpecialCode(message = "::拟合名称"+CommonErrorCode.SPECIAL)
	private String name;
	@Length( min = 1, max = 60, message = "::数据中心ID"+CommonErrorCode.LENGTH60)
	@SpecialCode(message = "::数据中心ID"+CommonErrorCode.SPECIAL)
	private String datacenterId;
	@Length( min = 1, max = 60, message = "::设备ID"+CommonErrorCode.LENGTH60)
	@SpecialCode(message = "::设备ID"+CommonErrorCode.SPECIAL)
	private String deviceId;
	private Integer degree;

}
