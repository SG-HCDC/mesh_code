package com.gcloud.mesh.header.msg.asset;

import javax.validation.constraints.Pattern;

import org.hibernate.validator.constraints.Length;
import org.springframework.lang.Nullable;

import com.gcloud.mesh.header.constant.RegexpPattern;
import com.gcloud.mesh.header.exception.AssetErrorCode;
import com.gcloud.mesh.header.exception.CommonErrorCode;
import com.gcloud.mesh.header.exception.SpecialCode;
import com.gcloud.mesh.header.msg.BaseMsg;

import lombok.Data;

@Data
public class UpdateNodeSmMsg extends BaseMsg {
	@Length( min = 1, max = 60, message = "::设备ID"+CommonErrorCode.LENGTH60)
	@SpecialCode(message = "::设备ID"+CommonErrorCode.SPECIAL)
	private String id;
	@Length( min = 1, max = 10, message = "::设备名称"+CommonErrorCode.LENGTH10)
	@SpecialCode(message = "::设备名称"+CommonErrorCode.SPECIAL)
	private String name;
	@Length( min = 1, max = 10, message = "::设备识别号"+CommonErrorCode.LENGTH10)
	@SpecialCode(message = "::设备识别号"+CommonErrorCode.SPECIAL)
	private String esn;
	@Length( min = 1, max = 10, message = "::设备型号"+CommonErrorCode.LENGTH10)
	@SpecialCode(message = "::设备型号"+CommonErrorCode.SPECIAL)
	private String deviceModel;
	@Length( min = 1, max = 10, message = "::设备厂商"+CommonErrorCode.LENGTH10)
	@SpecialCode(message = "::设备厂商"+CommonErrorCode.SPECIAL)
	private String deviceManufacturer;
	@Length( min = 1, max = 60, message = "::数据中心ID"+CommonErrorCode.LENGTH60)
	@SpecialCode(message = "::数据中心ID"+CommonErrorCode.SPECIAL)
	private String datacenterId;
	@Pattern( regexp = RegexpPattern.IP, message = "::管理网IP格式错误")
	private String mgmIp;
	@Pattern( regexp = RegexpPattern.IP, message = "带外管理IP格式错误")
	private String ipmiIp;
	@Length( min = 1, max = 10, message = "::带外管理用户"+CommonErrorCode.LENGTH10)
	@SpecialCode(message = "::带外管理用户"+CommonErrorCode.SPECIAL)
	private String ipmiUser;
//	@Length( min = 6, max = 20, message = "::带外管理密码"+CommonErrorCode.LENGTH6_20)
//	@SpecialCode(message = "::带外管理密码"+CommonErrorCode.SPECIAL)
	private String ipmiPassword;
	private Float powerConsumption;
	private Float energyEfficiencyRatio;
	private Float storageCapacity;
	@Length( min = 1, max = 500, message = "::重要数据"+CommonErrorCode.LENGTH200)
	private String smData;
	
	private Boolean isolation;
	// @NotBlank( message = AssetErrorCode.HOSTNAME_NOT_BLANK)
	@Nullable
	@Length( min = 1, max = 60, message = AssetErrorCode.HOSTNAME_LENGTH_ERROR)
	@SpecialCode(message = "::主机名"+CommonErrorCode.SPECIAL)
	private String hostname;
	
	@Length( min = 1, max = 60, message = "::额定功率"+CommonErrorCode.LENGTH60)
	private String ratedPowerSm;	
	
	@Length( min = 1, max = 60, message = "::额定电压"+CommonErrorCode.LENGTH60)
	private String ratedVoltageSm;
	@Override
	public String toString() {
		return "名称=" + name + " 设备识别号=" + esn + " 设备型号=" + deviceModel + " 设备厂商="
				+ deviceManufacturer + " 管理IP=" + mgmIp + " 主机名=" + hostname + "";
	}
	
	
	
}
