
package com.gcloud.mesh.header.vo.dcs;

import lombok.Data;

@Data
public class ModelScoreVo {

    private String factorId;
    private String factorName;
    private Integer score;
    private String layer;

}
