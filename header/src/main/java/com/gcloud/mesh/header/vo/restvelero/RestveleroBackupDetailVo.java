package com.gcloud.mesh.header.vo.restvelero;

import lombok.Data;

@Data
public class RestveleroBackupDetailVo {

	private RestveleroBackupDataVo data;

	private Integer status;

}
