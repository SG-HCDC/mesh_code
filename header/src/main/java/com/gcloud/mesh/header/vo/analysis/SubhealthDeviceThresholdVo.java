
package com.gcloud.mesh.header.vo.analysis;

import javax.validation.Valid;

import org.hibernate.validator.constraints.Range;

import lombok.Data;

@Data
@Valid
public class SubhealthDeviceThresholdVo {
    
	private String id;
    private String resourceId;
    private String type;
    private String meter;
    private String level;
//    @Range(min=1, max=100, message = "::亚健康因子取值1-100")
    private Float upperThreshold;
//    @Range(min=1, max=100, message = "::亚健康因子取值1-100")
    private Float lowerThreshold;
    private String unit;

}
