package com.gcloud.mesh.header.exception;

public class DatacenterEnergyErrorCode {

	public static final String DATACENTER_ID_NOT_BLANK = "100101";
	public static final String YEAR_NOT_BLANK = "100102";
	public static final String YEAR_FORMAT_ERROR = "100103";
	
	public static final String QUARTER_FORMAT_ERROR = "100104";
	public static final String MONTH_FORMAT_ERROR = "100105";
	
}
