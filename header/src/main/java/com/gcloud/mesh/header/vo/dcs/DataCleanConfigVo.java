package com.gcloud.mesh.header.vo.dcs;


import lombok.Data;

@Data
public class DataCleanConfigVo {

	private String id;
	private Boolean enabled;
	private String source;
	private String sourceName;
	private String operateType;

}
