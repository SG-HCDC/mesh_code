package com.gcloud.mesh.header.exception;

public class SupplierErrorCode {
	
	public static final String CONFIG_NOT_NULL = "040101";

	public static final String SUPPLIER_ALREADY_EXISTS = "040201";
	public static final String SUPPLIER_NOT_FOUND = "040202";
	public static final String SYSTEM_TYPE_NOT_FOUND = "040203";
}
