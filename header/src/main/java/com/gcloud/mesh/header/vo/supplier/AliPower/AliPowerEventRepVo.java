package com.gcloud.mesh.header.vo.supplier.AliPower;

import lombok.Data;


@Data
public class AliPowerEventRepVo {
    private String type;
    /*value 设备id、esn */
    private  String value;
    private String desc;
    private String price;
    private String tag;
    private String ruleType;
}
