package com.gcloud.mesh.header.vo.dcs;

import lombok.Data;

@Data
public class AppInstanceVo {
	private String instanceName;
	private String instanceId;
	private Integer status;
	private String statusName;
	private String resourceName;
	private String resourceId;
	private String srcDatacenterId;
	private String srcDatacenterName;
	private String srcCloudResourceId;
	private String srcCloudResourceName;
	private String dstCloudResourceId;
	private String dstDatacenterId;
	private String dstDatacenterName;
	private String datacenterName;
	private String datacenterId;
	private String appId;
	private String appName;
	private Integer type;
	
}
