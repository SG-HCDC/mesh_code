package com.gcloud.mesh.header.msg.analysis;

import com.gcloud.mesh.header.exception.CommonErrorCode;
import com.gcloud.mesh.header.exception.SpecialCode;
import com.gcloud.mesh.header.msg.BaseQueryMsg;

import org.hibernate.validator.constraints.Length;

import lombok.Data;

@Data
public class PageNodeHistoryMsg extends BaseQueryMsg  {
	
	@Length( min = 1, max = 10, message = "::服务器名称"+CommonErrorCode.LENGTH10)
	@SpecialCode(message = "::服务器名称"+CommonErrorCode.SPECIAL)
	private String name;
	
}
