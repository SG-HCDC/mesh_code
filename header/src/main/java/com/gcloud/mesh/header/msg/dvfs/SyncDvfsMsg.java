
package com.gcloud.mesh.header.msg.dvfs;

import com.gcloud.mesh.header.msg.MqBaseMsg;
import lombok.Data;
import lombok.experimental.Accessors;

@Data
public class SyncDvfsMsg extends MqBaseMsg {
}
