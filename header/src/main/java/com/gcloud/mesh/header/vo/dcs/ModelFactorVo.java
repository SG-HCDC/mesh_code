
package com.gcloud.mesh.header.vo.dcs;

import javax.validation.Valid;
import javax.validation.constraints.Digits;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.Range;

import com.gcloud.mesh.header.exception.CommonErrorCode;
import com.gcloud.mesh.header.valid.ValidId;

import lombok.Data;

@Data
@Valid
public class ModelFactorVo {

	@ValidId
	private String id;
	private String factorType;
	private String factorName;
	private Boolean enabled;
	// @Range(min=0, max=9999, message = "::评分因子在0-9999间")
	@Max(value = 999999, message = "::评分因子样本在0-999999间")
	@Min(value = 0, message = "::评分因子样本在0-999999间")
	private Integer scoreSample;
	// @NotNull(message = "::评分类型" + CommonErrorCode.NULL)
	private String scoreType;
	// @NotNull(message = "::评分因子权重不能为空")
	// @Range(min=0, max=100, message = "::评分权重在0-100间")
	@Max(value = 100, message = "::评分因子权重在0-100间")
	@Min(value = 0, message = "::评分因子权重在0-100间")
	private Integer weight;
	private Integer unitConversion;
	private Integer startX;
	private Integer startY;
	private Integer endX;
	private Integer endY;
	private Integer scoreBestPredict;
	private String unit;
	private String layer;

	private Integer diff;

	@Override
	public String toString() {
		String enableStr = "";
		if (enabled != null) {
			if (enabled) {
				enableStr = "开启";
			} else {
				enableStr = "关闭";
			}
		}
		return "[ 因子=" + (factorName == null ? "无" : factorName) + " 是否开启=" + enableStr + " 样本值=" + scoreSample + " 权重="
				+ weight + "%]";
	}

	public String whithoutqz() {
		String enableStr = "";
		if (enabled != null) {
			if (enabled) {
				enableStr = "开启";
			} else {
				enableStr = "关闭";
			}
		}
		return "[ 因子=" + (factorName == null ? "无" : factorName) + " 是否开启=" + enableStr + " 样本值=" + scoreSample + "]";
	}

	public String onlyqz() {
		return "[ 因子=" + (factorName == null ? "无" : factorName) + " 权重=" + weight + "%]";
	}

}
