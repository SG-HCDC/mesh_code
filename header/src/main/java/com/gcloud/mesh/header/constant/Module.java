
package com.gcloud.mesh.header.constant;

public class Module {

    public static final String CONFIG = "config";
    public static final String PROFILE = "profile";
    public static final String NAMING = "naming";
    public static final String SERVICE = "service";
    public static final String COMPONENT = "component";
    public static final String UPGRADE = "upgrade";
    public static final String PACK = "pack";
    public static final String POLICY = "policy";
    public static final String ENV = "env";
    public static final String TASK = "task";
    public static final String IMAGE = "image";
    public static final String IMAGE_STORAGE = "image_storage";
    public static final String LOCK = "lock";
    public static final String DICT = "dict";

}
