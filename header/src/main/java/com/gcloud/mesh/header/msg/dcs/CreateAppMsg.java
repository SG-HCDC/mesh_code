
package com.gcloud.mesh.header.msg.dcs;

import java.util.List;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Length;

import com.gcloud.mesh.header.exception.AppErrorCode;
import com.gcloud.mesh.header.exception.CommonErrorCode;
import com.gcloud.mesh.header.exception.SpecialCode;
import com.gcloud.mesh.header.msg.BaseMsg;

import lombok.Data;

@Data
public class CreateAppMsg extends BaseMsg {

	@NotBlank( message = AppErrorCode.NAME_NOT_BLANK)
	@SpecialCode(message = "::应用名称"+CommonErrorCode.SPECIAL)
	@Length( min = 1, max = 10, message = "::应用名称"+CommonErrorCode.LENGTH10)
    private String name;
    
	@NotBlank( message = AppErrorCode.CLOUDRESOURCEID_NOT_BLANK)
	@Length( min = 1, max = 60, message = "::云资源ID"+CommonErrorCode.LENGTH60)
	@SpecialCode(message = "::资源ID"+CommonErrorCode.SPECIAL)
    private String cloudResourceId;
	
	private List<AppInstance> instances;

	@Override
	public String toString() {
		return "创建应用 参数[名称=" + name + "]"+","+"云资源ID："+cloudResourceId;
	}
	
	
	
}
