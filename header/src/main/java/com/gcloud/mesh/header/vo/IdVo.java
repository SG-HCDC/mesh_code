
package com.gcloud.mesh.header.vo;

import lombok.Data;

@Data
public class IdVo {

    private String id;

}
