package com.gcloud.mesh.header.msg.asset;


import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

import org.hibernate.validator.constraints.Length;

import com.gcloud.mesh.header.constant.RegexpPattern;
import com.gcloud.mesh.header.exception.AssetErrorCode;
import com.gcloud.mesh.header.exception.CommonErrorCode;
import com.gcloud.mesh.header.exception.SpecialCode;
import com.gcloud.mesh.header.msg.BaseMsg;

import lombok.Data;

@Data
public class CreateDeviceMsg extends BaseMsg {
	
	@NotBlank( message = AssetErrorCode.NAME_NOT_BLANK)
	@Length( min = 1, max = 10, message = AssetErrorCode.NAME_LENGTH_ERROR)
	@SpecialCode(message = "::设备名称"+CommonErrorCode.SPECIAL)
//	@Pattern( regexp = "^[0-9a-zA-Z_]{1,}$", message = AssetErrorCode.NAME_FORMAT_ERROR)
	private String name;
	
	@NotBlank( message = AssetErrorCode.ESN_NOT_BLANK)
	@Length( min = 1, max = 10, message = AssetErrorCode.ESN_LENGTH_ERROR)
	@SpecialCode(message = "::资源序列号"+CommonErrorCode.SPECIAL)
//	@Pattern( regexp = RegexpPattern.STAND_ID, message = AssetErrorCode.ESN_FORMAT_ERROR)
	private String esn;
	
	@NotNull( message = AssetErrorCode.TYPE_FORMAT_ERROR)
	private Integer type;
	
	@NotBlank( message = AssetErrorCode.MODEL_NOT_BLANK)
	@Length( min = 1, max = 10, message = AssetErrorCode.MODEL_LENGTH_ERROR)
	@SpecialCode(message = "::设备型号"+CommonErrorCode.SPECIAL)
//	@Pattern( regexp = RegexpPattern.STAND_ID, message = AssetErrorCode.MODEL_FORMAT_ERROR)
	private String deviceModel;
	
	@NotBlank( message = AssetErrorCode.MANUFACTURER_NOT_BLANK)
	@Length( min = 1, max = 10, message = AssetErrorCode.MANUFACTURER_LENGTH_ERROR)
	@SpecialCode(message = "::设备厂商"+CommonErrorCode.SPECIAL)
//	@Pattern( regexp = "^[0-9a-zA-Z_]{1,}$", message = AssetErrorCode.MANUFACTURER_FORMAT_ERROR)
	private String deviceManufacturer;
	
	@NotBlank( message = AssetErrorCode.DATACENTER_ID_NOT_BLANK)
	@Length( min = 1, max = 60, message = AssetErrorCode.DATACENTER_ID_LENGTH_ERROR)
	@Pattern( regexp = RegexpPattern.STAND_ID, message = AssetErrorCode.DATACENTER_ID_FORMAT_ERROR)
	@SpecialCode(message = "::数据中心ID"+CommonErrorCode.SPECIAL)
	private String datacenterId;
	
	private String from;
	
	private String deviceId;
	
	@Override
	public String toString() {
		String typeName = "";
		if(type!=null && type == 1) {
			typeName = " 空调";
		}
		if(type!=null && type == 2) {
			typeName = "UPS";
		}
		if(type!=null && type == 3) {
			typeName = " 配电箱";
		}
		return " 名称=" + name + " 设备识别号=" + esn + " 设备类型=" + typeName + " 设备型号=" + deviceModel
				+ " 设备厂商=" + deviceManufacturer + "";
	}

}
