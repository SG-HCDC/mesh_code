package com.gcloud.mesh.header.vo.restvelero;

import lombok.Data;

@Data
public class RestveleroBackupListVo {

	private String status;
	private String errors;
	private String warnings;
	private String created;
	private String storageLocation;
	
}
