
package com.gcloud.mesh.header.vo.restvelero;

import lombok.Data;

@Data
public class RestveleroRestoreMessageVo {

	private String restore_name;

}
