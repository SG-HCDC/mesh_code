package com.gcloud.mesh.header.msg.alert;

import com.gcloud.mesh.header.exception.AlertErrorCode;
import com.gcloud.mesh.header.exception.CommonErrorCode;
import com.gcloud.mesh.header.exception.SpecialCode;
import com.gcloud.mesh.header.msg.BaseMsg;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;

import org.hibernate.validator.constraints.Length;

import lombok.Data;

@Data
public class CreateLinkmanMsg extends BaseMsg{

	@NotBlank( message = AlertErrorCode.PAGELINKMSG_USERNAME_NOT_NULL)
	@Length( min = 1, max = 10, message = "::邮箱联系人"+CommonErrorCode.LENGTH10)
	@SpecialCode(message = "::告警联系人"+CommonErrorCode.SPECIAL)
	private String userName;
	@NotBlank( message = AlertErrorCode.PAGELINKMSG_EMAIL_NOT_NULL)
	@Length( min = 1, max = 26, message = "::邮箱"+CommonErrorCode.LENGTH26)
	@Pattern( regexp = "^[a-zA-Z0-9_-]+@[a-zA-Z0-9_-]+(\\.[a-zA-Z0-9_-]+)+$", message = "::请输入正确的联系邮箱")
	private String email;
	private String phoneNumber;
}
