package com.gcloud.mesh.header.vo.dcs;

import lombok.Data;

@Data
public class StatisticsVo {

	private Double max;
	private Double min;
	private Double avg;
}
