package com.gcloud.mesh.header.msg.alert;

import lombok.Data;

@Data
public class FlowHistoryMsg {

	private Integer level;
	private String startTime;
	private String endTime;
}
