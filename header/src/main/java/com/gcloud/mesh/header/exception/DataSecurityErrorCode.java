package com.gcloud.mesh.header.exception;

public class DataSecurityErrorCode {

	public static final String FREQUENCY_NOT_BLANK = "080101";
	public static final String FREQUENCY_FORMAT_ERROR = "080102";
	public static final String TIME_NOT_BLANK = "080103";
	public static final String TIME_FORMAT_ERROR = "080104";
	public static final String POLICY_NOT_BLANK = "080105";
	public static final String POLICY_FORMAT_ERROR = "080106";
	public static final String ENABLED_NOT_NULL = "080107";
	
	public static final String ALREADY_ENABLED_ERROR = "080201";
	public static final String ALREADY_DISABLED_ERROR = "080202";
	
}
