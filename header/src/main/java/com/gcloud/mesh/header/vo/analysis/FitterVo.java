package com.gcloud.mesh.header.vo.analysis;

import java.util.List;

import lombok.Data;

@Data
public class FitterVo {

	private Integer degree;
	private List<ChartVo> charts;
	
}
