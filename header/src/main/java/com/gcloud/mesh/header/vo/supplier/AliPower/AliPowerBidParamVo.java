package com.gcloud.mesh.header.vo.supplier.AliPower;

import lombok.Data;

@Data
public class AliPowerBidParamVo {
    private String device_id;
    private Integer price;
    private String idToken;
}
