package com.gcloud.mesh.header.msg.dvfs;

import com.gcloud.mesh.header.msg.MqBaseMsg;
import lombok.Data;

import java.util.List;

/**
 * @see #supportStrategies 支持的策略
 * @see #frequency 频率
 */
@Data
public class GetDvfsInfoMsg extends MqBaseMsg {
    private String resourceId;
    private List<String> supportStrategies;
    private Float frequency;
    private Boolean success;
    private String errorCode;
}
