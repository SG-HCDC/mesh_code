package com.gcloud.mesh.header.msg.analysis;

import java.util.Date;

import org.hibernate.validator.constraints.Length;

import com.gcloud.mesh.header.exception.CommonErrorCode;
import com.gcloud.mesh.header.exception.SpecialCode;
import com.gcloud.mesh.header.msg.BaseMsg;

import lombok.Data;

@Data
public class UpdateSimulateMsg extends BaseMsg {
	@Length( min = 1, max = 60, message = "::拟合ID"+CommonErrorCode.LENGTH60)
	@SpecialCode(message = "::拟合ID"+CommonErrorCode.SPECIAL)
	private String id;
	private Integer nLevel;
	private String result;
	private Date startTime;
	private Date entTime;
	private Date updateTime;
}
