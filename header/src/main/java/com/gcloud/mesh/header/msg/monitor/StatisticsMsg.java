package com.gcloud.mesh.header.msg.monitor;

import javax.validation.constraints.NotNull;

import lombok.Data;

@Data
public class StatisticsMsg {

	private String dcId;

	@NotNull(message = "MeterCannotNull")
	private String meter;

	private String resourceId;

	private String host;

	private String instance;

	private String resourceType;

	private String func;

	@NotNull(message = "BeginTimeCannotNull")
	private String beginTime;

	@NotNull(message = "EndTimeCannotNull")
	private String endTime;

	@NotNull(message = "IntervalCannotNull")
	private String interval;
}
