package com.gcloud.mesh.header.vo.dcs;

import lombok.Data;

@Data
public class AuthorityVo {

    private String id;
    private String name;
    private String classification;
    private Boolean enabled;

}
