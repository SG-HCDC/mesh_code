
package com.gcloud.mesh.header.vo;

import lombok.Data;

@Data
public class RoleVo {

    private String role;
    private String id;

    public RoleVo() {

    }

    public RoleVo(String role, String id) {
        this.role = role;
        this.id = id;
    }

}
