package com.gcloud.mesh.header.vo.asset;

import java.util.Date;

import lombok.Data;


@Data
public class MockCloudResourceVo {

	private String id;
	private String name;
	private Integer type;
	private String datacenterId;
	private String expand;
	private String createTime;
	private String from;
	private Integer cpuCores;
	private Integer memory;
	private String objectId;
}
