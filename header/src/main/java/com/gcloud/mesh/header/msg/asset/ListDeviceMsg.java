package com.gcloud.mesh.header.msg.asset;

import com.gcloud.mesh.header.msg.BaseMsg;

import lombok.Data;

@Data
public class ListDeviceMsg extends BaseMsg {

	private String from;
	
	private String datacenterId;
	
	private Integer type;
}
