
package com.gcloud.mesh.header.vo.dcs;

import java.util.List;

import javax.validation.constraints.NotNull;

import com.gcloud.mesh.header.exception.SchedulerErrorCode;
import com.gcloud.mesh.header.msg.BaseMsg;

import lombok.Data;

@Data
public class UpdateSchedulerVo extends BaseMsg {

	@NotNull( message = SchedulerErrorCode.DATACENTERIDS_NOT_NULL)
    private List<String> datacenterIds;

	@Override
	public String toString() {
		StringBuffer buff = new StringBuffer();

		int size = datacenterIds.size();
		int i = 1;
		for(String dataCenter : datacenterIds) {
			buff.append(" 可调度数据中心ID="+dataCenter);
			if(i == size) {
				buff.append(",");
			}
		}
		return "参数：[" + buff.toString() + "]";
	}
	
	
  

}
