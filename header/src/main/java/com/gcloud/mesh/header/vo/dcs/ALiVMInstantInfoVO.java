package com.gcloud.mesh.header.vo.dcs;

import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class ALiVMInstantInfoVO {

    private String instanceType;
    private String instanceChargeType;
    private String internetChargeType;
    private Integer internetMaxBandwidthIn;
    private Integer internetMaxBandwidthOut;
    private String hostName;
    private String instanceId;

}
