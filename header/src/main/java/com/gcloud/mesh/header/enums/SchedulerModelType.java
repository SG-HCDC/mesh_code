
package com.gcloud.mesh.header.enums;

import com.gcloud.mesh.header.exception.BaseException;

public enum SchedulerModelType {


    DATACENTER_COST(0), // 数据中心最优成本
    DATACENTER_DEVICE(1); // 数据中心最优设备

	
    private int deviceType;

    private SchedulerModelType(int deviceType) {
        this.deviceType = deviceType;
    }

    public static SchedulerModelType checkAndGet(String type) throws BaseException {
        if (type != null) {
            for (SchedulerModelType t : SchedulerModelType.values()) {
                if (t.name().equals(type)) {
                    return t;
                }
            }
        }
        return null;
    }

    public static SchedulerModelType get(String type) {
        if (type != null) {
            for (SchedulerModelType t : SchedulerModelType.values()) {
                if (t.name().equals(type)) {
                    return t;
                }
            }
        }
        return null;
    }

    public int getDeviceType() {
        return deviceType;
    }
    


}
