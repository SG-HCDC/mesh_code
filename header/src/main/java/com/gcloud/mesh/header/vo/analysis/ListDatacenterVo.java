package com.gcloud.mesh.header.vo.analysis;

import lombok.Data;

@Data
public class ListDatacenterVo {
	
    private String id;
    private String deviceType;
    private String meter;
    private String meterName;
    private String level;
    private Integer upperThreshold;
    private Integer lowerThreshold;
    private Boolean subhealth;

}
