package com.gcloud.mesh.header.vo.analysis;

import java.util.List;

import lombok.Data;

@Data
public class DoFitterVo {
	
	List<ChartVo> charts;

//	private ScatterChartVo scatter;
//	
//	private LineChartVo line;
	
}
