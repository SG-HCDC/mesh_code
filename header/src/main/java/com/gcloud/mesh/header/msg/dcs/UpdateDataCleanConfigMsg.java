package com.gcloud.mesh.header.msg.dcs;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;

import org.hibernate.validator.constraints.Length;

import com.gcloud.mesh.header.exception.CommonErrorCode;
import com.gcloud.mesh.header.exception.DataCleanErrorCode;
import com.gcloud.mesh.header.exception.SpecialCode;
import com.gcloud.mesh.header.msg.BaseMsg;

import lombok.Data;

@Data
public class UpdateDataCleanConfigMsg extends BaseMsg {

	@NotBlank( message = DataCleanErrorCode.ID_NOT_BLANK )
	@Length( min = 1, max = 60, message = "::数据清洗ID"+CommonErrorCode.LENGTH60)
	@SpecialCode(message = "::数据清洗ID"+CommonErrorCode.SPECIAL)
	private String id;
	
	private Boolean enabled;
	
	private String operateType;
	
//	private String source;
	
//	@Min( value = 0, message = DataCleanErrorCode.CLEAN_TIME_FORMAT_ERROR)
//	private Integer cleanTime;
//	
//	private String cleanType;

//	@Override
//	public String toString() {
//		String enableStr = "";
//		if(enabled != null) {
//			if(enabled) {
//				enableStr = "开启";
//			}else {
//				enableStr = "关闭";
//			}
//		}
//		String ctype = cleanType;
//		if(cleanType != null && cleanType.equals("ALERT_HISTORY")) {
//			ctype = "诊断告警记录";
//		}
//		String s = "";
//		if(cleanTime != null) {
//			Date beforeDate = BeforeDayNow(cleanTime);
//			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
//			s = sdf.format(beforeDate);
//		}
//		return " 是否开启=" + enableStr + ", 清洗时间=" + s + ", 清洗策略=" + ctype
//				+ "";
//	}
//	public static Date BeforeDayNow(int day) {
//		return diffDate(new Date(), 0, 0, day, 0, 0, 0, 0);
//	}
//	public static Date diffDate(Date date, int year, int month, int day, int hour, int min, int second, int millisecond) {
//		Calendar base = Calendar.getInstance();
//		base.setTime(date);
//		year = -year;
//		month = -month;
//		day = -day;
//		hour = -hour;
//		min = -min;
//		second = -second;
//		base.add(Calendar.YEAR, year);
//		base.add(Calendar.MONDAY, month);
//		base.add(Calendar.DAY_OF_MONTH, day);
//		base.add(Calendar.HOUR, hour);
//		base.add(Calendar.MINUTE, min);
//		base.add(Calendar.SECOND, second);
//		base.add(Calendar.MILLISECOND, millisecond);
//		return base.getTime();
//	}
	
	
}
