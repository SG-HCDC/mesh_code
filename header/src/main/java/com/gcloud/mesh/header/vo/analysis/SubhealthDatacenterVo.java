package com.gcloud.mesh.header.vo.analysis;

import lombok.Data;

@Data
public class SubhealthDatacenterVo {
	
    private String id;
	private String name;
    private String deviceType;
//    private int serverCount;
    private int airCount;
    private int upsCount;
    private Boolean subhealth;

}
