package com.gcloud.mesh.header.vo.dcs;

import lombok.Data;

@Data
public class BestScoreDatacenter {
	private String datacenterId;
}
