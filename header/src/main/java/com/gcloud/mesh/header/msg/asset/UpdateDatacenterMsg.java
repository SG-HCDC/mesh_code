package com.gcloud.mesh.header.msg.asset;

import javax.validation.constraints.Digits;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;

import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.Range;
import org.springframework.lang.Nullable;

import com.gcloud.mesh.header.constant.RegexpPattern;
import com.gcloud.mesh.header.exception.AssetErrorCode;
import com.gcloud.mesh.header.exception.CommonErrorCode;
import com.gcloud.mesh.header.exception.SpecialCode;
import com.gcloud.mesh.header.msg.BaseMsg;

import lombok.Data;

@Data
public class UpdateDatacenterMsg extends BaseMsg {
	
	@NotBlank( message = AssetErrorCode.ID_NOT_BLANK)
	@Length( min = 0, max = 60, message = AssetErrorCode.ID_LENGTH_ERROR)
	@Pattern( regexp = RegexpPattern.STAND_ID, message = AssetErrorCode.ID_FORMAT_ERROR)
	@SpecialCode(message = "::数据中心ID"+CommonErrorCode.SPECIAL)
	private String id;
	
	@Nullable
	@Length( min = 1, max = 60, message = AssetErrorCode.NAME_LENGTH_ERROR)
	@SpecialCode(message = "::数据中心名称"+CommonErrorCode.SPECIAL)
//	@Pattern( regexp = RegexpPattern.STAND_NAME, message = AssetErrorCode.NAME_FORMAT_ERROR)
	private String name;
	
	@Nullable
	@Pattern( regexp = RegexpPattern.IP, message = AssetErrorCode.IP_FORAMT_ERROR)
	private String ip;

	private Integer power;
	private Boolean powerConservation;
	//增加架构参数
	private String structure;

	//@Digits(integer = 6,fraction = 0, message = AssetErrorCode.THRESHOLD_OUT_RANGE)
	@Max(value = 9999, message = "::动环节能开启阈值在0-9999间")
	@Min(value = 0, message = "::动环节能开启阈值在0-9999间")
	// @Range(min=0, max=9999, message = "::动环节能开启阈值在0-9999间")
	private Integer threshold;
}
