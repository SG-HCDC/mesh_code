package com.gcloud.mesh.header.msg;

import lombok.Data;

@Data
public class MqBaseReplyMsg extends MqBaseMsg {

    private boolean success = true;
    private String errorCode;

    public MqBaseReplyMsg() {

    }

    public MqBaseReplyMsg(boolean success, String errorCode) {
        this.success = success;
        this.errorCode = errorCode;
    }
    
}
