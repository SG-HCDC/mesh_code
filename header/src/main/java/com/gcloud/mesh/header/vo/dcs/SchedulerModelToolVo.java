
package com.gcloud.mesh.header.vo.dcs;

import lombok.Data;

@Data
public class SchedulerModelToolVo {

    private String  name;
    private String  type;
    private Integer  value;
    private Boolean isCheck;
}
