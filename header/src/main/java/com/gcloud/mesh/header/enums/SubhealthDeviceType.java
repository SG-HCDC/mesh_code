
package com.gcloud.mesh.header.enums;

import com.gcloud.mesh.header.exception.BaseException;
import java.util.stream.Stream;

public enum SubhealthDeviceType {

    AIR("AIR", "空调亚健康"), // 空调
    UPS("UPS", "UPS亚健康"), // UPS
    DATACENTER("DATACENTER", "数据中心亚健康"); // 机房
	
	private String name;
	private String cnName;
	
	SubhealthDeviceType(String name, String cnName) {
		this.name = name;
		this.cnName = cnName;
	}

    public static SubhealthDeviceType checkAndGet(String type) throws BaseException {
        if (type != null) {
            for (SubhealthDeviceType t : SubhealthDeviceType.values()) {
                if (t.name().equals(type)) {
                    return t;
                }
            }
        }
        throw new BaseException("not found");
    }

    public static SubhealthDeviceType get(String type) {
        if (type != null) {
            for (SubhealthDeviceType t : SubhealthDeviceType.values()) {
                if (t.name().equals(type)) {
                    return t;
                }
            }
        }
        return null;
    }
    
    public static SubhealthDeviceType getByName(String name) {
    	
    	return Stream.of(SubhealthDeviceType.values())
    			.filter( s -> s.getName().equals(name))
    			.findFirst()
    			.orElse(null);
    }

    public String getName() {
    	return this.name;
    }
    
    public String getCnName() {
    	return this.cnName;
    }

}
