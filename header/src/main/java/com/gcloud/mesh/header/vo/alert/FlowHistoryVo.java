package com.gcloud.mesh.header.vo.alert;

import lombok.Data;

@Data
public class FlowHistoryVo {
	private String alertTime;
	private Integer sum;
}
