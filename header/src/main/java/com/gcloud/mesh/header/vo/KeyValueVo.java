
package com.gcloud.mesh.header.vo;

import lombok.Data;

@Data
public class KeyValueVo {

    private String key;
    private String value;

}
