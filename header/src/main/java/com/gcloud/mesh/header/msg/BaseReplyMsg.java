
package com.gcloud.mesh.header.msg;

import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain=true)
public class BaseReplyMsg extends BaseMsg {

    private boolean success = true;
    
    private Object data;
    
    private String errorCode;

    public BaseReplyMsg() {

    }

    public BaseReplyMsg(boolean success, String errorCode) {
        this.success = success;
        this.errorCode = errorCode;
    }

}
