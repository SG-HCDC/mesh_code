package com.gcloud.mesh.header.vo.monitor;

import java.util.Map;

import lombok.Data;

@Data
public class PollsterVo {

	private String id;
	private String name;
	private String pollster;
	private Map<String, String> params;
	private boolean isDefault;
	private String suitableDiscovery;
	private Integer defaultFrequency;
}
