package com.gcloud.mesh.header.enums;

import java.util.stream.Stream;

public enum DataClassificationLevel {

	GENERAL("general", "一般业务数据"),
	VITAL("vital", "重要业务数据");
	
	private String name;
	private String cnName;
	
	DataClassificationLevel(String name, String cnName) {
		this.name = name;
		this.cnName = cnName;
	}
	
	public static DataClassificationLevel getByName(String name) {
		return Stream.of(DataClassificationLevel.values())
				.filter( s -> s.getName().equals(name))
				.findFirst()
				.orElse(null);
	}

	public String getName() {
		return name;
	}

	public String getCnName() {
		return cnName;
	}
	
	
	
}
