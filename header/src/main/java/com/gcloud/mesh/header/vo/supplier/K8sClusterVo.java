package com.gcloud.mesh.header.vo.supplier;

import lombok.Data;

@Data
public class K8sClusterVo {

	private String clusterId;
	
	private String restveleroIp;
}
