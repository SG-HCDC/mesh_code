
package com.gcloud.mesh.header.msg.asset;

import javax.validation.constraints.*;

import org.hibernate.validator.constraints.Length;

import com.gcloud.mesh.header.exception.CommonErrorCode;
import com.gcloud.mesh.header.exception.SpecialCode;
import com.gcloud.mesh.header.msg.BaseMsg;
import com.gcloud.mesh.header.param.AliMigrationParam;
import com.gcloud.mesh.header.param.ClusterMigrationParam;
import com.gcloud.mesh.header.param.HuaweiMigrationParam;

import lombok.Data;

@Data
public class CreateCloudResourceMsg extends BaseMsg {
	@Length(min = 1, max = 10, message = "::云资源名称" + CommonErrorCode.LENGTH10)
	@SpecialCode(message = "::云资源名称" + CommonErrorCode.SPECIAL)
	private String name;
	private Integer type;
	// @NotBlank(message = "::数据中心ID不能为空")
	@Length(min = 1, max = 60, message = "::数据中心ID" + CommonErrorCode.LENGTH60)
	@SpecialCode(message = "::数据中心ID" + CommonErrorCode.SPECIAL)
	private String datacenterId;
	// @NotBlank(message = "::服务器ID不能为空")
	@Length(min = 1, max = 60, message = "::服务器ID" + CommonErrorCode.LENGTH60)
	@SpecialCode(message = "::服务器ID" + CommonErrorCode.SPECIAL)
	private String nodeId;

	@Max(value = 99, message = "::CPU核数" + CommonErrorCode.LENGTH1_2)
	@Min(value = 1, message = "::CPU核数" + CommonErrorCode.LENGTH1_2)
	private Integer cpuCores;

	@Max(value = 99, message = "::内存" + CommonErrorCode.LENGTH1_2)
	@Min(value = 1, message = "::内存" + CommonErrorCode.LENGTH1_2)
	private Integer memory;
	
	private String objectId;
	
    // k8s
    private ClusterMigrationParam clusterMigrationParam;

    // 华为
    private HuaweiMigrationParam huaweiMigrationParam;

    // 阿里
    private AliMigrationParam aliMigrationParam;

	@Override
	public String toString() {
		return " 名称=" + name + " 数据中心ID=" + datacenterId + " 节点ID=" + nodeId
				+ " cpu核数=" + cpuCores + " 内存=" + memory + "";
	}
	
	
	
	
	

}
