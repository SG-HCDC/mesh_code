
package com.gcloud.mesh.header.vo.analysis;

import java.util.Date;

import com.gcloud.mesh.header.valid.ValidId;

import lombok.Data;

@Data
public class SubhealthAlertVo {

    @ValidId
    private String id;
    private String deviceType;
    private String deviceTypeCnName;
    private String meter;
    private String meterName;
    private String resourceId;
    private String resourceName;
    private String level;
    private String levelCnName;
    private String content;
    private Date createTime;
    private String typeCount;

}
