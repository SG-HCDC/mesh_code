package com.gcloud.mesh.header.msg.alert;

import com.gcloud.mesh.header.msg.BaseReplyMsg;
import com.gcloud.mesh.header.vo.alert.PolicyVo;

import lombok.Data;

@Data
public class DetailPolicyReplyMsg extends BaseReplyMsg {
	private PolicyVo policy;
}
