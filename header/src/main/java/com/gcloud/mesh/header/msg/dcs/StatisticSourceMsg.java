package com.gcloud.mesh.header.msg.dcs;

import javax.validation.constraints.NotBlank;

import com.gcloud.mesh.header.exception.DataClassificationErrorCode;
import com.gcloud.mesh.header.msg.BaseMsg;

import lombok.Data;

@Data
public class StatisticSourceMsg extends BaseMsg {

	@NotBlank(message = DataClassificationErrorCode.LEVEL_NOT_BLANK)
	private String level;
	
}
