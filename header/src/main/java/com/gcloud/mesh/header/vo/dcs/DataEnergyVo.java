package com.gcloud.mesh.header.vo.dcs;

import lombok.Data;

@Data
public class DataEnergyVo {

	private String label;
	private Double value;
	private Long count;
	private String datacenterId;
	
	public DataEnergyVo() {
		
	}
	
	public DataEnergyVo(String label, Double value, Long count, String datacenterId) {
		this.label = label;
		this.value = value;
		this.count = count;
		this.datacenterId = datacenterId;
	}
}
