package com.gcloud.mesh.header.vo.dcs;

import java.util.List;

import lombok.Data;

@Data
public class SchedulerResultAnalysisVo {

	private Double temperatureImagine;

	private Double distributionBoxVoltageImagine;

	private Double datacenterEnergyImagine;

	private Double businessRespondImagine;

	private Double temperatureResult;

	private Double distributionBoxVoltageResult;

	private Double datacenterEnergyResult;

	private Double businessRespondResult;

	private Double temperatureResultDifference;

	private Double distributionBoxVoltageResultDifference;

	private Double datacenterEnergyResultDifference;

	private Double businessRespondResultDifference;
	
	private String id;
	private List<SchedulerEffectPredictionFactorVo> factors;
}
