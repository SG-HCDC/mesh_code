package com.gcloud.mesh.header.msg.asset;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;

import org.hibernate.validator.constraints.Length;

import com.gcloud.mesh.header.constant.RegexpPattern;
import com.gcloud.mesh.header.exception.AssetErrorCode;
import com.gcloud.mesh.header.exception.CommonErrorCode;
import com.gcloud.mesh.header.exception.SpecialCode;
import com.gcloud.mesh.header.msg.BaseMsg;

import lombok.Data;

@Data
public class CreateNodeSmMsg extends BaseMsg {
	
	@NotBlank( message = AssetErrorCode.NAME_NOT_BLANK)
	@Length( min = 1, max = 10, message = AssetErrorCode.NAME_LENGTH_ERROR)
	@SpecialCode(message = "::设备名称"+CommonErrorCode.SPECIAL)
//	@Pattern( regexp = RegexpPattern.STAND_NAME, message = AssetErrorCode.NAME_FORMAT_ERROR)
	private String name;
	
	@NotBlank( message = AssetErrorCode.ESN_NOT_BLANK)
	@Length( min = 1, max = 10, message = AssetErrorCode.ESN_LENGTH_ERROR)
	@SpecialCode(message = "::设备识别号"+CommonErrorCode.SPECIAL)
//	@Pattern( regexp = RegexpPattern.ONLY_NUMBER_CHAR, message = AssetErrorCode.ESN_FORMAT_ERROR)
	private String esn;
	
	@NotBlank( message = AssetErrorCode.MODEL_NOT_BLANK)
	@Length( min = 1, max = 10, message = AssetErrorCode.MODEL_LENGTH_ERROR)
	@SpecialCode(message = "::设备型号"+CommonErrorCode.SPECIAL)
//	@Pattern( regexp = RegexpPattern.STAND_NAME, message = AssetErrorCode.MODEL_FORMAT_ERROR)
	private String deviceModel;
	
	@NotBlank( message = AssetErrorCode.MANUFACTURER_NOT_BLANK)
	@Length( min = 1, max = 10, message = AssetErrorCode.MANUFACTURER_LENGTH_ERROR)
	@SpecialCode(message = "::设备厂商"+CommonErrorCode.SPECIAL)
//	@Pattern( regexp = RegexpPattern.STAND_NAME, message = AssetErrorCode.MANUFACTURER_FORMAT_ERROR)
	private String deviceManufacturer;
	
	@NotBlank( message = AssetErrorCode.DATACENTER_ID_NOT_BLANK)
	@Length( min = 1, max = 60, message = AssetErrorCode.DATACENTER_ID_LENGTH_ERROR)
	@Pattern( regexp = RegexpPattern.STAND_ID, message = AssetErrorCode.DATACENTER_ID_FORMAT_ERROR)
	@SpecialCode(message = "::数据中心ID"+CommonErrorCode.SPECIAL)
	private String datacenterId;
	
	@NotBlank( message = AssetErrorCode.MGM_IP_NOT_BLANK)
	@Pattern( regexp = RegexpPattern.IP, message = AssetErrorCode.MGM_IP_FORMAT_ERROR)
	private String mgmIp;
	
	@NotBlank( message = AssetErrorCode.IPMI_IP_NOT_BLANK)
	@Pattern( regexp = RegexpPattern.IP, message = AssetErrorCode.IPMI_IP_FORMAT_ERROR)
	private String ipmiIp;
	
	@NotBlank( message = AssetErrorCode.IPMI_USER_NOT_BLANK)
	@Length( min = 1, max = 10, message = CommonErrorCode.LENGTH10)
	@SpecialCode(message = "::带外管理用户"+CommonErrorCode.SPECIAL)
	private String ipmiUser;
	
	@NotBlank( message = AssetErrorCode.HOSTNAME_NOT_BLANK)
	@Length( min = 1, max = 10, message = CommonErrorCode.LENGTH10)
	@SpecialCode(message = "::主机名"+CommonErrorCode.SPECIAL)
	//@Pattern( regexp = RegexpPattern.STAND_NAME, message = AssetErrorCode.HOSTNAME_FORMAT_ERROR)
	private String hostname;
	
	@Length( min = 1, max = 200, message = "::重要数据"+CommonErrorCode.LENGTH200)
	private String smData;
	
//	@Length( min = 1, max = 60, message = "::额定功率"+CommonErrorCode.LENGTH60)
	private String ratedPowerSm;
	
//	@Length( min = 1, max = 60, message = "::额定电压"+CommonErrorCode.LENGTH60)
	private String ratedVoltageSm;
	
	private String ipmiPassword;
	
	private String cabinetId;

	@Override
	public String toString() {
		return " 名称=" + name + " 设备识别号=" + esn + " 设备型号=" + deviceModel + " 设备厂商="
				+ deviceManufacturer + " 管理IP=" + mgmIp + " 主机名=" + hostname + "";
	}
	
	
}
