package com.gcloud.mesh.header.vo.analysis;

import lombok.Data;

@Data
public class NodeFitterVo {

	private String Id;
	private String name;
	private String ipmiIp;
	private String mgmIp;
	private String region;
	private String datacenterId;
	private String powerConsumption;
	private String powerMode;
	private Integer powerStatus;
	private String powerStatusCnName;
	
}
