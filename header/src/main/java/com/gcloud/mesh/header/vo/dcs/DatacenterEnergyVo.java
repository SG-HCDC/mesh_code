package com.gcloud.mesh.header.vo.dcs;

import java.util.List;

import lombok.Data;

@Data
public class DatacenterEnergyVo {

	private StatisticsVo statistics;
	private List<DataEnergyVo> list;
	
}
