package com.gcloud.mesh.header.msg.dcs;

import javax.validation.constraints.NotBlank;

import com.gcloud.mesh.header.exception.DataClassificationErrorCode;
import com.gcloud.mesh.header.msg.BaseMsg;

import lombok.Data;

@Data
public class StatisticsOverviewMsg extends BaseMsg {

	@NotBlank(message = DataClassificationErrorCode.LEVEL_NOT_BLANK)
	private String level;
	
//	@NotBlank(message = DataClassificationErrorCode.PERIOD_NOT_BLANK)
	private String period;
}
