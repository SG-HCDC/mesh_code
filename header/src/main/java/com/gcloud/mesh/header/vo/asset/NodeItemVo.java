package com.gcloud.mesh.header.vo.asset;

import java.util.Date;

import lombok.Data;

@Data
public class NodeItemVo {
	
	private String id;
	private String name;
	private String esn;
	private Integer type;
	private String typeCnName;
	private String deviceId;
	private String deviceModel;
	private String deviceManufacturer;
	private String datacenterId;
	private String datacenterName;
	private String datacenterIp;
//	private String creator;
	private Date createTime;
	private String mgmIp;
	private String ipmiIp;
	private String ipmiUser;
	private String ipmiPassword;
	private Integer networkStatus;
	private String networkStatusCnName;
	private Integer powerStatus;
	private String powerStatusCnName;
	private String hostname;
	private Float ratedPower;
	private Float powerConsumption; 
	private Float energyEfficiencyRatio;
	private Float storageCapacity;
	private Boolean isolation;
	private String from;
	private String ratedVoltageSm;
	private String ratedPowerSm;
	private String smData;
	private String cabinetId;

}
