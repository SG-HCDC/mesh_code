
package com.gcloud.mesh.header.msg;

import org.hibernate.validator.constraints.Range;

import com.gcloud.mesh.header.exception.CommonErrorCode;

import lombok.Data;

@Data
public class BaseQueryMsg extends BaseMsg {

    @Range(min = 1, max = 999999, message = CommonErrorCode.PAGE_NO_ERROR)
    private Integer pageNo = 1;
    @Range(min = 1, max = 999999, message = CommonErrorCode.PAGE_SIZE_ERROR)
    private Integer pageSize = 10;

}
