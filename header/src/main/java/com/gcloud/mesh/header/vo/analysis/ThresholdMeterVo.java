package com.gcloud.mesh.header.vo.analysis;

import lombok.Data;

@Data
public class ThresholdMeterVo {

	private String name;
	private String meter;
	private String type;
	private String cnName;
	
	public ThresholdMeterVo() {
		
	}
	
	public ThresholdMeterVo(String type, String name, String cnName, String meter) {
		this.name = name;
		this.cnName = cnName;
		this.type = type;
		this.meter = meter;
	}
}
