package com.gcloud.mesh.header.vo.supplier.AliPower;

import lombok.Data;

import java.util.Date;

@Data
public class AliPowerDeviceMapVo {
    private String administrator;
    private String business_type;
    private String cpu_type;
    private Date create_time;
    private String data_center;
    private String device_id;
    private String device_model;
    private Date drive_release_time;
    private String driver_version;
    private String gpu_type;
    private String hard_disk;
    private String hardware_version;
    private String host_name;
    private String incompatible_feature;
    private String ip;
    private String ipv6;
    private String located_cabinet;
    private String location_id;
    private String logical_site;
    private String maintenance_provider;
    private Date manufacture_date;
    private String manufacture_type;
    private String manufacturer;
    private String memory;
    private Date modified_time;
    private String network_card;
    private String oob_ip;
    private String oob_mac;
    private String po;
    private String purchase_date;
    private String rated_power;
    private String remarks;
    private String room;
    private String save_function;
    private String type;
    private String usage_status;
}
