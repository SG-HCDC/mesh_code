package com.gcloud.mesh.header.enums;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public enum CalendarMonth {

	JANUARY(1, "01", "Jan", "一月", 1),
	FEBRUARY(2, "02", "Feb", "二月", 1),
	MARCH(3, "03", "Mar", "三月", 1),
	APRIL(4, "04", "Apr", "四月", 2),
	MAY(5, "05", "May", "五月", 2),
	JUNE(6, "06", "Jun", "六月", 2),
	JULY(7, "07", "July", "七月", 3),
	AUGUST(8, "08", "Aug", "八月", 3),
	SEPTEMBER(9, "09", "Sept", "九月", 3),
	OCTOBER(10, "10", "Oct", "十月", 4),
	NOVEMBER(11, "11", "Nov", "十一月", 4),
	DECEMBER(12, "12", "Dec", "十二月", 4);
	
	private int no;
	private String num;
	private String name;
	private String cnName;
	private int quarter;
	
	CalendarMonth(int no, String num, String name, String cnName, int quarter) {
		this.no = no;
		this.num = num;
		this.name = name;
		this.cnName = cnName;
		this.quarter = quarter;
	}
	
	public static CalendarMonth getByNo(int no) {
		return Stream.of(CalendarMonth.values())
				.filter( s -> s.getNo() == no)
				.findFirst()
				.orElse(null);
	}
	
	public static CalendarMonth getByNum(String num) {
		return Stream.of(CalendarMonth.values())
				.filter( s -> s.getNum().equals(num))
				.findFirst()
				.orElse(null);
	}
	
	public static CalendarMonth getByName(String name) {
		return Stream.of(CalendarMonth.values())
				.filter( s -> s.getName().equals(name))
				.findFirst()
				.orElse(null);
	}
	
	public static List<CalendarMonth> listByQuarter(int quarter) {
		return Stream.of(CalendarMonth.values())
				.filter( s -> s.getQuarter() == quarter)
				.collect(Collectors.toList());
	}

	public int getNo() {
		return no;
	}

	public String getName() {
		return name;
	}

	public String getCnName() {
		return cnName;
	}

	public int getQuarter() {
		return quarter;
	}

	public String getNum() {
		return num;
	}
	
	
}
