
package com.gcloud.mesh.header.vo.dcs;

import lombok.Data;

@Data
public class SchedulerStrategyRuleVo {

	private String rule;
}
