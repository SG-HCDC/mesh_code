
package com.gcloud.mesh.header.msg.dcs;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;

import org.hibernate.validator.constraints.Length;

import com.gcloud.mesh.header.constant.RegexpPattern;
import com.gcloud.mesh.header.exception.AssetErrorCode;
import com.gcloud.mesh.header.exception.CommonErrorCode;
import com.gcloud.mesh.header.exception.ModelErrorCode;
import com.gcloud.mesh.header.exception.SpecialCode;
import com.gcloud.mesh.header.msg.BaseQueryMsg;

import lombok.Data;

@Data
public class PageModelScoreMsg extends BaseQueryMsg {

	@NotBlank( message = ModelErrorCode.MODEL_TYPE_NOT_BLANK)
	@SpecialCode(message = "::模型类型"+CommonErrorCode.SPECIAL)
    private String modelType;
	
	@Length( min = 1, max = 60, message = "::数据中心ID"+CommonErrorCode.LENGTH60)
	@SpecialCode(message = "::数据中心ID"+CommonErrorCode.SPECIAL)
    private String datacenterId;
    
	@Length( min = 1, max = 10, message = "::设备名称"+CommonErrorCode.LENGTH10)
	@SpecialCode(message = "::设备名称"+CommonErrorCode.SPECIAL)
    private String deviceName;
	
    private Boolean isIllness;
    private Boolean isolation;
}
