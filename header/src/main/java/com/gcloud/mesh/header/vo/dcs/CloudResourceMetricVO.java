package com.gcloud.mesh.header.vo.dcs;

import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class CloudResourceMetricVO {
    private String cloudResourceId;
    
    private Integer type;
    // cpu 使用率
    private String cupVal;
    // 内存 使用率
    private String memVal;
    
}
