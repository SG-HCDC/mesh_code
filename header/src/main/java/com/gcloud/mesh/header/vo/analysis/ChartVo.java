package com.gcloud.mesh.header.vo.analysis;

import java.util.List;

import lombok.Data;

@Data
public class ChartVo {

	private String xLabel;
	
	private String yLabel;
	
	private String type;
	
	private List<DataVo> data;
	
}
