package com.gcloud.mesh.header.vo.monitor;

import lombok.Data;

@Data
public class CollectorVo {

	private String id;
	private PollsterVo pollster;
	private String resourceId;
	private String resourceType;
	private Integer frequency;
}
