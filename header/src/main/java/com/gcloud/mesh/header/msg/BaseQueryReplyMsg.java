
package com.gcloud.mesh.header.msg;

import lombok.Data;

@Data
public class BaseQueryReplyMsg extends BaseMsg {

    protected Object list;
    private Integer pageNo;
    private Integer pageSize;
    private Integer totalCount;
}
