package com.gcloud.mesh.header.vo.analysis;

import lombok.Data;

@Data
public class DataVo {
	
	private String timestamp;

	private Double dataX;
	
	private Double dataY;
	
	public DataVo() {
		
	}
	
	public DataVo(String timestamp, double dataX, double dataY) {
		this.timestamp = timestamp;
		this.dataX = dataX;
		this.dataY = dataY;
	}
	
	
}
