
package com.gcloud.mesh.header.msg.dcs;

import javax.validation.constraints.NotBlank;

import org.apache.commons.lang3.StringUtils;
import org.hibernate.validator.constraints.Length;

import com.gcloud.mesh.header.exception.AppProfileErrorCode;
import com.gcloud.mesh.header.exception.CommonErrorCode;
import com.gcloud.mesh.header.exception.SpecialCode;
import com.gcloud.mesh.header.msg.BaseMsg;

import lombok.Data;

@Data
public class UpdateProfileSceneChooseMsg extends BaseMsg {

	@NotBlank( message = AppProfileErrorCode.APPID_NOT_BLANK)
	@Length( min = 1, max = 60, message = "::应用ID"+CommonErrorCode.LENGTH60)
	//@SpecialCode(message = "::应用ID"+CommonErrorCode.SPECIAL)
    private String appId;
	
	@NotBlank( message = AppProfileErrorCode.SCENECHOOSE_NOT_BLANK)
    private String sceneChoose;
	
	



	@Override
	public String toString() {
		
		StringBuffer buffer = new StringBuffer();
		if(StringUtils.isNotBlank(sceneChoose)) {
			switch(sceneChoose){
				case "OFFLINE" :
			       buffer.append(" 应用场景=离线型");
			       break; //可选
			    case "REALTIME" :
			       buffer.append(" 应用场景=实时型");
			       break; //可选
			    case "SEQUENTIAL" :
			       buffer.append(" 应用场景=时序型");
			       break; //可选
		    default : //可选
			}
	
		}
		return buffer.toString();
	}

}
