
package com.gcloud.mesh.header.vo.dcs;

import java.util.Date;

import lombok.Data;

@Data
public class AppItemVo {

	private String id;
	private String name;
	private String cloudResourceId;
	private String cloudResourceName;
	private String typeName;
	private Integer type;
	private Date createTime;
	private String datacenterId;
	private String datacenterName;
	
}
