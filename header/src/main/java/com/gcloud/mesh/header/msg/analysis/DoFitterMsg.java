package com.gcloud.mesh.header.msg.analysis;

import java.util.Date;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

import org.apache.commons.lang3.StringUtils;
import org.hibernate.validator.constraints.Length;
import org.springframework.format.annotation.DateTimeFormat;

import com.gcloud.mesh.header.constant.RegexpPattern;
import com.gcloud.mesh.header.exception.CommonErrorCode;
import com.gcloud.mesh.header.msg.BaseMsg;

import lombok.Data;

@Data
public class DoFitterMsg extends BaseMsg {

//	@NotBlank( message = "")
//	@Length( min = 1, max = 60, message = "")
//	@Pattern( regexp = RegexpPattern.STAND_NAME, message = "")
	@Length( min = 1, max = 60, message = "::服务器ID"+CommonErrorCode.LENGTH60)
	private String nodeId;
	
//	@NotBlank( message = "")
//	@Length( min = 1, max = 60, message = "")
//	@Pattern( regexp = RegexpPattern.STAND_NAME, message = "")
	@Length( min = 1, max = 60, message = "::空调ID"+CommonErrorCode.LENGTH60)
	private String airId;
	
//	@NotBlank( message = "")
//	@Length( min = 1, max = 60, message = "")
//	@Pattern( regexp = RegexpPattern.STAND_NAME, message = "")
	private String metricX;
	
//	@NotBlank( message = "")
//	@Length( min = 1, max = 60, message = "")
//	@Pattern( regexp = RegexpPattern.STAND_NAME, message = "")
	private String metricY;
	
//	@NotNull( message = "")
	private Integer degree;
	
//	@NotNull( message = "")
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private Date startTime;
	
//	@NotNull( message = "")
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private Date endTime;

	@Override
	public String toString() {
		StringBuffer buffer = new StringBuffer();
		buffer.append("拟合=关键设备节点传导性拟合");
		if(StringUtils.isNotBlank(nodeId)) {
			buffer.append(" 服务器ID="+nodeId);
		}
		if(StringUtils.isNotBlank(airId)) {
			buffer.append(" 制冷设备ID="+airId);
		}
		if(startTime != null) {
			buffer.append(" 开始时间="+startTime);
		}
		if(endTime != null) {
			buffer.append(" 结束时间="+endTime);
		}
		return buffer.toString();
	}
	
	
	
}
