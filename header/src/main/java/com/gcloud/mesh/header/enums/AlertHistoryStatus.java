package com.gcloud.mesh.header.enums;

public enum AlertHistoryStatus {
	TRIGGER_ALARM("trigger_alarm", "触发告警", 0),
	SEND_ALARM("send_alarm", "发送告警", 1),
	CHANNEL_SILENCE("channel_silence", "通道沉默", 2),
	NONE_NOTIFY("none_notify", "不发送告警", 3),
	NOTIFY_FAILED("notify_failed","发送告警失败", 4);
	
	private String enValue;
	private String cnValue;
	private int value;
	
	private AlertHistoryStatus(String enValue, String cnValue, int value) {
		this.enValue = enValue;
		this.cnValue = cnValue;
		this.value = value;
	}

	public String getEnValue() {
		return enValue;
	}
	
	public String getCnValue() {
		return cnValue;
	}

	public int getValue() {
		return value;
	}
	
	//根据状态返回保存到数据库的编码
	public static int getValueByEnValue(String enValue) {
		for (AlertHistoryStatus status : AlertHistoryStatus.values()) {
			if(status.getEnValue().equals(enValue)) {
				return status.getValue();
			}
		}
		return -1;
	}
	
	public static String getEnValueByValue(int value) {
		for (AlertHistoryStatus status : AlertHistoryStatus.values()) {
			if(status.getValue() == value) {
				return status.getEnValue();
			}
		}
		return "unknow";
	}
	
	public static String getCnValueByValue(int value) {
		for (AlertHistoryStatus status : AlertHistoryStatus.values()) {
			if(status.getValue() == value) {
				return status.getCnValue();
			}
		}
		return "未知状态";
	}
}
