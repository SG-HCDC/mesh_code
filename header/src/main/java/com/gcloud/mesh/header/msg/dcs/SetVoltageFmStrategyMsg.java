package com.gcloud.mesh.header.msg.dcs;

import com.gcloud.mesh.header.constant.RegexpPattern;
import com.gcloud.mesh.header.exception.CommonErrorCode;
import com.gcloud.mesh.header.exception.DvfsErrorCode;
import com.gcloud.mesh.header.exception.SpecialCode;
import com.gcloud.mesh.header.msg.BaseMsg;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import org.apache.commons.lang3.StringUtils;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.Range;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

@Data
@NotNull
public class SetVoltageFmStrategyMsg extends BaseMsg {
    @ApiModelProperty(value = "设备id",required = true)
    @NotBlank
    @Pattern( regexp = RegexpPattern.STAND_ID, message = DvfsErrorCode.ID_FORMAT_ERROR)
    @Length( min = 1, max = 60, message = DvfsErrorCode.ID_LENGTH_ERROR)
    @SpecialCode(message = "::电压调频ID"+CommonErrorCode.SPECIAL)
    private String id;

    @ApiModelProperty(value = "调频策略",required = true)
    @Pattern( regexp = RegexpPattern.STAND_NAME, message = DvfsErrorCode.NAME_FORMAT_ERROR)
    @Length( min = 1, max = 60, message = DvfsErrorCode.NAME_LENGTH_ERROR)
    @SpecialCode(message = "::调频策略"+CommonErrorCode.SPECIAL)
    private String voltageFmStrategy;

    @ApiModelProperty(value = "用户定义的频率,仅策略为USERSPACE时可用")
    // @Range(min = 0,max = 10*1000)
	@Max(value = 10*1000, message = "::频率大小范围： 0-10*1000")
	@Min(value = 0, message = "::频率大小范围：0-10*1000")
    private Float userFrequency;

    @ApiModelProperty(value = "控制方式，0为自动，1为手动")
    // @Range(min = 0,max = 1)
    private Integer controlType;

	@Override
	public String toString() {
		StringBuffer buffer = new StringBuffer();
	
		if(userFrequency != null) {
			buffer.append(" 频率="+userFrequency);
		}
		if(controlType != null) {
			buffer.append("功耗控制方式=").append(controlType == 0 ? "自动":"手动");
			if(StringUtils.isNotBlank(voltageFmStrategy)) {
				buffer.append(" 调频策略=" + voltageFmStrategy + "");
			}
		}else {
			if(StringUtils.isNotBlank(voltageFmStrategy)) {
				buffer.append(" 动态电压调频策略=" + voltageFmStrategy + "");
			}
		}
		return buffer.toString();
	}
    
    
    
}
