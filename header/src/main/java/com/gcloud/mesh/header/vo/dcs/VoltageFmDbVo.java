package com.gcloud.mesh.header.vo.dcs;

import lombok.Data;

@Data
public class VoltageFmDbVo {
    private String id;
    private String factorName;
    private String ipmiIp;
    private String datacenterId;
    private String datacenterName;
    private Float ratedPower;
    private Float powerConsumption;
    private String voltageFmStrategy;
    private String supportStrategies;
    private Integer controlType;
}
