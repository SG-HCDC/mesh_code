
package com.gcloud.mesh.header.msg.dcs;

import javax.validation.constraints.NotNull;

import com.gcloud.mesh.header.exception.SchedulerErrorCode;
import com.gcloud.mesh.header.msg.BaseMsg;

import lombok.Data;

@Data
public class UpdateSchedulerStrategyConfigMsg extends BaseMsg {

	@NotNull( message = SchedulerErrorCode.CONFIG_NOT_NULL)
	private Integer config;

	@Override
	public String toString() {
		String configstr = "";
		if(config != null ) {
			if(config.equals(0)) {
				configstr = " 迁移策略=全量迁移";
			}else {
				configstr = " 迁移策略=模板迁移";
			}
			
		}
		return configstr;
	}
	
	
}
