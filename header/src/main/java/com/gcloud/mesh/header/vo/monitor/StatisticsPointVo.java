package com.gcloud.mesh.header.vo.monitor;

import lombok.Data;

@Data
public class StatisticsPointVo{
	private String time;
	private Double value;
}
