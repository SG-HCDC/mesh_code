
package com.gcloud.mesh.header.msg.asset;

import org.hibernate.validator.constraints.Length;

import com.gcloud.mesh.header.exception.CommonErrorCode;
import com.gcloud.mesh.header.exception.SpecialCode;
import com.gcloud.mesh.header.msg.BaseMsg;

import lombok.Data;

@Data
public class UpdateCloudResourceMsg extends BaseMsg {
	@Length(min = 1, max = 60, message = "::资源ID" + CommonErrorCode.LENGTH60)
	@SpecialCode(message = "::资源ID" + CommonErrorCode.SPECIAL)
	private String id;
	@Length(min = 1, max = 10, message = "::资源名称" + CommonErrorCode.LENGTH10)
	@SpecialCode(message = "::资源名称" + CommonErrorCode.SPECIAL)
	private String name;
	@Length(min = 1, max = 60, message = "::数据中心ID" + CommonErrorCode.LENGTH60)
	@SpecialCode(message = "::数据中心ID" + CommonErrorCode.SPECIAL)
	private String datacenterId;
	private String expand;

	@Override
	public String toString() {
		return " 名称=" + name + " 数据中心id=" + datacenterId + "";
	}

}
