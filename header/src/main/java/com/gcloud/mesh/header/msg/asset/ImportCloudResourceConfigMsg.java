package com.gcloud.mesh.header.msg.asset;

import com.gcloud.mesh.header.param.AliMigrationParam;
import com.gcloud.mesh.header.param.ClusterMigrationParam;
import com.gcloud.mesh.header.param.HuaweiMigrationParam;

import lombok.Data;
@Data
public class ImportCloudResourceConfigMsg {
	
	private String cloudResourceId;
	
	private String datacenterId;
	
	private Integer type;
	  // k8s
    private ClusterMigrationParam clusterMigrationParam;

    // 华为
    private HuaweiMigrationParam huaweiMigrationParam;

    // 阿里
    private AliMigrationParam aliMigrationParam;
}
