package com.gcloud.mesh.header.vo.dcs;

import lombok.Data;

@Data
public class PageAppModelScoreVo {
    private String id;
    private String name;
    private String datacenterId;
    private String datacenterName;
    private String resourceId;
    private Integer totalScore;
    private String scores;
    private String factorNames;
    	
}
