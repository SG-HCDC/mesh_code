package com.gcloud.mesh.header.enums;

import java.util.stream.Stream;

public enum SchedulerTriggerPolicy {
	
	SERVER_POWER("ServerPower", "服务器实时功率", "W"),
	CLUSTER_CPU_CORES("ClusterCpuCores", "服务器CPU利用率", "%"),
	CLUSTER_MEM_SPACE("ClusterMemSpace", "服务器内存利用率", "%"),
	ALI_CPU_CORES("AliCpuCores", "阿里云cpu使用率","%"),
	ALI_MEM_SPACE( "AliMemSpace", "阿里云内存使用率","%"),

	HUAWEI_CPU_CORES( "HuaweiCpuCores", "华为云cpu使用率","%"),
	HUAWEI_MEM_SPACE( "HuaweiMemSpace", "华为云内存使用率","%");


	
	private String meter;
	private String meterCnName;
	private String unit;
	
	SchedulerTriggerPolicy(String meter, String meterCnName, String unit) {
		this.meter = meter;
		this.meterCnName = meterCnName;
		this.unit = unit;
	}
	
	public static SchedulerTriggerPolicy getByMeter(String meter) {
		return Stream.of(SchedulerTriggerPolicy.values())
				.filter( s -> s.getMeter().equals(meter))
				.findFirst()
				.orElse(null);
	}

	public String getMeter() {
		return meter;
	}

	public String getMeterCnName() {
		return meterCnName;
	}

	public String getUnit() {
		return unit;
	}
	
	
	

}
