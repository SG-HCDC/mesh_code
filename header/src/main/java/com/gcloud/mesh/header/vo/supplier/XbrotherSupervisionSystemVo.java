package com.gcloud.mesh.header.vo.supplier;

import lombok.Data;

@Data
public class XbrotherSupervisionSystemVo extends SupervisionSystemAbstract {
	
	private static final String TYPE = "xbrother";
	
	private String hostIp;
	
	private String loginCode;

	@Override
	public String getType() {
		// TODO Auto-generated method stub
		return TYPE;
	}
	
	

}
