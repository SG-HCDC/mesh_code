package com.gcloud.mesh.header.exception;

public class AppErrorCode {

	public static final String NAME_NOT_BLANK = "140101";
	public static final String NAME_DUPLICATE = "140102";
	public static final String CLOUDRESOURCEID_NOT_BLANK = "140103";
	public static final String TYPE_NOT_NULL = "140104";
	public static final String TYPE_FORMAT_ERROR = "140105";
	public static final String APPID_NOT_BLANK = "140106";
	public static final String DATACENTERID_NOT_BLANK = "140107";
	public static final String THRESHOLDKEY_NOT_BLANK = "140108";
	public static final String THRESHOLDOPERATION_NOT_BLANK = "140109";
	public static final String THRESHOLDVALUE_NOT_NULL = "140110";
	
	public static final String APP_NOT_EXIST = "140201";
	public static final String DATACENTERID_NOT_EXIST = "140202";
	public static final String APP_SYNC_NOT_UPDATE = "140203";
	
	
}
