
package com.gcloud.mesh.header.msg.dcs;

import org.hibernate.validator.constraints.Length;

import com.gcloud.mesh.header.exception.CommonErrorCode;
import com.gcloud.mesh.header.exception.SpecialCode;
import com.gcloud.mesh.header.msg.BaseQueryMsg;

import lombok.Data;

@Data
public class PageVmMsg extends BaseQueryMsg {

	private Integer type;
	
	@Length( min = 1, max = 10, message = "::应用名称"+CommonErrorCode.LENGTH10)
	@SpecialCode(message = "::应用名称"+CommonErrorCode.SPECIAL)
	private String name;
	
	@Length( min = 1, max = 60, message = "::应用ID"+CommonErrorCode.LENGTH60)
	@SpecialCode(message = "::应用ID"+CommonErrorCode.SPECIAL)
	private String cloudResourceId;
	
	private String datacenterId;
}
