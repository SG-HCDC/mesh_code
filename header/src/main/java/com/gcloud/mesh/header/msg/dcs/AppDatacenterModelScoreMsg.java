
package com.gcloud.mesh.header.msg.dcs;

import com.gcloud.mesh.header.msg.BaseQueryMsg;

import lombok.Data;

@Data
public class AppDatacenterModelScoreMsg extends BaseQueryMsg {
	private String datacenterId;
	
}
