package com.gcloud.mesh.header.constant;

public class RegexpPattern {

	public static final String STAND_NAME = "^[0-9a-zA-Z_]{1,}$";
	
	public static final String ONLY_NUMBER_CHAR = "^[0-9a-zA-Z]{1,}$";
	
	public static final String IP = "^(\\d{1,2}|1\\d\\d|2[0-4]\\d|25[0-5])\\.(\\d{1,2}|1\\d\\d|2[0-4]\\d|25[0-5])\\.(\\d{1,2}|1\\d\\d|2[0-4]\\d|25[0-5])\\.(\\d{1,2}|1\\d\\d|2[0-4]\\d|25[0-5])$";

	public static final String STAND_ID = "^[0-9a-zA-Z_-]{1,}$";
	
	public static final String NUMBER = "^[0-9]*[1-9][0-9]*$";
}
