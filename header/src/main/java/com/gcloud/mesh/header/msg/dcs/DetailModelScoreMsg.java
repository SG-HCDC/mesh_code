
package com.gcloud.mesh.header.msg.dcs;

import javax.validation.constraints.NotBlank;

import com.gcloud.mesh.header.exception.CommonErrorCode;
import com.gcloud.mesh.header.exception.ModelErrorCode;
import com.gcloud.mesh.header.exception.SpecialCode;
import com.gcloud.mesh.header.msg.BaseMsg;
import com.gcloud.mesh.header.valid.ValidId;

import lombok.Data;

@Data
public class DetailModelScoreMsg extends BaseMsg {

	@NotBlank( message = ModelErrorCode.MODEL_TYPE_NOT_BLANK)
//	@SpecialCode(message = "::模型ID"+CommonErrorCode.SPECIAL)
    private String modelType;
	
//    @ValidId
    private String resourceId;

    private String datacenterId;
}
