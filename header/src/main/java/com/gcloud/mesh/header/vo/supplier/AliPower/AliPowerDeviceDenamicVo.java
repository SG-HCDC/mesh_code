package com.gcloud.mesh.header.vo.supplier.AliPower;

import lombok.Data;

@Data
public class AliPowerDeviceDenamicVo {
    private String collect_time;
    private String comment_freq;
    private String cpu_freq;
    private String cpu_margin_temp;
    private String cpu_temp;
    private String cpu_usage;
    private String cpu_vr_temp;
    private String dimmg_temp;
    private String fan_rpm;
    private String frame_power;
    private String gmt_create;
    private String gmt_modified;
    private String id;
    private String inlet_temp;
    private String nvme_temp;
    private String outlet_temp;
    private String pch_temp;
    private String power;
    private String ps_power;
    private String ps_status;
    private String qos;
    private String server_sn;
    private String uncapping_freq;
}
