package com.gcloud.mesh.header.vo.alert;

import lombok.Data;

@Data
public class NotifyTemplateVo {
	private String id;
    private String name;
    private String title;
    private String content;
}
