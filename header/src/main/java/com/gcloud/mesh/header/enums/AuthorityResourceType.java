package com.gcloud.mesh.header.enums;

import java.util.stream.Stream;

public enum AuthorityResourceType {
	
	DISTRIBUTE_BOX("distribution_box", "配电设备", AuthorityResourceClassification.IAAS),
	AIR("air", "制冷设备", AuthorityResourceClassification.IAAS),
	PDU("pdu", "pdu", AuthorityResourceClassification.IAAS),
	UPS("ups", "ups", AuthorityResourceClassification.IAAS),
	TEMPERTURE_SENSOR("temperture_sensor", "温度传感器", AuthorityResourceClassification.IAAS),
	CABINET("cabinet", "机柜", AuthorityResourceClassification.IAAS),
	SERVER("server", "服务器", AuthorityResourceClassification.IT_DEVICE),
	SWITCHER("switcher", "交换机", AuthorityResourceClassification.IT_DEVICE),
	VM("vm", "虚拟机", AuthorityResourceClassification.CLOUD),
	K8S("k8s", "交换机", AuthorityResourceClassification.CLOUD),
	APP("app", "应用资源", AuthorityResourceClassification.APP);
	
	private String name;
	private String cnName;
	private AuthorityResourceClassification classification;
	
	AuthorityResourceType(String name, String cnName, AuthorityResourceClassification classification) {
		this.name = name;
		this.cnName = cnName;
		this.classification = classification;
	}
	
	public static AuthorityResourceType getByName(String name) {
		return Stream.of(AuthorityResourceType.values())
				.filter( s -> s.getName().equals(name))
				.findFirst()
				.orElse(null);
	}

	public String getName() {
		return name;
	}

	public String getCnName() {
		return cnName;
	}

	public AuthorityResourceClassification getClassification() {
		return classification;
	}
	
	

}
