
package com.gcloud.mesh.header.vo.analysis;

import com.gcloud.mesh.header.valid.ValidId;

import lombok.Data;

@Data
public class SubhealthThresholdVo {

    @ValidId
    private String id;
    private String deviceType;
    private String meter;
    private String meterName;
    private Float threshold;

}
