package com.gcloud.mesh.header.vo.dcs;

import lombok.Data;

@Data
public class DatacenterReportVo {

	private int no;
	private String label;
	private Double max;
	private Double min;
	private Double avg;
	private String datacenterName;
	
}
