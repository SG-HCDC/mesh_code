
package com.gcloud.mesh.header.msg.dvfs;

import java.util.List;

import com.gcloud.mesh.header.msg.BaseReplyMsg;

import lombok.Data;

@Data
public class AllDvfsReplyMsg extends BaseReplyMsg {

    private List<DvfsVo> vos;

}
