package com.gcloud.mesh.header.msg.dcs;

import com.gcloud.mesh.header.exception.AppErrorCode;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Data
public class MyUpdateAppSchedulerConfig {
    @ApiModelProperty(name = "thresholdKey", value = "监控项")
    @NotBlank( message = AppErrorCode.THRESHOLDKEY_NOT_BLANK)
    private String thresholdKey;


    @ApiModelProperty(name = "thresholdValue", value = "阈值")
    @NotNull( message = AppErrorCode.THRESHOLDVALUE_NOT_NULL)
    @Max(value = 9999, message = "::阈值在0-9999间")
    @Min(value = 0, message = "::阈值在0-9999间")
    private Double thresholdValue;
}
