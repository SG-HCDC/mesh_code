
package com.gcloud.mesh.header.msg.dcs;

import java.util.ArrayList;
import java.util.List;

import com.gcloud.mesh.header.msg.BaseReplyMsg;
import com.gcloud.mesh.header.vo.dcs.ModelBestPredictVo;

import lombok.Data;

@Data
public class ModelBestPredictReplyMsg extends BaseReplyMsg {

    private List<ModelBestPredictVo> vos = new ArrayList<>();

}
