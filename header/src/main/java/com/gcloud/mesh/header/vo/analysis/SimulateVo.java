package com.gcloud.mesh.header.vo.analysis;

import java.util.List;

import lombok.Data;

@Data
public class SimulateVo {
	
	private Integer degree;
	private List<ChartVo> charts;

}
