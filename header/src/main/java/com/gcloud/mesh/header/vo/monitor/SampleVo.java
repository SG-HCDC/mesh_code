package com.gcloud.mesh.header.vo.monitor;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import com.gcloud.mesh.header.msg.DataCleanBaseMsg;

import lombok.Data;

@Data
public class SampleVo extends DataCleanBaseMsg {

	private String resourceType;

	private String resourceId;

	private String resourceInstance;

	private String meter;

	private String unit;

	private String host;

	private String platformType;

	private String regionId;

	private Date createTime;

	private Long timestamp;

	private Double volume;

	Map<String, Object> resourceMetadata = new HashMap<String, Object>();
}
