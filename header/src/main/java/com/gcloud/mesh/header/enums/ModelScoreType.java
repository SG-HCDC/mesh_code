
package com.gcloud.mesh.header.enums;

import java.util.Objects;

import com.gcloud.mesh.header.exception.BaseException;

public enum ModelScoreType {

	SAMPLE_PERCENTAGE; // 与样本百分比
	
	public static ModelScoreType checkAndGet(String type) throws BaseException{
		if (type!=null) {
			for(ModelScoreType t:ModelScoreType.values()) {
				if (Objects.equals(t.name(), type)) {
					return t;
				}
			}
		}
		throw new BaseException("not found");
	}

}
