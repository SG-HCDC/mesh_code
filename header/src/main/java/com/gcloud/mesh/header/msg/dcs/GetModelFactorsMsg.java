
package com.gcloud.mesh.header.msg.dcs;

import javax.validation.constraints.NotBlank;

import com.gcloud.mesh.header.exception.CommonErrorCode;
import com.gcloud.mesh.header.exception.ModelErrorCode;
import com.gcloud.mesh.header.exception.SpecialCode;
import com.gcloud.mesh.header.msg.BaseMsg;

import lombok.Data;

@Data
public class GetModelFactorsMsg extends BaseMsg {

	@NotBlank( message = ModelErrorCode.MODEL_TYPE_NOT_BLANK)
	@SpecialCode(message = "::模型类型"+CommonErrorCode.SPECIAL)
    private String modelType;

}
