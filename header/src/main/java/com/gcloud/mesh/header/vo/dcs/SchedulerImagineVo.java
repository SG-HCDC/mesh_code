package com.gcloud.mesh.header.vo.dcs;

import java.util.Date;

import lombok.Data;

@Data
public class SchedulerImagineVo {
	private String id;
	private String appId;
	private String sourceNodeId;
	private String destNodeId;
	private String modelType;
	private Double difference;
	private Double temperatureImagine;
	private Double distributionBoxVoltageImagine;
	private Double datacenterEnergyImagine;
	private Double businessRespondImagine;
	private Date updateTime;
}
