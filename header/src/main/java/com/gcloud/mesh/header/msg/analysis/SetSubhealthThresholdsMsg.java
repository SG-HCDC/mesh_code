
package com.gcloud.mesh.header.msg.analysis;

import java.util.List;

import javax.validation.constraints.NotEmpty;

import com.gcloud.mesh.header.msg.BaseMsg;
import com.gcloud.mesh.header.vo.analysis.SubhealthThresholdVo;

import lombok.Data;

@Data
public class SetSubhealthThresholdsMsg extends BaseMsg {

    @NotEmpty(message = "::设备类型不能为空")
    private String deviceType;
    @NotEmpty
    private List<SubhealthThresholdVo> vos;

}
