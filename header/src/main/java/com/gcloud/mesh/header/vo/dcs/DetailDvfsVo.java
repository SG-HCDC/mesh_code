package com.gcloud.mesh.header.vo.dcs;

import lombok.Data;

import java.util.List;

@Data
public class DetailDvfsVo {
    private String resourceId;
    private List<String> supportStrategies;
    private Integer controlType;
}
