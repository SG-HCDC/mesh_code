package com.gcloud.mesh.header.exception;

public class AuthorityErrorCode {
	
	public static final String CLASSIFICATION_NOT_BLANK = "110101";
	public static final String CLASSIFICATION_FORMAT_ERROR = "110102";
	public static final String TYPE_NOT_BLANK = "110103";
	public static final String TYPE_FORMAT_ERROR = "110104";

}
