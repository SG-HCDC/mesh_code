
package com.gcloud.mesh.header.vo.restvelero;

import lombok.Data;

@Data
public class RestveleroRestoreDeleteReply {

	private Integer status;

}
