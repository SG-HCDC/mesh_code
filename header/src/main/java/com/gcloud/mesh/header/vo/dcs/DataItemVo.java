package com.gcloud.mesh.header.vo.dcs;

import lombok.Data;

@Data
public class DataItemVo {
	private String database;
	private String table;
	private Long count;
}
