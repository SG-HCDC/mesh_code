
package com.gcloud.mesh.header.vo.dcs;

import lombok.Data;

@Data
public class SchedulerThresholdVo {

    private String key;
    private String operation;
    private Double value;
    

}
