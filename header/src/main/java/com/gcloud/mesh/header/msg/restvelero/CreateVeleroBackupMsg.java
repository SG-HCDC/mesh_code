
package com.gcloud.mesh.header.msg.restvelero;

import lombok.Data;

@Data
public class CreateVeleroBackupMsg {

	private String namespace;
	
	public CreateVeleroBackupMsg (String namespace) {
		this.namespace = namespace;
	}

}
