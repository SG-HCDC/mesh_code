package com.gcloud.mesh.header.msg.agent;



import com.gcloud.mesh.header.msg.MqBaseMsg;

import lombok.Data;

@Data
public class SetDvfsMsg extends MqBaseMsg {

	/** nodeId = esn **/
	private String nodeId;
	private String strategy;
	private Float frequency;

}
