package com.gcloud.mesh.header.msg.alert;

import com.gcloud.mesh.header.exception.AlertErrorCode;
import com.gcloud.mesh.header.exception.CommonErrorCode;
import com.gcloud.mesh.header.exception.SpecialCode;
import com.gcloud.mesh.header.msg.BaseMsg;
import javax.validation.constraints.NotBlank;

import org.hibernate.validator.constraints.Length;

import lombok.Data;

@Data
public class DetailPolicyMsg extends BaseMsg {
	@NotBlank( message = AlertErrorCode.POLICYID_NOT_NULL)
	@Length( min = 1, max = 60, message = "::策略ID"+CommonErrorCode.LENGTH60)
	@SpecialCode(message = "::策略ID"+CommonErrorCode.SPECIAL)
	private String policyId;
}
