
package com.gcloud.mesh.header.msg.dcs;

import javax.validation.constraints.NotNull;

import com.gcloud.mesh.header.exception.SchedulerErrorCode;
import com.gcloud.mesh.header.msg.BaseMsg;

import lombok.Data;

@Data
public class UpdateSchedulerModelMsg extends BaseMsg {

	@NotNull( message = SchedulerErrorCode.TYPE_NOT_NULL)
	private Integer type;

	@Override
	public String toString() {
		String typeStr = "";
		if(type.equals(0)) {
			typeStr = "调度模型=最优成本模型";
		}
		if(type.equals(1)) {
			typeStr = "调度模型=最优设备模型";
		}
		return typeStr;
	}
	
}
 