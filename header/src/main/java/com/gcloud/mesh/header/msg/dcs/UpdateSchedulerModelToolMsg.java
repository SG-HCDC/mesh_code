
package com.gcloud.mesh.header.msg.dcs;

import java.util.List;

import javax.validation.constraints.NotNull;

import com.gcloud.mesh.header.exception.SchedulerErrorCode;

import lombok.Data;

@Data
public class UpdateSchedulerModelToolMsg {

	@NotNull( message = SchedulerErrorCode.TOOL_NOT_NULL)
	private List<Integer> tool;

	@Override
	public String toString() {
		StringBuffer buff = new StringBuffer();
		buff.append("");
		if(tool != null && tool.size() > 0) {
				if(tool.get(0).equals(0)) {
					buff.append(" 容器迁移工具："+"Kubernetes迁移工具");
				}
				if(tool.get(1).equals(1)) {
					buff.append(" 虚拟机迁移工具："+"阿里云迁移工具");
				}else if(tool.get(1).equals(2)) {
					buff.append(" 虚拟机迁移工具："+"华为迁移工具");
				}
		}
		return buff.toString();
	}
	
	
}
