package com.gcloud.mesh.header.msg.analysis;

import java.util.Date;

import org.hibernate.validator.constraints.Length;

import com.gcloud.mesh.header.exception.CommonErrorCode;
import com.gcloud.mesh.header.exception.SpecialCode;
import com.gcloud.mesh.header.msg.BaseMsg;

import lombok.Data;

@Data
public class CreateFitterMsg extends BaseMsg {
	@Length( min = 1, max = 60, message = "::服务器ID"+CommonErrorCode.LENGTH60)
	@SpecialCode(message = "::服务器ID"+CommonErrorCode.SPECIAL)
	private String nodeId;
	@Length( min = 1, max = 60, message = "::空调ID"+CommonErrorCode.LENGTH60)
	@SpecialCode(message = "::空调ID"+CommonErrorCode.SPECIAL)
	private String airId;
	private String metricX;
	private String metricY;
	private Integer status;
	private Integer degree;
	private Date startTime;
	private Date endTime;

}
