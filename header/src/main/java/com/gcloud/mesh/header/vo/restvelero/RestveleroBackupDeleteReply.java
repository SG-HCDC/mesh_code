
package com.gcloud.mesh.header.vo.restvelero;

import lombok.Data;

@Data
public class RestveleroBackupDeleteReply {

	private Integer status;

}
