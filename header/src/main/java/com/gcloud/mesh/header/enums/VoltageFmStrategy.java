package com.gcloud.mesh.header.enums;

import com.gcloud.mesh.header.exception.BaseException;
import com.gcloud.mesh.header.exception.DvfsErrorCode;

import java.util.Objects;

public enum VoltageFmStrategy {

	/*平时以低速运行，系统负载提高时按需提高频率*/
	ONDEMAND,
	/**CPU高频*/
	PERFORMANCE,
	/*跟ondemand方式类似， 不同之处在于提高频率时渐进提高*/
	CONSERVATIVE,
	/** CPU低频*/
	POWERSAVE,
	/*使用用户在/sys 节点scaling_setspeed设置的频率运行*/
	USERSPACE;

	public static VoltageFmStrategy getAndCheck(String value) throws BaseException{
		for (VoltageFmStrategy v:VoltageFmStrategy.values()){
			if (Objects.equals(v.name(),value)){
				return v;
			}
		}
		throw new BaseException(DvfsErrorCode.STRATEGY_NOT_EXIST,"策略不存在");
	}
}
