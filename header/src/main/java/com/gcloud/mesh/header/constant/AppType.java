
package com.gcloud.mesh.header.constant;

public enum AppType {

    VM,
    CONTAINER;

}
