
package com.gcloud.mesh.header.vo.analysis;

import lombok.Data;

@Data
public class SubhealthAirVo {

    private String id;
    private String name;
    private String datacenterId;
    private String datacenterName;
    private Boolean subhealth;
	private String airOut;
	private String airIn;
	private String airHumidityOut;
	private String airHumidityIn;

}
