package com.gcloud.mesh.header.msg.asset;

import javax.validation.constraints.Pattern;

import org.hibernate.validator.constraints.Length;
import org.springframework.lang.Nullable;

import com.gcloud.mesh.header.exception.AssetErrorCode;
import com.gcloud.mesh.header.exception.CommonErrorCode;
import com.gcloud.mesh.header.exception.SpecialCode;
import com.gcloud.mesh.header.msg.BaseQueryMsg;

import lombok.Data;

@Data
public class PageNodeMsg extends BaseQueryMsg {
	
	@Nullable
	@Length( min = 1, max = 10, message = "::服务器名称"+CommonErrorCode.LENGTH10)
	@SpecialCode(message = "::服务器名称"+CommonErrorCode.SPECIAL)
	private String name;
	
	@Nullable
	@Pattern( regexp = "^(\\d{1,2}|1\\d\\d|2[0-4]\\d|25[0-5])\\.(\\d{1,2}|1\\d\\d|2[0-4]\\d|25[0-5])\\.(\\d{1,2}|1\\d\\d|2[0-4]\\d|25[0-5])\\.(\\d{1,2}|1\\d\\d|2[0-4]\\d|25[0-5])$", 
	message = AssetErrorCode.IP_FORAMT_ERROR)
	private String ip;

	private String order;
	
	private String datacenterId;
}
