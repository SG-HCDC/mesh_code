package com.gcloud.mesh.header.msg.analysis;

import java.util.Date;

import com.gcloud.mesh.header.msg.BaseMsg;

import lombok.Data;

@Data
public class UpdateFitterMsg extends BaseMsg {
	
	private String id;
	private Integer nLevel;
	private String result;
	private Date startTime;
	private Date entTime;
	private Date updateTime;

}
	