package com.gcloud.mesh.header.exception;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;


public class SpecialCodeValidator implements ConstraintValidator<SpecialCode, Object>{
	
	public static String[] keywords = new String[] {
	          "javascript",
	          "script",
	          "function",
	          "jscript",
	          "vbscript",
	          "onfocus",
	          "onblur",
	          "alert",
	          "location",
	          "document",
	          "window",
	          "window",
	          "href",
	          "onfocus",
	          "confirm",
	          "prompt",
	          "onerror",
	          "eval",
	          "url",
	          "expr",
	          "urlunencoded",
	          "referrer",
	          "write",
	          "writeln",
	          "execScript",
	          "setInterval",
	          "setTimeout",
	          "open",
	          "navigate",
	          "srcdoc",
	          "null",
	          "underfined",
	          "and",
	  //        "or",
	          "exec",
	          "execute",
	          "insert",
	          "select",
	          "delete",
	          "update",
	          "alter",
	          "create",
	          "drop",
	          "count",
	          "chr",
	          "char",
	          "asc",
	          "mid",
	          "substring",
	          "master",
	          "truncate",
	          "declare",
	          "xp_cmdshell",
	          "restore",
	          "backup",
	          "netlocalgroup administrators",
	          "net user",
	          "like",
	          "table",
	          "from",
	          "grant",
	          "column_name",
	          "group_concat",
	          "information_schema.columns",
	          "table_schema",
	          "union",
	          "where",
	          "order",
	          "by",
	          "join",
	          "modify",
	          "into",
	          "substr",
	          "ascii",
	          "having",
	          "iframe",
	          "body",
	       //   "form",
	          "base",
	          "img",
	          "src",
	          "style",
	          "div",
	          "object",
	          "meta",
	          "link",
	          "input",
	          "comment",
	          "br",
	};

	@Override
	public boolean isValid(Object value, ConstraintValidatorContext context) {
		// TODO Auto-generated method stub
		return vaildate(value);
	}
	private boolean vaildate(Object value) {
		if(value == null) {
			return true;
		}
		String str = value.toString();
		
		str = str.toLowerCase();
		//判断是否是特殊字符
		return StringFilter(str);
	}
	 private boolean StringFilter(String str) {
		String regEx="[`~!@#$%^&*()+=|{}':;',\\[\\].<>/?~！@#￥%……&*（）——+|{}【】‘；：”“’。，、？]";
		String keywordStr = String.join("|", keywords);
		regEx = regEx + "|" + keywordStr;
		Pattern p = Pattern.compile(regEx);
		Matcher m = p.matcher(str);
		if(m.find()) {
			return false;
		}
		return  true;
	}
}
