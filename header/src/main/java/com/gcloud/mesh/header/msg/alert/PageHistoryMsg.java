package com.gcloud.mesh.header.msg.alert;

import java.util.List;

import com.gcloud.mesh.header.msg.BaseQueryMsg;

import lombok.Data;

@Data
public class PageHistoryMsg extends BaseQueryMsg {

	private List<String> resourceTypes; 
}
