package com.gcloud.mesh.header.vo.supplier;

import lombok.Data;

@Data
public class XbortherAssetItemVo {

	private String cabinet_name;
	private String device_info;
	private String device_num;
	private String eic_num;
	private String ip;
	private String location_state;
	private String name;
	private String parent;
	private String rack_id;
	private String resource_id;
	private String start_u;
	private String u_height;
	private String vendor_info;

}
