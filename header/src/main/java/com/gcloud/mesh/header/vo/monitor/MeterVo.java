package com.gcloud.mesh.header.vo.monitor;

import lombok.Data;

@Data
public class MeterVo {
	private Long id;

	private String meter;

	private String chnName;

	private String unit;

	private String platform;

	private String regionId;

	private String resourceType;
}
