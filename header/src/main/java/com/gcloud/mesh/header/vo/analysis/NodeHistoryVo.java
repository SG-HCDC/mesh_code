package com.gcloud.mesh.header.vo.analysis;

import lombok.Data;

@Data
public class NodeHistoryVo {

	private String id;
	private String name;
	private String ipmiIp;
	private String mgmIp;
	private String region;
	private String datacenterId;
	private String powerConsumption;
	private String powerMode;
	private Double historyMetric;
	private Double historyValue;
	private Integer powerStatus;
	private String powerStatusCnName;
	
}
