
package com.gcloud.mesh.header.vo.dcs;

import lombok.Data;

@Data
public class SchedulerModelVo {

    private String  name;
    private Integer  value;
    private Boolean isCheck;
}
