package com.gcloud.mesh.header.vo.dcs;

import javax.validation.constraints.NotBlank;

import com.gcloud.mesh.header.exception.SchedulerErrorCode;
import com.gcloud.mesh.header.msg.BaseMsg;

import lombok.Data;

@Data
public class MigrateResourceVo extends BaseMsg {

	@NotBlank(message = SchedulerErrorCode.SRCNODEID_NOT_BLANK)
	private String srcNodeId;

	@NotBlank(message = SchedulerErrorCode.DSTNODEID_NOT_BLANK)
	private String dstNodeId;

}
