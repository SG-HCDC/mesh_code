package com.gcloud.mesh.header.msg.analysis;

import com.gcloud.mesh.header.exception.AnalysisErrorCode;
import com.gcloud.mesh.header.msg.BaseQueryMsg;
import javax.validation.constraints.NotBlank;
import lombok.Data;

@Data
public class PageNodeSimulateMsg extends BaseQueryMsg {
//	@NotBlank( message = AnalysisErrorCode.NAME_NOT_NULL)
	private String name;

}
