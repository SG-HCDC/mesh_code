
package com.gcloud.mesh.header.msg.dvfs;

import com.gcloud.mesh.header.msg.MqBaseReplyMsg;

import lombok.Data;

@Data
public class SetDvfsReplyMsg extends MqBaseReplyMsg {

}
