package com.gcloud.mesh.header.msg.supplier;


import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class AliPowerListDevicePageReqMsg {
    private String condition;
    private String business_type;
    private String type;
    private String usage_status;
    private Integer pageNum;
    private Integer pageSize;
    private String cabinet_sn;

}
