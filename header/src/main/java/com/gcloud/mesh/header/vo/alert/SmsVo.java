package com.gcloud.mesh.header.vo.alert;

import java.util.Date;

import lombok.Data;

@Data
public class SmsVo {

	private String smsPlatform;
	private String smsPlatformName;
	private String config;
	private boolean enable;
	private Date createTime;
}
