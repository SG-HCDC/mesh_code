package com.gcloud.mesh.header.vo.asset;

import java.util.Date;

import lombok.Data;

@Data
public class CloudResourceAnalysisVo {

	private Integer vmCount;
	private String name;
	private Integer type;
	private String typeName;
	private String zoneId;
	private String zoneName;
	private Integer zoneType;
	private Date createTime;
}
