
package com.gcloud.mesh.header.msg.supplier;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;

import org.hibernate.validator.constraints.Length;

import com.gcloud.mesh.header.constant.RegexpPattern;
import com.gcloud.mesh.header.exception.AssetErrorCode;
import com.gcloud.mesh.header.exception.CommonErrorCode;
import com.gcloud.mesh.header.exception.SpecialCode;
import com.gcloud.mesh.header.msg.BaseMsg;

import lombok.Data;

@Data
public class UpdateSupplierMsg extends BaseMsg {

    @NotBlank( message = AssetErrorCode.ID_NOT_BLANK)
	@Length( min = 1, max = 60, message = AssetErrorCode.ID_LENGTH_ERROR)
	@Pattern( regexp = RegexpPattern.STAND_ID, message = AssetErrorCode.ID_FORMAT_ERROR)
    @SpecialCode(message = "::设备ID"+CommonErrorCode.SPECIAL)
    private String id;
    
    @NotBlank( message = AssetErrorCode.NAME_NOT_BLANK)
	@Length( min = 1, max = 60, message = AssetErrorCode.NAME_LENGTH_ERROR)
	@Pattern( regexp = RegexpPattern.STAND_NAME, message = AssetErrorCode.NAME_FORMAT_ERROR)
    @SpecialCode(message = "::设备名称"+CommonErrorCode.SPECIAL)
    private String name;
    
    private String config;

    private String insecureCommand;

    private String clusterId;

}
