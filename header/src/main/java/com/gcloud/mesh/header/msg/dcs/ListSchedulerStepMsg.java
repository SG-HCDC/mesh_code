
package com.gcloud.mesh.header.msg.dcs;

import javax.validation.constraints.NotBlank;

import com.gcloud.mesh.header.exception.CommonErrorCode;
import com.gcloud.mesh.header.exception.SchedulerStepErrorCode;
import com.gcloud.mesh.header.exception.SpecialCode;
import com.gcloud.mesh.header.msg.BaseMsg;
import com.gcloud.mesh.header.valid.ValidId;

import lombok.Data;

@Data
public class ListSchedulerStepMsg extends BaseMsg {

    @ValidId
    @NotBlank( message = SchedulerStepErrorCode.SCHEDULERJOBID_NOT_BLANK)
    @SpecialCode(message = "::调度ID"+CommonErrorCode.SPECIAL)
    private String schedulerJobId;

}
