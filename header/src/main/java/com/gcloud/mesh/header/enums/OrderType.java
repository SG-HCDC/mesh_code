package com.gcloud.mesh.header.enums;

import java.util.Locale;
import java.util.stream.Stream;

public enum OrderType {
	
	POWER_CONSUMPTION(0, "power_consumption"),
	STORAGE_CAPACITY(1, "storage_capacity"),
	ENERGY_EFFICIENCY_RATIO(2, "energy_efficiency_ratio");
	
	private int no;
	private String name;
	
	OrderType(int no, String name) {
		this.no = no;
		this.name = name;
	}
	
	public static OrderType getByName(String name) {
		return Stream.of(OrderType.values())
				.filter( s -> s.getName().equals(name.toLowerCase(Locale.ENGLISH)))
				.findFirst()
				.orElse(null);
	}

	public int getNo() {
		return no;
	}

	public String getName() {
		return name;
	}
	
	

}
