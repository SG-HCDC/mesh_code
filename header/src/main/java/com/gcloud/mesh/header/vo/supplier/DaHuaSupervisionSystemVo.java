package com.gcloud.mesh.header.vo.supplier;

import lombok.Data;

@Data
public class DaHuaSupervisionSystemVo extends SupervisionSystemAbstract {

	private static final String TYPE = "dahua";

	private String hostIp;

	private String port;

	@Override
	public String getType() {
		return TYPE;
	}

}
