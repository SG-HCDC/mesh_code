
package com.gcloud.mesh.header.msg.dcs;

import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import com.gcloud.mesh.header.exception.CommonErrorCode;
import com.gcloud.mesh.header.exception.ModelErrorCode;
import com.gcloud.mesh.header.exception.SpecialCode;
import com.gcloud.mesh.header.msg.BaseMsg;
import com.gcloud.mesh.header.vo.dcs.ModelFactorVo;

import lombok.Data;

@Data
public class SetModelFactorsMsg extends BaseMsg {

	@NotBlank( message = ModelErrorCode.MODEL_TYPE_NOT_BLANK)
	@SpecialCode(message = "::模型类型"+CommonErrorCode.SPECIAL)
    private String modelType;
	@Valid
    @NotNull( message = ModelErrorCode.FACTORS_NOT_NULL)
    private List<ModelFactorVo> factors;
	@Override
	public String toString() {
		StringBuffer buff = new StringBuffer();
		buff.append("");
		String type = content == null ? "无":content;
		if("AIR".equals(modelType)) {
			type = " 制冷设备最优成本模型指标设置";
		}else if("UPS".equals(modelType)) {
			type = " 供电设备最优成本模型指标设置";
		}
		buff.append(" 设置模型类型=");
		buff.append(type);
//		buff.append("，");
		for(ModelFactorVo vo : factors) {
			if("异步迁移代价因子权重设置".equals(type)) {
				buff.append(vo.onlyqz());
			}else if("代价模型影响因子设置".equals(type)) {
				buff.append(vo.whithoutqz());
			}else {
				buff.append(vo.toString());
			}
			
		}
		return buff.toString();
	}
	
	

}
