package com.gcloud.mesh.header.vo.dcs;

import lombok.Data;

@Data
public class StatisticTypeVo {

	private String label;
	private String labelCnName;
	private Long count;
	
	public StatisticTypeVo() {
		
	}
	
	public StatisticTypeVo(String label, String labelCnName, Long count) {
		this.label = label;
		this.labelCnName = labelCnName;
		this.count = count;
	}
	
}
