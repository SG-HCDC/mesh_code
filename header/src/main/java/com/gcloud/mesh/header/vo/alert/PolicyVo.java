package com.gcloud.mesh.header.vo.alert;

import java.util.Date;
import java.util.List;

import lombok.Data;

@Data
public class PolicyVo {
	private String id;
	private String policyName;
	private Boolean enable;
	private Integer enableStartTime;
	private Integer enableEndTime;
	private Integer channelSilence;
	private Boolean notifyEnable;
	private Date createTime;
	private String platform;
	private String regionId;
	private List<LinkmanVo> linkmanVos;
	private List<RuleVo> ruleVos;
}
