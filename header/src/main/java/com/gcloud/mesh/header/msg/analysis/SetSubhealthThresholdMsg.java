package com.gcloud.mesh.header.msg.analysis;

import java.util.List;

import javax.validation.Valid;

import org.hibernate.validator.constraints.Length;

import com.gcloud.mesh.header.enums.SubhealthThresholdMeter;
import com.gcloud.mesh.header.exception.AppErrorCode;
import com.gcloud.mesh.header.msg.BaseMsg;
import com.gcloud.mesh.header.vo.analysis.SubhealthDeviceThresholdVo;

import lombok.Data;

@Data
public class SetSubhealthThresholdMsg extends BaseMsg {
	private String resourceId;
	private String type;
	@Valid
    private List<SubhealthDeviceThresholdVo> subhealthThresholds;
	@Override
	public String toString() {
		StringBuffer buff = new StringBuffer();
		buff.append("");
		if(type != null) {
			String typeName = "";
			if(type.endsWith("AIR")) {
				typeName = " 空调";
			}
			if(type.endsWith("DATACENTER")) {
				typeName = " 数据中心";
			}
			if(type.endsWith("UPS")) {
				typeName = " UPS设备";
			}
			buff.append(" 资源类型=").append(typeName);
		}
		buff.append(" 监控指标：");
		for(SubhealthDeviceThresholdVo vo : subhealthThresholds) {
			String typeName = "[";
			if(vo.getLevel().endsWith("critical")) {
				typeName = " 危急";
			}
			if(vo.getLevel().endsWith("warning")) {
				typeName = " 警告";
			}
			if(vo.getLevel().endsWith("info")) {
				typeName = " 一般";
			}
			buff.append(" 告警级别=").append(typeName);
			buff.append(" ").append("监控项=").append(SubhealthThresholdMeter.getByMeter(vo.getMeter()).getCnName()).append(" ");
			buff.append("]");
		}
		return buff.toString();
	}
	
	
}
