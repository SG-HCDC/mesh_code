package com.gcloud.mesh.header.vo.dcs;

import lombok.Data;

@Data
public class SchedulerEffectPredictionFactorVo {

	private String factorType;
	private String unit;
	private String factorName;
	private Double difference;
	private Double imagine;
	private Double after;
	private Double before;
}
