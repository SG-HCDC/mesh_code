package com.gcloud.mesh.header.vo.dcs;

public class AppNodExpand {
	private String nodeId;

	public String getNodeId() {
		return nodeId;
	}

	public void setNodeId(String nodeId) {
		this.nodeId = nodeId;
	}
}
