package com.gcloud.mesh.header.enums;

import java.util.stream.Stream;

public enum ResourceSourceType {

	MANUAL("manual", "手动"),
	SYNC("sync", "同步");
	
	private String name;
	private String cnName;
	
	ResourceSourceType(String name, String cnName) {
		this.name = name;
		this.cnName = cnName;
	}
	
	public static ResourceSourceType getByName(String name) {
		return Stream.of(ResourceSourceType.values())
				.filter( s -> s.getName().equals(name))
				.findFirst()
				.orElse(null);					
	}

	public String getName() {
		return name;
	}

	public String getCnName() {
		return cnName;
	}
	
	
	
	
}
