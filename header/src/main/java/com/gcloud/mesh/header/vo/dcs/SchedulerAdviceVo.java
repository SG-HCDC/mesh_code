
package com.gcloud.mesh.header.vo.dcs;

import lombok.Data;

@Data
public class SchedulerAdviceVo {

    private String id;
    private String appId;
    private Integer status;
    private String srcDatacenterId;
    private String dstDatacenterId;
    private Integer schedulerModelTool;
    private Integer schedulerStrategyConfig;
    private Integer schedulerModel;
    private String appName;
    private String cloudResourceTypeName;
    private String cloudResourceId;
    private String cloudResourceName;
    private String instanceId;
    private String instanceName;
    private String dstCloudResourceId;
    
	private String srcDatacenterName;
	private String dstDatacenterName;
}
