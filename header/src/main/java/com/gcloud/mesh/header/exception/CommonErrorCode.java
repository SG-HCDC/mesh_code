
package com.gcloud.mesh.header.exception;

public class CommonErrorCode {

    public static final String UNKNOWN_ERROR = "000000";

    public static final String PROCESSOR_NOT_FOUND = "000101";
    public static final String SYNC_MSG_TIMEOUT = "000102";
    
    public static final String PAGE_NO_ERROR = "000103";
    public static final String PAGE_SIZE_ERROR = "000104";
    
    public static final String LENGTH1_2 = "长度需处于在1~2之间";
	public static final String LENGTH10 =  "长度不能大于10";
	public static final String LENGTH6_20 =  "长度需处于在6~20之间";
	public static final String LENGTH26 =  "长度不能大于26";
	public static final String LENGTH60 =  "长度不能大于60";
	public static final String LENGTH200 =  "长度不能大于200";
	public static final String SPECIAL = "不能包含特殊字符";
	public static final String NULL = "不能为空";
}
