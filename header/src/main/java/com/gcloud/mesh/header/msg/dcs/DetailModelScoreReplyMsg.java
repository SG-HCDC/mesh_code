
package com.gcloud.mesh.header.msg.dcs;

import java.util.ArrayList;
import java.util.List;

import com.gcloud.mesh.header.msg.BaseReplyMsg;
import com.gcloud.mesh.header.vo.dcs.ModelFactorVo;
import com.gcloud.mesh.header.vo.dcs.ModelScoreVo;

import lombok.Data;

@Data
public class DetailModelScoreReplyMsg extends BaseReplyMsg {

    private Integer totalScore = 0;
    private List<ModelScoreVo> scores = new ArrayList<>();
    private List<ModelFactorVo> factors;
    private boolean needMigrate;
    private String resourceId;
    private String resourceName;

}
