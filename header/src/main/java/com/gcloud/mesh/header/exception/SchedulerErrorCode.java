package com.gcloud.mesh.header.exception;

public class SchedulerErrorCode {

	public static final String DATACENTERIDS_NOT_NULL = "160101";
	public static final String TYPE_NOT_NULL = "160102";
	public static final String TYPE_FORMAT_ERROR = "160103";
	public static final String TOOL_NOT_NULL = "160104";
	public static final String TOOL_FORMAT_NULL = "160105";
	public static final String CONFIG_NOT_NULL = "160106";
	public static final String CONFIG_FORMAT_ERROR = "160107";
	public static final String RULE_NOT_BLANK = "160108";
	public static final String APPID_NOT_BLANK = "160109";
	public static final String SRCNODEID_NOT_BLANK = "160110";
	public static final String DSTNODEID_NOT_BLANK = "160111";
	
	public static final String NODE_NOT_EXIST = "160201";
	public static final String SRCDATACENTER_NOT_EXIST = "160202";
	public static final String DSTDATACENTER_NOT_EXIST = "160203";
	public static final String APP_NOT_EXIST = "160204";
	public static final String RESTORE_OPERATE_FAILED = "160205";
	public static final String DATACENTER_SCHEDULE_BACKUP_OVERTIME = "160206";
	public static final String DATACENTER_SCHEDULE_BACKUP_FAILED = "160207";
}
