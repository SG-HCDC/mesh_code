
package com.gcloud.mesh.header.msg.dcs;

import java.util.List;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import org.apache.commons.lang3.StringUtils;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.Range;

import com.gcloud.mesh.header.exception.AppErrorCode;
import com.gcloud.mesh.header.exception.CommonErrorCode;
import com.gcloud.mesh.header.exception.SpecialCode;
import com.gcloud.mesh.header.msg.BaseMsg;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class UpdateAppSchedulerConfigMsg extends BaseMsg {

	@ApiModelProperty(name = "datacenterId", value = "数据中心ID")
	@NotBlank( message = AppErrorCode.DATACENTERID_NOT_BLANK)
	@Length( min = 1, max = 60, message = "::数据中心ID"+CommonErrorCode.LENGTH60)
	@SpecialCode(message = "::数据中心ID"+CommonErrorCode.SPECIAL)
	private String datacenterId;

	@ApiModelProperty(name = "thresholdKey", value = "监控项")
	@NotBlank( message = AppErrorCode.THRESHOLDKEY_NOT_BLANK)
	private String thresholdKey;

	@ApiModelProperty(name = "thresholdOperation", value = "比较符")
	private String thresholdOperation;
	
	@ApiModelProperty(name = "thresholdValue", value = "阈值")
	@NotNull( message = AppErrorCode.THRESHOLDVALUE_NOT_NULL)
	@Max(value = 9999, message = "::阈值在0-9999间")
	@Min(value = 0, message = "::阈值在0-9999间")
	// @Range(min=0, max=9999, message = "::阈值在0-9999间")
	private Double thresholdValue;

	@Override
	public String toString() {
		String unit = "%";
		StringBuffer buffer = new StringBuffer();
		if(StringUtils.isNotBlank(thresholdKey)) {
			buffer.append(" 触发策略：");
			if(thresholdKey.equals("ServerPower")) {
				buffer.append(" 服务器功率");
				unit = "W";
			}else if(thresholdKey.equals("ClusterCpuCores")) {
				buffer.append(" 服务器CPU利用率");
			}else if(thresholdKey.equals("ClusterMemSpace")) {
				buffer.append(" 服务器内存利用率");
			}
		}
		if(StringUtils.isNotBlank(thresholdOperation)) {
			switch(thresholdOperation){
			case "0" :
		       buffer.append(" >=");
		       break; //可选
		    case "1" :
		       buffer.append(" >");
		       break; //可选
		    case "2" :
		       buffer.append(" <=");
		       break; //可选
		    case "3" :
		    	buffer.append(" <");
			   break; //可选
			case "4" :
				buffer.append(" =");
			   break; //可选
			case "5" :
				buffer.append(" !=");
			   break; //可选
		    default : //可选
		       //语句
		}
	
	}
		
		if(thresholdValue != null) {
			buffer.append(" "+thresholdValue).append(unit);
		}
		return buffer.toString();
	}
	

}
