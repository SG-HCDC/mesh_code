
package com.gcloud.mesh.header.msg.dvfs;

import com.gcloud.mesh.header.msg.MqBaseMsg;
import lombok.Data;

@Data
public class SetDvfsMsg extends MqBaseMsg {

    private String nodeId;
    private String strategy;
    private String frequency;

}
