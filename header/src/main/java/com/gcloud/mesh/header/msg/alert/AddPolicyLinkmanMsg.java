package com.gcloud.mesh.header.msg.alert;

import java.util.List;

import com.gcloud.mesh.header.exception.AlertErrorCode;
import com.gcloud.mesh.header.exception.CommonErrorCode;
import com.gcloud.mesh.header.exception.SpecialCode;
import com.gcloud.mesh.header.msg.BaseMsg;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Length;

import lombok.Data;

@Data
public class AddPolicyLinkmanMsg extends BaseMsg{

	@NotNull( message = "::告警联系人ID不能为空")
	private List<String> linkmanIds;
	@NotBlank( message = "::策略ID不能为空")
	@Length( min = 1, max = 60, message = "::策略ID"+CommonErrorCode.LENGTH60)
	@SpecialCode(message = "::策略ID"+CommonErrorCode.SPECIAL)
	private String policyId;
}
