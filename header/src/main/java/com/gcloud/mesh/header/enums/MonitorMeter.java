package com.gcloud.mesh.header.enums;

import java.util.ArrayList;
import java.util.List;

public enum MonitorMeter {

	SERVER_POWER("服务器", "服务器实时功耗", "server.power", "W", "265", 150d, 450d, "L2"),
	SERVER_CPU_CORES("服务器", "服务器CPU核数", "server.cpu_cores", "", "4", 1d, 32d, "L2"),
	SERVER_CPU_l2CACHE("服务器", "服务器CPU缓存", "server.cpu_L2cache", "", "1024", 128d, 4096d, "L2"),
	SERVER_CPU_FREQUENCY("服务器", "服务器CPU频率", "server.cpu_frequency", "GHz", "2.2", 0.5d, 4.6d, "L2"),
	SERVER_MEM_SPEED("服务器", "服务器内存速率", "server.mem_speed", "MHz", "2200", 800d, 5000d, "L2"),
	SERVER_MEM_TOTAL("服务器", "服务器内存总量", "server.mem_total", "GB", "125", 64d, 300d, "L2"),
	SERVER_STORAGE_TOTAL("服务器", "服务器存储总量", "server.storage_total", "GB", "128", 32d, 1000d, "L2"),
	SERVER_STORAGE_AVAILABLE("服务器", "服务器存储可用量", "server.storage_available", "GB", "64", 0.1d, 300d, "L2"),
	SERVER_BANDWIDTH("服务器", "服务器带宽", "server.bandWidth", "MB/s", "1000", 100d, 2000d, "L2"),
	SERVER_CPU_UTIL("服务器", "服务器CPU利用率", "host.cpu_util", "%", "30", 0d, 100d, "L2"),
	SERVER_TEMPERATURE("服务器", "服务器温度", "host.temperature", "°C", "45", 26d, 85d, "L2"),
	SERVER_CPU_LOAD("服务器", "服务器CPU负载", "host.cpu_load_1min", "", "5.2", 0.2d, 30d, "L2"),
	SERVER_MEMORY_UTIL("服务器", "服务器内存利用率", "host.memory_util", "%", "40", 0d, 100d, "L2"),
	SERVER_DISK_READ_RATE("服务器", "服务器磁盘读速率", "host.disk_read_rate", "KB/s", "1024", 0d, 1024000d, "L2"),
	SERVER_DISK_WRITE_RATE("服务器", "服务器磁盘写速率", "host.disk_write_rate", "KB/s", "1024", 0d, 1024000d, "L2"),
	SERVER_CPU_MARGIN_TEMPERATURE("服务器", "服务器CPU温度余量", "server.cpu_margin_temperature", "°C", "54", 10d, 85d, "L2"),
	SERVER_CPU_TEMPERATURE("服务器", "服务器CPU温度", "server.cpu_temperature", "°C", "55", 10d, 85d, "L2"),
	SERVER_CPU_VR_TEMPERATURE("服务器", "服务器CPU VR温度", "server.cpu_vr_temperature", "°C", "30", 10d, 85d, "L2"),
	SERVER_DIMMG_TEMPERATURE("服务器", "服务器内存温度", "server.dimmg_temperature", "°C", "25", 10d, 60d, "L2"),
	SERVER_FAN_RPM("服务器", "服务器风扇转速", "server.fan_rpm", "rpm", "4128", 0d, 6000d, "L2"),
	SERVER_FRAME_POWER("服务器", "服务器机框功率", "server.frame_power", "W", "213", 10d, 440d, "L2"),
	SERVER_INLET_TEMPERATURE("服务器", "服务器入风温度", "server.inlet_temperature", "°C", "19", 10d, 50d, "L2"),
	SERVER_OUTLET_TEMPERATURE("服务器", "服务器出风温度", "server.outlet_temperature", "°C", "25", 10d, 50d, "L2"),
	SERVER_NVME_TEMPERATURE("服务器", "服务器硬盘温度", "server.nvme_temperature", "°C", "23", 10d, 70d, "L2"),
	SERVER_PCH_TEMPERATURE("服务器", "服务器芯片组温度", "server.pch_temperature", "°C", "35", 10d, 85d, "L2"),
	SERVER_PS_POWER("服务器", "服务器ps电源功率", "server.ps_power", "W", "112", 40d, 300d, "L2"),
	AIR_IN_TEMPERATURE("空调", "空调回风温度", "environment.air_in_temperature", "°C", "22.3", 20d, 30d, "L1"),
	AIR_OUT_TEMPERATURE("空调", "空调出风温度", "environment.air_out_temperature", "°C", "12", 8d, 30d, "L1"),
	AIR_SETTING_TEMPERATURE("空调", "空调设定温度", "environment.air_setting_temperature", "°C", "23", 18d, 30d, "L1"),
	AIR_IN_HUMIDITY("空调", "空调回风湿度", "environment.air_in_humidity", "%RH", "40.5", 20d, 60d, "L1"),
	AIR_OUT_HUMIDITY("空调", "空调出风湿度", "environment.air_out_humidity", "%RH", "3.06", 0d, 10d, "L1"),
	AIR_OUT_WIND("空调", "空调送风量", "environment.air_out_wind", "m³/h", "10", 0d, 30d, "L1"),
	AIR_IN_WIND("空调", "空调回风量", "environment.air_in_wind", "m³/h", "15", 0d, 30d, "L1"),	
	DISTRIBUTION_BOX_VOLTAGE("配电箱", "配电箱实时电压", "environment.distribution_box_voltage", "V", "380", 360d, 400d, "L1"),
	DISTRIBUTION_BOX_ELECTRON("配电箱", "配电箱实时电流", "environment.distribution_box_electron", "A", "10", 1d, 60d, "L1"),
	DISTRIBUTION_BOX_POWER("配电箱", "配电箱有功功率", "environment.distribution_box_power", "KW", "0.6", 0d, 10d, "L1"),
	DISTRIBUTION_BOX_ENERGT("配电箱", "配电箱有功电能", "environment.distribution_box_energy", "KW/h", "623.8", 0d, 1000d, "L1"),
	UPS_IN_VOLTAGE("UPS", "UPS输入电压", "environment.ups_in_voltage", "V", "220", 198d, 242d, "L1"),
	UPS_OUT_VOLTAGE("UPS", "UPS输出电压", "environment.ups_out_voltage", "V", "220", 198d, 242d, "L1"),
	UPS_TEMPERATURE("UPS", "UPS温度", "environment.ups_temperature", "°C", "25", 22d, 43d, "L1"),
	UPS_IN_FREQUENCY("UPS", "UPS输入频率", "environment.ups_in_frequency", "Hz", "49", 22d, 70d, "L1"),
	UPS_OUT_FREQUENCY("UPS", "UPS输出频率", "environment.ups_out_frequency", "Hz", "49", 22d, 70d, "L1"),
	UPS_BATTERY_VOLTAGE("UPS", "UPS电池电压", "environment.ups_battery_voltage", "V", "38", 22d, 70d, "L1"),
	K8S_CPU_UTIL("容器", "容器CPU利用率", "k8s.cpu_util", "%", "50", 0d, 100d, "L3"),
	K8S_MEMORY_UTIL("容器", "容器内存利用率", "k8s.memory_util", "%", "50", 0d, 100d, "L3"),
	K8S_MEMORY_USAGE("容器", "容器内存使用量", "k8s.memory_usage", "MB", "1024", 0d, 102400d, "L3"),
	APP_CPU_UTIL("应用", "应用CPU利用率", "container.cpu_util", "%", "50", 0d, 100d, "L4"),
	APP_MEMORY_UTIL("应用", "应用内存利用率", "container.memory_util", "%", "50", 0d, 100d, "L4"),
	APP_MEMORY_USAGE("应用", "应用内存使用量", "container.memory_usage", "MB", "1024", 0d, 102400d, "L4"),
	APP_NET_RX_RATE("应用", "应用网络下行速率", "container.net_rx_rate", "KB/s", "1024", 0d, 1024000d, "L4"),
	APP_NET_TX_RATE("应用", "应用网络上行速率", "container.net_tx_rate", "KB/s", "1024", 0d, 1024000d, "L4"),
	APP_FS_WRITES_RATE("应用", "应用磁盘写速率", "container.fs_writes_rate", "KB/s", "1024", 0d, 1024000d, "L4"),
	APP_FS_READS_RATE("应用", "应用磁盘读速率", "container.fs_reads_rate", "KB/s", "1024", 0d, 1024000d, "L4"),
	APP_CORES("应用", "应用CPU核数", "container.cores", "", "0.2", 0d, 0.9d, "L4"),
	APP_STORAGE_USAGE("应用", "应用存储使用量", "container.storage_usage", "GB", "0.2", 0d, 100d, "L4"),
	APP_BANDWIDTH("应用", "应用带宽", "container.bandWidth", "MB/s", "1", 0d, 1024d, "L4"),
	APP_RESPOND_TIME("应用", "应用迁移时长", "container.respond_time", "s", "3", 1d, 6d, "L4"),
	SWITCHER_IN_OCTETS("交换机", "交换机收包速率", "switcher.in_octets", "KB/s", "1024", 0d, 1024000d, "L2"),
	SWITCHER_OUT_OCTETS("交换机", "交换机发包速率", "switcher.out_octets", "KB/s", "1024", 0d, 1024000d, "L2"),
	VM_CPU_UTIL("虚拟机", "虚拟机CPU利用率", "vm.cpu_util", "%", "20", 0d, 100d, "L3"),
	VM_MEMORY_UTIL("虚拟机", "虚拟机内存利用率", "vm.memory_util", "%", "30", 0d, 100d, "L3"),
	COST_LOAD("迁移代价", "迁移负荷差", "cost.load", "KW", "60", 0d, 100d, ""),
	COST_TEMPERATURE("迁移代价", "迁移温度差", "cost.temperature", "°C", "4", 0d, 9d, ""),
	COST_POWER("迁移代价", "迁移功耗差", "cost.power", "KWH", "75", 0d, 150d, ""),
	COST_RESPONSE_TIME("迁移代价", "迁移响应时间差", "cost.response_time", "s", "0.6", 0d, 1.2d, ""),
	DATACENTER_LOAD("数据中心", "数据中心配电子网负荷", "datacenter.load", "KW", "3000", 2000d, 5000d, "L0"),
	DATACENTER_POWER("数据中心", "数据中心能耗", "datacenter.power", "KWH", "1500", 1300d, 3000d, "L0"),
	DATACENTER_IT_POWER("数据中心", "数据中心IT设备能耗", "datacenter.it_power", "KWH", "1200", 800d, 1300d, "L0"),
	DATACENTER_TEMPERATURE("数据中心", "数据中心温度", "datacenter.temperature", "°C", "23", 18d, 32d, "L0"),
	DATACENTER_ENVIRONMENT_POWER("数据中心","数据中心动环能耗","datacenter.environment_power", "KWH", "1000", 100d, 2000d, "L0"),
	PUE("PUE", "数据中心能效比", "datacenter.pue", "", "1.3", 1d, 1.9d, "L0"),
	DATACENTER_CPU_UTIL("数据中心", "数据中心CPU利用率", "datacenter.cpu_util", "%", "30", 0d, 100d, "L3"),
	DATACENTER_MEMORY_UTIL("数据中心", "数据中心内存利用率", "datacenter.memory_util", "%", "25", 0d, 100d, "L3"),
	DATACENTER_RESPONSE_TIME("数据中心", "数据中心应用迁移时长", "datacenter.response_time", "s", "1.3", 0.1d, 10d, "L4"),
	DATACENTER_QOS("数据中心", "数据中心应用QOS", "datacenter.qos", "ms", "500", 0d, 1000d, "L4"),
	DATACENTER_APP_CPU_UTIL("数据中心", "数据中心APPcpu利用率", "datacenter.app_cpu_util", "%", "30", 0d, 100d, "L4"),
	DATACENTER_APP_MEMORY_UTIL("数据中心", "数据中心APP内存利用率", "datacenter.app_memory_util", "%", "25", 0d, 100d, "L4"),
	DATACENTER_BANDWIDTH("数据中心", "数据中心带宽", "datacenter.bandWidth", "MB/s", "1000", 100d, 2000d, "L2"),
	DATACENTER_SERVER_POWER("数据中心", "数据中心实时功耗", "datacenter.server_power", "KW", "265", 150d, 450d, "L2"),
	DATACENTER_SERVER_TEMPERATURE("数据中心", "数据中心温度", "datacenter.server_temperature", "°C", "23", 18d, 32d, "L2"),
	DATACENTER_SERVER_CPU_FREQUENCY("数据中心", "数据中心CPU频率", "datacenter.server_cpu_frequency", "GHz", "5", 0d, 20d, "L2"),
	DATACENTER_SERVER_CPU_LOAD("数据中心", "数据中心CPU负载", "datacenter.server_cpu_load_1min", "", "5.2", 0.2d, 30d, "L2"),
	DATACENTER_SERVER_CPU_UTIL("数据中心", "数据中心CPU利用率", "datacenter.host_cpu_util", "%", "30", 0d, 100d, "L2"),
	DATACENTER_SERVER_MEMORY_UTIL("数据中心", "数据中心存利用率", "datacenter.mem_speed", "MHz", "2000", 800d, 5000d, "L2"),
	DATACENTER_SERVER_DISK_READ_RATE("数据中心", "数据中心磁盘读速率", "datacenter.server_disk_read_rate", "KB/s", "1024", 0d, 1024000d, "L2"),
	DATACENTER_SERVER_DISK_WRITE_RATE("数据中心", "数据中心磁盘写速率", "datacenter.server_disk_write_rate", "KB/s", "1024", 0d, 1024000d, "L2");
	
	private String type;
	private String name;
	private String meter;
	private String unit;
	private String value;
	private Double start;
	private Double end;
	private String layer;
	
	MonitorMeter(String type, String name, String meter, String unit, String value, Double start, Double end, String layer) {
		this.type = type;
		this.name = name;
		this.meter = meter;
		this.unit = unit;
		this.value = value;
		this.start = start;
		this.end = end;
		this.layer = layer;
	}
	
	public String getType() {
		return type;
	}
	
	public String getName() {
		return name;
	}

	public String getMeter() {
		return meter;
	}
	
	public String getUnit() {
		return unit;
	}
	
	public String getValue() {
		return value;
	}
	
	public Double getStart() {
		return start;
	}
	
	public Double getEnd() {
		return end;
	}
	
	public String getLayer() {
		return layer;
	}
	
	public static String getNameByMeter(String meter) {
		for (MonitorMeter meters : MonitorMeter.values()) {
			if(meters.getMeter().equals(meter)) {
				return meters.getName();
			}
		}
		return "unknown";
	}
	
	public static MonitorMeter getByMeter(String meter) {
		for (MonitorMeter meters : MonitorMeter.values()) {
			if(meters.getMeter().equals(meter)) {
				return meters;
			}
		}
		return null;
	}
	
	public static List<MonitorMeter> getTypeByName(String meter) {
		List<MonitorMeter> res = new ArrayList<MonitorMeter>();
		for(MonitorMeter meters : MonitorMeter.values()) {
			if(meters.getMeter().startsWith(meter)) {
				res.add(meters);
			}
		}
		return res;
	}
}
