package com.gcloud.mesh.header.vo.dcs;

import lombok.Data;

@Data
public class ComputeOverview {
	private Integer appCount;
	private Integer k8sCount;
	private Integer vmCount;
	private Integer nodeCount;
	private Integer iaasCount;
	private Integer datacenterCount;
	private Integer itCount;
	private Integer cloudResourceCount;
}
