package com.gcloud.mesh.header.exception;

public class AlertErrorCode {

    public static final String REGIONID_NOT_BLANK = "150101";
    public static final String RESOURCENAME_NOT_BLANK = "150102";
    public static final String NOTIFYCOUNT_NOT_NULL = "150103";
    public static final String CLOUDRESOURCETYPE_NOT_BLANK = "150104";
    public static final String CREATEHIS_LEVEL_NOT_NULL = "150105";

    public static final String STATUS_NOT_NULL = "150106";
    public static final String CLOUDRESOURCEID_NOT_BLANK = "150107";

    public static final String PAGELINKMSG_USERNAME_NOT_NULL = "150201";
    public static final String PAGELINKMSG_EMAIL_NOT_NULL = "150202";
    public static final String PAGELINKMSG_PHONE_NOT_NULL = "150203";
    public static final String LINKMANID_NOT_NULL = "150204";
    public static final String USERNAME_NOT_NULL = "150205";

    public static final String POLICYNAME_NOT_NULL = "150301";
    public static final String ENABLE_NOT_NULL = "150302";
    public static final String STARTTIME_NOT_NULL = "150303";
    public static final String ENDTIME_NOT_NULL = "150304";
    public static final String CHANNELSILENCE_NOT_NULL = "150305";
    public static final String NOTIFYENABLE_NOT_NULL = "150306";
    public static final String RULEVOS_NOT_NULL = "150307";
    public static final String LINKMANS_NOT_NULL = "150308";
    public static final String MANAGETYPE_NOT_NULL = "150309";
    public static final String PLATFORMTYPE_NOT_NULL = "150310";
    public static final String POLICYID_NOT_NULL = "150311";

    public static final String RULEID_NOT_NULL = "150401";
}
