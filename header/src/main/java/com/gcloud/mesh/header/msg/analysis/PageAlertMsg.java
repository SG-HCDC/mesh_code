
package com.gcloud.mesh.header.msg.analysis;

import java.util.Date;

import org.springframework.format.annotation.DateTimeFormat;

import com.gcloud.mesh.header.msg.BaseQueryMsg;

import lombok.Data;

@Data
public class PageAlertMsg extends BaseQueryMsg {

    private String deviceType;
    private String meter;
    private String level;
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date startTime;
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date endTime;

}
