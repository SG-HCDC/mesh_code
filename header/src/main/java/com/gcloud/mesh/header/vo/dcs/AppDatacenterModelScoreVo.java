package com.gcloud.mesh.header.vo.dcs;

import lombok.Data;

@Data
public class AppDatacenterModelScoreVo {
    private String datacenterId;
    private String datacenterName;
    private Integer deviceTotalScore;
    private Integer costTotalScore;
    private Integer powerTotalScore;
    private Integer electricTotalScore;
    	
}
