package com.gcloud.mesh.header.enums;

import java.util.stream.Stream;

public enum DataCleanOperateType {
	
	REPLACE("replace", "替换"),
	DELETE("delete", "删除");
	
	private String name;
	private String cnName;
	
	DataCleanOperateType(String name, String cnName) {
		this.name = name;
		this.cnName = cnName;
	}
	
	public static DataCleanOperateType getByName(String name) {
		return Stream.of(DataCleanOperateType.values()).filter( s -> s.getName().equals(name)).findFirst().orElse(null);
	}
	
	public String getName() {
		return name;
	}
	
	public String getCnName() {
		return cnName;
	}
}
