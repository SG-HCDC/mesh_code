package com.gcloud.mesh.header.vo.alert;

import java.util.Date;

import javax.validation.constraints.DecimalMax;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.Digits;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.Range;

import com.gcloud.mesh.header.enums.MonitorMeter;
import com.gcloud.mesh.header.exception.CommonErrorCode;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class RuleVo {
	private String id;

	@NotNull(message = "::策略ID不能为空")
	@Length(min = 1, max = 60, message = "::策略D" + CommonErrorCode.LENGTH60)
	private String policyId;

	@ApiModelProperty(value = "监控项")
	@NotNull(message = "::监控项不能为空")
	private String meter;

	private String meterChnName;

	private String unit;

	@NotNull(message = "::资源类型不能为空")
	private String resourceType;

	private String resourceId;

	@ApiModelProperty(value = "阈值")
	@NotNull(message = "::阈值不能为空")
	// @Range( min = 0, max = 9999, message = "::阈值取值范围0~9999")
	@Digits(integer = 6, fraction = 2, message = "::阈值取值范围0~999999")
	// @DecimalMax(value = "999999", message = "::阈值取值范围0~999999")
	// @DecimalMin(value = "0", message = "::阈值取值范围0~999999")
	private Double threshold;

	@ApiModelProperty(value = "是否开启禁用")
	@NotNull(message = "::禁用开关不能为空")
	private Boolean enabled;

	@ApiModelProperty(value = "统计方法，现阶段支持avg")
	private String statistics;

	@ApiModelProperty(value = "比较符：0:>=,1:>,2:<=,3:<,4:=,5:!=")
	@NotNull(message = "::比较符不能为空")
	private Integer comparisonOperator;

	@ApiModelProperty(value = "连续多少秒超过阈值")
	@NotNull(message = "::持续时间不能为空")
	private Integer duration;

	private Date createTime;

	@ApiModelProperty(value = "告警等级：1：一般、2：重要、3：严重")
	@NotNull(message = "::告警等级不能为空")
	private Integer level;

	@Override
	public String toString() {
		String levelstr = "";
		if (level.equals(1)) {
			levelstr = "一般";
		} else if (level.equals(2)) {
			levelstr = "重要";
		} else if (level.equals(3)) {
			levelstr = "严重";
		}
		String meterStr = "";
		if(meter != null) {
			for (MonitorMeter mmeter : MonitorMeter.values()) {
				if(mmeter.getMeter().equals(meter)) {
					meterStr = mmeter.getName();
				}
			}
		}
		String comparisonOperatorStr = "";
		//1:>,2:<=,3:<,4:=,5:!="
		if(comparisonOperator != null) {
			if(comparisonOperator.equals(0)) {
				comparisonOperatorStr = ">=";
			}else if(comparisonOperator.equals(1)) {
				comparisonOperatorStr = ">";
			}else if(comparisonOperator.equals(2)) {
				comparisonOperatorStr = "<=";
			}else if(comparisonOperator.equals(3)) {
				comparisonOperatorStr = "<";
			}else if(comparisonOperator.equals(4)) {
				comparisonOperatorStr = "=";
			}else if(comparisonOperator.equals(5)) {
				comparisonOperatorStr = "!=";
			}
			
		}
		return "[监控项=" + meterStr+" "+comparisonOperatorStr  +threshold+ ", 级别=" + levelstr + "]";
	}

}
