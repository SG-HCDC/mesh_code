
package com.gcloud.mesh.header.vo;

import lombok.Data;

@Data
public class IscServerUrlVo {

    private String iscServerUrl;

}
