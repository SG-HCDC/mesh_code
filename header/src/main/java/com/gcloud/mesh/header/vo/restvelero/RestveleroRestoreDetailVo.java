package com.gcloud.mesh.header.vo.restvelero;

import lombok.Data;

@Data
public class RestveleroRestoreDetailVo {

	private RestveleroRestoreDataVo data;
	private Integer status;

}
