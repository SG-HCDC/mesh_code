package com.gcloud.mesh.header.vo.asset;

import java.util.Date;

import lombok.Data;

@Data
public class CloudResourceItemVo {

	private String id;
	private String name;
	private Integer type;
	private String typeName;
	private String datacenterId;
	private String datacenterName;
	private Date createTime;
	private String nodeId;
	private String nodeName;
	private String from;
	private Integer cpuCores;
	private Integer memory;
	private String objectId;
}
