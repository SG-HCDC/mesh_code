package com.gcloud.mesh.header.msg.dcs;

import java.util.List;

import com.gcloud.mesh.header.msg.BaseMsg;
import com.gcloud.mesh.header.vo.dcs.AuthorityItemVo;

import lombok.Data;

@Data
public class SetAuthorityMsg extends BaseMsg {
	
	private List<AuthorityItemVo> items;

	@Override
	public String toString() {
		StringBuffer buff = new StringBuffer();
		buff.append("设置权限参数：");
		for(AuthorityItemVo vo : items) {
			buff.append(vo.toString());
		}
		return buff.toString();
	}
	

}
