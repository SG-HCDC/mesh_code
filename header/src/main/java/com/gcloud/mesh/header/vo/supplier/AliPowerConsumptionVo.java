package com.gcloud.mesh.header.vo.supplier;

import lombok.Data;

@Data
public class AliPowerConsumptionVo {
	
	private String hostIp;
	private String idToken;

}
