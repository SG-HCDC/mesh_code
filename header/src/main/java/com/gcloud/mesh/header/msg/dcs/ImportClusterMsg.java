package com.gcloud.mesh.header.msg.dcs;

import com.gcloud.mesh.header.constant.RegexpPattern;
import com.gcloud.mesh.header.exception.AssetErrorCode;
import com.gcloud.mesh.header.exception.CommonErrorCode;
import com.gcloud.mesh.header.exception.SpecialCode;
import com.gcloud.mesh.header.msg.BaseMsg;
import com.gcloud.mesh.header.param.AliMigrationParam;
import com.gcloud.mesh.header.param.ClusterMigrationParam;
import com.gcloud.mesh.header.param.HuaweiMigrationParam;
import lombok.Data;
import org.hibernate.validator.constraints.Length;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;

@Data
public class ImportClusterMsg extends BaseMsg {


    @NotBlank( message = AssetErrorCode.MODEL_NOT_BLANK)
    @Length( min = 1, max = 60, message = AssetErrorCode.NAME_LENGTH_ERROR)
    @Pattern( regexp = RegexpPattern.STAND_NAME, message = AssetErrorCode.NAME_FORMAT_ERROR)
    @SpecialCode(message = "::国网云名称"+ CommonErrorCode.SPECIAL)
    private String name;

    @NotBlank( message = "190102")
    @SpecialCode(message = "::国网云类型"+ CommonErrorCode.SPECIAL)
    private String type;

    @NotBlank( message = "190103")
    @Length( min = 1, max = 60, message = AssetErrorCode.DATACENTER_ID_LENGTH_ERROR)
    @Pattern( regexp = RegexpPattern.STAND_ID, message = AssetErrorCode.DATACENTER_ID_FORMAT_ERROR)
    @SpecialCode(message = "::数据中心ID"+CommonErrorCode.SPECIAL)
    private String datacenterId;


    // k8s
    private ClusterMigrationParam clusterMigrationParam;

    // 华为
    private HuaweiMigrationParam huaweiMigrationParam;

    // 阿里
    private AliMigrationParam aliMigrationParam;



}
