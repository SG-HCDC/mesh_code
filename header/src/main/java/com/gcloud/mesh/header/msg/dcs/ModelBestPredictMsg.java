
package com.gcloud.mesh.header.msg.dcs;

import javax.validation.constraints.NotBlank;

import com.gcloud.mesh.header.exception.ModelErrorCode;
import com.gcloud.mesh.header.msg.BaseMsg;

import lombok.Data;

@Data
public class ModelBestPredictMsg extends BaseMsg {

	@NotBlank( message = ModelErrorCode.MODEL_TYPE_NOT_BLANK)
    private String modelType;

}
