
package com.gcloud.mesh.header.vo.supplier;

import lombok.Data;

@Data
public class SupplierVo {

    private String id;
    private String name;
    private String type;
    private String datacenterId;
    private String config;

}
