
package com.gcloud.mesh.header.vo.dcs;

import java.util.Date;

import lombok.Data;

@Data
public class SchedulerStepVo {

	private String id;
	private String schedulerJobId;
	private Integer status;
	private String statusName;
	private String name;
	private Date beginTime;
	private Date endTime;
	private String resultMsg;
}
