package com.gcloud.mesh.header.vo.supplier;

import lombok.Data;

@Data
public class YingZeSupervisionSystemVo extends SupervisionSystemAbstract{

	private static final String TYPE = "yingze";
	
	private String hostIp;
	
	private String port;
	
	@Override
	public String getType() {
		return TYPE;
	}

}
