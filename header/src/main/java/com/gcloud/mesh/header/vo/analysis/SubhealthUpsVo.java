
package com.gcloud.mesh.header.vo.analysis;

import lombok.Data;

@Data
public class SubhealthUpsVo {

    private String id;
    private String name;
    private String deviceModel;
    private String upsVoltageIn;
    private String upsVoltageOut;
    private String upsTemp;
    private Boolean subhealth;
    private String datacenterName;

}
