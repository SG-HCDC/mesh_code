package com.gcloud.mesh.header.vo.asset;

import java.util.Date;

import lombok.Data;

@Data
public class DeviceItemVo {
	
	private String id;
	private String name;
	private String esn;
	private Integer type;
	private String typeCnName;
	private String deviceId;
	private String deviceModel;
	private String deviceManufacturer;
	private String datacenterId;
	private String datacenterName;
//	private String creator;
	private Date createTime;
	private String from;

}
