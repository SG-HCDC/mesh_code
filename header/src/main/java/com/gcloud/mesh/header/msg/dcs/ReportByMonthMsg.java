package com.gcloud.mesh.header.msg.dcs;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;

import org.apache.commons.lang3.StringUtils;

import com.gcloud.mesh.header.exception.DatacenterEnergyErrorCode;
import com.gcloud.mesh.header.msg.BaseMsg;
import com.gcloud.mesh.header.msg.BaseQueryMsg;

import lombok.Data;

@Data
public class ReportByMonthMsg extends BaseQueryMsg {

//	@NotBlank( message = DatacenterEnergyErrorCode.DATACENTER_ID_NOT_BLANK)
	private String datacenterId;
	
//	@NotBlank( message = DatacenterEnergyErrorCode.YEAR_NOT_BLANK)
//	@Pattern( regexp = "^\\d{4}$", message = DatacenterEnergyErrorCode.YEAR_FORMAT_ERROR)
	private String year;
	
	private int month;
	
	@Override
	public String toString() {
		StringBuffer buff = new StringBuffer();
		if(StringUtils.isNotBlank(datacenterId)) {
			buff.append(" 数据中心ID："+datacenterId);
		}
		if(StringUtils.isNotBlank(year)) {
			buff.append(" 年份："+year);
		}
		if(month != 0) {
			buff.append(" 月份："+month);
		}
		return buff.toString();
	}
}
