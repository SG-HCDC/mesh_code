package com.gcloud.mesh.header.msg.alert;

import com.gcloud.mesh.header.msg.BaseQueryMsg;

import lombok.Data;

@Data
public class PagePolicyMsg extends BaseQueryMsg {

	private String resourceType;
}
