package com.gcloud.mesh.header.msg.asset;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

import org.hibernate.validator.constraints.Length;

import com.gcloud.mesh.header.constant.RegexpPattern;
import com.gcloud.mesh.header.exception.AssetErrorCode;
import com.gcloud.mesh.header.exception.CommonErrorCode;
import com.gcloud.mesh.header.exception.SpecialCode;
import com.gcloud.mesh.header.msg.BaseMsg;

import lombok.Data;

@Data
public class CreateDatacenterMsg extends BaseMsg {

	@NotBlank( message = AssetErrorCode.NAME_NOT_BLANK)
	@Length( min = 1, max = 60, message = AssetErrorCode.NAME_LENGTH_ERROR)
	@SpecialCode(message = "::数据中心名称"+CommonErrorCode.SPECIAL)
//	@Pattern( regexp = RegexpPattern.STAND_NAME, message = AssetErrorCode.NAME_FORMAT_ERROR)
	private String name;
	
	@NotBlank( message = AssetErrorCode.IP_NOT_BLANK)
	@Pattern( regexp = RegexpPattern.IP, message = AssetErrorCode.IP_FORAMT_ERROR)
	private String ip;
	
	//增加架构参数
	private String structure;
	
	//增加机房名,以逗号分隔
	private String rooms;
}
