package com.gcloud.mesh.header.msg.dcs;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

import org.apache.commons.lang3.StringUtils;

import com.gcloud.mesh.header.exception.DataSecurityErrorCode;
import com.gcloud.mesh.header.msg.BaseMsg;

import lombok.Data;

@Data
public class SetDataSecurityMsg extends BaseMsg {

	@NotBlank( message = DataSecurityErrorCode.FREQUENCY_NOT_BLANK)
	private String frequency;
	@NotBlank( message = DataSecurityErrorCode.TIME_NOT_BLANK)
	@Pattern( regexp = "^(0?[0-9]|1[0-9]|[2][0-3]):(0?[0-9]|[1-5][0-9]):(0?[0-9]|[1-5][0-9])$", message = DataSecurityErrorCode.TIME_NOT_BLANK)
	private String time;
	@NotBlank( message = DataSecurityErrorCode.POLICY_NOT_BLANK)
	private String policy;
	
	@NotNull( message = DataSecurityErrorCode.ENABLED_NOT_NULL)
	private Boolean enabled;
	
	@Override
	public String toString() {
		String enableStr = "";
		if(enabled != null) {
			if(enabled) {
				enableStr = "开启";
			}else {
				enableStr = "关闭";
			}
		}
		String fq= "";
		if(StringUtils.isNotBlank(frequency)) {
			if(frequency.equals("daily")) {
				fq = " 每天";
			}else if(frequency.equals("weekly")) {
				fq = " 每周一";
			}else if(frequency.equals("monthly")) {
				fq = " 每月一号";
			}
		}
		String pl = "";
		if(StringUtils.isNotBlank(policy)) {
			if("increment".equals(policy)) {
				pl = " 增量备份";
			}else {
				pl = " 完全备份";
			}
		}
		return " 是否开启=" + enableStr +" "+"备份频率="+fq+" 备份时间="+time+" "+"备份策略="+pl+"";
	}
}
