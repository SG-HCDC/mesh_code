
package com.gcloud.mesh.header.msg.dcs;

import org.hibernate.validator.constraints.Length;

import com.gcloud.mesh.header.exception.CommonErrorCode;
import com.gcloud.mesh.header.exception.SpecialCode;
import com.gcloud.mesh.header.msg.BaseQueryMsg;

import lombok.Data;

@Data
public class PageSchedulerAdviceMsg extends BaseQueryMsg {
	@Length( min = 1, max = 10, message = "::应用名称"+CommonErrorCode.LENGTH10)
	@SpecialCode(message = "::应用名称"+CommonErrorCode.SPECIAL)
	private String name;
	
	@Length( min = 1, max = 60, message = "::数据中心ID"+CommonErrorCode.LENGTH60)
	@SpecialCode(message = "::数据中心ID"+CommonErrorCode.SPECIAL)
	private String datacenterId;
	
	private Integer modelType;
}
