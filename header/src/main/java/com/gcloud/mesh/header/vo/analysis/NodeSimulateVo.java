package com.gcloud.mesh.header.vo.analysis;

import lombok.Data;

@Data
public class NodeSimulateVo {

	private String Id;
	private String name;
	private String deviceId;
	private String datacenterId;
	private Double simulateMetric;
	private Double simulateValue;
	
}
