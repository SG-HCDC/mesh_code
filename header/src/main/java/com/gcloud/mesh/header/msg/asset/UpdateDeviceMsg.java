package com.gcloud.mesh.header.msg.asset;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;

import org.hibernate.validator.constraints.Length;
import org.springframework.lang.Nullable;

import com.gcloud.mesh.header.exception.AssetErrorCode;
import com.gcloud.mesh.header.exception.CommonErrorCode;
import com.gcloud.mesh.header.exception.SpecialCode;
import com.gcloud.mesh.header.msg.BaseMsg;

import lombok.Data;

@Data
public class UpdateDeviceMsg extends BaseMsg {
	
	@NotBlank( message = AssetErrorCode.ID_NOT_BLANK)
	@Length( min = 1, max = 60, message = AssetErrorCode.ID_LENGTH_ERROR)
	@Pattern( regexp = "^[0-9a-zA-Z-]{1,}$", message = AssetErrorCode.ID_FORMAT_ERROR)
	@SpecialCode(message = "::设备ID"+CommonErrorCode.SPECIAL)
	private String id;
	
	@Nullable
	@Length( min = 1, max = 10, message = AssetErrorCode.NAME_LENGTH_ERROR)
	@SpecialCode(message = "::设备名称"+CommonErrorCode.SPECIAL)
//	@Pattern( regexp = "^[0-9a-zA-Z_]{1,}$", message = AssetErrorCode.NAME_FORMAT_ERROR)
	private String name;
	
	@Nullable
	@Length( min = 1, max = 10, message = AssetErrorCode.ESN_LENGTH_ERROR)
	@SpecialCode(message = "::资源序列号"+CommonErrorCode.SPECIAL)
//	@Pattern( regexp = "^[0-9a-zA-Z_]{1,}$", message = AssetErrorCode.ESN_FORMAT_ERROR)
	private String esn;
	
	@Nullable
	private Integer type;
	
	@Nullable
	@Length( min = 1, max = 10, message = AssetErrorCode.MODEL_LENGTH_ERROR)
	@SpecialCode(message = "::设备型号"+CommonErrorCode.SPECIAL)
//	@Pattern( regexp = "^[0-9a-zA-Z_]{1,}$", message = AssetErrorCode.MODEL_FORMAT_ERROR)
	private String deviceModel;
	
	@Nullable
	@Length( min = 1, max = 10, message = AssetErrorCode.MANUFACTURER_LENGTH_ERROR)
	@SpecialCode(message = "::设备厂商"+CommonErrorCode.SPECIAL)
//	@Pattern( regexp = "^[0-9a-zA-Z_]{1,}$", message = AssetErrorCode.MANUFACTURER_FORMAT_ERROR)
	private String deviceManufacturer;
	
	@Nullable
	@Length( min = 1, max = 60, message = AssetErrorCode.DATACENTER_ID_LENGTH_ERROR)
	@Pattern( regexp = "^[0-9a-zA-Z_-]{1,}$", message = AssetErrorCode.DATACENTER_ID_FORMAT_ERROR)
	@SpecialCode(message = "::数据中心ID"+CommonErrorCode.SPECIAL)
	private String datacenterId;

	@Override
	public String toString() {
		String typeName = "";
		if(type!=null && type == 1) {
			typeName = " 空调";
		}
		if(type!=null && type == 2) {
			typeName = " UPS";
		}
		if(type!=null && type == 3) {
			typeName = " 配电箱";
		}
		return " 名称=" + name + " 设备识别号=" + esn +" 设备型号=" + deviceModel
				+ " 设备厂商=" + deviceManufacturer + "";
	}

}
