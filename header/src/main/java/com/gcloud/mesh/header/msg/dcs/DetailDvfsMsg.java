package com.gcloud.mesh.header.msg.dcs;

import com.gcloud.mesh.header.constant.RegexpPattern;
import com.gcloud.mesh.header.exception.CommonErrorCode;
import com.gcloud.mesh.header.exception.DvfsErrorCode;
import com.gcloud.mesh.header.exception.SpecialCode;
import com.gcloud.mesh.header.msg.BaseMsg;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;

@Data
public class DetailDvfsMsg extends BaseMsg {
    @NotBlank
    @Pattern( regexp = RegexpPattern.STAND_ID, message = DvfsErrorCode.ID_FORMAT_ERROR)
    @Length( min = 1, max = 60, message = DvfsErrorCode.ID_LENGTH_ERROR)
    @SpecialCode(message = "::电压调频ID"+CommonErrorCode.SPECIAL)
    private String id;
}
