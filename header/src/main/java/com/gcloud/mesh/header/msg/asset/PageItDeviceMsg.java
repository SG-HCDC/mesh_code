package com.gcloud.mesh.header.msg.asset;

import javax.validation.constraints.Pattern;

import org.hibernate.validator.constraints.Length;
import org.springframework.lang.Nullable;

import com.gcloud.mesh.header.constant.RegexpPattern;
import com.gcloud.mesh.header.exception.AssetErrorCode;
import com.gcloud.mesh.header.exception.CommonErrorCode;
import com.gcloud.mesh.header.exception.SpecialCode;
import com.gcloud.mesh.header.msg.BaseQueryMsg;

import lombok.Data;

@Data
public class PageItDeviceMsg extends BaseQueryMsg {

	@Nullable
	@Length( min = 1, max = 10, message = AssetErrorCode.NAME_LENGTH_ERROR)
	@SpecialCode(message = "::设备名称"+CommonErrorCode.SPECIAL)
	private String name;
	
	@Nullable
	@Length( min = 1, max = 60, message = AssetErrorCode.DATACENTER_ID_LENGTH_ERROR)
	@Pattern( regexp = RegexpPattern.STAND_ID, message = AssetErrorCode.DATACENTER_ID_FORMAT_ERROR)
	@SpecialCode(message = "::数据中心ID"+CommonErrorCode.SPECIAL)
	private String datacenterId;
	
	@Nullable
	private Integer type;
}
