package com.gcloud.mesh.header.msg.alert;

import com.gcloud.mesh.header.msg.BaseReplyMsg;

public class DeleteLinkmanReplyMsg extends BaseReplyMsg {
	private String linkManName;

	public String getLinkManName() {
		return linkManName;
	}

	public void setLinkManName(String linkManName) {
		this.linkManName = linkManName;
	}

}
