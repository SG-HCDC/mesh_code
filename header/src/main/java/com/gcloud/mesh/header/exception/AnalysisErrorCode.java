package com.gcloud.mesh.header.exception;

public class AnalysisErrorCode {
	
	public static final String NODE_ID_NOT_BLANK = "060101";
	public static final String NODE_ID_FORMAT_ERROR = "060102";
	public static final String AIR_ID_NOT_BLANK = "060103";
	public static final String AIR_ID_FORMAT_ERROR = "060104";
	public static final String METRICX_NOT_BLANK = "060105";
	public static final String METRICY_NOT_BLANK = "060106";
	public static final String DEGREE_NOT_NULL = "060107";
	public static final String STARTTIME_NOT_NULL = "060108";
	public static final String ENDTIME_NOT_NULL = "060109";
	
	public static final String DATABASE_SAVE_ERROR = "060201";
	public static final String DATABASE_UPDATE_ERROR = "060202";
	public static final String DATACLEANING_SERVICE_ERROR = "060203";
	public static final String TYPE_NOT_FOUND = "060204";
	public static final String DATEBASE_DELETE_ERROR = "060205";
	public static final String RESOURCE_NOT_FOUND = "060206";
	public static final String RESOURCE_ALREADY_EXISTS = "060207";
	public static final String SIMULATE_RUNNING = "060208";
	public static final String DEGREE_NOT_SUPPORT = "060209";
	
	public static final String STATISTICS_SERVICE_ERROR = "060301";
	public static final String ASSET_SERVICE_ERROR = "060302";
	public static final String SUPPLIER_SERVICE_ERROR = "060303";
	public static final String METER_DUPLICATE_ERROR = "060304";
	public static final String METER_NOT_FOUND = "060305";
	public static final String LEVEL_NOT_FOUND = "060306";
	public static final String DEVICETYPE_NOT_FOUND = "060307";
	public static final String DATACENTERID_NOT_NULL = "060401";
	public static final String NAME_NOT_NULL = "060402";	
	
}
