
package com.gcloud.mesh.header.msg.dcs;

import javax.validation.constraints.NotBlank;

import org.apache.commons.lang3.StringUtils;

import com.gcloud.mesh.header.exception.SchedulerErrorCode;
import com.gcloud.mesh.header.msg.BaseMsg;

import lombok.Data;

@Data
public class UpdateSchedulerStrategyRuleMsg extends BaseMsg {

	@NotBlank( message = SchedulerErrorCode.RULE_NOT_BLANK) 
	private String rule;

	@Override
	public String toString() {
		if(StringUtils.isNotBlank(rule)) {
			rule = rule.replace("cooling", " 冷却时间").replace("beginTime", " 开始时间").replace("endTime", " 结束时间").replace("concurrent", " 并发数").replace("{", "").replace("}", "");
		}
		return "更新迁移规则 [规则=" + rule + "]";
	}
	
	
}
