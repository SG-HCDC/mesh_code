
package com.gcloud.mesh.header.vo.dcs;

import java.util.Date;

import lombok.Data;

@Data
public class SchedulerJobVo {

	private String id;
	private Integer status;
	private String statusName;
	private String errorStep;
	private String appId;
	private String appName;
	private String cloudResourceId;
	private String cloudResourceName;
	private Integer cloudResourceType;
	private String cloudResourceTypeName;
	private String srcDatacenterId;
	private String srcDatacenterName;
	private String dstDatacenterId;
	private String sourceNodeId;
	private String dstNodeId;
	private String dstDatacenterName;
	private Date beginTime;
	private Date endTime;
	private Integer schedulerModelTool;
    private Integer schedulerStrategyConfig;
    private Integer schedulerModel;
    private int stepCount;//成功步骤数量
    private String schedulerProgress;//执行的百分比
    private String instanceName;
}
