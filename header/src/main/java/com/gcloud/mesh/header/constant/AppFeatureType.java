
package com.gcloud.mesh.header.constant;

import java.util.stream.Stream;

public enum AppFeatureType {

    CPU,
    IO,
    AI,
	UNKNOWN;
	
	public static AppFeatureType getByName(String name) {
		return Stream.of(AppFeatureType.values())
				.filter( s -> s.name().equals(name))
				.findFirst()
				.orElse(null);
	}
}
