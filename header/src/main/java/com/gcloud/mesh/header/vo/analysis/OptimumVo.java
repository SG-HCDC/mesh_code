package com.gcloud.mesh.header.vo.analysis;

import lombok.Data;

@Data
public class OptimumVo {
	
	private Double optimalValue;
	private String optimalValueCnName;
	private Double optimalMetric;
	private String metricX;
	private String metricXCnName;

}
