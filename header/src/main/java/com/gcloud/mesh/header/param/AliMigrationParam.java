package com.gcloud.mesh.header.param;

import lombok.Data;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

@Accessors(chain=true)
@Data
public class AliMigrationParam implements Serializable {

    private String myJobId;

    private String srcAppName;

    @NotBlank(message = "访问KEY不能为空")
    private String accessKeyId;
    @NotBlank(message = "访问秘钥不能为空")
    private String accessSecret;
    //目标实例ID
//    @NotBlank(message = "目标实例ID不能为空")
    private String instanceId;

    private String srcRegionId;

    private String srcInstanceId;



    //迁移源要迁入的目标阿里云地域ID。cn-shanghai
    @NotBlank(message = "地域ID不能为空")
    private String regionId;

    //迁移源
    private String sourceId;

    //目标阿里云服务器ECS的系统盘大小
    private Integer systemDiskSize;
}
