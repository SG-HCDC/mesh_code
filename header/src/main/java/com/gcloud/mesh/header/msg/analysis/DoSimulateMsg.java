package com.gcloud.mesh.header.msg.analysis;

import java.util.Date;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

import org.apache.commons.lang3.StringUtils;
import org.hibernate.validator.constraints.Length;
import org.springframework.format.annotation.DateTimeFormat;

import com.gcloud.mesh.header.constant.RegexpPattern;
import com.gcloud.mesh.header.exception.CommonErrorCode;
import com.gcloud.mesh.header.exception.SpecialCode;
import com.gcloud.mesh.header.msg.BaseMsg;

import lombok.Data;

@Data
public class DoSimulateMsg extends BaseMsg {

//	@NotBlank( message = "")
//	@Length( min = 1, max = 60, message = "")
//	@Pattern( regexp = RegexpPattern.STAND_NAME, message = "")
	@Length( min = 1, max = 60, message = "::数据中心ID"+CommonErrorCode.LENGTH60)
	@SpecialCode(message = "::数据中心ID"+CommonErrorCode.SPECIAL)
	private String datacenterId;
	@Length( min = 1, max = 60, message = "::服务器ID"+CommonErrorCode.LENGTH60)
	@SpecialCode(message = "::服务器ID"+CommonErrorCode.SPECIAL)
	private String deviceId;
	
//	@NotBlank( message = "")
//	@Length( min = 1, max = 60, message = "")
//	@Pattern( regexp = RegexpPattern.STAND_NAME, message = "")
	private String metricX;
	
//	@NotNull( message = "")
	private Integer degree;
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private Date startTime;
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private Date endTime;
	
	@Override
	public String toString() {
		StringBuffer buffer = new StringBuffer();
		buffer.append("仿真= 基于能耗传导的数据中心运行仿真");
		if(StringUtils.isNotBlank(datacenterId)) {
			buffer.append(" 数据中心ID="+datacenterId);
		}
		if(StringUtils.isNotBlank(deviceId)) {
			buffer.append(" 设备ID="+deviceId);
		}
		if(startTime != null) {
			buffer.append(" 开始时间="+startTime);
		}
		if(endTime != null) {
			buffer.append(" 结束时间="+endTime);
		}
		return buffer.toString();
	}

}
