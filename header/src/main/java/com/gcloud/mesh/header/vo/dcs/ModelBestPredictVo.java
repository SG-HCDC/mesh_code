
package com.gcloud.mesh.header.vo.dcs;

import lombok.Data;

@Data
public class ModelBestPredictVo {

	private String factorType;
    private String factorName;
    private Integer scoreBestPredict;
    private String unit;

}
