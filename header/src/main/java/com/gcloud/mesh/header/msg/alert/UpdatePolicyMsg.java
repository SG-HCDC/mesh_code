package com.gcloud.mesh.header.msg.alert;

import java.util.List;

import com.gcloud.mesh.header.exception.AlertErrorCode;
import com.gcloud.mesh.header.exception.CommonErrorCode;
import com.gcloud.mesh.header.exception.SpecialCode;
import com.gcloud.mesh.header.msg.BaseMsg;
import com.gcloud.mesh.header.vo.alert.RuleVo;
import javax.validation.constraints.NotBlank;

import org.apache.commons.lang3.StringUtils;
import org.hibernate.validator.constraints.Length;

import lombok.Data;

@Data
public class UpdatePolicyMsg extends BaseMsg {

	@NotBlank(message = AlertErrorCode.POLICYID_NOT_NULL)
	@Length(min = 1, max = 60, message = "::策略ID" + CommonErrorCode.LENGTH60)
	@SpecialCode(message = "::告警策略ID" + CommonErrorCode.SPECIAL)
	private String policyId;
	@Length(min = 1, max = 10, message = "::策略名称" + CommonErrorCode.LENGTH10)
	@SpecialCode(message = "::告警名称" + CommonErrorCode.SPECIAL)
	private String policyName;
	private Boolean enable;
	private Integer enableStartTime;
	private Integer enableEndTime;
	private Integer channelSilence;
	private Boolean notifyEnable;
	private List<RuleVo> ruleVos;

	@Override
	public String toString() {
		StringBuffer buff = new StringBuffer();
		// buff.append("更新告警策略:");
//		if(StringUtils.isNotBlank(policyName)) {
//			buff.append("策略名称");
//		}
		if(notifyEnable != null ) {
			buff.append("，邮箱通知：").append(notifyEnable?"开启":"关闭");
		}
		if (enable != null) {
			buff.append("，状态：").append(enable ? "启用" : "禁用").append("");
		}
		if (ruleVos != null) {
			buff.append("，策略规则：");
			for (RuleVo vo : ruleVos) {
				buff.append(vo.toString());
				buff.append("，");
			}
		}
		String con = buff.toString();
		if (con.endsWith("，")) {
			con.substring(0, con.length() - 1);
		}
		return con;
	}
}
