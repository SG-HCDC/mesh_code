package com.gcloud.mesh.header.vo.analysis;

import lombok.Data;

@Data
public class AirFitterVo {

	private String Id;
	private String name;
	private String region;
	private String currentTemp;
	private String setTemp;
	private String supplyAir;
	private String returnAir;
	private String datacenterId;
	
}
