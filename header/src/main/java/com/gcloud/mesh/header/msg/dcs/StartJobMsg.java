package com.gcloud.mesh.header.msg.dcs;

import lombok.Data;

@Data
public class StartJobMsg {
	private String instanceId;
	private String datacenterId;
	private String cloudResourceId;
}
