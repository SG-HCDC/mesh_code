
package com.gcloud.mesh.header.vo.dcs;

import lombok.Data;

@Data
public class SchedulerStrategyConfigVo {

    private String  name;
    private Integer  value;
    private Boolean isCheck;
}
