package com.gcloud.mesh.header.enums;

import java.util.stream.Stream;

public enum SubhealthAlertLevelType {
	
	INFO("info", "一般"),
	WARNING("warning", "警告"),
	CRITICAL("critical", "危急");
	
	private String name;
	private String cnName;
	
	SubhealthAlertLevelType(String name, String cnName) {
		this.name = name;
		this.cnName = cnName;
	}
	
	public static SubhealthAlertLevelType getByName(String name) {
		return Stream.of(SubhealthAlertLevelType.values())
				.filter( s -> s.getName().equals(name))
				.findFirst()
				.orElse(null);
	}
	
	public String getName() {
		return this.name;
	}
	
	public String getCnName() {
		return this.cnName;
	}

}
