package com.gcloud.mesh.header.msg.alert;

import com.gcloud.mesh.header.msg.BaseMsg;

import lombok.Data;

@Data
public class AccessSmsMsg extends BaseMsg {
	
	private String smsPlatform;
	private String smsPlatformName;
	private String config;
	private boolean enable;
}
