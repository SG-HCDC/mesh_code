package com.gcloud.mesh.header.vo.dcs;

import java.util.List;

import lombok.Data;

@Data
public class StatisticsOverviewVo {
	
	private Long count;  //数据总量
	private List<StatisticTypeVo> statisticType; //数据占比
	private List<StatisticTypeVo> statisticSource; //数据源占比
	private ListCountTrendVo listCountTrend; //数据变化趋势

}
