package com.gcloud.mesh.header.msg.alert;

import com.gcloud.mesh.header.exception.AlertErrorCode;
import com.gcloud.mesh.header.exception.CommonErrorCode;
import com.gcloud.mesh.header.exception.SpecialCode;
import com.gcloud.mesh.header.msg.BaseMsg;
import javax.validation.constraints.NotBlank;

import org.hibernate.validator.constraints.Length;

import lombok.Data;

@Data
public class DeletePolicyLinkmanMsg extends BaseMsg{
	@NotBlank( message = "::策略ID不能为空")
	@Length( min = 1, max = 60, message = "::告警策略ID"+CommonErrorCode.LENGTH60)
	@SpecialCode(message = "::告警策略ID"+CommonErrorCode.SPECIAL)
	private String policyId;
	@NotBlank( message = "::告警联系人ID不能为空")
	@Length( min = 1, max = 60, message = "::告警联系人ID"+CommonErrorCode.LENGTH60)
	@SpecialCode(message = "::告警联系人ID"+CommonErrorCode.SPECIAL)
	private String linkmanId;
}
