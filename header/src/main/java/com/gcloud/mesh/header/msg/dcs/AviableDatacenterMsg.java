package com.gcloud.mesh.header.msg.dcs;

import lombok.Data;

@Data
public class AviableDatacenterMsg {
	private Integer type;
	private String datacenterId;
	private String cloudResourceId;
	private String chosedDatacenterId;
}
