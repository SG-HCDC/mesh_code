package com.gcloud.mesh.header.msg.asset;

import com.gcloud.mesh.header.msg.BaseMsg;

import lombok.Data;

@Data
public class ListNodeMsg extends BaseMsg {

	private String datacenterId;
}
