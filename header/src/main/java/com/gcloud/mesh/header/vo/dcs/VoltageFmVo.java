package com.gcloud.mesh.header.vo.dcs;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

@Data
public class VoltageFmVo {
	private String id;

	@ApiModelProperty(value = "设备名称")
	private String factorName;

	@ApiModelProperty(value = "带外ip")
	private String ipmiIp;

	private String datacenterId;
	private String datacenterName;

	@ApiModelProperty(value = "额定功率")
	private Float ratedPower;

	@ApiModelProperty(value = "功耗")
	private Float powerConsumption;

	@ApiModelProperty(value = "调频策略")
	private String voltageFmStrategy;

	@ApiModelProperty(value = "设备支持的策略")
	private List<String> supportStrategies;

	@ApiModelProperty(value = "控制方式",notes = "控制方式，0为自动，1为手动")
	private Integer controlType;
}
