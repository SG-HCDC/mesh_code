
package com.gcloud.mesh.header.enums;

import com.gcloud.mesh.header.exception.BaseException;

public enum ModelType {

    OPTIMAL_COST(6), // 服务器最优成本
    OPTIMAL_DEVICE(6), // 服务器最优设备
    DATACENTER_COST(7), // 数据中心最优成本
    DATACENTER_DEVICE(7), // 数据中心最优设备
    DATACENTER_POWER(7), // 数据中心最优能耗
    DATACENTER_ELECTRIC(7), // 数据中心最优配电子网
    APP(8),
    AIR(1), // 制冷设备
    UPS(2), // 供电设备
	COST(3); // 迁移代价
	
    private int deviceType;

    private ModelType(int deviceType) {
        this.deviceType = deviceType;
    }

    public static ModelType checkAndGet(String type) throws BaseException {
        if (type != null) {
            for (ModelType t : ModelType.values()) {
                if (t.name().equals(type)) {
                    return t;
                }
            }
        }
        return null;
    }

    public static ModelType get(String type) {
        if (type != null) {
            for (ModelType t : ModelType.values()) {
                if (t.name().equals(type)) {
                    return t;
                }
            }
        }
        return null;
    }

    public int getDeviceType() {
        return deviceType;
    }
    
    public static ModelType[] getSchedulerModelType() {
    	ModelType[] types = new ModelType[] {ModelType.DATACENTER_COST, ModelType.DATACENTER_DEVICE};
    	return types;
    }

}
