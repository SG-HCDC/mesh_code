package com.gcloud.mesh.header.exception;

public class DataCleanErrorCode {

	public static final String ID_NOT_BLANK = "120101";
	public static final String CLEAN_TIME_FORMAT_ERROR = "120102";
	
}
