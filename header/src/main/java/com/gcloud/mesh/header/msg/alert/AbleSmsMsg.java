package com.gcloud.mesh.header.msg.alert;

import javax.validation.constraints.NotBlank;

import org.hibernate.validator.constraints.Length;

import com.gcloud.mesh.header.exception.AlertErrorCode;
import com.gcloud.mesh.header.exception.CommonErrorCode;
import com.gcloud.mesh.header.exception.SpecialCode;
import com.gcloud.mesh.header.msg.BaseMsg;

import lombok.Data;

@Data
public class AbleSmsMsg extends BaseMsg {
	@NotBlank( message = "::邮箱ID不能为空")
	@SpecialCode(message = "::邮箱ID"+CommonErrorCode.SPECIAL)
	@Length( min = 1, max = 60, message = "::邮箱ID"+CommonErrorCode.LENGTH60)
	private String smsId;
	@NotBlank( message = "::邮箱开关设置不能为空")
	@SpecialCode(message = "邮箱开关"+CommonErrorCode.SPECIAL)
	private boolean enable;
}
