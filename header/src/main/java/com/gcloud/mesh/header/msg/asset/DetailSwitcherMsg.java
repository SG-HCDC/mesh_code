package com.gcloud.mesh.header.msg.asset;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;

import org.hibernate.validator.constraints.Length;

import com.gcloud.mesh.header.constant.RegexpPattern;
import com.gcloud.mesh.header.exception.AssetErrorCode;
import com.gcloud.mesh.header.exception.CommonErrorCode;
import com.gcloud.mesh.header.exception.SpecialCode;
import com.gcloud.mesh.header.msg.BaseMsg;

import lombok.Data;

@Data
public class DetailSwitcherMsg extends BaseMsg {
	
	@NotBlank( message = AssetErrorCode.ID_NOT_BLANK)
	@Pattern( regexp = RegexpPattern.STAND_ID, message = AssetErrorCode.ID_FORMAT_ERROR)
	@Length( min = 1, max = 60, message = AssetErrorCode.ID_LENGTH_ERROR)
	@SpecialCode(message = "::设备ID"+CommonErrorCode.SPECIAL)
	private String id;

}
