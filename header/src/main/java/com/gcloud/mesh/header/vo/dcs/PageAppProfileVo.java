package com.gcloud.mesh.header.vo.dcs;

import java.util.Date;

import lombok.Data;

@Data
public class PageAppProfileVo {
	
	private String appId;
	private String appName;
	private String datacenterName;
	private String datacenterId;
	private Date createTime;
	private String sceneSuggest;
	private String sceneChoose;
	private String featureSuggest;
	private String featureChoose;
}
