package com.gcloud.mesh.header.vo.analysis;

import lombok.Data;

@Data
public class AlertLevelVo {

	private String name;
	private String cnName;
	
	public AlertLevelVo() {
		
	}
	
	public AlertLevelVo(String name, String cnName) {
		this.name = name;
		this.cnName = cnName;
	}
}
