package com.gcloud.mesh.header.msg.alert;

import java.util.List;

import com.gcloud.mesh.header.enums.MonitorMeter;
import com.gcloud.mesh.header.exception.CommonErrorCode;
import com.gcloud.mesh.header.exception.SpecialCode;
import com.gcloud.mesh.header.msg.BaseMsg;
import com.gcloud.mesh.header.vo.alert.RuleVo;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import org.apache.commons.lang3.StringUtils;
import org.hibernate.validator.constraints.Length;

import lombok.Data;

@Data
public class CreatePolicyMsg extends BaseMsg {

	@NotBlank(message = "::策略名称不可为空")
	@Length(min = 1, max = 10, message = "::策略名称" + CommonErrorCode.LENGTH10)
	@SpecialCode(message = "::策略名称" + CommonErrorCode.SPECIAL)
	private String policyName;

	@NotNull(message = "::状态不可为空")
	private Boolean enable;

	private Integer enableStartTime;

	private Integer enableEndTime;

	private Integer channelSilence;

	@NotNull(message = "::通知方式不可为空")
	private Boolean notifyEnable;
	private List<RuleVo> ruleVos;
	private List<String> linkmans;
	private String manageType;
	private String platformType;
	private String regionId;
	@Override
	public String toString() {
		StringBuffer buff = new StringBuffer();
		if(StringUtils.isNotBlank(policyName)) {
			buff.append("策略名称：").append(policyName);
		}
//		if(enable != null) {
//			buff.append(",是否开启：").append(enable?"开启":"关闭").append("");
//		}
		buff.append(",策略规则：");
		for(RuleVo vo : ruleVos) {
			buff.append(vo.toString());
			buff.append(",");
		}
		String con = buff.toString();
		if(con.endsWith(",")) {
			con.substring(0,con.length()-1);
		}
		return con;
	}
	
	
}
