
package com.gcloud.mesh.header.vo.dcs;

import lombok.Data;

@Data
public class MockAppVo {

	private String id;
	private String name;
	private String cloudResourceId;
	private String createTime;
	private Integer type;
	private String from;

}
