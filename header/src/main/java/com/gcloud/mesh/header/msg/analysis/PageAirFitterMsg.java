package com.gcloud.mesh.header.msg.analysis;

import javax.validation.constraints.Pattern;

import org.hibernate.validator.constraints.Length;
import org.springframework.lang.Nullable;

import com.gcloud.mesh.header.constant.RegexpPattern;
import com.gcloud.mesh.header.exception.AssetErrorCode;
import com.gcloud.mesh.header.msg.BaseQueryMsg;

import lombok.Data;

@Data
public class PageAirFitterMsg extends BaseQueryMsg {

//	@Nullable
//	@Length( min = 1, max = 60, message = AssetErrorCode.NAME_LENGTH_ERROR)
//	@Pattern( regexp = RegexpPattern.STAND_NAME, message = AssetErrorCode.NAME_FORMAT_ERROR)
	private String name;
	
	private String datacenterId;
}
