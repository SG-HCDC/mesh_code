package com.gcloud.mesh.header.msg.dcs;

import com.gcloud.mesh.header.constant.RegexpPattern;
import com.gcloud.mesh.header.exception.AssetErrorCode;
import com.gcloud.mesh.header.exception.CommonErrorCode;
import com.gcloud.mesh.header.exception.DvfsErrorCode;
import com.gcloud.mesh.header.exception.SpecialCode;
import com.gcloud.mesh.header.msg.BaseQueryMsg;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.Pattern;

@Data
public class PageVoltageFmMsg extends BaseQueryMsg{

	@Length( min = 1, max = 10, message = "::服务器名称"+CommonErrorCode.LENGTH10)
	@SpecialCode(message = "::服务器名称"+CommonErrorCode.SPECIAL)
	private String factorName;

	@Pattern( regexp = RegexpPattern.STAND_ID, message = DvfsErrorCode.ID_FORMAT_ERROR)
	@Length( min = 1, max = 60, message = DvfsErrorCode.ID_LENGTH_ERROR)
	private String datacenterId;
}
