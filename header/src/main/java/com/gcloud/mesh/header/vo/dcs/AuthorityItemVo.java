package com.gcloud.mesh.header.vo.dcs;

import javax.validation.constraints.NotBlank;

import com.gcloud.mesh.header.exception.AuthorityErrorCode;

import lombok.Data;

@Data
public class AuthorityItemVo {
	
	private Boolean enabled;
	
	@NotBlank( message = AuthorityErrorCode.TYPE_NOT_BLANK)
	private String type;

	@Override
	public String toString() {
		String enableStr = "";
		if(enabled != null) {
			if(enabled) {
				enableStr = "开启";
			}else {
				enableStr = "关闭";
			}
		}
		String typeStr = "";
		if(type.equals("air_condition")) {
			typeStr = "空调设备";
		}else if(type.equals("distribution_box")) {
			typeStr = "配电箱设备";
		}else if(type.equals("ups")) {
			typeStr = "ups设备";
		}else if(type.equals("server")) {
			typeStr = "服务器";
		}else if(type.equals("switcher")) {
			typeStr = "交换机";
		}else if(type.equals("k8s")) {
			typeStr = "容器资源";
		}else if(type.equals("vm")) {
			typeStr = "虚拟机资源";
		}else if(type.equals("app")) {
			typeStr = "应用资源";
		}
		
		return "[资源类型：" + typeStr + "， 权限状态：" + enableStr + "]";
	}
	
	
	
}
