package com.gcloud.mesh.header.vo.dcs;

import java.util.Date;

import lombok.Data;

@Data
public class DataClassificationPointVo {
	
	private String label;
	private Date timestamp;
	private Long generalCount;
	private Long vitalCount;

}
