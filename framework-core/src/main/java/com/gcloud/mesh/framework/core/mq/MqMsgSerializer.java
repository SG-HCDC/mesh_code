package com.gcloud.mesh.framework.core.mq;

import java.io.UnsupportedEncodingException;

import com.gcloud.mesh.framework.core.util.JsonUtil;
import com.gcloud.mesh.framework.core.util.MqJsonUtil;
import com.gcloud.mesh.header.exception.BaseException;
import com.gcloud.mesh.header.msg.MqBaseMsg;


public class MqMsgSerializer {
	
    public static byte[] serialize(MqBaseMsg msg) {
        return serialize(msg, true);
    }

    public static byte[] serialize(MqBaseMsg msg, boolean async) {
        return serialize(msg, async, true);
    }

    public static byte[] serialize(MqBaseMsg msg, boolean async, boolean withRoleInfo) {
        try {
            return MqJsonUtil.toJson(msg).getBytes("UTF-8");
        }
        catch (UnsupportedEncodingException e) {
            throw new BaseException("failed to serialize data");
        }
    }

    public static Object deserialize(byte[] data, Class clazz) {
        try {
            return MqJsonUtil.fromJson(new String(data, "UTF-8"), clazz);
        }
        catch (UnsupportedEncodingException e) {
            throw new BaseException("failed to deserialize data");
        }
    }  

}
