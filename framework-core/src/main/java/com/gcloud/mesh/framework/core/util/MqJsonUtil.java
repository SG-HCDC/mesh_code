package com.gcloud.mesh.framework.core.util;

import java.lang.reflect.Type;

import com.google.gson.ExclusionStrategy;
import com.google.gson.FieldAttributes;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class MqJsonUtil {
	
   private static Gson gson = new GsonBuilder().disableHtmlEscaping().setExclusionStrategies(new ExclusionStrategy() {


        public boolean shouldSkipField(FieldAttributes f) {
            return false;
        }


        public boolean shouldSkipClass(Class<?> clazz) {
            return false;
        }


    }).setDateFormat("yyyy-MM-dd'T'HH:mm:ss").create();

    public static String toJson(Object obj) {
        return gson.toJson(obj);
    }

    public static <T> T fromJson(String jsonData, Class<T> clazz) {
        return gson.fromJson(jsonData, (Type)clazz);
    }

}
