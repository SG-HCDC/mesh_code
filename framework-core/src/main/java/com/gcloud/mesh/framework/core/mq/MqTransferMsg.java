package com.gcloud.mesh.framework.core.mq;

import java.io.Serializable;

import com.gcloud.mesh.header.msg.BaseMsg;

import lombok.Data;

@Data
public class MqTransferMsg implements Serializable {

    private static final long serialVersionUID = 1L;

    private String action;
    private boolean async = true;
    private Object data;

    public MqTransferMsg() {

    }

    public MqTransferMsg(String action, boolean async, BaseMsg data) {
        this.action = action;
        this.async = async;
        this.data = data;
    }

}
