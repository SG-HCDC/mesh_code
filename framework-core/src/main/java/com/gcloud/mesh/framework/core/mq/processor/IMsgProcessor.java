package com.gcloud.mesh.framework.core.mq.processor;

import com.gcloud.mesh.header.msg.MqBaseMsg;
import com.gcloud.mesh.header.msg.MqBaseReplyMsg;

public interface IMsgProcessor <T extends MqBaseMsg, R extends MqBaseReplyMsg> {

	R process(T msg);
	
}
