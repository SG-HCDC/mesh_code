
package com.gcloud.mesh.framework.core.web.handle;

import java.io.IOException;
import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.TypeMismatchException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.support.DefaultMessageSourceResolvable;
import org.springframework.ui.Model;
import org.springframework.validation.BindException;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import com.gcloud.mesh.framework.core.SpringUtil;
import com.gcloud.mesh.header.exception.BaseException;
import com.gcloud.mesh.header.exception.CommonErrorCode;
import com.gcloud.mesh.header.msg.BaseReplyMsg;

import lombok.extern.slf4j.Slf4j;

//@RestControllerAdvice(basePackages = "com.gcloud.mesh")

@Slf4j
public class GlobalExceptionHandle {

//    @Autowired
//    private ErrorCodeMapper errorCodeMapper;
//
//    /**
//     * 应用到所有@RequestMapping注解方法，在其执行之前初始化数据绑定器
//     * @param binder
//     */
//    @InitBinder
//    public void initBinder(WebDataBinder binder) {
//
//    }
//
//    /**
//     * 把值绑定到Model中，使全局@RequestMapping可以获取到该值
//     * @param model
//     */
//    @ModelAttribute
//    public void addAttributes(Model model) {
//
//    }
//
//    /**
//     * 全局异常捕捉处理
//     * 
//     */
//    @ExceptionHandler(value = Exception.class)
//    public BaseReplyMsg defaultErrorHandle(HttpServletRequest request, HttpServletResponse response, Exception e, Locale locale) throws IOException {
//        BaseReplyMsg result = new BaseReplyMsg();
//        String errorCode = null;
//        String errorMessage = null;
//
//        if (e instanceof BaseException) {
//            errorCode = ((BaseException)e).getErrorCode();
//            errorMessage = ((BaseException)e).getErrorMsg();
//
//        }
//        else if (e instanceof MethodArgumentNotValidException) {
//            List<FieldError> fieldErrors = ((MethodArgumentNotValidException)e).getBindingResult().getFieldErrors();
//            if (fieldErrors != null && fieldErrors.size() > 0) {
//                errorCode = fieldErrors.get(0).getDefaultMessage();
//            }
//            else {
//                errorCode = CommonErrorCode.UNKNOWN_ERROR;
//            }
//        }
//        else if (e instanceof BindException) {
//        	errorCode = ((BindException)e).getBindingResult()
//        						.getAllErrors()
//        						.stream()
//        						.map(DefaultMessageSourceResolvable::getDefaultMessage)
//        						.collect(Collectors.joining());
//        	if (!StringUtils.isNotBlank(errorCode)) {
//        		errorCode = CommonErrorCode.UNKNOWN_ERROR;
//        	}
//        }
//        else {
//            errorCode = CommonErrorCode.UNKNOWN_ERROR;
//            log.error("unexpected error " + request.getRequestURI() + ": " + e.getMessage(), e);
//        }
//
//        if (errorMessage == null) {
//            errorMessage = this.errorCodeMapper.getErrorMsg(errorCode);
//        }     
//        log.error("request error {}: {} {}", request.getRequestURI(), errorCode, errorMessage);
//
//        result.setSuccess(false);
//        result.setErrorCode(errorCode);
//        result.setData(errorMessage);
//        
////        ISysBaseAPI sysBaseAPI = SpringUtil.getBean(SysBaseApiImpl.class);
//        
//        return result;
//    }

}

