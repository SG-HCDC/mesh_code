package com.gcloud.mesh.framework.core.mq.role;

public class RoleType {
	
    public static final String MESH_CONTROLLER = "mesh_controller";
    public static final String MESH_AGENT = "mesh_agent";
    public static final String MESH_SERVER = "mesh_server";
    
}
