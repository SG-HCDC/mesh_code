
package com.gcloud.mesh.framework.core.init;

public interface IInitor {

    InitPriority initPriority();

    void init() throws Exception;

}
