package com.gcloud.mesh.framework.core.util;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

import org.apache.commons.lang3.StringUtils;

public class DateUtil {

	public static final String DEFAULT_DATE_FORMAT = "yyyy-MM-dd'T'HH:mm:ss'Z'";
	public static final String DEFAULT_FORMAT_DATE_FORMAT = "yyyy-MM-dd HH:mm:ss";

	public static String httpDate(TimeZone serverTimeZone) {
		final String DateFormat = "yyyy-MM-dd'T'HH:mm:00'Z'";
		SimpleDateFormat format = new SimpleDateFormat(DateFormat, Locale.US);
		format.setTimeZone(serverTimeZone);
		return format.format(new Date());
	}

	public static String formatStringDate(Date date) {
		SimpleDateFormat formatter = new SimpleDateFormat(DEFAULT_FORMAT_DATE_FORMAT);
		if (date == null) {
			return null;
		}
		return formatter.format(date);
	}

	public static String formatStringDateTZ(Date date) {
		SimpleDateFormat formatter = new SimpleDateFormat(DEFAULT_DATE_FORMAT);
		if (date == null) {
			return null;
		}
		return formatter.format(date);
	}

	public static Date stringToDate(String str) {
		if (StringUtils.isBlank(str)) {
			return null;
		}
		DateFormat format = null;

		if (str.length() > 20) {
			str = str.substring(0, 19);
		}

		if (str.length() == 17) {
			format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm'Z'");

			Date date = null;
			try {
				date = format.parse(str);
			} catch (ParseException e) {
				e.printStackTrace();
			}
			return date;
		} else if (str.length() == 20) {
			format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
		} else if (str.length() == 19) {
			if (str.contains("T")) {
				format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
			} else {
				format = new SimpleDateFormat("yyyy-MM-dd' 'HH:mm:ss");
			}
		} else {
			format = new SimpleDateFormat("yyyy-MM-dd");
		}

		Date date = null;
		try {
			date = format.parse(str);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return date;
	}

	public static String dateTransformBetweenTimeZone(Date sourceDate, TimeZone sourceTimeZone, TimeZone targetTimeZone) {
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat(DEFAULT_DATE_FORMAT);
		Long targetTime = sourceDate.getTime() - sourceTimeZone.getRawOffset() + targetTimeZone.getRawOffset();
		return getTime(new Date(targetTime), simpleDateFormat);
	}

	public static String getTime(Date date, DateFormat formatter) {
		return formatter.format(date);
	}

	public static String getTime(Date date, String format) {
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat(format);
		return getTime(date, simpleDateFormat);
	}

	public static String dateTransformGmt8(String sourceTime) {
		if (sourceTime == null) {
			return null;
		}
		TimeZone sourceTimeZone = TimeZone.getTimeZone("UTC");
		TimeZone targetTimeZone = TimeZone.getTimeZone("GMT+8:00");
		Date sourceDate = DateUtil.stringToDate(sourceTime);
		return DateUtil.dateTransformBetweenTimeZone(sourceDate, sourceTimeZone, targetTimeZone);
	}

	public static String dateTransformGmt8(Date sourceTime) {
		if (sourceTime == null) {
			return null;
		}
		TimeZone sourceTimeZone = TimeZone.getTimeZone("UTC");
		TimeZone targetTimeZone = TimeZone.getTimeZone("GMT+8:00");
		return DateUtil.dateTransformBetweenTimeZone(sourceTime, sourceTimeZone, targetTimeZone);
	}

	public static String dateTransformUtc(String sourceTime) {
		TimeZone sourceTimeZone = TimeZone.getTimeZone("GMT+8:00");
		TimeZone targetTimeZone = TimeZone.getTimeZone("UTC");
		Date sourceDate = DateUtil.stringToDate(sourceTime);
		return DateUtil.dateTransformBetweenTimeZone(sourceDate, sourceTimeZone, targetTimeZone);
	}

}
