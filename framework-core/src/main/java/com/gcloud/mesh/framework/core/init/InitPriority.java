
package com.gcloud.mesh.framework.core.init;

public enum InitPriority {

    AFTER_ROLE_INIT(1),
    MIDDLE(50),
    LAST(100);

    private int code;

    InitPriority(int code) {
        this.code = code;
    }

    public int getCode() {
        return this.code;
    }

}
