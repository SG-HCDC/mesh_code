package com.gcloud.mesh.framework.core.msg;

import java.util.HashMap;
import java.util.Map;

import com.gcloud.mesh.header.msg.MqBaseMsg;


public class MsgResolver {
	 private static final Map<Class<? extends MqBaseMsg>, MsgConstruct> CONSTRUCTS = new HashMap<>();

	    public static MsgConstruct resolve(Class<? extends MqBaseMsg> msgClass) {
	        
            synchronized (CONSTRUCTS) {
            	MsgConstruct res = CONSTRUCTS.get(msgClass);
                if (res == null) {
                    res = new MsgConstruct();

                    String tmp[] = msgClass.getPackage().getName().split("\\.");
                    res.setModule(tmp[tmp.length - 1]);

                    String msgClassName = msgClass.getSimpleName();
                    res.setAction(msgClassName.substring(0, msgClassName.length() - 3));

                    res.setUrlAction(res.getAction().substring(0, 1).toLowerCase() + res.getAction().substring(1));

                    CONSTRUCTS.put(msgClass, res);
                }
                return res;
            }
	        
	    }

	    public static String getModule(Class<? extends MqBaseMsg> msgClass) {
	        MsgConstruct res = resolve(msgClass);
	        return res.getModule();
	    }

	    public static String getAction(Class<? extends MqBaseMsg> msgClass) {
	        MsgConstruct res = resolve(msgClass);
	        return res.getAction();
	    }

	    public static String getUrlAction(Class<? extends MqBaseMsg> msgClass) {
	        MsgConstruct res = resolve(msgClass);
	        return res.getUrlAction();
	    }
}
