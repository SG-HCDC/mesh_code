package com.gcloud.mesh.framework.core.msg;

import lombok.Data;

@Data
public class MsgConstruct {
	
	private String module;
    private String action;
    private String urlAction;
}
