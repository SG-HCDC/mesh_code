echo 1.更新代码
svn cleanup
svn up

echo 2.打包项目
mvn clean package

echo 3.停止服务
docker-compose stop sgcc-controller
docker-compose stop sgcc-monitor
docker-compose stop sgcc-migrate

echo 4.更新项目工程
rm -rf /opt/sgcc/mesh-controller-1.0.0.jar
cp controller/target/mesh-controller-1.0.0.jar /opt/sgcc/

rm -rf /opt/sgcc/monitor/mesh-monitor-1.0.0.jar
cp monitor/target/mesh-monitor-1.0.0.jar /opt/sgcc/monitor

rm -rf /opt/sgcc/migrate/mesh-migrate-1.0.0.jar
cp migrate/target/mesh-migrate-1.0.0.jar /opt/sgcc/migrate

echo 5.启动服务
docker-compose start sgcc-controller
docker-compose start sgcc-monitor
docker-compose start sgcc-migrate