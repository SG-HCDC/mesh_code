echo 1.更新代码
  svn cleanup
  svn up

echo 2.打包项目
  yarn run build

echo 3.构建镜像
  docker build -t nginx:jeecgboot .

echo 4.清空环境
  docker stop jeecg-boot-nginx
  docker rm jeecg-boot-nginx

echo 5.启动镜像
  docker run --name jeecg-boot-nginx -p 80:80 -d nginx:jeecgboot