import Vue from 'vue'
import router from './router'
import store from './store'
// import notification from 'ant-design-vue/es/notification'
import {
  ACCESS_TOKEN,
  INDEX_MAIN_PAGE_PATH
} from '@/store/mutation-types'
import {
  generateIndexRouter
} from "@/utils/util"

function getUrlParam(paraName) {
  let url = document.location.toString();
  let arrObj = url.split("?");

  if (arrObj.length > 1) {
    let arrPara = arrObj[1].split("&");
    let arr;

    for (let i = 0; i < arrPara.length; i++) {
      arr = arrPara[i].split("=");

      if (arr != null && arr[0] == paraName) {
        if (arr[1].indexOf("#") > -1) {
          return arr[1].substr(0, arr[1].indexOf("#"));
        } else {
          return arr[1];
        }
      }
    }
    return "";
  }
  else {
    return "";
  }
}

const whiteList = ['/user/login'] // no redirect whitelist

router.beforeEach((to, from, next) => {
  if (!(to && to.path && to.path.startsWith("/f0"))) {
    store.commit('SET_LOADING', true)
  }
  if (to.path === '/skip') {
    next()
  }
  else if (Vue.ls.get(ACCESS_TOKEN)) {
    /* has token */
    if (to.path === '/user/login') {
      next({
        path: INDEX_MAIN_PAGE_PATH
      })
    } else {
      if (store.getters.permissionList.length === 0) {
        store.dispatch('GetPermissionList').then(res => {
            const menuData = res.result.menu;
            //console.log(res.message)
            if (menuData === null || menuData === "" || menuData === undefined) {
              return;
            }
            let constRoutes = [];
            constRoutes = generateIndexRouter(menuData);
            // 添加主界面路由
            store.dispatch('UpdateAppRouter', {
              constRoutes
            }).then(() => {
              // 根据roles权限生成可访问的路由表
              // 动态添加可访问路由表
              router.addRoutes(store.getters.addRouters)
              const redirect = decodeURIComponent(from.query.redirect || to.path)
              if (to.path === redirect) {
                // hack方法 确保addRoutes已完成 ,set the replace: true so the navigation will not leave a history record
                next({
                  ...to,
                  replace: true
                })
              } else {
                // 跳转到目的路由
                next({
                  path: redirect
                })
              }
            })
          })
          .catch(() => {
            /* notification.error({
               message: '系统提示',
               description: '请求用户信息失败，请重试！'
             })*/
            store.dispatch('Logout').then(() => {
              // if (process.env.VUE_APP_MODE !== "sso") {
              // }
              next({
                path: '/user/login',
                query: {
                  redirect: to.fullPath
                }
              })
            })
          })
      } else {
        next()
      }
    }
  } else {
    if (process.env.VUE_APP_MODE === "sso") {
      const st = getUrlParam("ticket");
      if (st) {
        let params = {
          ticket: st,
          service: window._CONFIG['serviceURL'],
        };
        store.dispatch('ValidateLogin', params);
      } else {
        next({
          path: '/skip'
        })
      }
    } else {
      if (whiteList.indexOf(to.path) !== -1) {
        // 在免登录白名单，直接进入
        next()
      } else {
        next({
          path: '/user/login',
          query: {
            redirect: to.fullPath
          }
        })
      }
    }
  }
})

router.afterEach(() => {
  store.commit('SET_LOADING', false)
})