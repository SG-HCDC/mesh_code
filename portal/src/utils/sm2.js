export function sm2Encrypt(msg, publicKey) {
    const sm2 = require('sm-crypto').sm2
    return sm2.doEncrypt(msg, publicKey, 0)
}