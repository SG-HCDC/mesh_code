import { USER_AUTH } from "@/store/mutation-types"
import * as api from '@/api/api'
import {
  isURL
} from '@/utils/validate'

/**
 * 触发 window.resize
 */
export function triggerWindowResizeEvent() {
  let event = document.createEvent('HTMLEvents')
  event.initEvent('resize', true, true)
  event.eventType = 'message'
  window.dispatchEvent(event)
}

/**
 * 过滤对象中为空的属性
 * @param obj
 * @returns {*}
 */
export function filterObj(obj) {
  if (!(typeof obj == 'object')) {
    return;
  }

  for (let key in obj) {
    if (obj.hasOwnProperty(key) &&
      (obj[key] == null || obj[key] == undefined || obj[key] === '')) {
      delete obj[key];
    }
  }
  return obj;
}

/**
 * 时间格式化
 * @param value
 * @param fmt
 * @returns {*}
 */
export function formatDate(value, fmt) {
  let regPos = /^\d+(\.\d+)?$/;
  if (regPos.test(value)) {
    //如果是数字
    let getDate = new Date(value);
    let o = {
      'M+': getDate.getMonth() + 1,
      'd+': getDate.getDate(),
      'h+': getDate.getHours(),
      'm+': getDate.getMinutes(),
      's+': getDate.getSeconds(),
      'q+': Math.floor((getDate.getMonth() + 3) / 3),
      'S': getDate.getMilliseconds()
    };
    if (/(y+)/.test(fmt)) {
      fmt = fmt.replace(RegExp.$1, (getDate.getFullYear() + '').substr(4 - RegExp.$1.length))
    }
    for (let k in o) {
      if (new RegExp('(' + k + ')').test(fmt)) {
        fmt = fmt.replace(RegExp.$1, (RegExp.$1.length === 1) ? (o[k]) : (('00' + o[k]).substr(('' + o[k]).length)))
      }
    }
    return fmt;
  } else {
    //TODO
    value = value.trim();
    return value.substr(0, fmt.length);
  }
}

// 生成首页路由
export function generateIndexRouter(data) {
  const children = generateChildRouters(data)
  children.push({
    path: "/dashboard/analysis",
    name: "dashboard-analysis",
    component: resolve => require(['@/views/dashboard/Analysis.vue'], resolve),
    redirect: null,
    hidden: true,
    meta: {
      "title": "首页",
      "icon": "home",
      "keepAlive": false,
      "internalOrExternal": false
    }
  });
  let indexRouter = [{
    path: '/',
    name: 'dashboard',
    //component: () => import('@/components/layouts/BasicLayout'),
    component: resolve => require(['@/components/layouts/TabLayout'], resolve),
    meta: {
      title: '首页'
    },
    redirect: '/dashboard/analysis',
    children: [
      ...children
    ]
  }, {
    "path": "*",
    "redirect": "/404",
    "hidden": true
  }]
  return indexRouter;
}

// 生成嵌套路由（子路由）

function generateChildRouters(data) {
  const routers = [];
  // const routersName = [];
  for (let item of data) {
    // if (routersName.indexOf(item.name) > -1) {
    //   continue;
    // } else {
    //   routersName.push(item.name);
    // }

    let component = "";
    if (item.component && item.component.indexOf("layouts") >= 0) {
      component = "components/" + item.component;
    } else {
      component = "views/" + item.component;
    }

    // eslint-disable-next-line
    let URL = (item.meta.url || '').replace(/{{([^}}]+)?}}/g, (s1, s2) => eval(s2)) // URL支持{{ window.xxx }}占位符变量
    if (isURL(URL)) {
      item.meta.url = URL;
    }

    let componentPath
    if (item.component && item.component.length) {
      componentPath = resolve => require(['@/' + component + '.vue'], resolve)
    } else {
      componentPath = resolve => require(['@/components/layouts/RouteView'], resolve)
      if (!(item.children && item.children.length > 0)) {
        let viewPath = '/'
        for (let k = 1; k < (item.name).length; k += 2) {
          viewPath += 'f' + (item.name).substring(1, k + 2) + '/'
        }
        viewPath += (item.name).toUpperCase()
        componentPath = resolve => require(['@/views' + viewPath + '.vue'], resolve)
      }
    }

    let menu = {
      path: item.path,
      name: item.name,
      redirect: item.redirect,
      component: componentPath,
      hidden: item.hidden,
      //component:()=> import(`@/views/${item.component}.vue`),
      meta: {
        title: item.meta.title,
        icon: item.meta.icon,
        url: item.meta.url,
        permissionList: item.meta.permissionList,
        keepAlive: item.meta.keepAlive,
        /*update_begin author:wuxianquan date:20190908 for:赋值 */
        internalOrExternal: item.meta.internalOrExternal
        /*update_end author:wuxianquan date:20190908 for:赋值 */
      }
    }
    if (item.alwaysShow) {
      menu.alwaysShow = true;
      menu.redirect = menu.path;
    }
    if (item.children && item.children.length > 0) {
      menu.children = [...generateChildRouters(item.children)];
    }
    //--update-begin----author:scott---date:20190320------for:根据后台菜单配置，判断是否路由菜单字段，动态选择是否生成路由（为了支持参数URL菜单）------
    //判断是否生成路由
    if (item.route && item.route === '0') {
      //console.log(' 不生成路由 item.route：  '+item.route);
      //console.log(' 不生成路由 item.path：  '+item.path);
    } else {
      // routers.push(menu);
    }
    routers.push(menu);
    //--update-end----author:scott---date:20190320------for:根据后台菜单配置，判断是否路由菜单字段，动态选择是否生成路由（为了支持参数URL菜单）------
  }

  // if (routersName.indexOf("dashboard-analysis") < 0) {
  //   routers.push({
  //     path: "/dashboard/analysis",
  //     name: "dashboard-analysis",
  //     component: resolve => require(['@/views/dashboard/Analysis.vue'], resolve),
  //     redirect: null,
  //     hidden: true,
  //     meta: {
  //         "title":"首页",
  //         "icon":"home",
  //         "keepAlive":false,
  //         "internalOrExternal":false
  //     }
  //   });
  // }

  return routers
}

/**
 * 深度克隆对象、数组
 * @param obj 被克隆的对象
 * @return 克隆后的对象
 */
export function cloneObject(obj) {
  return JSON.parse(JSON.stringify(obj))
}

/**
 * 下划线转驼峰
 * @param string
 * @returns {*}
 */
export function underLine2CamelCase(string) {
  return string.replace(/_([a-z])/g, function (all, letter) {
    return letter.toUpperCase();
  });
}

/**
 * 判断是否显示办理按钮
 * @param bpmStatus
 * @returns {*}
 */
export function showDealBtn(bpmStatus) {
  if (bpmStatus != "1" && bpmStatus != "3" && bpmStatus != "4") {
    return true;
  }
  return false;
}

/**
 * 增强CSS，可以在页面上输出全局css
 * @param css 要增强的css
 * @param id style标签的id，可以用来清除旧样式
 */
export function cssExpand(css, id) {
  let style = document.createElement('style')
  style.type = "text/css"
  style.innerHTML = `@charset "UTF-8"; ${css}`
  // 清除旧样式
  if (id) {
    let $style = document.getElementById(id)
    if ($style != null) $style.outerHTML = ''
    style.id = id
  }
  // 应用新样式
  document.head.appendChild(style)
}

/**
 * 重复值验证工具方法
 *
 * 使用示例：
 * { validator: (rule, value, callback) => validateDuplicateValue('sys_fill_rule', 'rule_code', value, this.model.id, callback) }
 *
 * @param tableName 被验证的表名
 * @param fieldName 被验证的字段名
 * @param fieldVal 被验证的值
 * @param dataId 数据ID，可空
 * @param callback
 */
export function validateDuplicateValue(tableName, fieldName, fieldVal, dataId, callback) {
  if (fieldVal) {
    let params = {
      tableName,
      fieldName,
      fieldVal,
      dataId
    }
    api.duplicateCheck(params).then(res => {
      res['success'] ? callback() : callback(res['message'])
    }).catch(err => {
      callback(err.message || err)
    })
  } else {
    callback()
  }
}

/**
 * 根据编码校验规则code，校验传入的值是否合法
 *
 * 使用示例：
 * { validator: (rule, value, callback) => validateCheckRule('common', value, callback) }
 *
 * @param ruleCode 编码校验规则 code
 * @param value 被验证的值
 * @param callback
 */
export function validateCheckRule(ruleCode, value, callback) {
  if (ruleCode && value) {
    value = encodeURIComponent(value)
    api.checkRuleByCode({
      ruleCode,
      value
    }).then(res => {
      res['success'] ? callback() : callback(res['message'])
    }).catch(err => {
      callback(err.message || err)
    })
  } else {
    callback()
  }
}

/**
 * 如果值不存在就 push 进数组，反之不处理
 * @param array 要操作的数据
 * @param value 要添加的值
 * @param key 可空，如果比较的是对象，可能存在地址不一样但值实际上是一样的情况，可以传此字段判断对象中唯一的字段，例如 id。不传则直接比较实际值
 * @returns {boolean} 成功 push 返回 true，不处理返回 false
 */
export function pushIfNotExist(array, value, key) {
  for (let item of array) {
    if (key && (item[key] === value[key])) {
      return false
    } else if (item === value) {
      return false
    }
  }
  array.push(value)
  return true
}

/**
 * 可用于判断是否成功
 * @type {symbol}
 */
export const succeedSymbol = Symbol()
/**
 * 可用于判断是否失败
 * @type {symbol}
 */
export const failedSymbol = Symbol()

/**
 * 使 promise 无论如何都会 resolve，除非传入的参数不是一个Promise对象或返回Promise对象的方法
 * 一般用在 Promise.all 中
 *
 * @param promise 可传Promise对象或返回Promise对象的方法
 * @returns {Promise<any>}
 */
export function alwaysResolve(promise) {
  return new Promise((resolve, reject) => {
    let p = promise
    if (typeof promise === 'function') {
      p = promise()
    }
    if (p instanceof Promise) {
      p.then(data => {
        resolve({
          type: succeedSymbol,
          data
        })
      }).catch(error => {
        resolve({
          type: failedSymbol,
          error
        })
      })
    } else {
      reject('alwaysResolve: 传入的参数不是一个Promise对象或返回Promise对象的方法')
    }
  })
}

/**
 * 简单实现防抖方法
 *
 * 防抖(debounce)函数在第一次触发给定的函数时，不立即执行函数，而是给出一个期限值(delay)，比如100ms。
 * 如果100ms内再次执行函数，就重新开始计时，直到计时结束后再真正执行函数。
 * 这样做的好处是如果短时间内大量触发同一事件，只会执行一次函数。
 *
 * @param fn 要防抖的函数
 * @param delay 防抖的毫秒数
 * @returns {Function}
 */
export function simpleDebounce(fn, delay = 100) {
  let timer = null
  return function () {
    let args = arguments
    if (timer) {
      clearTimeout(timer)
    }
    timer = setTimeout(() => {
      fn.apply(null, args)
    }, delay)
  }
}

/**
 * 不用正则的方式替换所有值
 * @param text 被替换的字符串
 * @param checker  替换前的内容
 * @param replacer 替换后的内容
 * @returns {String} 替换后的字符串
 */
export function replaceAll(text, checker, replacer) {
  let lastText = text
  text = text.replace(checker, replacer)
  if (lastText !== text) {
    return replaceAll(text, checker, replacer)
  }
  return text
}

/**
 * 获取事件冒泡路径，兼容 IE11，Edge，Chrome，Firefox，Safari
 * 目前使用的地方：JEditableTable Span模式
 */
export function getEventPath(event) {
  let target = event.target
  let path = (event.composedPath && event.composedPath()) || event.path

  if (path != null) {
    return (path.indexOf(window) < 0) ? path.concat(window) : path
  }

  if (target === window) {
    return [window]
  }

  let getParents = (node, memo) => {
    memo = memo || []
    const parentNode = node.parentNode

    if (!parentNode) {
      return memo
    } else {
      return getParents(parentNode, memo.concat(parentNode))
    }
  }
  return [target].concat(getParents(target), window)
}

/**
 * 根据组件名获取父级
 * @param vm
 * @param name
 * @returns {Vue | null|null|Vue}
 */
export function getVmParentByName(vm, name) {
  let parent = vm.$parent
  if (parent && parent.$options) {
    if (parent.$options.name === name) {
      return parent
    } else {
      let res = getVmParentByName(parent, name)
      if (res) {
        return res
      }
    }
  }
  return null
}

export function getRandomNumber() {
  let str = '';
  let dateStr = (new Date()).getTime().toString();
  str += dateStr.charAt(dateStr.length - 1);
  str += dateStr.charAt(dateStr.length - 2);
  str += dateStr.charAt(dateStr.length - 3);

  return str/1000
}

export function randomNum(Min, Max) {
  const Range = Max - Min;
  let Rand = Math.random();
  let num = Min + Math.round(Rand * Range); //四舍五入
  return num;
}

export function randrange(min, max) {
  let range = max - min;
  if (range <= 0) {
    return 0;
  }
  let requestBytes = Math.ceil(Math.log2(range) / 8);
  if (!requestBytes) { // No randomness required
    return min;
  }
  let maxNum = Math.pow(256, requestBytes);
  let ar = new Uint8Array(requestBytes);

  while (true) {
    window.crypto.getRandomValues(ar);

    let val = 0;
    for (let i = 0; i < requestBytes; i++) {
      val = (val << 8) + ar[i];
    }

    if (val < maxNum - maxNum % range) {
      return min + (val % range);
    }
  }
}

export function filterNoNum(val) {
  if (val) {
    val += '';
    val = val.replace(/[^\d]/g, '');
    if (val) {
      val = parseInt(val);
    }
  }
  return val
}

export function filterToFloat(val) {
  if (val) {
    val += '';
    val = val.replace(/[^\d.]/g, "");  //清除“数字”和“.”以外的字符
    val = val.replace(/\.{2,}/g, "."); //只保留第一个. 清除多余的
    val = val.replace(".", "$#$").replace(/\./g, "").replace("$#$", ".");
    val = val.replace(/^(\-)*(\d+)\.(\d\d).*$/, '$1$2.$3');//只能输入两个小数
    //以上已经过滤，此处控制的是如果没有小数点，首位不能为类似于 01、02的金额
    if (val !== "") {
      let dotIndex = val.indexOf(".");
      if (dotIndex < 0) {
        val = parseInt(val);
      } else {
        val = parseInt(val.substring(0, dotIndex)) + val.substring(dotIndex, val.length);
      }
    }
  }
  val = isNaN(val) ? 0 : val;
  return val
}

export function hasRight(functionCode) {
  let allRights = sessionStorage.getItem(USER_AUTH);
  if (allRights && allRights.indexOf(functionCode) < 0) {
    return false;
  }
  return true;
}

// 常常在项目中遇到特殊的需求
// 在固定width的容器中，要去显示固定长度的文字，但有些名称过长或是中英文，
// 判断代码    （截取需要的长度，过长加省略号）
export function stringSp(str, len) {
  var newLength = 0;
  var newStr = "";
  var chineseRegex = /[^\x00-\xff]/g;
  var singleChar = "";
  var strLength = str.replace(chineseRegex, "**").length;
  for (var i = 0; i < strLength; i++) {
    singleChar = str.charAt(i).toString();
    if (singleChar.match(chineseRegex) != null) {
      newLength += 2;
    }
    else {
      newLength++;
    }
    if (newLength > len) {
      break;
    }
    newStr += singleChar;
  }

  if (strLength > len) {
    newStr += "...";
  }
  return newStr;
}

export function sortAsc(jsonObj) {
  let arr = new Array();
  let num = 0;
  for (let i in jsonObj) {
      arr[num] = i;
      num++;
  }
  let sortArr = arr.sort();
  let sortObj = {};
  for (let i in sortArr) {
      sortObj[sortArr[i]] = jsonObj[sortArr[i]];
  }
  return sortObj;
}

export function isJSON(str) {
  if (typeof str == 'string') {
    try {
      JSON.parse(str);
      return true;
    } catch (e) {
      return false;
    }
  } else {
    return false;
  }
}

export function objFilterBlank(parameter) {
  if (parameter) {
    Object.keys(parameter).forEach((item) => {
      if (parameter[item] === '' || parameter[item] === null || parameter[item] === undefined) {
        delete parameter[item]
      }
    })
  }
  return parameter
}

export function parseQueryString(str){
  let arr = [];
  let length = 0;
  let res = {};
  arr = str.split('&');
  length = arr.length;
  for(let i = 0; i < length; i++){
    res[arr[i].split('=')[0]] = arr[i].split('=')[1];
  }
  return res;
}