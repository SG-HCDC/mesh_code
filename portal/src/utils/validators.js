const validators = {
  datetimeValidator() {
    return function (rule, value, callback) {
      if (value && value.length > 1 && !((value[0]).valueOf() < (value[1]).valueOf())) {
        callback('开始时间应早于结束时间')
      } else {
        callback()
      }
    }
  },
  // 不能包含空格
  noWhitespace() {
    return function (rule, value, callback) {
      /^\S*$/g.test(value) ? callback() : callback('不能包含空格')
    }
  },
  // 数字、字母、中文、下划线
  cnName() {
    return function (rule, value, callback) {
      /^[\u4e00-\u9fa5\w]+$/g.test(value) ? callback() : callback('只能由数字、字母、中文、下划线组成')
    }
  },
  // 数字、大小写字母
  enName() {
    return function (rule, value, callback) {
      /^[a-zA-Z0-9]+$/g.test(value) ? callback() : callback('只能由数字、大小写字母组成')
    }
  },
  /**
   * IP格式校验
   */
  isValidIp() {
    return function (rule, value, callback) {
      let errorMsg = "";
      if (value) {
        const regular = /^(\d{1,2}|1\d\d|2[0-4]\d|25[0-5])\.(\d{1,2}|1\d\d|2[0-4]\d|25[0-5])\.(\d{1,2}|1\d\d|2[0-4]\d|25[0-5])\.(\d{1,2}|1\d\d|2[0-4]\d|25[0-5])$/
        const aa = value.split('.')
        if (!regular.test(value) || aa[0] === '0' && value !== '0.0.0.0') {
          errorMsg = '请填写正确格式的IP,如192.168.1.1';
        }
      }
      if (errorMsg && errorMsg.length) {
        callback(errorMsg)
      } else {
        callback()
      }
    }
  },
  /**
   * 特殊字符校验
   */
  noIllegalString() {
    return function (rule, value, callback) {
      let errorMsg = "";
      if (value) {
        const regular = /^[a-zA-Z0-9\u4e00-\u9fa5]+$/
        value = value.toLowerCase();
        if (!regular.test(value)) {
          errorMsg = '不能含有特殊字符';
        }
        // const IllegalString = "[`~!@#$%^&*()-_+=|{}':;',\\[\\].<>/?~！@#￥……&*（）——|{}【】‘；：”“'。，、？]";
        // for (let i = 0; i < value.length; i++) {
        //   if (IllegalString.indexOf(value.charAt(i)) > -1) {
        //     errorMsg = '不能含有特殊字符';
        //     break;
        //   }
        // }
        const forbiddenArray = [
          'javascript',
          'script',
          'function',
          'jscript',
          'vbscript',
          'onfocus',
          'onblur',
          'alert',
          'location',
          'document',
          'window',
          'window',
          'href',
          'onfocus',
          'confirm',
          'prompt',
          'onerror',
          'eval',
          'url',
          'expr',
          'urlunencoded',
          'referrer',
          'write',
          'writeln',
          'execScript',
          'setInterval',
          'setTimeout',
          'open',
          'navigate',
          'srcdoc',
          'null',
          'underfined',
          'and',
          'or',
          'exec',
          'execute',
          'insert',
          'select',
          'delete',
          'update',
          'alter',
          'create',
          'drop',
          'count',
          'chr',
          'char',
          'asc',
          'mid',
          'substring',
          'master',
          'truncate',
          'declare',
          'xp_cmdshell',
          'restore',
          'backup',
          'netlocalgroup administrators',
          'net user',
          'like',
          'table',
          'from',
          'grant',
          //'use',
          'column_name',
          'group_concat',
          'information_schema.columns',
          'table_schema',
          'union',
          'where',
          'order',
          'by',
          'join',
          'modify',
          'into',
          'substr',
          'ascii',
          'having',
          'iframe',
          'body',
          'form',
          'base',
          'img',
          'src',
          'style',
          'div',
          'object',
          'meta',
          'link',
          'input',
          'comment',
          'br',
        ];
        for (let j = 0; j < forbiddenArray.length; j++) {
          if (value.indexOf(forbiddenArray[j]) > -1) {
            errorMsg = '不能含有' + forbiddenArray[j];
            break;
          }
        }
      }
      if (errorMsg && errorMsg.length) {
        callback(errorMsg)
      } else {
        callback()
      }
    }
  },
  /**
   * 正整数校验
   */
  positiveIntegers() {
    return function (rule, value, callback) {
      /^[1-9][0-9]*$/.test(value) ? callback() : callback('只能输入正整数')
    }
  },
  /**
   * 最大值校验
   */
  xmax({
    max = 1
  }) {
    return function (rule, value, callback) {
      (parseFloat(value) > parseFloat(max)) ? callback('最大值不能大于' + max) : callback()
    }
  },
  /**
   * 最小值校验
   */
  xmin({
    min = 1
  }) {
    return function (rule, value, callback) {
      (parseFloat(value) < parseFloat(min)) ? callback('最小值不能小于' + min) : callback()
    }
  },
  /**
   * 可区分全角字符/半角字符的长度校验。
   * @param min
   * @param max
   * @returns {Function}
   */
  length({
    min = 0,
    max = 100000000
  }) {
    return function (rule, value, callback) {
      //将一个全角字符替换成两个半角字符，以得到真实长度。
      let realLength = value.replace(/[\u0391-\uFFE5]/g, 'aa').length;
      realLength <= max && realLength >= min ? callback() : max < 100000000 ? callback('输入长度应在' + min + '到' + max + '个字符之间') : callback('至少应输入' + min + '个字符');
    }
  }
}
const install = function (Vue, options) {
  Vue.prototype.validators = validators;
}
export default {
  install
}