import Vue from 'vue'
import { SM4_KEY } from "@/store/mutation-types"

export function sm4Encrypt(msg) {
    const sm4 = require('sm-crypto').sm4
    return sm4.encrypt(msg, Vue.ls.get(SM4_KEY))
}

export function sm4Decrypt(encryptData, key) {
    const sm4 = require('sm-crypto').sm4
    if (key) {
        return sm4.decrypt(encryptData, key)
    }
    return sm4.decrypt(encryptData, Vue.ls.get(SM4_KEY))
}

export function randomString(len) {
    len = len || 32;
    const $chars = 'abcdef0123456789';
　　const maxPos = $chars.length;
　　let str = '';
　　for (let i = 0; i < len; i++) {
        str += $chars.charAt(Math.floor(window._CONFIG['getNum']() * maxPos));
　　}
　　return str;
}