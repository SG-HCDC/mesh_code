import api from './index'
import { axios } from '@/utils/request'
import { postAction } from '@/api/manage'

/**
 * login func
 * parameter: {
 *     username: '',
 *     password: '',
 *     remember_me: true,
 *     captcha: '12345'
 * }
 * @param parameter
 * @returns {*}
 */
export function login(parameter) {
  return postAction('/sys/login', parameter)
}

export function phoneLogin(parameter) {
  return postAction('/sys/phoneLogin', parameter)
}

export function getSmsCaptcha(parameter) {
  return postAction(api.SendSms, parameter)
}

export function getInfo() {
  return axios({
    url: '/api/user/info',
    method: 'get',
    headers: {
      'Content-Type': 'application/json;charset=UTF-8'
    }
  })
}

export function logout(logoutToken) {
  return axios({
    url: '/sys/logout',
    method: 'post',
    headers: {
      'Content-Type': 'application/json;charset=UTF-8',
      'X-Access-Token':  logoutToken
    }
  })
}

/**
 * 第三方登录
 * @param token
 * @returns {*}
 */
export function thirdLogin(token) {
  return axios({
    url: `/thirdLogin/getLoginUser/${token}`,
    method: 'get',
    headers: {
      'Content-Type': 'application/json;charset=UTF-8'
    }
  })
}