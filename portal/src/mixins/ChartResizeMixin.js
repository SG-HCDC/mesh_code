export const ChartResizeMixin = {
  data() {
    return {
      chart: undefined
    }
  },
  beforeDestroy() {
    this.release()
  },
  mounted() {
    window.addEventListener('resize', this.resize)
  },
  methods: {
    resize() {
      if (this.chart) {
        this.chart.resize()
      }
    },
    release() {
      window.removeEventListener('resize', this.resize)

      if (this.chart) {
        this.chart.clear()
        this.chart.dispose()
      }
    },
  }
}