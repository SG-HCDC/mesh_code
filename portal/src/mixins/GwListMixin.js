export const GwListMixin = {
  data() {
    return {
      gBaseUrl: '/mesh-controller',
      /* 查询条件 */
      gQueryParam: {},
      /* 数据源 */
      gDataSource: [],
      /* 分页参数 */
      gPagination: {
        current: 1,
        pageSize: 10,
        pageSizeOptions: ['10', '20', '30'],
        showTotal: (total, range) => {
          return range[0] + "-" + range[1] + " 共" + total + "条"
        },
        showQuickJumper: false,
        showSizeChanger: true,
        total: 0
      },
    }
  },
  methods: {
    deleteCallBack() {
      const pageCount = Math.ceil(this.gPagination.total / this.gPagination.pageSize);
      if (this.gPagination.current === pageCount && this.gDataSource.length === 1) {
        this.gPagination.current--;
      }
      if (this.gPagination.current < 1) {
        this.gPagination.current = 1;
      }
      this.gQueryParam.pageNo = this.gPagination.current;
      this.loadData();
    },
    gHandleTableChange(pagination, filters, sorter) {
      //分页、排序、筛选变化时触发
      this.gPagination = pagination;
      this.gQueryParam.pageNo = this.gPagination.current;
      this.gQueryParam.pageSize = this.gPagination.pageSize;
      this.loadData();
    }
  }
}