echo 1.更新代码
svn cleanup
svn up

echo 2.打包项目
cd portal
yarn install
yarn run build
cd ../

echo 3.停止服务
docker-compose stop sgcc-portal

echo 4.更新项目工程
rm -rf /opt/sgcc/dist
cp -r portal/dist /opt/sgcc/

echo 5.启动服务
docker-compose start sgcc-portal