
package com.gcloud.mesh.sdk;

import java.net.URI;
import java.net.URISyntaxException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.openfeign.FeignClientsConfiguration;
import org.springframework.context.annotation.Import;
import org.springframework.stereotype.Component;

import com.gcloud.mesh.header.msg.restvelero.CreateVeleroBackupMsg;
import com.gcloud.mesh.header.vo.restvelero.RestveleroBackupDeleteReply;
import com.gcloud.mesh.header.vo.restvelero.RestveleroBackupDetailVo;
import com.gcloud.mesh.header.vo.restvelero.RestveleroBackupReply;
import com.gcloud.mesh.header.vo.restvelero.RestveleroRestoreDeleteReply;
import com.gcloud.mesh.header.vo.restvelero.RestveleroRestoreDetailVo;
import com.gcloud.mesh.header.vo.restvelero.RestveleroRestoreReply;
import com.gcloud.mesh.sdk.gce.NamespaceVo;
import com.gcloud.mesh.sdk.restvelero.RestveleroFeignClient;

import feign.Feign;
import feign.Target;
import feign.codec.Decoder;
import feign.codec.Encoder;

@Component
@Import(FeignClientsConfiguration.class)
public class RestveleroSDK {

	private RestveleroFeignClient restveleroFeignClient;

	@Autowired
	public RestveleroSDK(Decoder decoder, Encoder encoder) {
		restveleroFeignClient = Feign.builder().encoder(encoder).decoder(decoder)
				.target(Target.EmptyTarget.create(RestveleroFeignClient.class));
	}


	public RestveleroBackupDetailVo backupDetail(String url, String backupName) throws URISyntaxException {
		return restveleroFeignClient.backupDetail(new URI(url), backupName);
	}

	public RestveleroBackupReply backup(String url, String backupName, CreateVeleroBackupMsg msg) throws URISyntaxException {
		return restveleroFeignClient.backup(new URI(url), backupName, msg);
	}

	public RestveleroBackupDeleteReply backupDelete(String url, String backupName) throws URISyntaxException {
		return restveleroFeignClient.backupDelete(new URI(url), backupName);
	}

	
	public RestveleroRestoreReply restore(String url, String restoreName) throws URISyntaxException {
		return restveleroFeignClient.restore(new URI(url), restoreName);
	}

	public RestveleroRestoreDetailVo restoreDetail(String url, String restoreName) throws URISyntaxException {
		return restveleroFeignClient.restoreDetail(new URI(url), restoreName);
	}
	
	public RestveleroRestoreDeleteReply restoreDelete(String url, String restoreName) throws URISyntaxException {
		return restveleroFeignClient.restoreDelete(new URI(url), restoreName);
	}

}
