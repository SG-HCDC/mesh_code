
package com.gcloud.mesh.sdk.gce;

import java.util.List;

import lombok.Data;

@Data
public class GcePageReply<E> {

	private GcePage pagination;
	private List<E> data;

}
