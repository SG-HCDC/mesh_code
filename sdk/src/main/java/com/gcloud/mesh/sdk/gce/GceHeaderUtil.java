
package com.gcloud.mesh.sdk.gce;

import java.util.Collections;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class GceHeaderUtil {

	@Value("${gce.auth.userId:cc81efe719bf4b05a3c21524ccf6c1ea}")
	private String userId;

	@Value("${gce.auth.gceTenantId:f6d4b7a3-db29-476b-aaa2-392fad843442}")
	private String gceTenantId;

	@Value("${gce.auth.tokenId:f6d4b7a3-db29-476b-aaa2-392fad843442}")
	private String tokenId;

	private Map<String, String> headerMap = new HashMap<String, String>();

	@PostConstruct
	public void init() {
		headerMap.put("X-USER-ID", userId);
		headerMap.put("x-gce-tenant-id", gceTenantId);
		headerMap.put("X-TOKENID-IDS", tokenId);
	}

	public void addHeader(String name, String value) {
		headerMap.put(name, value);
	}

	public String getHeader(String name) {
		String headerValue = getHeader(name);
		if (headerMap.containsKey(name)) {
			headerValue = headerMap.get(name);
		}
		return headerValue;
	}

	public Enumeration<String> getHeaderNames() {
		List<String> names = Collections.list(getHeaderNames());
		for (String name : headerMap.keySet()) {
			names.add(name);
		}
		return Collections.enumeration(names);
	}

	public Enumeration<String> getHeaders(String name) {
		List<String> values = Collections.list(getHeaders(name));
		if (headerMap.containsKey(name)) {
			values.add(headerMap.get(name));
		}
		return Collections.enumeration(values);
	}

	public Map<String, String> headersMap() {
		return headerMap;
	}

}
