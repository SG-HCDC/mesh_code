
package com.gcloud.mesh.sdk.gce;

import java.io.Serializable;

import lombok.Data;

@Data
public class NamespaceVo implements Serializable {

	private static final long serialVersionUID = 1L;

	private String id;
	private String name;
	private Integer state;
	private String cnState;
	private String createAt;
	private String providerRefId;

}
