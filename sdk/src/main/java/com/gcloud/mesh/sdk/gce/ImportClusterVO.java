package com.gcloud.mesh.sdk.gce;

import lombok.Data;

@Data
public class ImportClusterVO {
    private String clusterId;
    private String insecureCommand;
}
