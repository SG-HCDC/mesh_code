
package com.gcloud.mesh.sdk.gce;

import lombok.Data;

import java.io.Serializable;

@Data
public class NamespaceDetailVo implements Serializable {

	private static final long serialVersionUID = 1L;
	private String namespaceId;
}
