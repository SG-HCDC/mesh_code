
package com.gcloud.mesh.sdk.gce;

import lombok.Data;

@Data
public class GcePage {

	private int limit;
	private int total;
	private int pageNumber;

}
