package com.gcloud.mesh.sdk.restvelero;

import java.net.URI;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestBody;

import com.gcloud.mesh.header.msg.restvelero.CreateVeleroBackupMsg;
import com.gcloud.mesh.header.vo.restvelero.RestveleroBackupDeleteReply;
import com.gcloud.mesh.header.vo.restvelero.RestveleroBackupDetailVo;
import com.gcloud.mesh.header.vo.restvelero.RestveleroBackupReply;
import com.gcloud.mesh.header.vo.restvelero.RestveleroRestoreDeleteReply;
import com.gcloud.mesh.header.vo.restvelero.RestveleroRestoreDetailVo;
import com.gcloud.mesh.header.vo.restvelero.RestveleroRestoreReply;

import feign.Param;
import feign.RequestLine;

@FeignClient(name = "RestveleroFeignClient")
public interface RestveleroFeignClient {


	@RequestLine("GET /k8s/backup/{backupName}")
	RestveleroBackupDetailVo backupDetail(URI baseUri, @Param("backupName") String backupName);
	
	@RequestLine("DELETE /k8s/backup/{backupName}")
	RestveleroBackupDeleteReply backupDelete(URI baseUri, @Param("backupName") String backupName);

	@RequestLine("POST /k8s/backup/{backupName}")
	RestveleroBackupReply backup(URI baseUri, @Param("backupName") String backupName, @RequestBody CreateVeleroBackupMsg msg);

	@RequestLine("POST /k8s/restore/{restoreName}")
	RestveleroRestoreReply restore(URI baseUri, @Param("restoreName") String backupName);

	@RequestLine("GET /k8s/restore/{restoreName}")
	RestveleroRestoreDetailVo restoreDetail(URI baseUri, @Param("restoreName") String restoreName);

	@RequestLine("DELETE /k8s/restore/{restoreName}")
	RestveleroRestoreDeleteReply restoreDelete(URI baseUri, @Param("restoreName") String restoreName);
}
