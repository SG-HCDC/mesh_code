
package com.gcloud.mesh.sdk.gce;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import feign.RequestInterceptor;
import feign.RequestTemplate;

@Component
public class GceFeignInterceptor implements RequestInterceptor {

	@Autowired
	private GceHeaderUtil header;

	@Override
	public void apply(RequestTemplate template) {
		String url = template.url();
		// 只对特定url进行签名添加Header的操作
		if (url.startsWith("/gce")) {
			template.uri(url.substring(4), false);
			for (Map.Entry<String, String> h : header.headersMap().entrySet()) {
				template.header(h.getKey(), h.getValue());
			}
		}
	}

}
