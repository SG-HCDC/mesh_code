
package com.gcloud.mesh.sdk;

import com.gcloud.mesh.sdk.gce.*;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.Map;

@FeignClient(name="mesh-gce-api")
@RequestMapping(path = "/gce")
public interface GceSDK {

	@RequestMapping(value = "/namespace/page", method = RequestMethod.GET)
	GcePageReply<NamespaceVo> pageNamespace(@RequestParam String clusterId, @RequestParam int pageNumber,
			@RequestParam int limit);

	@RequestMapping(value = "/cluster/page", method = RequestMethod.GET)
	GcePageReply<ClusterVo> pageCluster(@RequestParam int pageNumber, @RequestParam int limit);

	@RequestMapping(value = "/cluster/detail", method = RequestMethod.GET)
	ClusterVo detailCluster(@RequestParam String id);

	@RequestMapping(value = "/pod/page", method = RequestMethod.GET)
	GcePageReply<PodVo> pagePod(@RequestParam String clusterId, @RequestParam String namespaceId,
			@RequestParam int pageNumber, @RequestParam int limit);

	/**
	 * 删除命名空间
	 * @param namespaceId 主键ID
	 * @return
	 */
	@RequestMapping(value = "/namespace/delete", method = RequestMethod.POST)
	GcePageReply<PodVo> delateNamespace(@RequestParam(name = "id") String namespaceId);


	/**
	 * 导入集群
	 * @param name
	 * @param config
	 * @param api
	 * @param token
	 * @return
	 */
	@RequestMapping(value = "/cluster/import", method = RequestMethod.POST)
	ImportClusterVO importCluster(@RequestParam String name ,@RequestParam String config,@RequestParam String api,@RequestParam String token);

	/**
	 * 新增命名空间
	 * @param clusterId
	 * @param name
	 * @return
	 */
	@RequestMapping(value = "/namespace/create", method = RequestMethod.POST)
	NamespaceDetailVo createNamespace(@RequestParam String clusterId ,@RequestParam String name);

	/**
	 * 命名空间的监控指标
	 * @param namespaceId
	 * @return
	 */
	@RequestMapping(value = "/namespace/monitormetrics", method = RequestMethod.GET)
	Map<String,Object> namespaceMonitormetrics(@RequestParam String namespaceId);


	/**
	 * 集群监控指标
	 * @param id 集群ID
	 * @return
	 */
	@RequestMapping(value = "/cluster/monitormetrics", method = RequestMethod.GET)
	Map<String,Object> clusterMonitormetrics(@RequestParam String id);


}
