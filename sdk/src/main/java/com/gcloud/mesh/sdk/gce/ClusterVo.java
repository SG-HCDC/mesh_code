
package com.gcloud.mesh.sdk.gce;

import java.io.Serializable;

import lombok.Data;

@Data
public class ClusterVo implements Serializable {

	private static final long serialVersionUID = 1L;

	private String id;
	private String name;
	
	private ClusterCapacity cluster;
}
