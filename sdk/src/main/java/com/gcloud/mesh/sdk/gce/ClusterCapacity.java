package com.gcloud.mesh.sdk.gce;

import java.util.Map;

import lombok.Data;

@Data
public class ClusterCapacity {
	private Map<String, String> capacity;
	private Map<String, String> requested;
	private String id;
	private String name;
	private String insecureCommand;
}
