package com.gcloud.mesh.sdk.gce;

import java.io.Serializable;

import lombok.Data;

@Data
public class PodVo implements Serializable{
	
	private static final long serialVersionUID = 1L;

	private String uuid;
	private String name;
	private String namespaceId;
}
