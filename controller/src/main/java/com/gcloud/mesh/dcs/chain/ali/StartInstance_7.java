package com.gcloud.mesh.dcs.chain.ali;

import com.alibaba.fastjson.JSONObject;
import com.aliyun.ecs20140526.models.DescribeInstancesRequest;
import com.aliyun.ecs20140526.models.DescribeInstancesResponseBody;
import com.aliyun.ecs20140526.models.StartInstanceRequest;
import com.gcloud.mesh.dcs.chain.FilterChain;
import com.gcloud.mesh.dcs.chain.IMigrationStepChain;
import com.gcloud.mesh.dcs.dao.SchedulerStepDao;
import lombok.extern.slf4j.Slf4j;
import org.jeecg.common.exception.MyBusinessException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

//@Component
@Slf4j
public class StartInstance_7 extends AbstractAliMigrationStep {

    @Autowired
    private SchedulerStepDao schedulerStepDao;

    @Override
    public void doExec(String schedulerJobId, String attach, FilterChain filterChain, IMigrationStepChain stepChain) {
        JSONObject jsonObject = JSONObject.parseObject(attach);
        String regionId = jsonObject.getString("regionId");
        String accessKeyId = jsonObject.getString("accessKeyId");
        String accessSecret = jsonObject.getString("accessSecret");
        String dstInstanceId = jsonObject.getString("dstInstanceId");
        this.startInstance(accessKeyId,accessSecret,regionId,dstInstanceId);
        filterChain.doExec(schedulerJobId,jsonObject.toString(),filterChain,stepChain);
    }


    @Override
    public String chainName(String attach) {
        return "启动实例";
    }

    @Override
    public int getOrder() {
        return 7;
    }


    public String startInstance(String accessKeyId, String accessKeySecret, String regionId,String InstanceId){
        try {
            com.aliyun.ecs20140526.Client client =  createECSClient(accessKeyId,accessKeySecret,regionId);
            StartInstanceRequest startInstanceRequest = new StartInstanceRequest()
                    .setInstanceId(InstanceId);
            return client.startInstance(startInstanceRequest).getBody().getRequestId();
        } catch (Exception e) {
            log.error("启动实例失败",e);
            throw new MyBusinessException("启动实例失败"+"::"+e.getMessage());
        }

    }

    @Override
    public boolean checkStatus(String stepId) {
        JSONObject jsonObject = JSONObject.parseObject(schedulerStepDao.getById(stepId).getAttach());
        String regionId = jsonObject.getString("regionId");
        String accessKeyId = jsonObject.getString("accessKeyId");
        String accessSecret = jsonObject.getString("accessSecret");
        String dstInstanceId = jsonObject.getString("dstInstanceId");
        return isInstanceRunning(accessKeyId,accessSecret,regionId,dstInstanceId);
    }

    @Override
    public boolean isSyncChain() {
        return false;
    }

    public boolean isInstanceRunning(String accessKeyId, String accessKeySecret, String regionId, String instanceId){
        try {
            com.aliyun.ecs20140526.Client ecsClient = createECSClient(accessKeyId, accessKeySecret, regionId);
            DescribeInstancesRequest describeInstancesRequest = new DescribeInstancesRequest()
                    .setRegionId(regionId)
                    .setInstanceIds("[\""+instanceId+"\"]");
            DescribeInstancesResponseBody.DescribeInstancesResponseBodyInstancesInstance ii = ecsClient.describeInstances(describeInstancesRequest).getBody().getInstances().getInstance().get(0);
            return ii==null?false:"Running".equalsIgnoreCase(ii.getStatus());
        } catch (Exception e) {
            return false;
        }
    }
}
