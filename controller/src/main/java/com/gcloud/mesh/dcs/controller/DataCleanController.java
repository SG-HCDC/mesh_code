package com.gcloud.mesh.dcs.controller;

import com.gcloud.mesh.dcs.annotation.ParamSecurity;
import com.gcloud.mesh.dcs.service.DataCleanService;
import com.gcloud.mesh.header.msg.dcs.UpdateDataCleanConfigMsg;
import com.gcloud.mesh.header.vo.dcs.DataCleanConfigVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.jeecg.common.aspect.annotation.AutoLog;
import org.jeecg.common.constant.CommonConstant;
import org.jeecg.common.constant.enums.SysLogOperateType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.jeecg.common.system.api.ISysBaseAPI;

import javax.validation.Valid;

@Api(tags = "数据清洗")
@RestController
@RequestMapping("/dataClean")
public class DataCleanController {

	@Autowired
	private DataCleanService service;

	@Autowired
	private ISysBaseAPI sysBaseAPI;
	
	//处理Mass Assignment: Insecure Binder Configuration
	@InitBinder
	public void populateCustomerRequest(WebDataBinder binder) {
	    binder.setDisallowedFields(new String[]{});
	}

    @AutoLog(value = "查看数据清洗策略",operateType = CommonConstant.OPERATE_TYPE_DETAIL, module = "精确管控与供电制冷联动-精确能耗管控-冗余数据清洗")
	@ApiOperation(value = "展示数据清洗策略", notes = "展示数据清洗策略")
	@RequestMapping(value = "/get1", method = RequestMethod.POST)
	@RequiresPermissions(value = {"dataCleanGet"})
    @ParamSecurity(enabled = true)
	public DataCleanConfigVo get() throws Exception {
		return service.get();
	}

//    @AutoLog(value = "修改数据清洗策略",operateType = CommonConstant.OPERATE_TYPE_UPDATE)
	@ApiOperation(value = "修改数据清洗策略", notes = "修改数据清洗策略")
	@RequestMapping(value = "/update1", method = RequestMethod.POST)
	@RequiresPermissions(value = {"dataCleanUpdate"})
//	@ParamSecurity(enabled = true)
	public void update(@Valid UpdateDataCleanConfigMsg msg) throws Exception {
		service.update(msg);
    	StringBuffer buff = new StringBuffer();
    	buff.append("修改冗余数据清洗策略，");
    	buff.append(msg.toString());
		sysBaseAPI.addLog(buff.toString(), null, SysLogOperateType.UPDATE, null, null, "精确管控与供电制冷联动-精确能耗管控-冗余数据清洗");
	}
}
