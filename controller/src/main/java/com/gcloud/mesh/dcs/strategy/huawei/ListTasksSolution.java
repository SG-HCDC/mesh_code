package com.gcloud.mesh.dcs.strategy.huawei;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;

import com.huaweicloud.sdk.core.auth.GlobalCredentials;
import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.exception.RequestTimeoutException;
import com.huaweicloud.sdk.core.exception.ServiceResponseException;
import com.huaweicloud.sdk.core.http.HttpConfig;
import com.huaweicloud.sdk.sms.v3.SmsClient;
import com.huaweicloud.sdk.sms.v3.model.ListTasksRequest;
import com.huaweicloud.sdk.sms.v3.model.ListTasksResponse;
import com.huaweicloud.sdk.sms.v3.region.SmsRegion;
import com.sun.mail.iap.ConnectionException;

import lombok.extern.slf4j.Slf4j;
@Slf4j
@Component
public class ListTasksSolution {
	public static void main(String[] args) throws ConnectionException {
		ListTasksSolution listTask = new ListTasksSolution();
		listTask.listTasks(null, null);
    }
	public ListTasksResponse listTasks(String ak, String sk) {
		ListTasksResponse response = null;
    	if(StringUtils.isBlank(ak)) {
      		 ak = "8QVISVT4XJHUNMIEPZBQ";
   	   	}
   	   	if(StringUtils.isBlank(sk)) {
   	   		sk = "GingsSlHInSkQ3x47jMSEjFT0uumSJgplZs926Cw";
   	   	}
  	    HttpConfig config = HttpConfig.getDefaultHttpConfig();
  	    config.withIgnoreSSLVerification(true);
        ICredential auth = new GlobalCredentials()
                .withAk(ak)
                .withSk(sk);

        SmsClient client = SmsClient.newBuilder()
                .withCredential(auth)
                .withHttpConfig(config)
                .withRegion(SmsRegion.valueOf("ap-southeast-1"))
                .build();
        ListTasksRequest request = new ListTasksRequest();
        try {
            response = client.listTasks(request);
            System.out.println(response.toString());
        } catch (RequestTimeoutException e) {
        	log.error("迁移获取任务列表超时："+ e);
            // e.printStackTrace();
        } catch (ServiceResponseException e) {
        	log.error("迁移获取任务列表s失败："+ e);
//            e.printStackTrace();
//            System.out.println(e.getHttpStatusCode());
//            System.out.println(e.getErrorCode());
//            System.out.println(e.getErrorMsg());
        }
        return response;
	}
}
