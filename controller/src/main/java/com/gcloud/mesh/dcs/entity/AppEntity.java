
package com.gcloud.mesh.dcs.entity;

import java.util.Date;

import com.gcloud.framework.db.jdbc.annotation.ID;
import com.gcloud.framework.db.jdbc.annotation.Table;

import lombok.Data;
import lombok.experimental.Accessors;

@Table(name = "dcs_apps")
@Data
@Accessors(chain = true)
public class AppEntity {

	@ID
	private String id;
	private String name;
	private String cloudResourceId;
	private Date createTime;
	private Integer type;
	private String from;
	private String nodeExpand;
	private Boolean deleted;

}
