
package com.gcloud.mesh.dcs.dao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.gcloud.framework.db.dao.impl.JdbcBaseDaoImpl;
import com.gcloud.mesh.dcs.entity.SchedulerConfigEntity;
import com.gcloud.mesh.dcs.enums.SchedulerModelDict;

@Repository
public class SchedulerConfigDao extends JdbcBaseDaoImpl<SchedulerConfigEntity, String> {
	
	public Map<String, Double> getDatacenterAppScore(){
		// 查找所有监控策略 dcs_scheduler_configs
		Map<String, Double> datacenterScore = new HashMap<>();
		List<SchedulerConfigEntity> configs = findByProperty("thresholdKey",SchedulerModelDict.APP.name());
		if(configs != null) {
			for(SchedulerConfigEntity config : configs) {
				datacenterScore.put(config.getDatacenterId(), config.getThresholdValue());
			}
		}
		return datacenterScore;
	}  
	
}
