
package com.gcloud.mesh.dcs.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.gcloud.framework.db.dao.impl.JdbcBaseDaoImpl;
import com.gcloud.mesh.dcs.entity.AuthorityEntity;

@Repository
public class AuthorityDao extends JdbcBaseDaoImpl<AuthorityEntity, String> {
	
    public <E> List<E> listByClassification(String classification, Class<E> clazz) {
        StringBuilder sb = new StringBuilder();

        sb.append(" SELECT * FROM dcs_authorities ");
        sb.append(" WHERE 1=1");
        if(classification != null) {
        	sb.append(" AND classification = '" + classification + "'");
        }
        
        return this.findBySql(sb.toString(), clazz);
    }

}
