package com.gcloud.mesh.dcs.strategy;

public interface IReleaseNode {
	void releaseNode(String nodeId);
}
