package com.gcloud.mesh.dcs.chain.k8s;

import com.alibaba.fastjson.JSONObject;
import com.gcloud.mesh.dcs.chain.FilterChain;
import com.gcloud.mesh.dcs.chain.IMigrationStepChain;
import com.gcloud.mesh.dcs.dao.SchedulerStepDao;
import com.gcloud.mesh.dcs.entity.SchedulerStepEntity;
import com.gcloud.mesh.dcs.enums.RestveleroBackupStatus;
import com.gcloud.mesh.header.exception.BaseException;
import com.gcloud.mesh.header.exception.SchedulerErrorCode;
import com.gcloud.mesh.header.vo.restvelero.RestveleroRestoreDetailVo;
import com.gcloud.mesh.header.vo.restvelero.RestveleroRestoreReply;
import com.gcloud.mesh.sdk.RestveleroSDK;
import com.gcloud.mesh.supplier.enums.SystemType;
import lombok.extern.slf4j.Slf4j;
import org.jeecg.common.exception.MyBusinessException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.UUID;

@Component
@Slf4j
public class RestoreNamespace implements IMigrationStepChain {

    @Autowired
    private RestveleroSDK restveleroSDK;

    @Autowired
    private SchedulerStepDao schedulerStepDao;

    @Override
    public void doExec(String schedulerJobId, String attach, FilterChain filterChain, IMigrationStepChain stepChain) {
        JSONObject jsonObject = JSONObject.parseObject(attach);
        String destUrl = jsonObject.getString("dstUrl");
        String backupName = jsonObject.getString("backupName");
        try {
            RestveleroRestoreReply resRestore = restveleroSDK.restore(destUrl, backupName);
            if (resRestore.getStatus() != 200) {
                throw new BaseException(SchedulerErrorCode.RESTORE_OPERATE_FAILED);
            }
            jsonObject.put("restoreName",resRestore.getMessage().getRestore_name());

            schedulerStepDao.updateAttachBySchedulerJobId(schedulerJobId,jsonObject.toString());
        }catch (Exception e){
            log.error("Exception",e);
            throw new MyBusinessException("执行恢复镜像失败，请联系管理员"+"::"+e.getMessage());
        }
    }

    @Override
    public boolean checkStatus(String stepId) {
        String uid = UUID.randomUUID().toString();
        try {
            SchedulerStepEntity stepEntity = schedulerStepDao.getById(stepId);
            if(stepEntity!=null){
                JSONObject jsonObject = JSONObject.parseObject(stepEntity.getAttach());
                String destUrl = jsonObject.getString("dstUrl");
                String restoreName = jsonObject.getString("restoreName");
                RestveleroRestoreDetailVo restore = restveleroSDK.restoreDetail(destUrl, restoreName);
                log.info("[schedule] 数据中心调度-恢复状态,uid:{},id:{}, status:{}", uid,restoreName, restore.getData().getStatus().getPhase());
                if (restore != null && HttpStatus.OK.value() == restore.getStatus() && RestveleroBackupStatus.COMPLETED.getValue().equals(restore.getData().getStatus().getPhase())) {
                   return true;
                }
            }
        }catch(Exception e){
            log.error("========>"+uid+"检查恢复镜像状态失败",e);
        }
        return false;
    }

    @Override
    public String chainType() {
        return SystemType.K8S.getName();
    }

    @Override
    public String chainName(String attach) {
        JSONObject jsonObject = JSONObject.parseObject(attach);
        return String.format("%s恢复镜像",jsonObject.getString("dstDatacenterName"));
    }

    @Override
    public boolean isSyncChain() {
        return false;
    }

    @Override
    public String getExecResult() {
        return null;
    }

    @Override
    public int getOrder() {
        return 2;
    }
}
