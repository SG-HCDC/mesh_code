package com.gcloud.mesh.dcs.controller;

import java.util.List;

import javax.validation.Valid;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.jeecg.common.aspect.annotation.AutoLog;
import org.jeecg.common.constant.CommonConstant;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.gcloud.mesh.dcs.annotation.ParamSecurity;
import com.gcloud.mesh.dcs.service.DataClassificationService;
import com.gcloud.mesh.header.msg.BaseReplyMsg;
import com.gcloud.mesh.header.msg.dcs.ListCountTrendMsg;
import com.gcloud.mesh.header.msg.dcs.StatisticSourceMsg;
import com.gcloud.mesh.header.msg.dcs.StatisticsOverviewMsg;
import com.gcloud.mesh.header.vo.dcs.ListCountTrendVo;
import com.gcloud.mesh.header.vo.dcs.StatisticTypeVo;
import com.gcloud.mesh.header.vo.dcs.StatisticsOverviewVo;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@Api(tags = "数据归类")
@RestController
@RequestMapping("/dataClassification")
public class DataClassificationController {

	@Autowired
	private DataClassificationService service;
	
	//处理Mass Assignment: Insecure Binder Configuration
	@InitBinder
	public void populateCustomerRequest(WebDataBinder binder) {
	    binder.setDisallowedFields(new String[]{});
	}

//	@ApiOperation(value = "数据归类 - 总量", notes = "数据归类 - 总量")
//	@RequestMapping(value = "/count", method = RequestMethod.POST)
//	@RequiresPermissions(value = {"dataClassificationCount"})
//	@AutoLog(value = "数据归类 - 总量", logType = CommonConstant.LOG_TYPE_OPERATE, operateType = CommonConstant.OPERATE_TYPE_DETAIL)
//	public BaseReplyMsg count() throws Exception {
//		BaseReplyMsg reply = new BaseReplyMsg();
//		Long count = service.count();
//		reply.setData(count);
//		return reply;
//	}

//	@ApiOperation(value = "数据归类 - 数据占比", notes = "数据归类 - 数据占比")
//	@RequestMapping(value = "/statisticType", method = RequestMethod.POST)
//	@RequiresPermissions(value = {"dataClassificationStatisticType"})
//	@AutoLog(value = "数据归类 - 数据占比", logType = CommonConstant.LOG_TYPE_OPERATE, operateType = CommonConstant.OPERATE_TYPE_DETAIL)
//	public BaseReplyMsg statisticType() throws Exception {
//		BaseReplyMsg reply = new BaseReplyMsg();
//		List<StatisticTypeVo> result = service.statisticType();
//		reply.setData(result);	
//		return reply;
//	}

//	@ApiOperation(value = "数据归类 - 数据源占比", notes = "数据归类 - 数据源占比")
//	@RequestMapping(value = "/statisticSource", method = RequestMethod.POST)
//	@RequiresPermissions(value = {"dataClassificationStatisticSource"})
//	@AutoLog(value = "数据归类 - 数据源占比", logType = CommonConstant.LOG_TYPE_OPERATE, operateType = CommonConstant.OPERATE_TYPE_DETAIL)
//	public BaseReplyMsg statisticSource(@Valid StatisticSourceMsg msg) throws Exception {
//		BaseReplyMsg reply = new BaseReplyMsg();
//		List<StatisticTypeVo> result = service.statisticSource(msg);
//		reply.setData(result);
//		return reply;
//	}
	
	@ApiOperation(value = "查看数据归类 - 数据源占比", notes = "数据归类 - 数据源占比")
	@RequestMapping(value = "/statisticSource", method = RequestMethod.POST)
	@RequiresPermissions(value = {"dataClassificationStatisticSource"})
	@AutoLog(value = "查看数据归类 - 数据源占比", logType = CommonConstant.LOG_TYPE_OPERATE, operateType = CommonConstant.OPERATE_TYPE_DETAIL)
	@ParamSecurity(enabled = true)
	public BaseReplyMsg statisticSource(@Valid StatisticsOverviewMsg msg) throws Exception {
		BaseReplyMsg reply = new BaseReplyMsg();
		StatisticsOverviewVo result = service.statisticSource(msg);
		reply.setData(result);
		return reply;
	}

	@ApiOperation(value = "数据归类 - 趋势", notes = "数据归类 - 趋势")
	@RequestMapping(value = "/listCountTrend", method = RequestMethod.POST)
	@RequiresPermissions(value = {"dataClassificationListCountTrend"})
	@AutoLog(value = "查看数据归类 - 趋势", logType = CommonConstant.LOG_TYPE_OPERATE, operateType = CommonConstant.OPERATE_TYPE_DETAIL)
	@ParamSecurity(enabled = true)
	public BaseReplyMsg listCountTrend(@Valid ListCountTrendMsg msg) throws Exception {
		BaseReplyMsg reply = new BaseReplyMsg();
		ListCountTrendVo result = service.listCountTrend(msg);
		reply.setData(result);
		return reply;
	}

}
