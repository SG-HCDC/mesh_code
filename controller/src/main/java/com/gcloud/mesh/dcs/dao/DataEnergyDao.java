
package com.gcloud.mesh.dcs.dao;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Repository;

import com.gcloud.framework.db.dao.impl.JdbcBaseDaoImpl;
import com.gcloud.mesh.dcs.entity.DataEnergyEntity;

@Repository
public class DataEnergyDao extends JdbcBaseDaoImpl<DataEnergyEntity, String> {
	
	
	public <E> List<E> listByDay(String datacenterId, String year, String month, Class<E> clazz) {
		
		StringBuffer sql = new StringBuffer();
		List<Object> values=new ArrayList<>();
//		sql.append("select * from (select date_format(timestamp, '%Y-%m-%d') label, sum(value) value, sum(count) count, datacenter_id from `dcs_energy_data` d");
		sql.append("select * from (select date_format(timestamp, '%Y-%m-%d') label, sum(value) value, sum(count) count from `dcs_energy_data` d");
		if(datacenterId != null) {
			sql.append(" where datacenter_id = ?");
			values.add(datacenterId);
		}
		sql.append(" group by label ) cte ");
		sql.append(" where 1 = 1 ");
		if(year != null && month != null) {
			sql.append(" and cte.label like concat(?, '-', ?, '%')");
			values.add(year);
			values.add(month);
		}     
     
        return this.findBySql(sql.toString(), values, clazz);
	}
	
	public <E> List<E> listByMonth(String datacenterId, String year, Class<E> clazz) {
		
		StringBuffer sql = new StringBuffer();
		List<Object> values=new ArrayList<>();
//		sql.append("select * from (select date_format(timestamp, '%Y-%m') label, sum(value) value, sum(count) count, datacenter_id from `dcs_energy_data` d");
		sql.append("select * from (select date_format(timestamp, '%Y-%m') label, sum(value) value, sum(count) count from `dcs_energy_data` d");
		if(datacenterId != null) {
			sql.append(" where datacenter_id = ?");
			values.add(datacenterId);
		}
		sql.append(" group by label ) cte ");
		sql.append(" where 1 = 1 ");
		if(year != null) {
			sql.append(" and cte.label like concat(?, '%')");
			values.add(year);
		}     
     
        return this.findBySql(sql.toString(), values, clazz);
	}
	
	public <E> List<E> listByYear(String datacenterId, String year, Class<E> clazz) {
		
		StringBuffer sql = new StringBuffer();
		sql.append("select * from (select date_format(timestamp, '%Y') label, sum(value) value, sum(count) count, datacenter_id from `dcs_energy_data` d");
		List<Object> values=new ArrayList<>();
		if(datacenterId != null) {
			sql.append(" where datacenter_id = '%s'");
			values.add(datacenterId);
		}
		sql.append(" group by label ) cte ");
   
        return this.findBySql(sql.toString(), values, clazz);
	}
	
}
