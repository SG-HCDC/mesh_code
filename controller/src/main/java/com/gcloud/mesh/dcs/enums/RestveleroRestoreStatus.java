package com.gcloud.mesh.dcs.enums;

public enum RestveleroRestoreStatus {

	READY("ready", "准备"), PROGRESS("InProgress", "进行中"), PARTIALLY_FAILED("PartiallyFailed", "失败"), COMPLETED("Completed", "成功");

	private String name;
	private String value;

	RestveleroRestoreStatus(String value, String name) {
		this.name = name;
		this.value = value;
	}

	public String getName() {
		return name;
	}

	public String getValue() {
		return value;
	}

}
