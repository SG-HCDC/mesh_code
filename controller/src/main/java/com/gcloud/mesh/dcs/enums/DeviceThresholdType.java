package com.gcloud.mesh.dcs.enums;

import java.util.stream.Stream;

public enum DeviceThresholdType {

	DISTRIBUTION_BOX_RATED_POWER(0, "DistributionBoxRatedPower", "配电箱额定功率"),
	SERVER_POWER(1, "ServerPower", "服务器实时功率"),
	CLUSTER_CPU_CORES(2, "ClusterCpuCores", "集群可用cpu"),
	CLUSTER_MEM_SPACE(3, "ClusterMemSpace", "集群可用内存"),

	ALI_CPU_CORES(4, "AliCpuCores", "阿里云cpu使用率"),
	ALI_MEM_SPACE(5, "AliMemSpace", "阿里云内存使用率"),

	HUAWEI_CPU_CORES(6, "HuaweiCpuCores", "华为云cpu使用率"),
	HUAWEI_MEM_SPACE(7, "HuaweiMemSpace", "华为云内存使用率");

	private int no;
	private String name;
	private String cnName;
	
	DeviceThresholdType(int no, String name, String cnName) {
		this.no = no;
		this.name = name;
		this.cnName = cnName;
	}
	
	public static DeviceThresholdType getDeviceThresholdTypeByNo(int no) {
		return Stream.of(DeviceThresholdType.values())
				.filter( s -> s.getNo() == no)
				.findFirst()
				.orElse(null);
	}
	
	public static DeviceThresholdType getDeviceThresholdTypeByName(String name) {
		return Stream.of(DeviceThresholdType.values())
				.filter( s -> s.getName().equals(name))
				.findFirst()
				.orElse(null);
	}
	
	public int getNo() {
		return no;
	}
	
	public String getName() {
		return name;
	}
	
	public String getCnName() {
		return cnName;
	}
	
}
