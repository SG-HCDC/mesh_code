package com.gcloud.mesh.dcs.service;

import com.gcloud.mesh.framework.core.SpringUtil;
import com.gcloud.mesh.sdk.GceSDK;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.gcloud.framework.db.PageResult;
import com.gcloud.mesh.asset.service.IAssetService;
import com.gcloud.mesh.dcs.dao.AppDao;
import com.gcloud.mesh.dcs.enums.DeviceThresholdType;
import com.gcloud.mesh.header.enums.MonitorMeter;
import com.gcloud.mesh.header.vo.asset.DatacenterItemVo;
import com.gcloud.mesh.header.vo.asset.NodeItemVo;
import com.gcloud.mesh.monitor.service.StatisticsCheatService;
import com.gcloud.mesh.sdk.gce.ClusterVo;
import com.gcloud.mesh.supplier.dao.SupplierDao;
import com.gcloud.mesh.supplier.enums.K8sClusterType;
import com.gcloud.mesh.supplier.enums.SystemType;

import lombok.extern.slf4j.Slf4j;

import java.math.BigDecimal;
import java.math.RoundingMode;

@Component
@Slf4j
public class ClusterCpuCoresThreholdService implements IDeviceThresholdService {

	@Autowired
	private AppDao appDao;

	@Autowired
	private SupplierDao supplierDao;

	@Autowired
	private AppService appService;

	@Autowired
	IAssetService assetService;

	@Autowired
	StatisticsCheatService statisticsCheatService;

	@Override
	public DeviceThresholdType device() {
		return DeviceThresholdType.CLUSTER_CPU_CORES;
	}

	@Override
	public Boolean checkScheduler(String datacenterId,String operation, Double value) {
		String clusterId = supplierDao.getConfigValueByDatacenterIdAndConfigName(datacenterId, SystemType.K8S.getName(), K8sClusterType.CLUSTER_ID.getName());
		ClusterVo clusterVo = null;
		if (clusterId != null) {
			GceSDK sdk = SpringUtil.getBean(GceSDK.class);
			clusterVo = sdk.detailCluster(clusterId);
		}
		if (clusterVo != null && clusterVo.getCluster() != null) {
			String voCpuCores = clusterVo.getCluster().getCapacity().get("cpu");
			String voRestCpuCores = clusterVo.getCluster().getRequested().get("cpu");
			if (voCpuCores.endsWith("m") || voRestCpuCores.endsWith("m")) {
				voCpuCores = voCpuCores.replace("m", "");
				voRestCpuCores = voRestCpuCores.replace("m", "");
			}

			voRestCpuCores = new BigDecimal(voRestCpuCores).divide(new BigDecimal("1000")).toString();

			if(!"0".equalsIgnoreCase(voCpuCores)){
				voRestCpuCores = new BigDecimal(voRestCpuCores).setScale(2,RoundingMode.HALF_UP).divide(new BigDecimal(voCpuCores)).setScale(2, RoundingMode.HALF_UP).multiply(new BigDecimal("100")).toString();
			}

			String cpuUtilStr = voRestCpuCores;
			log.info("监控项[SERVER_CPU_UTIL] id:{}, value:{}", datacenterId, cpuUtilStr);
			if (cpuUtilStr.length() <= 0) {
				return false;
			}
			Double cpuCore = Double.parseDouble(cpuUtilStr);
			if ("<".equals(operation)) {
				if (cpuCore < value) {
					return true;
				}
			} else if (">".equals(operation)) {
				if (cpuCore > value) {
					return true;
				}
			} else if ("=".equals(operation)) {
				if (cpuCore == value) {
					return true;
				}
			} else if ("<=".equals(operation)) {
				if (cpuCore <= value) {
					return true;
				}
			} else if (">=".equals(operation)) {
				if (cpuCore >= value) {
					return true;
				}
			} else if ("!=".equals(operation)) {
				if (cpuCore != value) {
					return true;
				}
			} else {
				return false;
			}
		}
		return false;
	}

}
