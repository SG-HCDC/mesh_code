package com.gcloud.mesh.dcs.dataclass;

import java.util.ArrayList;
import java.util.List;

import com.gcloud.mesh.header.vo.dcs.DataItemVo;

public class DataClassificationCache {
	private static List<DataItemVo> dataVos;
	public static void saveCache(List<DataItemVo> vos) {
		if(dataVos == null) {
			dataVos = new ArrayList<>();
		}
		dataVos.addAll(vos);
	}
	public static List<DataItemVo> getCache(){
		return dataVos;
	}
	public static void clear() {
		if(dataVos != null) {
			dataVos.clear();
		}
	}
}
