package com.gcloud.mesh.dcs.dataclass;

import java.util.List;

import org.jeecg.common.util.dataclassification.DataClassificationUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.gcloud.mesh.config.DataClassificationConfig;
import com.gcloud.mesh.header.vo.dcs.DataItemVo;
import com.gcloud.mesh.header.vo.dcs.DataSourceItemVo;

@Component
public class DataClassificationCacheTimer {
	
	@Autowired
	private DataClassificationConfig classificationConfig;

	@Scheduled(fixedDelay = 1000 * 60 * 5)
	public void work() {
		List<DataSourceItemVo> dataSources = classificationConfig.getSource();  
		DataClassificationCache.clear();
    	if(dataSources != null) {
        	for(DataSourceItemVo item: dataSources) {
        		List<DataItemVo> vos = DataClassificationUtil.getStatisticsByDb(classificationConfig.getHost(), classificationConfig.getPort(), classificationConfig.getUser(), classificationConfig.getPassword(), item.getDatabase());
        		DataClassificationCache.saveCache(vos);
        	}
    	}
	}
}
