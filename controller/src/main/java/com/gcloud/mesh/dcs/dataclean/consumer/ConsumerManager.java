package com.gcloud.mesh.dcs.dataclean.consumer;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;

import lombok.extern.slf4j.Slf4j;
import me.zhyd.oauth.log.Log;

@Slf4j
@Component
public class ConsumerManager {

	private List<IConsumer> chains = new ArrayList<IConsumer>();
	
	public ConsumerManager addChain(IConsumer consumer) {
		chains.add(consumer);
		return this;
	}
	
	public Object process(Object msg) throws Exception {
		Object nextMsg = msg;
		try {
			for(IConsumer consumer: chains) {
				if(nextMsg != null) {
					nextMsg = consumer.process(nextMsg);
				}else {
					nextMsg = consumer.process(msg);
				}
			}
		} catch(Exception e) {
			log.error("[Consumer] process处理失败:{}", e.getMessage());
		}
		
		return nextMsg;
	}
	
	public void resetChain() {
		chains.clear();
	}
	
}
