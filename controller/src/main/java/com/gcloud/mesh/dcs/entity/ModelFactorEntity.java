
package com.gcloud.mesh.dcs.entity;

import com.gcloud.framework.db.jdbc.annotation.ID;
import com.gcloud.framework.db.jdbc.annotation.Table;

import lombok.Data;

@Table(name = "dcs_model_factors")
@Data
public class ModelFactorEntity {

    @ID
    private String id;
    private String modelType;
    private String factorType;
    private String factorName;
    private Boolean enabled;
    private Integer scoreSample;
    private String scoreType;
    private Integer weight;
    private Integer unitConversion;
    private Integer startX;
    private Integer startY;
    private Integer endX;
    private Integer endY;
    private Integer scoreBestPredict;
    private Integer scorePredictStandard;

}
