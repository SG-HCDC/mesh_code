
package com.gcloud.mesh.dcs.entity;

import java.util.Date;

import com.gcloud.framework.db.jdbc.annotation.ID;
import com.gcloud.framework.db.jdbc.annotation.Table;

import lombok.Data;

@Table(name = "dcs_scheduler_configs")
@Data
public class SchedulerConfigEntity {

    @ID
    private String id;
    private String datacenterId;
    
    private String thresholdKey;
    private String thresholdOperation;
	private Double thresholdValue;
	private Date updateTime;

}
