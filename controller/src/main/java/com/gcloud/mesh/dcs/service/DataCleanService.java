package com.gcloud.mesh.dcs.service;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import com.gcloud.mesh.dcs.dao.DataCleanDao;
import com.gcloud.mesh.dcs.dataclean.DataCleanManager;
import com.gcloud.mesh.dcs.entity.DataCleanConfigEntity;
import com.gcloud.mesh.header.enums.DataCleanOperateType;
import com.gcloud.mesh.header.exception.BaseException;
import com.gcloud.mesh.header.msg.dcs.UpdateDataCleanConfigMsg;
import com.gcloud.mesh.header.vo.dcs.DataCleanConfigVo;
import com.gcloud.mesh.header.vo.monitor.SampleVo;

@Service
public class DataCleanService {

	@Autowired
	private DataCleanDao dao;
	
	@Autowired
	private DataCleanManager dataCleanManager;
	
	@PostConstruct
	private void init() {
		DataCleanConfigVo config = get();
		dataCleanManager.reset();
		if(config.getEnabled() != null && config.getEnabled() == true) {
			dataCleanManager.init(config.getOperateType());
		}
	}

	public DataCleanConfigVo get() {

		DataCleanConfigVo vo = new DataCleanConfigVo();
		List<DataCleanConfigEntity> entities = dao.findAll();
		for(DataCleanConfigEntity entity: entities) {
			vo.setId(entity.getId());
			vo.setEnabled(entity.getEnabled());
			vo.setSource(entity.getSource());
			vo.setSourceName(entity.getSourceName());
			vo.setOperateType(entity.getOperateType());
			break;
		}
		return vo;
	}

	public void update(UpdateDataCleanConfigMsg msg) {

		DataCleanConfigEntity entity = dao.getById(msg.getId());

		List<String> fields = new ArrayList<String>();

		if (msg.getOperateType() != null) {
			if(DataCleanOperateType.getByName(msg.getOperateType()) == null) {
				throw new BaseException("::未识别的操作类型");
			}
			fields.add("operate_type");
			entity.setOperateType(msg.getOperateType());
		}
//		if (msg.getSource() != null) {
//			fields.add("source");
//			entity.setSource(msg.getSource());
//		}
		if (msg.getEnabled() != null && msg.getEnabled() == entity.getEnabled()) {
			fields.add("enabled");
			entity.setEnabled(msg.getEnabled());
		}
		if (msg.getEnabled() != null) {
//			if(msg.getEnabled() == entity.getEnabled() && entity.getEnabled() == true) {
//				throw new BaseException("::策略已是启用状态");
//			}else if(msg.getEnabled() == entity.getEnabled() && entity.getEnabled() == false) {
//				throw new BaseException("::策略已是禁用状态");
//			}
			fields.add("enabled");
			entity.setEnabled(msg.getEnabled());
		}
		
		try {
			dao.update(entity, fields);
			if(msg.getEnabled() == true) {
				dataCleanManager.reset();
				dataCleanManager.init(entity.getOperateType());
			}else if(msg.getEnabled() == false) {
				dataCleanManager.reset();
			}
		} catch(Exception e) {
			throw new BaseException("DataCleanConfigEntity 配置修改失败", e.getMessage());
		}
		
	}
	
	@Scheduled(fixedRate = 1*1000)
	private void test() {
		SampleVo vo = new SampleVo();
		vo.setMeter("test");
		SampleVo newVo = (SampleVo)dataCleanManager.syncProcess(vo);
	}

}
