
package com.gcloud.mesh.dcs.service;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Component;

import com.gcloud.mesh.dcs.entity.ModelFactorStepEntity;
import com.gcloud.mesh.header.enums.ModelFactorType;
import com.gcloud.mesh.header.vo.dcs.ModelFactorVo;
import com.google.common.collect.Lists;

@Component
public class SamplePercentageCalculator {

	private static final Map<String, List<ModelFactorStepEntity>> FACTOR_STEPS = new HashMap<>();

	static {
		List<ModelFactorStepEntity> steps = Lists.newArrayList();
		steps.add(new ModelFactorStepEntity(null, -10, 0, 1));
		steps.add(new ModelFactorStepEntity(null, 1, 5, 2));
		steps.add(new ModelFactorStepEntity(null, 6, 10, 4));
		steps.add(new ModelFactorStepEntity(null, 11, 20, 8));
		steps.add(new ModelFactorStepEntity(null, 21, 999, 100));
		FACTOR_STEPS.put(ModelFactorType.CPU_LOAD.name(), steps);
	}

	/*
	 * public static int calculate(int value, String factorType, Integer
	 * scoreSample) { int diff = value - scoreSample; for( ModelFactorStepEntity
	 * step : FACTOR_STEPS.get(factorType)) { if( diff >= step.getLeft() &&
	 * diff<step.getRight()) {
	 * 
	 * } } return diff; }
	 */

	public static int calculate(int value, String factorType, Integer scoreSample) {
		int diff = Math.abs(value - scoreSample);
		int score = diff > 100 ? 0 : 100 - diff;
		return score;
	}

	public static int calculate(ModelFactorVo factor, int value) {
		if (value <= factor.getStartX()) {
			return factor.getStartY();
		} else if (value >= factor.getEndX()) {
			return factor.getEndY();
		} else if (value == factor.getScoreSample()) {
			return 100;
		} else if (value < factor.getScoreSample()) {
			double rate = 1.0 * (100 - factor.getStartY()) / (factor.getScoreSample() - factor.getStartX());
			double res = 100 - rate * (factor.getScoreSample() - value);
			return (int) Math.round(res);
		} else {
			double rate = 1.0 * (100 - factor.getEndY()) / (factor.getEndX() - factor.getScoreSample());
			double res = (100 - rate * (value - factor.getScoreSample()));
			return (int) Math.round(res);
		}
	}
	
	public static double calculateD(ModelFactorVo factor, double value) {
		if (value <= factor.getStartX()) {
			return factor.getStartY();
		} else if (value >= factor.getEndX()) {
			return factor.getEndY();
		} else if (value == factor.getScoreSample()) {
			return 100;
		} else if (value < factor.getScoreSample()) {
			double rate = 1.0 * (100 - factor.getStartY()) / (factor.getScoreSample() - factor.getStartX());
			double res = 100 - rate * (factor.getScoreSample() - value);
			return Math.round(res);
		} else {
			double rate = 1.0 * (100 - factor.getEndY()) / (factor.getEndX() - factor.getScoreSample());
			double res = (100 - rate * (value - factor.getScoreSample()));
			return Math.round(res);
		}
	}

}
