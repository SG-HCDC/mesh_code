
package com.gcloud.mesh.dcs.entity;

import java.util.Date;

import com.gcloud.framework.db.jdbc.annotation.ID;
import com.gcloud.framework.db.jdbc.annotation.Table;

import lombok.Data;
import lombok.experimental.Accessors;

@Table(name = "dcs_app_instance")
@Data
@Accessors(chain = true)
public class AppInstanceEntity {

	@ID
	private Integer id;
	private String name;
	private String appId;
	private Date createTime;
	private String instanceId;

}
