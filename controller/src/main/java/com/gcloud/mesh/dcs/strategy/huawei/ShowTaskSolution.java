package com.gcloud.mesh.dcs.strategy.huawei;



import com.huaweicloud.sdk.core.auth.ICredential;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;

import com.huaweicloud.sdk.core.auth.GlobalCredentials;
import com.huaweicloud.sdk.core.exception.ConnectionException;
import com.huaweicloud.sdk.core.exception.RequestTimeoutException;
import com.huaweicloud.sdk.core.exception.ServiceResponseException;
import com.huaweicloud.sdk.core.http.HttpConfig;
import com.huaweicloud.sdk.sms.v3.*;
import com.huaweicloud.sdk.sms.v3.region.SmsRegion;

import lombok.extern.slf4j.Slf4j;

import com.huaweicloud.sdk.sms.v3.model.*;

@Slf4j
@Component
public class ShowTaskSolution {

    public static void main(String[] args) {
    	ShowTaskSolution showtask = new ShowTaskSolution();
    	showtask.showTask(null,null,null);
    }
    public ShowTaskResponse showTask(String ak, String sk, String taskId) {
    	if(StringUtils.isBlank(ak)) {
   		 ak = "8QVISVT4XJHUNMIEPZBQ";
	   	}
	   	if(StringUtils.isBlank(sk)) {
	   		sk = "GingsSlHInSkQ3x47jMSEjFT0uumSJgplZs926Cw";
	   	}
  	    HttpConfig config = HttpConfig.getDefaultHttpConfig();
  	    config.withIgnoreSSLVerification(true);
        ShowTaskResponse response = null;
        ICredential auth = new GlobalCredentials()
                .withAk(ak)
                .withSk(sk);

        SmsClient client = SmsClient.newBuilder()
                .withCredential(auth)
                .withHttpConfig(config)
                .withRegion(SmsRegion.valueOf("ap-southeast-1"))
                .build();
        ShowTaskRequest request = new ShowTaskRequest();
        request.withTaskId(taskId);
        try {
            response = client.showTask(request);
            response.getState();
        } catch (ConnectionException e) {
        	log.error("迁移获取任务详情失败："+ e);
//            e.printStackTrace();
        } catch (RequestTimeoutException e) {
        	log.error("迁移获取任务详情失败："+ e);
//            e.printStackTrace();
        } catch (ServiceResponseException e) {
        	log.error("迁移获取任务想请失败："+ e);
//            e.printStackTrace();
//            System.out.println(e.getHttpStatusCode());
//            System.out.println(e.getErrorCode());
//            System.out.println(e.getErrorMsg());
        }
        return response;
    }
}