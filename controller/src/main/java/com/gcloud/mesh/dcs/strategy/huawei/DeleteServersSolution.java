package com.gcloud.mesh.dcs.strategy.huawei;




import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;

import com.gcloud.mesh.dcs.annotation.SchedulerLog;
import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.exception.ConnectionException;
import com.huaweicloud.sdk.core.exception.RequestTimeoutException;
import com.huaweicloud.sdk.core.exception.ServiceResponseException;
import com.huaweicloud.sdk.core.http.HttpConfig;
import com.huaweicloud.sdk.ecs.v2.EcsClient;
import com.huaweicloud.sdk.ecs.v2.model.DeleteServersRequest;
import com.huaweicloud.sdk.ecs.v2.model.DeleteServersRequestBody;
import com.huaweicloud.sdk.ecs.v2.model.DeleteServersResponse;
import com.huaweicloud.sdk.ecs.v2.model.ServerId;
import com.huaweicloud.sdk.ecs.v2.region.EcsRegion;

import me.zhyd.oauth.log.Log;

@Component
public class DeleteServersSolution {

    public static void main(String[] args) {
    	DeleteServersSolution delete = new DeleteServersSolution();
    	delete.deleteServers("","8QVISVT4XJHUNMIEPZBQ", "GingsSlHInSkQ3x47jMSEjFT0uumSJgplZs926Cw", "cn-south-1", "436db1ab-e396-4c41-9c12-d93b5004e6d9");
    }
    @SchedulerLog(stepName="删除源虚拟机实例")
    public DeleteServersResponse deleteServers(String jobId, String ak, String sk,  String regionId, String instanceId) {
  	    HttpConfig config = HttpConfig.getDefaultHttpConfig();
  	    config.withIgnoreSSLVerification(true);
        ICredential auth = new BasicCredentials()
                .withAk(ak)
                .withSk(sk);

        EcsClient client = EcsClient.newBuilder()
        		.withHttpConfig(config)
                .withCredential(auth)
                .withRegion(EcsRegion.valueOf(regionId))
                .build();
        DeleteServersRequest request = new DeleteServersRequest();
        DeleteServersRequestBody body = new DeleteServersRequestBody();
        List<ServerId> listbodyServers = new ArrayList<>();
        listbodyServers.add(
            new ServerId()
                .withId(instanceId)
        );
        body.withServers(listbodyServers);
        body.withDeleteVolume(true);
        body.withDeletePublicip(true);
        request.setBody(body);
        DeleteServersResponse response = null;
        try {
            response = client.deleteServers(request);
        } catch (ConnectionException e) {
        	Log.error("删除失败"+e);
            e.printStackTrace();
        } catch (RequestTimeoutException e) {
        	Log.error("删除失败"+e);
            e.printStackTrace();
        } catch (ServiceResponseException e) {
        	Log.error("删除失败"+e);
            e.printStackTrace();

        }
        return response;
    }
}