package com.gcloud.mesh.dcs.enums;

import java.util.Objects;

public enum  AliPowerDeviceType {
    AIR("air","空调"),
    SERVER("server","服务器"),
    CABINET("cabinet","机柜"),
    PDU("pdu","pdu");

    private String name;
    private String cnName;

    AliPowerDeviceType(String name, String cnName) {
        this.name = name;
        this.cnName = cnName;
    }

    public static boolean contains(String name){
        for (AliPowerDeviceType type: AliPowerDeviceType.values()){
            if (Objects.equals(type.name,name)){
                return true;
            }
        }
        return false;
    }

    public static AliPowerDeviceType getByName(String name){
        for (AliPowerDeviceType type: AliPowerDeviceType.values()){
            if (Objects.equals(type.name,name)){
                return type;
            }
        }
        return null;
    }
}
