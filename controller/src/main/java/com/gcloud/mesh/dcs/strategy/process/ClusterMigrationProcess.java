package com.gcloud.mesh.dcs.strategy.process;

import com.gcloud.mesh.dcs.annotation.SchedulerLog;
import com.gcloud.mesh.framework.core.SpringUtil;
import com.gcloud.mesh.sdk.GceSDK;
import lombok.extern.slf4j.Slf4j;
import org.jeecg.common.exception.MyBusinessException;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class ClusterMigrationProcess {

    @SchedulerLog(stepName="删除源命名空间")
    public void deleteSrcNameSpace(String jobId,String nameSpaceId,Integer maxCheckTime){
        try {
            GceSDK gceSDK = SpringUtil.getBean(GceSDK.class);
            //删除命名空间
            gceSDK.delateNamespace(nameSpaceId);
        } catch (Exception e) {
            if(maxCheckTime!=null && maxCheckTime-->0){
                deleteSrcNameSpace(jobId,nameSpaceId,maxCheckTime);
            }
            log.error("删除源命名空间失败",e);
            throw new MyBusinessException("删除源命名空间失败");
        }
    }


}
