
package com.gcloud.mesh.dcs.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.gcloud.mesh.asset.dao.IaasDao;
import com.gcloud.mesh.asset.entity.IaasEntity;
import com.gcloud.mesh.asset.enums.DeviceType;
import com.gcloud.mesh.dcs.dao.AppDao;
import com.gcloud.mesh.dcs.enums.DeviceThresholdType;
import com.gcloud.mesh.header.enums.MonitorMeter;
import com.gcloud.mesh.monitor.service.StatisticsCheatService;
import com.gcloud.mesh.monitor.service.StatisticsService;

import lombok.extern.slf4j.Slf4j;

@Component
@Slf4j
public class DistributionBoxRatedPowerThresholdService implements IDeviceThresholdService {

	@Autowired
	private AppDao appDao;

	@Autowired
	private IaasDao iaasDao;

	@Autowired
	private StatisticsService statisticsService;

	@Autowired
	private StatisticsCheatService statisticsCheatService;

	@Override
	public DeviceThresholdType device() {
		return DeviceThresholdType.DISTRIBUTION_BOX_RATED_POWER;
	}

	@Override
	public Boolean checkScheduler(String datacenterId,String operation, Double value) {
		Map<String, Object> props = new HashMap<>();
		// props.put("datacenter_id", app.getDatacenterId());
		props.put("type", DeviceType.DISTRIBUTION_BOX.getNo());
		List<IaasEntity> iaases = iaasDao.findByProperties(props);
		if (iaases != null) {
			for (IaasEntity iaas : iaases) {
				String voltageStr = statisticsCheatService.latestSample(MonitorMeter.DISTRIBUTION_BOX_VOLTAGE.getMeter(), iaas.getId());
				String electronStr = statisticsCheatService.latestSample(MonitorMeter.DISTRIBUTION_BOX_ELECTRON.getMeter(), iaas.getId());
				log.info("监控项[DISTRIBUTION_BOX_VOLTAGE] id:{}, value:{}", iaas.getId(), voltageStr);
				log.info("监控项[DISTRIBUTION_BOX_ELECTRON] id:{}, value:{}", iaas.getId(), electronStr);
				if (voltageStr.length() <= 0 && electronStr.length() <= 0) {
					continue;
				}
				Double ratedPower = Double.parseDouble(voltageStr) * Double.parseDouble(electronStr); // 额定电流
																										// x
																										// 额定电压
				if ("<".equals(operation)) {
					if (ratedPower < value) {
						return true;
					}
				} else if (">".equals(operation)) {
					if (ratedPower > value) {
						return true;
					}
				} else if ("=".equals(operation)) {
					if (ratedPower == value) {
						return true;
					}
				} else if ("<=".equals(operation)) {
					if (ratedPower <= value) {
						return true;
					}
				} else if (">=".equals(operation)) {
					if (ratedPower >= value) {
						return true;
					}
				} else if ("!=".equals(operation)) {
					if (ratedPower != value) {
						return true;
					}
				} else{
					return false;
				}

			}
		}
		return false;
	}

}
