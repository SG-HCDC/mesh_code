package com.gcloud.mesh.dcs.strategy.huawei;



import com.huaweicloud.sdk.core.auth.ICredential;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;

import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.exception.ConnectionException;
import com.huaweicloud.sdk.core.exception.RequestTimeoutException;
import com.huaweicloud.sdk.core.exception.ServiceResponseException;
import com.huaweicloud.sdk.core.http.HttpConfig;
import com.huaweicloud.sdk.ecs.v2.*;
import com.huaweicloud.sdk.ecs.v2.region.EcsRegion;

import lombok.extern.slf4j.Slf4j;

import com.huaweicloud.sdk.ecs.v2.model.*;

@Slf4j
@Component
public class ListServersDetailsSolution {

    public static void main(String[] args) {
    	ListServersDetailsSolution listServer = new ListServersDetailsSolution();
    	listServer.listServers("cn-north-1", "8QVISVT4XJHUNMIEPZBQ", "GingsSlHInSkQ3x47jMSEjFT0uumSJgplZs926Cw", null);
    }
    
    public ListServersDetailsResponse listServers(String regionId, String ak, String sk, String projectId) {
    	ListServersDetailsResponse response = null;
      	if(StringUtils.isBlank(ak)) {
     		 ak = "8QVISVT4XJHUNMIEPZBQ";
  	   	}
  	   	if(StringUtils.isBlank(sk)) {
  	   		sk = "GingsSlHInSkQ3x47jMSEjFT0uumSJgplZs926Cw";
  	   	}
  	   	if(StringUtils.isBlank(projectId)) {
  	   	    // projectId = "0a8a8ceac300f5fc2fddc00317ac8464";
  	   	}
  	   	if(StringUtils.isBlank(regionId)) {
  	   		regionId = "cn-south-1";
  	   	}
  	    HttpConfig config = HttpConfig.getDefaultHttpConfig();
  	    config.withIgnoreSSLVerification(true);
        ICredential auth = new BasicCredentials()
                .withProjectId(projectId)
                .withAk(ak)
                .withSk(sk);

        EcsClient client = EcsClient.newBuilder()
                .withCredential(auth)
                .withHttpConfig(config)
                .withRegion(EcsRegion.valueOf(regionId))
                .build();
        ListServersDetailsRequest request = new ListServersDetailsRequest();
        try {
            response = client.listServersDetails(request);
            // System.out.println(response.toString());
        }  catch (ConnectionException e) {
        	log.error("迁移获取服务器实例连接失败："+ e);
        } catch (RequestTimeoutException e) {
        	log.error("迁移获取服务器实例超时："+ e);
            e.printStackTrace();
        } catch (ServiceResponseException e) {
        	log.error("迁移获取服务器实例异常："+ e.getErrorMsg());
        }
        return response;
    }
}