package com.gcloud.mesh.dcs.strategy;

import java.util.List;

import com.gcloud.mesh.dcs.entity.AppEntity;
import com.google.common.collect.Lists;

public interface QueryInstance {
	default List<AppEntity> queryServerList(String datacenterId){return Lists.newArrayList();};
}
