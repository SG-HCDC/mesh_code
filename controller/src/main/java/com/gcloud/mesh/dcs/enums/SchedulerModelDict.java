package com.gcloud.mesh.dcs.enums;

import java.util.Objects;

public enum  SchedulerModelDict {
	DATACENTER_COST("0"), // 最优成本
    DATACENTER_DEVICE("1"), // 最优设备
    DATACENTER_POWER("2"), // 最优能效
    DATACENTER_ELECTRIC("3"), // 配电子网均衡
	APP("4");

    private String DictItem;

    SchedulerModelDict(String dictItem) {
        DictItem = dictItem;
    }

    public String getDictItem() {
        return DictItem;
    }

    public static SchedulerModelDict get(String dictItem){
        if (dictItem!=null) {
            for (SchedulerModelDict s : SchedulerModelDict.values()) {
                if (Objects.equals(s.getDictItem(),dictItem)){
                    return s;
                }
            }
        }
        return null;
    }
}
