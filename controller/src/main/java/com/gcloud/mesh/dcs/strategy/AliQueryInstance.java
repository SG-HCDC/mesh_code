package com.gcloud.mesh.dcs.strategy;

import java.util.List;
import java.util.stream.Collectors;

import org.jeecg.common.exception.MyBusinessException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.alibaba.fastjson.JSONObject;
import com.aliyun.ecs20140526.models.DescribeInstancesRequest;
import com.aliyun.ecs20140526.models.DescribeInstancesResponse;
import com.aliyun.ecs20140526.models.DescribeInstancesResponseBody;
import com.aliyun.teaopenapi.models.Config;
import com.gcloud.mesh.asset.dao.CloudResourceConfigDao;
import com.gcloud.mesh.asset.entity.CloudResourceConfigEntity;
import com.gcloud.mesh.dcs.entity.AppEntity;
import com.gcloud.mesh.header.param.AliMigrationParam;
import com.google.common.collect.Lists;

import lombok.extern.slf4j.Slf4j;
@Slf4j
@Component
public class AliQueryInstance implements QueryInstance{
	
    @Autowired
    private CloudResourceConfigDao cloudResourceConfigDao;
    
    
    @Override
    public List<AppEntity> queryServerList(String cloudResourceId) {

		CloudResourceConfigEntity cloudResource = cloudResourceConfigDao.findOneByProperty("cloud_resource_id", cloudResourceId);
        if(cloudResource==null) return Lists.newArrayList();

        AliMigrationParam aliMigrationParam = JSONObject.toJavaObject(JSONObject.parseObject(cloudResource.getConfig()), AliMigrationParam.class);

        return queryServerList(aliMigrationParam);
    }
    
    public List<AppEntity> queryServerList(AliMigrationParam aliMigrationParam){
        Config config = new Config()
                // 您的AccessKey ID
                .setAccessKeyId(aliMigrationParam.getAccessKeyId())
                // 您的AccessKey Secret
                .setAccessKeySecret(aliMigrationParam.getAccessSecret());
        // 访问的域名
        config.endpoint = "ecs."+aliMigrationParam.getRegionId()+".aliyuncs.com";

        com.aliyun.ecs20140526.Client client = null;
        try {
            client = new  com.aliyun.ecs20140526.Client(config);
            DescribeInstancesRequest describeInstancesRequest = new DescribeInstancesRequest()
                    .setRegionId(aliMigrationParam.getRegionId())
                    .setMaxResults(100);
            DescribeInstancesResponse describeInstancesResponse = client.describeInstances(describeInstancesRequest);
            DescribeInstancesResponseBody.DescribeInstancesResponseBodyInstances instances = describeInstancesResponse.getBody().getInstances();
            List<DescribeInstancesResponseBody.DescribeInstancesResponseBodyInstancesInstance> instance = instances.getInstance();
            return instance.stream().map(i -> {
                AppEntity app = new AppEntity();
                app.setId(i.getInstanceId());
                app.setName(i.getHostName());
                return app;
            }).collect(Collectors.toList());
        } catch (Exception e) {
            log.error("查询阿里云虚拟机列表错误",e);
            throw new MyBusinessException("查询阿里云虚拟机列表错误");
        }
        
    }
}
