package com.gcloud.mesh.dcs.enums;

import java.util.stream.Stream;

public enum SchedulerJobStatus {

	READY(0, "准备"), PROGRESS(1, "进行中"), FAIL(2,  "失败"), SUCCESS(3, "成功");

	private Integer value;
	private String name;

	SchedulerJobStatus(Integer value, String name) {
		this.value = value;
		this.name = name;
	}

	public Integer getValue() {
		return value;
	}

	public String getName() {
		return name;
	}

	public static SchedulerJobStatus getByValue(Integer value) {
		return Stream.of(SchedulerJobStatus.values()).filter(s -> s.getValue() == value).findFirst().orElse(null);
	}

}
