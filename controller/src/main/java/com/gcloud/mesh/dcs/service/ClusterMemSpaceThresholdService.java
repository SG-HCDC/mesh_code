package com.gcloud.mesh.dcs.service;

import com.gcloud.mesh.framework.core.SpringUtil;
import com.gcloud.mesh.sdk.GceSDK;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.gcloud.framework.db.PageResult;
import com.gcloud.mesh.asset.service.IAssetService;
import com.gcloud.mesh.dcs.dao.AppDao;
import com.gcloud.mesh.dcs.enums.DeviceThresholdType;
import com.gcloud.mesh.header.enums.MonitorMeter;
import com.gcloud.mesh.header.vo.asset.DatacenterItemVo;
import com.gcloud.mesh.header.vo.asset.NodeItemVo;
import com.gcloud.mesh.monitor.service.StatisticsCheatService;
import com.gcloud.mesh.sdk.gce.ClusterVo;
import com.gcloud.mesh.supplier.dao.SupplierDao;
import com.gcloud.mesh.supplier.enums.K8sClusterType;
import com.gcloud.mesh.supplier.enums.SystemType;

import lombok.extern.slf4j.Slf4j;

import java.math.BigDecimal;
import java.math.RoundingMode;

@Component
@Slf4j
public class ClusterMemSpaceThresholdService implements IDeviceThresholdService {

	@Autowired
	private AppDao appDao;

	@Autowired
	private SupplierDao supplierDao;

	@Autowired
	IAssetService assetService;

	@Autowired
	StatisticsCheatService statisticsCheatService;

	@Override
	public DeviceThresholdType device() {
		return DeviceThresholdType.CLUSTER_MEM_SPACE;
	}

	@Override
	public Boolean checkScheduler(String datacenterId,String operation, Double value) {
		String clusterId = supplierDao.getConfigValueByDatacenterIdAndConfigName(datacenterId, SystemType.K8S.getName(), K8sClusterType.CLUSTER_ID.getName());
		ClusterVo clusterVo = null;
		if (clusterId != null) {
			GceSDK sdk = SpringUtil.getBean(GceSDK.class);
			clusterVo = sdk.detailCluster(clusterId);
		}
		if (clusterVo != null && clusterVo.getCluster()!=null) {
			String voMemSpaceG = clusterVo.getCluster().getCapacity().get("memory");
			String voRestMemSpaceG = clusterVo.getCluster().getRequested().get("memory");
			double memSpaceG;
			double restMemSpaceG;
 			if (voMemSpaceG.endsWith("i") || voRestMemSpaceG.endsWith("i")) {
				if (voMemSpaceG.endsWith("Ki")) {
					memSpaceG = Double.valueOf(voMemSpaceG.replace("Ki", "")) / 1024 / 1024;
				} else if (voMemSpaceG.endsWith("Mi")) {
					memSpaceG = Double.valueOf(voMemSpaceG.replace("Mi", "")) / 1024;
				} else if (voMemSpaceG.endsWith("Gi")) {
					memSpaceG = Double.valueOf(voMemSpaceG.replace("Gi",""));
				} else {
					memSpaceG = 0.0;
				}
				//
				if (voRestMemSpaceG.endsWith("Ki")) {
					restMemSpaceG = Double.valueOf(voRestMemSpaceG.replace("Ki", "")) / 1024 / 1024;
				} else if (voRestMemSpaceG.endsWith("Mi")) {
					restMemSpaceG = Double.valueOf(voRestMemSpaceG.replace("Mi", "")) / 1024;
				} else if (voRestMemSpaceG.endsWith("Gi")) {
					restMemSpaceG = Double.valueOf(voRestMemSpaceG.replace("Gi", ""));
				} else {
					restMemSpaceG = 0.0;
				}
			} else {
				memSpaceG = Double.valueOf(voMemSpaceG);
				restMemSpaceG = Double.valueOf(voRestMemSpaceG);
			}


			if(memSpaceG>0){
				// 实际内存 四舍五入保留一位小数  与前端计算方式一致
				BigDecimal a1 = new BigDecimal(restMemSpaceG).setScale(1,RoundingMode.HALF_UP);
				// 总内存 四舍五入保留一位小数  与前端计算方式一致
				BigDecimal a2 = new BigDecimal(memSpaceG).setScale(1,RoundingMode.HALF_UP);
				//百分比 四舍五入保留两位位小数  与前端计算方式一致
				BigDecimal a3 = (a1.divide(a2,5,RoundingMode.HALF_UP).multiply(new BigDecimal("100"))).setScale(2, RoundingMode.HALF_UP);
				restMemSpaceG = a3.doubleValue();
			}

			String memSpaceStr = restMemSpaceG + "";

			log.info("监控项[SERVER_MEMORY_UTIL] id:{}, value:{}", datacenterId, memSpaceStr);
			if (memSpaceStr.length() <= 0) {
				return false;
			}
			Double memSpace = Double.parseDouble(memSpaceStr);
			if ("<".equals(operation)) {
				if (memSpace < value) {
					return true;
				}
			} else if (">".equals(operation)) {
				if (memSpace > value) {
					return true;
				}
			} else if ("=".equals(operation)) {
				if (memSpace == value) {
					return true;
				}
			} else if ("<=".equals(operation)) {
				if (memSpace <= value) {
					return true;
				}
			} else if (">=".equals(operation)) {
				if (memSpace >= value) {
					return true;
				}
			} else if ("!=".equals(operation)) {
				if (memSpace != value) {
					return true;
				}
			} else {
				return false;
			}
		}
			return false;
		}
}
