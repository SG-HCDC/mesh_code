package com.gcloud.mesh.dcs.controller;

import com.gcloud.framework.db.PageResult;
import com.gcloud.mesh.dcs.annotation.ParamSecurity;
import com.gcloud.mesh.dcs.service.VoltageFmService;
import com.gcloud.mesh.header.msg.dcs.DetailDvfsMsg;
import com.gcloud.mesh.header.msg.dcs.PageVoltageFmMsg;
import com.gcloud.mesh.header.msg.dcs.SetVoltageFmStrategyMsg;
import com.gcloud.mesh.header.vo.dcs.DetailDvfsVo;
import com.gcloud.mesh.header.vo.dcs.VoltageFmVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.jeecg.common.aspect.annotation.AutoLog;
import org.jeecg.common.constant.CommonConstant;
import org.jeecg.common.constant.enums.SysLogOperateType;
import org.jeecg.common.system.api.ISysBaseAPI;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@Api(tags = "动态电压调频")
@RestController
@RequestMapping("/fm")
@Slf4j
public class VoltageFmController {

	@Autowired
	private VoltageFmService service;
	
	@Autowired
	private ISysBaseAPI sysBaseAPI;
	
	//处理Mass Assignment: Insecure Binder Configuration
	@InitBinder
	public void populateCustomerRequest(WebDataBinder binder) {
	    binder.setDisallowedFields(new String[]{});
	}

    // @AutoLog(value = "查询调频列表",operateType = CommonConstant.OPERATE_TYPE_LIST)
	@ApiOperation(value = "获取调频列表", notes = "获取调频列表")
	@RequestMapping(value = "/page", method = RequestMethod.POST)
	@RequiresPermissions(value = {"fmPage"})
    @ParamSecurity(enabled = true)
	public PageResult<VoltageFmVo> dvfsPage(@Valid PageVoltageFmMsg msg) {
    	if(msg != null && msg.getTitle() != null) {
    		sysBaseAPI.addLog("查询计算节点控制列表", null, SysLogOperateType.QUERY, null, null, msg.getTitle());
    	}else {
    		sysBaseAPI.addLog("查询动态电压调频列表", null, SysLogOperateType.QUERY);
    	}
		return service.page(msg);
	}

    // @AutoLog(value = "设置动态电压调频",operateType = CommonConstant.OPERATE_TYPE_CONFIG)
	@ApiOperation(value = "设置动态电压调频", notes = "设置动态电压调频")
	@RequestMapping(value = "/setStrategy", method = RequestMethod.POST)
	@RequiresPermissions(value = {"fmSetStrategy"})
    @ParamSecurity(enabled = true)
	public void setFmStrategy(@Valid SetVoltageFmStrategyMsg msg) {
		service.setFmStrategy(msg);
		StringBuffer buff = new StringBuffer();
    	buff.append("设置动态电压调频，动态电压调频ID："+msg.getId());
    	buff.append(","+msg.toString());
    	if(msg != null && msg.getTitle() != null) {
    		sysBaseAPI.addLog(buff.toString(), null, SysLogOperateType.UPDATE, null, null, msg.getTitle());
    	}else {
    		sysBaseAPI.addLog(buff.toString(), null, SysLogOperateType.UPDATE);
    	}
		
	}

    @AutoLog(value = "查看动态电压调频详情",operateType = CommonConstant.OPERATE_TYPE_DETAIL)
	@ApiOperation(value = "获取动态电压调频详情", notes = "获取动态电压调频详情")
	@RequestMapping(value = "/detailDvfs", method = RequestMethod.POST)
	@RequiresPermissions(value = {"fmDetailDvfs"})
    @ParamSecurity(enabled = true)
	public DetailDvfsVo detailDvfs(@Valid DetailDvfsMsg msg) {
		return service.detailDvfs(msg);
	}
}
