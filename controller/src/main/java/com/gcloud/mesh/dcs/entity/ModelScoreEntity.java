
package com.gcloud.mesh.dcs.entity;

import java.util.Date;

import com.gcloud.framework.db.jdbc.annotation.ID;
import com.gcloud.framework.db.jdbc.annotation.Table;

import lombok.Data;

@Table(name = "dcs_model_scores")
@Data
public class ModelScoreEntity {

    @ID
    private String id;
    private String resourceId;
    private String modelType;
    private String scores;
    private Integer totalScore;
    private String factorNames;
    private String samples;
    private Date updateTime;
    private String values;

}
