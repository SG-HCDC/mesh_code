package com.gcloud.mesh.dcs.strategy.huawei;



import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.auth.GlobalCredentials;
import com.huaweicloud.sdk.core.exception.ConnectionException;
import com.huaweicloud.sdk.core.exception.RequestTimeoutException;
import com.huaweicloud.sdk.core.exception.ServiceResponseException;
import com.huaweicloud.sdk.core.http.HttpConfig;
import com.huaweicloud.sdk.sms.v3.*;
import com.huaweicloud.sdk.sms.v3.region.SmsRegion;
import com.huaweicloud.sdk.sms.v3.model.*;

import java.util.List;
import java.util.ArrayList;

public class CreateTemplateSolution {

    public static void main(String[] args) {
    	String ak = "8QVISVT4XJHUNMIEPZBQ";
    	String sk = "GingsSlHInSkQ3x47jMSEjFT0uumSJgplZs926Cw";
  	    HttpConfig config = HttpConfig.getDefaultHttpConfig();
  	    config.withIgnoreSSLVerification(true);
        ICredential auth = new GlobalCredentials()
                .withAk(ak)
                .withSk(sk);

        SmsClient client = SmsClient.newBuilder()
                .withCredential(auth)
                .withHttpConfig(config)
                .withRegion(SmsRegion.valueOf("ap-southeast-1"))
                .build();
        CreateTemplateRequest request = new CreateTemplateRequest();
        CreateTemplateReq body = new CreateTemplateReq();
        List<TemplateDisk> listTemplateDisk = new ArrayList<>();
        listTemplateDisk.add(
            new TemplateDisk()
        );
        List<SgObject> listTemplateSecurityGroups = new ArrayList<>();
        List<Nics> listTemplateNics = new ArrayList<>();
        TemplateRequest templatebody = new TemplateRequest();
        templatebody.withName("mesh_test")
            .withIsTemplate(true)
            .withRegion("cn-north-1")
            .withProjectid("0549615a7f00264d2fd7c003a74b8a4b")
            // .withVpc()
            .withNics(listTemplateNics)
            .withSecurityGroups(listTemplateSecurityGroups)
            //.withPublicip()
            
            .withDisk(listTemplateDisk);
        body.withTemplate(templatebody);
        request.withBody(body);
        try {
            CreateTemplateResponse response = client.createTemplate(request);
            System.out.println(response.toString());
        } catch (ConnectionException e) {
            e.printStackTrace();
        } catch (RequestTimeoutException e) {
            e.printStackTrace();
        } catch (ServiceResponseException e) {
            e.printStackTrace();
            System.out.println(e.getHttpStatusCode());
            System.out.println(e.getErrorCode());
            System.out.println(e.getErrorMsg());
        }
    }
}