
package com.gcloud.mesh.dcs.entity;

import java.util.Date;

import com.gcloud.framework.db.jdbc.annotation.ID;
import com.gcloud.framework.db.jdbc.annotation.Table;

import com.gcloud.framework.db.jdbc.annotation.TableField;
import lombok.Data;

@Table(name = "dcs_scheduler_steps")
@Data
public class SchedulerStepEntity {

    @ID
    private String id;
    private String schedulerJobId;
    private Integer status;
    private String name;
    private Date beginTime;
    private Date endTime;
    private String schedulerType;
    private String attach;
    @TableField(persistence=false)
    private Integer stepNo;
    private String processClass;
    private Boolean syncExec;

}
