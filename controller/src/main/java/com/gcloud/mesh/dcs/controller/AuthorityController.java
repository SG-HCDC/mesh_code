package com.gcloud.mesh.dcs.controller;

import com.gcloud.mesh.dcs.annotation.ParamSecurity;
import com.gcloud.mesh.dcs.service.AuthorityService;
import com.gcloud.mesh.header.msg.BaseReplyMsg;
import com.gcloud.mesh.header.msg.dcs.ListByClassificationMsg;
import com.gcloud.mesh.header.msg.dcs.SetAuthorityMsg;
import com.gcloud.mesh.header.vo.dcs.AuthorityItemVo;
import com.gcloud.mesh.header.vo.dcs.AuthorityVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.jeecg.common.aspect.annotation.AutoLog;
import org.jeecg.common.constant.CommonConstant;
import org.jeecg.common.constant.enums.SysLogOperateType;
import org.jeecg.common.constant.enums.SysLogType;
import org.jeecg.common.system.api.ISysBaseAPI;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;

@Api(tags = "权限管理")
@RestController
@RequestMapping("/authority")
public class AuthorityController {

	@Autowired
	private AuthorityService service;
	@Autowired
	private ISysBaseAPI sysBaseAPI;

//	@ApiOperation(value = "权限管理 - 启用", notes = "数据安全 - 启用")
//	@RequestMapping(value = "/enabled", method = RequestMethod.POST)
//    @RequiresPermissions(value = {"authorityEnabled"})
//    public void enabled(SetEnabledMsg msg) throws Exception {
//		service.enabled(msg);
//    }

//	@ApiOperation(value = "权限管理 - 禁用", notes = "权限管理 - 禁用")
//	@RequestMapping(value = "/disabled", method = RequestMethod.POST)
//    @RequiresPermissions(value = {"authorityDisabled"})
//    public void disabled(SetEnabledMsg msg) throws Exception {
//		service.disabled(msg);
//    }
	
	//处理Mass Assignment: Insecure Binder Configuration
	@InitBinder
	public void populateCustomerRequest(WebDataBinder binder) {
	    binder.setDisallowedFields(new String[]{});
	}

    // @AutoLog(value = "权限管理-列表",operateType = CommonConstant.OPERATE_TYPE_LIST)
	@ApiOperation(value = "查看权限管理列表", notes = "权限管理 - 列表")
	@RequestMapping(value = "/listByClassification", method = RequestMethod.POST)
    @RequiresPermissions(value = {"authorityListByClassification"})
    @ParamSecurity(enabled = true)
	public BaseReplyMsg listByClassification(@Valid ListByClassificationMsg msg) throws Exception {
		BaseReplyMsg reply = new BaseReplyMsg();
		List<AuthorityVo> res = service.listByClassification(msg);
		if(msg != null) {
			if("iaas".equals(msg.getClassification())) {
				sysBaseAPI.addLog("查看L1层：基础设施权限", SysLogType.OPERATE, SysLogOperateType.DETAIL, null, null,"精确管控与供电制冷联动-基础资源细粒度感知-权限管理-L1层：基础设施");
			}else if("device".equals(msg.getClassification())) {
				sysBaseAPI.addLog("查看L2层：IT设备权限", SysLogType.OPERATE, SysLogOperateType.DETAIL, null, null,"精确管控与供电制冷联动-基础资源细粒度感知-权限管理-L2层：IT设备");
			}else if("cloud".equals(msg.getClassification())) {
				sysBaseAPI.addLog("查看L3层：云平台资源权限", SysLogType.OPERATE, SysLogOperateType.DETAIL, null, null,"精确管控与供电制冷联动-基础资源细粒度感知-权限管理-L3层：云平台资源");
			}else if("app".equals(msg.getClassification())) {
				sysBaseAPI.addLog("查看L4层：应用资源权限", SysLogType.OPERATE, SysLogOperateType.DETAIL, null, null,"精确管控与供电制冷联动-基础资源细粒度感知-权限管理-L4层：应用资源");
			}
		}else {
			sysBaseAPI.addLog("权限管理-列表", null, CommonConstant.OPERATE_TYPE_LIST);
		}
		reply.setData(res);
		return reply;
	}

    // @AutoLog(value = "设置权限",operateType = CommonConstant.OPERATE_TYPE_CONFIG)
	@ApiOperation(value = "权限管理 - 设置权限", notes = "权限管理 - 设置权限")
	@RequestMapping(value = "/set1", method = RequestMethod.POST)
    @RequiresPermissions(value = {"authorityDisabled"})
    @ParamSecurity(enabled = true)
	public void set(@Valid SetAuthorityMsg msg) throws Exception {
		String module = "";
		if(msg.getItems() != null) {
			for(AuthorityItemVo vo : msg.getItems()) {
				if("air_condition".equals(vo.getType()) || "distribution_box".equals(vo.getType()) || "ups".equals(vo.getType())) {
					module = "精确管控与供电制冷联动-基础资源细粒度感知-权限管理-L1层：基础设施";
				}else if("server".equals(vo.getType()) || "switcher".equals(vo.getType())) {
					module = "精确管控与供电制冷联动-基础资源细粒度感知-权限管理-L2层：IT设备";
				}else if("k8s".equals(vo.getType()) || "vm".equals(vo.getType())) {
					module = "精确管控与供电制冷联动-基础资源细粒度感知-权限管理-L3层：云平台资源";
				}else if("app".equals(vo.getType())) {
					module = "精确管控与供电制冷联动-基础资源细粒度感知-权限管理-L4层：应用资源";
				}
			}
		}
		service.set(msg);
    	StringBuffer buff = new StringBuffer();
    	buff.append("设置权限");
    	buff.append("，"+msg.toString());
    	sysBaseAPI.addLog(buff.toString(), null, SysLogOperateType.UPDATE, null, null ,module);
	}

}
