package com.gcloud.mesh.dcs.enums;

public enum SchedulerAdviceStatus {

	READY(0, "ready", "准备中"), TOSCH(1, "tosch", "待迁移"), HASSCH(3,"hassch", "迁移中");

	private Integer value;
	private String name;
	private String cnName;

	SchedulerAdviceStatus(Integer value, String name, String cnName) {
		this.value = value;
		this.name = name;
		this.cnName = cnName;
	}

	public Integer getValue() {
		return value;
	}

	public String getName() {
		return name;
	}

	public String getCnName() {
		return cnName;
	}

}
