package com.gcloud.mesh.dcs.strategy;

import com.gcloud.mesh.dcs.entity.AppEntity;
import com.gcloud.mesh.header.vo.dcs.DescribeMetricVO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Slf4j
@Component
public class ServerMigrationStrategyContext {
    private Map<String,ServerMigrationStrategy> serverMigrationStrategy = new HashMap<String,ServerMigrationStrategy>();

    public ServerMigrationStrategyContext(List<ServerMigrationStrategy> strategys){
        strategys.forEach(strategy -> {
            serverMigrationStrategy.put(strategy.getStrategyName(), strategy);
        });
    }

    public Object exec(String strategyName, Serializable p){
        return serverMigrationStrategy.get(strategyName).doMigration(p);
    };

    public Boolean checkStatus(String strategyName,Integer stepNo,String attach){
        return serverMigrationStrategy.get(strategyName).checkStatus(stepNo,attach);
    }

    public List<AppEntity> queryServerList(String strategyName,String cloudResourceId){
        return serverMigrationStrategy.get(strategyName).queryServerList(cloudResourceId);
    };
    public List<DescribeMetricVO> queryDescribeMetricLastList(String strategyName,String datacenterId){
        return serverMigrationStrategy.get(strategyName).queryDescribeMetricLastList(datacenterId);
    };

}
