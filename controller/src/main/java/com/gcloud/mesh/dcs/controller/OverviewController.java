
package com.gcloud.mesh.dcs.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.gcloud.framework.db.PageResult;
import com.gcloud.mesh.asset.service.IAssetService;
import com.gcloud.mesh.asset.service.ICloudResourceService;
import com.gcloud.mesh.dcs.annotation.ParamSecurity;
import com.gcloud.mesh.dcs.service.AppService;
import com.gcloud.mesh.header.msg.asset.PageCloudResourceMsg;
import com.gcloud.mesh.header.msg.asset.PageDeviceMsg;
import com.gcloud.mesh.header.msg.asset.PageNodeMsg;
import com.gcloud.mesh.header.msg.dcs.PageAppMsg;
import com.gcloud.mesh.header.msg.dcs.PageVmMsg;
import com.gcloud.mesh.header.vo.asset.CloudResourceItemVo;
import com.gcloud.mesh.header.vo.asset.DatacenterItemVo;
import com.gcloud.mesh.header.vo.asset.DeviceItemVo;
import com.gcloud.mesh.header.vo.asset.NodeItemVo;
import com.gcloud.mesh.header.vo.dcs.AppItemVo;
import com.gcloud.mesh.header.vo.dcs.ComputeOverview;
import com.gcloud.mesh.header.vo.dcs.VmItemVo;

@RestController
@RequestMapping("/compute")
public class OverviewController {
	@Autowired
	private AppService appService;
	@Autowired
	private ICloudResourceService cloudResourceService;
	@Autowired
	private IAssetService assetService;
	
	private String userId = null;
	
	@RequestMapping(value = "overview", method = RequestMethod.POST)
	@ParamSecurity(enabled = true)
	public ComputeOverview overview(String datacenterId){
		ComputeOverview overview = new ComputeOverview();
		overview.setK8sCount(0);
		overview.setNodeCount(0);
		overview.setVmCount(0);
		PageVmMsg vmMsg = new PageVmMsg();
		vmMsg.setDatacenterId(datacenterId);
		PageResult<VmItemVo>  vmPage = appService.vmpage(vmMsg);
		overview.setVmCount(vmPage.getTotalCount());
		
		
		PageAppMsg appMsg = new PageAppMsg();
		appMsg.setDatacenterId(datacenterId);
		PageResult<AppItemVo> appPage = appService.page(appMsg);
		overview.setAppCount(appPage.getTotalCount());
		
		
		PageCloudResourceMsg cloudResourceMsg = new PageCloudResourceMsg();
		cloudResourceMsg.setType(1);
		cloudResourceMsg.setDatacenterId(datacenterId);
		PageResult<CloudResourceItemVo> clusterPage = cloudResourceService.page(cloudResourceMsg);
		overview.setK8sCount(clusterPage.getTotalCount());
		
		PageCloudResourceMsg cloudResourceMsg2 = new PageCloudResourceMsg();
		cloudResourceMsg2.setDatacenterId(datacenterId);
		PageResult<CloudResourceItemVo> cloudResourcePage = cloudResourceService.page(cloudResourceMsg2);
		overview.setCloudResourceCount(cloudResourcePage.getTotalCount());
		
		PageNodeMsg msg = new PageNodeMsg();
		msg.setDatacenterId(datacenterId);
		PageResult<NodeItemVo> Nodepage = assetService.pageNode(msg.getPageNo(), msg.getPageSize(), msg.getDatacenterId(), null, msg.getOrder(), userId);
		overview.setNodeCount(Nodepage.getTotalCount());
		
		
		PageDeviceMsg devmsg = new PageDeviceMsg();
		devmsg.setDatacenterId(datacenterId);
		PageResult<DeviceItemVo> devicePage = assetService.pageDevice(devmsg.getPageNo(), devmsg.getPageSize(), datacenterId, null, null, null);
		overview.setIaasCount(devicePage.getTotalCount());
		
		PageResult<DatacenterItemVo> datacenterPage = assetService.pageDatacenter(10, 10, null, null, userId);
		overview.setDatacenterCount(datacenterPage.getTotalCount());
		
		
		PageResult<DeviceItemVo> its = assetService.pageItDevice(10, 10, datacenterId, null,null);
		overview.setItCount(its.getTotalCount());
		return overview;
	}



}
