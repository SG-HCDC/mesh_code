package com.gcloud.mesh.dcs.task;

import java.util.List;

import org.jeecg.common.util.RedisUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.alibaba.fastjson.JSONObject;
import com.gcloud.mesh.dcs.strategy.ServerMigrationStrategyContext;
import com.gcloud.mesh.header.vo.dcs.DescribeMetricVO;
import com.gcloud.mesh.supplier.dao.SupplierDao;
import com.gcloud.mesh.supplier.entity.SupplierEntity;

import lombok.extern.slf4j.Slf4j;


/**
 * 每分钟同步vm 使用情况
 */
@Slf4j
//@Component
public class SyncVmUsageTask {


	@Autowired
	private RedisUtil redisUtil;

	@Autowired
	private ServerMigrationStrategyContext serverMigrationStrategyContext;

	@Autowired
	private SupplierDao supplierDao;



	@Scheduled(fixedDelay = 1000 * 55)
	public void work() {
		try {
			log.debug("[Scheduler] 同步vm 使用情况时器开始");
			List<SupplierEntity> supplierEntities = supplierDao.findAll();
			if (supplierEntities == null || supplierEntities.isEmpty()) return;
			for (SupplierEntity supplierEntity : supplierEntities) {
				log.debug("[Scheduler] 同步 "+supplierEntities.size()+" 个数据中心的使用情况");
				List<DescribeMetricVO> describeMetricVOS = serverMigrationStrategyContext.queryDescribeMetricLastList(supplierEntity.getType(), supplierEntity.getDatacenterId());
				redisUtil.set("job:vmusage:"+supplierEntity.getDatacenterId(), JSONObject.toJSONString(describeMetricVOS));
			}
		}finally {
			log.debug("[Scheduler] 同步vm 使用情况时器结束");
		}
	}

}
