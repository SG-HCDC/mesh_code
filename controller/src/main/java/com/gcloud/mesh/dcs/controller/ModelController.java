
package com.gcloud.mesh.dcs.controller;

import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import com.gcloud.framework.db.PageResult;
import com.gcloud.mesh.dcs.annotation.ParamSecurity;
import com.gcloud.mesh.dcs.service.ModelService;
import com.gcloud.mesh.header.msg.dcs.*;
import com.gcloud.mesh.header.vo.dcs.AppDatacenterModelScoreVo;
import com.gcloud.mesh.header.vo.dcs.ModelFactorVo;
import com.gcloud.mesh.header.vo.dcs.PageAppModelScoreVo;
import com.gcloud.mesh.header.vo.dcs.PageDeviceScoreVo;
import com.gcloud.mesh.header.vo.dcs.PageModelScoreVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.jeecg.common.aspect.annotation.AutoLog;
import org.jeecg.common.constant.CommonConstant;
import org.jeecg.common.constant.enums.SysLogOperateType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.jeecg.common.system.api.ISysBaseAPI;

import javax.validation.Valid;
import java.util.List;

@Api(tags = "最优模型")
@RestController
@RequestMapping("/model")
@Slf4j
public class ModelController {

	@Autowired
	private ModelService factorService;

	@Autowired
	private ISysBaseAPI sysBaseAPI;
	
	//处理Mass Assignment: Insecure Binder Configuration
	@InitBinder
	public void populateCustomerRequest(WebDataBinder binder) {
	    binder.setDisallowedFields(new String[]{});
	}

    // @AutoLog(value = "模型因子设置",operateType = CommonConstant.OPERATE_TYPE_CONFIG)
	@ApiOperation(value = "模型因子设置", notes = "模型因子设置")
	@RequestMapping(value = "/setModelFactors", method = RequestMethod.POST)
	@RequiresPermissions(value = {"modelSetModelFactors"})
    @ParamSecurity(enabled = true)
	public void setModelFactors(@Valid SetModelFactorsMsg msg) throws Exception {
		factorService.setModelFactors(msg);
		StringBuffer buff = new StringBuffer();
    	buff.append("模型因子设置，");
    	buff.append("");
    	buff.append(msg.toString());
    	// buff.append("，"+msg.toString());
    	if(StringUtils.isNotBlank(msg.getTitle())) {
    		sysBaseAPI.addLog(buff.toString(), null, SysLogOperateType.UPDATE, null, null, msg.getTitle());
    	}else {
    		if("AIR".equals(msg.getModelType())) {
    			sysBaseAPI.addLog(buff.toString(), null, SysLogOperateType.UPDATE, null, null, "精确管控与供电制冷联动-供电制冷最优模型-供电制冷设备指标管理-制冷设备最有成本模型指标设置");
    		}else {
    			sysBaseAPI.addLog(buff.toString(), null, SysLogOperateType.UPDATE, null, null, "精确管控与供电制冷联动-供电制冷最优模型-供电制冷设备指标管理-供电设备最有成本模型指标设置");
    		}
    		
    	}
		
	}

    // @AutoLog(value = "查看模型因子",operateType = CommonConstant.OPERATE_TYPE_DETAIL)
	@ApiOperation(value = "模型因子展示", notes = "模型因子展示")
	@RequestMapping(value = "/getModelFactors", method = RequestMethod.POST)
	@RequiresPermissions(value = {"modelGetModelFactors"})
    @ParamSecurity(enabled = true)
	public List<ModelFactorVo> getModelFactors(@Valid GetModelFactorsMsg msg) throws Exception {
		
		if(StringUtils.isNotBlank(msg.getTitle())) {
			sysBaseAPI.addLog(msg.getContent() != null?msg.getContent():"查看模型因子", null, SysLogOperateType.DETAIL, null, null, msg.getTitle());
		}else {
			if("AIR".equals(msg.getModelType())) {
				sysBaseAPI.addLog(msg.getContent() != null?msg.getContent():"查看模型因子", null, SysLogOperateType.DETAIL, "精确管控与供电制冷联动-供电制冷最优模型-供电制冷设备指标管理-制冷设备最有成本模型指标设置");
			}else {
				sysBaseAPI.addLog(msg.getContent() != null?msg.getContent():"查看模型因子", null, SysLogOperateType.DETAIL, "精确管控与供电制冷联动-供电制冷最优模型-供电制冷设备指标管理-供电设备最有成本模型指标设置");
			}
	  }
		return factorService.getModelFactors(msg);
	}
    // @AutoLog(value = "查询最优模型评分设备列表",operateType = CommonConstant.OPERATE_TYPE_LIST)
	@ApiOperation(value = "最优模型评分 - 设备列表", notes = "最优模型评分 - 设备列表")
	@RequestMapping(value = "/pageModelScore", method = RequestMethod.POST)
	@RequiresPermissions(value = {"modelPageModelScore"})
    @ParamSecurity(enabled = true)
	public PageResult<PageModelScoreVo> pageModelScore(@Valid PageModelScoreMsg msg) throws Exception {
		if(StringUtils.isNotBlank(msg.getTitle())) {
			sysBaseAPI.addLog(msg.getContent() != null?msg.getContent():"设备模型评分列表", null, SysLogOperateType.QUERY, null, null, msg.getTitle());
		}else {
			if(msg.getModelType()!= null && msg.getModelType().equals("UPS")) {
				sysBaseAPI.addLog(msg.getContent() != null?msg.getContent():"查询供电设备最优成本模型", null, SysLogOperateType.QUERY, null, null, "精确管控与供电制冷联动-供电制冷最优模型-设备最优成本模型总览-供电设备最优成本模型");
			}else if(msg.getModelType()!= null && msg.getModelType().equals("AIR")) {
				sysBaseAPI.addLog(msg.getContent() != null?msg.getContent():"查询制冷设备最优成本模型", null, SysLogOperateType.QUERY, null, null, "精确管控与供电制冷联动-供电制冷最优模型-设备最优成本模型总览-制冷设备最优成本模型");
			}
		}
		return factorService.pageModelScore(msg);
	}

//    @AutoLog(value = "最优模型评分-评分详情",operateType = CommonConstant.OPERATE_TYPE_DETAIL)
	@ApiOperation(value = "查看最优模型评分评分详情", notes = "最优模型评分 - 评分详情")
	@RequestMapping(value = "/detailModelScore", method = RequestMethod.POST)
	@RequiresPermissions(value = {"modelDetailModelScore"})
	@ParamSecurity(enabled = true)
	public DetailModelScoreReplyMsg detailModelScore(@Valid DetailModelScoreMsg msg) throws Exception {
		if(StringUtils.isNotBlank(msg.getTitle())) {
			sysBaseAPI.addLog("查看最优模型评分详情；ID:" + msg.getResourceId(), null, SysLogOperateType.DETAIL, null, null, msg.getTitle());
		}else {
			sysBaseAPI.addLog("查看迁移代价信息；ID:" + msg.getResourceId(), null, SysLogOperateType.DETAIL, null, null, "离线业务多数据中心迁移-离线业务异步迁移代价-代价展示-异步迁移代价计算结果展示");
		}
		
		return factorService.detailModelScore(msg);
	}
	
	
	@ApiOperation(value = "数据中心评分列表", notes = "数据中心评分列表")
	@RequestMapping(value = "/datacenterModelScore", method = RequestMethod.POST)
	public List<DetailModelScoreReplyMsg> datacenterModelScore(@Valid DatacenterModelScoreMsg msg) throws Exception {
		return factorService.datacenterModelScore(msg);
	}
	

   //  @AutoLog(value = "查询最优模型评分服务器列表",operateType = CommonConstant.OPERATE_TYPE_LIST)
	@ApiOperation(value = "最优模型评分 - 服务器列表", notes = "最优模型评分 - 服务器列表")
	@RequestMapping(value = "/pageDeviceScore", method = RequestMethod.POST)
	@RequiresPermissions(value = {"modelPageDeviceScore"})
    @ParamSecurity(enabled = true)
	public PageResult<PageDeviceScoreVo> pageDeviceScore(@Valid PageModelScoreMsg msg) throws Exception {
		if(StringUtils.isNotBlank(msg.getTitle())) {
			sysBaseAPI.addLog("查询最优模型评分服务器列表", null, SysLogOperateType.QUERY, null, null, msg.getTitle());
		}else {
			sysBaseAPI.addLog("查询最优模型评分服务器列表", null, SysLogOperateType.QUERY);
		}
    	return factorService.pageDeviceScore(msg);
	}

    // @AutoLog(value = "查看最优模型评分-最优评估",operateType=CommonConstant.OPERATE_TYPE_DETAIL)
	@ApiOperation(value = "最优模型评分 - 最优评估", notes = "最优模型评分 - 最优评估")
	@RequestMapping(value = "/bestPredict", method = RequestMethod.POST)
	@RequiresPermissions(value = {"modelBestPredict"})
    @ParamSecurity(enabled = true)
	public ModelBestPredictReplyMsg bestPredict(@Valid ModelBestPredictMsg msg) throws Exception {
		if(StringUtils.isNotBlank(msg.getTitle())) {
			if("OPTIMAL_COST".equals(msg.getModelType())) {
				sysBaseAPI.addLog(msg.getContent() != null ?msg.getContent():"查看最优策略评估算法", null, SysLogOperateType.DETAIL, null, null, msg.getTitle());
			}else if("OPTIMAL_DEVICE".equals(msg.getModelType())) {
				sysBaseAPI.addLog(msg.getContent() != null ?msg.getContent():"查看最优策略评估算法", null, SysLogOperateType.DETAIL,null, null, msg.getTitle());
			}
		}else {
			if("OPTIMAL_COST".equals(msg.getModelType())) {
				sysBaseAPI.addLog(msg.getContent() != null ?msg.getContent():"查看最优成本策略评估算法", null, SysLogOperateType.DETAIL);
			}else if("OPTIMAL_DEVICE".equals(msg.getModelType())) {
				sysBaseAPI.addLog(msg.getContent() != null ?msg.getContent():"查看最优设备策略评估算法", null, SysLogOperateType.DETAIL);
			}	
			sysBaseAPI.addLog(msg.getContent() != null ?msg.getContent():"查看最优策略评估算法", null, SysLogOperateType.DETAIL,null, null, msg.getTitle());
		}
    	return factorService.bestPredict(msg);
	}
	@ApiOperation(value = "最优应用模型评分", notes = "最优应用模型评分 ")
	@RequestMapping(value = "/pageAppModelScore", method = RequestMethod.POST)
	public PageResult<PageAppModelScoreVo> pageModelScore(@Valid PageAppModelScoreMsg msg) throws Exception {
		return factorService.pageAppModelScore(msg);
	}
	
	
	@ApiOperation(value = "数据中心模型评分", notes = "数据中心模型评分 ")
	@RequestMapping(value = "/appDatacenterModelScore", method = RequestMethod.POST)
	public AppDatacenterModelScoreVo appDatacenterModelScore(@Valid AppDatacenterModelScoreMsg msg) throws Exception {
		return factorService.appDatacenterModelScore(msg);
	}
	
	
}
