package com.gcloud.mesh.dcs.entity;

import com.gcloud.framework.db.jdbc.annotation.ID;
import com.gcloud.framework.db.jdbc.annotation.Table;
import lombok.Data;
import lombok.experimental.Accessors;

import java.util.Date;

@Table(name = "dcs_ali_power_nodes")
@Data
@Accessors(chain = true)
public class AliPowerEntity {
    @ID
    private String id;
    private String esn;
    private String deviceModel;
    private String deviceManufacturer;
    private String datacenterId;
    private Date createTime;
    private String mgmIp;
    private String ipmiIp;
    private String hostname;
    private Integer powerStatus;
    private Float powerConsumption;
}
