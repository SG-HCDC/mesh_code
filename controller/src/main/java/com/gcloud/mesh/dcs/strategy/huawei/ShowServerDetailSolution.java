package com.gcloud.mesh.dcs.strategy.huawei;



import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.exception.ConnectionException;
import com.huaweicloud.sdk.core.exception.RequestTimeoutException;
import com.huaweicloud.sdk.core.exception.ServiceResponseException;
import com.huaweicloud.sdk.core.http.HttpConfig;
import com.huaweicloud.sdk.ecs.v2.*;
import com.huaweicloud.sdk.ecs.v2.region.EcsRegion;
import com.huaweicloud.sdk.ecs.v2.model.*;


public class ShowServerDetailSolution {

    public static void main(String[] args) {
    	ShowServerDetailSolution show = new ShowServerDetailSolution();
    	ShowServerResponse resp = show.showServer("8QVISVT4XJHUNMIEPZBQ", "GingsSlHInSkQ3x47jMSEjFT0uumSJgplZs926Cw", "cn-south-1", "82326c7b-b071-44b6-a770-d26484e5124b");
    	System.out.println(resp);
    }
    public ShowServerResponse showServer(String ak, String sk, String regionId, String appId) {
  	    HttpConfig config = HttpConfig.getDefaultHttpConfig();
  	    config.withIgnoreSSLVerification(true);
        ICredential auth = new BasicCredentials()
                .withAk(ak)
                .withSk(sk);

        EcsClient client = EcsClient.newBuilder()
        		.withHttpConfig(config)
                .withCredential(auth)
                .withRegion(EcsRegion.valueOf(regionId))
                .build();
        ShowServerRequest request = new ShowServerRequest();
        request.withServerId(appId);
        ShowServerResponse response = null;
        try {
            response = client.showServer(request);
            //System.out.println(response.toString());
        } catch (ConnectionException e) {
            e.printStackTrace();
        } catch (RequestTimeoutException e) {
            e.printStackTrace();
        } catch (ServiceResponseException e) {
            e.printStackTrace();

        }
        return response;
    }
}