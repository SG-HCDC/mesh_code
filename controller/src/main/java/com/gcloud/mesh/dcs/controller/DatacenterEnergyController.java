package com.gcloud.mesh.dcs.controller;

import com.gcloud.framework.db.PageResult;
import com.gcloud.mesh.dcs.annotation.ParamSecurity;
import com.gcloud.mesh.dcs.service.DatacenterEnergyService;
import com.gcloud.mesh.header.msg.BaseReplyMsg;
import com.gcloud.mesh.header.msg.dcs.ReportByMonthMsg;
import com.gcloud.mesh.header.msg.dcs.ReportByQuarterMsg;
import com.gcloud.mesh.header.msg.dcs.ReportByYearMsg;
import com.gcloud.mesh.header.msg.dcs.StatisticByMonthMsg;
import com.gcloud.mesh.header.msg.dcs.StatisticByQuarterMsg;
import com.gcloud.mesh.header.msg.dcs.StatisticByYearMsg;
import com.gcloud.mesh.header.vo.dcs.AppItemVo;
import com.gcloud.mesh.header.vo.dcs.DatacenterEnergyVo;
import com.gcloud.mesh.header.vo.dcs.DatacenterReportVo;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.jeecg.common.aspect.annotation.AutoLog;
import org.jeecg.common.constant.CommonConstant;
import org.jeecg.common.constant.enums.SysLogOperateType;
import org.jeecg.common.constant.enums.SysLogType;
import org.jeecg.common.system.api.ISysBaseAPI;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

import javax.validation.Valid;

@Api(tags = "数据中心统计")
@RestController
@RequestMapping("/dcEnergy")
public class DatacenterEnergyController {
	@Autowired
	private ISysBaseAPI sysBaseAPI;
	@Autowired
	private DatacenterEnergyService service;
	
	//处理Mass Assignment: Insecure Binder Configuration
	@InitBinder
	public void populateCustomerRequest(WebDataBinder binder) {
	    binder.setDisallowedFields(new String[]{});
	}

    // @AutoLog(value = "查看数据中心统计-年度能耗统计",operateType= CommonConstant.OPERATE_TYPE_DETAIL, module = "系统管理-统计报表-数据中心统计-年度能耗统计")
	@ApiOperation(value = "数据中心统计 - 年度能耗统计", notes = "数据中心统计 - 年度能耗统计")
	@RequestMapping(value = "/statisticsByYear", method = RequestMethod.POST)
//	@RequiresPermissions(value = {"dcEnergyStatisticsByYear"})
    @ParamSecurity(enabled = true)
	public BaseReplyMsg statisticsByYear(@Valid StatisticByYearMsg msg) throws Exception {
		BaseReplyMsg reply = new BaseReplyMsg();
		DatacenterEnergyVo vo = service.statisticsByYear(msg);
		sysBaseAPI.addLog((msg.getContent() != null ? msg.getContent():"查看数据中心统计-年度能耗统计")+msg.toString(), SysLogType.OPERATE, SysLogOperateType.DETAIL, null ,null, "系统管理-统计报表-数据中心统计-年度能耗统计");
		reply.setData(vo);
		return reply;
	}
   //  @AutoLog(value = "查看数据中心统计-季度能耗统计",operateType=CommonConstant.OPERATE_TYPE_DETAIL, module = "系统管理-统计报表-数据中心统计-季度能耗统计")
	@ApiOperation(value = "数据中心统计 - 季度能耗统计", notes = "数据中心统计 - 季度能耗统计")
	@RequestMapping(value = "/statisticsByQuarter", method = RequestMethod.POST)
//	@RequiresPermissions(value = {"dcEnergyStatisticsByQuarter"})
    @ParamSecurity(enabled = true)
	public BaseReplyMsg statisticsByQuarter(@Valid StatisticByQuarterMsg msg) throws Exception {
		BaseReplyMsg reply = new BaseReplyMsg();
		DatacenterEnergyVo vo = service.statisticsByQuarter(msg);
		sysBaseAPI.addLog((msg.getContent() != null ? msg.getContent():"查看数据中心统计-季度能耗统计")+msg.toString(), SysLogType.OPERATE, SysLogOperateType.DETAIL, null ,null, "系统管理-统计报表-数据中心统计-季度能耗统计");
		// sysBaseAPI.addLog((msg.getContent() != null ? msg.getContent():"查看数据中心统计-年度能耗统计")+msg.toString(), null, CommonConstant.OPERATE_TYPE_DETAIL);
		reply.setData(vo);
		return reply;
	}

   //  @AutoLog(value = "查看数据中心统计-月度能耗统计",operateType=CommonConstant.OPERATE_TYPE_DETAIL, module = "系统管理-统计报表-数据中心统计-月度能耗统")
	@ApiOperation(value = "数据中心统计 - 月度能耗统计", notes = "数据中心统计 - 月度能耗统计")
	@RequestMapping(value = "/statisticsByMonth", method = RequestMethod.POST)
//	@RequiresPermissions(value = {"dcEnergyStatisticsByMonth"})
    @ParamSecurity(enabled = true)
	public BaseReplyMsg statisticsByMonth(@Valid StatisticByMonthMsg msg) throws Exception {
		BaseReplyMsg reply = new BaseReplyMsg();
		DatacenterEnergyVo vo = service.statisticsByMonth(msg);
		sysBaseAPI.addLog((msg.getContent() != null ? msg.getContent():"查看数据中心统计-月度能耗统计")+msg.toString(), SysLogType.OPERATE, SysLogOperateType.DETAIL, null ,null, "系统管理-统计报表-数据中心统计-月度能耗统");
		
		reply.setData(vo);
		return reply;
	}
    
   //  @AutoLog(value = "查看数据中心报表-年度能耗统计报表",operateType= CommonConstant.OPERATE_TYPE_LIST, module = "系统管理-统计报表-数据中心报表-年度能耗统计报表")
	@ApiOperation(value = "数据中心报表-年度能耗统计报表", notes = "数据中心报表-年度报表统计")
	@RequestMapping(value = "/reportByYear", method = RequestMethod.POST)
//	@RequiresPermissions(value = {"dcEnergyReportByYear"})
    @ParamSecurity(enabled = true)
	public PageResult<DatacenterReportVo> reportByYear(@Valid ReportByYearMsg msg) throws Exception {
    	sysBaseAPI.addLog((msg.getContent() != null ? msg.getContent():"查看数据中心报表-年度能耗统计报表")+msg.toString(), SysLogType.OPERATE, SysLogOperateType.QUERY, null ,null, "系统管理-统计报表-数据中心报表-年度能耗统计报表");
    	return service.reportByYear(msg);
	}
    //@AutoLog(value = "查看数据中心报表-季度能耗统计报表",operateType=CommonConstant.OPERATE_TYPE_LIST, module = "系统管理-统计报表-数据中心报表-季度能耗统计报表")
	@ApiOperation(value = "数据中心报表-季度能耗统计报表", notes = "数据中心报表-季度报表统计")
	@RequestMapping(value = "/reportByQuarter", method = RequestMethod.POST)
//	@RequiresPermissions(value = {"dcEnergyReportByQuarter"})
    @ParamSecurity(enabled = true)
	public PageResult<DatacenterReportVo> reportByQuarter(@Valid ReportByQuarterMsg msg) throws Exception {
    	sysBaseAPI.addLog((msg.getContent() != null ? msg.getContent():"查看数据中心报表-季度能耗统计报表")+msg.toString(), SysLogType.OPERATE, SysLogOperateType.QUERY, null ,null, "系统管理-统计报表-数据中心报表-季度能耗统计报表");
    	return service.reportByQuarter(msg);
	}

    // @AutoLog(value = "查看数据中心报表-月度能耗统计报表",operateType=CommonConstant.OPERATE_TYPE_LIST, module = "系统管理-统计报表-数据中心报表-月度能耗统计报表")
	@ApiOperation(value = "数据中心报表-月度能耗统计报表", notes = "数据中心报表-月度报表统计")
	@RequestMapping(value = "/reportByMonth", method = RequestMethod.POST)
//	@RequiresPermissions(value = {"dcEnergyReportByMonth"})
    @ParamSecurity(enabled = true)
	public PageResult<DatacenterReportVo> reportByMonth(@Valid ReportByMonthMsg msg) throws Exception {
    	sysBaseAPI.addLog((msg.getContent() != null ? msg.getContent():"查看数据中心报表-月度能耗统计报表")+msg.toString(), SysLogType.OPERATE, SysLogOperateType.QUERY, null ,null, "系统管理-统计报表-数据中心报表-月度能耗统计报表");
    	return service.reportByMonth(msg);
	}


}
