package com.gcloud.mesh.dcs.controller;

import com.gcloud.mesh.sdk.GceSDK;
import org.apache.commons.lang.StringUtils;
import org.jeecg.common.exception.ParamException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.gcloud.mesh.dcs.annotation.ParamSecurity;
import com.gcloud.mesh.dcs.service.ClusterService;
import com.gcloud.mesh.header.msg.BaseReplyMsg;
import com.gcloud.mesh.header.msg.dcs.ImportClusterMsg;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@Api(tags = "集群管理")
@RequestMapping("/cluster")
@RestController
public class ClusterController {

    @Autowired
    ClusterService clusterService;

    @Autowired
    GceSDK gceSDK;

    @ApiOperation(value = "集群列表", notes = "集群列表")
    @GetMapping("/queryList")
    @ParamSecurity(enabled = true)
    public BaseReplyMsg queryList(String datacenterId) throws Exception {
    	 if(StringUtils.isBlank(datacenterId)) throw new ParamException("datacenterId不能为空");
         return new BaseReplyMsg().setData(clusterService.queryList(datacenterId));
    }

    @ApiOperation(value = "接入国网", notes = "接入国网")
    @PostMapping("/importCluster")
    @ParamSecurity(enabled = true)
    public BaseReplyMsg importCluster(@RequestBody ImportClusterMsg importClusterMsg) throws Exception {
        return new BaseReplyMsg().setData(clusterService.importCluster(importClusterMsg));
    }

}
