
package com.gcloud.mesh.dcs.controller;

import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import com.gcloud.framework.db.PageResult;
import com.gcloud.mesh.dcs.annotation.ParamSecurity;
import com.gcloud.mesh.dcs.service.AppProfileService;
import com.gcloud.mesh.header.msg.dcs.PageFeatureMsg;
import com.gcloud.mesh.header.msg.dcs.PageSceneMsg;
import com.gcloud.mesh.header.msg.dcs.UpdateProfileFeatureChooseMsg;
import com.gcloud.mesh.header.msg.dcs.UpdateProfileSceneChooseMsg;
import com.gcloud.mesh.header.vo.asset.CountAppProfileVo;
import com.gcloud.mesh.header.vo.dcs.PageAppFeatureVo;
import com.gcloud.mesh.header.vo.dcs.PageAppProfileVo;
import com.gcloud.mesh.header.vo.dcs.PageAppSceneVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.jeecg.common.aspect.annotation.AutoLog;
import org.jeecg.common.constant.CommonConstant;
import org.jeecg.common.constant.enums.SysLogOperateType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.jeecg.common.system.api.ISysBaseAPI;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

@Api(tags = "应用画像")
@RestController
@RequestMapping("/profile")
public class AppProfileController {

	@Autowired
	private AppProfileService service;

	@Autowired
	private ISysBaseAPI sysBaseAPI;
	
	//处理Mass Assignment: Insecure Binder Configuration
	@InitBinder
	public void populateCustomerRequest(WebDataBinder binder) {
	    binder.setDisallowedFields(new String[]{});
	}

    // @AutoLog(value = "设置应用业务特点",operateType = CommonConstant.OPERATE_TYPE_CONFIG)
	@ApiOperation(value = "设置应用业务特点", notes = "设置应用业务特点")
	@RequestMapping(value = "/updateFeatureChoose", method = RequestMethod.POST)
	@RequiresPermissions(value = {"profileUpdateFeatureChoose"})
	@ParamSecurity(enabled = true)
	public void updateFeatureChoose(@Valid UpdateProfileFeatureChooseMsg msg) throws Exception {
		service.updateFeatureChoose(msg);
		sysBaseAPI.addLog("设置应用业务特点；应用ID:" + msg.getAppId()+msg.toString(), null, CommonConstant.OPERATE_TYPE_UPDATE);
	}

    // @AutoLog(value = "设置应用场景特点",operateType = CommonConstant.OPERATE_TYPE_CONFIG)
	@ApiOperation(value = "设置应用场景特点", notes = "设置应用场景特点")
	@RequestMapping(value = "/updateSceneChoose", method = RequestMethod.POST)
	@RequiresPermissions(value = {"profileUpdateSceneChoose"})
	@ParamSecurity(enabled = true)
	public void updateSceneChoose(@Valid UpdateProfileSceneChooseMsg msg) throws Exception {
		service.updateSceneChoose(msg);
		sysBaseAPI.addLog("设置应用场景特点；应用ID:" + msg.getAppId()+msg.toString(), null, CommonConstant.OPERATE_TYPE_UPDATE);
	}

    // @AutoLog(value = "查询业务应用画像业务特点管理列表",operateType = CommonConstant.OPERATE_TYPE_LIST)
	@ApiOperation(value = "应用业务特点列表", notes = "应用业务特点列表")
	@RequestMapping(value = "/pageFeature", method = RequestMethod.POST)
	@RequiresPermissions(value = {"profilePageFeature"})
    @ParamSecurity(enabled = true)
	public PageResult<PageAppFeatureVo> pageFeature(@Valid PageFeatureMsg msg) throws Exception {
		
		PageResult<PageAppFeatureVo> page =  service.pageFeature(msg);
		sysBaseAPI.addLog((msg.getContent() != null ? msg.getContent():"查询业务应用画像业务特点管理列表")+msg.toString(), null, SysLogOperateType.QUERY, null, null, msg.getTitle());
		return page;
		
	}

    // @AutoLog(value = "查询业务应用画像应用场景管理列表",operateType = CommonConstant.OPERATE_TYPE_LIST)
	@ApiOperation(value = "应用场景特点列表", notes = "应用场景特点列表")
	@RequestMapping(value = "/pageScene", method = RequestMethod.POST)
	@RequiresPermissions(value = {"profilePageScene"})
    @ParamSecurity(enabled = true)
	public PageResult<PageAppSceneVo> pageScene(@Valid PageSceneMsg msg) throws Exception {
		PageResult<PageAppSceneVo> page =  service.pageScene(msg);
		sysBaseAPI.addLog((msg.getContent() != null ? msg.getContent():"查询业务应用画像应用场景管理列表")+msg.toString(), null, SysLogOperateType.QUERY, null, null, msg.getTitle());
		return page;
	}

    // @AutoLog(value = "查询应用画像列表",operateType = CommonConstant.OPERATE_TYPE_LIST, module = "离线业务多数据中心迁移-离线业务负载管理")
	@ApiOperation(value = "应用画像列表", notes = "应用画像列表")
	@RequestMapping(value = "/pageAppProfile", method = RequestMethod.POST)
	@RequiresPermissions(value = {"profilePageAppProfile"})
    @ParamSecurity(enabled = true)
	public PageResult<PageAppProfileVo> pageAppProfile(@Valid PageFeatureMsg msg) throws Exception {
		if(StringUtils.isNotBlank(msg.getTitle())) {
			sysBaseAPI.addLog((msg.getContent() != null ? msg.getContent():"查询应用画像列表")+msg.toString(), null, SysLogOperateType.QUERY, null, null, msg.getTitle());
		}else {
			sysBaseAPI.addLog((msg.getContent() != null ? msg.getContent():"查询应用画像列表")+msg.toString(), null, CommonConstant.OPERATE_TYPE_LIST);
		}
		return service.pageAppProfile(msg);
	}

    // @AutoLog(value = "查看应用画像统计",operateType=CommonConstant.OPERATE_TYPE_DETAIL)
	@ApiOperation(value = "应用画像统计", notes = "应用画像统计")
	@RequestMapping(value = "/countAppProfile", method = RequestMethod.POST)
	@RequiresPermissions(value = {"profileCountAppProfile"})
    @ParamSecurity(enabled = true)
	public CountAppProfileVo countAppProfile(String datacenterId, HttpServletRequest request) throws Exception {
		CountAppProfileVo vo =  service.countAppProfile(datacenterId);
		String content = request.getParameter("content");
		sysBaseAPI.addLog((content != null ? content : "查询应用画像列表")+"，数据总线ID："+(datacenterId == null ?"无":datacenterId), null, SysLogOperateType.DETAIL, null, null, request.getParameter("title"));
		return vo;
	}
}
