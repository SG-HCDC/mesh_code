package com.gcloud.mesh.dcs.strategy;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.alibaba.fastjson.JSONObject;
import com.gcloud.mesh.asset.dao.CloudResourceConfigDao;
import com.gcloud.mesh.asset.entity.CloudResourceConfigEntity;
import com.gcloud.mesh.dcs.entity.AppEntity;
import com.gcloud.mesh.dcs.strategy.huawei.ListServersDetailsSolution;
import com.gcloud.mesh.header.param.HuaweiMigrationParam;
import com.gcloud.mesh.utils.DateUtil;
import com.google.common.collect.Lists;
import com.huaweicloud.sdk.ecs.v2.model.ListServersDetailsResponse;
import com.huaweicloud.sdk.ecs.v2.model.ServerDetail;
@Component
public class HuaweiQueryInstance implements QueryInstance{
    @Autowired
    private CloudResourceConfigDao cloudResourceConfigDao;
	
	@Override
	public List<AppEntity> queryServerList(String cloudResourceId) {
		
		
        // SupplierEntity supplierEntity = supplierDao.findOneByProperty("datacenter_id", datacenterId);
		CloudResourceConfigEntity cloudResource = cloudResourceConfigDao.findOneByProperty("cloud_resource_id", cloudResourceId);
        if(cloudResource==null) return Lists.newArrayList();

        HuaweiMigrationParam huaweiMigrationParam = JSONObject.toJavaObject(JSONObject.parseObject(cloudResource.getConfig()), HuaweiMigrationParam.class);

        return queryServerList(huaweiMigrationParam);
		
	}
	
	public List<AppEntity> queryServerList(HuaweiMigrationParam aliMigrationParam){
        List<AppEntity> list = new ArrayList<>();
        ListServersDetailsSolution listServer = new ListServersDetailsSolution();
        ListServersDetailsResponse response = listServer.listServers(aliMigrationParam.getRegionId(), aliMigrationParam.getAccessKey(), aliMigrationParam.getSecretKey(), aliMigrationParam.getProjectId());
        List<ServerDetail>  lists =  response.getServers();
        if(response != null && !response.getServers().isEmpty()) {
        	for(ServerDetail detail : lists) {
            	AppEntity en = new AppEntity();
            	// en.setCloudResourceId(detail.getId());
            	en.setId(detail.getId());
            	en.setName(detail.getName());
            	en.setCreateTime(DateUtil.stringToDate(detail.getCreated()));
            	list.add(en);
        	}

        }
        return list;
	}
}
