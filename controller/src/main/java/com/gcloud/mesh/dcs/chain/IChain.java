package com.gcloud.mesh.dcs.chain;

import org.springframework.core.Ordered;

public interface IChain extends Ordered {
    /**
     * 执行方法
     * @param schedulerJobId 迁移任务ID
     * @param attach 附加属性
     * @param filterChain 执行链
     * @param stepChain 执行步骤
     */
    void doExec(String schedulerJobId, String attach, FilterChain filterChain, IMigrationStepChain stepChain);
}
