package com.gcloud.mesh.dcs.task;

import com.gcloud.mesh.asset.dao.CloudResourceConfigDao;
import com.gcloud.mesh.asset.dao.CloudResourceDao;
import com.gcloud.mesh.asset.entity.CloudResourceConfigEntity;
import com.gcloud.mesh.asset.entity.CloudResourceEntity;
import com.gcloud.mesh.asset.enums.CloudResourceType;
import com.gcloud.mesh.dcs.dao.AppDao;
import com.gcloud.mesh.dcs.dao.AppInstanceDao;
import com.gcloud.mesh.dcs.entity.AppEntity;
import com.gcloud.mesh.dcs.entity.AppInstanceEntity;
import com.gcloud.mesh.dcs.service.AppProfileService;
import com.gcloud.mesh.framework.core.SpringUtil;
import com.gcloud.mesh.framework.core.util.DateUtil;
import com.gcloud.mesh.header.enums.ResourceSourceType;
import com.gcloud.mesh.header.vo.supplier.SupplierVo;
import com.gcloud.mesh.sdk.GceSDK;
import com.gcloud.mesh.sdk.gce.ClusterVo;
import com.gcloud.mesh.sdk.gce.GcePageReply;
import com.gcloud.mesh.sdk.gce.NamespaceVo;
import com.gcloud.mesh.supplier.dao.SupplierDao;
import com.gcloud.mesh.supplier.entity.SupplierEntity;
import com.gcloud.mesh.supplier.enums.SystemType;
import com.gcloud.mesh.supplier.service.SupplierService;
import com.google.common.collect.Maps;
import lombok.extern.slf4j.Slf4j;
import org.jeecg.common.util.RedisUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

/**
 * 同步本地app信息（迁移导致appid错误）
 */
@Slf4j
//@Component
public class SyncUpdateAppTask {

//	@Autowired
//	private GceSDK gceSdk;

	@Autowired
	private AppInstanceDao appInstanceDao;

	@Autowired
	private AppDao appDao;

	@Autowired
	private SupplierDao supplierDao;

	@Autowired
	private CloudResourceDao cloudResourceDao;

	@Autowired
	private CloudResourceConfigDao cloudResourceConfigDao;

	@Autowired
	private JdbcTemplate jdbcTemplate;

	@Autowired
	private RedisUtil redisUtil;

	@Scheduled(fixedDelay = 1000 * 5)
	public void work() {
		try {
			log.debug("[App] 同步更新应用定时器开始");
			if(redisUtil.get("gce:sync:app")!=null) {
				log.debug("[App] 同步更新应用定时器 - 上一任务还在执行，跳过（默认最大5分钟）。");return;
			}
			redisUtil.set("gce:sync:app",1,60*5);
			List<AppInstanceEntity> allApp = appInstanceDao.findAll();
			if (allApp != null && !allApp.isEmpty()) {
				for (AppInstanceEntity instanceEntity : allApp) {
					AppEntity appEntity = appDao.getById(instanceEntity.getAppId());
					if(appEntity==null) continue;
					CloudResourceEntity cloudResourceEntity = cloudResourceDao.getById(appEntity.getCloudResourceId());
					if (cloudResourceEntity == null) continue;
					if(cloudResourceEntity.getType()!=SystemType.K8S.getNo()) continue;
					CloudResourceConfigEntity configEntity = cloudResourceConfigDao.findOneByProperty("cloudResourceId", cloudResourceEntity.getId());
					if(configEntity==null) continue;
					GceSDK gceSdk = SpringUtil.getBean(GceSDK.class);
					String clusterId = configEntity.getClusterId();
					GcePageReply<NamespaceVo> namespacePage = gceSdk.pageNamespace(clusterId, 1, Integer.MAX_VALUE);
					List<NamespaceVo> namespaceVoList = namespacePage.getData();
					if (namespaceVoList == null || namespaceVoList.isEmpty()) continue;
					for (NamespaceVo namespaceVo : namespaceVoList) {
						if(namespaceVo.getName().equals(appEntity.getName()) && !namespaceVo.getId().equals(appEntity.getId())){
							jdbcTemplate.update("update dcs_app_instance set app_id = ? where app_id = ?", namespaceVo.getId(), appEntity.getId());
						}
					}
				}
			}
		}finally {
			redisUtil.del("gce:sync:app");
			log.debug("[App] 同步更新应用定时器结束");
		}


	}

}
