package com.gcloud.mesh.dcs.controller;

import com.gcloud.framework.db.PageResult;
import com.gcloud.mesh.dcs.annotation.ParamSecurity;
import com.gcloud.mesh.dcs.service.DataSecurityService;
import com.gcloud.mesh.header.msg.BaseReplyMsg;
import com.gcloud.mesh.header.msg.dcs.DetailDataSecurityMsg;
import com.gcloud.mesh.header.msg.dcs.PageBackupMsg;
import com.gcloud.mesh.header.msg.dcs.SetDataSecurityMsg;
import com.gcloud.mesh.header.vo.dcs.DataBackupVo;
import com.gcloud.mesh.header.vo.dcs.DataSecurityVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.jeecg.common.aspect.annotation.AutoLog;
import org.jeecg.common.constant.CommonConstant;
import org.jeecg.common.constant.enums.SysLogOperateType;
import org.jeecg.common.system.api.ISysBaseAPI;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@Api(tags = "数据安全")
@RestController
@RequestMapping("/dataSecurity")
public class DataSecurityController {

	@Autowired
	private DataSecurityService service;
	
	@Autowired
	private ISysBaseAPI sysBaseAPI;
	
	//处理Mass Assignment: Insecure Binder Configuration
	@InitBinder
	public void populateCustomerRequest(WebDataBinder binder) {
	    binder.setDisallowedFields(new String[]{});
	}

//    @AutoLog(value = "数据安全-启用",operateType = CommonConstant.OPERATE_TYPE_CONFIG)
//	@ApiOperation(value = "数据安全 - 启用", notes = "数据安全 - 启用")
//	@RequestMapping(value = "/enabled", method = RequestMethod.POST)
//    @RequiresPermissions(value = {"dataSecurityEnabled"})
//	public void enabled() throws Exception {
//		service.enabled();
//	}
//
//    @AutoLog(value = "数据安全-禁用",operateType = CommonConstant.OPERATE_TYPE_CONFIG)
//	@ApiOperation(value = "数据安全 - 禁用", notes = "数据安全 - 禁用")
//	@RequestMapping(value = "/disabled", method = RequestMethod.POST)
//    @RequiresPermissions(value = {"dataSecurityDisabled"})
//	public void disabled() throws Exception {
//		service.disabled();
//	}

   // @AutoLog(value = "数据安全-设置",operateType = CommonConstant.OPERATE_TYPE_CONFIG)
	@ApiOperation(value = "数据安全 - 设置", notes = "数据安全 - 设置")
	@RequestMapping(value = "/set2", method = RequestMethod.POST)
    @RequiresPermissions(value = {"dataSecuritySet"})
    @ParamSecurity(enabled = true)
	public void set(@Valid SetDataSecurityMsg msg) throws Exception {
		service.set(msg);
    	StringBuffer buff = new StringBuffer();
    	buff.append("设置数据安全策略");
    	buff.append("，"+msg.toString());
		sysBaseAPI.addLog(buff.toString(), null, SysLogOperateType.UPDATE, null, null, "精确管控与供电制冷联动-精确能耗管控-数据安全");
	}

    // @AutoLog(value = "数据安全-详情",operateType = CommonConstant.OPERATE_TYPE_DETAIL)
	@ApiOperation(value = "查看数据安全 - 详情", notes = "数据安全 - 详情")
	@RequestMapping(value = "/detail", method = RequestMethod.POST)
	@RequiresPermissions(value = { "dataSecurityDetail" })
	@ParamSecurity(enabled = true)
	public BaseReplyMsg detail(@Valid DetailDataSecurityMsg msg) throws Exception {
		DataSecurityVo vo = service.detail(msg);
		BaseReplyMsg reply = new BaseReplyMsg();
		reply.setData(vo);
		sysBaseAPI.addLog("查看数据安全策略设置", null, SysLogOperateType.DETAIL, null, null, "精确管控与供电制冷联动-精确能耗管控-数据安全");
		return reply;
	}

    @AutoLog(value = "查询数据安全-备份记录列表列表",operateType = CommonConstant.OPERATE_TYPE_LIST)
	@ApiOperation(value = "数据安全 - 备份记录分页列表", notes = "数据安全 - 备份记录分页列表")
	@RequestMapping(value = "/pageBackup", method = RequestMethod.POST)
	@RequiresPermissions(value = { "dataSecurityPageBackup" })
    @ParamSecurity(enabled = true)
	public PageResult<DataBackupVo> pageBackup(@Valid PageBackupMsg msg) throws Exception {
		return service.pageBackup(msg);
	}

}
