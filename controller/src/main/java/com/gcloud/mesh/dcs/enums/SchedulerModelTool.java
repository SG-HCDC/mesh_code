package com.gcloud.mesh.dcs.enums;

public enum SchedulerModelTool {

	K8S_TOOL(0, "k8s", "Kubernetes迁移工具"),
	ALIYUN_TOOL(1, "vm", "阿里云迁移工具"),
	HUAWEI_TOOL(2, "vm", "华为迁移工具");

	private Integer value;
	private String type;
	private String name;

	SchedulerModelTool(Integer value, String type, String name) {
		this.value = value;
		this.type = type;
		this.name = name;
	}

	public static SchedulerModelTool getByType(Integer value) {
		if (value != null) {
			for (SchedulerModelTool driver : SchedulerModelTool.values()) {
				if (driver.getValue().equals(value)) {
					return driver;
				}
			}
		}
		return null;
	}

	public Integer getValue() {
		return value;
	}
	
	public String getType() {
		return type;
	}

	public String getName() {
		return name;
	}

}
