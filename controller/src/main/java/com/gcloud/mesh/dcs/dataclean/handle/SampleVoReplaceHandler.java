package com.gcloud.mesh.dcs.dataclean.handle;

import org.springframework.stereotype.Component;

import com.gcloud.mesh.header.vo.monitor.SampleVo;

@Component
public class SampleVoReplaceHandler implements IHandler<SampleVo> {

	@Override
	public SampleVo process(SampleVo msg) {
		// TODO Auto-generated method stub
		// 具体的替换逻辑
		if(msg.getVolume() == null) {
			msg.setVolume(0d);
		}
		return msg;
	}

}
