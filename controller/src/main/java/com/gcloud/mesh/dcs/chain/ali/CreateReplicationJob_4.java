package com.gcloud.mesh.dcs.chain.ali;

import com.alibaba.fastjson.JSONObject;
import com.aliyun.smc20190601.Client;
import com.aliyun.smc20190601.models.CreateReplicationJobRequest;
import com.aliyun.smc20190601.models.CreateReplicationJobResponse;
import com.gcloud.mesh.dcs.chain.FilterChain;
import com.gcloud.mesh.dcs.chain.IMigrationStepChain;
import com.google.gson.Gson;
import lombok.extern.slf4j.Slf4j;
import org.jeecg.common.exception.MyBusinessException;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;

//@Component
@Slf4j
public class CreateReplicationJob_4 extends AbstractAliMigrationStep {
    @Override
    public void doExec(String schedulerJobId, String attach, FilterChain filterChain, IMigrationStepChain stepChain) {
        JSONObject jsonObject = JSONObject.parseObject(attach);
        String sourceId = jsonObject.getString("sourceId");
        String regionId = jsonObject.getString("regionId");
        Integer systemDiskSize = jsonObject.getInteger("systemDiskSize");
        String accessKeyId = jsonObject.getString("accessKeyId");
        String accessSecret = jsonObject.getString("accessSecret");
        Client client = super.createSMCClient(accessKeyId,accessSecret);
        String jobId = this.createReplicationJob4Image(client, sourceId, systemDiskSize, regionId);
        jsonObject.put("jobId",jobId);
        filterChain.doExec(schedulerJobId,jsonObject.toString(),filterChain,stepChain);
    }


    @Override
    public String chainName(String attach) {
        return "创建迁移镜像任务";
    }

    @Override
    public int getOrder() {
        return 4;
    }


    public String createReplicationJob4Image (Client client, String sourceId, Integer systemDiskSize, String regionId){
        BigDecimal bd = new BigDecimal(systemDiskSize.toString()).subtract(new BigDecimal("0.001"));
        BigDecimal bigDecimal = bd.multiply(new BigDecimal(1024).pow(3));
        CreateReplicationJobRequest.CreateReplicationJobRequestSystemDiskPart systemDiskPart0 = new CreateReplicationJobRequest.CreateReplicationJobRequestSystemDiskPart()
                .setSizeBytes(bigDecimal.longValue())
                .setDevice("0_0");
        //创建API请求，并设置参数
        CreateReplicationJobRequest request = new CreateReplicationJobRequest();
        //迁移源ID
        request.setSourceId(sourceId);
        //目标阿里云服务器ECS的系统盘大小，单位为GiB
        request.setSystemDiskSize(systemDiskSize);
        //保证请求幂等性，您可以从客户端生成一个不超过64个ASCII字符的参数值，并将值赋予ClientToken
//        request.setClientToken(UUID.randomUUID().toString());
//        request.setRunOnce(true);
        request.setRegionId(regionId);
        request.setSystemDiskPart(java.util.Arrays.asList(
                systemDiskPart0
        ));
        //发送请求获取返回值或处理异常
        CreateReplicationJobResponse response = null;
        try {
            response = client.createReplicationJob(request);
            log.debug(new Gson().toJson(response));
            log.debug("**********成功创建迁移镜像任务**********");
        } catch (Exception e) {
            log.error("创建迁移镜像任务失败",e);
            throw new MyBusinessException("创建迁移镜像任务失败"+"::"+e.getMessage());
        }
        return response.getBody().getJobId();


    }

}
