package com.gcloud.mesh.common.runner;

import org.jeecg.common.cache.PermissionCodeCache;
import org.jeecg.common.isc.IIscService;
import org.jeecg.common.vo.PermissionsCompareVo;
import org.jeecg.common.vo.PermissionsInfoVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import com.baomidou.mybatisplus.core.toolkit.StringUtils;

import com.sgcc.isc.service.adapter.utils.JsonUtil;

import lombok.extern.slf4j.Slf4j;

@Slf4j
//@Component
public class PermissionCodeCacheRunner implements CommandLineRunner {

	@Autowired
	private IIscService service;

	@Override
	public void run(String... args) throws Exception {

		log.info("【权限校验加载-PermissionCodeCacheRunner】");

		try {
			PermissionsCompareVo res = service.permissionsCompare();
			log.info("【权限校验加载-PermissionCodeCacheRunner】" + JsonUtil.toJson(res));

			if (res != null && res.getMeshPermissions() != null) {
				for (PermissionsInfoVo p : res.getMeshPermissions()) {
					if(StringUtils.isNotBlank(p.getUrl())) {
						PermissionCodeCache.put(p.getUrl(), p);
					}		
				}
			}
			log.info("cache 加载成功："+PermissionCodeCache.getUrls().size());
		} catch (Exception e) {
			log.error("【权限手动加载失败-PermissionCodeCacheRunner】", e);
		}
	}

}