package com.gcloud.mesh.common.service.impl;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.jeecg.common.isc.IIscService;
import org.jeecg.common.vo.PermissionsCompareVo;
import org.jeecg.common.vo.PermissionsInfoVo;
import org.jeecg.config.IscConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


import com.gcloud.mesh.framework.core.SpringUtil;
import com.gcloud.mesh.framework.core.util.JsonUtil;
import com.gcloud.mesh.monitor.service.StatisticsCheatService;
import com.sgcc.isc.core.orm.resource.Function;
import com.sgcc.isc.service.adapter.factory.AdapterFactory;
import com.sgcc.isc.service.adapter.helper.IResourceService;

import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class IscServiceImpl implements IIscService {

	@Autowired
	StatisticsCheatService statisticsCheatService;

	public PermissionsCompareVo permissionsCompare() throws Exception {
		List<PermissionsInfoVo> meshPermissions = new ArrayList<>();
		List<PermissionsInfoVo> iscPermissions = new ArrayList<>();
		List<PermissionsInfoVo> iscLackPermissions = new ArrayList<>();

		// 获取mesh权限标签列表
		PermissionsCompareVo res = new PermissionsCompareVo();
		Map<String, Object> controllers = SpringUtil.getApplicationContext().getBeansWithAnnotation(RestController.class);
		if (controllers != null) {
			for (Object controller : controllers.values()) {

				String className = controller.getClass().getName();
				String[] name = className.split("\\$\\$");
				Class<?> controllerClass = Class.forName(name[0]);

				RequestMapping requestController = controllerClass.getAnnotation(RequestMapping.class);
				if (requestController != null) {
					System.out.println(className);
					for (Method apiMethod : controllerClass.getMethods()) {
						Annotation[] as = apiMethod.getAnnotations();
						for (Annotation p : as) {
							PermissionsInfoVo permissionsInfoVo = new PermissionsInfoVo();
							if (p instanceof RequiresPermissions) {
								Method m = p.getClass().getDeclaredMethod("value", null);
								String[] values = (String[]) m.invoke(p, null);

								if (values != null && values.length > 0) {

									permissionsInfoVo.setCode(values[0]);

								}
							}

							ApiOperation api = apiMethod.getAnnotation(ApiOperation.class);
							if (api != null) {
								permissionsInfoVo.setName(api.value());
							}

							RequestMapping request = apiMethod.getAnnotation(RequestMapping.class);
							if (request != null) {
								String url = "";
								if (request.value()[0].startsWith("/")) {
									url = requestController.value()[0] + request.value()[0];
								} else {
									url = requestController.value()[0] + "/" + request.value()[0];
								}

								permissionsInfoVo.setUrl(url);
							}
							meshPermissions.add(permissionsInfoVo);
						}
					}
				}
			}
		}

		// TODO 添加ISC数组 iscPermissions.add(xxxx);
		IResourceService resourceService = AdapterFactory.getResourceService();
		Map<String, Object> p = new HashMap<>();
		String[] s = new String[] {};
		List<Function> functions = resourceService.getSubFuncs(IscConfig.getIscAppId(), "", p, s);
		
		log.info("functions:" + JsonUtil.listToJson(functions));
//		
//		JSONObject object = JSONObject.parseObject(JsonUtil.listToJson(functions));
//	    String pretty = JSON.toJSONString(object, SerializerFeature.PrettyFormat, SerializerFeature.WriteMapNullValue, 
//	            SerializerFeature.WriteDateUseDateFormat);
//		
//		log.info("pretty:" + pretty);
		for (Function function : functions) {
			if (function.getFuncCategory().equals("002")) {
				PermissionsInfoVo per = new PermissionsInfoVo();
				per.setCode(function.getBusiCode());
				per.setName(function.getName());
				per.setUrl(function.getUrl());
				iscPermissions.add(per);
			}
		}

		// 生成对照数组
		List<String> iscCodeList = new ArrayList<>();
		for (PermissionsInfoVo iscPermission : iscPermissions) {
			iscCodeList.add(iscPermission.getCode());
		}

		for (PermissionsInfoVo meshPermission : meshPermissions) {
			if (!iscCodeList.contains(meshPermission.getCode())) {
				iscLackPermissions.add(meshPermission);
			}
		}

		res.setIscLackPermissions(iscLackPermissions);
		res.setIscPermissions(iscPermissions);
		res.setMeshPermissions(meshPermissions);
		return res;
	}
}
