
package com.gcloud.mesh.common.controller;

import org.jeecg.common.aspect.annotation.AutoLog;
import org.jeecg.common.constant.CommonConstant;
import org.jeecg.common.isc.IIscService;
import org.jeecg.common.vo.PermissionsCompareVo;
import org.jeecg.config.IscConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.gcloud.mesh.header.vo.IscServerUrlVo;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@Api(tags = "ISC模块")
@RestController
@RequestMapping("/isc")
public class IscController {

	@Autowired
	private IIscService service;

	// 处理Mass Assignment: Insecure Binder Configuration
	@InitBinder
	public void populateCustomerRequest(WebDataBinder binder) {
		binder.setDisallowedFields(new String[] {});
	}

	// @AutoLog(value = "java.lang.IllegalArgumentException: Source must not be
	// null at org.springframework.util.Assert.notNull(Assert.java:198)
	// ~[spring-core-5.2.4.RELEASE.jar!/:5.2.4.RELEASE]", logType =
	// CommonConstant.LOG_TYPE_DEBUG, operateType =
	// CommonConstant.OPERATE_TYPE_PROCEDURE_DEBUG, module = "程序权限列表调试")
	@ApiOperation(value = "权限列表对照接口", notes = "权限列表对照接口")
	@RequestMapping(value = "/permissions_compare", method = RequestMethod.GET)
	public PermissionsCompareVo permissionsCompare() throws Exception {
		return service.permissionsCompare();
	}

	// @AutoLog(value =
	// "org.springframework.web.bind.MissingRequestHeaderException: Missing
	// request header 'pass' on config file", logType =
	// CommonConstant.LOG_TYPE_DEBUG, operateType =
	// CommonConstant.OPERATE_TYPE_CONFIG_DEBUG, module = "Isc调试")
	@ApiOperation(value = "获取ISC地址", notes = "获取ISC地址")
	@RequestMapping(value = "/get_isc_url", method = RequestMethod.GET)
	public Boolean getIscUrl() throws Exception {
		return true;
	}

	@AutoLog(value = "获取数据库状态", logType = CommonConstant.LOG_TYPE_DEBUG, operateType = CommonConstant.OPERATE_TYPE_DB_DEBUG, module = "数据库调试")
	@ApiOperation(value = "获取数据库状态", notes = "获取数据库状态")
	@RequestMapping(value = "/get_db_status", method = RequestMethod.GET)
	public Boolean getDbStatus() throws Exception {
		return true;
	}

	@ApiOperation(value = "权限列表对照接口", notes = "权限列表对照接口")
	@RequestMapping(value = "/isc_server_url", method = RequestMethod.GET)
	public IscServerUrlVo iscServerUrl() throws Exception {
		IscServerUrlVo vo = new IscServerUrlVo();
		vo.setIscServerUrl(IscConfig.getIscSsoPath());
		return vo;
	}

}
