package com.gcloud.mesh.distributedSchedule;

import com.gcloud.mesh.distributedSchedule.exception.SchedulerException;

public interface IDistributedSchedule {

    void addCluster(ICluster cluster) throws Exception;

    void rmCluster(String clusterId);

    String scheduleToCluster(IApplication app) throws SchedulerException;
}
