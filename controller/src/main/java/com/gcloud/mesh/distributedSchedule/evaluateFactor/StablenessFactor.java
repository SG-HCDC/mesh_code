package com.gcloud.mesh.distributedSchedule.evaluateFactor;

import java.util.concurrent.ConcurrentHashMap;

import com.gcloud.mesh.distributedSchedule.ICluster;
import com.gcloud.mesh.distributedSchedule.IEvaluateFactor;
import com.gcloud.mesh.distributedSchedule.application.Application;

/**
 * 迁移稳定性评价函数.策略很简单，迁移越少，越稳定
 */
public class StablenessFactor extends EvaluateFactor implements IEvaluateFactor {

    public static String nameId = "stableness_factor";

    @Override
    public evalResult evaluate(Application[] scheduleAppList, ConcurrentHashMap<String, ICluster> targetClusters, String[] clusterIds) {
        evalResult er = new evalResult(nameId);
        // HashMap<String, Set<Integer>> cltSort = this.sort(clusterIds);
        er.qualified = true;

        // 初始值满分，采用扣分形式，迁移得越多，稳定性越差，扣分越多
        er.score = scheduleAppList.length * 10;

        for (Application app : scheduleAppList) {
            if (! app.getClusterId().equals(app.getMigrateCluster())) {
                er.score -= 1 * this.migrateAffection(app,
                                                      targetClusters.get(app.getClusterId()),
                                                      targetClusters.get(app.getMigrateCluster()));
            }
        }

        return er;
    }

    /**
     * 计算从源到目的集群迁移对应用稳定性的影响程度，影响越大，返回值越大
     * @param srcCluster
     * @param dstCluster
     * @return 返回一个数值指代影响程度，大小介于 1 - 10
     */
    private double migrateAffection(Application app, ICluster srcCluster, ICluster dstCluster) {
        // TODO: 占个位，待完善，迁移稳定性可以从应用的存储大小，内存使用大小，实时带宽占用的大小，以及两个集群的性能差异大小来判断
        return 5.0;
    }
}
