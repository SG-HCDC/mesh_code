package com.gcloud.mesh.distributedSchedule.application;

public class Application {

    protected String clusterId;
    protected String appId;
    protected String appName;

    // standard
    protected int cpuCores;
    protected double memSpace;
    protected int storageSpace;
    protected int bandWidthKBPS;

    // 用来临时存储预期调度此应用到的目标集群id,用于多目标调度器
    private String migrateCluster;

    // TODO: app type
    // CPU  Mem  IO ...

    public String getAppName() {
        return appName;
    }

    public void setAppName(String appName) {
        this.appName = appName;
    }

    public String getClusterId() {
        return clusterId;
    }

    public void setClusterId(String clusterId) {
        this.clusterId = clusterId;
    }

    public String getAppId() {
        return appId;
    }

    public void setAppId(String appId) {
        this.appId = appId;
    }

    /**
     * 设置应用将要迁移的集群id
     * @param clusterId
     */
    public void setMigrateCluster(String clusterId) {
        this.migrateCluster = clusterId;
    }
    public String getMigrateCluster() {
        return this.migrateCluster;
    }

    public int getCpuCost() {
        return this.cpuCores;
    }

    public int getStorageSpaceCost() {
        return this.storageSpace;
    }

    public int getBandWidtdKBPS() {
        return this.bandWidthKBPS;
    }

    public double getMemSpaceCost() {
        return this.memSpace;
    }
}


