package com.gcloud.mesh.distributedSchedule;

import java.util.HashMap;

import com.gcloud.mesh.distributedSchedule.application.Application;

public interface ICluster {

    /**
     * 初始化集群（IDC)的基本资源
     * @param cpuCores           总处理器内核数量
     * @param memSpaceG
     * @param storageSpaceG
     * @param bandWidthKBPS
     * @param kwhCost            电价，￥/kwh
     * @param maxInputPowerKwh   集群配电子网允许的最大输入功率
     */
    void init(int cpuCores, double memSpaceG, int storageSpaceG, int bandWidthKBPS, double kwhCost, int maxInputPowerKwh);

    /**
     * 更新集群（IDC)的实时功耗信息
     * @param serverPowerWatt  服务器瞬时功率
     * @param refrigerationPowerWatt 制冷系统功率
     * @param psuLossPowerWatt  机房电源模块损耗功率（功率因数小于1），通过输入输出功率可得到
     * @param devicePowerWatt 其他设备的功率，包括存储设备，网络设备等
     */
    void updateInstantPower(int serverPowerWatt, int refrigerationPowerWatt, int psuLossPowerWatt, int devicePowerWatt);

    /**
     * 更新集群实时剩余计算资源，用来做迁移评估，以及计算集群的实时负载情况，  restCpuCore / totalCpuCore ...
     * @param cpuCores
     * @param memSpaceG
     * @param storageSpaceG
     * @param bandWidthKBPS
     */
    void updateInstantRestResource(int cpuCores, double memSpaceG, int storageSpaceG, int bandWidthKBPS);

    /**
     * 集群里节点单个CPU的性能参数，用以评估集群的能效
     * @param cpuCoreNum              cpu逻辑内核数量
     * @param highestCpuFrequencyGB   cpu最高频率，单位G，小数
     * @param L2CacheSizeKB           L2 cache的大小，单位KB
     * @param cpuTDP                  cpu的设计TDP
     * @param memorySizeG             节点上内存的大小，如果节点是多路CPU，则总内存除以CPU数量
     * @param memorySpeedG            内存工作频率，单位G，小数
     */
    void setupPerformanceInfo(int cpuCoreNum, float highestCpuFrequencyGB, int L2CacheSizeKB, int cpuTDP, double memorySizeG, float memorySpeedG);

    /**
     * add app and watching how IDC's instance power and PUE changing.
     * @param app 应用描述实例
     */
    void addApplication(IApplication app) throws Exception;

    void rmApplication(String appId);

    String getId();

    /**
     * 返回效能值
     * @param app 代表要迁移入的应用后的预测能效值，如果传入null，返回当前负载下的能效值
     * @return
     */
    int getEfficientValue(IApplication app);

    /**
     * 返回集群可用资源
     * @return
     */
    ClusterAvailableResource getAvailResource();

    /**
     * 有些动环设备能直接采集到实时PUE数据，可以直接设置
     */
    void updatePue(double pueValue);

    void updateEnvTemperature(double outdoorTemp, double indoorTemp);

    /**
     * 设置高效负载系数，集群负载达到此系数则认为处于高效状态，默认0.7
     * @param loadRate
     */
    void setEfficientLoadRate(double loadRate) throws Exception;
    double getEfficientLoadRate();

    /**
     * 评估剩下的资源.这个接口主要是用于被评估函数用来做评估用的。
     * @param appList 假设集群在现有状态基础上再额外运行的应用列表。
     * @return
     */
    ClusterAvailableResource getAvailResource(Application[] appList);

    /**
     * 返回集群的总体资源数量
     * @return
     */
    ClusterAvailableResource getTotalResource();

    int getNodeCpuCoreNum();
    double getNodeMemSizeG();
    float getNodeCpuHighestFrequency();
    int getL2CacheSizeKB();

    void setSecurePowerRate(double rate) throws Exception;
    double getSecurePowerRate();

    HashMap<String, IApplication> getAppMap();

    int getInstantPowerKWH();
    int getMaxInputPowerKwh();
}
