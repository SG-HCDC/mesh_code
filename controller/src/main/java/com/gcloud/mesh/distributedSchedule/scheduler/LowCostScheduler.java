package com.gcloud.mesh.distributedSchedule.scheduler;

import com.gcloud.mesh.distributedSchedule.IApplication;
import com.gcloud.mesh.distributedSchedule.IDistributedSchedule;
import com.gcloud.mesh.distributedSchedule.exception.SchedulerException;

/**
 * 低成本调度，综合考虑了电价和效能。能效/电价 越高，则成本越低
 */
public class LowCostScheduler extends Scheduler implements IDistributedSchedule {
    public LowCostScheduler(int initialCapacity) {
        super(initialCapacity);
    }

    @Override
    public String scheduleToCluster(IApplication app) throws SchedulerException {
        return null;
    }
}
