package com.gcloud.mesh.distributedSchedule.scheduler;

import java.util.concurrent.ConcurrentHashMap;

import com.gcloud.mesh.distributedSchedule.ICluster;

/**
 * 单目标调度器父类
 */
public class Scheduler {

	private ConcurrentHashMap<String, ICluster> clusterMap = null;

	public ConcurrentHashMap<String, ICluster> getClusterMap() {
		return clusterMap;
	}

	public void setClusterMap(ConcurrentHashMap<String, ICluster> clusterMap) {
		this.clusterMap = clusterMap;
	}

	public Scheduler(int initialCapacity) {
		if (initialCapacity > 0)
			this.clusterMap = new ConcurrentHashMap<>(initialCapacity);
		else
			this.clusterMap = new ConcurrentHashMap<>();
	}

	public void addCluster(ICluster cluster) throws Exception {
		if (clusterMap.containsKey(cluster.getId())) {
			throw new Exception(String.format("cluster id existed [%s].", cluster.getId()));
		} else {
			this.clusterMap.put(cluster.getId(), cluster);
		}
	}

	public void updateCluster(ICluster cluster) throws Exception {
		this.clusterMap.put(cluster.getId(), cluster);
	}

	public void rmCluster(String clusterId) {
		this.clusterMap.remove(clusterId);
	}
}
