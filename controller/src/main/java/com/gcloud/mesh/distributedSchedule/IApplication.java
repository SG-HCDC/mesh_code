package com.gcloud.mesh.distributedSchedule;

/**
 * 记录应用对资源的需求，包括预估与实时状态
 */
public interface IApplication {

    // initial resource demands of application(s)
    void init(int cpuCore, double memSpaceG, int storageSpaceG, int bandWidthKBPS);

    // update status of running application(s), 0 means no update.
    void updateStatus(int cpuCore, double memSpaceG, int storageSpaceG, int bandWidthKBPS);
    AppStatus getStatus();

    void appStartTime(long startTime);

    void appEnd();

    /**
     * 应用id
     */
    String getId();

    /**
     * 应用名
     * @return
     */
    String getName();

    /**
     * 应用运行的集群id
     * @return
     */
    String getClusterId();
    void setClusterId(String id);

    /**
     * 用于判断集群资源是否满足本应用的运行
     * @param availRes 可用资源
     * @return  是否满足
     */
    boolean isResourceSatisfied(ClusterAvailableResource availRes);

    /**
     * 迁移代价计算
     * @return
     */
    int migrateCost(ICluster cluster);

    int getCpuCost();

    int getStorageSpaceCost();

    int getBandWidtdKBPS();

    double getMemSpaceCost();
}
