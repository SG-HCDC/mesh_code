package com.gcloud.mesh.distributedSchedule.scheduler;

import com.gcloud.mesh.distributedSchedule.IApplication;
import com.gcloud.mesh.distributedSchedule.IDistributedSchedule;
import com.gcloud.mesh.distributedSchedule.exception.SchedulerException;

public class HighPerformanceScheduler extends Scheduler implements IDistributedSchedule {

    public HighPerformanceScheduler(int initialCapacity) {
        super(initialCapacity);
    }

    @Override
    public String scheduleToCluster(IApplication app) throws SchedulerException {
        return null;
    }
}
