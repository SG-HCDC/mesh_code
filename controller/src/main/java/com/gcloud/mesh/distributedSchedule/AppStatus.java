package com.gcloud.mesh.distributedSchedule;

public class AppStatus {
    public int cpuCores;
    public double memSpaceG;
    public int storageSpaceG;
    public int bandWidthKBPS;

    public AppStatus(int cpuCores, double memSpaceG, int storageSpaceG, int bandWidthKBPS) {
        this.cpuCores = cpuCores;
        this.memSpaceG = memSpaceG;
        this.storageSpaceG = storageSpaceG;
        this.bandWidthKBPS = bandWidthKBPS;
    }
}
