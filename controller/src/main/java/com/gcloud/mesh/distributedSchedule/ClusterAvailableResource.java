package com.gcloud.mesh.distributedSchedule;

/**
 * transport available resource data between cluster and application.
 */
public class ClusterAvailableResource {
    public int availCpuCores;
    public double availMemSpace;
    public int availStorageSpace;
    public int availBandWidth;

    public ClusterAvailableResource(int cpuCores, double memSpace, int storageSpace, int bandWidth) {
        this.availBandWidth = bandWidth;
        this.availCpuCores = cpuCores;
        this.availMemSpace = memSpace;
        this.availStorageSpace = storageSpace;
    }

    public int getAvailCpuCores() {
        return availCpuCores;
    }

    public void setAvailCpuCores(int availCpuCores) {
        this.availCpuCores = availCpuCores;
    }

    public double getAvailMemSpace() {
        return availMemSpace;
    }

    public void setAvailMemSpace(double availMemSpace) {
        this.availMemSpace = availMemSpace;
    }

    public int getAvailStorageSpace() {
        return availStorageSpace;
    }

    public void setAvailStorageSpace(int availStorageSpace) {
        this.availStorageSpace = availStorageSpace;
    }

    public int getAvailBandWidth() {
        return availBandWidth;
    }

    public void setAvailBandWidth(int availBandWidth) {
        this.availBandWidth = availBandWidth;
    }
}
