package com.gcloud.mesh.distributedSchedule.evaluateFactor;

import java.util.concurrent.ConcurrentHashMap;

import com.gcloud.mesh.distributedSchedule.ICluster;
import com.gcloud.mesh.distributedSchedule.IEvaluateFactor;
import com.gcloud.mesh.distributedSchedule.application.Application;

/**
 * 应用关联性，互斥，亲和，绑定
 */
public class ApplicationRelevancy extends EvaluateFactor implements IEvaluateFactor {

    public static String nameId = "powernet_security_factor";

    @Override
    public evalResult evaluate(Application[] scheduleAppList, ConcurrentHashMap<String, ICluster> targetClusters, String[] clusterIds) {
        return null;
    }
}
