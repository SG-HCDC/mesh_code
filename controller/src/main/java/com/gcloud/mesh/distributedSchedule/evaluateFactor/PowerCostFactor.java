package com.gcloud.mesh.distributedSchedule.evaluateFactor;

import java.util.HashMap;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import com.gcloud.mesh.distributedSchedule.ICluster;
import com.gcloud.mesh.distributedSchedule.IEvaluateFactor;
import com.gcloud.mesh.distributedSchedule.application.Application;

/**
 * 迁移后的电费越低，得分越高
 */
public class PowerCostFactor extends EvaluateFactor implements IEvaluateFactor {

    public static String nameId = "powercost_factor";

    @Override
    public evalResult evaluate(Application[] scheduleAppList, ConcurrentHashMap<String, ICluster> targetClusters, String[] clusterIds) {
        evalResult er = new evalResult(nameId);
        HashMap<String, Set<Integer>> cltSort = this.sort(clusterIds);

        return null;
    }
}
