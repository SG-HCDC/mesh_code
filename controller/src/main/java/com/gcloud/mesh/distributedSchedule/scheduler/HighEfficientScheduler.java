package com.gcloud.mesh.distributedSchedule.scheduler;

import com.gcloud.mesh.distributedSchedule.IApplication;
import com.gcloud.mesh.distributedSchedule.ICluster;
import com.gcloud.mesh.distributedSchedule.IDistributedSchedule;
import com.gcloud.mesh.distributedSchedule.exception.SchedulerException;

/**
 * 高效能，以低能耗为目标（注意和低成本不同)
 */
public class HighEfficientScheduler extends Scheduler implements IDistributedSchedule {

    public HighEfficientScheduler(int initialCapacity) {
        super(initialCapacity);
    }

    @Override
    public String scheduleToCluster(IApplication app) throws SchedulerException {
        ICluster highestEfficientCluster = null;

        for (ICluster cluster : this.getClusterMap().values()) {
            if (cluster.getId().equals(app.getClusterId()))
                continue;

            if (app.isResourceSatisfied(cluster.getAvailResource())) {
                if (highestEfficientCluster != null) {
                    if (cluster.getEfficientValue(app) > highestEfficientCluster.getEfficientValue(app))
                        highestEfficientCluster = cluster;
                }
                else {
                    highestEfficientCluster = cluster;
                }
            }
        }

        if (highestEfficientCluster != null)
            return highestEfficientCluster.getId();
        else {
            throw new SchedulerException("can not found a cluster to schedule.");
        }
    }
}
