package com.gcloud.mesh.distributedSchedule;

import java.util.concurrent.ConcurrentHashMap;

import com.gcloud.mesh.distributedSchedule.application.Application;

/**
 * 多目标优化问题中的评价函数
 */
public interface IEvaluateFactor {

    /**
     * 评价等级，由用户自定义设置，代表特定评价因子的权重。
     * 权重系数会乘上评价函数对调度方案的评分，以此来影响特定
     * 评价函数的影响力，从而实现评价优先级。
     * 所以这个评价等级，本质上是评价优先级。
     */
    enum Level {
        HIGH(1.0, "高优先级"),
        MEDIUM(0.7, ""),
        LOW(0.5, ""),
        CLOSE(0, "");

        private double weight;
        private String msg;
        private Level(double weight, String msg) {
            this.weight = weight;
            this.msg = msg;
        }

        public String toString() {
            return this.msg;
        }

        public double getWeight() {
            return this.weight;
        }
    }

    /**
     * 设置权重变量，权重变量
     * @param l 权重变量
     */
    void setLevel(Level l);
    Level getLevel();

    // 记录评估结果
    class evalResult {
        public String evaluatorId;

        public boolean qualified = true;  // 是否合格
        public double score = 0.0;          // 具体得分

        public evalResult(String evaId) {
            this.evaluatorId = evaId;
        }
    }

    String getId();

    /**
     * 执行评价函数,基于 clustersIds 的集群来评估部署是否合理
     * @param scheduleAppList  要调度的应用
     * @param targetClusters   app要调度到的目标集群
     * @param clusterIds       应用要调度到的集群id列表，顺序和 scheduleAppList 保持一致
     * @return 评估结果, 是否符合所设置的评价权重
     */
    evalResult evaluate(Application[] scheduleAppList, ConcurrentHashMap<String, ICluster> targetClusters, String[] clusterIds);

    boolean scaleFactorInit();

    void setScaleFactor(double val);

    double getScaleFactor();

    /**
     * 返回修正后的分数
     * @return
     */
    double getFixedScore(double origScore);
}
