package com.gcloud.mesh.ml;

import com.gcloud.mesh.ml.TrainingSet.Data;

public interface IMachineLearning {

    /**
     * 设置训练数据
     * @param data
     */
    void setTrainingData(TrainingSet data);

    /**
     * training model.
     */
    void training() throws Exception;

    /**
     * predict result with supplied data.
     * @param d
     * @return
     */
    Data predict(Data d);
}
