package com.gcloud.mesh.ml;

import java.util.ArrayList;
import java.util.List;

/** 数据集,具有通用性 */
public class TrainingSet {

    public List<Data> dataList = new ArrayList<>();

    public void add(double y, double... x) {
        Data data = new Data();
        data.x = x;
        data.y = y;
        dataList.add(data);
    }

    public static class Data {
        // 多个参数
        public double[] x;
        // 结果
        public double y;
    }

}