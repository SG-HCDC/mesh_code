package com.gcloud.mesh.analysis.entity;

import java.util.Date;

import com.gcloud.framework.db.jdbc.annotation.ID;
import com.gcloud.framework.db.jdbc.annotation.Table;

import lombok.Data;

@Table( name = "analysis_fitter_devices")
@Data
public class FitterEntity {

	@ID
	private String id;
	private String nodeId;
	private String airId;
	private String algorithm;
	private String metricX;
	private String metricY;
	private Integer status;
	private Integer degree;
	private String result;
	private Date startTime;
	private Date endTime;
	private Date updateTime;
}
