package com.gcloud.mesh.analysis.dao;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Repository;

import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import com.gcloud.framework.db.dao.impl.JdbcBaseDaoImpl;
import com.gcloud.mesh.analysis.entity.DemoEntity;


@Repository
public class DemoDao extends JdbcBaseDaoImpl<DemoEntity, String>{
	
	public DemoEntity getFirst(String type) {
		
		StringBuffer sql = new StringBuffer();
		sql.append(" select * from analysis_demos ");
		sql.append(" where 1 = 1");
		if(type != null) {
			sql.append(" and type = \'" + type + "\'");
		}
		sql.append(" limit 1");
		List<DemoEntity> demos = this.findBySql(sql.toString());
		if(demos != null && demos.size() != 0) {
			return demos.get(0);
		}else {
			return null;
		}
		
	}
	
	public List<DemoEntity> list(String type, String startTime, String endTime) {
		
		StringBuffer sql = new StringBuffer();
		sql.append(" select * from analysis_demos ");
		sql.append(" where 1 = 1");
		List<Object> values = new ArrayList<Object>();
		if(StringUtils.isNotBlank(type)) {
			sql.append(" and type = ?");
			values.add(type);
		}
		if(StringUtils.isNotBlank(startTime) && StringUtils.isNotBlank(endTime)) {
			sql.append(" and timestamp between ? and ?");
			values.add(startTime);
			values.add(endTime);
		}
		sql.append(" limit 100");
		
		return this.findBySql(sql.toString(), values);
		
	}
	
	public List<DemoEntity> list(String type, String resourceId, String startTime, String endTime) {
		
		StringBuffer sql = new StringBuffer();
		sql.append(" select * from analysis_demos ");
		sql.append(" where 1 = 1");
		List<Object> values = new ArrayList<Object>();
		if(StringUtils.isNotBlank(type)) {
			sql.append(" and type = ?");
			values.add(type);
		}
		if(StringUtils.isNotBlank(resourceId)) {
			sql.append(" and resource_id = ?");
			values.add(resourceId);
		}
		if(StringUtils.isNotBlank(startTime) && StringUtils.isNotBlank(endTime)) {
			sql.append(" and timestamp between ? and ?");
			values.add(startTime);
			values.add(endTime);
		}
		sql.append(" limit 100");
		
		return this.findBySql(sql.toString(), values);
		
	}

}
