
package com.gcloud.mesh.analysis.entity;

import java.util.Date;

import com.gcloud.framework.db.jdbc.annotation.ID;
import com.gcloud.framework.db.jdbc.annotation.Table;

import lombok.Data;

@Table(name = "dcs_subhealth_alerts")
@Data
public class SubhealthAlertEntity {

    @ID
    private String id;
    private String deviceType;
    private String meter;
    private String meterName;
    private String resourceId;
    private String resourceName;
    private String content;
    private String level;
    private Date createTime;

}
