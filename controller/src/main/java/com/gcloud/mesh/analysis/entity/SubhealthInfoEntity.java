
package com.gcloud.mesh.analysis.entity;

import com.gcloud.framework.db.jdbc.annotation.ID;
import com.gcloud.framework.db.jdbc.annotation.Table;

import lombok.Data;

@Table(name = "dcs_subhealth_infos")
@Data
public class SubhealthInfoEntity {

    @ID
    private String id;
    private String deviceType;
    private Boolean subhealth;

}
