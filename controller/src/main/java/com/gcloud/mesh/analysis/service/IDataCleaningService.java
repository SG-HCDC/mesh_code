package com.gcloud.mesh.analysis.service;

import java.util.Date;

import com.gcloud.mesh.analysis.data.IDataCleaning;

public interface IDataCleaningService {
	
	public IDataCleaning acquireFitterData(Date startTime, Date endTime, String nodeId, String airId);
	
	public IDataCleaning acquireSimulateData(Date startTime, Date endTime, String dcId, String metriX);
	
	public IDataCleaning acquireSimulateData(Date startTime, Date endTime, String dcId, String deviceId, String metriX);
	
	public String getAirCurrentTemp(String airId);
	
	public String getAirSetTemp(String airId);
	
	public String getAirOut(String airId);
	
	public String getAirIn(String airId);
	
	public String geAirInTemp(String airId);
	
	public String getAirOutTemp(String airId);
	
	public String getPwrNode(String nodeId);
	
	public String getAirHumidityOut(String airId);

	public String getAirHumidityIn(String airId);
	
	public String getUpsVoltageOut(String upsId);

	public String getUpsVoltageIn(String upsId);

	public String getUpsTemp(String upsId);


}
