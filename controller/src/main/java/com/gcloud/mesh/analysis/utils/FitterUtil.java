package com.gcloud.mesh.analysis.utils;

public class FitterUtil {

	public static String calcFitterMode(double[] factors) {
		String result = "";
		for(double d: factors) {
			result = result + "|" + Double.toString(d);
		}
		return result.substring(1, result.length());
	}
	
	public static double[] recalcFitterMode(String factors) {

		String[] factorsStr = factors.split("\\|");
		double[] factorsDouble = new double[] {0, 0, 0, 0, 0, 0};
		for(int i=0; i < factorsStr.length; i++) {
			factorsDouble[i] = Double.parseDouble(factorsStr[i]);
		}
		return factorsDouble;
	}
}
