
package com.gcloud.mesh.analysis.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.gcloud.framework.db.dao.impl.JdbcBaseDaoImpl;
import com.gcloud.mesh.analysis.entity.SubhealthThresholdEntity;

@Repository
public class SubhealthThresholdDao extends JdbcBaseDaoImpl<SubhealthThresholdEntity, String> {

    public <E> List<E> getByDeviceType(String deviceType, Class<E> clazz) {
        return this.findByProperty("deviceType", deviceType, clazz);
    }

    public <E> List<E> list(Class<E> clazz) {
    	String sql = "SELECT *, device_type type FROM dcs_subhealth_thresholds";
    	return this.findBySql(sql, clazz);
    }
    
    public <E> List<E> list(String resourceId, Class<E> clazz) {
    	String sql = "SELECT *, device_type type FROM dcs_subhealth_thresholds WHERE 1 = 1 ";
    	if(resourceId != null) {
    		sql = sql + " AND resource_id = '" + resourceId + "'";
    	}	
    	return this.findBySql(sql, clazz);
    }
}
