package com.gcloud.mesh.analysis.data;

import java.util.List;

import com.gcloud.mesh.header.vo.analysis.DataVo;

public class PolynomialDataCleaning implements IDataCleaning {
	
//	private double[] dataX = new double[] {6, 8, 10, 14, 18};
//	
//	private double[] dataY = new double[] {7, 9, 13, 17.5, 18};
	
	private volatile List<DataVo> data = null;
	
	public PolynomialDataCleaning() {
		
	}

	@Override
	public double[] getX() {
		// TODO Auto-generated method stub
		double[] dataX = new double[data.size()];
		for(int i=0; i< data.size(); i++) {
			dataX[i] = data.get(i).getDataX();
		}
		return dataX;
	}

	@Override
	public double[] getY() {
		// TODO Auto-generated method stub
		double[] dataY = new double[data.size()];
		for(int i=0; i< data.size(); i++) {
			dataY[i] = data.get(i).getDataY();
		}
		return dataY;
	}

	@Override
	public List<DataVo> getData() {
		// TODO Auto-generated method stub
		return data;
	}
	
	public void setData(List<DataVo> data) {
		this.data = data;
	}

}
