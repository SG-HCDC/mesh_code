package com.gcloud.mesh.analysis.service;

import java.util.List;

import com.gcloud.framework.db.PageResult;
import com.gcloud.mesh.analysis.entity.SimulateEntity;
import com.gcloud.mesh.header.exception.BaseException;
import com.gcloud.mesh.header.msg.analysis.CreateFitterMsg;
import com.gcloud.mesh.header.msg.analysis.CreateSimulateMsg;
import com.gcloud.mesh.header.msg.analysis.DoFitterMsg;
import com.gcloud.mesh.header.msg.analysis.DoSimulateMsg;
import com.gcloud.mesh.header.msg.analysis.ListOptimumMsg;
import com.gcloud.mesh.header.msg.analysis.ListSimulateMsg;
import com.gcloud.mesh.header.msg.analysis.OptimumMsg;
import com.gcloud.mesh.header.msg.analysis.UpdateFitterMsg;
import com.gcloud.mesh.header.msg.analysis.UpdateSimulateMsg;
import com.gcloud.mesh.header.vo.analysis.AirFitterVo;
import com.gcloud.mesh.header.vo.analysis.FitterVo;
import com.gcloud.mesh.header.vo.analysis.NodeFitterVo;
import com.gcloud.mesh.header.vo.analysis.NodeHistoryVo;
import com.gcloud.mesh.header.vo.analysis.NodeSimulateVo;
import com.gcloud.mesh.header.vo.analysis.OptimumVo;
import com.gcloud.mesh.header.vo.analysis.SimulateVo;

public interface IIntelligentDeviceService {

    public String createFitter(CreateFitterMsg msg, String userId) throws BaseException;
    
    public FitterVo doFitter(DoFitterMsg msg, String userId) throws BaseException;
	
	public String updateFitter(UpdateFitterMsg msg, String userId) throws BaseException;
	
	public FitterVo detailFitter(String id, String userId) throws BaseException;
	
	public PageResult<NodeFitterVo> pageNodeFitter(int pageNum, int pageSize, String datacenterId, String name, String userId) throws BaseException;
	
	public PageResult<AirFitterVo> pageAirFitter(int pageNum, int pageSize, String datacenterId, String name, String userId) throws BaseException;
	
	public String deleteFitter(String id, String userId) throws BaseException;
	
	public String createSimulate(CreateSimulateMsg msg, String userId) throws BaseException;
	
	public SimulateVo doSimulate(DoSimulateMsg msg, String userId) throws BaseException;
	
	public String updateSimulate(UpdateSimulateMsg msg, String userId) throws BaseException;
	
	public PageResult<SimulateVo> pageSimulate(int pageNum, int pageSize, String name, String userId) throws BaseException;
	
	public String deleteSimulate(String id, String userId) throws BaseException;
	
	public List<SimulateEntity> listSimulate(ListSimulateMsg msg) throws BaseException;
	
	public List<OptimumVo> listOptimum(ListOptimumMsg msg, String userId) throws BaseException;
	
	public OptimumVo optimum(OptimumMsg msg, String userId) throws BaseException;
	
	public PageResult<NodeHistoryVo> pageNodeHistory(int pageNum, int pageSize, String name, String userId) throws BaseException;
	
	public PageResult<NodeSimulateVo> pageNodeSimulate(int pageNum, int pageSize, String name, String userId) throws BaseException;

}
