package com.gcloud.mesh.analysis.ml;

import com.gcloud.mesh.header.vo.analysis.DataVo;

public interface IFitterOptimizer {

	public DataVo getOptimum();

}