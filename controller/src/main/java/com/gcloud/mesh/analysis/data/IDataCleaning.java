package com.gcloud.mesh.analysis.data;

import java.util.List;

import com.gcloud.mesh.header.vo.analysis.DataVo;

public interface IDataCleaning {
	
	public double[] getX();
	
	public double[] getY();
	
	public List<DataVo> getData();
	
	public void setData(List<DataVo> data);

}
