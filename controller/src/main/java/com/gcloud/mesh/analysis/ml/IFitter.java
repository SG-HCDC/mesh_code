package com.gcloud.mesh.analysis.ml;

import com.gcloud.mesh.analysis.data.IDataCleaning;

public interface IFitter {
	
	public double[] train(IDataCleaning data, int degree);
	
	public IFitterOptimizer getOptimum(double[] factors, int degree);
	
}
