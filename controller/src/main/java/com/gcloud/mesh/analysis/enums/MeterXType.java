package com.gcloud.mesh.analysis.enums;

import java.util.stream.Stream;

import com.gcloud.mesh.header.enums.MonitorMeter;

public enum MeterXType {

	SERVER_POWER_CONSUMPTION("server_power_consumption", "服务器功耗(W)", MonitorMeter.SERVER_POWER),
	AIR_IN_TEMP("air_in_temp", "空调回风温度(°C)", MonitorMeter.AIR_IN_TEMPERATURE),
	AIR_OUT_TEMP("air_out_temp", "空调出风温度(°C)", MonitorMeter.AIR_OUT_TEMPERATURE);
	
	
	private String name;
	private String cnName;
	private MonitorMeter meter;
	
	MeterXType(String name, String cnName, MonitorMeter meter) {
		this.name = name;
		this.cnName = cnName;
		this.meter = meter;
	}
	
	public static MeterXType getTypeByName(String name) {
		return Stream.of(MeterXType.values())
				.filter( s -> s.getName().equals(name))
				.findFirst()
				.orElse(null);
	}
	
	public String getName() {
		return this.name;
	}
	
	public String getCnName() {
		return this.cnName;
	}
	
	public MonitorMeter getMeter() {
		return this.meter;
	}
}
