package com.gcloud.mesh.analysis.entity;

import java.util.Date;

import com.gcloud.framework.db.jdbc.annotation.ID;
import com.gcloud.framework.db.jdbc.annotation.Table;

import lombok.Data;

@Table( name = "analysis_simulate_datacenters")
@Data
public class SimulateEntity {

	@ID
	private String id;
	private String name;
	private String datacenterId;
	private String deviceId;
	private String metricX;
	private Integer status;
	private Integer degree;
	private String result;
	private Date startTime;
	private Date endTime;
	private Double historyMetric;
	private Double historyValue;
	private Double simulateMetric;
	private Double simulateValue;
	private Boolean enabled;
	private Date updateTime;
}
