package com.gcloud.mesh.analysis.entity;

import java.util.Date;

import com.gcloud.framework.db.jdbc.annotation.ID;
import com.gcloud.framework.db.jdbc.annotation.Table;

import lombok.Data;

@Table( name = "analysis_demos")
@Data
public class DemoEntity {
	
	@ID
	private String id;
	private Date timestamp;
	private Double dataX;
	private Double dataY;
	private String result;
	private String type;
	private String resourceId;

}
