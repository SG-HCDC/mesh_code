
package com.gcloud.mesh.analysis.controller;

import com.gcloud.framework.db.PageResult;
import com.gcloud.mesh.analysis.service.impl.SubhealthThresholdService;
import com.gcloud.mesh.dcs.annotation.ParamSecurity;
import com.gcloud.mesh.header.msg.analysis.*;
import com.gcloud.mesh.header.vo.analysis.*;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.jeecg.common.aspect.annotation.AutoLog;
import org.jeecg.common.constant.CommonConstant;
import org.jeecg.common.constant.enums.SysLogOperateType;
import org.jeecg.common.system.api.ISysBaseAPI;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;

@Api(tags = "亚健康")
@RestController
@RequestMapping("/subhealth")
public class SubhealthController {

	@Autowired
	private SubhealthThresholdService service;
	@Autowired
	private ISysBaseAPI sysBaseAPI;
	
	//处理Mass Assignment: Insecure Binder Configuration
	@InitBinder
	public void populateCustomerRequest(WebDataBinder binder) {
	    binder.setDisallowedFields(new String[]{});
	}

    @AutoLog(value = "查询空调亚健康分析列表",operateType = CommonConstant.OPERATE_TYPE_LIST, module = "健康智能监控-数据中心设备亚健康分析与预警-空调亚健康分析")
	@ApiOperation(value = "亚健康 - 空调列表", notes = "亚健康 - 空调列表")
	@RequestMapping(value = "/pageAir", method = RequestMethod.POST)
	@RequiresPermissions(value = {"subhealthPageAir"})
    @ParamSecurity(enabled = true)
	public PageResult<SubhealthAirVo> pageAir(@Valid PageAirSubhealthMsg msg) throws Exception {
		return service.pageAir(msg);
	}

    @AutoLog(value = "查询UPS亚健康分析列表",operateType = CommonConstant.OPERATE_TYPE_LIST, module = "健康智能监控-数据中心设备亚健康分析与预警-UPS亚健康分析")
	@ApiOperation(value = "亚健康 - UPS列表", notes = "亚健康 - UPS列表")
	@RequestMapping(value = "/pageUps", method = RequestMethod.POST)
	@RequiresPermissions(value = {"subhealthPageUps"})
    @ParamSecurity(enabled = true)
	public PageResult<SubhealthUpsVo> pageUps(@Valid PageUpsSubhealthMsg msg) throws Exception {
		return service.pageUps(msg);
	}

    //@AutoLog(value = "查询数据中心亚健康分析列表",operateType = CommonConstant.OPERATE_TYPE_LIST, module = "健康智能监控-数据中心设备亚健康分析与预警-数据中心亚健康分析")
	@ApiOperation(value = "亚健康 - 机房列表", notes = "亚健康 - 机房列表")
	@RequestMapping(value = "/listDatacenter", method = RequestMethod.POST)
	@RequiresPermissions(value = {"subhealthListDatacenter"})
    @ParamSecurity(enabled = true)
	public List<SubhealthDatacenterVo> listDatacenter(@Valid ListDatacenterSubhealthMsg msg) throws Exception {
		List<SubhealthDatacenterVo> vo =  service.listDatacenter(msg);
		if(msg.getTitle() != null) {
			sysBaseAPI.addLog(msg.getContent() != null?msg.getContent():"查看亚健康阈值设置页面", null, SysLogOperateType.DETAIL, null, null ,msg.getTitle() == null?"健康智能监控-数据中心设备亚健康分析与预警-亚健康阈值设置":msg.getTitle());
		}else {
			sysBaseAPI.addLog(msg.getContent() != null?msg.getContent():"查询数据中心亚健康分析列表", null, SysLogOperateType.QUERY, null, null ,msg.getTitle() == null?"健康智能监控-数据中心设备亚健康分析与预警-数据中心亚健康分析":msg.getTitle());
		}
		
		return vo;
	}

    @AutoLog(value = "查询数据中心亚健康分析列表",operateType = CommonConstant.OPERATE_TYPE_LIST, module = "健康智能监控-数据中心设备亚健康分析与预警-数据中心亚健康分析")
	@ApiOperation(value = "亚健康 - 机房分页列表", notes = "亚健康 - 机房分页列表")
	@RequestMapping(value = "/pageDatacenter", method = RequestMethod.POST)
	@RequiresPermissions(value = {"subhealthPageDatacenter"})
    @ParamSecurity(enabled = true)
	public PageResult<SubhealthDatacenterVo> pageDatacenter(@Valid PageDatacenterSubhealthMsg msg) throws Exception {
		return service.pageDatacenter(msg);
	}

    // @AutoLog(value = "设置阈值",operateType = CommonConstant.OPERATE_TYPE_CONFIG, module = "健康智能监控-数据中心设备亚健康分析与预警-亚健康阈值设置")
	@ApiOperation(value = "设置阈值", notes = "设置阈值")
	@RequestMapping(value = "/setThreshold", method = RequestMethod.POST)
	@RequiresPermissions(value = {"subhealthSetThreshold"})
    @ParamSecurity(enabled = true)
	public void setThreshold(@Valid SetSubhealthThresholdMsg msg) throws Exception {
		
		this.service.setThreshold(msg);
		sysBaseAPI.addLog("设置亚健康阈值"+msg.toString(), null, SysLogOperateType.UPDATE, null, null, "健康智能监控-数据中心设备亚健康分析与预警-亚健康阈值设置");
	}

    @AutoLog(value = "查询阈值列表",operateType = CommonConstant.OPERATE_TYPE_LIST, module = "健康智能监控-数据中心设备亚健康分析与预警-亚健康阈值设置")
	@ApiOperation(value = "阈值列表", notes = "阈值列表")
	@RequestMapping(value = "/listThreshold", method = RequestMethod.POST)
	@RequiresPermissions(value = {"subhealthListThreshold"})
    @ParamSecurity(enabled = true)
	public List<SubhealthDeviceThresholdVo> listThreshold(@Valid ListSubhealthThresholdMsg msg) throws Exception {
		return this.service.listThreshold(msg);
	}

    @AutoLog(value = "查询亚健康-阈值告警项列表",operateType = CommonConstant.OPERATE_TYPE_LIST, module = "健康智能监控-数据中心设备亚健康分析与预警-亚健康阈值设置")
	@ApiOperation(value = "亚健康 - 阈值告警项列表", notes = "亚健康 - 阈值告警项列表")
	@RequestMapping(value = "/listThresholdMeter", method = RequestMethod.POST)
	@RequiresPermissions(value = {"subhealthListThresholdMeter"})
    @ParamSecurity(enabled = true)
	public List<ThresholdMeterVo> listThresholdMeter(@Valid ListThresholdMeterMsg msg) throws Exception {
		return this.service.listThresholdMeter(msg);
	}

    @AutoLog(value = "查询亚健康-阈值告警级别列表",operateType = CommonConstant.OPERATE_TYPE_LIST, module = "健康智能监控-数据中心设备亚健康分析与预警-亚健康阈值设置")
	@ApiOperation(value = "亚健康 - 阈值告警级别列表", notes = "亚健康 - 阈值告警级别列表")
	@RequestMapping(value = "/listAlertLevel", method = RequestMethod.POST)
	@RequiresPermissions(value = {"subhealthListA_lertLevel"})
    @ParamSecurity(enabled = true)
	public List<AlertLevelVo> listAlertLevel() throws Exception {
		return this.service.listAlertLevel();
	}

    @AutoLog(value = "查看告警数量",operateType = CommonConstant.OPERATE_TYPE_DETAIL, module = "健康智能监控-数据中心设备亚健康分析与预警-亚健康预警")
	@ApiOperation(value = "告警数量", notes = "告警数量")
	@RequestMapping(value = "/countAlerts", method = RequestMethod.POST)
	@RequiresPermissions(value = {"subhealthCountA_lerts"})
    @ParamSecurity(enabled = true)
	public Integer countAlerts(@Valid CountAlertsMsg msg) throws Exception {
		return this.service.countAlerts(msg);
	}

    @AutoLog(value = "查看告警统计",operateType = CommonConstant.OPERATE_TYPE_DETAIL, module = "健康智能监控-数据中心设备亚健康分析与预警-亚健康预警")
	@ApiOperation(value = "告警统计", notes = "告警统计")
	@RequestMapping(value = "/alertStatistic", method = RequestMethod.POST)
	@RequiresPermissions(value = {"subhealthA_lertStatistic"})
    @ParamSecurity(enabled = true)
	public List<AlertStatisticVo> countAlerts(@Valid AlertStatisticMsg msg) throws Exception {
		return this.service.alertStatistic(msg);
	}

//    @AutoLog(value = "查看告警列表",operateType = CommonConstant.OPERATE_TYPE_LIST)
//	@ApiOperation(value = "告警分页", notes = "告警分页")
//	@RequestMapping(value = "/pageAlert", method = RequestMethod.POST)
//	@RequiresPermissions(value = {"subhealthPageA_lert"})
//	public PageResult<SubhealthAlertVo> pageAlert(@Valid PageAlertMsg msg) throws Exception {
//		return this.service.pageAlert(msg);
//	}
    
    @AutoLog(value = "查询告警列表",operateType = CommonConstant.OPERATE_TYPE_LIST, module = "健康智能监控-数据中心设备亚健康分析与预警-亚健康预警")
	@ApiOperation(value = "告警分页", notes = "告警分页")
	@RequestMapping(value = "/pageAlert", method = RequestMethod.POST)
	@RequiresPermissions(value = {"subhealthPageA_lert"})
    @ParamSecurity(enabled = true)
	public SubhealthAlertCountPageVo pageAlert(@Valid PageAlertMsg msg) throws Exception {
		return this.service.pageCountAndAlert(msg);
	}


}
