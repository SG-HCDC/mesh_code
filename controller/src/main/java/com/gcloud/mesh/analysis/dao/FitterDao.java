package com.gcloud.mesh.analysis.dao;

import java.util.HashMap;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.gcloud.framework.db.dao.impl.JdbcBaseDaoImpl;
import com.gcloud.mesh.analysis.entity.FitterEntity;

@Repository
public class FitterDao extends JdbcBaseDaoImpl<FitterEntity, String>{
	
	public <E> E getByNodeAndAirWithId(String nodeId, String airId, Class<E> clazz) {
		
		Map<String, Object> props = new HashMap<String, Object>();
		if(nodeId != null) {
			props.put("node_id", nodeId);
		}
		if(airId != null) {
			props.put("air_id", airId);
		}
		return this.findUniqueByProperties(props, clazz);
	}

}
