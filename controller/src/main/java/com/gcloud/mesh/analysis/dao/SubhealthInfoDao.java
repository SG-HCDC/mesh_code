
package com.gcloud.mesh.analysis.dao;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Repository;

import com.gcloud.framework.db.PageResult;
import com.gcloud.framework.db.dao.impl.JdbcBaseDaoImpl;
import com.gcloud.mesh.analysis.entity.SubhealthInfoEntity;

@Repository
public class SubhealthInfoDao extends JdbcBaseDaoImpl<SubhealthInfoEntity, String> {

    public <E> PageResult<E> pageAir(Integer pageNo, Integer pageSize, Class<E> clazz) {
        StringBuilder sb = new StringBuilder();
        List<Object> values = new ArrayList<>();

        sb.append("SELECT i.id, i.name, i.datacenter_id, IFNULL(s.subhealth, false) subhealth");
        sb.append(" FROM asset_iaas i");
        sb.append(" LEFT JOIN dcs_subhealth_infos s ON i.id=s.id");
        sb.append(" WHERE 1=1");
        sb.append(" AND type=1");

        return this.findBySql(sb.toString(), values, pageNo, pageSize, clazz);
    }

    public <E> PageResult<E> pageUps(Integer pageNo, Integer pageSize, Class<E> clazz) {
        StringBuilder sb = new StringBuilder();
        List<Object> values = new ArrayList<>();

        sb.append("SELECT i.id, i.name, IFNULL(s.subhealth, false) subhealth");
        sb.append(" FROM asset_iaas i");
        sb.append(" LEFT JOIN dcs_subhealth_infos s ON i.id=s.id");
        sb.append(" WHERE 1=1");
        sb.append(" AND type=2");

        return this.findBySql(sb.toString(), values, pageNo, pageSize, clazz);
    }
    
    public <E> PageResult<E> pageDatacenter(Integer pageNo, Integer pageSize, Class<E> clazz) {
        StringBuilder sb = new StringBuilder();
        sb.append(" SELECT i.id id, i.device_type, i.subhealth, t.meter, t.meter_name, t.level, t.upper_threshold, t.lower_threshold FROM dcs_subhealth_infos i left join dcs_subhealth_thresholds t on i.id = t.resource_id ");
        sb.append(" WHERE 1=1");
        sb.append(" AND i.device_type = 'DATACENTER'");
     
        return this.findBySql(sb.toString(), pageNo, pageSize, clazz);
    }
    
    public <E> List<E> listDatacenter(Class<E> clazz) {
        StringBuilder sb = new StringBuilder();
        
//        sb.append(" SELECT d.id id, IFNULL(i.subhealth, false) subhealth, t.meter, t.meter_name, t.level, t.upper_threshold, t.lower_threshold FROM asset_datacenters d"); 
//        sb.append(" LEFT JOIN dcs_subhealth_infos i on i.id = d.id");
//        sb.append(" LEFT JOIN dcs_subhealth_thresholds t on i.id = t.resource_id");

        sb.append(" SELECT i.id id, i.device_type, i.subhealth, t.meter, t.meter_name, t.level, t.upper_threshold, t.lower_threshold FROM dcs_subhealth_infos i left join dcs_subhealth_thresholds t on i.id = t.resource_id ");
        sb.append(" WHERE 1=1");
        sb.append(" AND i.device_type = 'DATACENTER'");

        return this.findBySql(sb.toString(), clazz);
    }

}
