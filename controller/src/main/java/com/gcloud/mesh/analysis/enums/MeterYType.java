package com.gcloud.mesh.analysis.enums;

import java.util.stream.Stream;

import com.gcloud.mesh.header.enums.MonitorMeter;

public enum MeterYType {

	SERVER_POWER_CONSUMPTION("server_power_consumption", "服务器功耗", MonitorMeter.SERVER_POWER),
	PUE("pue", "数据中心PUE", MonitorMeter.PUE);
	
	private String name;
	private String cnName;
	private MonitorMeter meter;
	
	MeterYType(String name, String cnName, MonitorMeter meter) {
		this.name = name;
		this.cnName = cnName;
		this.meter = meter;
	}
	
	public static MeterYType getTypeByName(String name) {
		return Stream.of(MeterYType.values())
				.filter( s -> s.getName().equals(name))
				.findFirst()
				.orElse(null);
	}
	
	public String getName() {
		return this.name;
	}
	
	public String getCnName() {
		return this.cnName;
	}
	
	public MonitorMeter getMeter() {
		return this.meter;
	}
}
