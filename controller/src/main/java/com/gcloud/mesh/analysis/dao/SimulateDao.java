package com.gcloud.mesh.analysis.dao;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Repository;
import org.springframework.util.StringUtils;

import com.gcloud.framework.db.PageResult;
import com.gcloud.framework.db.dao.impl.JdbcBaseDaoImpl;
import com.gcloud.mesh.analysis.entity.SimulateEntity;


@Repository
public class SimulateDao extends JdbcBaseDaoImpl<SimulateEntity, String>{

	public <E> PageResult<E> page(int pageNum, int pageSize, String name, String userId, Class<E> clazz) {
		
		StringBuffer sql = new StringBuffer();
		sql.append(" select d.* from asset_iaas i ");
		sql.append(" left join analysis_simulate_datacenters d on i.id = d.device_id ");
		sql.append(" where 1=1");
		sql.append(" and i.type=6");
		sql.append(" and i.visible=1");
			
		List<Object> values = new ArrayList<Object>();
		if(!StringUtils.isEmpty(userId)) {
			sql.append(" and d.creator = ?");
			values.add(userId);
		}
		if(!StringUtils.isEmpty(name)) {
			sql.append(" and d.name like concat('%', ?, '%')");
			values.add(name);
		}
		
		sql.append(" order by d.update_time");
		
		return this.findBySql(sql.toString(), values, pageNum, pageSize, clazz);	
	}
	
	public void updateSimulateBatch(String datacenterId, double value) {
		
		StringBuilder sql = new StringBuilder();
		sql.append(" update analysis_simulate_datacenters set ");
		sql.append(" simulate_value=" + value);
		sql.append(" where 1=1");
		sql.append(" and datacenter_id="+"\'"+datacenterId+"\'");
		this.jdbcTemplate.execute(sql.toString());
	}
}
