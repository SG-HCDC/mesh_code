package com.gcloud.mesh.analysis.enums;

import java.util.stream.Stream;

public enum AlgorithmType {

	POLYNOMIAL("polynomial", "多项式拟合");
	
	private String name;
	private String cnName;
	
	AlgorithmType(String name, String cnName) {
		this.name = name;
		this.cnName = cnName;
	}
	
	public static AlgorithmType getTypeByName(String name) {
		return Stream.of(AlgorithmType.values())
				.filter( s -> s.getName().equals(name))
				.findFirst()
				.orElse(null);
	}
	
	public String getName() {
		return this.name;
	}
	
	public String getCnName() {
		return this.cnName;
	}
}
