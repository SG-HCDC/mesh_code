
package com.gcloud.mesh.analysis.entity;

import com.gcloud.framework.db.jdbc.annotation.ID;
import com.gcloud.framework.db.jdbc.annotation.Table;

import lombok.Data;

@Table(name = "dcs_subhealth_thresholds")
@Data
public class SubhealthThresholdEntity {
    
	@ID
    private String id;
    private String resourceId;
    private String deviceType;
    private String meter;
    private String meterName;
    private String level;
    private Float upperThreshold;
    private Float lowerThreshold;

}
