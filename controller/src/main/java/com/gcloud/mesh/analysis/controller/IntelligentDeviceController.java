package com.gcloud.mesh.analysis.controller;

import com.gcloud.framework.db.PageResult;
import com.gcloud.mesh.analysis.service.IIntelligentDeviceService;
import com.gcloud.mesh.dcs.annotation.ParamSecurity;
import com.gcloud.mesh.header.msg.BaseReplyMsg;
import com.gcloud.mesh.header.msg.analysis.*;
import com.gcloud.mesh.header.vo.analysis.*;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.jeecg.common.aspect.annotation.AutoLog;
import org.jeecg.common.constant.CommonConstant;
import org.jeecg.common.constant.enums.SysLogOperateType;
import org.jeecg.common.system.api.ISysBaseAPI;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@Api(tags = "健康智能监控")
@RestController
@RequestMapping("/analysis")
public class IntelligentDeviceController {

	@Autowired
	private IIntelligentDeviceService service;

	private String userId = null;
	@Autowired
	private ISysBaseAPI sysBaseAPI;
	
	//处理Mass Assignment: Insecure Binder Configuration
	@InitBinder
	public void populateCustomerRequest(WebDataBinder binder) {
	    binder.setDisallowedFields(new String[]{});
	}

    // @AutoLog(value = "查看服务器数据分析",operateType = CommonConstant.OPERATE_TYPE_LIST, module = "健康智能监控-数据中心关键设备节点传导特性拟合")
	@ApiOperation(value = "服务器列表", notes = "服务器列表")
	@RequestMapping(value = "/pageNodeFitter", method = RequestMethod.POST)
	@RequiresPermissions(value = {"analysisPageNodeFitter"})
    @ParamSecurity(enabled = true)
	public PageResult<NodeFitterVo> pageNodeFitter(@Valid PageNodeFitterMsg msg) throws Exception {
		PageResult<NodeFitterVo> reply = service.pageNodeFitter(msg.getPageNo(), msg.getPageSize(), msg.getDatacenterId(), msg.getName(),
				userId);
		sysBaseAPI.addLog((msg.getContent() != null?msg.getContent():"拟合"), null, SysLogOperateType.DETAIL, null, null ,msg.getTitle() == null?"健康智能监控-数据中心关键设备节点传导特性拟合":msg.getTitle());
		return reply;
	}

    // @AutoLog(value = "查看空调数据分析",operateType = CommonConstant.OPERATE_TYPE_LIST, module = "健康智能监控-数据中心关键设备节点传导特性拟合")
	@ApiOperation(value = "空调列表", notes = "空调列表")
	@RequestMapping(value = "/pageAirFitter", method = RequestMethod.POST)
	@RequiresPermissions(value = {"analysisPageAirFitter"})
    @ParamSecurity(enabled = true)
	public PageResult<AirFitterVo> pageAirFitter(@Valid PageAirFitterMsg msg) throws Exception {
		PageResult<AirFitterVo> reply = service.pageAirFitter(msg.getPageNo(), msg.getPageSize(), msg.getDatacenterId(), msg.getName(),
				userId);
		
		return reply;
	}

    // @AutoLog(value = "拟合",operateType = CommonConstant.OPERATE_TYPE_DETAIL, module = "健康智能监控-数据中心关键设备节点传导特性拟合")
	@ApiOperation(value = "拟合", notes = "拟合")
	@RequestMapping(value = "/doFitter", method = RequestMethod.POST)
	@RequiresPermissions(value = {"analysisDoFitter"})
    @ParamSecurity(enabled = true)
	public BaseReplyMsg doFitter(@Valid DoFitterMsg msg) throws Exception {
		FitterVo vo = service.doFitter(msg, userId);
		BaseReplyMsg reply = new BaseReplyMsg();
		reply.setData(vo);
		sysBaseAPI.addLog((msg.getContent() != null?msg.getContent():"拟合")+("，")+msg.toString(), null, SysLogOperateType.DETAIL, null, null ,msg.getTitle() == null ? "健康智能监控-数据中心关键设备节点传导特性拟合":msg.getTitle());
		return reply;
	}

    // @AutoLog(value = "查看基于能耗传导的数据中心运行仿真",operateType = CommonConstant.OPERATE_TYPE_DETAIL,  module = "健康智能监控-基于能耗传导的数据中心运行仿真")
	@ApiOperation(value = "仿真", notes = "仿真")
	@RequestMapping(value = "/doSimulate", method = RequestMethod.POST)
	@RequiresPermissions(value = {"analysisDoSimulate"})
    @ParamSecurity(enabled = true)
	public BaseReplyMsg doSimulate(@Valid DoSimulateMsg msg) throws Exception {
		SimulateVo vo = service.doSimulate(msg, userId);
		BaseReplyMsg reply = new BaseReplyMsg();
		reply.setData(vo);
		sysBaseAPI.addLog((msg.getContent() != null?msg.getContent():"仿真")+("，")+msg.toString(), null, SysLogOperateType.DETAIL, null, null ,msg.getTitle() == null ? "健康智能监控-基于能耗传导的数据中心运行仿真":msg.getTitle());
		return reply;
	}

    // @AutoLog(value = "查看最优策略建议",operateType = CommonConstant.OPERATE_TYPE_DETAIL, module = "健康智能监控-设备运行情况分析模型的最优策略建议-最优策略建议")
	@ApiOperation(value = "历史最优PUE建议", notes = "历史最优PUE建议")
	@RequestMapping(value = "/optimum", method = RequestMethod.POST)
	@RequiresPermissions(value = {"analysisOptimum"})
    @ParamSecurity(enabled = true)
	public BaseReplyMsg optimum(@Valid OptimumMsg msg) throws Exception {
		OptimumVo result = service.optimum(msg, userId);
		BaseReplyMsg reply = new BaseReplyMsg();
		reply.setData(result);
		sysBaseAPI.addLog((msg.getContent() != null?msg.getContent():"查看最优策略建议")+("")+msg.toString(), null, SysLogOperateType.DETAIL, null, null ,msg.getTitle() == null ? "健康智能监控-设备运行情况分析模型的最优策略建议-最优策略建议":msg.getTitle());
		return reply;
	}

    @AutoLog(value = "查询历史最优策略建议列表",operateType = CommonConstant.OPERATE_TYPE_LIST, module = "健康智能监控-设备运行情况分析模型的最优策略建议-历史最优策略建议")
	@ApiOperation(value = "节点历史最优策略列表", notes = "节点历史最优策略列表")
	@RequestMapping(value = "/pageNodeHistory", method = RequestMethod.POST)
	@RequiresPermissions(value = {"analysisPageNodeHistory"})
    @ParamSecurity(enabled = true)
	public PageResult<NodeHistoryVo> pageNodeHistory(@Valid PageNodeHistoryMsg msg) throws Exception {
		PageResult<NodeHistoryVo> reply = service.pageNodeHistory(msg.getPageNo(), msg.getPageSize(), msg.getName(),
				userId);
		return reply;
	}

    // @AutoLog(value = "查询最优策略仿真列表",operateType = CommonConstant.OPERATE_TYPE_LIST, module = "健康智能监控-设备运行情况分析模型的最优策略建议-历史最优策略仿真")
	@ApiOperation(value = "节点历史最优策略仿真列表", notes = "节点历史最优策略仿真列表")
	@RequestMapping(value = "/pageNodeSimulate", method = RequestMethod.POST)
	@RequiresPermissions(value = {"analysisPageNodeSimulate"})
    @ParamSecurity(enabled = true)
	public PageResult<NodeSimulateVo> pageNodeSimulate(@Valid PageNodeSimulateMsg msg) throws Exception {
		PageResult<NodeSimulateVo> reply = service.pageNodeSimulate(msg.getPageNo(), msg.getPageSize(), msg.getName(),
				userId);
		sysBaseAPI.addLog(msg.getContent() != null?msg.getContent():"查看历史最优策略仿真", null, SysLogOperateType.DETAIL, null, null ,"健康智能监控-设备运行情况分析模型的最优策略建议-历史最优策略仿真");
		return reply;
	}

}
