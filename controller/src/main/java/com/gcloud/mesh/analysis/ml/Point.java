package com.gcloud.mesh.analysis.ml;

import java.math.BigDecimal;

import lombok.Data;

@Data
public class Point {
	
	public Point() {
		
	}
	
	public Point(BigDecimal x, BigDecimal y) {
		this.x = x;
		this.y = y;
	}
	
	public Point(double x, double y) {
		this.x = new BigDecimal(x);
		this.y = new BigDecimal(y);
	}
	
	private BigDecimal x;
	private BigDecimal y;

}
