package com.gcloud.mesh.analysis.enums;

import java.util.stream.Stream;

public enum ChartType {

	SCATTER("scatter", "散点图"),
	LINE("line", "曲线图");
	
	private String name;
	private String cnName;
	
	ChartType(String name, String cnName) {
		this.name = name;
		this.cnName = cnName;
	}
	
	public static ChartType getTypeByName(String name) {
		return Stream.of(ChartType.values())
				.filter( s -> s.getName().equals(name))
				.findFirst()
				.orElse(null);
	}
	
	public String getName() {
		return this.name;
	}
	
	public String getCnName() {
		return this.cnName;
	}
}
