package com.gcloud.mesh.testmodel;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.Data;
@Data
public class NodeItemVoModel {
	@JsonInclude(JsonInclude.Include.NON_NULL)
	private String id;
	@JsonInclude(JsonInclude.Include.NON_NULL)
	private String name;
	@JsonInclude(JsonInclude.Include.NON_NULL)
	private String esn;
	@JsonInclude(JsonInclude.Include.NON_NULL)
	private Integer type;
	@JsonInclude(JsonInclude.Include.NON_NULL)
	private String typeCnName;
	@JsonInclude(JsonInclude.Include.NON_NULL)
	private String deviceId;
	@JsonInclude(JsonInclude.Include.NON_NULL)
	private String deviceModel;
	@JsonInclude(JsonInclude.Include.NON_NULL)
	private String deviceManufacturer;
	@JsonInclude(JsonInclude.Include.NON_NULL)
	private String datacenterId;
	@JsonInclude(JsonInclude.Include.NON_NULL)
	private String datacenterName;
	@JsonInclude(JsonInclude.Include.NON_NULL)
	private String datacenterIp;
//	private String creator;
	@JsonInclude(JsonInclude.Include.NON_NULL)
	private Date createTime;
	@JsonInclude(JsonInclude.Include.NON_NULL)
	private String mgmIp;
	@JsonInclude(JsonInclude.Include.NON_NULL)
	private String ipmiIp;
	@JsonInclude(JsonInclude.Include.NON_NULL)
	private String ipmiUser;
	@JsonInclude(JsonInclude.Include.NON_NULL)
	private String ipmiPassword;
	@JsonInclude(JsonInclude.Include.NON_NULL)
	private Integer networkStatus;
	@JsonInclude(JsonInclude.Include.NON_NULL)
	private String networkStatusCnName;
	@JsonInclude(JsonInclude.Include.NON_NULL)
	private Integer powerStatus;
	@JsonInclude(JsonInclude.Include.NON_NULL)
	private String powerStatusCnName;
	@JsonInclude(JsonInclude.Include.NON_NULL)
	private String hostname;
	@JsonInclude(JsonInclude.Include.NON_NULL)
	private Float ratedPower;
	@JsonInclude(JsonInclude.Include.NON_NULL)
	private Float powerConsumption; 
	@JsonInclude(JsonInclude.Include.NON_NULL)
	private Float energyEfficiencyRatio;
	@JsonInclude(JsonInclude.Include.NON_NULL)
	private Float storageCapacity;
	@JsonInclude(JsonInclude.Include.NON_NULL)
	private Boolean isolation;
}
