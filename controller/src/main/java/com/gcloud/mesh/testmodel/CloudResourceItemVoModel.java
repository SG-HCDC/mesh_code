package com.gcloud.mesh.testmodel;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.Data;
@Data
public class CloudResourceItemVoModel {
	@JsonInclude(JsonInclude.Include.NON_NULL)
	private String id;
	@JsonInclude(JsonInclude.Include.NON_NULL)
	private String name;
	@JsonInclude(JsonInclude.Include.NON_NULL)
	private Integer type;
	@JsonInclude(JsonInclude.Include.NON_NULL)
	private String typeName;
	@JsonInclude(JsonInclude.Include.NON_NULL)
	private String datacenterId;
	@JsonInclude(JsonInclude.Include.NON_NULL)
	private String datacenterName;
	@JsonInclude(JsonInclude.Include.NON_NULL)
	private Date createTime;
	@JsonInclude(JsonInclude.Include.NON_NULL)
	private String nodeId;
	@JsonInclude(JsonInclude.Include.NON_NULL)
	private String nodeName;
	@JsonInclude(JsonInclude.Include.NON_NULL)
	private String from;
}
