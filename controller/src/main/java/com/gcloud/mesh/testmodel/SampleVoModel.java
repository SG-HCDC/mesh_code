package com.gcloud.mesh.testmodel;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.Data;
@Data
public class SampleVoModel {
	@JsonInclude(JsonInclude.Include.NON_NULL)
	private String resourceType;
	@JsonInclude(JsonInclude.Include.NON_NULL)
	private String resourceId;
	@JsonInclude(JsonInclude.Include.NON_NULL)
	private String resourceInstance;
	@JsonInclude(JsonInclude.Include.NON_NULL)
	private String meter;
	@JsonInclude(JsonInclude.Include.NON_NULL)
	private String unit;
	@JsonInclude(JsonInclude.Include.NON_NULL)
	private String host;
	@JsonInclude(JsonInclude.Include.NON_NULL)
	private String platformType;
	@JsonInclude(JsonInclude.Include.NON_NULL)
	private String regionId;
	@JsonInclude(JsonInclude.Include.NON_NULL)
	private Date createTime;
	@JsonInclude(JsonInclude.Include.NON_NULL)
	private Long timestamp;
	@JsonInclude(JsonInclude.Include.NON_NULL)
	private Double volume;
}
