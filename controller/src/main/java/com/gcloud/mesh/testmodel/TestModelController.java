package com.gcloud.mesh.testmodel;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.jeecg.common.aspect.annotation.AutoLog;
import org.jeecg.common.constant.CommonConstant;
import org.jeecg.common.system.api.ISysBaseAPI;
import org.jeecg.common.system.util.JwtUtil;
import org.jeecg.common.system.vo.LoginUser;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.gcloud.mesh.asset.dao.CloudResourceDao;
import com.gcloud.mesh.asset.entity.CloudResourceEntity;
import com.gcloud.mesh.asset.service.IAssetService;
import com.gcloud.mesh.asset.service.ICloudResourceService;
import com.gcloud.mesh.dcs.service.VoltageFmService;
import com.gcloud.mesh.header.msg.BaseReplyMsg;
import com.gcloud.mesh.header.msg.asset.DetailDeviceMsg;
import com.gcloud.mesh.header.msg.asset.DetailNodeMsg;
import com.gcloud.mesh.header.msg.dcs.SetVoltageFmStrategyMsg;
import com.gcloud.mesh.header.msg.monitor.LatestSampleMsg;
import com.gcloud.mesh.header.vo.asset.CloudResourceItemVo;
import com.gcloud.mesh.header.vo.asset.DeviceItemVo;
import com.gcloud.mesh.header.vo.asset.NodeItemVo;
import com.gcloud.mesh.header.vo.monitor.SampleVo;
import com.gcloud.mesh.monitor.service.StatisticsCheatService;

import io.swagger.annotations.ApiOperation;


@RestController
@RequestMapping("/model")
public class TestModelController {
	private String userId = null;

	private static final String HEADER_TOKEN = "X-Access-Token";
	@Autowired
	private ICloudResourceService cloudResourceService;
	
	@Autowired
	private IAssetService service;
	
	@Autowired
	private StatisticsCheatService cheatService;

	@Autowired
	private CloudResourceDao cloudResourceDao;
	
	@Autowired
	private ISysBaseAPI sysBaseAPI;
	
	@Autowired
	private VoltageFmService fmservice;
	
	//处理Mass Assignment: Insecure Binder Configuration
	@InitBinder
	public void populateCustomerRequest(WebDataBinder binder) {
	    binder.setDisallowedFields(new String[]{});
	}
	
	@ApiOperation(value = "虚拟机详情", notes = "虚拟机详情")
	@RequestMapping(value = "/vm_detail", method = RequestMethod.POST)
	public BaseReplyMsg vmDetail(String id) throws Exception {
		CloudResourceItemVoModel voModel = new CloudResourceItemVoModel();
		CloudResourceItemVo vo = cloudResourceService.detail(id);
		if(vo != null) {
			BeanUtils.copyProperties(vo, voModel);
		}
		BaseReplyMsg msg = new BaseReplyMsg();
		msg.setErrorCode("nothing");
		msg.setData(voModel);
		return msg;
	}
	@ApiOperation(value = "容器详情", notes = "容器详情")
	@RequestMapping(value = "/k8s_detail", method = RequestMethod.POST)
	public BaseReplyMsg k8sDetail(String id) throws Exception {
		CloudResourceItemVoModel voModel = new CloudResourceItemVoModel();
		CloudResourceItemVo vo =  cloudResourceService.detail(id);
		if(vo != null) {
			BeanUtils.copyProperties(vo, voModel);
		}
		BaseReplyMsg msg = new BaseReplyMsg();
		msg.setErrorCode("nothing");
		msg.setData(voModel);
		return msg;
	}
    // 模拟接口
	@ApiOperation(value = "迁移虚拟机", notes = "迁移虚拟机")
	@RequestMapping(value = "/migrate_vm", method = RequestMethod.GET)
	public BaseReplyMsg migrateVm(String id, String dataCenterId) throws Exception {
		CloudResourceEntity entity = cloudResourceDao.getById(id);
		if(entity != null) {
			entity.setDatacenterId(dataCenterId);
			cloudResourceDao.update(entity);
		}
		BaseReplyMsg msg = new BaseReplyMsg();
		msg.setErrorCode("nothing");
		msg.setData(true);
		return msg;
	}
	
	 // 模拟接口
	@ApiOperation(value = "迁移容器", notes = "迁移容器")
	@RequestMapping(value = "/migrate_k8s", method = RequestMethod.GET)
	public BaseReplyMsg migrateK8s(String id, String dataCenterId) throws Exception {
		CloudResourceEntity entity = cloudResourceDao.getById(id);
		if(entity != null) {
			entity.setDatacenterId(dataCenterId);
			cloudResourceDao.update(entity);
		}
		BaseReplyMsg msg = new BaseReplyMsg();
		msg.setErrorCode("nothing");
		msg.setData(true);
		return msg;
	}
	@ApiOperation(value = "最新一类数据", notes = "最新一类数据")
	@RequestMapping(value = "latestSamples", method = RequestMethod.POST)
	public BaseReplyMsg latestSamples(LatestSampleMsg request) throws Exception {
		List<SampleVo> res = this.cheatService.latestSamples(request.getMeter(), request.getResourceId());
		List<SampleVoModel> models = new ArrayList<>();
		for(SampleVo vo : res) {
			SampleVoModel model = new SampleVoModel();
			BeanUtils.copyProperties(vo, model);
			models.add(model);
		}
		BaseReplyMsg msg = new BaseReplyMsg();
		msg.setErrorCode("nothing");
		msg.setData(models);
		return msg;
	}
	@RequestMapping(value = "/detailNode", method = RequestMethod.POST)
	public BaseReplyMsg detailNode(@Valid DetailNodeMsg msg, HttpServletRequest request, HttpServletResponse response) throws Exception {
		NodeItemVoModel model = new NodeItemVoModel();
		NodeItemVo vo = service.detailNode(msg.getId(), getUser(request, response));
		BeanUtils.copyProperties(vo, model);
		BaseReplyMsg res = new BaseReplyMsg();
		res.setErrorCode("nothing");
		res.setData(model);
		return res;
	}
    private String getUser(HttpServletRequest request, HttpServletResponse response) {
    	String token = request.getHeader(HEADER_TOKEN);  	
    	String userId = null;
    	try {
    		String username = JwtUtil.getUsername(token);
    		LoginUser sysUser = sysBaseAPI.getUserByName(username);
    		userId = sysUser.getId();
    	} catch(Exception e) {
    	}	
		return userId;
    }
	@RequestMapping(value = "/detailDevice", method = RequestMethod.POST)
	public BaseReplyMsg detailDevice(@Valid DetailDeviceMsg msg) throws Exception {
		DeviceItemVoModel model = new DeviceItemVoModel();
		DeviceItemVo vo = service.detailDevice(msg.getId(), userId);
		BeanUtils.copyProperties(vo, model);
		BaseReplyMsg res = new BaseReplyMsg();
		res.setErrorCode("nothing");
		res.setData(model);
		return res;
	}
	@RequestMapping(value = "/powerRate", method = RequestMethod.POST)
	public BaseReplyMsg powerRate(String id,  HttpServletResponse response) throws Exception {
		String res = this.cheatService.latestSample("server.power", id);
		BaseReplyMsg ress = new BaseReplyMsg();
		ress.setErrorCode("nothing");
		ress.setData(res);
		return ress;
	}
	@RequestMapping(value = "/setStrategy", method = RequestMethod.POST)
	public BaseReplyMsg setFmStrategy(@Valid SetVoltageFmStrategyMsg msg) {
		fmservice.setFmStrategy(msg);
		BaseReplyMsg ress = new BaseReplyMsg();
		ress.setErrorCode("nothing");
		ress.setData(true);
		return ress;
	}
}
