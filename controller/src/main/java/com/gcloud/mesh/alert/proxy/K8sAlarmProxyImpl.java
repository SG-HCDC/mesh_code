package com.gcloud.mesh.alert.proxy;

import java.util.Map;

import org.springframework.stereotype.Service;

import com.gcloud.mesh.header.vo.alert.HistoryVo;
import com.gcloud.mesh.header.vo.monitor.StatisticsResourceVo;

@Service("K8sAlarmProxyImpl")
public class K8sAlarmProxyImpl implements IAlarmProxy {

	@Override
	public void setAlarmHistory4ImportData(HistoryVo alarmHistoryVo, StatisticsResourceVo statistics) {
		alarmHistoryVo.setResourceId(statistics.getResourceId());
		alarmHistoryVo.setResourceName(statistics.getResourceName());
	}

	@Override
	public void setTemplatesParams(Map<String, String> params, StatisticsResourceVo statistics) {
	}

	@Override
	public void setRecoverTemplatesParams(Map<String, String> params, StatisticsResourceVo statistics) {
	}
}
