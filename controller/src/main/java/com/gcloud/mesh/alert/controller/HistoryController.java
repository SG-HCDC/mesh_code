package com.gcloud.mesh.alert.controller;

import com.gcloud.framework.db.PageResult;
import com.gcloud.mesh.alert.service.EmailService;
import com.gcloud.mesh.alert.service.HistoryService;
import com.gcloud.mesh.dcs.annotation.ParamSecurity;
import com.gcloud.mesh.header.msg.alert.CreateHistoryMsg;
import com.gcloud.mesh.header.msg.alert.FlowHistoryMsg;
import com.gcloud.mesh.header.msg.alert.PageHistoryMsg;
import com.gcloud.mesh.header.vo.alert.FlowHistoryVo;
import com.gcloud.mesh.header.vo.alert.HistoryVo;
import com.sgcc.isc.service.adapter.utils.StringUtil;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

import org.apache.commons.lang.StringUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.jeecg.common.aspect.annotation.AutoLog;
import org.jeecg.common.constant.CommonConstant;
import org.jeecg.common.constant.enums.SysLogOperateType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.jeecg.common.system.api.ISysBaseAPI;

import javax.validation.Valid;
import java.util.List;

@Api(tags = "告警历史记录管理")
@RestController
@RequestMapping("/history")
public class HistoryController {

	@Autowired
	private HistoryService service;

	@Autowired
	private EmailService emailService;

	@Autowired
	private ISysBaseAPI sysBaseAPI;
	
	//处理Mass Assignment: Insecure Binder Configuration
	@InitBinder
	public void populateCustomerRequest(WebDataBinder binder) {
	    binder.setDisallowedFields(new String[]{});
	}

   // @AutoLog(value = "查看告警历史记录列表",operateType = CommonConstant.OPERATE_TYPE_LIST, module = "告警管理")
	@ApiOperation(value = "告警历史记录分页", notes = "告警历史记录分页")
	@RequestMapping(value = "page", method = RequestMethod.POST)
	@RequiresPermissions(value = {"historyPage"})
	@ParamSecurity( enabled = true)
	public PageResult<HistoryVo> page(@Valid PageHistoryMsg request) throws Exception {
		PageResult<HistoryVo> res = this.service.page(request);
		if(!StringUtils.isBlank(request.getTitle())) {
			sysBaseAPI.addLog("查询诊断告警信息列表" , null, SysLogOperateType.QUERY, null, null, request.getTitle());
		}
		return res;
	}

//    @AutoLog(value = "新增告警历史记录",operateType = CommonConstant.OPERATE_TYPE_ADD)
	@ApiOperation(value = "新增告警历史记录", notes = "新增告警历史记录")
	@RequestMapping(value = "create", method = RequestMethod.POST)
	@RequiresPermissions(value = {"historyCreate"})
	@ParamSecurity( enabled = true)
	public void create(@Valid CreateHistoryMsg request) throws Exception {
		String id = this.service.create(request);
		// sysBaseAPI.addLog("更新云资源；告警历史记录ID:" + id, null, SysLogOperateType.ADD, null, null, "告警管理");
	}

    // @AutoLog(value = "告警历史记录列表",operateType = CommonConstant.OPERATE_TYPE_LIST, module = "告警管理")
	@ApiOperation(value = "告警历史记录列表", notes = "告警历史记录列表")
	@RequestMapping(value = "list", method = RequestMethod.POST)
	@RequiresPermissions(value = {"historyList"})
	@ParamSecurity( enabled = true)
	public List<HistoryVo> list() throws Exception {
		return this.service.list();
	}
	
	@RequestMapping(value = "flow", method = RequestMethod.POST)
	public PageResult<FlowHistoryVo> flowCount(FlowHistoryMsg request){
		return this.service.flowCount(request.getLevel(), request.getStartTime(), request.getEndTime());
	}

}
