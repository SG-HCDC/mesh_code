package com.gcloud.mesh.alert.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.gcloud.framework.db.PageResult;
import com.gcloud.mesh.alert.dao.HistoryDao;
import com.gcloud.mesh.alert.entity.HistoryEntity;
import com.gcloud.mesh.header.enums.AlertLevelType;
import com.gcloud.mesh.header.enums.MonitorMeter;
import com.gcloud.mesh.header.msg.alert.CreateHistoryMsg;
import com.gcloud.mesh.header.msg.alert.PageHistoryMsg;
import com.gcloud.mesh.header.vo.alert.FlowHistoryVo;
import com.gcloud.mesh.header.vo.alert.HistoryVo;

@Service
public class HistoryService {

	@Autowired
	HistoryDao historyDao;

	public String create(CreateHistoryMsg msg) {

		HistoryEntity entity = new HistoryEntity();
		entity.setCreateTime(new Date());
		entity.setId(UUID.randomUUID().toString());
		entity.setLevel(msg.getLevel());
		entity.setMeter(msg.getMeter());
		entity.setNotifyCount(msg.getNotifyCount());
		entity.setRegionId(msg.getRegionId());
		entity.setResourceName(msg.getResourceName());
		entity.setResourceType(msg.getResourceType());
		entity.setResourceId(msg.getResourceId());
		entity.setStatus(msg.getStatus());
		entity.setTitle(msg.getTitle());
		entity.setContent(msg.getContent());
		entity.setAlertTime(msg.getAlertTime());
		entity.setPlatformType(msg.getPlatformType());
		entity.setResourceInstance(msg.getResourceInstance());
		historyDao.save(entity);

		return entity.getId();
	}

	public PageResult<HistoryVo> page(PageHistoryMsg request) {

		PageResult<HistoryVo> pages = historyDao.page(request.getPageNo(), request.getPageSize(),
				request.getResourceTypes(), HistoryVo.class);
		for (HistoryVo vo : pages.getList()) {
			vo.setResourceType(MonitorMeter.getByMeter(vo.getMeter()).getType());
			vo.setMeter(MonitorMeter.getNameByMeter(vo.getMeter()));
			vo.setLevelCnName(AlertLevelType.getCnName(vo.getLevel()));
		}
		return pages;
	}

	public List<HistoryVo> list() {
		List<HistoryVo> historyVos = new ArrayList<HistoryVo>();

		for (HistoryEntity entity : historyDao.findAll()) {
			HistoryVo vo = new HistoryVo();
			vo.setCreateTime(entity.getCreateTime());
			vo.setId(entity.getId());
			vo.setLevel(entity.getLevel());
			vo.setMeter(entity.getMeter());
			vo.setNotifyCount(entity.getNotifyCount());
			vo.setRegionId(entity.getRegionId());
			vo.setResourceName(entity.getResourceName());
			vo.setResourceType(entity.getResourceType());
			vo.setResourceId(entity.getResourceId());
			vo.setStatus(entity.getStatus());
			vo.setTitle(entity.getTitle());
			vo.setContent(entity.getContent());
			vo.setAlertTime(entity.getAlertTime());
			vo.setResourceInstance(entity.getResourceInstance());
			vo.setPlatformType(entity.getPlatformType());

			historyVos.add(vo);
		}

		return historyVos;
	}

	public void clear(Date beforeDate) {
		this.historyDao.clear(beforeDate);
	}

	public int count(Integer level) {
		return this.historyDao.count(level);
	}
	
	public PageResult<FlowHistoryVo> flowCount(Integer level, String startTime, String endTime){
		return this.historyDao.flowConut(level, startTime, endTime);
	}
}
