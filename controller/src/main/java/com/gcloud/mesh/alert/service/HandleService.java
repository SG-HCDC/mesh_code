package com.gcloud.mesh.alert.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.gcloud.mesh.header.msg.alert.CreateHistoryMsg;
import com.gcloud.mesh.header.vo.alert.HistoryVo;
import com.gcloud.mesh.header.vo.alert.LinkmanVo;

@Service
public class HandleService {

	@Autowired
	HistoryService historyService;

	@Autowired
	LinkmanService linkmanService;

	@Autowired
	EmailService emailService;

	public void handleAlarms(List<HistoryVo> vos) {

		if (null != vos && !vos.isEmpty()) {
			for (HistoryVo vo : vos) {
				CreateHistoryMsg msg = new CreateHistoryMsg();
				msg.setAlertTime(vo.getAlertTime());
				msg.setContent(vo.getContent());
				msg.setLevel(vo.getLevel());
				msg.setMeter(vo.getMeter());
				msg.setNotifyCount(vo.getNotifyCount());
				msg.setPlatformType(vo.getPlatformType());
				msg.setRegionId(vo.getRegionId());
				msg.setResourceId(vo.getResourceId());
				msg.setResourceInstance(vo.getResourceInstance());
				msg.setResourceName(vo.getResourceName());
				msg.setResourceType(vo.getResourceType());
				msg.setStatus(vo.getStatus());
				msg.setTitle(vo.getTitle());

				this.historyService.create(msg);
			}
		}
	}

	public void sendMessage(String title, String content) {
		List<LinkmanVo> linkmanVos = this.linkmanService.list();

		if (linkmanVos != null && linkmanVos.size() > 0) {
			emailService.pushEmail(title, content, linkmanVos.get(0).getEmail());
		}
	}
}
