package com.gcloud.mesh.alert.dao;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Repository;

import com.gcloud.framework.db.PageResult;
import com.gcloud.framework.db.dao.impl.JdbcBaseDaoImpl;
import com.gcloud.mesh.alert.entity.PolicyEntity;

@Repository
public class PolicyDao extends JdbcBaseDaoImpl<PolicyEntity, String> {
	public <E> PageResult<E> page(int pageNo, int pageSize, String resourceType, Class<E> clazz) {
		StringBuilder sb = new StringBuilder();
		List<Object> values = new ArrayList<>();
		sb.append("SELECT p.* FROM alert_policys p LEFT JOIN alert_rules r ON p.id = r.policy_id ");
		sb.append(" WHERE 1 = 1");
		if (StringUtils.isNotBlank(resourceType)) {
    		sb.append(" AND r.resource_type = ? ");
			values.add(resourceType);
    	}
		sb.append(" GROUP BY p.id order by p.create_time DESC");
		return this.findBySql(sb.toString(), values, pageNo, pageSize, clazz);
	}
}
