package com.gcloud.mesh.alert.consts;

public class RedisKeyConsts {
	private static final String KEY_PREFIX = "mesh_controller";

	public static final class RedisKey {
		// 用户session key
		// public static final String ALARM_RULE_ID_METER_OBJECT_ID =
		// String.format("%s_%s", KEY_PREFIX,
		// "ruleId_{0}_meter_{1}_objectId_{2}_instance_{3}");
		public static final String ALARM_RULE_ID_METER_OBJECT_ID = String.format("%s_%s", KEY_PREFIX, "ruleId_{0}_meter_{1}_recourceId_{2}_host_{3}_instance_{4}");
		public static final String ALARM_COUNT_RULE_ID_METER_OBJECT_ID = String.format("%s_%s", KEY_PREFIX, "count_ruleId_{0}_meter_{1}_recourceId_{2}_host_{3}_instance_{4}");
		public static final String AlARM_PUSH_RULE_ID_METER_OBJECT_ID = String.format("%s_%s", KEY_PREFIX, "push_ruleId_{0}_meter_{1}_recourceId_{2}_host_{3}_instance_{4}");

		// 多云改造
		public static final String ALARM_PLATFORM_REGION_RULE_ID_METER_OBJECT_ID = String.format("%s_%s", KEY_PREFIX, "platform_{0}_region_{1}_ruleId_{2}_meter_{3}_recourceId_{4}_host_{5}_instance_{6}");
		public static final String ALARM_PLATFORM_REGION_COUNT_RULE_ID_METER_OBJECT_ID = String.format("%s_%s", KEY_PREFIX, "count_platform_{0}_region_{1}_ruleId_{2}_meter_{3}_recourceId_{4}_host_{5}_instance_{6}");
		public static final String ALARM_PLATFORM_REGION_PUSH_RULE_ID_METER_OBJECT_ID = String.format("%s_%s", KEY_PREFIX, "push_platform_{0}_region_{1}_ruleId_{2}_meter_{3}_recourceId_{4}_host_{5}_instance_{6}");

		// 假数据
		public static final String MOCK_DATA_TYPE_ID = String.format("%s_%s", KEY_PREFIX, "mock_type_{0}_id_{1}");
		
		public static final String MOCK_DATA_TYPE_SEARCH = String.format("%s_%s", KEY_PREFIX, "mock_type_{0}_id_*");
		
		public static final String MONITOR_DATA = String.format("%s_%s", KEY_PREFIX, "meter_{0}_id_{1}");
		
		//系统相关
		public static final String SYS_BEGIN_TIME =  "sys_begin_time";
		public static final String SYS_SERVICE_RESPONSE_TIME = "sys_service_response_time";
		public static final String SYS_USER_TOKEN = String.format("%s_%s", "prefix_user_token", "{0}");
		
		//调度相关
		public static final String SCHEDULER_COOLING = "scheduler_cooling";
	}

}
