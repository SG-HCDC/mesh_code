package com.gcloud.mesh.alert.entity;


import java.util.Date;

import com.gcloud.framework.db.jdbc.annotation.ID;
import com.gcloud.framework.db.jdbc.annotation.Table;

import lombok.Data;

@Data
@Table(name = "alert_sms")
public class SmsEntity {
	
	@ID
    private String id;
    private String smsPlatform;
    private String smsPlatformName;
    private String config;
    private boolean enable; 
    private Date createTime;
}
