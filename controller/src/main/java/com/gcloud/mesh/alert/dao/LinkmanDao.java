package com.gcloud.mesh.alert.dao;

import org.springframework.stereotype.Repository;

import com.gcloud.framework.db.PageResult;
import com.gcloud.framework.db.dao.impl.JdbcBaseDaoImpl;
import com.gcloud.mesh.alert.entity.LinkmanEntity;

@Repository
public class LinkmanDao extends JdbcBaseDaoImpl<LinkmanEntity, String>{
	public <E> PageResult<E> page(int pageNo, int pageSize, Class<E> clazz) {
		StringBuilder sb = new StringBuilder();
		sb.append("SELECT * FROM alert_linkman");
		return this.findBySql(sb.toString(), pageNo, pageSize, clazz);
	}
}
