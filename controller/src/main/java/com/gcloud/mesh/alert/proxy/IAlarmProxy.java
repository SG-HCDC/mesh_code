package com.gcloud.mesh.alert.proxy;

import java.util.Map;

import com.gcloud.mesh.header.vo.alert.HistoryVo;
import com.gcloud.mesh.header.vo.monitor.StatisticsResourceVo;

public interface IAlarmProxy {
	/**
	 * 给AlarmHistoryVo的其中4个重要的属性赋值，这4个属性是Host,ResourceId,Instance,ResourceName
	 * @param alarmHistoryVo 需要赋值AlarmHistoryVo对象
	 * @param statistics 数值的来源
	 */
	public void setAlarmHistory4ImportData(HistoryVo alarmHistoryVo, StatisticsResourceVo statistics);
	
	/**
	 * 给告警模板参数赋值
	 * @param params 需要赋值的模板的key-value
	 * @param statistics 数值的来源
	 */
	public void setTemplatesParams(Map<String, String> params, StatisticsResourceVo statistics);
	
	/**
	 * 给恢复告警模板参数赋值
	 * @param params 需要赋值的模板的key-value
	 * @param statistics 数值的来源
	 */
	public void setRecoverTemplatesParams(Map<String, String> params, StatisticsResourceVo statistics);
}
