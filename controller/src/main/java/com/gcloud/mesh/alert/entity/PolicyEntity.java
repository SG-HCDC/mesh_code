package com.gcloud.mesh.alert.entity;

import java.util.Date;

import com.gcloud.framework.db.jdbc.annotation.ID;
import com.gcloud.framework.db.jdbc.annotation.Table;

import lombok.Data;

@Data
@Table(name = "alert_policys")
public class PolicyEntity {
	@ID
	private String id;
	private String policyName;
	private Boolean enable;
	private Integer enableStartTime;
	private Integer enableEndTime;
	private Integer channelSilence;
	private Boolean notifyEnable;
	private Date createTime;
	private Date updateTime;

	private String platformType;
	private String regionId;
}
