package com.gcloud.mesh.alert.controller;

import com.gcloud.framework.db.PageResult;
import com.gcloud.mesh.alert.service.LinkmanService;
import com.gcloud.mesh.dcs.annotation.ParamSecurity;
import com.gcloud.mesh.header.msg.alert.*;
import com.gcloud.mesh.header.vo.alert.LinkmanVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

import org.apache.commons.lang.StringUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.jeecg.common.aspect.annotation.AutoLog;
import org.jeecg.common.constant.CommonConstant;
import org.jeecg.common.constant.enums.SysLogOperateType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.jeecg.common.system.api.ISysBaseAPI;

import javax.validation.Valid;
import java.util.List;

@Api(tags = "告警联系人管理")
@RestController
@RequestMapping("/linkman")
public class LinkmanController {

	@Autowired
	private LinkmanService service;

	@Autowired
	private ISysBaseAPI sysBaseAPI;
	
	//处理Mass Assignment: Insecure Binder Configuration
	@InitBinder
	public void populateCustomerRequest(WebDataBinder binder) {
	    binder.setDisallowedFields(new String[]{});
	}

    // @AutoLog(value = "查看联系人列表",operateType = CommonConstant.OPERATE_TYPE_LIST, module = "告警管理")
	@ApiOperation(value = "联系人分页", notes = "联系人分页")
	@RequestMapping(value = "page", method = RequestMethod.POST)
	@RequiresPermissions(value = {"linkmanPage"})
	@ParamSecurity( enabled = true)
	public PageResult<LinkmanVo> page(@Valid PageLinkmanMsg request) throws Exception {
		PageResult<LinkmanVo> res = this.service.page(request);
		if(!StringUtils.isBlank(request.getTitle())) {
			sysBaseAPI.addLog(request.getContent() != null?request.getContent():"查看联系人", null, SysLogOperateType.DETAIL, null, null, request.getTitle());
		}
		return res;
	}

//    @AutoLog(value = "新增联系人",operateType = CommonConstant.OPERATE_TYPE_ADD)
	@ApiOperation(value = "新增联系人", notes = "新增联系人")
	@RequestMapping(value = "create", method = RequestMethod.POST)
	@RequiresPermissions(value = {"linkmanCreate"})
	@ParamSecurity( enabled = true)
	public void create(@Valid CreateLinkmanMsg request) throws Exception {
		String id = this.service.create(request);
		if(!StringUtils.isBlank(request.getTitle())) {
			StringBuffer buff = new StringBuffer();
			buff.append(request.getContent() != null?request.getContent():"新增联系人").append("；ID:").append(id).append(",").append("联系人名称：").append(request.getUserName());
			buff.append(",邮箱：").append(request.getEmail()).append("");
			sysBaseAPI.addLog(buff.toString(), null, SysLogOperateType.ADD, null, null, request.getTitle());
		}
	}

//    @AutoLog(value = "删除联系人",operateType = CommonConstant.OPERATE_TYPE_DELETE)
	// @ApiOperation(value = "删除联系人", notes = "删除联系人")
	@RequestMapping(value = "delete", method = RequestMethod.POST)
	@RequiresPermissions(value = {"linkmanDelete"})
	@ParamSecurity( enabled = true)
	public void delete(@Valid DeleteLinkmanMsg request) throws Exception {
		DeleteLinkmanReplyMsg msg = this.service.delete(request);
		if(!StringUtils.isBlank(request.getTitle())) {
			sysBaseAPI.addLog(request.getContent() != null?request.getContent()+"；ID":"删除联系人:" + request.getLinkmanId()+"，联系人名称："+msg.getLinkManName(), null, SysLogOperateType.DELETE, null, null, request.getTitle());
		}
	}

//    @AutoLog(value = "更新联系人",operateType = CommonConstant.OPERATE_TYPE_UPDATE)
	@ApiOperation(value = "更新联系人", notes = "更新联系人")
	@RequestMapping(value = "update1", method = RequestMethod.POST)
	@RequiresPermissions(value = {"linkmanUpdate"})
	@ParamSecurity( enabled = true)
	public void update(@Valid UpdateLinkmanMsg request) throws Exception {
		this.service.update(request);
		if(!StringUtils.isBlank(request.getTitle())) {
			StringBuffer buff = new StringBuffer();
			buff.append(request.getContent() != null?request.getContent()+"；ID":"更新联系人；ID：").append(request.getLinkmanId()).append(request.toString());
			// .append("，联系人名称：").append(request.getUserName());
			sysBaseAPI.addLog(buff.toString(), null, SysLogOperateType.UPDATE, null, null, request.getTitle());
		}
	}

//    @AutoLog(value = "联系人详情",operateType = CommonConstant.OPERATE_TYPE_DETAIL)
	@ApiOperation(value = "联系人详情", notes = "联系人详情")
	@RequestMapping(value = "detail", method = RequestMethod.POST)
	@RequiresPermissions(value = {"linkmanDetail"})
	@ParamSecurity( enabled = true)
	public LinkmanVo detail(@Valid DetailLinkmanMsg request) throws Exception {
		// sysBaseAPI.addLog("联系人详情；ID:" + request.getLinkmanId()+"", null, SysLogOperateType.DETAIL, null, null, "告警管理");
		LinkmanVo vo =  this.service.detail(request);
		return vo;
	}

// @AutoLog(value = "联系人列表",operateType = CommonConstant.OPERATE_TYPE_LIST, module = "告警管理")
	@ApiOperation(value = "联系人列表", notes = "联系人列表")
	@RequestMapping(value = "list1", method = RequestMethod.POST)
	@RequiresPermissions(value = {"linkmanList"})
	@ParamSecurity( enabled = true)
	public List<LinkmanVo> list(@Valid PageLinkmanMsg request) throws Exception {
		List<LinkmanVo> vos = this.service.list();
		if(!StringUtils.isBlank(request.getTitle())) {
			sysBaseAPI.addLog((request.getContent() != null?request.getContent():"查看告警联系人")+(vos.isEmpty() ? "":("，联系人ID："+vos.get(0).getId())) , null, SysLogOperateType.DETAIL, null, null, request.getTitle());
		}
		return vos;
	}
}
