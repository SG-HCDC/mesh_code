package com.gcloud.mesh.alert.controller;

import com.gcloud.framework.db.PageResult;
import com.gcloud.mesh.alert.service.PolicyService;
import com.gcloud.mesh.dcs.annotation.ParamSecurity;
import com.gcloud.mesh.header.msg.alert.*;
import com.gcloud.mesh.header.vo.alert.PolicyVo;
import com.gcloud.mesh.header.vo.alert.RuleVo;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

import org.apache.commons.lang.StringUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.jeecg.common.aspect.annotation.AutoLog;
import org.jeecg.common.constant.CommonConstant;
import org.jeecg.common.constant.enums.SysLogOperateType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.jeecg.common.system.api.ISysBaseAPI;

import javax.validation.Valid;
import java.util.List;

@Api(tags = "告警策略管理")
@RestController
@RequestMapping("/policy")
public class PolicyController {

	@Autowired
	private PolicyService service;

	@Autowired
	private ISysBaseAPI sysBaseAPI;
	
	//处理Mass Assignment: Insecure Binder Configuration
	@InitBinder
	public void populateCustomerRequest(WebDataBinder binder) {
	    binder.setDisallowedFields(new String[]{});
	}

    // @AutoLog(value = "查看策略列表",operateType = CommonConstant.OPERATE_TYPE_LIST, module = "告警管理")
	@ApiOperation(value = "策略分页", notes = "策略分页")
	@RequestMapping(value = "page", method = RequestMethod.POST)
	@RequiresPermissions(value = {"policyPage"})
	@ParamSecurity( enabled = true)
	public PageResult<PolicyVo> page(@Valid PagePolicyMsg request) throws Exception {
		PageResult<PolicyVo> res = this.service.page(request);
		if(!StringUtils.isBlank(request.getTitle())) {
			if(request.getTitle().contains("告警通知方式设置")) {
				sysBaseAPI.addLog(request.getContent() != null?request.getContent():"查询告警通知方式设置列表", null, SysLogOperateType.QUERY, null, null, request.getTitle());
			}else if(request.getTitle().contains("告警策略设置")){
				sysBaseAPI.addLog(request.getContent() != null?request.getContent():"查询告警策略设置列表", null, SysLogOperateType.QUERY, null, null, request.getTitle());
			}else {
				sysBaseAPI.addLog(request.getContent() != null?request.getContent():"查询告警策略列表", null, SysLogOperateType.QUERY, null, null, request.getTitle());
			}
			
		}
		return res;
	}

//    @AutoLog(value = "新增策略",operateType = CommonConstant.OPERATE_TYPE_ADD)
	@ApiOperation(value = "新增策略", notes = "新增策略")
	@RequestMapping(value = "create", method = RequestMethod.POST)
	@RequiresPermissions(value = {"policyCreate"})
	@ParamSecurity( enabled = true)
	public void create(@Valid CreatePolicyMsg request) throws Exception {
		String id = this.service.create(request);
		if(!StringUtils.isBlank(request.getTitle())) {
			StringBuffer buff = new StringBuffer();
			buff.append((request.getContent() != null?request.getContent():"新增策略"));
			// .append(id).append("，策略名称："+request.getPolicyName());
			buff.append(request.toString());
			sysBaseAPI.addLog(buff.toString(), null, SysLogOperateType.ADD, null, null, request.getTitle());
		}
		
	}

//    @AutoLog(value = "删除策略",operateType = CommonConstant.OPERATE_TYPE_DELETE)
	@ApiOperation(value = "删除策略", notes = "删除策略")
	@RequestMapping(value = "delete", method = RequestMethod.POST)
	@RequiresPermissions(value = {"policyDelete"})
	@ParamSecurity( enabled = true)
	public void delete(@Valid DeletePolicyMsg request) throws Exception {
		this.service.delete(request);
		if(!StringUtils.isBlank(request.getTitle())) {
			sysBaseAPI.addLog(request.getContent() != null?request.getContent():"删除策略；删除策略ID：" + request.getPolicyId()+"", null, SysLogOperateType.DELETE, null, null, request.getTitle());
		}
		
	}

//    @AutoLog(value = "更新策略",operateType = CommonConstant.OPERATE_TYPE_UPDATE)
	@ApiOperation(value = "更新策略", notes = "更新策略")
	@RequestMapping(value = "update1", method = RequestMethod.POST)
	@RequiresPermissions(value = {"policyUpdate"})
	@ParamSecurity( enabled = true)
	public void update(@Valid UpdatePolicyMsg request) throws Exception {
		this.service.update(request);
		if(!StringUtils.isBlank(request.getTitle())) {
			StringBuffer buff = new StringBuffer();
			buff.append(request.getContent() != null?request.getContent()+"；ID:":"更新策略；ID:").append(request.getPolicyId());
			if(StringUtils.isNotBlank(request.getPolicyName())) {
				buff.append("，策略名称："+request.getPolicyName());
			}
			buff.append(request.toString());
			sysBaseAPI.addLog(buff.toString(), null, SysLogOperateType.UPDATE, null, null, request.getTitle());
		}
		
	}

//    @AutoLog(value = "策略详情",operateType = CommonConstant.OPERATE_TYPE_DETAIL)
	@ApiOperation(value = "策略详情", notes = "策略详情")
	@RequestMapping(value = "detail", method = RequestMethod.POST)
	@RequiresPermissions(value = {"policyDetail"})
	@ParamSecurity( enabled = true)
	public PolicyVo detail(@Valid DetailPolicyMsg request) throws Exception {
		PolicyVo vo =  this.service.detail(request);
		if(!StringUtils.isBlank(request.getTitle())) {
			sysBaseAPI.addLog(request.getContent() != null?request.getContent():"查看策略详情，策略详情ID：" + request.getPolicyId()+"策略名称："+vo.getPolicyName(), null, SysLogOperateType.DETAIL, null, null, request.getTitle());
			// sysBaseAPI.addLog("删除联系人；ID:" + request.getLinkmanId(), null, SysLogOperateType.DELETE, null, null, request.getTitle());
		}
		return vo;
	}

    // @AutoLog(value = "策略列表",operateType = CommonConstant.OPERATE_TYPE_LIST, module = "告警管理")
	@ApiOperation(value = "策略列表", notes = "策略列表")
	@RequestMapping(value = "list", method = RequestMethod.POST)
	@RequiresPermissions(value = {"policyList"})
	@ParamSecurity( enabled = true)
	public List<PolicyVo> list() throws Exception {
		return this.service.list();
	}

//    @AutoLog(value = "新增策略联系人",operateType = CommonConstant.OPERATE_TYPE_ADD)
	@ApiOperation(value = "新增策略联系人", notes = "新增策略联系人")
	@RequestMapping(value = "addPolicyLinkman", method = RequestMethod.POST)
	@RequiresPermissions(value = {"policyAddPolicyLinkman"})
	@ParamSecurity( enabled = true)
	public void addPolicyLinkman(@Valid AddPolicyLinkmanMsg request) throws Exception {
		this.service.addPolicyLinkman(request);
		if(!StringUtils.isBlank(request.getTitle())) {
			sysBaseAPI.addLog((request.getContent() != null?request.getContent():"新增策略联系人")+"，策略ID：" + request.getPolicyId()+"，"+"联系人ID："+request.getLinkmanIds() == null ? request.getLinkmanIds().get(0):"无", null, SysLogOperateType.ADD, null, null, request.getTitle());
		}
	}

//    @AutoLog(value = "删除策略联系人",operateType = CommonConstant.OPERATE_TYPE_DELETE)
	@ApiOperation(value = "删除策略联系人", notes = "删除策略联系人")
	@RequestMapping(value = "deletePolicyLinkman", method = RequestMethod.POST)
	@RequiresPermissions(value = {"policyDeletePolicyLinkman"})
	@ParamSecurity( enabled = true)
	public void deletePolicyLinkman(@Valid DeletePolicyLinkmanMsg request) throws Exception {
		this.service.deletePolicyLinkman(request);
		if(!StringUtils.isBlank(request.getTitle())) {
			sysBaseAPI.addLog(request.getContent() != null?request.getContent()+"；ID:":"删除策略联系人；删除策略联系人ID:" + request.getPolicyId(), null, SysLogOperateType.DELETE, null, null, request.getTitle());
		}
	}
}
