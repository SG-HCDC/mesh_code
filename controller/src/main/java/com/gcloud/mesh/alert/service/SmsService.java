package com.gcloud.mesh.alert.service;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import org.jeecg.common.exception.ParamException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.gcloud.mesh.alert.dao.SmsDao;
import com.gcloud.mesh.alert.entity.SmsEntity;
import com.gcloud.mesh.header.exception.BaseException;
import com.gcloud.mesh.header.msg.alert.AbleSmsMsg;
import com.gcloud.mesh.header.msg.alert.AccessSmsMsg;
import com.gcloud.mesh.header.msg.alert.DetailSmsMsg;
import com.gcloud.mesh.header.msg.alert.UpdateSmsMsg;
import com.gcloud.mesh.header.vo.alert.SmsVo;

@Service
public class SmsService {

	@Autowired
	SmsDao smsDao;

	public SmsVo getSms() {
		SmsEntity entity = new SmsEntity();
		List<SmsEntity> smsconfig = smsDao.findByProperty("enable", true);

		if (smsconfig.size() <= 0) {
			throw new BaseException("no_sms_platform", "No Sms Platform");
		} else if (smsconfig.size() > 1) {
			throw new BaseException("default_not_unique", "Default value is not unique");
		} else {
			entity = smsconfig.get(0);
		}

		SmsVo vo = new SmsVo();
		vo.setConfig(entity.getConfig());
		vo.setCreateTime(entity.getCreateTime());
		vo.setEnable(entity.isEnable());
		vo.setSmsPlatform(entity.getSmsPlatform());
		vo.setSmsPlatformName(entity.getSmsPlatformName());

		return vo;
	}

	public void accessSms(AccessSmsMsg request) {
		SmsEntity entity = new SmsEntity();

		List<SmsEntity> list = smsDao.findByProperty("smsPlatform", request.getSmsPlatform());
		if (list == null || list.size() == 0) {
			entity.setId(UUID.randomUUID().toString());
			entity.setCreateTime(new Date());
		} else if (list.size() > 1) {
			throw new ParamException("sms_platform_more_than_one", "相同的短信平台超过一个");
		} else {
			entity = list.get(0);
		}

		String config = request.getConfig();
		try {
			config = URLDecoder.decode(config, "utf-8");
		} catch (UnsupportedEncodingException e) {
			throw new ParamException("sms_platform_config_decode_error", "短信平台配置编码异常");
		}

		entity.setSmsPlatform(request.getSmsPlatform());
		entity.setSmsPlatformName(request.getSmsPlatformName());
		entity.setConfig(config);
		entity.setEnable(request.isEnable());

		smsDao.saveOrUpdate(entity);
	}

	public void ableSms(AbleSmsMsg request) {
		SmsEntity result = smsDao.findUniqueByProperty("id", request.getSmsId());
		if (result == null) {
			throw new ParamException("SmsPlatformNotExist", "短信平台不存在");
		}

		result.setEnable(request.isEnable());
		smsDao.update(result);
	}

	public void updateSms(UpdateSmsMsg request) {
		SmsEntity result = smsDao.findUniqueByProperty("id", request.getSmsId());
		if (result == null) {
			throw new ParamException("SmsNotExist", "短信平台不存在");
		}

		result.setSmsPlatform(request.getSmsPlatform());
		result.setSmsPlatformName(request.getSmsPlatformName());
		result.setConfig(request.getConfig());
		smsDao.update(result);
	}

	public SmsVo detail(DetailSmsMsg request) {
		List<SmsEntity> entitys = smsDao.findByProperty("smsPlatform", request.getPlatform());
		SmsVo vo = new SmsVo();
		if (entitys == null || entitys.size() == 0) {
		} else if (entitys.size() > 1) {
			throw new ParamException("sms_platform_more_than_one", "相同的短信平台超过一个");
		} else {
			SmsEntity entity = entitys.get(0);
			vo.setSmsPlatform(entity.getSmsPlatform());
			vo.setSmsPlatformName(entity.getSmsPlatformName());
			vo.setConfig(entity.getConfig());
			vo.setEnable(entity.isEnable());
			vo.setCreateTime(entity.getCreateTime());
		}
		return vo;
	}
}
