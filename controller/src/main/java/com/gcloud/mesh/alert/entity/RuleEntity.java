package com.gcloud.mesh.alert.entity;

import java.util.Date;

import com.gcloud.framework.db.jdbc.annotation.ID;
import com.gcloud.framework.db.jdbc.annotation.Table;

import lombok.Data;

@Data
@Table(name = "alert_rules")
public class RuleEntity {
	@ID
	private String id;
	private String policyId;
	private String meter;
	private String resourceType;
	private String resourceId;
	private Date createTime;
	private Double threshold;
	private boolean enabled = true;
	private Integer duration = 1;
	private String statistics;
	private Integer comparisonOperator;
	private Integer level;
	private String meterChnName;
	private String unit;
	
}
