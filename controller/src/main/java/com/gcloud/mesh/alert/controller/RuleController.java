package com.gcloud.mesh.alert.controller;

import com.gcloud.mesh.alert.service.RuleService;
import com.gcloud.mesh.dcs.annotation.ParamSecurity;
import com.gcloud.mesh.header.msg.alert.CreateRuleMsg;
import com.gcloud.mesh.header.msg.alert.DeleteRuleMsg;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.jeecg.common.aspect.annotation.AutoLog;
import org.jeecg.common.constant.CommonConstant;
import org.jeecg.common.constant.enums.SysLogOperateType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.jeecg.common.system.api.ISysBaseAPI;

import javax.validation.Valid;

@Api(tags = "告警规则管理")
@RestController
@RequestMapping("/rule")
public class RuleController {

	@Autowired
	RuleService service;

	@Autowired
	private ISysBaseAPI sysBaseAPI;
	
	//处理Mass Assignment: Insecure Binder Configuration
	@InitBinder
	public void populateCustomerRequest(WebDataBinder binder) {
	    binder.setDisallowedFields(new String[]{});
	}

//    @AutoLog(value = "新增规则",operateType = CommonConstant.OPERATE_TYPE_ADD)
	@ApiOperation(value = "新增规则", notes = "新增规则")
	@RequestMapping(value = "create", method = RequestMethod.POST)
	@RequiresPermissions(value = {"ruleCreate"})
	@ParamSecurity( enabled = true)
	public void create(@Valid CreateRuleMsg request) throws Exception {
		this.service.create(request);
		//sysBaseAPI.addLog("新增规则；策略ID:" + request.getPolicyId(), null, SysLogOperateType.ADD, null, null, "告警管理");
	}

//    @AutoLog(value = "删除规则",operateType = CommonConstant.OPERATE_TYPE_DELETE)
	@ApiOperation(value = "删除规则", notes = "删除规则")
	@RequestMapping(value = "delete", method = RequestMethod.POST)
	@RequiresPermissions(value = {"ruleDelete"})
	@ParamSecurity( enabled = true)
	public void delete(@Valid DeleteRuleMsg request) throws Exception {
		this.service.delete(request);
		//sysBaseAPI.addLog("删除规则；ID:" + request.getRuleId(), null, SysLogOperateType.DELETE, null, null, "告警管理");
	}
}
