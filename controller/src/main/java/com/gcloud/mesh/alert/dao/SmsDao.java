package com.gcloud.mesh.alert.dao;

import org.springframework.stereotype.Repository;

import com.gcloud.framework.db.dao.impl.JdbcBaseDaoImpl;
import com.gcloud.mesh.alert.entity.SmsEntity;

@Repository
public class SmsDao extends JdbcBaseDaoImpl<SmsEntity, String> {

}
