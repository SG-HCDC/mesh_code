package com.gcloud.mesh.alert.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.gcloud.mesh.alert.dao.LinkmanDao;
import com.gcloud.mesh.alert.dao.PolicyLinkmanDao;
import com.gcloud.mesh.alert.entity.LinkmanEntity;
import com.gcloud.mesh.alert.entity.PolicyLinkmanEntity;
import com.gcloud.mesh.header.vo.alert.LinkmanVo;

@Service
public class PolicyLinkmanService {

	@Autowired
	PolicyLinkmanDao policyLinkmanDao;

	@Autowired
	LinkmanDao linkmanDao;

	public List<LinkmanVo> getByPolicyId(String policyId) {
		List<LinkmanVo> res = new ArrayList<LinkmanVo>();
		for (PolicyLinkmanEntity entity : policyLinkmanDao.findByProperty("policyId", policyId)) {
			LinkmanEntity linkman = linkmanDao.getById(entity.getLinkmanId());
			if (linkman != null) {
				res.add(convertLinkmanEntityToVo(linkman));
			}
		}
		return res;
	}

	private LinkmanVo convertLinkmanEntityToVo(LinkmanEntity entity) {
		LinkmanVo linkmanVo = new LinkmanVo();
		linkmanVo.setCreateTime(entity.getCreateTime());
		linkmanVo.setEmail(entity.getEmail());
		linkmanVo.setId(entity.getId());
		linkmanVo.setPhoneNumber(entity.getPhoneNumber());
		linkmanVo.setUserName(entity.getUserName());
		return linkmanVo;
	}
}
