package com.gcloud.mesh.alert.entity;

import java.util.Date;

import com.gcloud.framework.db.jdbc.annotation.ID;
import com.gcloud.framework.db.jdbc.annotation.Table;

import lombok.Data;

@Data
@Table(name = "alert_history")
public class HistoryEntity {

	@ID
	private String id;
	private String title;
	private String resourceId;
	private String resourceType;
	private String meter;
	private Integer level;
	private String resourceName;
	private Date createTime;
	private Date alertTime;
	private Integer notifyCount;
	private String regionId;
	private Integer status;
	private String content;
	private String resourceInstance;
	private String platformType;
}
