package com.gcloud.mesh.alert.entity;

import java.util.Date;

import com.gcloud.framework.db.jdbc.annotation.ID;
import com.gcloud.framework.db.jdbc.annotation.Table;

import lombok.Data;

@Data
@Table(name = "alert_policy_linkman")
public class PolicyLinkmanEntity {
	
	@ID
	private Integer id;
	private String policyId;
	private String linkmanId;
	private Date createTime;
	
}
