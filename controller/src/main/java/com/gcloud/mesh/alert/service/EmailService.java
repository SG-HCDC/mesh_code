package com.gcloud.mesh.alert.service;

import java.util.List;

import org.jeecg.modules.message.util.PushMsgUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.gcloud.mesh.alert.dao.EmailDao;
import com.gcloud.mesh.alert.entity.EmailEntity;
import com.gcloud.mesh.header.exception.BaseException;
import com.gcloud.mesh.header.vo.alert.EmailVo;

@Service
public class EmailService {

	@Autowired
	EmailDao emailDao;

	@Autowired
	PushMsgUtil pushMsgUtil;

	public EmailVo getEmail() {
		EmailEntity entity = new EmailEntity();
		List<EmailEntity> entitys = emailDao.findAll();

		if (entitys.size() <= 0) {
			throw new BaseException("no_email_config", "No Email Config");
		} else if (entitys.size() > 1) {
			throw new BaseException("default_not_unique", "Default value is not unique");
		} else {
			entity = entitys.get(0);
		}

		EmailVo vo = new EmailVo();
		vo.setEmailProtocol(entity.getEmailProtocol());
		vo.setEmailSenderAccount(entity.getEmailSenderAccount());
		vo.setEmailSenderName(entity.getEmailSenderName());
		vo.setEmailSenderPasswd(entity.getEmailSenderPasswd());
		vo.setEmailServer(entity.getEmailServer());
		vo.setId(entity.getId());

		return vo;
	}

	public boolean pushEmail(String title, String content, String sendTo) {
		boolean isSuccess = pushMsgUtil.sendMessage("2", title, content, sendTo);
		return isSuccess;
	}
}
