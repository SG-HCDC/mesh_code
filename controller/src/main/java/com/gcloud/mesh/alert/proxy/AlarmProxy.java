package com.gcloud.mesh.alert.proxy;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.gcloud.mesh.framework.core.SpringUtil;
import com.gcloud.mesh.header.exception.BaseException;

public class AlarmProxy {
	private static Logger LOG = LoggerFactory.getLogger(AlarmProxy.class);

	public static IAlarmProxy getAlarmProxyByResourceType(String resourceType) {
		LOG.debug("入参resourceType:" + resourceType);
		IAlarmProxy proxy = null;
		String beanName = AlarmProxy.toUpperCaseFirstOne(resourceType) + "AlarmProxyImpl";
		proxy = (IAlarmProxy) SpringUtil.getBean(beanName);
		if (proxy == null) {
			LOG.error(beanName + "不存在");
			throw new BaseException("ResourceTypeNotFound", beanName + "不存在");
		}
		return proxy;
	}

	// 首字母转大写
	public static String toUpperCaseFirstOne(String s) {
		if (!Character.isUpperCase(s.charAt(0)))
			return (new StringBuilder()).append(Character.toUpperCase(s.charAt(0))).append(s.substring(1)).toString();
		else
			return s;
	}
}
