package com.gcloud.mesh.monitor.cloud;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.jeecg.common.util.UUIDGenerator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import com.gcloud.mesh.header.enums.MonitorMeter;
import com.gcloud.mesh.header.vo.dcs.CloudResourceMetricVO;
import com.gcloud.mesh.header.vo.dcs.DescribeMetricVO;
import com.gcloud.mesh.monitor.dao.StatisticsDao;
import com.gcloud.mesh.monitor.entity.StatisticsEntity;
import com.gcloud.mesh.supplier.enums.SystemType;

// @Component
public class CloudMetricTimer {
	@Autowired
	private CloudMonitorContext context;
	@Autowired
	private StatisticsDao statisticsDao;
	// @Scheduled(fixedDelay = 1000 * 60)
	public void work() {
		//查询虚拟机和应用的监控信息
		Map<String, List<DescribeMetricVO>>  maps = context.getAppinstanceMetrics();
		if(maps != null) {
			List<StatisticsEntity> entitys = new ArrayList<>();
			for(Entry<String, List<DescribeMetricVO>> kv : maps.entrySet()) {
				List<DescribeMetricVO>  vos = kv.getValue();
				Double appcpu = 0.0;
				Double appmen = 0.0;
				for(DescribeMetricVO vo : vos) {
					if(vo.getType().equals(SystemType.ALI.getNo()) || vo.getType().equals(SystemType.HUAWEI.getNo())) {
						//虚拟机的监控数据返回
						if(StringUtils.isNotBlank(vo.getCupVal()) && Double.valueOf(vo.getCupVal()) > 0) {
							appcpu = Math.max(Double.valueOf(vo.getCupVal()), appcpu);
							StatisticsEntity entitycpu = new StatisticsEntity();
							entitycpu.setId(UUIDGenerator.generate());
							entitycpu.setMeter(MonitorMeter.VM_CPU_UTIL.getMeter());
							entitycpu.setResourceId(vo.getInstanceId());
							entitycpu.setTimestamp(new Date());
							entitycpu.setValue(Double.valueOf(vo.getCupVal()));
							entitys.add(entitycpu);
						}
						if(StringUtils.isNotBlank(vo.getMemVal()) && Double.valueOf(vo.getMemVal()) > 0) {
							appmen = Math.max(Double.valueOf(vo.getMemVal()), appmen);
							StatisticsEntity entitymem = new StatisticsEntity();
							entitymem.setId(UUIDGenerator.generate());
							entitymem.setMeter(MonitorMeter.VM_MEMORY_UTIL.getMeter());
							entitymem.setResourceId(vo.getInstanceId());
							entitymem.setTimestamp(new Date());
							entitymem.setValue(Double.valueOf(vo.getMemVal()));
							entitys.add(entitymem);
						}
						
					}else {
						//一个应用多个namespace，取namespace使用率最大的那个
						if(StringUtils.isNotBlank(vo.getCupVal())) {
							if(Math.max(Double.valueOf(vo.getCupVal()), appcpu) > 0) {
								appcpu = Math.max(Double.valueOf(vo.getCupVal()), appcpu);
							}
							
						}
						if(StringUtils.isNotBlank(vo.getMemVal())) {
							if(Math.max(Double.valueOf(vo.getMemVal()), appmen) > 0) {
								appmen = Math.max(Double.valueOf(vo.getMemVal()), appmen);
							}
						}
					}
					
				}
				if(appcpu > 0) {
					StatisticsEntity appcpuEn = new StatisticsEntity();
					appcpuEn.setId(UUIDGenerator.generate());
					appcpuEn.setMeter(MonitorMeter.APP_CPU_UTIL.getMeter());
					appcpuEn.setResourceId(kv.getKey());
					appcpuEn.setTimestamp(new Date());
					appcpuEn.setValue(appcpu);
					entitys.add(appcpuEn);
				}

				
				if(appmen > 0) {
					StatisticsEntity appmemEn = new StatisticsEntity();
					appmemEn.setId(UUIDGenerator.generate());
					appmemEn.setMeter(MonitorMeter.APP_MEMORY_UTIL.getMeter());
					appmemEn.setResourceId(kv.getKey());
					appmemEn.setTimestamp(new Date());
					appmemEn.setValue(appmen);
					entitys.add(appmemEn);
				}
			}
			if(!entitys.isEmpty()) {
				statisticsDao.saveBatch(entitys);
			}
			
		}
		
		//查询集群的监控信息
		Map<String, CloudResourceMetricVO>  cloudResourceMap = context.getCloudResourceMetric();
		if(cloudResourceMap != null && !cloudResourceMap.isEmpty()) {
			List<StatisticsEntity> cloudEntitys = new ArrayList<>();
			for(Entry<String, CloudResourceMetricVO> kv : cloudResourceMap.entrySet()) {
				CloudResourceMetricVO vo = kv.getValue();
				if(StringUtils.isNotBlank(vo.getCupVal()) && Double.valueOf(vo.getCupVal()) > 0) {
					StatisticsEntity cloudResourceCpu = new StatisticsEntity();
					cloudResourceCpu.setId(UUIDGenerator.generate());
					cloudResourceCpu.setMeter(MonitorMeter.K8S_CPU_UTIL.getMeter());
					cloudResourceCpu.setResourceId(vo.getCloudResourceId());
					cloudResourceCpu.setTimestamp(new Date());
					cloudResourceCpu.setValue(Double.valueOf(vo.getCupVal()));
					cloudEntitys.add(cloudResourceCpu);
				}
				if(StringUtils.isNotBlank(vo.getMemVal()) && Double.valueOf(vo.getMemVal()) > 0) {
					StatisticsEntity cloudResourceMem = new StatisticsEntity();
					cloudResourceMem.setId(UUIDGenerator.generate());
					cloudResourceMem.setMeter(MonitorMeter.K8S_MEMORY_UTIL.getMeter());
					cloudResourceMem.setResourceId(vo.getCloudResourceId());
					cloudResourceMem.setTimestamp(new Date());
					cloudResourceMem.setValue(Double.valueOf(vo.getMemVal()));
					cloudEntitys.add(cloudResourceMem);
				}
			}
			if(!cloudEntitys.isEmpty()) {
				statisticsDao.saveBatch(cloudEntitys);
			}
		}

	}
}
