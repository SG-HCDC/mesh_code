package com.gcloud.mesh.monitor.cloud;

import java.util.List;

import com.gcloud.mesh.header.vo.dcs.CloudResourceMetricVO;
import com.gcloud.mesh.header.vo.dcs.DescribeMetricVO;

public interface ICloudBaseMonitor {
	List<DescribeMetricVO> queryDescribeMetricLastList(String cloudResourceId);
	
	CloudResourceMetricVO queryCloudResourceMetric(String cloudResourceId);
}
