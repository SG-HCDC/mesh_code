package com.gcloud.mesh.monitor.entity;

import java.util.Date;

import com.gcloud.framework.db.jdbc.annotation.ID;
import com.gcloud.framework.db.jdbc.annotation.Table;

import lombok.Data;

@Table(name = "dcs_statistics_data")
@Data
public class StatisticsEntity {

	@ID
	private String id;
	private String meter;
	private String resourceId;
	private Date timestamp;
	private Double value;
}
