
package com.gcloud.mesh.monitor.timer;

public interface ITimer {

    boolean init();

    void start();

    void stop();

    /**
     * 是否随系统初始时由框架自动启动
     * 
     */
    boolean autoStart();

    /**
     * 延时启动的时间，调用timer的触发方法后延迟多久才生效
     * 单位为毫秒，默认20秒
     */
    long getDelay();

    /**
     * 定时线程触发频率
     * 单位为毫秒，默认1分钟
     */
    long getRate();

}
