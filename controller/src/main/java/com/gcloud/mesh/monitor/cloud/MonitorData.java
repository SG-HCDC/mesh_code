package com.gcloud.mesh.monitor.cloud;

import lombok.Data;

@Data
public class MonitorData {
	private Double cpu;
	private Double memory;
}
