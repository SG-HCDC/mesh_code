package com.gcloud.mesh.monitor.controller;

import com.gcloud.mesh.dcs.annotation.ParamSecurity;
import com.gcloud.mesh.header.msg.BaseReplyMsg;
import com.gcloud.mesh.header.msg.monitor.LatestSampleMsg;
import com.gcloud.mesh.header.msg.monitor.StatisticsMsg;
import com.gcloud.mesh.header.vo.monitor.MeterVo;
import com.gcloud.mesh.header.vo.monitor.SampleVo;
import com.gcloud.mesh.header.vo.monitor.StatisticsVo;
import com.gcloud.mesh.monitor.service.StatisticsCheatService;
import com.gcloud.mesh.monitor.service.StatisticsService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.jeecg.common.aspect.annotation.AutoLog;
import org.jeecg.common.constant.CommonConstant;
import org.jeecg.common.constant.enums.SysLogOperateType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.jeecg.common.system.api.ISysBaseAPI;

import java.util.List;

@Api(tags = "监控")
@RestController
@RequestMapping("/statistics")
public class StatisticsController {

	// 假数据使用cheatService

	@Autowired
	private StatisticsService service;

	@Autowired
	private StatisticsCheatService cheatService;

	@Autowired
	private ISysBaseAPI sysBaseAPI;
	
	//处理Mass Assignment: Insecure Binder Configuration
	@InitBinder
	public void populateCustomerRequest(WebDataBinder binder) {
	    binder.setDisallowedFields(new String[]{});
	}

	@ApiOperation(value = "申请实时数据", notes = "申请实时数据")
	@RequestMapping(value = "requestStatistics", method = RequestMethod.POST)
	@ParamSecurity(enabled = true)
	public boolean requestStatistics(String id) throws Exception {
		boolean res = this.cheatService.requestStatistics();
		return res;
	}
	
//    @AutoLog(value = "实时数据",operateType = CommonConstant.OPERATE_TYPE_DETAIL)
	@ApiOperation(value = "查看实时数据", notes = "实时数据")
	@RequestMapping(value = "statistics", method = RequestMethod.POST)
	//@RequiresPermissions(value = {"statisticsStatistics"})
	@ParamSecurity(enabled = true)
	public StatisticsVo statistics(StatisticsMsg request) throws Exception {
		StatisticsVo res = this.service.statistics(request);
	//	sysBaseAPI.addLog("实时数据；数据中心ID:" + request.getDcId(), null, CommonConstant.OPERATE_TYPE_DETAIL);
		return res;
	}

//    @AutoLog(value = "最新单个数据",operateType = CommonConstant.OPERATE_TYPE_DETAIL)
	@ApiOperation(value = "查看最新单个数据", notes = "最新单个数据")
	@RequestMapping(value = "latestSample", method = RequestMethod.POST)
	@RequiresPermissions(value = {"statisticsLatestSample"})
	@ParamSecurity(enabled = true)
	public BaseReplyMsg latestSample(LatestSampleMsg request) throws Exception {
		String res = this.service.latestSampleFromDb(request.getMeter(), request.getResourceId());
		BaseReplyMsg reply = new BaseReplyMsg();
		reply.setData(res);
		if(StringUtils.isNotBlank(request.getTitle())) {
			if(request.getTitle().contains("精确管控与供电制冷联动-基础资源细粒度感知-指标监控")) {
				if(request.getMeter().equals("switcher.out_octets") || "server.power".equals(request.getMeter())) {
					sysBaseAPI.addLog("查看IT设备监控详情:" + request.getResourceId(), null, SysLogOperateType.DETAIL, null ,null,request.getTitle()+"-L2层：IT设备");
				}else if(request.getMeter().equals("k8s.cpu_util") || "vm.cpu_util".equals(request.getMeter())) {
					sysBaseAPI.addLog("查看云平台资源监控详情:" + request.getResourceId(), null, SysLogOperateType.DETAIL, null ,null,request.getTitle()+"-L3层：云平台资源");
				}else if(request.getMeter().equals("container.memory_usage")) {
					sysBaseAPI.addLog("查看应用资源监控详情,ID:" + request.getResourceId(), null, SysLogOperateType.DETAIL, null ,null,request.getTitle()+"-L4层：应用资源");
				}
			}else if(request.getTitle().contains("多数据中心调度-数据跨层感知与智能分析-L1层：基础设施")) {
				if(request.getMeter().equals("environment.air_out_temperature") || "environment.ups".equals(request.getMeter()) || "environment.distribution".equals(request.getMeter())) {
					sysBaseAPI.addLog(request.getContent() != null?request.getContent() :"监控详情", null, SysLogOperateType.DETAIL, null ,null,request.getTitle());
				}
			}
		}
	    
		return reply;
	}

//    @AutoLog(value = "最新一类数据",operateType = CommonConstant.OPERATE_TYPE_DETAIL)
	@ApiOperation(value = "查看最新一类数据", notes = "最新一类数据")
	@RequestMapping(value = "latestSamples", method = RequestMethod.POST)
	@RequiresPermissions(value = {"statisticsLatestSamples"})
	@ParamSecurity(enabled = true)
	public List<SampleVo> latestSamples(LatestSampleMsg request) throws Exception {
		List<SampleVo> res = this.service.latestSamplesFromDb(request.getMeter(), request.getResourceId());
	//	sysBaseAPI.addLog("最新监控数据；资源ID:" + request.getResourceId(), null, CommonConstant.OPERATE_TYPE_DETAIL);
		if(StringUtils.isNotBlank(request.getTitle())) {
			if(request.getTitle().contains("精确管控与供电制冷联动-基础资源细粒度感知-指标监控")) {
				if(request.getMeter().equals("environment.air")) {
					sysBaseAPI.addLog("监控详情,ID:" + request.getResourceId(), null, SysLogOperateType.DETAIL, null ,null,request.getTitle()+"-L1层：基础设施");
				}
			}
		}
		return res;
	}

    @AutoLog(value = "查看监控项列表",operateType = CommonConstant.OPERATE_TYPE_LIST, module = "精确管控与供电制冷联动-基础资源细粒度感知-诊断告警-告警策略设置")
	@ApiOperation(value = "监控项列表", notes = "监控项列表")
	@RequestMapping(value = "listMeter", method = RequestMethod.POST)
	@RequiresPermissions(value = {"statisticsListMeter"})
    @ParamSecurity(enabled = true)
	public List<MeterVo> listMeter() throws Exception {
		List<MeterVo> res = this.service.listMeter();
		return res;
	}

}
