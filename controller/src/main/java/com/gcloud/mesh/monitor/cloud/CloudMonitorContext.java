package com.gcloud.mesh.monitor.cloud;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.gcloud.mesh.asset.dao.CloudResourceConfigDao;
import com.gcloud.mesh.asset.entity.CloudResourceConfigEntity;
import com.gcloud.mesh.dcs.dao.AppInstanceDao;
import com.gcloud.mesh.dcs.entity.AppInstanceEntity;
import com.gcloud.mesh.framework.core.SpringUtil;
import com.gcloud.mesh.header.vo.dcs.AppMetricVO;
import com.gcloud.mesh.header.vo.dcs.CloudResourceMetricVO;
import com.gcloud.mesh.header.vo.dcs.DescribeMetricVO;
import com.gcloud.mesh.supplier.enums.SystemType;
@Component
public class CloudMonitorContext {
	@Autowired
	private CloudResourceConfigDao configDao;
	@Autowired
	private AppInstanceDao instanceDao;
	
    private List<DescribeMetricVO> queryDescribeMetricLastList(String resourceTypeName,String cloudResourceId){
    	Object object = null;
		try {
			object = SpringUtil.getBean(resourceTypeName+"Monitor");
		} catch (Exception e) {
			return null;
		}
		ICloudBaseMonitor instance = (ICloudBaseMonitor) object;
		return instance.queryDescribeMetricLastList(cloudResourceId);

    }
    
    
    public List<AppMetricVO> queryAppMetricLastList(){
    	List<AppMetricVO> vos = new ArrayList<>();
    	//查出所有的cloudResource

    	return vos;
    	
    }
    public Map<String, List<DescribeMetricVO>> getAppinstanceMetrics(){
    	List<CloudResourceConfigEntity> configs = configDao.findAll();
    	Map<String, DescribeMetricVO> voMap = new HashMap<>();
    	for(CloudResourceConfigEntity entity : configs) {
    		SystemType type = SystemType.getSystemTypeByNo(entity.getType());
    		 List<DescribeMetricVO> vores = this.queryDescribeMetricLastList(type.getName(),entity.getCloudResourceId());
    		 if(vores != null) {
        		 for(DescribeMetricVO vo : vores) {
        			 vo.setType(type.getNo());
        			 voMap.put(vo.getInstanceId(), vo);
        		 }
    		 }
    	}
    	Map<String, List<DescribeMetricVO>> appVoMap = new HashMap<>(); 
    	List<AppInstanceEntity> dbinstances = instanceDao.findAll();
    	for(AppInstanceEntity entity : dbinstances) {
    		DescribeMetricVO vo = voMap.get(entity.getInstanceId());
    		if(vo != null) {
    			List<DescribeMetricVO> appvos = appVoMap.get(entity.getAppId());
    			if(appvos == null) {
    				appvos = new ArrayList<DescribeMetricVO>();
    				appvos.add(vo);
    				appVoMap.put(entity.getAppId(), appvos);
    			}else {
    				appvos.add(vo);
    			}
    		}
    	}
    	return appVoMap;
    }
    
    private CloudResourceMetricVO queryCloudResourceMetric(String resourceTypeName,String cloudResourceId) {
     	Object object = null;
    		try {
    			object = SpringUtil.getBean(resourceTypeName+"Monitor");
    		} catch (Exception e) {
    			return null;
    		}
    		ICloudBaseMonitor instance = (ICloudBaseMonitor) object;
    		return instance.queryCloudResourceMetric(cloudResourceId);
    }
    
    
    public Map<String, CloudResourceMetricVO> getCloudResourceMetric(){
    	List<CloudResourceConfigEntity> configs = configDao.findAll();
    	Map<String, CloudResourceMetricVO> voMap = new HashMap<>();
    	for(CloudResourceConfigEntity entity : configs) {
    		SystemType type = SystemType.getSystemTypeByNo(entity.getType());
    		 CloudResourceMetricVO vores = this.queryCloudResourceMetric(type.getName(),entity.getCloudResourceId());
    		 if(vores != null) {
    			 voMap.put(vores.getCloudResourceId(), vores);
    		 }
    	}
    	return voMap;
    }
    
}
