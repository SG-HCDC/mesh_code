package com.gcloud.mesh.quartz;

import java.io.File;
import java.util.Date;

import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.stereotype.Component;

import com.gcloud.mesh.config.DataSecurityConfig;
import com.gcloud.mesh.dcs.dao.DataBackupDao;
import com.gcloud.mesh.dcs.dao.DataSecurityDao;
import com.gcloud.mesh.dcs.entity.DataBackupEntity;
import com.gcloud.mesh.dcs.entity.DataSecurityEntity;
import com.gcloud.mesh.dcs.utils.DataSecurityBackupUtil;
import com.gcloud.mesh.framework.core.SpringUtil;
import com.gcloud.mesh.header.enums.DataSecurityPolicy;
import com.gcloud.mesh.utils.TimestampUtil;

import cn.hutool.core.lang.UUID;
import lombok.extern.slf4j.Slf4j;

@Component
@Slf4j
public class DataSecurityBackupJob implements Job {
	
	public static final String ID = "security";
	
	public static final String NAME_TEMPLATE = "yyyy-MM-dd_HH-mm-ss";

	@Override
	public void execute(JobExecutionContext arg0) throws JobExecutionException {
		// TODO Auto-generated method stub
		DataSecurityConfig dataSecurityConfig = SpringUtil.getBean(DataSecurityConfig.class);
		DataSecurityDao dataSecurityDao = SpringUtil.getBean(DataSecurityDao.class);
		DataBackupDao dataBackupDao = SpringUtil.getBean(DataBackupDao.class);
		if(dataSecurityDao != null) {
			log.info("[DataSecurityBackupJob][execute] 定时任务运行正常");
			DataSecurityEntity data = dataSecurityDao.getById(ID);
	        if(data.getEnabled()) {
	    		execute(dataSecurityConfig, dataSecurityDao, dataBackupDao);
	        }
		}else {
			log.info("[DataSecurityBackupJob][execute] 获取DataBackupDao bean异常");
		}

	}

    public void execute(DataSecurityConfig dataSecurityConfig, DataSecurityDao dataSecurityDao, DataBackupDao dataBackupDao) {
    	
    	DataSecurityEntity data = dataSecurityDao.getById(ID);
    	String name = TimestampUtil.formatDate(NAME_TEMPLATE, new Date());
    	saveBackup(dataBackupDao, name, data.getPolicy());
    	if(DataSecurityPolicy.FULL.equals(DataSecurityPolicy.getByName(data.getPolicy()))) {
    		log.info("[DataSecurityBackupJob][execute] 全量备份任务开始【{}】", data.getCron());
    		DataSecurityBackupUtil.backupFull(dataSecurityConfig.getEnable(), dataSecurityConfig.getFulls(), dataSecurityConfig.getHost(), 
    				dataSecurityConfig.getPort(), dataSecurityConfig.getUser(), dataSecurityConfig.getPassword());
    		log.info("[DataSecurityBackupJob][execute] 全量备份任务结束【{}】", data.getCron());
    	}
    	if(DataSecurityPolicy.INCREMENT.equals(DataSecurityPolicy.getByName(data.getPolicy()))) {
    		log.info("[DataSecurityBackupJob][execute] 增量备份任务开始【{}】", data.getCron());
//        	String incrementalDir = new File(dataSecurityConfig.getIncrements(), TimestampUtil.dateToStr(data.getUpdateTime())).getPath();
        	String incrementalDir = data.getBaseDir();
        	String baseDir = DataSecurityBackupUtil.getBaseDir(incrementalDir);
        	if(baseDir != null) {
        		DataSecurityBackupUtil.backupIncrement(dataSecurityConfig.getEnable(), incrementalDir, baseDir, dataSecurityConfig.getHost(),
            			dataSecurityConfig.getPort(), dataSecurityConfig.getUser(), dataSecurityConfig.getPassword());
        	}    	
    		log.info("[DataSecurityBackupJob][execute] 增量备份任务结束【{}】", data.getCron());
    	}
    }
    
    private void saveBackup(DataBackupDao dataBackupDao, String name, String policy) {
    	DataBackupEntity backup = new DataBackupEntity();
    	backup.setId(UUID.randomUUID().toString());
    	backup.setName(name);
    	backup.setPolicy(policy);
    	backup.setCreateTime(new Date());
    	try {
    		dataBackupDao.save(backup);
    	}catch (Exception e) {
    		log.error("[DataSecurityBackupJob][saveBackup] 数据保存失败: {}", e.getMessage());
    	}
    }
    
}
