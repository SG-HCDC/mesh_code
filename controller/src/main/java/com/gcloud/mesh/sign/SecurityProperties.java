package com.gcloud.mesh.sign;

import java.util.List;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import lombok.Data;

@Component
@ConfigurationProperties(prefix = "spring.security")
@Data
public class SecurityProperties {
	
	List<String> ignoreSignUri;

	Integer signTimeout;
}
