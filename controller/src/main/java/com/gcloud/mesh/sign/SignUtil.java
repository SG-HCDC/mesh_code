package com.gcloud.mesh.sign;

import org.apache.commons.lang3.StringUtils;
import org.jeecg.common.util.sm.SmManager;

import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.serializer.SerializerFeature;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class SignUtil {
	public static boolean verifySign(JSONObject params, String sign, Long timestamp) throws Exception {
		String paramsJsonStr = JSONObject.toJSONString(params,SerializerFeature.SortField) + timestamp + "_fc95e4f0ff6648d3aa1fac90db6b7bf4";
		return verifySign(paramsJsonStr, sign);
	}

	/**
	 * 验证签名
	 *
	 * @param params
	 * @param sign
	 * @return
	 * @throws Exception
	 */
	public static boolean verifySign(String params, String sign) throws Exception {
		if (StringUtils.isEmpty(params)) {
			return false;
		}
		String paramsSign = getParamsSign(params);
		log.info("入参：[{}],签名：[{}],传入签名：[{}]", params, paramsSign, sign);
		return sign.equals(paramsSign);
	}

	/**
	 * @return 得到签名
	 * @throws Exception
	 */
	public static String getParamsSign(String params) throws Exception {
		String sign = SmManager.encryptSm3(params);
		return sign;
	}
}
