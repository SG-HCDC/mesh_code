
package com.gcloud.mesh.sm.controller;

import com.gcloud.mesh.header.msg.BaseReplyMsg;
import com.gcloud.mesh.sm.service.SmService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;

import org.jeecg.common.system.api.ISysBaseAPI;
import org.jeecg.common.system.util.JwtUtil;
import org.jeecg.common.system.vo.LoginUser;
import org.jeecg.common.util.sm.SmManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@RestController
@RequestMapping("/sm")
@Api(tags = "密钥管理")
@Slf4j
public class SmController {
	
	private static final String SM2KEY = "sm2";
	
	private static final String SM3KEY = "sm3";
	    
	private static final String SM4KEY = "sm4";
	    
	private static final String HEADER_TOKEN = "X-Access-Token";
	
	//处理Mass Assignment: Insecure Binder Configuration
	@InitBinder
	public void populateCustomerRequest(WebDataBinder binder) {
	    binder.setDisallowedFields(new String[]{});
	}

    @Autowired
    private SmService service;
    
	@Autowired
	private ISysBaseAPI sysBaseAPI;

//    @ApiOperation(value = "秘钥管理 - 获取sm2公钥", notes = "秘钥管理 - 获取sm2公钥")
//    @RequestMapping(value = "/getsm2PublicKey")
//    public String getSm2(HttpSession session) throws Exception {
//    	Object sm2Public = session.getAttribute(SM2KEY);
//    	String sm2 = null;
//    	if(sm2Public != null) {
//    		sm2 = String.valueOf(sm2Public);
//    	}else {
//    		sm2 = service.publicKey();
//    	}
//    	session.setAttribute(SM2KEY, sm2);
//    	return sm2;
//    }
    
//    		
//    @ApiOperation(value = "获取sm2公钥")
//    @RequestMapping(value = "/getsm2")
//    public String getSm2(HttpSession session) throws Exception {
//    	Object sm2Public = session.getAttribute(SM2KEY);
//    	String sm2 = null;
//    	if(sm2Public != null) {
//    		sm2 = String.valueOf(sm2Public);
//    	}else {
//    		sm2 = service.publicKey();
//

    @ApiOperation(value = "密钥管理 - 接收服务端端sm2公钥", notes = "密钥管理 - 接收服务端端sm2公钥")
    @RequestMapping(value = "/getSm2PublicKey", method = RequestMethod.POST)
//    @RequiresPermissions(value = {"smGetSm2PublicKey"})
    public BaseReplyMsg getSm2PublicKey(HttpServletRequest request, HttpServletResponse response) throws Exception {
    	BaseReplyMsg reply = new BaseReplyMsg();
    	LoginUser user = getUser(request, response);  
    	String publicKey = service.getSm2PublicKeyByUser(user.getId());
		response.setHeader(SmManager.SM2_HEADER, publicKey);
		log.info("[SM][SmController][getSm2PublicKey] 往客户端发送用户{}【{}】SM2公钥：{}", user.getUsername(), user.getId(), publicKey);
//		request.setAttribute(SmManager.SM2_HEADER, publicKey);
		return reply;
    }

    @ApiOperation(value = "密钥管理 - 接收client端sm4秘钥", notes = "密钥管理 - 接收client端sm4秘钥")
    @RequestMapping(value = "/saveSm4", method = RequestMethod.POST)
//    @RequiresPermissions(value = {"smSaveSm4"})
    public BaseReplyMsg saveSm4(HttpServletRequest request, HttpServletResponse response) throws Exception {
    	BaseReplyMsg reply = new BaseReplyMsg();
    	String cipher = request.getHeader(SM4KEY);
    	LoginUser user = getUser(request, response);  	
    	if(cipher != null && user != null) {
    		service.saveSm4KeyPairByUser(cipher, user.getId());	
    		log.info("[SM][SmController][saveSm4] 接收并存储用户{}【{}】SM4秘钥：{}", user.getUsername(), user.getId(), cipher);
    	}
    	return reply;
     }
	
    private LoginUser getUser(HttpServletRequest request, HttpServletResponse response) {
    	String token = request.getHeader(HEADER_TOKEN);  		
    	LoginUser sysUser = null;
    	try {
    		String username = JwtUtil.getUsername(token);
    		sysUser = sysBaseAPI.getUserByName(username);
    	} catch(Exception e) {
    		throw e;
    	}	
		return sysUser;
    }


}
