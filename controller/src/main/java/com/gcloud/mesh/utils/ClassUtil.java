package com.gcloud.mesh.utils;

import java.lang.reflect.Field;
import java.lang.reflect.Method;

public class ClassUtil {

	public static String toStr(Object obj) {
		Class clazz = obj.getClass();
		Field[] f = clazz.getDeclaredFields();
		StringBuilder sql = new StringBuilder();
		String result = null;
		sql.append("{");
		try {
			for(int i=0; i<f.length; i++) {
				String attributeName = f[i].getName();
				String methodName = attributeName.substring(0, 1).toUpperCase() + attributeName.substring(1);
				Method getMethod = clazz.getMethod("get" + methodName);
				Object value = getMethod.invoke(obj);
				sql.append(attributeName + "=" + value.toString() + ", ");
			}
			result = sql.substring(0, sql.length()-2) + "}"; 
		} catch (Exception e) {
			result = "{}";
		}
		return result;
	}
	
}
