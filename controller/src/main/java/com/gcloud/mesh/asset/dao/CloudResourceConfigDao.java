package com.gcloud.mesh.asset.dao;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import com.gcloud.framework.db.dao.impl.JdbcBaseDaoImpl;
import com.gcloud.mesh.asset.entity.CloudResourceConfigEntity;
import com.gcloud.mesh.dcs.entity.SchedulerConfigEntity;
import com.gcloud.mesh.dcs.enums.SchedulerModelDict;
import com.gcloud.mesh.header.msg.dcs.AviableDatacenterMsg;
import com.gcloud.mesh.header.vo.dcs.DatacenterResourceVo;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Repository
public class CloudResourceConfigDao extends JdbcBaseDaoImpl<CloudResourceConfigEntity, String>{
	public List<CloudResourceConfigEntity>  getSameTypeDatacenterId(Integer type, String srcResourceId, String structure){
		StringBuffer sql = new StringBuffer();
		List<Object> param = new ArrayList<>();
		sql.append(" select acc.* from asset_cloud_resource_config acc left join asset_datacenters ad on acc.datacenter_id = ad.id where 1=1");
		if(type != null) {
			sql.append(" and type = ? ");
			param.add(type);
		}
//		if(StringUtils.isNotBlank(srcResourceId)) {
//			sql.append(" and cloud_resource_id <> ? ");
//			param.add(srcResourceId);
//		}
		if(StringUtils.isNotBlank(structure)) {
			sql.append(" and structure  = ? ");
			param.add(structure);
		}
		
		sql.append(" and is_scheduler = true ");
		List<CloudResourceConfigEntity> types = findBySql(sql.toString(), param);
		return types;
	}
	
	public List<DatacenterResourceVo> getAviableDatacenter(AviableDatacenterMsg msg, String structure){
		StringBuffer sql = new StringBuffer();
		List<Object> param = new ArrayList<>();
		sql.append(" select acc.*,ad.name as datacenterName,acr.name as cloud_resource_name, acr.id as cloudResourceId from asset_cloud_resource_config acc left join asset_datacenters ad on acc.datacenter_id = ad.id ");
		sql.append(" left join asset_cloud_resources acr on acc.cloud_resource_id = acr.id ");
		sql.append(" where 1=1 ");
		if(msg.getType() != null) {
			sql.append(" and acr.type = ? ");
			param.add(msg.getType());
		}
		if(StringUtils.isNotBlank(msg.getCloudResourceId())) {
			sql.append(" and acc.cloud_resource_id <> ? ");
			param.add(msg.getCloudResourceId());
		}
		if(StringUtils.isNotBlank(structure)) {
			sql.append(" and ad.structure  = ? ");
			param.add(structure);
		}
		if(StringUtils.isNotBlank(msg.getChosedDatacenterId())) {
			sql.append(" and acc.datacenter_id  = ? ");
			param.add(msg.getChosedDatacenterId());
		}
		List<DatacenterResourceVo> types = findBySql(sql.toString(), param, DatacenterResourceVo.class);
		return types;
	}

	public void deleteByCloudResourceId(String cloudResourceId) {
		StringBuffer buffer = new StringBuffer();
		buffer.append(" delete from asset_cloud_resource_config where cloud_resource_id = ? ");
		this.jdbcTemplate.update(buffer.toString(), cloudResourceId);
		
	}

}
