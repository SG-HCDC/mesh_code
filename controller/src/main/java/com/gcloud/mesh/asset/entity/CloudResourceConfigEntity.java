package com.gcloud.mesh.asset.entity;

import com.gcloud.framework.db.jdbc.annotation.ID;
import com.gcloud.framework.db.jdbc.annotation.Table;

import lombok.Data;
import lombok.experimental.Accessors;

@Table(name = "asset_cloud_resource_config")
@Data
@Accessors(chain = true)
public class CloudResourceConfigEntity {

	@ID
	private Integer id;
	private Integer type;
	private String datacenterId;
	private String config;
	private String cloudResourceId;
	
    private String insecureCommand;
    private String clusterId;

}
