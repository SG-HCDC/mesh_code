package com.gcloud.mesh.asset.entity;

import com.gcloud.framework.db.jdbc.annotation.ID;
import com.gcloud.framework.db.jdbc.annotation.Table;

import lombok.Data;

@Table( name = "asset_switchers")
@Data
public class SwitcherEntity {
	
	@ID
	private String id;
	private String ip;
	private String community;
	private Integer status;
	private String cabinetId;

}
