package com.gcloud.mesh.asset.enums;

import java.util.stream.Stream;

public enum PowerStatus {
	
	SHUTOFF(0, "shut_off", "已关闭"),
	RUNNING(1, "running", "运行中");
	
	private int no;
	private String name;
	private String cnName;
	
	PowerStatus(int no, String name, String cnName) {
		this.no = no;
		this.name = name;
		this.cnName = cnName;
	}
	
	public static PowerStatus getStatusByNo(int no) {
		return Stream.of(PowerStatus.values())
			.filter( s -> s.getNo() == no)
			.findFirst()
			.orElse(null);
	}
	
	public static PowerStatus getStatusByName(String name) {
		return Stream.of(PowerStatus.values())
			.filter( s -> s.getName().equals(name))
			.findFirst()
			.orElse(null);
	}
	
	public int getNo() {
		return no;
	}
	
	public String getName() {
		return name;
	}
	
	public String getCnName() {
		return cnName;
	}

}
