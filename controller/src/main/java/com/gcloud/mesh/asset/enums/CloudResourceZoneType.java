package com.gcloud.mesh.asset.enums;

import java.util.stream.Stream;

public enum CloudResourceZoneType {

	DATACENTER(0, "数据中心"),
	HOST(1, "服务器");

	private Integer type;
	private String name;

	CloudResourceZoneType(Integer type, String name) {
		this.type = type;
		this.name = name;
	}

	public static CloudResourceZoneType getByType(Integer type) {
		return Stream.of(CloudResourceZoneType.values()).filter(s -> s.getType() == type).findFirst().orElse(null);
	}

	public Integer getType() {
		return type;
	}

	public String getName() {
		return name;
	}

}
