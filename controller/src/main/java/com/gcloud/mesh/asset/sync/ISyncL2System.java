package com.gcloud.mesh.asset.sync;

public interface ISyncL2System {
	
	public void sync();
	
	public void syncNode();
	
	public void syncSwitcher();

}
