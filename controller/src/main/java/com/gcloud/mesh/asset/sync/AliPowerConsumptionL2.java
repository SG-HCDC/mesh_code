package com.gcloud.mesh.asset.sync;

import com.gcloud.mesh.asset.dao.IaasDao;
import com.gcloud.mesh.asset.dao.NodeDao;
import com.gcloud.mesh.asset.entity.IaasEntity;
import com.gcloud.mesh.asset.entity.NodeEntity;
import com.gcloud.mesh.asset.enums.DeviceType;
import com.gcloud.mesh.dcs.dao.AliPowerDao;
import com.gcloud.mesh.dcs.entity.AliPowerEntity;
import com.gcloud.mesh.header.enums.ResourceSourceType;
import com.gcloud.mesh.header.vo.asset.DeviceItemVo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.stream.Collectors;

@Slf4j
@Component
@ConditionalOnProperty(name = "asset.sync.l2", havingValue = "ali")
public class AliPowerConsumptionL2 implements ISyncL2System {
	
	@Autowired
	private AliPowerDao aliPowerDao;
	
	@Autowired
	private IaasDao iaasDao;
	
	@Autowired
	private NodeDao nodeDao;
	
	private static final String GENERAL = "general";

	@Override
	public void syncNode() {
		// TODO Auto-generated method stub
		List<AliPowerEntity> aliPowerNodes = aliPowerDao.findAll();
		Map<String, AliPowerEntity> aliPowerMap = aliPowerNodes.stream().collect(Collectors.toMap(AliPowerEntity::getEsn, s -> s));
		List<DeviceItemVo> nodes = iaasDao.listDevice(null,  DeviceType.SERVER.getNo()).stream().filter( s -> s.getFrom().equals(ResourceSourceType.SYNC.getName())).collect(Collectors.toList());
		Map<String, DeviceItemVo> nodeMap = nodes.stream().collect(Collectors.toMap(DeviceItemVo::getEsn, s -> s));
		List<String> nodeEsns = nodes.stream().map( s -> s.getEsn()).collect(Collectors.toList());
		for(Map.Entry<String, AliPowerEntity> entry: aliPowerMap.entrySet()) {
			//同步删除
			if(nodeEsns.contains(entry.getKey())) {
				delete(nodeMap.get(entry.getKey()));
			}
			//同步新增
			save(entry.getValue());
		}
				
	}

	@Override
	public void syncSwitcher() {
		// TODO Auto-generated method stub
		
	}
	
	private void delete(DeviceItemVo deviceItem) {
		try {
			nodeDao.deleteById(deviceItem.getDeviceId());
			iaasDao.deleteById(deviceItem.getId());
		}catch(Exception e) {
			log.error("[AliPowerConsumption][delete] 服务器【%s】删除失败", deviceItem.getId());
		}
	}
	
	private void save(AliPowerEntity aliPower) {
		try {
			IaasEntity iaas = new IaasEntity();
			iaas.setId(UUID.randomUUID().toString());
			iaas.setDatacenterId(aliPower.getDatacenterId());
			iaas.setDeviceModel(aliPower.getDeviceModel());
			iaas.setDeviceManufacturer(aliPower.getDeviceManufacturer());
			iaas.setEsn(aliPower.getEsn());
			iaas.setFrom(ResourceSourceType.SYNC.getName());
			iaas.setName(aliPower.getEsn());
			iaas.setType(DeviceType.SERVER.getNo());
			iaas.setVisible(true);
			iaas.setDeviceId(aliPower.getId());
			Date createTime = new Date();
			iaas.setCreateTime(createTime);
			NodeEntity node = new NodeEntity();
			node.setId(aliPower.getId());
			node.setHostname(aliPower.getHostname());
			node.setIpmiIp(aliPower.getIpmiIp());
			node.setMgmIp(aliPower.getMgmIp());
			node.setPowerConsumption(aliPower.getPowerConsumption());
			node.setPowerStatus(aliPower.getPowerStatus());
			iaasDao.save(iaas);
			nodeDao.save(node);
		}catch(Exception e) {
			log.error("[AliPowerConsumption][save] 服务器【ESN:%s】保存失败", aliPower.getEsn());
		}
	}

	@Override
	public void sync() {
		// TODO Auto-generated method stub
		syncNode();
		syncSwitcher();
	}
 
}
