package com.gcloud.mesh.asset.timer;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.gcloud.mesh.analysis.dao.SubhealthAlertDao;
import com.gcloud.mesh.dcs.dao.ModelScoreDao;
import com.gcloud.mesh.dcs.service.DataCleanService;
import com.gcloud.mesh.header.exception.BaseException;
import com.gcloud.mesh.header.vo.dcs.DataCleanConfigVo;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
public class SubhealthlogCleanTimer {

	@Autowired
	SubhealthAlertDao subhealDao;

	@Autowired
	ModelScoreDao scoreDao;

	// 一天一清
	@Scheduled(fixedDelay = 1000 * 60 * 60 * 24)
	public void work() {
		log.info("[SubhealthlogClean] 亚健康告警日志清洗开始 START");
		String date = getPastDate(3);
		log.info(" delete subhealthLog date is" + date);
		subhealDao.deleteByDate(date);

		log.info("[SubhealthlogClean] 亚健康告警日志清洗结束END");

		log.info("[ModelScoreClean] 评分记录清洗开始 START");
		date = getPastDate(1);
		log.info(" delete modelScore date is" + date);
		scoreDao.deleteByDate(date);

		log.info("[ModelScoreClean] 评分记录清洗结束END");
	}

	public static String getPastDate(int past) {
		Calendar calendar = Calendar.getInstance();
		calendar.set(Calendar.DAY_OF_YEAR, calendar.get(Calendar.DAY_OF_YEAR) - past);
		Date today = calendar.getTime();
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
		String result = format.format(today);
		return result;
	}
}
