package com.gcloud.mesh.asset.enums;

import java.util.stream.Stream;

public enum DaHuaDeviceType {
	
	//GENERAL(0, "general", "内存设备", DeviceType.GENERAL),
	AIR_CONDITION(1, "air_condition", "2", DeviceType.AIR_CONDITION),
	DISTRIBUTION_BOX(2, "distribution_box", "1", DeviceType.DISTRIBUTION_BOX),
	UPS(3, "ups", "3", DeviceType.UPS);

	private int no;
	private String name;
	private String deviceCategroy;
	private DeviceType deviceType;

	DaHuaDeviceType(int no, String name, String deviceCategroy, DeviceType deviceType) {
		this.no = no;
		this.name = name;
		this.deviceCategroy = deviceCategroy;
		this.deviceType = deviceType;
	}

	public static DaHuaDeviceType getDeviceTypeByNo(int no) {
		return Stream.of(DaHuaDeviceType.values()).filter(s -> s.getNo() == no).findFirst().orElse(null);
	}

	public static DaHuaDeviceType getDeviceTypeByName(String name) {
		return Stream.of(DaHuaDeviceType.values()).filter(s -> s.getName().equals(name)).findFirst().orElse(null);
	}

	public static DaHuaDeviceType getDeviceTypeByDeviceCategroy(String deviceCategroy) {
		return Stream.of(DaHuaDeviceType.values()).filter(s -> s.getDeviceCategroy().equals(deviceCategroy)).findFirst().orElse(null);
	}

	public int getNo() {
		return no;
	}

	public String getName() {
		return name;
	}

	public String getDeviceCategroy() {
		return deviceCategroy;
	}

	public DeviceType getDeviceType() {
		return deviceType;
	}
}
