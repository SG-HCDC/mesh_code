package com.gcloud.mesh.asset.timer;

import java.util.Calendar;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.gcloud.mesh.dcs.service.DataCleanService;
import com.gcloud.mesh.header.exception.BaseException;
import com.gcloud.mesh.monitor.dao.StatisticsDao;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
public class AlertDataCleanTimer {

	@Autowired
	DataCleanService service;

	@Autowired
	StatisticsDao statisticsDao;

	// 一天一清
	@Scheduled(fixedDelay = 1000 * 60 * 60 * 24)
	public void work() {

		// 保存半年
		Calendar c = Calendar.getInstance();
		c.setTime(new Date());
		c.add(Calendar.MONTH, -6);
		Date day = c.getTime();

		try {
			statisticsDao.deleteByTime(day);
		} catch (Exception e) {
			throw new BaseException(e);
		}
	}
}
