package com.gcloud.mesh.asset.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.gcloud.framework.db.PageResult;
import com.gcloud.framework.db.dao.impl.JdbcBaseDaoImpl;
import com.gcloud.mesh.asset.entity.DatacenterEntity;
import com.gcloud.mesh.asset.enums.DeviceType;

import io.micrometer.core.instrument.util.StringUtils;

@Repository
public class DatacenterDao extends JdbcBaseDaoImpl<DatacenterEntity, String> {

	public DatacenterEntity getById(String id, String userId) {
		StringBuffer sql = new StringBuffer();
		sql.append(" select * from asset_datacenters");
		sql.append(" where 1 = 1");
		Map<String, Object> props = new HashMap<String, Object>();
		if (StringUtils.isNotEmpty(id)) {
			props.put("id", id);
		}
		if (StringUtils.isNotEmpty(userId)) {
			props.put("creator", userId);
		}
		return this.findUniqueByProperties(props);
	}

	public <E> PageResult<E> page(int pageNum, int pageSize, String id, String name, String ip, String userId, Boolean hasNode,
			Class<E> clazz) {
		StringBuffer sql = new StringBuffer();
		//sql.append(" select dt.*,sp.cluster_id,sp.type as sp_type from asset_datacenters dt left join supplier_suppliers sp on sp.datacenter_id = dt.id");
		//zhangdp去掉mcj的左关联查询导致的页面重复记录问题
		sql.append(" select dt.* from asset_datacenters dt");
		if(hasNode != null && hasNode == true) {
			sql.append(" LEFT JOIN (select datacenter_id,count(id) as count  from asset_iaas where type = ").append(String.valueOf(DeviceType.SERVER.getNo())).append(" GROUP BY datacenter_id ) ds on dt.id = ds.datacenter_id ");
		}
		sql.append(" where 1 = 1");
		if(hasNode != null && hasNode == true) {
			sql.append(" and count IS NOT NULL");
		}
		List<Object> values = new ArrayList<Object>();
		if (StringUtils.isNotEmpty(userId)) {
			sql.append(" and dt.creator = ?");
			values.add(userId);
		}
		if (StringUtils.isNotEmpty(name)) {
			sql.append(" and dt.name like concat('%', ?, '%')");
			values.add(name);
		}
		if (StringUtils.isNotEmpty(ip)) {
			sql.append(" and dt.ip like concat('%', ?, '%')");
			values.add(ip);
		}
		// 增加数据中心id过滤
		if (StringUtils.isNotEmpty(id)) {
			sql.append(" and dt.id = ?");
			values.add(id);
		}

		sql.append(" order by dt.create_time");
		return this.findBySql(sql.toString(), values, pageNum, pageSize, clazz);
	}

	public <E> PageResult<E> page(int pageNum, int pageSize, String name, String ip, String userId, Boolean hasNode, Class<E> clazz) {

		return page(pageNum, pageSize, null, name, ip, userId, hasNode, clazz);
	}

	public <E> PageResult<E> pageById(int pageNum, int pageSize, String id, String ip, String userId, Class<E> clazz) {

		return page(pageNum, pageSize, id, null, ip, userId, null ,clazz);
	}

	public int count() {
		StringBuilder sql = new StringBuilder("SELECT COUNT(*) FROM asset_datacenters WHERE 1=1");
		return this.countBySql(sql.toString(), new ArrayList<Object>());
	}
}
