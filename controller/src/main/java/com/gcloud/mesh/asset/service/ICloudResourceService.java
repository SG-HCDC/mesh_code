package com.gcloud.mesh.asset.service;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import com.gcloud.framework.db.PageResult;
import com.gcloud.mesh.asset.entity.CloudResourceEntity;
import com.gcloud.mesh.header.msg.asset.CreateCloudResourceMsg;
import com.gcloud.mesh.header.msg.asset.PageCloudResourceMsg;
import com.gcloud.mesh.header.msg.asset.UpdateCloudResourceMsg;
import com.gcloud.mesh.header.vo.asset.CloudResourceAnalysisVo;
import com.gcloud.mesh.header.vo.asset.CloudResourceItemVo;

public interface ICloudResourceService {

	String create(@Valid CreateCloudResourceMsg msg);
	
	String create(@Valid CreateCloudResourceMsg msg, String userId, HttpServletRequest request, HttpServletResponse response);

	CloudResourceEntity delete(String id);

	void update(UpdateCloudResourceMsg msg);
	
	void update(UpdateCloudResourceMsg msg, String userId, HttpServletRequest request, HttpServletResponse response);

	PageResult<CloudResourceItemVo> page(@Valid PageCloudResourceMsg msg);

	CloudResourceItemVo detail(String id);
	
	CloudResourceItemVo detail(String id, String userId, HttpServletRequest request, HttpServletResponse response);
	
	String sync();

	CloudResourceAnalysisVo analysis();
	
	int count(Integer type);
	
	// void updateMigrationResult(String appid, String dstDatacenterId, String dstNodeId, String schedulerModel);
}
