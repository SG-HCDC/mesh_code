package com.gcloud.mesh.asset.enums;

import java.util.stream.Stream;

import com.gcloud.mesh.header.enums.AuthorityResourceType;

public enum AuthorityDeviceType {
	
	DISTRIBUTE_BOX(AuthorityResourceType.DISTRIBUTE_BOX, DeviceType.DISTRIBUTION_BOX),
	AIR(AuthorityResourceType.AIR, DeviceType.AIR_CONDITION),
	PDU(AuthorityResourceType.PDU, DeviceType.UPS),
	UPS(AuthorityResourceType.UPS, DeviceType.PDU),
	TEMPERTURE_SENSOR(AuthorityResourceType.TEMPERTURE_SENSOR, DeviceType.TEMPER_SENSOR),
	CABINET(AuthorityResourceType.CABINET, DeviceType.CABINET),
	SERVER(AuthorityResourceType.SERVER, DeviceType.SERVER),
	SWITCHER(AuthorityResourceType.SWITCHER, DeviceType.SWITCHER);
	
	private AuthorityResourceType resource;
	private DeviceType device;
	
	AuthorityDeviceType(AuthorityResourceType resource, DeviceType device) {
		this.resource = resource;
		this.device = device;
	}

	public static AuthorityDeviceType getByResource(AuthorityResourceType resource) {
		return Stream.of(AuthorityDeviceType.values())
				.filter( s -> s.getResource().equals(resource))
				.findFirst()
				.orElse(null);
	}
	
	public static AuthorityDeviceType getByDeviceResource(DeviceType device) {
		return Stream.of(AuthorityDeviceType.values())
				.filter( s -> s.getResource().equals(device))
				.findFirst()
				.orElse(null);
	}
	
	public AuthorityResourceType getResource() {
		return resource;
	}

	public DeviceType getDevice() {
		return device;
	}
	
	
}

