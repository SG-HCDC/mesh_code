package com.gcloud.mesh.asset.utils.supervisionsystem;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.gcloud.mesh.asset.dao.IaasDao;
import com.gcloud.mesh.asset.entity.IaasEntity;
import com.gcloud.mesh.asset.enums.DaHuaDeviceType;
import com.gcloud.mesh.asset.service.IAssetService;
import com.gcloud.mesh.header.enums.ResourceSourceType;
import com.gcloud.mesh.header.msg.asset.ListDatacenterMsg;
import com.gcloud.mesh.header.vo.asset.DatacenterItemVo;
import com.gcloud.mesh.header.vo.asset.DeviceItemVo;
import com.gcloud.mesh.header.vo.supplier.DaHuaAssetItemVo;
import com.gcloud.mesh.header.vo.supplier.DaHuaSupervisionSystemVo;
import com.gcloud.mesh.supplier.dao.SupplierDao;
import com.gcloud.mesh.supplier.entity.SupplierEntity;
import com.gcloud.mesh.utils.SupplierSystemTypeUtil;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
@ConditionalOnProperty(name = "asset.supervision_system", havingValue = "dahua")
public class DaHuaSupervisionSystem implements ISupervisionSystem {

	@Autowired
	private SupplierDao supplierDao;
	
	@Autowired
	private IAssetService assetService;

	@Autowired
	private IaasDao iaasDao;

	private static final String TYPE = "supervision_system_dahua";

	private static final String GENERAL = "general";

	private static final String path = "/xtjk/webService/queryAllDeviceInfo.action";

	@Override
	public <E> List<E> sync(Class clazz) {
		return null;
	}

	@Override
	public void syncDevice(int type) {
		List<SupplierEntity> suppliers = supplierDao.findAll().stream().filter(s -> TYPE.equals(s.getType()))
				.collect(Collectors.toList());
		for (SupplierEntity supplier : suppliers) {
			DaHuaSupervisionSystemVo obj = (DaHuaSupervisionSystemVo) SupplierSystemTypeUtil
					.getByName(supplier.getType(), supplier.getConfig());
			List<DaHuaAssetItemVo> items = listAssets(obj.getHostIp(), obj.getPort());
			Map<String, Object> props = new HashMap<String, Object>();
			props.put("datacenter_id", supplier.getDatacenterId());
			props.put("from", ResourceSourceType.SYNC.getName());
			List<DeviceItemVo> devices = iaasDao.findByProperties(props, DeviceItemVo.class).stream()
					.filter(s -> s.getType() == type).collect(Collectors.toList());
			saveDevice(items, devices, supplier.getDatacenterId());
			updateDevice(items, devices, supplier.getDatacenterId());
			deleteDevice(items, devices);
		}
	}

	public List<DaHuaAssetItemVo> listAssets(String hostIp, String port) {
		String url = "http://" + hostIp + ":" + port + path;
		RestTemplate restTemplate = new RestTemplate();
		String res = restTemplate.getForEntity(url, String.class).getBody();
		JSONObject resJ = JSONObject.parseObject(res);
		JSONArray jsonArray = resJ.getJSONArray("rows");
		List<DaHuaAssetItemVo> assets = new ArrayList<DaHuaAssetItemVo>();

		for (int i = 0; i < jsonArray.size(); i++) {
			JSONObject json = jsonArray.getJSONObject(i);
			DaHuaAssetItemVo vo = (DaHuaAssetItemVo) JSONObject.parseObject(json.toString(), DaHuaAssetItemVo.class);
			assets.add(vo);
		}

		return assets;
	}

	private void saveDevice(List<DaHuaAssetItemVo> items, List<DeviceItemVo> devices, String datacenterId) {
		List<String> exists = devices.stream().map(s -> s.getName()).collect(Collectors.toList());
		List<DaHuaAssetItemVo> newItems = items.stream().filter(s -> !exists.contains(s.getDeviceName()))
				.collect(Collectors.toList());
		if (newItems != null) {
			for (DaHuaAssetItemVo vo : newItems) {
				IaasEntity iaas = new IaasEntity();
				iaas.setId(UUID.randomUUID().toString());
				iaas.setName(vo.getDeviceName());
				iaas.setEsn(GENERAL);
				
				//根据机房划分数据中心
				List<DatacenterItemVo> dcs = assetService.listDatacenter(new ListDatacenterMsg());
				for(DatacenterItemVo dc : dcs){
					if(dc.getRooms() != null && dc.getRooms().contains(vo.getRoomName())){
						iaas.setDatacenterId(dc.getId());
					}else {
						iaas.setDatacenterId(datacenterId);
					}
				}
				iaas.setCreator(null);
				iaas.setCreateTime(new Date());
				iaas.setDeviceManufacturer(GENERAL);
				iaas.setDeviceModel(GENERAL);
				// iaas.setVisible(true);
				iaas.setFrom(ResourceSourceType.SYNC.getName());

				iaas.setDeviceId(vo.getId().toString());

				if (DaHuaDeviceType.getDeviceTypeByDeviceCategroy(vo.getDeviceCategroy()) != null) {
					iaas.setType(DaHuaDeviceType.getDeviceTypeByDeviceCategroy(vo.getDeviceCategroy()).getDeviceType().getNo());
				}

				try {
					iaasDao.save(iaas);
				} catch (Exception e) {
					log.error("save deivce resource from supervision system failed : {}", e.getMessage());
				}
			}
		}
	}

	private void updateDevice(List<DaHuaAssetItemVo> items, List<DeviceItemVo> devices, String datacenterId) {

	}

	private void deleteDevice(List<DaHuaAssetItemVo> items, List<DeviceItemVo> devices) {
		List<String> exists = items.stream().map(s -> s.getDeviceName()).collect(Collectors.toList());
		List<DeviceItemVo> deletes = devices.stream().filter(s -> !exists.contains(s.getName()))
				.collect(Collectors.toList());
		if (deletes != null) {
			for (DeviceItemVo vo : deletes) {
				try {
					iaasDao.deleteById(vo.getId());
				} catch (Exception e) {
					log.error("delete deivce resource from supervision system failed : {}", e.getMessage());
				}
			}
		}
	}

	@Override
	public void syncNodeAndSwitcher(int type) {
		// TODO Auto-generated method stub

	}

}
