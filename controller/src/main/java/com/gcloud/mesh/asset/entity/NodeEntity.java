package com.gcloud.mesh.asset.entity;

import com.gcloud.framework.db.jdbc.annotation.ID;
import com.gcloud.framework.db.jdbc.annotation.Table;

import lombok.Data;

@Table( name = "asset_nodes")
@Data
public class NodeEntity {
	
	@ID
	private String id;
	private String mgmIp;
	private String ipmiIp;
	private String ipmiUser;
	private String ipmiPassword;
	private Integer networkStatus;
	private Integer powerStatus;
	private String hostname;
	private Float ratedPower;
	private Float powerConsumption; 
	private Float energyEfficiencyRatio;
	private Float storageCapacity;
	private Boolean isolation;
	private String ratedPowerSm;
	private String ratedVoltageSm;
	private String smData;
	private String cabinetId;
}
