package com.gcloud.mesh.asset.entity;

import java.util.Date;

import com.gcloud.framework.db.jdbc.annotation.ID;
import com.gcloud.framework.db.jdbc.annotation.Table;

import lombok.Data;

@Table(name = "asset_cloud_resources")
@Data
public class CloudResourceEntity {

	@ID
	private String id;
	private String name;
	private Integer type;
	private String datacenterId;
	private String expand;
	private Date createTime;
	private Integer cpuCores;
	private Integer memory;
	private String from;
	private String objectId;

}
