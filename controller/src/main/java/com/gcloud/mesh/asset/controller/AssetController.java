package com.gcloud.mesh.asset.controller;

import com.gcloud.framework.db.PageResult;
import com.gcloud.mesh.asset.entity.IaasEntity;
import com.gcloud.mesh.asset.service.IAssetService;
import com.gcloud.mesh.dcs.annotation.ParamSecurity;
import com.gcloud.mesh.header.msg.BaseReplyMsg;
import com.gcloud.mesh.header.msg.asset.*;
import com.gcloud.mesh.header.vo.asset.*;
import com.gcloud.mesh.sm.service.SmService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import me.zhyd.oauth.log.Log;

import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.jeecg.common.aspect.annotation.AutoLog;
import org.jeecg.common.constant.CommonConstant;
import org.jeecg.common.constant.enums.SysLogOperateType;
import org.jeecg.common.constant.enums.SysLogType;
import org.jeecg.common.system.api.ISysBaseAPI;
import org.jeecg.common.system.util.JwtUtil;
import org.jeecg.common.system.vo.LoginUser;
import org.jeecg.common.util.sm.SmManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.util.List;

@Api(tags = "资产管理")
@RestController
@RequestMapping("/asset")
@Slf4j
public class AssetController {

	@Autowired
	private IAssetService service;
	
	@Autowired
	private SmService smService;
	
	@Autowired
	private ISysBaseAPI sysBaseAPI;
	
	private String userId = null;

	private static final String HEADER_TOKEN = "X-Access-Token";
	
	//处理Mass Assignment: Insecure Binder Configuration
	@InitBinder
	public void populateCustomerRequest(WebDataBinder binder) {
	    binder.setDisallowedFields(new String[]{});
	}

    @AutoLog(value = "新增数据中心",operateType = CommonConstant.OPERATE_TYPE_ADD, module = "精确管控与供电制冷联动-基础资源细粒度感知-资源管理-数据中心")
	@ApiOperation(value = "新增数据中心", notes = "新增数据中心")
	@RequestMapping(value = "/createDatacenter", method = RequestMethod.POST)
	@RequiresPermissions(value = {"assetCreateDatacenter"})
    @ParamSecurity(enabled = true)
	public void createDatacenter(@Valid CreateDatacenterMsg msg) throws Exception {
		service.createDatacenter(msg, userId);
	}

    // @AutoLog(value = "编辑数据中心",operateType = CommonConstant.OPERATE_TYPE_UPDATE)
	@ApiOperation(value = "编辑数据中心", notes = "编辑数据中心")
	@RequestMapping(value = "/updateDatacenter", method = RequestMethod.POST)
	@RequiresPermissions(value = {"assetUpdateDatacenter"})
	@ParamSecurity(enabled = true)
	public void updateDatacenter(@Valid UpdateDatacenterMsg msg) throws Exception {
		if(StringUtils.isNotBlank(msg.getTitle())) {
			sysBaseAPI.addLog((msg.getContent() != null?msg.getContent()+"":"设置动环节能")+"，数据中心ID："+ msg.getId() , SysLogType.OPERATE, SysLogOperateType.UPDATE, null, null, "精确管控与供电制冷联动-统一管理联动平台-动环节能");
		}else {
			sysBaseAPI.addLog(msg.getContent() != null?msg.getContent()+"；ID:":"更新数据中心；ID:" + msg.getId(), SysLogType.OPERATE, SysLogOperateType.UPDATE, null, null, "精确管控与供电制冷联动-基础资源细粒度感知-资源管理-数据中心");
		}
		
		service.updateDatacenter(msg, userId);
	}

    @AutoLog(value = "删除数据中心",operateType = CommonConstant.OPERATE_TYPE_DELETE)
	@ApiOperation(value = "删除数据中心", notes = "删除数据中心")
	@RequestMapping(value = "/deleteDatacenter", method = RequestMethod.POST)
	@RequiresPermissions(value = {"assetDeleteDatacenter"})
    @ParamSecurity(enabled = true)
	public void deleteDatacenter(@Valid DeleteDatacenterMsg msg) throws Exception {
		service.deleteDatacenter(msg.getId(), userId);

	}

    // @AutoLog(value = "数据中心详情",operateType = CommonConstant.OPERATE_TYPE_DETAIL)
	@ApiOperation(value = "数据中心详情", notes = "数据中心详情")
	@RequestMapping(value = "/detailDatacenter", method = RequestMethod.POST)
	@RequiresPermissions(value = {"assetDetailDatacenter"})
	@ParamSecurity(enabled = true)
	public BaseReplyMsg detailDatacenter(@Valid DetailDatacenterMsg msg) throws Exception {
		DatacenterItemVo vo = service.detailDatacenter(msg.getId(), userId);
		BaseReplyMsg reply = new BaseReplyMsg();
		reply.setData(vo);
		sysBaseAPI.addLog((msg.getContent() != null?msg.getContent():"数据中心详情")+ "，数据中心ID："+ msg.getId(), SysLogType.OPERATE, SysLogOperateType.DETAIL, null, null, "精确管控与供电制冷联动-基础资源细粒度感知-资源管理-数据中心");
		return reply;
	}

  
    // @AutoLog(value = "查看数据中心列表",operateType = CommonConstant.OPERATE_TYPE_LIST, module = "精确管控与供电制冷联动-基础资源细粒度感知-资源管理-数据中心")
	@ApiOperation(value = "数据中心分页", notes = "数据中心分页")
	@RequestMapping(value = "/pageDatacenter", method = RequestMethod.POST)
	@RequiresPermissions(value = {"assetPageDatacenter"})
    @ParamSecurity(enabled = true)
	public PageResult<DatacenterItemVo> pageDatacenter(@Valid PageDatacenterMsg msg) throws Exception {
		PageResult<DatacenterItemVo> result = service.pageDatacenter(msg.getPageNo(), msg.getPageSize(), msg.getId(),
				msg.getName(), msg.getIp(), userId, msg.getHasNode());
		if(msg.getTitle() != null) {
			sysBaseAPI.addLog(msg.getContent() != null?msg.getContent():"查看数据中心", SysLogType.OPERATE, SysLogOperateType.DETAIL, null, null, msg.getTitle() != null ? msg.getTitle():"精确管控与供电制冷联动-基础资源细粒度感知-资源管理-数据中心");
		}
		return result;
	}

    //@AutoLog(value = "查询数据中心列表",operateType = CommonConstant.OPERATE_TYPE_LIST, module =  "精确管控与供电制冷联动-基础资源细粒度感知-资源管理-数据中心")
	@ApiOperation(value = "数据中心列表", notes = "数据中心列表")
	@RequestMapping(value = "/listDatacenter", method = RequestMethod.POST)
	@RequiresPermissions(value = {"assetListDatacenter"})
    @ParamSecurity(enabled = true)
	public List<DatacenterItemVo> listDatacenter(ListDatacenterMsg msg) throws Exception {
		List<DatacenterItemVo> result = service.listDatacenter(msg);
		if(StringUtils.isNotBlank(msg.getTitle())) {
			if(msg != null) {
				sysBaseAPI.addLog("查询数据中心列表", SysLogType.OPERATE, SysLogOperateType.QUERY, null, null, msg.getTitle() != null ? msg.getTitle():"精确管控与供电制冷联动-基础资源细粒度感知-资源管理-数据中心");
			}
		}
		return result;
	}

    // @AutoLog(value = "新增设备",operateType = CommonConstant.OPERATE_TYPE_ADD)
	@ApiOperation(value = "新增设备", notes = "新增设备")
	@RequestMapping(value = "/createDevice", method = RequestMethod.POST)
	@RequiresPermissions(value = {"assetCreateDevice"})
	@ParamSecurity(enabled = true)
	public BaseReplyMsg createDevice(@Valid CreateDeviceMsg msg) throws Exception {
    	BaseReplyMsg reply = new BaseReplyMsg();

    	// sysBaseAPI.addLog("新增设备", SysLogType.OPERATE, SysLogOperateType.ADD, null, null, "精确管控与供电制冷联动-基础资源细粒度感知-资源管理-L1层：基础设施");
		String id = service.createDevice(msg, userId);
    	StringBuffer buff = new StringBuffer();
    	buff.append("新增基础设施；ID：" + id);
    	buff.append("，"+msg.toString());
    	sysBaseAPI.addLog(buff.toString(), SysLogType.OPERATE, SysLogOperateType.ADD, null, null, "精确管控与供电制冷联动-基础资源细粒度感知-资源管理-L1层：基础设施");
		return reply;
	}

    // @AutoLog(value = "编辑设备",operateType = CommonConstant.OPERATE_TYPE_UPDATE)
	@ApiOperation(value = "编辑设备", notes = "编辑设备")
	@RequestMapping(value = "/updateDevice", method = RequestMethod.POST)
	@RequiresPermissions(value = {"assetUpdateDevice"})
	@ParamSecurity(enabled = true)
	public BaseReplyMsg updateDevice(@Valid UpdateDeviceMsg msg) throws Exception {
    	BaseReplyMsg reply = new BaseReplyMsg();
		service.updateDevice(msg, userId);
    	StringBuffer buff = new StringBuffer();
    	buff.append("编辑基础设施；ID:" + msg.getId());
    	buff.append(msg.toString());
		sysBaseAPI.addLog(buff.toString(), SysLogType.OPERATE, SysLogOperateType.UPDATE, null, null, "精确管控与供电制冷联动-基础资源细粒度感知-资源管理-L1层：基础设施");
		//sysBaseAPI.addLog("编辑设备；ID:" + msg.getId(), null, CommonConstant.OPERATE_TYPE_UPDATE);
		return reply;
	}

    // @AutoLog(value = "删除设备",operateType = CommonConstant.OPERATE_TYPE_DELETE)
	@ApiOperation(value = "删除设备", notes = "删除设备")
	@RequestMapping(value = "/deleteDevice", method = RequestMethod.POST)
	@RequiresPermissions(value = {"assetDeleteDevice"})
	@ParamSecurity(enabled = true)
	public BaseReplyMsg deleteDevice(@Valid DeleteDeviceMsg msg) throws Exception {
    	BaseReplyMsg reply = new BaseReplyMsg();
    	IaasEntity en = service.deleteDevice(msg.getId(), userId);
		sysBaseAPI.addLog("删除基础设施；ID:" + msg.getId()+"，基础设施名称："+en.getName(),SysLogType.OPERATE, SysLogOperateType.DELETE, null, null, "精确管控与供电制冷联动-基础资源细粒度感知-资源管理-L1层：基础设施");
		// sysBaseAPI.addLog("删除设备；ID:" + msg.getId(), null, CommonConstant.OPERATE_TYPE_DELETE);
		return reply;
	}

    // @AutoLog(value = "设备详情",operateType = CommonConstant.OPERATE_TYPE_DETAIL)
	@ApiOperation(value = "设备详情", notes = "设备详情")
	@RequestMapping(value = "/detailDevice", method = RequestMethod.POST)
	@RequiresPermissions(value = {"assetDetailDevice"})
	@ParamSecurity(enabled = true)
	public BaseReplyMsg detailDevice(@Valid DetailDeviceMsg msg) throws Exception {
		DeviceItemVo vo = service.detailDevice(msg.getId(), userId);
		BaseReplyMsg reply = new BaseReplyMsg();
		reply.setData(vo);
		sysBaseAPI.addLog("查看基础设施详情；ID:" + msg.getId()+"，基础设施名称："+vo.getName(),SysLogType.OPERATE, SysLogOperateType.DETAIL, null, null, "精确管控与供电制冷联动-基础资源细粒度感知-资源管理-L1层：基础设施");
		// sysBaseAPI.addLog("设备详情；ID:" + msg.getId(), null, CommonConstant.OPERATE_TYPE_DETAIL);
		return reply;
	}

    // @AutoLog(value = "查看设备列表",operateType = CommonConstant.OPERATE_TYPE_LIST, module = "精确管控与供电制冷联动-基础资源细粒度感知-资源管理-L1层：基础设施")
	@ApiOperation(value = "设备分页", notes = "设备分页")
	@RequestMapping(value = "/pageDevice", method = RequestMethod.POST)
	@RequiresPermissions(value = {"assetPageDevice"})
    @ParamSecurity(enabled = true)
	public PageResult<DeviceItemVo> pageDevice(@Valid PageDeviceMsg msg) throws Exception {
		PageResult<DeviceItemVo> result = service.pageDevice(msg.getPageNo(), msg.getPageSize(), msg.getDatacenterId(), msg.getType(),
				msg.getName(), userId);
		sysBaseAPI.addLog(msg.getContent() != null?msg.getContent():"查询基础设施列表", SysLogType.OPERATE, SysLogOperateType.QUERY, null, null, msg.getTitle() != null ? msg.getTitle():"精确管控与供电制冷联动-基础资源细粒度感知-资源管理-L1层：基础设施");
		return result;
	}

    // @AutoLog(value = "新增交换机",operateType = CommonConstant.OPERATE_TYPE_ADD)
	@ApiOperation(value = "新增交换机", notes = "新增交换机")
	@RequestMapping(value = "/createSwitcher", method = RequestMethod.POST)
	@RequiresPermissions(value = {"assetCreateSwitcher"})
	@ParamSecurity(enabled = true)
	public BaseReplyMsg createSwitcher(@Valid CreateSwitcherMsg msg, HttpServletRequest request, HttpServletResponse response) throws Exception {
    	BaseReplyMsg reply = new BaseReplyMsg();
//    	service.createSwitcher(msg, getUser(request, response));
    	String id = service.createSwitcher(msg, getUser(request, response), request, response);
    	StringBuffer buff = new StringBuffer();
    	buff.append("新增IT设备；ID:" + id);
    	buff.append("，"+msg.toString());
    	sysBaseAPI.addLog(buff.toString() + id,SysLogType.OPERATE, SysLogOperateType.ADD, null, null, msg.getTitle() != null ? msg.getTitle():"精确管控与供电制冷联动-基础资源细粒度感知-资源管理-L2层：IT设备");
//    	sysBaseAPI.addLog("新增交换机；ID:" + id, null, CommonConstant.OPERATE_TYPE_ADD);
    	return reply;
	}

    // @AutoLog(value = "编辑交换机",operateType = CommonConstant.OPERATE_TYPE_UPDATE)
	@ApiOperation(value = "编辑交换机", notes = "编辑交换机")
	@RequestMapping(value = "/updateSwitcher", method = RequestMethod.POST)
	@RequiresPermissions(value = {"assetUpdateSwitcher"})
	@ParamSecurity(enabled = true)
	public BaseReplyMsg updateSwitcher(@Valid UpdateSwitcherMsg msg, HttpServletRequest request, HttpServletResponse response) throws Exception {
    	BaseReplyMsg reply = new BaseReplyMsg();
//    	service.updateSwitcher(msg, getUser(request, response));
    	service.updateSwitcher(msg, getUser(request, response), request, response);
    	StringBuffer buff = new StringBuffer();
    	buff.append("编辑IT设备；ID:" + msg.getId());
    	buff.append("，"+msg.toString());
    	sysBaseAPI.addLog(buff.toString(),SysLogType.OPERATE, SysLogOperateType.UPDATE, null, null, msg.getTitle() != null ? msg.getTitle(): "精确管控与供电制冷联动-基础资源细粒度感知-资源管理-L2层：IT设备");
 //   	sysBaseAPI.addLog("编辑交换机；ID:" + msg.getId(), null, CommonConstant.OPERATE_TYPE_UPDATE);
    	return reply;
	}

    // @AutoLog(value = "删除交换机",operateType = CommonConstant.OPERATE_TYPE_DELETE)
	@ApiOperation(value = "删除交换机", notes = "删除交换机")
	@RequestMapping(value = "/deleteSwitcher", method = RequestMethod.POST)
	@RequiresPermissions(value = {"assetDeleteSwitcher"})
	@ParamSecurity(enabled = true)
	public BaseReplyMsg deleteSwitcher(@Valid DeleteSwitcherMsg msg) throws Exception {
    	BaseReplyMsg reply = new BaseReplyMsg();
    	IaasEntity iaas = service.deleteSwitcher(msg.getId(), userId);
    	sysBaseAPI.addLog("删除设备；ID:" + msg.getId()+"，交换机名称："+iaas.getName(),SysLogType.OPERATE, SysLogOperateType.DELETE, null, null, msg.getTitle() != null ? msg.getTitle():"精确管控与供电制冷联动-基础资源细粒度感知-资源管理-L2层：IT设备");
		// sysBaseAPI.addLog("删除交换机；ID:" + msg.getId(), null, CommonConstant.OPERATE_TYPE_UPDATE);
		return reply;
	}

    // @AutoLog(value = "交换机详情",operateType = CommonConstant.OPERATE_TYPE_DETAIL)
	@ApiOperation(value = "交换机详情", notes = "交换机详情")
	@RequestMapping(value = "/detailSwitcher", method = RequestMethod.POST)
	@RequiresPermissions(value = {"assetDetailSwitcher"})
	@ParamSecurity(enabled = true)
	public BaseReplyMsg detailSwitcher(@Valid DetailSwitcherMsg msg, HttpServletRequest request, HttpServletResponse response) throws Exception {
		SwitcherItemVo vo = service.detailSwitcher(msg.getId(), getUser(request, response), request, response);
		BaseReplyMsg reply = new BaseReplyMsg();
		reply.setData(vo);
		sysBaseAPI.addLog("查看IT设备详情；ID" + msg.getId()+"，交换机名称："+vo.getName(),SysLogType.OPERATE, SysLogOperateType.DETAIL, null, null, msg.getTitle() != null ? msg.getTitle():"精确管控与供电制冷联动-基础资源细粒度感知-资源管理-L2层：IT设备");
		// sysBaseAPI.addLog("交换机详情；ID:" + msg.getId(), null, CommonConstant.OPERATE_TYPE_DETAIL);
		return reply;
	}
    
    // @AutoLog(value = "查看交换机列表",operateType = CommonConstant.OPERATE_TYPE_LIST, module = "精确管控与供电制冷联动-基础资源细粒度感知-资源管理-L2层：IT设备")
	@ApiOperation(value = "交换机分页", notes = "交换机分页")
	@RequestMapping(value = "/pageSwitcher", method = RequestMethod.POST)
	@RequiresPermissions(value = {"assetPageSwitcher"})
    @ParamSecurity(enabled = true)
	public PageResult<SwitcherItemVo> pageSwitcher(@Valid PageSwitcherMsg msg, HttpServletRequest request, HttpServletResponse response) throws Exception {
		PageResult<SwitcherItemVo> result = service.pageSwitcher(msg.getPageNo(), msg.getPageSize(), msg.getName(),
				getUser(request, response), request, response);
		sysBaseAPI.addLog("查询IT设备列表", SysLogType.OPERATE, SysLogOperateType.QUERY, null, null, msg.getTitle() != null ? msg.getTitle():"精确管控与供电制冷联动-基础资源细粒度感知-资源管理-L2层：IT设备");
		return result;
	}

    // @AutoLog(value = "新增服务器",operateType = CommonConstant.OPERATE_TYPE_ADD)
	@ApiOperation(value = "新增服务器", notes = "新增服务器")
	@RequestMapping(value = "/createNode", method = RequestMethod.POST)
	@RequiresPermissions(value = {"assetCreateNode"})
	@ParamSecurity(enabled = true)
	public BaseReplyMsg createNode(@Valid CreateNodeSmMsg msg, HttpServletRequest request, HttpServletResponse response) throws Exception {
    	BaseReplyMsg reply = new BaseReplyMsg();
//    	service.createNode(msg, getUser(request, response));
    	String id = service.createNode(msg, getUser(request, response), request, response);
    	StringBuffer buff = new StringBuffer();
    	buff.append("新增IT设备；ID：" + id);
    	buff.append("，"+msg.toString());
    	sysBaseAPI.addLog(buff.toString(), SysLogType.OPERATE, SysLogOperateType.ADD, userId, null, msg.getTitle() != null ? msg.getTitle():"精确管控与供电制冷联动-基础资源细粒度感知-资源管理-L2层：IT设备");
//    	sysBaseAPI.addLog("新增服务器；ID:" + id, null, CommonConstant.OPERATE_TYPE_ADD);
    	return reply;
	}

    // @AutoLog(value = "编辑服务器",operateType = CommonConstant.OPERATE_TYPE_UPDATE)
	@ApiOperation(value = "编辑服务器", notes = "编辑服务器")
	@RequestMapping(value = "/updateNode", method = RequestMethod.POST)
	@RequiresPermissions(value = {"assetUpdateNode"})
	@ParamSecurity(enabled = true)
	public BaseReplyMsg updateNode(@Valid UpdateNodeSmMsg msg, HttpServletRequest request, HttpServletResponse response) throws Exception {
    	BaseReplyMsg reply = new BaseReplyMsg();
//    	service.updateNode(msg, getUser(request, response));
    	service.updateNode(msg, getUser(request, response), request, response);
    	StringBuffer buff = new StringBuffer();
    	buff.append("编辑IT设备；ID:" + msg.getId());
    	buff.append("，"+msg.toString());
//    	sysBaseAPI.addLog("编辑服务器；ID:" + msg.getId(), null, CommonConstant.OPERATE_TYPE_UPDATE);
    	//sysBaseAPI.addLog("查询易分析的" + sysLogPageReq.getTitle() + "列表", SysLogType.OPERATE, SysLogOperateType.AUDIT_QUERY, null, null, "日志管理");
    	if(StringUtils.isNotBlank(msg.getTitle())) {
    		if(msg.getIsolation() != null) {
    			if(msg.getIsolation()) {
    				sysBaseAPI.addLog("服务器"+"("+ msg.getId()+")"+"进行设备隔离", SysLogType.OPERATE, SysLogOperateType.UPDATE, userId, null, msg.getTitle());
    			}else {
    				sysBaseAPI.addLog("服务器"+"("+ msg.getId()+")"+"进行异常复位", SysLogType.OPERATE, SysLogOperateType.UPDATE, userId, null, msg.getTitle() != null ? msg.getTitle():"精确管控与供电制冷联动-统一管理联动-设备恢复");
    			}    			
    		}else {
    			sysBaseAPI.addLog(buff.toString(), SysLogType.OPERATE, SysLogOperateType.UPDATE, userId, null, msg.getTitle() != null ? msg.getTitle():"精确管控与供电制冷联动-基础资源细粒度感知-资源管理-L2层：IT设备");
    		}
    	}else {
    		sysBaseAPI.addLog(buff.toString(), SysLogType.OPERATE, SysLogOperateType.UPDATE, userId, null, msg.getTitle() != null ? msg.getTitle():"精确管控与供电制冷联动-基础资源细粒度感知-资源管理-L2层：IT设备");
    	}
    	
    	return reply;
	}

    // @AutoLog(value = "删除服务器",operateType = CommonConstant.OPERATE_TYPE_DELETE)
	@ApiOperation(value = "删除服务器", notes = "删除服务器")
	@RequestMapping(value = "/deleteNode", method = RequestMethod.POST)
	@RequiresPermissions(value = {"assetDeleteNode"})
	@ParamSecurity(enabled = true)
	public BaseReplyMsg deleteNode(@Valid DeleteNodeMsg msg) throws Exception {
    	BaseReplyMsg reply = new BaseReplyMsg();
    	IaasEntity iaas = service.deleteNode(msg.getId(), userId);
		sysBaseAPI.addLog("删除IT设备；ID:" + msg.getId()+"，服务器名称："+iaas.getName(), SysLogType.OPERATE, SysLogOperateType.DELETE, userId, null, msg.getTitle() != null ? msg.getTitle():"精确管控与供电制冷联动-基础资源细粒度感知-资源管理-L2层：IT设备");
		// sysBaseAPI.addLog("删除服务器；ID:" + msg.getId(), null, CommonConstant.OPERATE_TYPE_DELETE);
		return reply;
	}

    // @AutoLog(value = "服务器详情",operateType = CommonConstant.OPERATE_TYPE_DETAIL)
	@ApiOperation(value = "服务器详情", notes = "服务器详情")
	@RequestMapping(value = "/detailNode", method = RequestMethod.POST)
	@RequiresPermissions(value = {"assetDetailNode"})
	@ParamSecurity(enabled = true)
	public BaseReplyMsg detailNode(@Valid DetailNodeMsg msg, HttpServletRequest request, HttpServletResponse response) throws Exception {
//		NodeItemVo vo = service.detailNode(msg.getId(), getUser(request, response));
		NodeItemVo vo = service.detailNode(msg.getId(), getUser(request, response), request, response);
		BaseReplyMsg reply = new BaseReplyMsg();
		reply.setData(vo);
//		setSm3Header(vo.getIpmiPassword(), request, response);
		sysBaseAPI.addLog("查看IT设备详情；ID:" + msg.getId()+"，服务名称："+vo.getName(), SysLogType.OPERATE, SysLogOperateType.DETAIL, userId, null, msg.getTitle() != null ? msg.getTitle():"精确管控与供电制冷联动-基础资源细粒度感知-资源管理-L2层：IT设备");
//		sysBaseAPI.addLog("服务器详情；ID:" + msg.getId(), SysLogType.OPERATE, SysLogOperateType.DETAIL, userId, null, "资源管理");
//		sysBaseAPI.addLog("服务器详情；ID:" + msg.getId(), null, CommonConstant.OPERATE_TYPE_DETAIL);
		return reply;
	}

   //  @AutoLog(value = "查看服务器列表",operateType = CommonConstant.OPERATE_TYPE_LIST, module = "精确管控与供电制冷联动-基础资源细粒度感知-资源管理-L2层：IT设备")
	@ApiOperation(value = "服务器分页", notes = "服务器分页")
	@RequestMapping(value = "/pageNode", method = RequestMethod.POST)
	@RequiresPermissions(value = {"assetPageNode"})
    @ParamSecurity(enabled = true)
	public PageResult<NodeItemVo> pageNode(@Valid PageNodeMsg msg) throws Exception {
		PageResult<NodeItemVo> result = service.pageNode(msg.getPageNo(), msg.getPageSize(), msg.getDatacenterId(),
				msg.getName(), msg.getOrder(), userId);
		sysBaseAPI.addLog(msg.getContent() != null?msg.getContent():"查询服务器列表", SysLogType.OPERATE, SysLogOperateType.QUERY, null, null, msg.getTitle() != null ? msg.getTitle():"精确管控与供电制冷联动-基础资源细粒度感知-资源管理-L2层：IT设备");
		return result;
	}

    // @AutoLog(value = "基础设施同步",operateType = CommonConstant.OPERATE_TYPE_SYNC, module = "精确管控与供电制冷联动-基础资源细粒度感知-资源管理-L1层：基础设施")
	@ApiOperation(value = "基础设施同步", notes = "基础设施同步")
	@RequestMapping(value = "/syncDevice", method = RequestMethod.GET)
	@RequiresPermissions(value = {"AssetSyncDevice"})
    @ParamSecurity(enabled = true)
	public BaseReplyMsg syncDevice() throws Exception {
    	BaseReplyMsg reply = new BaseReplyMsg();
		String res = service.syncDevice();
		sysBaseAPI.addLog("基础设施同步"+"("+res+")", SysLogType.OPERATE, SysLogOperateType.SYNC, null, null, "精确管控与供电制冷联动-基础资源细粒度感知-资源管理-L1层：基础设施");
		reply.setData(res);
		return reply;
	}

    // @AutoLog(value = "IT设备同步",operateType = CommonConstant.OPERATE_TYPE_DETAIL, module = "精确管控与供电制冷联动-基础资源细粒度感知-资源管理-L2层：IT设备")
	@ApiOperation(value = "IT设备同步", notes = "IT设备同步")
	@RequestMapping(value = "/syncNodeSwitcher", method = RequestMethod.GET)
	@RequiresPermissions(value = {"AssetSyncNodeSwitcher"})
    @ParamSecurity(enabled = true)
	public BaseReplyMsg syncNodeSwitcher() throws Exception {
    	BaseReplyMsg reply = new BaseReplyMsg();
		String res = service.syncNodeSwitcher();
		sysBaseAPI.addLog("IT设备同步"+"("+res+")", SysLogType.OPERATE, SysLogOperateType.SYNC, null, null, "精确管控与供电制冷联动-基础资源细粒度感知-资源管理-L2层：IT设备");
		reply.setData(res);
		return reply;
	}

    // @AutoLog(value = "查询IT设备列表",operateType = CommonConstant.OPERATE_TYPE_LIST, module =  "精确管控与供电制冷联动-基础资源细粒度感知-资源管理-L2层：IT设备")
	@ApiOperation(value = "IT设备分页", notes = "IT设备分页")
	@RequestMapping(value = "/pageItDevice", method = RequestMethod.POST)
	@RequiresPermissions(value = {"assetPageItDevice"})
    @ParamSecurity(enabled = true)
	public PageResult<DeviceItemVo> pageItDevice(@Valid PageItDeviceMsg msg) throws Exception {
		PageResult<DeviceItemVo> result = service.pageItDevice(msg.getPageNo(), msg.getPageSize(), msg.getDatacenterId(), msg.getName(), msg.getType());
		sysBaseAPI.addLog(msg.getContent() != null?msg.getContent():"查询IT设备列表", SysLogType.OPERATE, SysLogOperateType.QUERY, null, null, msg.getTitle() != null ? msg.getTitle():"精确管控与供电制冷联动-基础资源细粒度感知-资源管理-L2层：IT设备");
		return result;
	}

    // @AutoLog(value = "查看IT设备统计",operateType = CommonConstant.OPERATE_TYPE_DETAIL, module = "精确管控与供电制冷联动-基础资源细粒度感知-资源管理-L2层：IT设备")
	@ApiOperation(value = "IT设备统计", notes = "IT设备统计")
	@RequestMapping(value = "/countItDevice", method = RequestMethod.POST)
	@RequiresPermissions(value = {"assetCountItDevice"})
    @ParamSecurity(enabled = true)
	public CountDeviceVo countItDevice(String datacenterId, HttpServletRequest request) throws Exception {
		CountDeviceVo result = service.countDevice(datacenterId);
		if(request.getParameter("title") != null) {
			sysBaseAPI.addLog(request.getParameter("content") != null ? request.getParameter("content"): "查询IT设备列表", SysLogType.OPERATE, SysLogOperateType.DETAIL, null, null, request.getParameter("title") != null ? request.getParameter("title"):"精确管控与供电制冷联动-基础资源细粒度感知-资源管理-L2层：IT设备");
		}
		return result;
	}

	// @AutoLog(value = "查询IT设备列表",operateType = CommonConstant.OPERATE_TYPE_LIST)
	@ApiOperation(value = "IT设备列表", notes = "IT设备列表")
	@RequestMapping(value = "/listDevice", method = RequestMethod.POST)
//	@RequiresPermissions(value = {"assetListDevice"})
	@ParamSecurity(enabled = true)
	public List<DeviceItemVo> listDevice(@Valid ListDeviceMsg msg) throws Exception {
		List<DeviceItemVo> result = service.listDevice(msg.getDatacenterId(), msg.getType(), userId);
		sysBaseAPI.addLog("查询IT设备列表", SysLogType.OPERATE, SysLogOperateType.QUERY, null, null, msg.getTitle() != null ? msg.getTitle():"精确管控与供电制冷联动-基础资源细粒度感知-资源管理-L2层：IT设备");
		return result;
	}

	@AutoLog(value = "查询节点列表",operateType = CommonConstant.OPERATE_TYPE_LIST)
	@ApiOperation(value = "节点列表", notes = "节点列表")
	@RequestMapping(value = "/listNode", method = RequestMethod.POST)
//	@RequiresPermissions(value = {"assetListNode"})
	@ParamSecurity(enabled = true)
	public List<NodeItemVo> listNode(@Valid ListNodeMsg msg) throws Exception {
		List<NodeItemVo> result = service.listNode(msg.getDatacenterId(), userId);
		return result;
	}
	
    private String getUser(HttpServletRequest request, HttpServletResponse response) {
    	String token = request.getHeader(HEADER_TOKEN);  	
    	String userId = null;
    	try {
    		String username = JwtUtil.getUsername(token);
    		LoginUser sysUser = sysBaseAPI.getUserByName(username);
    		userId = sysUser.getId();
    	} catch(Exception e) {
    		log.error("[SM][AssetController][getUser] 获取用户异常：{}", e.getMessage());
    	}	
		return userId;
    }
    
    private void setSm3Header(String content, HttpServletRequest request, HttpServletResponse response) {
    	String digest = null;
    	try {
    		digest = smService.encryptSm3(content, getUser(request, response));
    		log.info("[SM][AssetController][setSm3Header] SM3对数据【{}】，加密生成发送到客户端信息摘要：{}", content, digest);
    	} catch(Exception e) {
    		log.error("[SM][AssetController][setSm3Header] sm3 加密异常：{}", e.getMessage());
    	}
    	response.setHeader(SmManager.SM3_HEADER, digest);
    }
}
