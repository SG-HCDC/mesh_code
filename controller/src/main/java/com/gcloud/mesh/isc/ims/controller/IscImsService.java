package com.gcloud.mesh.isc.ims.controller;

import javax.jws.WebParam;
import javax.jws.WebService;

@WebService(name = "iscImsService", // 暴露服务名称
targetNamespace = "http://webservice.ims.webservice.isc.sgcc.com"// 命名空间,一般是接口的包名倒序
)
public interface IscImsService {

	String getKPIValue(@WebParam(name = "param") String param);
	
	String getStatus(@WebParam(name = "param") String param);
}
