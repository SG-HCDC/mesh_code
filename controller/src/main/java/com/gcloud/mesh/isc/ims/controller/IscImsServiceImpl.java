package com.gcloud.mesh.isc.ims.controller;

import java.io.IOException;
import java.io.StringReader;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;

import javax.jws.WebParam;
import javax.jws.WebService;

import org.jdom.Attribute;
import org.jdom.Document;
import org.jdom.Element;
import org.jdom.JDOMException;
import org.jdom.input.SAXBuilder;
import org.springframework.beans.factory.annotation.Autowired;

@WebService(serviceName = "iscImsService", // 与接口中指定的name一致
		targetNamespace = "http://webservice.ims.webservice.isc.sgcc.com", // 与接口中的命名空间一致,一般是接口的包名倒
		endpointInterface = "com.gcloud.mesh.isc.ims.controller.IscImsService"// 接口地址
)

public class IscImsServiceImpl implements IscImsService {

	@Autowired
	IscImsController controller;

	@Override
	public String getKPIValue(@WebParam(name = "param") String param) {

		String result = "";
		String corporationCode = "";
		SAXBuilder builder = new SAXBuilder();
		Document document;
		try {
			document = builder.build(new StringReader(param));
			Element root = document.getRootElement();
			List<Element> children = root.getChildren();
			List<String> apis = new ArrayList<String>();
			if (children != null && children.size() != 0) {
				for (Element element : children) {
					if (element.getName().equals("CorporationCode")) {
						// TODO 获取CorporationCode
						corporationCode = element.getValue();
						continue;
					}
					List<Attribute> attributes = element.getAttributes();
					for (Attribute attr : attributes) {
						System.out.println(attr.getName() + ":" + attr.getValue());

						apis.add(attr.getValue());
						result = controller.createXml(apis, corporationCode);
					}
				}
			}

		} catch (JDOMException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (SecurityException e) {
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (NoSuchMethodException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			e.printStackTrace();
		}
		return result;
	}

	@Override
	public String getStatus(@WebParam(name = "param") String param) {
		return param;
	}
}
