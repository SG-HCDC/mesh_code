package com.gcloud.mesh.isc.ims.controller;

public enum IscImsMethodType {
	
	BusinessUserRegNum("BusinessUserRegNum", "businessUserRegNum"),
	BusinessSystemOnlineNum("BusinessSystemOnlineNum", "businessSystemOnlineNum"),
	BusinessDayLoginNum("BusinessDayLoginNum", "businessDayLoginNum"),
	BusinessVisitCount("BusinessVisitCount", "businessVisitCount"),
	BusinessSystemSessionNum("BusinessSystemSessionNum", "businessSystemSessionNum"),
	BusinessSystemResponseTime("BusinessSystemResponseTime", "businessSystemResponseTime"),
	BusinessSystemRunningTime("BusinessSystemRunningTime", "businessSystemRunningTime"),
	BusinessDataTableSpace("BusinessDataTableSpace", "businessDataTableSpace"),
	BusinessSystemDBTime("BusinessSystemDBTime", "businessSystemDBTime"),
	BusinessSystemLoginRoll("BusinessSystemLoginRoll", "businessSystemLoginRoll"),
	BusinessSystemOnlineRoll("BusinessSystemOnlineRoll", "businessSystemOnlineRoll"),
	BusinessSystemIaasDeviceNum("BusinessSystemIaasDeviceNum", "businessSystemIaasDeviceNum"),
	BusinessSystemServerNum("BusinessSystemServerNum", "businessSystemServerNum"),
	BusinessSystemSwitcherNum("BusinessSystemSwitcherNum", "businessSystemSwitcherNum"),
	BusinessSystemAlertNum("BusinessSystemAlertNum", "businessSystemAlertNum"),
	BusinessSystemAppNum("BusinessSystemAppNum", "businessSystemAppNum"),
	BusinessSystemSchedulerJobNum("BusinessSystemSchedulerJobNum", "businessSystemSchedulerJobNum"),
	BusinessSystemSubhealthAlertNum("BusinessSystemSubhealthAlertNum", "businessSystemSubhealthAlertNum"),
	BusinessSystemDatacenterNum("BusinessSystemDatacenterNum", "businessSystemDatacenterNum"),
	BusinessSystemCloudResourceVmNum("BusinessSystemCloudResourceVmNum", "businessSystemCloudResourceVmNum"),
	BusinessSystemCloudResourceContainerNum("BusinessSystemCloudResourceContainerNum", "businessSystemCloudResourceContainerNum");
	
	private String name;
	private String methodName;
	
	public String getName() {
		return name;
	}
	
	public String getMethodName() {
		return methodName;
	}
	
	IscImsMethodType(String name, String methodName) {
		this.name = name;
		this.methodName = methodName;
	}
	
	public static String getMethodByName(String name) {
		for (IscImsMethodType type : IscImsMethodType.values()) {
			if(type.getName().equals(name)) {
				return type.getMethodName();
			}
		}
		return null;
	}
}
