package com.gcloud.mesh.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import lombok.Data;

@ConfigurationProperties(prefix = "mesh.data-security")
@Data
@Component
public class DataSecurityConfig {

	private Boolean enable = false;
	private String host;
	private String port;
	private String user;
	private String password;
	private String fulls;
	private String increments;

}
