
package com.gcloud.mesh.supplier.controller;

import com.gcloud.mesh.header.msg.BaseReplyMsg;
import com.gcloud.mesh.header.msg.supplier.*;
import com.gcloud.mesh.header.vo.supplier.SupplierVo;
import com.gcloud.mesh.supplier.service.SupplierService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.jeecg.common.aspect.annotation.AutoLog;
import org.jeecg.common.constant.CommonConstant;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/supplier")
@Api(tags = "第三方系统")
public class SupplierController {

	@Autowired
	private SupplierService service;
	
	//处理Mass Assignment: Insecure Binder Configuration
	@InitBinder
	public void populateCustomerRequest(WebDataBinder binder) {
	    binder.setDisallowedFields(new String[]{});
	}

    @AutoLog(value = "新增系统",operateType = CommonConstant.OPERATE_TYPE_ADD)
	@ApiOperation(value = "新增系统", notes = "新增系统")
	@RequestMapping(value = "/create", method = RequestMethod.POST)
	@RequiresPermissions(value = {"supplierCreate"})
	public void create(@Valid CreateSupplierMsg msg) throws Exception {
		service.create(msg);
	}

    @AutoLog(value = "移除系统",operateType = CommonConstant.OPERATE_TYPE_DELETE)
	@ApiOperation(value = "移除系统", notes = "移除系统")
	@RequestMapping(value = "/delete", method = RequestMethod.POST)
	@RequiresPermissions(value = {"supplierDelete"})
	public void delete(@Valid DeleteSupplierMsg msg) throws Exception {
		service.delete(msg);
	}

    @AutoLog(value = "编辑系统",operateType = CommonConstant.OPERATE_TYPE_UPDATE)
	@ApiOperation(value = "编辑系统", notes = "编辑系统")
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	@RequiresPermissions(value = {"supplierUpdate"})
	public void update(@Valid UpdateSupplierMsg msg) throws Exception {
		service.update(msg);
	}

    @AutoLog(value = "系统详情",operateType = CommonConstant.OPERATE_TYPE_DETAIL)
	@ApiOperation(value = "系统详情", notes = "系统详情")
	@RequestMapping(value = "/get", method = RequestMethod.POST)
	@RequiresPermissions(value = {"supplierGet"})
	public BaseReplyMsg get(@Valid GetSupplierMsg msg) throws Exception {
		SupplierVo vo = service.get(msg);
		BaseReplyMsg reply = new BaseReplyMsg();
		reply.setData(vo);
		return reply;
	}

    @AutoLog(value = "系统列表",operateType = CommonConstant.OPERATE_TYPE_LIST)
	@ApiOperation(value = "系统列表", notes = "系统列表")
	@RequestMapping(value = "/list", method = RequestMethod.POST)
	@RequiresPermissions(value = {"supplierList"})
	public List<SupplierVo> list(@Valid ListSuppliersMsg msg) throws Exception {
		return service.list(msg);
	}

    @AutoLog(value = "k8s系统详情",operateType = CommonConstant.OPERATE_TYPE_DETAIL)
	@ApiOperation(value = "k8s系统详情", notes = "k8s系统详情")
	@RequestMapping(value = "/getByClusterId", method = RequestMethod.POST)
	@RequiresPermissions(value = {"supplierGetByClusterId"})
	public SupplierVo getByClusterId(@Valid GetByClusterIdMsg msg) throws Exception {
		return service.getByClusterId(msg.getId());
	}

}
