package com.gcloud.mesh.supplier.enums;

import java.util.stream.Stream;

public enum SystemType {

	K8S(1, "k8s", "容器云"),
	ALI(2, "ali", "阿里云"),
	HUAWEI(3, "huawei", "华为云"),
	SS(4, "supervision_system", "动环系统"),
	SS_YINGZE(5, "supervision_system_yingze", "北京盈泽动环系统"),
	SS_DAHUA(6, "supervision_system_dahua", "浙江大华动环系统"),
	ALI_POWER(7, "ali_powerconsumption", "阿里功耗平台");
	
	
	private int no;
	private String name;
	private String cnName;
	
	SystemType(int no, String name, String cnName) {
		this.no = no;
		this.name = name;
		this.cnName = cnName;
	}
	
	public static SystemType getSystemTypeByNo(int no) {
		return Stream.of(SystemType.values())
				.filter( s -> s.getNo() == no)
				.findFirst()
				.orElse(null);
	}
	
	public static SystemType getSystemTypeByName(String name) {
		return Stream.of(SystemType.values())
				.filter( s -> s.getName().equals(name))
				.findFirst()
				.orElse(null);
	}

	public static SystemType getSystemTypeByCnName(String name) {
		return Stream.of(SystemType.values())
				.filter( s -> s.getCnName().equals(name))
				.findFirst()
				.orElse(null);
	}
	
	public int getNo() {
		return no;
	}
	
	public String getName() {
		return name;
	}
	
	public String getCnName() {
		return cnName;
	}
	
}
