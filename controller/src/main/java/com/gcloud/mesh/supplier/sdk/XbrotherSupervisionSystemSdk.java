package com.gcloud.mesh.supplier.sdk;

import java.util.ArrayList;
import java.util.List;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.web.client.RestTemplate;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.gcloud.mesh.header.vo.supplier.XbortherAssetItemVo;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class XbrotherSupervisionSystemSdk {
	
	private static final String HTTP = "http://";
	
	private RestTemplate restTemplate = new RestTemplate();
	
	private String hostIp;
	
	private String loginCode;
	
	private static XbrotherSupervisionSystemSdk sdk = null;

	public static void main(String[] args) {
		
		XbrotherSupervisionSystemSdk sdk = new XbrotherSupervisionSystemSdk("192.168.198.100", "BhWMZdFlb/GJu3sJbRDtHGkh3kxapzUQPdOGJQPTTpNDGg7YOB4HYTK7PIci+wYa");
		sdk.listAssets();
		
	}
	
	private XbrotherSupervisionSystemSdk() {}
	
	public XbrotherSupervisionSystemSdk(String hostIp, String loginCode) {
		this.hostIp = hostIp;
		this.loginCode = loginCode;
	}
	
	private String getSession(String hostIp, String loginCode) {

		String uri = "http://" + hostIp + "/api/v3/auth/sso/login";
		JSONObject postData = new JSONObject();
		postData.put("text", loginCode);
		String session = null;
		try {
			JSONObject json = restTemplate.postForEntity(uri, postData, JSONObject.class).getBody();
			JSONObject data = json.getJSONObject("data");
			session = data.getString("session");
		} catch(Exception e) {
			log.error("supplier system {} login error: {}", hostIp, e.getMessage());
		}
		return session;
	}
	
	public List<XbortherAssetItemVo> listAssets() {
		String token = getSession(hostIp, loginCode);
		String unixTimeStamp = Long.toString(System.currentTimeMillis());
		String uri = HTTP + hostIp +  "/api/v2/mblock/asset/list?order=DESC&_=" + unixTimeStamp;
		HttpHeaders headers = new HttpHeaders();
		headers.set("cookie", "X_GU_SID=" + token);
		HttpEntity<JSONObject> httpEntity = new HttpEntity<JSONObject>(headers);
		
		List<XbortherAssetItemVo> assets = new ArrayList<XbortherAssetItemVo>();
		try {
			JSONObject json = restTemplate.getForObject(uri, JSONObject.class, httpEntity);
			JSONArray jsonArray = json.getJSONArray("rows");
			if(jsonArray.size() > 0) {
				for(int i=0; i<jsonArray.size(); i++) {
					JSONObject obj = jsonArray.getJSONObject(i);
					XbortherAssetItemVo vo = (XbortherAssetItemVo)JSONObject.parseObject(obj.toString(), XbortherAssetItemVo.class);
					assets.add(vo);
				}
			}
		} catch(Exception e) {
			log.error("supplier system {} list error: {}", hostIp, e.getMessage());
		}
		return assets;
	}
	
	public static XbrotherSupervisionSystemSdk getInstance(String hostIp, String loginCode) {

		synchronized(XbrotherSupervisionSystemSdk.class) {
			if( sdk == null) {
				sdk = new XbrotherSupervisionSystemSdk(hostIp, loginCode);
			}
			return sdk;
		} 
		
	}

}
