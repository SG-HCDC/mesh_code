
package com.gcloud.mesh.supplier.dao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.util.Strings;
import org.springframework.stereotype.Repository;

import com.alibaba.fastjson.JSONObject;
import com.gcloud.framework.db.dao.impl.JdbcBaseDaoImpl;
import com.gcloud.mesh.supplier.entity.SupplierEntity;

@Repository
public class SupplierDao extends JdbcBaseDaoImpl<SupplierEntity, String> {

	public SupplierEntity getByName(String name) {
		return this.findUniqueByProperty("name", name);
	}

	public <E> List<E> list(String datacenterId, Class<E> clazz) {
		return this.findByProperty("datacenterId", datacenterId, clazz);
	}

	public String getConfigValueByDatacenterIdAndConfigName(String id, String type, String name) {
		Map<String, Object> param = new HashMap<>();
		param.put("datacenterId", id);
		param.put("type", type);
		List<SupplierEntity> supperlier = this.findByProperties(param);
		if (supperlier != null && supperlier.size() > 0) {
			if (StringUtils.isNotBlank(supperlier.get(0).getConfig())) {
				JSONObject json = JSONObject.parseObject(supperlier.get(0).getConfig());
				if (json != null) {
					return json.getString(name);
				}
			}
		}
		return null;
	}
	
	 public <E> List<E> list(String type, String datacenterId, Class<E> clazz) {
	    	
    	Map<String, Object> props = new HashMap<String, Object>();
    	if( type != null) {
    		props.put("type", type);
    	}
    	if( datacenterId != null) {
    		props.put("datacenter_id", datacenterId);
    	}
    	return this.findByProperties(props, clazz);
	 }

    public String getConfigValueByDatacenterId(String type,String datacenterId) {
		Map<String, Object> props = new HashMap<String, Object>();
		if( StringUtils.isNotBlank(type)) {
			props.put("type", type);
		}
		props.put("datacenter_id", datacenterId);
		SupplierEntity oneByProperties = this.findOneByProperties(props);
		if(oneByProperties==null) return Strings.EMPTY;
		return oneByProperties.getConfig();
    }
}
