package com.gcloud.mesh.supplier.enums;

public enum K8sClusterType {

	CLUSTER_ID("clusterId"),
	RESTVELERO_IP("restveleroIp"),
	HOST_IP("hostIp"),
	LOGIN_CODE("loginCode");

	private String name;

	K8sClusterType(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}

}