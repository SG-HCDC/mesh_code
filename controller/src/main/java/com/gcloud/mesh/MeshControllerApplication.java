package com.gcloud.mesh;

import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.jeecg.common.system.api.ISysBaseAPI;
import org.jeecg.modules.system.mapper.SysLogMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.FilterType;
import org.springframework.context.annotation.ComponentScan.Filter;
import org.springframework.core.env.Environment;
import org.springframework.scheduling.annotation.EnableScheduling;

import com.gcloud.mesh.redis.MockRedis;
import com.gcloud.mesh.utils.CmdExecuteUtil;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@EnableCaching
@EnableScheduling
// @EnableDiscoveryClient
@EnableFeignClients(basePackages = { "com.gcloud.mesh.sdk" })
@SpringBootApplication
@ComponentScan(basePackages = { "com.gcloud.mesh", "org.jeecg" }, excludeFilters = {@Filter(type = FilterType.REGEX, pattern = "com.gcloud.mesh.framework.core.mq.*")})
public class MeshControllerApplication {

	public static String runLogId;
	
	// @Autowired
	// private DiscoveryClient discoveryClient;
	//
	// @Autowired
	// private ConsulDiscoveryClient client;
	
	private static SysLogMapper sysLogMapper;
	
	private static MockRedis<Long> redis;

	private static ISysBaseAPI sysBaseAPI;

	public static void main(String[] args) throws UnknownHostException {
//		Date startTime = new Date();
		ConfigurableApplicationContext application = SpringApplication.run(MeshControllerApplication.class, args);
		Environment env = application.getEnvironment();
		String ip = getHostIp();
		String port = env.getProperty("server.port");
		String path = env.getProperty("server.servlet.context-path");
		log.info("\n----------------------------------------------------------\n\t" + "Application Mesh Controller is running! Access URLs:\n\t" + "External: \thttp://" + ip + ":" + port + path + "/\n\t" + "Swagger文档: \thttp://" + ip + ":" + port + path + "/doc.html\n" + "----------------------------------------------------------");


//		runLogId = sysBaseAPI.addLog("基于“国网云”的数据中心基础资源精确管控与供电制冷联动的多数据中心调度和关键设备智能健康分析系统", SysLogType.RUN, SysLogOperateType.RUN);
//		
//		
		Date now = new Date();
//		SysLog sysLog = sysLogMapper.selectById(Application.runLogId);
//		if (sysLog != null) {
//			sysLog.setRemark(DateUtils.date2Str(now, new SimpleDateFormat("yyyy-MM-dd HH:mm:ss")));
//			sysLog.setCreateTime(startTime);
//			sysLogMapper.updateById(sysLog);
//			log.info("修改启动时间");
//		}
//		
//		//保留系统启动时间，计算系统运行时长
		Long beginTime = now.getTime();
		redis.setBeginTime(beginTime);
		
	}	
	
	public SysLogMapper getSysLogMapper() {
		return sysLogMapper;
	}
	
	@Autowired
	public void setSysLogMapper(SysLogMapper sysLogMapper) {
		MeshControllerApplication.sysLogMapper = sysLogMapper;
	}
	
	public MockRedis<Long> getRedis() {
		return redis;
	}
	
	@Autowired
	public void setRedis(MockRedis<Long> redis) {
		MeshControllerApplication.redis = redis;
	}
	
	public ISysBaseAPI getSysBaseAPI() {
		return sysBaseAPI;
	}
	
	@Autowired
	public void setSysBaseAPI(ISysBaseAPI sysBaseAPI) {
		MeshControllerApplication.sysBaseAPI = sysBaseAPI;
	}
	
	private static String getHostIp() {
		String ip = "";
//		String ip = InetAddress.getLocalHost().getHostAddress();
		List<String> cmdStrs = new ArrayList<String>();
		cmdStrs.add(" ifconfig eth0");
		cmdStrs.add(" | ");
		cmdStrs.add(" grep 'inet ' ");
		cmdStrs.add(" | ");
		cmdStrs.add(" awk -F' ' '{printf $2}' ");
		String[] cmds = cmdStrs.toArray(new String[cmdStrs.size()]);
		try {
			ip = CmdExecuteUtil.runAndGetValueWithPipe(cmds);
		}catch(Exception e) {
			log.error("get ip error! : {}", e.getMessage());
		}	
		return ip;
	}

}
