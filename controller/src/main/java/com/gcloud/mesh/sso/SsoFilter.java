package com.gcloud.mesh.sso;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.jeecg.config.IscConfig;
import org.jeecg.modules.system.service.IUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import com.gcloud.mesh.framework.core.util.JsonUtil;
import com.sgcc.isc.service.adapter.utils.CASClient;
import com.sgcc.isc.ualogin.client.util.IscSSOResourceUtil;
import com.sgcc.isc.ualogin.client.vo.IscSSOUserBean;

import lombok.extern.slf4j.Slf4j;

@Slf4j
//@Component
public class SsoFilter implements Filter {

	@Autowired
	IUserService userService;

	@Autowired
	private IscConfig iscConfig;

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException  {
		HttpServletRequest req = (HttpServletRequest) request;

		
		IscSSOUserBean iscSSOUserBean = null;
		try {
			iscSSOUserBean = IscSSOResourceUtil.getIscUserBean(req);
		} catch (Exception e1) {
			e1.printStackTrace();
		}
		if (iscSSOUserBean == null || StringUtils.isBlank(iscSSOUserBean.getIscUserId())) {
			log.error("user no login isc ");
			throw new IOException("user no login isc ");
		} else {
			// ICasClientService cas = AdapterFactory.getCasClientService();
			// cas.ticketValidateTime(arg0, arg1, Iscu)
			String tgt = CASClient.getTgtByUserId(IscConfig.getIscSsoPath(), iscSSOUserBean.getIscUserId());
			if (StringUtils.isNotBlank(tgt)) {
				boolean result = CASClient.tgtValidate(IscConfig.getIscSsoPath(), tgt, iscSSOUserBean.getIscUserId());
				log.info("========-------token----username---" + iscSSOUserBean.getIscUserSourceId() + "=======tgtValidate:" + result);
				
				String ticketValidateTimeUrl = IscConfig.getIscSsoPath() + "/ticketValidateTime";
				RestTemplate restTemplate = new RestTemplate();
				Map<String, String> map = new HashMap<>();
				map.put("tgt", tgt);
				map.put("appId", IscConfig.getIscAppId());

				try {
					log.info("ISC ticketValidateTime url-user:" + iscSSOUserBean.getIscUserSourceId() + "    url:" + ticketValidateTimeUrl + "   params:" + JsonUtil.toJSONObject(map));
				} catch (Exception e) {
					e.printStackTrace();
				}
				ResponseEntity<String> ticketValidateTimeRes = restTemplate.getForEntity(ticketValidateTimeUrl, String.class, map);
				log.info("ISC ticketValidateTime res-user:" + iscSSOUserBean.getIscUserSourceId() + "   res:" + ticketValidateTimeRes.toString());
				log.info("ISC ticketValidateTime sucess-user:" + iscSSOUserBean.getIscUserSourceId());
				
				
			}
		}
		
		
	
		chain.doFilter(request, response);
	}

}
